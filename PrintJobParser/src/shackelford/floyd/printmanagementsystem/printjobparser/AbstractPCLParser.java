package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.IOException;

import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;


/**
 * <p>Title: AbstractPCLParser</p>
 * <p>Description:
 * abstract class for PCL parsers.
 * all PCL print jobs look like this:
 *
 * beginUEL
 *   streamHeader
 *   streamBody
 *     beginSession
 *       beginPage
 *       endPage
 *       ...
 *       beginPage
 *       endPage
 *     endSession
 *     ...
 *     beginSession
 *       ...
 *     endSession
 * endUEL
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPCLParser
  extends AbstractPrintJobLanguageParser
{

  protected static final String PJL_UEL = "\u001b%-12345X";	// <ESC>%-12345X   Universal End Language command

  protected static final String PJL_ENTER_LANGUAGE   = "@PJL ENTER LANGUAGE=";
  protected static final String PJL_SET_RESOLUTION   = "@PJL SET RESOLUTION=";
  protected static final String PJL_SET_RET          = "@PJL SET RET=";
  protected static final String PJL_SET_DUPLEX       = "@PJL SET DUPLEX=";
  protected static final String PJL_SET_ECONOMODE    = "@PJL SET ECONOMODE=";
  protected static final String PJL_SET_OUTBIN       = "@PJL SET OUTBIN=";
  protected static final String PJL_SET_FINISH       = "@PJL SET FINISH=";
  protected static final String PJL_SET_PAGEPROTECT  = "@PJL SET PAGEPROTECT=";
  protected static final String PJL_SET_PAPER        = "@PJL SET PAPER=";
  protected static final String PJL_SET_HOLD         = "@PJL SET HOLD=";
  protected static final String PJL_SET_BITSPERPIXEL = "@PJL SET BITSPERPIXEL=";
  protected static final String PJL_JOB_NAME         = "@PJL JOB NAME=";

  protected static final Object[] EMPTY_OBJECT_ARRAY = new Object[0];
  protected static final String   EMPTY_STRING       = "";

  /**
   * Constructor
   * @param printJob
   */
  protected AbstractPCLParser(PrintJob printJob)
  {
    super(printJob);
  }

  protected boolean ComputeOffsets()
    throws IOException
  {
    // there should be a UEL at the very beginning of the file.
    // we'll be generous and look for the first ocurrance of one in case it's not at the
    // exact beginning of the file.
    _beginUEL = _printJob.FindPattern(0, _printJob.GetFileLength(), PJL_UEL);
    if (_beginUEL == PrintJob.NOT_FOUND)
    {
      // _beginEUL not found
      return false;
    }

    // there should be a UEL at the very end of the file
    _endUEL = _printJob.FindPattern((_printJob.GetFileLength() - PJL_UEL.length()),_printJob.GetFileLength(), PJL_UEL);
    if (_endUEL == PrintJob.NOT_FOUND)
    {
      // there is no UEL at the end of the file, it might be earlier, so let's look for it after
      // the one at the beginning of the file
      _endUEL = _printJob.FindPattern((_beginUEL + PJL_UEL.length()),_printJob.GetFileLength(), PJL_UEL);
      if (_endUEL == PrintJob.NOT_FOUND)
      {
        // endUEL not found
        return false;
      }
    }

    // the pjl commands begin immediately after the beginning UEL
    _pjlStart = _beginUEL + PJL_UEL.length();

    // the stream starts right after the LINE_FEED following the PJL_ENTER_LANGUAGE command
    _streamStart = _printJob.FindPattern(_pjlStart,_endUEL, PJL_ENTER_LANGUAGE);
    if (_streamStart == PrintJob.NOT_FOUND)
    {
      // PJL_ENTER_LANGUAGE not found
      return false;
    }
    // some machines use \n\r (windows), other machines use \r\n (macintosh), and others just \n (unix)
    // since \n is common to all machines, we'll look for that
    _streamStart = _printJob.FindPattern((_streamStart + PJL_ENTER_LANGUAGE.length()),_endUEL, LINE_FEED);
    if (_streamStart == PrintJob.NOT_FOUND)
    {
      // LINE_FEED after PJL_ENTER_LANGUAGE not found
      return false;
    }
    // skip past the LINE_FEED
    _streamStart++;
    // skip past the CARRIAGE_RETURN if there's one there
    if (_printJob.GetByteAt(_streamStart) == CARRIAGE_RETURN.getBytes()[0])
    {
      _streamStart++;
    }

    return true;
  }

  /**
   * this method processes the PJL in the job header common to PCL print job streams
   */
  protected void ProcessJobHeader()
    throws IOException
  {
    _resolution = GetPJLCommandValue(PJL_SET_RESOLUTION);
    _language = GetPJLCommandValue(PJL_ENTER_LANGUAGE);
    _duplex = GetPJLCommandValue(PJL_SET_DUPLEX);
    _economode = GetPJLCommandValue(PJL_SET_ECONOMODE);
    _ret = GetPJLCommandValue(PJL_SET_RET);
    _outbin = GetPJLCommandValue(PJL_SET_OUTBIN);
    _finish = GetPJLCommandValue(PJL_SET_FINISH);
    _pageprotect = GetPJLCommandValue(PJL_SET_PAGEPROTECT);
    _paperType = GetPJLCommandValue(PJL_SET_PAPER);
    _hold = GetPJLCommandValue(PJL_SET_HOLD);
    _bitsPerPixel = GetPJLCommandValue(PJL_SET_BITSPERPIXEL);
    _jobName = GetPJLCommandValue(PJL_JOB_NAME);
  }

  protected String GetPJLCommandValue(String pjlCommand)
    throws IOException
  {
    int startOffset = _printJob.FindPattern(_pjlStart,_streamStart, pjlCommand);
    if (startOffset != PrintJob.NOT_FOUND)
    {
      startOffset +=pjlCommand.length();
      int endOffset = _printJob.FindPattern(startOffset,_streamStart, LINE_FEED);
      // back up to before the LINE_FEED
      endOffset--;
      // back up to before the CARRIAGE_RETURN if there's one there
      if (_printJob.GetByteAt(endOffset) == CARRIAGE_RETURN.getBytes()[0])
      {
        endOffset--;
      }
      // we should now know where the command value is
      return new String(_printJob.GetByteRange(startOffset,endOffset));
    }
    return null;
  }

  @Override
  protected void AssignPropertiesToPrintJob()
  {
    super.AssignPropertiesToPrintJob();

    if (_language != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_LANGUAGE,_language);
    }

    if (_duplex != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_DUPLEX,_duplex);
    }

    if (_economode != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_ECONOMODE,_economode);
    }

    if (_ret != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_RET,_ret);
    }

    if (_outbin != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_OUTBIN,_outbin);
    }

    if (_finish != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_FINISH,_finish);
    }

    if (_pageprotect != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAGE_PROTECT,_pageprotect);
    }

    if (_hold != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_HOLD,_hold);
    }

    if (_bitsPerPixel != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_BITS_PER_PIXEL,_bitsPerPixel);
    }

  }

  protected int      _beginUEL = 0;
  protected int      _endUEL = 0;
  protected int      _streamStart = 0;
  protected int      _pjlStart = 0;

  protected String   _bitsPerPixel = null;
  protected String   _duplex = null;
  protected String   _economode = null;
  protected String   _finish = null;
  protected String   _hold = null;
  protected String   _outbin = null;
  protected String   _pageprotect = null;
  protected String   _ret = null;

}