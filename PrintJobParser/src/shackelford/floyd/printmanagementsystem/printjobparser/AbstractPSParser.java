package shackelford.floyd.printmanagementsystem.printjobparser;



/**
 * <p>Title: AbstractPSParser</p>
 * <p>Description:
 * abstract class for PS parsers
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPSParser
  extends AbstractPrintJobLanguageParser
{

  public AbstractPSParser (PrintJob printJob)
  {
    super(printJob);
  }

}