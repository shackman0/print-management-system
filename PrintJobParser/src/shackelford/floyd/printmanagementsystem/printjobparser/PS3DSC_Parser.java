package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.IOException;

import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;


/**
 * <p>Title: PS3DSC_Parser</p>
 * <p>Description:
 * parses postscript 3 and extracts print job characteristics
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PS3DSC_Parser
  extends AbstractPSParser
{

  protected static final String PS_ADOBE_3 = "%!PS-Adobe-3";

  /*
   * the "PostScript Language Document Structuring Conventions Specification" states:
   * 
   * <quote>
   * DSC comments are specified by two percent characters (%%) as the first
   * characters on a line (no leading white space). These characters are immediately
   * followed by a unique keyword describing that particular comment �
   * again, no white space. The keyword always starts with a capital letter and
   * is almost always mixed-case. For example:
   * 
   * %%BoundingBox: 0 0 612 792
   * %%Pages: 45
   * %%BeginSetup
   * 
   * Note that some keywords end with a colon (considered to be part of the
   * keyword), which signifies that the keyword is further qualified by options or
   * arguments. There should be one space character between the ending colon
   * of a keyword and its subsequent arguments.
   * </quote>
   */

  protected static final String DSC_PREFIX = "%%";

  protected static final String DSC_CONTINUATION       = DSC_PREFIX + "+";
  protected static final String DSC_ORIENTATION        = DSC_PREFIX + "Orientation:";
  protected static final String DSC_PAGES              = DSC_PREFIX + "Pages:";
  protected static final String DSC_PAGE               = DSC_PREFIX + "Page:";
  protected static final String DSC_TITLE              = DSC_PREFIX + "Title:";
  protected static final String DSC_BOUNDING_BOX       = DSC_PREFIX + "BoundingBox:";
  protected static final String DSC_PAGE_ORIENTATION   = DSC_PREFIX + "PageOrientation:";
  protected static final String DSC_EOF                = DSC_PREFIX + "EOF";
  protected static final String DSC_DOCUMENT_MEDIA     = DSC_PREFIX + "DocumentMedia:";
  protected static final String DSC_REQUIREMENTS       = DSC_PREFIX + "Requirements:";
  protected static final String DSC_PAGE_REQUIREMENTS  = DSC_PREFIX + "PageRequirements:";
  protected static final String DSC_PAGE_TRAILER       = DSC_PREFIX + "PageTrailer";

  protected static final String COLOR_MODEL = "colormodel";

  // these specify the page counting technique i'm using
  protected static final int PAGE_COUNT_NONE = -1;
  protected static final int PAGE_COUNT_USING_PAGES = 0;
  protected static final int PAGE_COUNT_USING_PAGE = 1;
  protected static final int PAGE_COUNT_USING_PAGE_TRAILER = 2;

  protected static final String DSC_ATEND = "(atend)";


  /**
   * Constructor
   * @param printJob
   */
  /**
   * Constructor
   * @param printJob
   */
  public PS3DSC_Parser (PrintJob printJob)
  {
    super(printJob);
  }

  @Override
  protected boolean ICanParse()
  {
    try
    {
      return (_printJob.FindPattern(0,_printJob.GetFileLength(),PS_ADOBE_3) != PrintJob.NOT_FOUND);
    }
    catch (IOException excp)
    {
      return false;
    }
  }

  @Override
  public boolean ParseTheJob()
  {
    if (ICanParse() == false)
    {
      return false;
    }

    try
    {
      _printJob.SetCurrentOffset(0);
    }
    catch (IOException excp)
    {
      return false;
    }

    _language = "PS3DSC";

    String dscLine = GetNextDSCLine();
    while (dscLine != null)
    {
      ProcessDSCLine(dscLine);
      dscLine = GetNextDSCLine();
    }

    AssignPropertiesToPrintJob();

    return true;
  }

  protected void ProcessDSCLine(String dsc)
  {
    if (dsc.endsWith(DSC_ATEND) == true)
    {
      return;
    }

    /*
     * we can count pages using one of: DSC_PAGE, DSC_PAGE_TRAILER, or DSC_PAGES.
     * we prefer DSC_PAGES over DSC_PAGE and DSC_PAGE over DSC_PAGE_TRAILER.
     * if we find DSC_PAGES, we use it exclusively.
     * otherwise we use either DSC_PAGE or DSC_PAGE_TRAILER, whichever we find first
     * in the document.
     */
    if (dsc.startsWith(DSC_PAGES) == true)
    {
      _numberOfPages = new Integer(dsc.substring(DSC_PAGES.length()).trim()).intValue();
      _pageCountingTechnique = PAGE_COUNT_USING_PAGES;
      return;
    }

    if (dsc.startsWith(DSC_PAGE) == true)
    {
      if ( (_pageCountingTechnique == PAGE_COUNT_NONE) ||
           (_pageCountingTechnique == PAGE_COUNT_USING_PAGE) )
      {
        _pageCountingTechnique = PAGE_COUNT_USING_PAGE;
        _numberOfPages++;
      }
      return;
    }

    if (dsc.startsWith(DSC_PAGE_TRAILER) == true)
    {
      if ( (_pageCountingTechnique == PAGE_COUNT_NONE) ||
           (_pageCountingTechnique == PAGE_COUNT_USING_PAGE_TRAILER) )
      {
        _pageCountingTechnique = PAGE_COUNT_USING_PAGE_TRAILER;
        _numberOfPages++;
      }
      return;
    }

    if (dsc.startsWith(DSC_TITLE) == true)
    {
      if (_jobName == null)
      {
        _jobName = dsc.substring(DSC_TITLE.length()).trim();
      }
      return;
    }

    if (dsc.startsWith(DSC_BOUNDING_BOX) == true)
    {
      return;
    }

    if (dsc.startsWith(DSC_ORIENTATION) == true)
    {
      _orientation = dsc.substring(DSC_ORIENTATION.length()).trim();
      return;
    }

    if (dsc.startsWith(DSC_PAGE_ORIENTATION) == true)
    {
      if (_orientation == null)
      {
        _orientation = dsc.substring(DSC_PAGE_ORIENTATION.length()).trim();
      }
      return;
    }

    if (dsc.startsWith(DSC_DOCUMENT_MEDIA) == true)
    {
      ProcessDocumentMedia(dsc.substring(DSC_DOCUMENT_MEDIA.length()));
      return;
    }

    if (dsc.startsWith(DSC_REQUIREMENTS) == true)
    {
      ProcessRequirements(dsc.substring(DSC_REQUIREMENTS.length()));
      _requirementsSet = true;
      return;
    }

    if (dsc.startsWith(DSC_PAGE_REQUIREMENTS) == true)
    {
      if (_requirementsSet == false)
      {
        ProcessRequirements(dsc.substring(DSC_PAGE_REQUIREMENTS.length()));
      }
      return;
    }

    int index = dsc.toLowerCase().indexOf(COLOR_MODEL);
    if (index != -1)
    {
      _color = dsc.substring(index + COLOR_MODEL.length()).trim().toLowerCase();
      return;
    }

    if (dsc.startsWith(DSC_EOF) == true)
    {
      try
      {
        _printJob.SetCurrentOffset(_printJob.GetFileLength() - 1);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
      return;
    }

  }

  /*
   * the "PostScript Language Document Structuring Conventions Specification" states:
   * 
   * <quote>
   * %%Requirements: <requirement> [(<style> ...)] ...
   *   <requirement> ::= collate | color | duplex | faceup | fax | fold | jog |
   *                     manualfeed | numcopies | punch | resolution | rollfed |
   *                     staple
   *   <style> ::= <text>
   * </quote>
   */

  protected static final String DSC_REQUIREMENTS_COLLATE = "collate";
  protected static final String DSC_REQUIREMENTS_COLOR = "color";
  protected static final String DSC_REQUIREMENTS_DUPLEX = "duplex";
  protected static final String DSC_REQUIREMENTS_FACEUP = "faceup";
  protected static final String DSC_REQUIREMENTS_FAX = "fax";
  protected static final String DSC_REQUIREMENTS_FOLD = "fold";
  protected static final String DSC_REQUIREMENTS_JOB = "jog";
  protected static final String DSC_REQUIREMENTS_MANUALFEED = "manualfeed";
  protected static final String DSC_REQUIREMENTS_NUMCOPIES = "numcopies";
  protected static final String DSC_REQUIREMENTS_PUNCH = "punch";
  protected static final String DSC_REQUIREMENTS_RESOLUTION = "resolution";
  protected static final String DSC_REQUIREMENTS_ROLLFED = "rollfed";
  protected static final String DSC_REQUIREMENTS_STAPLE = "staple";

  protected void ProcessRequirements(String requirements)
  {
    String lowerCaseReqs = requirements.toLowerCase();
    int index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_COLOR);
    if (index != -1)
    {
      _color = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
    }

    index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_DUPLEX);
    if (index != -1)
    {
      _sides = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
    }

    index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_STAPLE);
    if (index != -1)
    {
      _staple = PrintJobPropertiesRow.VALUE_YES;
    }

    index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_NUMCOPIES);
    if (index != -1)
    {
      index += DSC_REQUIREMENTS_NUMCOPIES.length();
      int start = requirements.indexOf("(",index) + 1;
      int end =  requirements.indexOf(")",index);
      _copies = requirements.substring(start,end);
    }

    index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_PUNCH);
    if (index != -1)
    {
      _holePunch = PrintJobPropertiesRow.VALUE_YES;
    }

    index = lowerCaseReqs.indexOf(DSC_REQUIREMENTS_RESOLUTION);
    if (index != -1)
    {
      index += DSC_REQUIREMENTS_RESOLUTION.length();
      int start = requirements.indexOf("(",index) + 1;
      int end =  requirements.indexOf(")",index);
      _resolution = requirements.substring(start,end).replace(',','x');
    }
  }

  /*
   * the "PostScript Language Document Structuring Conventions Specification" states:
   * 
   * <quote>
   *    %%DocumentMedia: <medianame> <attributes>
   *      <medianame> ::= <text> (Tag name of the media)
   *      <attributes> ::= <width> <height> <weight> <color> <type>
   *      <width> ::= <real> (Width in PostScript units)
   *      <height> ::= <real> (Height in PostScript units)
   *      <weight> ::= <real> (Weight in g/m 2 )
   *      <color> ::= <text> (Paper color)
   *      <type> ::= <text> (Type of pre-printed form)
   * </quote>
   */

  protected void ProcessDocumentMedia(String media)
  {
    media = media.trim();
    // skip past the name of the media
    int index = media.indexOf(" ");
    if (index == -1)
    {
      return;
    }
    media = media.substring(index).trim();

    // get the width
    index = media.indexOf(" ");
    if (index == -1)
    {
      return;
    }
    String width = media.substring(0,index);
    float widthF = new Float(width).floatValue() / POINTS_PER_INCH;

    // get the height
    media = media.substring(index).trim();
    index = media.indexOf(" ");
    if (index == -1)
    {
      return;
    }
    String height = media.substring(0,index);
    float heightF = new Float(height).floatValue() / POINTS_PER_INCH;
    media = media.substring(index).trim();

    // construct the paper size property
    _paperSize = String.valueOf(widthF) + "\"x" + String.valueOf(heightF) + "\"";

    // get the paper type
    _paperType = media.substring(index).trim();

  }

  protected String GetNextDSCLine()
  {
    // read lines until we find a dsc_prefix'ed line
    String nextLine = null;
    do
    {
      nextLine = ReadNextLine();
    }
    while ( (nextLine != null) &&
            (nextLine.startsWith(DSC_PREFIX) == false) );

    // assemble the dsc line, which is this line plus any dsc_continuation lines
    String dscLine = nextLine;
    nextLine = PeekNextLine();
    while ( (nextLine != null) &&
            (nextLine.startsWith(DSC_CONTINUATION) == true) )
    {
      dscLine += ReadNextLine().substring(DSC_CONTINUATION.length());
      nextLine = PeekNextLine();
    }

    return dscLine;
  }

  protected String PeekNextLine()
  {
    String nextLine = null;
    if (_printJob.MoreBytes() == true)
    {
      try
      {
        int saveCurrentOffset = _printJob.GetCurrentOffset();
        nextLine = ReadNextLine();
        _printJob.SetCurrentOffset(saveCurrentOffset);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
    }
    return nextLine;
  }

  /*
   * the "PostScript Language Reference, Third Edition" states:
   * 
   * <quote>
   * The PostScript language scanner and the readline operator recognize all three external
   * forms of end-of-line (EOL) � CR alone, LF alone, and the CR-LF pair �
   * and treat them uniformly,
   * </quote>
   */

  protected String ReadNextLine()
  {
    String nextLine = null;
    try
    {

      // position to the first non end of line (eol) character
      while (_printJob.MoreBytes() == true)
      {
        byte nextByte = _printJob.GetNext();
        if ( (nextByte != LINE_FEED_BYTE) &&
             (nextByte != CARRIAGE_RETURN_BYTE) )
        {
          break;
        }
      }

      if (_printJob.MoreBytes() == false)
      {
        // end of file (eof) reached
        return null;
      }

      int startOffset = _printJob.GetCurrentOffset() - 1;

      // position to the next end of line (eol) character
      while (_printJob.MoreBytes() == true)
      {
        byte nextByte = _printJob.GetNext();
        if ( (nextByte == LINE_FEED_BYTE) ||
             (nextByte == CARRIAGE_RETURN_BYTE) )
        {
          break;
        }
      }

      int endOffset = _printJob.GetCurrentOffset() - 1;

      // get the next line
      nextLine = (new String(_printJob.GetByteRange(startOffset,endOffset))).trim();

    }
    catch (IOException excp)
    {
      excp.printStackTrace();
    }

    return nextLine;
  }

  protected int       _pageCountingTechnique = -1;
  protected boolean   _requirementsSet = false;

}