package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;


/**
 * <p>Title: PCL6Binary_Parser</p>
 * <p>Description:
 * parses HP PCL 6 and extracts print job characteristics
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
/**
 * <p>Title: PCL6Binary_Parser</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PCL6Binary_Parser
  extends AbstractPCLParser
{

  protected static final char     PCL_BINDING_ASCII                       = (char) 0x27;
  protected static final char     PCL_BINDING_HIGH_BYTE_FIRST             = (char) 0x28;                                                                       // java likes the high byte first
  protected static final char     PCL_BINDING_LOW_BYTE_FIRST              = (char) 0x29;

  protected static final String[] hexCharacters                           = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b", "c", "d", "e", "f" };

  // these are the values for the operators, data types, and attributes (aka parameters).
  // any values that are not defined here are either white space,
  // which may be safely ignored, or reserved for future use,
  // which also may be safely ignored for now.

  protected static final char     PCL_OPERATOR_LO                         = (char) 0x41;
  protected static final char     PCL_OPERATOR_HI                         = (char) 0xb9;
  protected static final char     PCL_OPERATOR_ArcPath                    = (char) 0x91;
  protected static final char     PCL_OPERATOR_BeginChar                  = (char) 0x52;
  protected static final char     PCL_OPERATOR_BeginFontHeader            = (char) 0x4f;
  protected static final char     PCL_OPERATOR_BeginImage                 = (char) 0xb0;
  protected static final char     PCL_OPERATOR_BeginPage                  = (char) 0x43;
  protected static final char     PCL_OPERATOR_BeginRastPattern           = (char) 0xb3;
  protected static final char     PCL_OPERATOR_BeginScan                  = (char) 0xb6;
  protected static final char     PCL_OPERATOR_BeginSession               = (char) 0x41;
  protected static final char     PCL_OPERATOR_BeginStream                = (char) 0x5b;
  protected static final char     PCL_OPERATOR_BezierPath                 = (char) 0x93;
  protected static final char     PCL_OPERATOR_BezierRelPath              = (char) 0x95;
  protected static final char     PCL_OPERATOR_Chord                      = (char) 0x96;
  protected static final char     PCL_OPERATOR_ChordPath                  = (char) 0x97;
  protected static final char     PCL_OPERATOR_CloseDataSource            = (char) 0x49;
  protected static final char     PCL_OPERATOR_CloseSubPath               = (char) 0x84;
  protected static final char     PCL_OPERATOR_Comment                    = (char) 0x47;
  protected static final char     PCL_OPERATOR_Ellipse                    = (char) 0x98;
  protected static final char     PCL_OPERATOR_EllipsePath                = (char) 0x99;
  protected static final char     PCL_OPERATOR_EndChar                    = (char) 0x54;
  protected static final char     PCL_OPERATOR_EndFontHeader              = (char) 0x51;
  protected static final char     PCL_OPERATOR_EndImage                   = (char) 0xb2;
  protected static final char     PCL_OPERATOR_EndPage                    = (char) 0x44;
  protected static final char     PCL_OPERATOR_EndRastPattern             = (char) 0xb5;
  protected static final char     PCL_OPERATOR_EndScan                    = (char) 0xb8;
  protected static final char     PCL_OPERATOR_EndSession                 = (char) 0x42;
  protected static final char     PCL_OPERATOR_EndStream                  = (char) 0x5d;
  protected static final char     PCL_OPERATOR_ExecStream                 = (char) 0x5e;
  protected static final char     PCL_OPERATOR_LinePath                   = (char) 0x9b;
  protected static final char     PCL_OPERATOR_LineRelPath                = (char) 0x9d;
  protected static final char     PCL_OPERATOR_NewPath                    = (char) 0x85;
  protected static final char     PCL_OPERATOR_OpenDataSource             = (char) 0x48;
  protected static final char     PCL_OPERATOR_PaintPath                  = (char) 0x86;
  protected static final char     PCL_OPERATOR_Pie                        = (char) 0x9e;
  protected static final char     PCL_OPERATOR_PiePath                    = (char) 0x9f;
  protected static final char     PCL_OPERATOR_PopGS                      = (char) 0x60;
  protected static final char     PCL_OPERATOR_PushGS                     = (char) 0x61;
  protected static final char     PCL_OPERATOR_ReadChar                   = (char) 0x53;
  protected static final char     PCL_OPERATOR_ReadFontHeader             = (char) 0x50;
  protected static final char     PCL_OPERATOR_ReadImage                  = (char) 0xb1;
  protected static final char     PCL_OPERATOR_ReadRastPattern            = (char) 0xb4;
  protected static final char     PCL_OPERATOR_ReadStream                 = (char) 0x5c;
  protected static final char     PCL_OPERATOR_Rectangle                  = (char) 0xa0;
  protected static final char     PCL_OPERATOR_RectanglePath              = (char) 0xa1;
  protected static final char     PCL_OPERATOR_RemoveFont                 = (char) 0x55;
  protected static final char     PCL_OPERATOR_SetCharAttributes          = (char) 0x56;
  protected static final char     PCL_OPERATOR_RemoveStream               = (char) 0x5f;
  protected static final char     PCL_OPERATOR_RoundRectangle             = (char) 0xa2;
  protected static final char     PCL_OPERATOR_RoundRectanglePath         = (char) 0xa3;
  protected static final char     PCL_OPERATOR_ScanLineRel                = (char) 0xb9;
  protected static final char     PCL_OPERATOR_SetClipReplace             = (char) 0x62;
  protected static final char     PCL_OPERATOR_SetBrushSource             = (char) 0x63;
  protected static final char     PCL_OPERATOR_SetCharAngle               = (char) 0x64;
  protected static final char     PCL_OPERATOR_SetCharBoldValue           = (char) 0x7d;
  protected static final char     PCL_OPERATOR_SetCharScale               = (char) 0x65;
  protected static final char     PCL_OPERATOR_SetCharShear               = (char) 0x66;
  protected static final char     PCL_OPERATOR_SetCharSubMode             = (char) 0x81;
  protected static final char     PCL_OPERATOR_SetClipIntersect           = (char) 0x67;
  protected static final char     PCL_OPERATOR_SetClipMode                = (char) 0x7f;
  protected static final char     PCL_OPERATOR_SetClipRectangle           = (char) 0x68;
  protected static final char     PCL_OPERATOR_SetClipToPage              = (char) 0x69;
  protected static final char     PCL_OPERATOR_SetColorSpace              = (char) 0x6a;
  protected static final char     PCL_OPERATOR_SetCursor                  = (char) 0x6b;
  protected static final char     PCL_OPERATOR_SetCursorRel               = (char) 0x6c;
  protected static final char     PCL_OPERATOR_SetHalftoneMethod          = (char) 0x6d;
  protected static final char     PCL_OPERATOR_SetFillMode                = (char) 0x6e;
  protected static final char     PCL_OPERATOR_SetFont                    = (char) 0x6f;
  protected static final char     PCL_OPERATOR_SetLineCap                 = (char) 0x71;
  protected static final char     PCL_OPERATOR_SetLineDash                = (char) 0x70;
  protected static final char     PCL_OPERATOR_SetLineJoin                = (char) 0x72;
  protected static final char     PCL_OPERATOR_SetMiterLimit              = (char) 0x73;
  protected static final char     PCL_OPERATOR_SetPageDefaultCTM          = (char) 0x74;
  protected static final char     PCL_OPERATOR_SetPageOrigin              = (char) 0x75;
  protected static final char     PCL_OPERATOR_SetPageRotation            = (char) 0x76;
  protected static final char     PCL_OPERATOR_SetPageScale               = (char) 0x77;
  protected static final char     PCL_OPERATOR_SetPathToClip              = (char) 0x80;
  protected static final char     PCL_OPERATOR_SetPatternTxMode           = (char) 0x78;
  protected static final char     PCL_OPERATOR_SetPenSource               = (char) 0x79;
  protected static final char     PCL_OPERATOR_SetPenWidth                = (char) 0x7a;
  protected static final char     PCL_OPERATOR_SetROP                     = (char) 0x7b;
  protected static final char     PCL_OPERATOR_SetSourceTxMode            = (char) 0x7c;
  protected static final char     PCL_OPERATOR_Text                       = (char) 0xa8;
  protected static final char     PCL_OPERATOR_TextPath                   = (char) 0xa9;
  protected static final char     PCL_DATA_TYPE_LO                        = (char) 0xc0;
  protected static final char     PCL_DATA_TYPE_HI                        = (char) 0xef;
  protected static final char     PCL_DATA_TYPE_real32                    = (char) 0xc5;
  protected static final char     PCL_DATA_TYPE_real32_array              = (char) 0xcd;
  protected static final char     PCL_DATA_TYPE_real32_box                = (char) 0xe5;
  protected static final char     PCL_DATA_TYPE_real32_xy                 = (char) 0xd5;
  protected static final char     PCL_DATA_TYPE_sint16                    = (char) 0xc3;
  protected static final char     PCL_DATA_TYPE_sint16_array              = (char) 0xcb;
  protected static final char     PCL_DATA_TYPE_sint16_box                = (char) 0xe3;
  protected static final char     PCL_DATA_TYPE_sint16_xy                 = (char) 0xd3;
  protected static final char     PCL_DATA_TYPE_sint32                    = (char) 0xc4;
  protected static final char     PCL_DATA_TYPE_sint32_array              = (char) 0xcc;
  protected static final char     PCL_DATA_TYPE_sint32_box                = (char) 0xe4;
  protected static final char     PCL_DATA_TYPE_sint32_xy                 = (char) 0xd4;
  protected static final char     PCL_DATA_TYPE_ubyte                     = (char) 0xc0;
  protected static final char     PCL_DATA_TYPE_ubyte_array               = (char) 0xc8;
  protected static final char     PCL_DATA_TYPE_ubyte_box                 = (char) 0xe0;
  protected static final char     PCL_DATA_TYPE_ubyte_xy                  = (char) 0xd0;
  protected static final char     PCL_DATA_TYPE_uint16                    = (char) 0xc1;
  protected static final char     PCL_DATA_TYPE_uint16_array              = (char) 0xc9;
  protected static final char     PCL_DATA_TYPE_uint16_box                = (char) 0xe1;
  protected static final char     PCL_DATA_TYPE_uint16_xy                 = (char) 0xd1;
  protected static final char     PCL_DATA_TYPE_uint32                    = (char) 0xc2;
  protected static final char     PCL_DATA_TYPE_uint32_array              = (char) 0xca;
  protected static final char     PCL_DATA_TYPE_uint32_box                = (char) 0xe2;
  protected static final char     PCL_DATA_TYPE_uint32_xy                 = (char) 0xd2;
  protected static final char     PCL_ATTRIBUTE_attr_ubyte                = (char) 0xf8;
  protected static final char     PCL_ATTRIBUTE_attr_uint16               = (char) 0xf9;
  protected static final char     PCL_ATTRIBUTE_PaletteDepth              = (char) 2;
  protected static final char     PCL_ATTRIBUTE_ColorSpace                = (char) 3;
  protected static final char     PCL_ATTRIBUTE_NullBrush                 = (char) 4;
  protected static final char     PCL_ATTRIBUTE_NullPen                   = (char) 5;
  protected static final char     PCL_ATTRIBUTE_PaletteData               = (char) 6;
  protected static final char     PCL_ATTRIBUTE_PatternSelectID           = (char) 8;
  protected static final char     PCL_ATTRIBUTE_GrayLevel                 = (char) 9;
  protected static final char     PCL_ATTRIBUTE_RGBColor                  = (char) 11;
  protected static final char     PCL_ATTRIBUTE_PatternOrigin             = (char) 12;
  protected static final char     PCL_ATTRIBUTE_NewDestinationSize        = (char) 13;
  protected static final char     PCL_ATTRIBUTE_PrimaryArray              = (char) 14;
  protected static final char     PCL_ATTRIBUTE_PrimaryDepth              = (char) 15;
  protected static final char     PCL_ATTRIBUTE_DeviceMatrix              = (char) 33;
  protected static final char     PCL_ATTRIBUTE_DitherMatrixDataType      = (char) 34;
  protected static final char     PCL_ATTRIBUTE_DitherOrigin              = (char) 35;
  protected static final char     PCL_ATTRIBUTE_MediaSize                 = (char) 37;
  protected static final char     PCL_ATTRIBUTE_MediaSource               = (char) 38;
  protected static final char     PCL_ATTRIBUTE_MediaType                 = (char) 39;
  protected static final char     PCL_ATTRIBUTE_Orientation               = (char) 40;
  protected static final char     PCL_ATTRIBUTE_PageAngle                 = (char) 41;
  protected static final char     PCL_ATTRIBUTE_PageOrigin                = (char) 42;
  protected static final char     PCL_ATTRIBUTE_PageScale                 = (char) 43;
  protected static final char     PCL_ATTRIBUTE_ROP3                      = (char) 44;
  protected static final char     PCL_ATTRIBUTE_TxMode                    = (char) 45;
  protected static final char     PCL_ATTRIBUTE_CustomMediaSize           = (char) 47;
  protected static final char     PCL_ATTRIBUTE_CustomMediaSizeUnits      = (char) 48;
  protected static final char     PCL_ATTRIBUTE_PageCopies                = (char) 49;
  protected static final char     PCL_ATTRIBUTE_DitherMatrixSize          = (char) 50;
  protected static final char     PCL_ATTRIBUTE_DitherMatrixDepth         = (char) 51;
  protected static final char     PCL_ATTRIBUTE_SimplexPageMode           = (char) 52;
  protected static final char     PCL_ATTRIBUTE_DuplexPageMode            = (char) 53;
  protected static final char     PCL_ATTRIBUTE_DuplexPageSide            = (char) 54;
  protected static final char     PCL_ATTRIBUTE_ArcDirection              = (char) 65;
  protected static final char     PCL_ATTRIBUTE_BoundingBox               = (char) 66;
  protected static final char     PCL_ATTRIBUTE_DashOffset                = (char) 67;
  protected static final char     PCL_ATTRIBUTE_EllipseDimension          = (char) 68;
  protected static final char     PCL_ATTRIBUTE_EndPoint                  = (char) 69;
  protected static final char     PCL_ATTRIBUTE_FillMode                  = (char) 70;
  protected static final char     PCL_ATTRIBUTE_LineCapStyle              = (char) 71;
  protected static final char     PCL_ATTRIBUTE_LineJoinStyle             = (char) 72;
  protected static final char     PCL_ATTRIBUTE_MiterLength               = (char) 73;
  protected static final char     PCL_ATTRIBUTE_LineDashStyle             = (char) 74;
  protected static final char     PCL_ATTRIBUTE_PenWidth                  = (char) 75;
  protected static final char     PCL_ATTRIBUTE_Point                     = (char) 76;
  protected static final char     PCL_ATTRIBUTE_NumberOfPoints            = (char) 77;
  protected static final char     PCL_ATTRIBUTE_SolidLine                 = (char) 78;
  protected static final char     PCL_ATTRIBUTE_StartPoint                = (char) 79;
  protected static final char     PCL_ATTRIBUTE_PointType                 = (char) 80;
  protected static final char     PCL_ATTRIBUTE_ControlPoint1             = (char) 81;
  protected static final char     PCL_ATTRIBUTE_ControlPoint2             = (char) 82;
  protected static final char     PCL_ATTRIBUTE_ClipRegion                = (char) 83;
  protected static final char     PCL_ATTRIBUTE_ClipMode                  = (char) 84;
  protected static final char     PCL_ATTRIBUTE_ColorDepth                = (char) 98;
  protected static final char     PCL_ATTRIBUTE_BlockHeight               = (char) 99;
  protected static final char     PCL_ATTRIBUTE_ColorMapping              = (char) 100;
  protected static final char     PCL_ATTRIBUTE_CompressMode              = (char) 101;
  protected static final char     PCL_ATTRIBUTE_DestinationBox            = (char) 102;
  protected static final char     PCL_ATTRIBUTE_DestinationSize           = (char) 103;
  protected static final char     PCL_ATTRIBUTE_PatternPersistence        = (char) 104;
  protected static final char     PCL_ATTRIBUTE_PatternDefineID           = (char) 105;
  protected static final char     PCL_ATTRIBUTE_SourceHeight              = (char) 107;
  protected static final char     PCL_ATTRIBUTE_SourceWidth               = (char) 108;
  protected static final char     PCL_ATTRIBUTE_StartLine                 = (char) 109;
  protected static final char     PCL_ATTRIBUTE_PadBytesMultiple          = (char) 110;
  protected static final char     PCL_ATTRIBUTE_BlockByteLength           = (char) 111;
  protected static final char     PCL_ATTRIBUTE_NumberOfScanLines         = (char) 115;
  protected static final char     PCL_ATTRIBUTE_CommentData               = (char) 129;
  protected static final char     PCL_ATTRIBUTE_DataOrg                   = (char) 130;
  protected static final char     PCL_ATTRIBUTE_Measure                   = (char) 134;
  protected static final char     PCL_ATTRIBUTE_SourceType                = (char) 136;
  protected static final char     PCL_ATTRIBUTE_UnitsPerMeasure           = (char) 137;
  protected static final char     PCL_ATTRIBUTE_StreamName                = (char) 139;
  protected static final char     PCL_ATTRIBUTE_StreamDataLength          = (char) 140;
  protected static final char     PCL_ATTRIBUTE_ErrorReport               = (char) 143;
  protected static final char     PCL_ATTRIBUTE_CharAngle                 = (char) 161;
  protected static final char     PCL_ATTRIBUTE_CharCode                  = (char) 162;
  protected static final char     PCL_ATTRIBUTE_CharDataSize              = (char) 163;
  protected static final char     PCL_ATTRIBUTE_CharScale                 = (char) 164;
  protected static final char     PCL_ATTRIBUTE_CharShear                 = (char) 165;
  protected static final char     PCL_ATTRIBUTE_CharSize                  = (char) 166;
  protected static final char     PCL_ATTRIBUTE_FontHeaderLength          = (char) 167;
  protected static final char     PCL_ATTRIBUTE_FontName                  = (char) 168;
  protected static final char     PCL_ATTRIBUTE_FontFormat                = (char) 169;
  protected static final char     PCL_ATTRIBUTE_SymbolSet                 = (char) 170;
  protected static final char     PCL_ATTRIBUTE_TextData                  = (char) 171;
  protected static final char     PCL_ATTRIBUTE_CharSubModeArray          = (char) 172;
  protected static final char     PCL_ATTRIBUTE_XSpacingData              = (char) 175;
  protected static final char     PCL_ATTRIBUTE_YSpacingData              = (char) 176;
  protected static final char     PCL_ATTRIBUTE_CharBoldValue             = (char) 177;
  protected static final char     PCL_ATTRIBUTE_MediaDestination          = (char) 0;                                                                          // the documentation did not specify the value
  protected static final char     PCL_EMBEDDED_DATA                       = (char) 0xfa;
  protected static final char     PCL_EMBEDDED_DATA_BYTE                  = (char) 0xfb;

  // attribute enumerations

  protected static final byte     ArcDirection_eClockWise                 = 0;
  protected static final byte     ArcDirection_eCounterClockWise          = 1;
  protected static final byte     CharSubModeArray_eNoSubstitution        = 0;
  protected static final byte     CharSubModeArray_eVerticalSubstitution  = 1;
  protected static final byte     ClipRegion_eInterior                    = 0;
  protected static final byte     ClipRegion_eExterior                    = 1;
  protected static final byte     ColorDepth_e1Bit                        = 0;
  protected static final byte     ColorDepth_e4Bit                        = 1;
  protected static final byte     ColorDepth_e8Bit                        = 2;
  protected static final byte     ColorMapping_eDirectPixel               = 0;
  protected static final byte     ColorMapping_eIndexedPixel              = 1;
  protected static final byte     ColorSpace_eGray                        = 1;
  protected static final byte     ColorSpace_eRGB                         = 2;
  protected static final byte     ColorSpace_eSRGB                        = 6;
  protected static final byte     CompressMode_eNoCompression             = 0;
  protected static final byte     CompressMode_eRLECompression            = 1;
  protected static final byte     CompressMode_eJPEGCompression           = 2;
  protected static final byte     DataOrg_eBinaryHighByteFirst            = 0;
  protected static final byte     DataOrg_eBinaryLowByteFirst             = 1;
  protected static final byte     DataSource_eDefault                     = 0;
  protected static final byte     DataSource_eUByte                       = 0;
  protected static final byte     DataSource_eSByte                       = 1;
  protected static final byte     DataSource_eUint16                      = 2;
  protected static final byte     DataSource_eSint16                      = 3;
  protected static final byte     DitherMatrix_eDeviceBest                = 0;
  protected static final byte     DuplexPageMode_eDuplexHorizontalBinding = 0;
  protected static final byte     DuplexPageMode_eDuplexVerticalBinding   = 1;
  protected static final byte     DuplexPageSide_eFrontMediaSide          = 0;
  protected static final byte     DuplexPageSide_eBackMediaSide           = 1;
  protected static final byte     ErrorReport_eNoReporting                = 0;
  protected static final byte     ErrorReport_eBackChannel                = 1;
  protected static final byte     ErrorReport_eErrorPage                  = 2;
  protected static final byte     ErrorReport_eBackChAndErrPage           = 3;
  protected static final byte     ErrorReport_eNWBackChannel              = 4;
  protected static final byte     ErrorReport_eNWErrorPage                = 5;
  protected static final byte     ErrorReport_eNWBackChAndErrPage         = 6;
  protected static final byte     FillMode_eNonZeroWinding                = 0;
  protected static final byte     FillMode_eEvenOdd                       = 1;
  protected static final byte     LineCap_eButtCap                        = 0;
  protected static final byte     LineCap_eRoundCap                       = 1;
  protected static final byte     LineCap_eSquareCap                      = 2;
  protected static final byte     LineCap_eTriangleCap                    = 3;
  protected static final byte     LineJoin_eMiterJoin                     = 0;
  protected static final byte     LineJoin_eRoundJoin                     = 1;
  protected static final byte     LineJoin_eBevelJoin                     = 2;
  protected static final byte     LineJoin_eNoJoin                        = 3;
  protected static final byte     Measure_eInch                           = 0;
  protected static final byte     Measure_eMillimeter                     = 1;
  protected static final byte     Measure_eTenthsOfAMillimeter            = 2;
  protected static final byte     MediaSize_eLetterPaper                  = 0;
  protected static final byte     MediaSize_eLegalPaper                   = 1;
  protected static final byte     MediaSize_eA4Paper                      = 2;
  protected static final byte     MediaSize_eExecPaper                    = 3;
  protected static final byte     MediaSize_eLedgerPaper                  = 4;
  protected static final byte     MediaSize_eA3Paper                      = 5;
  protected static final byte     MediaSize_eCOM10Envelope                = 6;
  protected static final byte     MediaSize_eMonarchEnvelope              = 7;
  protected static final byte     MediaSize_eC5Envelope                   = 8;
  protected static final byte     MediaSize_eDLEnvelope                   = 9;
  protected static final byte     MediaSize_eJB4Paper                     = 10;
  protected static final byte     MediaSize_eJB5Paper                     = 11;
  protected static final byte     MediaSize_eB5Envelope                   = 12;
  protected static final byte     MediaSize_eJPostcard                    = 14;
  protected static final byte     MediaSize_eJDoublePostcard              = 15;
  protected static final byte     MediaSize_eA5Paper                      = 16;
  protected static final byte     MediaSize_eA6Paper                      = 17;
  protected static final byte     MediaSize_eJB6Paper                     = 18;
  protected static final byte     MediaSource_eDefaultSource              = 0;
  protected static final byte     MediaSource_eAutoSelect                 = 1;
  protected static final byte     MediaSource_eManualFeed                 = 2;
  protected static final byte     MediaSource_eMultiPurposeTray           = 3;
  protected static final byte     MediaSource_eUpperCassette              = 4;
  protected static final byte     MediaSource_eLowerCassette              = 5;
  protected static final byte     MediaSource_eEnvelopeTray               = 6;
  protected static final byte     MediaSource_eThirdCassette              = 7;
  protected static final byte     MediaDestination_eDefaultDestination    = 0;
  protected static final byte     MediaDestination_eFaceDownBin           = 1;
  protected static final byte     MediaDestination_eFaceUpBin             = 2;
  protected static final byte     MediaDestination_eJobOffsetBin          = 3;
  protected static final byte     Orientation_ePortraitOrientation        = 0;
  protected static final byte     Orientation_eLandscapeOrientation       = 1;
  protected static final byte     Orientation_eReversePortrait            = 2;
  protected static final byte     Orientation_eReverseLandscape           = 3;
  protected static final byte     PatternPersistence_eTempPattern         = 0;
  protected static final byte     PatternPersistence_ePagePattern         = 1;
  protected static final byte     PatternPersistence_eSessionPattern      = 2;
  protected static final byte     SimplexPageMode_eSimplexFrontSide       = 0;
  protected static final byte     TxMode_eOpaque                          = 0;
  protected static final byte     TxMode_eTransparent                     = 1;
  protected static final byte     WritingMode_eHorizontal                 = 0;
  protected static final byte     WritingMode_eVertical                   = 1;

  /**
   * Constructor
   * @param printJob
   */
  public PCL6Binary_Parser(PrintJob printJob)
  {
    super(printJob);
  }

  @Override
  protected boolean ComputeOffsets()
    throws IOException
  {
    if (super.ComputeOffsets() == false)
    {
      return false;
    }

    // pcl6's stream consists of a stream header and a stream body
    _streamHeader = _streamStart;

    // the stream header ends with a LINE_FEED. the stream body begins immediately thereafter
    // the first part of the stream body is the begin session command.
    _streamBody = _printJob.FindPattern(_streamHeader, _endUEL, LINE_FEED);
    if (_streamBody == PrintJob.NOT_FOUND)
    {
      System.out.println("LINE_FEED after _streamHeader not found");
      return false;
    }
    // skip past the LINE_FEED
    _streamBody++;

    return true;
  }

  @Override
  protected boolean ICanParse()
    throws IOException
  {
    if (ComputeOffsets() == true)
    {
      // the pcl6 stream header begins with a binding format identifier: ', ), or (
      char firstStreamHeaderByte = _printJob.GetCharAt(_streamHeader);
      if ( //(firstStreamHeaderByte == PCL_BINDING_ASCII) ||             // we don't handle ascii
      (firstStreamHeaderByte == PCL_BINDING_HIGH_BYTE_FIRST) || (firstStreamHeaderByte == PCL_BINDING_LOW_BYTE_FIRST))
      {
        _bindingFormat = firstStreamHeaderByte;
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean ParseTheJob()
    throws IOException
  {
    if (ICanParse() == false)
    {
      return false;
    }

    ProcessJobHeader();
    _language = "PCL6";

    _printJob.SetCurrentOffset(_streamBody);
    while (_printJob.GetCurrentOffset() < (_endUEL - 1))
    {
      if (ProcessCommand() == false)
      {
        return false;
      }
    }

    AssignPropertiesToPrintJob();

    return true;
  }

  /**
   * the format of a command is:
   *
   *   [<attribute list>]<operator>[<embedded data><data length><data>]
   *
   * the format of <attribute list> is one or more of:
   *
   *   <data type><data><attribute id>
   *
   * [ & ] mean the values are optional
   *
   */
  protected boolean ProcessCommand()
    throws IOException
  {
    DataType data = null;
    DataType attribute = null;

    // first, we get all the data (aka parameters) associated with the command (if any)
    char peekChar = _printJob.PeekNextAsChar();
    while ((peekChar < PCL_OPERATOR_LO) || (peekChar > PCL_OPERATOR_HI))
    {
      //if (_printJob.GetCurrentOffset() > 101100)
      //{
      // System.out.println("at offset " + _printJob.GetCurrentOffset());
      //}
      // if it is a data type, get the data
      if ((peekChar >= PCL_DATA_TYPE_LO) && (peekChar <= PCL_DATA_TYPE_HI))
      {
        if (GlobalAttributes.__verbose == true)
        {
          String methodName = "PCL_" + ConvertToMethodString((byte) peekChar);
          DebugWindow.DisplayText("data " + methodName + ", offset = " + _printJob.GetCurrentOffset());
        }
        data = GetData();
      }
      else
        // if it is an attribute type, get the attribute
        if ((peekChar == PCL_ATTRIBUTE_attr_ubyte) || (peekChar == PCL_ATTRIBUTE_attr_uint16))
        {
          if (GlobalAttributes.__verbose == true)
          {
            String methodName = "PCL_" + ConvertToMethodString((byte) peekChar);
            DebugWindow.DisplayText("attribute " + methodName + ", offset = " + _printJob.GetCurrentOffset());
          }
          attribute = GetData();
          // we now have a parm name (attribute) and it's data, so put this parm pair into the parm hash map
          ProcessCommandParms(attribute, data);
        }
        else
          // look to see if we hit an earlier UEL
          if (peekChar == ESCAPE_CHAR)
          {
            int offset = _printJob.FindPattern(_printJob.GetCurrentOffset(), _printJob.GetCurrentOffset() + PJL_UEL.length(), PJL_UEL);
            if (offset == _printJob.GetCurrentOffset())
            {
              // we're done earlier than we thought
              _endUEL = _printJob.GetCurrentOffset();
              return true;
            }
            // oops. we weren't expecting this...
            new Exception("Unexpected value = " + (byte) peekChar + " at offset " + _printJob.GetCurrentOffset()).printStackTrace();
            return false;
          }
          else
          // oops. we weren't expecting this...
          {
            new Exception("Unexpected value = " + (byte) peekChar + " at offset " + _printJob.GetCurrentOffset()).printStackTrace();
            return false;
          }
      peekChar = _printJob.PeekNextAsChar();
    }

    // second, we are on the command and we have all the command's parameters,
    // so now we process the command

    // the method name is PCL_OPERATOR_0xXX where XX is the two-char hex value of the data type
    String methodName = "PCL_" + ConvertToMethodString(_printJob.GetNext());
    DebugWindow.DisplayText("operator " + methodName + ", offset = " + (_printJob.GetCurrentOffset() - 1));
    try
    {
      Method pclMethod = pclMethods.get(methodName);
      if (pclMethod != null)
      {
        pclMethod.invoke(this, EMPTY_OBJECT_ARRAY);
      }
    }
    catch (Exception excp)
    {
      // do nothing
    }

    @SuppressWarnings("unused")
    DataType embeddedData = null;
    // see if this command is followed by an embedded data attribute
    peekChar = _printJob.PeekNextAsChar();
    // if it is an embedded data type, get the embedded data
    if ((peekChar == PCL_EMBEDDED_DATA) || (peekChar == PCL_EMBEDDED_DATA_BYTE))
    {
      if (GlobalAttributes.__verbose == true)
      {
        methodName = "PCL_" + ConvertToMethodString((byte) peekChar);
        DebugWindow.DisplayText("embeddedData " + methodName + ", offset = " + _printJob.GetCurrentOffset());
      }
      embeddedData = GetData();
    }
    DebugWindow.DisplayText("");
    return true;
  }

  protected void ProcessCommandParms(DataType attribute, DataType data)
  {
    switch (attribute.GetNumber().intValue())
    {
      case PCL_ATTRIBUTE_Orientation:
        switch (data.GetNumber().intValue())
        {
          case Orientation_ePortraitOrientation:
            _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_PORTRAIT;
            break;
          case Orientation_eLandscapeOrientation:
            _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_LANDSCAPE;
            break;
          case Orientation_eReversePortrait:
            _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_REVERSE_PORTRAIT;
            break;
          case Orientation_eReverseLandscape:
            _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_REVERSE_LANDSCAPE;
            break;
          default:
            _orientation = PrintJobPropertiesRow.VALUE_UNKNOWN;
            break;
        }
        break;

      case PCL_ATTRIBUTE_MediaSize:
        Number number = data.GetNumber();
        if (number != null)
        {
          switch (data.GetNumber().intValue())
          {
            case MediaSize_eLetterPaper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LETTER;
              break;
            case MediaSize_eLegalPaper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LEGAL;
              break;
            case MediaSize_eA4Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A4;
              break;
            case MediaSize_eExecPaper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_EXECUTIVE;
              break;
            case MediaSize_eLedgerPaper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LEDGER;
              break;
            case MediaSize_eA3Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A3;
              break;
            case MediaSize_eCOM10Envelope:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_COM_10;
              break;
            case MediaSize_eMonarchEnvelope:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_MONARCH;
              break;
            case MediaSize_eC5Envelope:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_C5;
              break;
            case MediaSize_eDLEnvelope:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_DL;
              break;
            case MediaSize_eJB4Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_JB4;
              break;
            case MediaSize_eJB5Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_JB5;
              break;
            case MediaSize_eB5Envelope:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_B5;
              break;
            case MediaSize_eJPostcard:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_J;
              break;
            case MediaSize_eJDoublePostcard:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_JJ;
              break;
            case MediaSize_eA5Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A5;
              break;
            case MediaSize_eA6Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A6;
              break;
            case MediaSize_eJB6Paper:
              _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_JB6;
              break;
            default:
              _paperSize = PrintJobPropertiesRow.VALUE_UNKNOWN;
              break;
          }
        }
        else
        {
          UByte_array byteArray = (UByte_array) data;
          DataType[] numbers = byteArray.GetNumbers();
          _paperSize = "";
          for (int i = 0; i < numbers.length; i++)
          {
            _paperSize += (char) (numbers[i].GetNumber().intValue());
          }
        }
        break;

      case PCL_ATTRIBUTE_MediaType:
        UByte_array byteArray = (UByte_array) data;
        DataType[] numbers = byteArray.GetNumbers();
        _paperType = "";
        for (int i = 0; i < numbers.length; i++)
        {
          _paperType += (char) (numbers[i].GetNumber().intValue());
        }
        break;

      case PCL_ATTRIBUTE_MediaSource:
        switch (data.GetNumber().intValue())
        {
          case MediaSource_eDefaultSource:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_CURRENT;
            break;
          case MediaSource_eAutoSelect:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_PRINTERSPECIFIC;
            break;
          case MediaSource_eManualFeed:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_MANUALPAPER;
            break;
          case MediaSource_eMultiPurposeTray:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_MUTLIPURPOSETRAY;
            break;
          case MediaSource_eUpperCassette:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_UPPERTRAY;
            break;
          case MediaSource_eLowerCassette:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_LOWERTRAY;
            break;
          case MediaSource_eEnvelopeTray:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_OPTIONALENVELOPESOURCE;
            break;
          case MediaSource_eThirdCassette:
            _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_THIRDTRAY;
            break;
          default:
            _paperSource = PrintJobPropertiesRow.VALUE_UNKNOWN;
            break;
        }
        break;
      /*
            case PCL_ATTRIBUTE_MediaDestination:
                   switch (data.GetNumber().intValue())
                   {
                     case MediaDestination_eDefaultDestination:
                            _outbin = PrintJobPropertiesRow.VALUE_DESTINATION_DEFAULTBIN;
                            break;
                     case MediaDestination_eFaceDownBin:
                            _outbin = PrintJobPropertiesRow.VALUE_DESTINATION_FACEDOWNBIN;
                            break;
                     case MediaDestination_eFaceUpBin:
                            _outbin = PrintJobPropertiesRow.VALUE_DESTINATION_FACEUPBIN;
                            break;
                     case MediaDestination_eJobOffsetBin:
                            _outbin = PrintJobPropertiesRow.VALUE_DESTINATION_JOBOFFSETBIN;
                            break;
                     default:
                            _outbin = PrintJobPropertiesRow.VALUE_UNKNOWN;
                            break;
                   }
                   break;
      */
      case PCL_ATTRIBUTE_SimplexPageMode:
        switch (data.GetNumber().intValue())
        {
          case SimplexPageMode_eSimplexFrontSide:
            _sides = PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX;
            break;
          default:
            _sides = PrintJobPropertiesRow.VALUE_UNKNOWN;
            break;
        }
        break;

      case PCL_ATTRIBUTE_DuplexPageMode:
        switch (data.GetNumber().intValue())
        {
          case DuplexPageMode_eDuplexHorizontalBinding:
            _sides = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
            break;
          case DuplexPageMode_eDuplexVerticalBinding:
            _sides = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
            break;
          default:
            _sides = PrintJobPropertiesRow.VALUE_UNKNOWN;
            break;
        }
        break;

      case PCL_ATTRIBUTE_PageCopies:
        _copies = String.valueOf(data.GetNumber().intValue());
        break;

      case PCL_ATTRIBUTE_ColorSpace:
        switch (data.GetNumber().intValue())
        {
          case ColorSpace_eGray:
            _color = PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME;
            break;
          case ColorSpace_eRGB:
            _color = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
            break;
          case ColorSpace_eSRGB:
            _color = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
            break;
          default:
            _color = PrintJobPropertiesRow.VALUE_UNKNOWN;
            break;
        }
        break;
    }
  }

  protected DataType GetData()
    throws IOException
  {
    DataType theData = null;

    // the method name is PCL_XX where XX is the two-char hex value of the data type
    String methodName = "PCL_" + ConvertToMethodString(_printJob.GetNext());

    try
    {
      Method pclMethod = pclMethods.get(methodName);
      if (pclMethod != null)
      {
        theData = (DataType) pclMethod.invoke(this, EMPTY_OBJECT_ARRAY);
      }
    }
    catch (Exception excp)
    {
      // do nothing
    }
    return theData;
  }

  protected String ConvertToMethodString(byte aByte)
  {
    String methodString = "0x" + GetHexString(aByte);
    return methodString;
  }

  protected String GetHexString(byte aByte)
  {
    String hexString = hexCharacters[GetLeftNibble(aByte)] + hexCharacters[GetRightNibble(aByte)];
    return hexString;
  }

  protected byte GetLeftNibble(byte aByte)
  {
    byte leftNibble = (byte) ((aByte >> 4) & 0x0F);
    return leftNibble;
  }

  protected byte GetRightNibble(byte aByte)
  {
    byte rightNibble = (byte) (aByte & 0x0F);
    return rightNibble;
  }

  // PCL_OPERATOR_EndPage
  protected void PCL_0x44()
  {
    _numberOfPages++;
  }

  /*
    // PCL_OPERATOR_ArcPath
    protected void PCL_0x91()
    {

    }

    // PCL_OPERATOR_BeginChar
    protected void PCL_0x52()
    {

    }

    // PCL_OPERATOR_BeginFontHeader
    protected void PCL_0x4f()
    {

    }

    // PCL_OPERATOR_BeginImage
    protected void PCL_0xb0()
    {

    }

    // PCL_OPERATOR_BeginPage
    protected void PCL_0x43()
    {

    }

    // PCL_OPERATOR_BeginRastPattern
    protected void PCL_0xb3()
    {

    }

    // PCL_OPERATOR_BeginScan
    protected void PCL_0xb6()
    {

    }

    // PCL_OPERATOR_BeginSession
    protected void PCL_0x41()
    {

    }

    // PCL_OPERATOR_BeginStream
    protected void PCL_0x5b()
    {

    }

    // PCL_OPERATOR_BezierPath
    protected void PCL_0x93()
    {

    }

    // PCL_OPERATOR_BezierRelPath
    protected void PCL_0x95()
    {

    }

    // PCL_OPERATOR_Chord
    protected void PCL_0x96()
    {

    }

    // PCL_OPERATOR_ChordPath
    protected void PCL_0x97()
    {

    }

    // PCL_OPERATOR_CloseDataSource
    protected void PCL_0x49()
    {

    }

    // PCL_OPERATOR_CloseSubPath
    protected void PCL_0x84()
    {

    }

    // PCL_OPERATOR_Comment
    protected void PCL_0x47()
    {

    }

    // PCL_OPERATOR_Ellipse
    protected void PCL_0x98()
    {

    }

    // PCL_OPERATOR_EllipsePath
    protected void PCL_0x99()
    {

    }

    // PCL_OPERATOR_EndChar
    protected void PCL_0x54()
    {

    }

    // PCL_OPERATOR_EndFontHeader
    protected void PCL_0x51()
    {

    }

    // PCL_OPERATOR_EndImage
    protected void PCL_0xb2()
    {

    }

    // PCL_OPERATOR_EndRastPattern
    protected void PCL_0xb5()
    {

    }

    // PCL_OPERATOR_EndScan
    protected void PCL_0xb8()
    {

    }

    // PCL_OPERATOR_EndSession
    protected void PCL_0x42()
    {

    }

    // PCL_OPERATOR_EndStream
    protected void PCL_0x5d()
    {

    }

    // PCL_OPERATOR_ExecStream
    protected void PCL_0x5e()
    {

    }

    // PCL_OPERATOR_LinePath
    protected void PCL_0x9b()
    {

    }

    // PCL_OPERATOR_LineRelPath
    protected void PCL_0x9d()
    {

    }

    // PCL_OPERATOR_NewPath
    protected void PCL_0x85()
    {

    }

    // PCL_OPERATOR_OpenDataSource
    protected void PCL_0x48()
    {

    }

    // PCL_OPERATOR_PaintPath
    protected void PCL_0x86()
    {

    }

    // PCL_OPERATOR_Pie
    protected void PCL_0x9e()
    {

    }

    // PCL_OPERATOR_PiePath
    protected void PCL_0x9f()
    {

    }

    // PCL_OPERATOR_PopGS
    protected void PCL_0x60()
    {

    }

    // PCL_OPERATOR_PushGS
    protected void PCL_0x61()
    {

    }

    // PCL_OPERATOR_ReadChar
    protected void PCL_0x53()
    {

    }

    // PCL_OPERATOR_ReadFontHeader
    protected void PCL_0x50()
    {

    }

    // PCL_OPERATOR_ReadImage
    protected void PCL_0xb1()
    {

    }

    // PCL_OPERATOR_ReadRastPattern
    protected void PCL_0xb4()
    {

    }

    // PCL_OPERATOR_ReadStream
    protected void PCL_0x5c()
    {

    }

    // PCL_OPERATOR_Rectangle
    protected void PCL_0xa0()
    {

    }

    // PCL_OPERATOR_RectanglePath
    protected void PCL_0xa1()
    {

    }

    // PCL_OPERATOR_RemoveFont
    protected void PCL_0x55()
    {

    }

    // PCL_OPERATOR_SetCharAttributes
    protected void PCL_0x56()
    {

    }

    // PCL_OPERATOR_RemoveStream
    protected void PCL_0x5f()
    {

    }

    // PCL_OPERATOR_RoundRectangle
    protected void PCL_0xa2()
    {

    }

    // PCL_OPERATOR_RoundRectanglePath
    protected void PCL_0xa3()
    {

    }

    // PCL_OPERATOR_ScanLineRel
    protected void PCL_0xb9()
    {

    }

    // PCL_OPERATOR_SetClipReplace
    protected void PCL_0x62()
    {

    }

    // PCL_OPERATOR_SetBrushSource
    protected void PCL_0x63()
    {

    }

    // PCL_OPERATOR_SetCharAngle
    protected void PCL_0x64()
    {

    }

    // PCL_OPERATOR_SetCharBoldValue
    protected void PCL_0x7d()
    {

    }

    // PCL_OPERATOR_SetCharScale
    protected void PCL_0x65()
    {

    }

    // PCL_OPERATOR_SetCharShear
    protected void PCL_0x66()
    {

    }

    // PCL_OPERATOR_SetCharSubMode
    protected void PCL_0x81()
    {

    }

    // PCL_OPERATOR_SetClipIntersect
    protected void PCL_0x67()
    {

    }

    // PCL_OPERATOR_SetClipMode
    protected void PCL_0x7f()
    {

    }

    // PCL_OPERATOR_SetClipRectangle
    protected void PCL_0x68()
    {

    }

    // PCL_OPERATOR_SetClipToPage
    protected void PCL_0x69()
    {

    }

    // PCL_OPERATOR_SetColorSpace
    protected void PCL_0x6a()
    {

    }

    // PCL_OPERATOR_SetCursor
    protected void PCL_0x6b()
    {

    }

    // PCL_OPERATOR_SetCursorRel
    protected void PCL_0x6c()
    {

    }

    // PCL_OPERATOR_SetHalftoneMethod
    protected void PCL_0x6d()
    {

    }

    // PCL_OPERATOR_SetFillMode
    protected void PCL_0x6e()
    {

    }

    // PCL_OPERATOR_SetFont
    protected void PCL_0x6f()
    {

    }

    // PCL_OPERATOR_SetLineCap
    protected void PCL_0x71()
    {

    }

    // PCL_OPERATOR_SetLineDash
    protected void PCL_0x70()
    {

    }

    // PCL_OPERATOR_SetLineJoin
    protected void PCL_0x72()
    {

    }

    // PCL_OPERATOR_SetMiterLimit
    protected void PCL_0x73()
    {

    }

    // PCL_OPERATOR_SetPageDefaultCTM
    protected void PCL_0x74()
    {

    }

    // PCL_OPERATOR_SetPageOrigin
    protected void PCL_0x75()
    {

    }

    // PCL_OPERATOR_SetPageRotation
    protected void PCL_0x76()
    {

    }

    // PCL_OPERATOR_SetPageScale
    protected void PCL_0x77()
    {

    }

    // PCL_OPERATOR_SetPathToClip
    protected void PCL_0x80()
    {

    }

    // PCL_OPERATOR_SetPatternTxMode
    protected void PCL_0x78()
    {

    }

    // PCL_OPERATOR_SetPenSource
    protected void PCL_0x79()
    {

    }

    // PCL_OPERATOR_SetPenWidth
    protected void PCL_0x7a()
    {

    }

    // PCL_OPERATOR_SetROP
    protected void PCL_0x7b()
    {

    }

    // PCL_OPERATOR_SetSourceTxMode
    protected void PCL_0x7c()
    {

    }

    // PCL_OPERATOR_Text
    protected void PCL_0xa8()
    {

    }

    // PCL_OPERATOR_TextPath
    protected void PCL_0xa9()
    {

    }
  */

  // PCL_DATA_TYPE_attr_ubyte
  protected DataType PCL_0xf8()
    throws IOException
  {
    UByte ubyte = new UByte();
    ubyte.SetNumber();
    return ubyte;
  }

  // PCL_DATA_TYPE_attr_uint16
  protected DataType PCL_0xf9()
    throws IOException
  {
    UInt16 uint16 = new UInt16();
    uint16.SetNumber();
    return uint16;
  }

  // PCL_DATA_TYPE_embedded_data (32 bits)
  protected ArrayDataType PCL_0xfa()
    throws IOException
  {
    // the first 4 bytes tell me how many bytes of data follow
    UInt32 uInt = new UInt32();
    uInt.SetNumber();

    // optimization: just skip over everything
    // UByte_array uByteArray = new UByte_array(uInt.GetNumber().intValue());
    // uByteArray.SetNumber();
    UByte_array uByteArray = new UByte_array(0);
    _printJob.MoveCurrentOffset(uInt.GetNumber().intValue());

    return uByteArray;
  }

  // PCL_DATA_TYPE_embedded_data_byte (8 bits)
  protected ArrayDataType PCL_0xfb()
    throws IOException
  {
    // the first byte tells me how many bytes (1-255) of data follow
    UByte uByte = new UByte();
    uByte.SetNumber();

    // optimization: just skip over everything
    // UByte_array uByteArray = new UByte_array(uByte.GetNumber().intValue());
    // uByteArray.SetNumber();
    UByte_array uByteArray = new UByte_array(0);
    _printJob.MoveCurrentOffset(uByte.GetNumber().intValue());

    return uByteArray;
  }

  // PCL_DATA_TYPE_real32
  protected DataType PCL_0xc5()
    throws IOException
  {
    Real32 real32 = new Real32();
    real32.SetNumber();
    return real32;
  }

  // PCL_DATA_TYPE_real32_array
  protected ArrayDataType PCL_0xcd()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    Real32_array real32 = new Real32_array(arrayCount);
    real32.SetNumber();
    return real32;
  }

  // PCL_DATA_TYPE_real32_box
  protected ArrayDataType PCL_0xe5()
    throws IOException
  {
    Real32_box real32 = new Real32_box();
    real32.SetNumber();
    return real32;
  }

  // PCL_DATA_TYPE_real32_xy
  protected ArrayDataType PCL_0xd5()
    throws IOException
  {
    Real32_xy real32 = new Real32_xy();
    real32.SetNumber();
    return real32;
  }

  // PCL_DATA_TYPE_sint16
  protected DataType PCL_0xc3()
    throws IOException
  {
    SInt16 sint16 = new SInt16();
    sint16.SetNumber();
    return sint16;
  }

  // PCL_DATA_TYPE_sint16_array
  protected ArrayDataType PCL_0xcb()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    SInt16_array sint16 = new SInt16_array(arrayCount);
    sint16.SetNumber();
    return sint16;
  }

  // PCL_DATA_TYPE_sint16_box
  protected ArrayDataType PCL_0xe3()
    throws IOException
  {
    SInt16_box sint16 = new SInt16_box();
    sint16.SetNumber();
    return sint16;
  }

  // PCL_DATA_TYPE_sint16_xy
  protected ArrayDataType PCL_0xd3()
    throws IOException
  {
    SInt16_xy sint16 = new SInt16_xy();
    sint16.SetNumber();
    return sint16;
  }

  // PCL_DATA_TYPE_sint32
  protected DataType PCL_0xc4()
    throws IOException
  {
    SInt32 sint32 = new SInt32();
    sint32.SetNumber();
    return sint32;
  }

  // PCL_DATA_TYPE_sint32_array
  protected ArrayDataType PCL_0xcc()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    SInt32_array sint32 = new SInt32_array(arrayCount);
    sint32.SetNumber();
    return sint32;
  }

  // PCL_DATA_TYPE_sint32_box
  protected ArrayDataType PCL_0xe4()
    throws IOException
  {
    SInt32_box sint32 = new SInt32_box();
    sint32.SetNumber();
    return sint32;
  }

  // PCL_DATA_TYPE_sint32_xy
  protected ArrayDataType PCL_0xd4()
    throws IOException
  {
    SInt32_xy sint32 = new SInt32_xy();
    sint32.SetNumber();
    return sint32;
  }

  // PCL_DATA_TYPE_ubyte
  protected DataType PCL_0xc0()
    throws IOException
  {
    UByte ubyte = new UByte();
    ubyte.SetNumber();
    return ubyte;
  }

  // PCL_DATA_TYPE_ubyte_array
  protected ArrayDataType PCL_0xc8()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    UByte_array ubyte = new UByte_array(arrayCount);
    ubyte.SetNumber();
    return ubyte;
  }

  // PCL_DATA_TYPE_ubyte_box
  protected ArrayDataType PCL_0xe0()
    throws IOException
  {
    UByte_box ubyte = new UByte_box();
    ubyte.SetNumber();
    return ubyte;
  }

  // PCL_DATA_TYPE_ubyte_xy
  protected ArrayDataType PCL_0xd0()
    throws IOException
  {
    UByte_xy ubyte = new UByte_xy();
    ubyte.SetNumber();
    return ubyte;
  }

  // PCL_DATA_TYPE_uint16
  protected DataType PCL_0xc1()
    throws IOException
  {
    UInt16 uint16 = new UInt16();
    uint16.SetNumber();
    return uint16;
  }

  // PCL_DATA_TYPE_uint16_array
  protected ArrayDataType PCL_0xc9()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    UInt16_array uint16 = new UInt16_array(arrayCount);
    uint16.SetNumber();
    return uint16;
  }

  // PCL_DATA_TYPE_uint16_box
  protected ArrayDataType PCL_0xe1()
    throws IOException
  {
    UInt16_box uint16 = new UInt16_box();
    uint16.SetNumber();
    return uint16;
  }

  // PCL_DATA_TYPE_uint16_xy
  protected ArrayDataType PCL_0xd1()
    throws IOException
  {
    UInt16_xy uint16 = new UInt16_xy();
    uint16.SetNumber();
    return uint16;
  }

  // PCL_DATA_TYPE_uint32
  protected DataType PCL_0xc2()
    throws IOException
  {
    UInt32 uint32 = new UInt32();
    uint32.SetNumber();
    return uint32;
  }

  // PCL_DATA_TYPE_uint32_array
  protected ArrayDataType PCL_0xca()
    throws IOException
  {
    int arrayCount = GetData().GetNumber().intValue();
    UInt32_array uint32 = new UInt32_array(arrayCount);
    uint32.SetNumber();
    return uint32;
  }

  // PCL_DATA_TYPE_uint32_box
  protected ArrayDataType PCL_0xe2()
    throws IOException
  {
    UInt32_box uint32 = new UInt32_box();
    uint32.SetNumber();
    return uint32;
  }

  // PCL_DATA_TYPE_uint32_xy
  protected ArrayDataType PCL_0xd2()
    throws IOException
  {
    UInt32_xy uint32 = new UInt32_xy();
    uint32.SetNumber();
    return uint32;
  }

  protected int  _streamHeader  = 0;

  protected int  _streamBody    = 0;

  protected char _bindingFormat = 0;

  protected abstract class DataType
  {
    public abstract void SetNumber()
      throws IOException;

    public Number GetNumber()
    {
      return _number;
    }

    protected byte[] GetNextBytes(int numberOfBytes)
      throws IOException
    {
      byte[] bytes = _printJob.GetNextBytes(numberOfBytes);
      if (_bindingFormat == PCL_BINDING_LOW_BYTE_FIRST)
      {
        // reverse the order of the bytes
        byte[] reorderedBytes = new byte[numberOfBytes];
        for (int i = 0; i < numberOfBytes; i++)
        {
          reorderedBytes[i] = bytes[numberOfBytes - i - 1];
        }
        bytes = reorderedBytes;
      }
      return bytes;
    }

    protected Number _number;
  }

  protected abstract class ArrayDataType
    extends DataType
  {
    protected ArrayDataType(int arrayCount)
    {
      _arrayCount = arrayCount;
    }

    public DataType GetNumber(int index)
    {
      return _numbers[index];
    }

    public DataType[] GetNumbers()
    {
      return _numbers;
    }

    public void SetArrayCount(int arrayCount)
    {
      _arrayCount = arrayCount;
    }

    protected int        _arrayCount;

    protected DataType[] _numbers;
  }

  static final int SIZEOF_4_BYTES = 4;

  static final int SIZEOF_2_BYTES = 2;

  static final int SIZEOF_1_BYTE  = 1;

  protected class Real32
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      byte[] bytes = GetNextBytes(SIZEOF_4_BYTES);
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
      try
      (DataInputStream dataInputStream = new DataInputStream(byteArrayInputStream);)
      {
        float real32 = dataInputStream.readFloat();
        _number = new Float(real32);
      }
    }
  }

  protected class Real32_array
    extends ArrayDataType
  {
    public Real32_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType real32 = new Real32();
        real32.SetNumber();
        _numbers[i] = real32;
      }
    }
  }

  protected class Real32_box
    extends Real32_array
  {
    public Real32_box()
    {
      super(4);
    }

  }

  protected class Real32_xy
    extends Real32_array
  {
    public Real32_xy()
    {
      super(2);
    }
  }

  protected class SInt16
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      byte[] bytes = GetNextBytes(SIZEOF_2_BYTES);
      int sint16 = ((bytes[0] & 0xff) << 8) | (bytes[1] & 0xff);
      _number = new Integer(sint16);
    }
  }

  protected class SInt16_array
    extends ArrayDataType
  {
    public SInt16_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType sint16 = new SInt16();
        sint16.SetNumber();
        _numbers[i] = sint16;
      }
    }
  }

  protected class SInt16_box
    extends SInt16_array
  {
    public SInt16_box()
    {
      super(4);
    }
  }

  protected class SInt16_xy
    extends SInt16_array
  {
    public SInt16_xy()
    {
      super(2);
    }
  }

  protected class SInt32
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      byte[] bytes = GetNextBytes(SIZEOF_4_BYTES);
      int sint32 = ((bytes[0] & 0xff) << 24) | ((bytes[1] & 0xff) << 16) | ((bytes[2] & 0xff) << 8) | (bytes[3] & 0xff);
      _number = new Integer(sint32);
    }
  }

  protected class SInt32_array
    extends ArrayDataType
  {
    public SInt32_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType sint32 = new SInt32();
        sint32.SetNumber();
        _numbers[i] = sint32;
      }
    }
  }

  protected class SInt32_box
    extends SInt32_array
  {
    public SInt32_box()
    {
      super(4);
    }
  }

  protected class SInt32_xy
    extends SInt32_array
  {
    public SInt32_xy()
    {
      super(2);
    }
  }

  protected class UByte
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      char ubyte = (char) (_printJob.GetNext() & 0x00ff);
      _number = new Integer(ubyte);
    }
  }

  protected class UByte_array
    extends ArrayDataType
  {
    public UByte_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType ubyte = new UByte();
        ubyte.SetNumber();
        _numbers[i] = ubyte;
      }
    }
  }

  protected class UByte_box
    extends UByte_array
  {
    public UByte_box()
    {
      super(4);
    }
  }

  protected class UByte_xy
    extends UByte_array
  {
    public UByte_xy()
    {
      super(2);
    }
  }

  protected class UInt16
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      byte[] bytes = GetNextBytes(SIZEOF_2_BYTES);
      int uint16 = ((0 & 0xff) << 24) | ((0 & 0xff) << 16) | ((bytes[0] & 0xff) << 8) | (bytes[1] & 0xff);
      _number = new Integer(uint16);
    }
  }

  protected class UInt16_array
    extends ArrayDataType
  {
    public UInt16_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType uint16 = new UInt16();
        uint16.SetNumber();
        _numbers[i] = uint16;
      }
    }
  }

  protected class UInt16_box
    extends UInt16_array
  {
    public UInt16_box()
    {
      super(4);
    }
  }

  protected class UInt16_xy
    extends UInt16_array
  {
    public UInt16_xy()
    {
      super(2);
    }
  }

  protected class UInt32
    extends DataType
  {
    @Override
    public void SetNumber()
      throws IOException
    {
      byte[] bytes = GetNextBytes(SIZEOF_4_BYTES);
      long uint32 = ((bytes[0] & 0xff) << 24) | ((bytes[1] & 0xff) << 16) | ((bytes[2] & 0xff) << 8) | (bytes[3] & 0xff);
      _number = new Long(uint32);
    }
  }

  protected class UInt32_array
    extends ArrayDataType
  {
    public UInt32_array(int arrayCount)
    {
      super(arrayCount);
    }

    @Override
    public void SetNumber()
      throws IOException
    {
      _numbers = new DataType[_arrayCount];
      for (int i = 0; i < _arrayCount; i++)
      {
        DataType uint32 = new UInt32();
        uint32.SetNumber();
        _numbers[i] = uint32;
      }
    }
  }

  protected class UInt32_box
    extends UInt32_array
  {
    public UInt32_box()
    {
      super(4);
    }
  }

  protected class UInt32_xy
    extends UInt32_array
  {
    public UInt32_xy()
    {
      super(2);
    }
  }

  private static HashMap<String, Method> pclMethods;

  static
  {
    pclMethods = new HashMap<>(25);
    Method[] declaredMethods = PCL6Binary_Parser.class.getDeclaredMethods();
    for (int i = 0; i < declaredMethods.length; i++)
    {
      Method method = declaredMethods[i];
      String methodName = method.getName();
      if (methodName.startsWith("PCL_") == true)
      {
        pclMethods.put(methodName, method);
      }
    }
  }

}
