package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.IOException;

import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;

/**
 * <p>Title: AbstractPrintJobLanguageParser</p>
 * <p>Description:
 * an abstract class for all print job parsers, regardless of print rendering language
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPrintJobLanguageParser
{

  protected static final String CARRIAGE_RETURN    = "\r";          // \u000d
  protected static final String LINE_FEED          = "\n";          // \u000a
  protected static final String CR_LF              = CARRIAGE_RETURN + LINE_FEED;

  protected static final String PJL_VALUE_TRUE     = "ON";
  protected static final String PJL_VALUE_FALSE    = "OFF";
  protected static final String DOTS_PER_INCH      = "DPI";

  protected static final byte EQUAL_SIGN_BYTE      = '=';
  protected static final byte LINE_FEED_BYTE       = '\n';          // \u000a
  protected static final byte CARRIAGE_RETURN_BYTE = '\r';          // \u000d
  protected static final byte ESCAPE_BYTE          = '\u001b';      // <esc>
  protected static final byte LEFT_PAREN_BYTE      = '(';
  protected static final byte RIGHT_PAREN_BYTE     = ')';
  protected static final byte FORM_FEED_BYTE       = '\u000c';      // formfeed

  protected static final char ESCAPE_CHAR          = '\u001b';      // <esc>
  protected static final char LINE_FEED_CHAR       = '\n';          // \u000a
  protected static final char CARRIAGE_RETURN_CHAR = '\r';          // \u000d

  protected static final int POINTS_PER_INCH       = 72;


  /**
   * Parse the print job. This method determines the correct print job type and 
   * defers parsing to the appropriate concrete parser. So it's much like a
   * factory method.
   *
   * @return true means the job was parsed. false means we probably don't support 
   *         the print job data stream type.
   */
  public static boolean Parse(PrintJob printJob)
    throws IOException
  {
    PCL5_Parser pcl5Parser = new PCL5_Parser(printJob);
    if (pcl5Parser.ParseTheJob() == true)
    {
      return true;
    }
    pcl5Parser = null;

    PCL6Binary_Parser pcl6BinaryParser = new PCL6Binary_Parser(printJob);
    if (pcl6BinaryParser.ParseTheJob() == true)
    {
      return true;
    }
    pcl6BinaryParser = null;

    /*
     * these are not implemented yet

    PCL6ASCII_Parser pcl6ASCIIParser = new PCL6ASCII_Parser(printJob);
    if (pcl6ASCIIParser.ParseTheJob() == true)
    {
      return true;
    }
    pcl6ASCIIParser = null;

    PS2_Parser ps2Parser = new PS2_Parser(printJob);
    if (ps2Parser.ParseTheJob() == true)
    {
      return true;
    }
    ps2Parser = null;
    
     */

    PS3DSC_Parser ps3DSCParser = new PS3DSC_Parser(printJob);
    if (ps3DSCParser.ParseTheJob() == true)
    {
      return true;
    }
    ps3DSCParser = null;

    return false;
  }

  protected AbstractPrintJobLanguageParser(PrintJob printJob)
  {
    super();
    _printJob = printJob;
  }

  protected void AssignPropertiesToPrintJob()
  {
    if (_copies == null)
    {
      _copies = "1";
    }
    _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_COPIES,_copies);

    if (_numberOfPages == 0)
    {
      _numberOfPages = 1;
    }
    _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES,String.valueOf(_numberOfPages * new Integer(_copies).intValue()));

    if (_collate != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_COLLATE,_collate);
    }

    if (_color == null)
    {
      _color = PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME;
    }
    _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_COLOR,_color);

    if (_holePunch != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_HOLE_PUNCH,_holePunch);
    }

    if (_orientation != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_ORIENTATION,_orientation);
    }

    if (_pagesPerPage != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAGES_PER_PAGE,_pagesPerPage);
    }

    if (_paperLength != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_LENGTH,_paperLength);
    }

    if (_paperSize != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE,_paperSize);
    }

    if (_paperSource != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SOURCE,_paperSource);
    }

    if (_paperWidth != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_WIDTH,_paperWidth);
    }

    if (_printQuality != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PRINT_QUALITY,_printQuality);
    }

    if (_renderMode != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_RENDER_MODE,_renderMode);
    }

    if (_scale != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_SCALE,_scale);
    }

    if (_sides == null)
    {
      _sides = PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX;
    }
    _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_SIDES,_sides);

    if (_sort != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_SORT,_sort);
    }

    if (_staple != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_STAPLE,_staple);
    }

    if (_resolution != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_RESOLUTION,_resolution);
    }

    if (_paperType != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_TYPE,_paperType);
    }

    if (_jobName != null)
    {
      _printJob.SetProperty(PrintJobPropertiesRow.PROPERTY_JOB_NAME,_jobName);
    }

  }

  public abstract boolean ParseTheJob()
    throws IOException;

  protected abstract boolean ICanParse()
      throws IOException;

  protected PrintJob _printJob;

  protected String   _collate = null;
  protected String   _color = null;
  protected String   _copies = null;
  protected String   _holePunch = null;
  protected String   _jobName = null;
  protected String   _language = null;
  protected int      _numberOfPages = 0;
  protected String   _orientation = null;
  protected String   _pagesPerPage = null;
  protected String   _paperLength = null;
  protected String   _paperSize = null;
  protected String   _paperSource = null;
  protected String   _paperType = null;
  protected String   _paperWidth = null;
  protected String   _printQuality = null;
  protected String   _renderMode = null;
  protected String   _resolution = null;
  protected String   _scale = null;
  protected String   _sides = null;
  protected String   _sort = null;
  protected String   _staple = null;

}