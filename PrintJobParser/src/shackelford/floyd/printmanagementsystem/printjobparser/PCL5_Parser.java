package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;


/**
 * <p>Title: PCL5_Parser</p>
 * <p>Description:
 * parses HP PCL 5 and extracts print job characteristics
 * 
 * this information is taken from the HP "PCL 5 Printer Language Technical Reference Manual",
 *   HP Part No. 5961-0509, First Edition - October 1992.
 *
 *
 * A job typically consists of three parts:
 *   1. Commands providing job control.
 *   2. Commands providing page control.
 *   3. Print data.
 *
 * Structure of a Typical Job:
 *
 *   <esc>%-12345X    UEL Command (exit language).
 *   <esc>E           Printer Reset Command.
 *   Preamble         Job Control Commands.
 *   Page 1           Page Control Commands.
 *                    Data
 *   Page 2           Page Control Commands.
 *                    Data.
 *   ...
 *   Page n           Page Control Commands.
 *                    Data.
 *   <esc>E           Printer Reset Command.
 *   <esc>%-12345X    UEL Command (exit language).
 *
 *
 * There are two forms of PCL/PJL escape sequences:
 *   two-character escape sequences and parameterized escape sequences.
 *
 * Two-character escape sequences have the following form:
 *
 * <esc>X
 *
 * where "X" is a character that defines the operation to be performed.
 *
 * "X" may be any character from the ASCII table within the range 48-126 decimal
 * ("0" through "~").
 *
 * these are the two-characters escape sequences:
 *
 * <esc>9      - Clear Horizontal Margins
 * <esc>E      - Printer Reset
 * <esc>=      - Half-Line Feed
 * <esc>Y      - Display Functions, Enable
 * <esc>Z      - Display Functions, Disable
 *
 *
 * Parameterized escape sequences have the following form:
 *
 * <esc>Xy#z1#z2#z3...#Zn[data]
 *
 * where:
 *
 * y, #, zi (z1, z2, z3 . . . ) and [data] may be optional, depending on the command.
 *
 * X Parameterized Character - A character from the ASCII table within the range 33-47 decimal
 *   ("!" through "/") indicating that the escape sequence is parameterized.
 *
 * y Group Character - A character from the ASCII table within the range 96-126 decimal
 *   ("`" through "~") that specifies the group type of control being performed.
 *
 *   Use these three rules to combine and shorten printer commands (i.e. group commands):
 *     1. The first two characters after <esc> (the parameterized and group character)
 *        must be the same in all of the commands to be combined.
 *     2. All alphabetic characters within the combined printer command are lower-case,
 *        except the final letter which is always upper-case. The final character in the printer
 *        command must always be upper-case to let the printer know that the command is complete.
 *     3. The printer commands are performed in the order that they are combined (from left to right).
 *        Be sure to combine commands in the order that they are to be performed.
 *
 * # Value Field - A group of characters specifying a numeric value. The numeric value is
 *   represented as an ASCII string of characters within the range 48-57 decimal
 *   ("0" through "9") that may be preceded by a "+" or "-" sign and may contain a fractional
 *   portion indicated by the digits after a decimal point ("."). Numeric value fields are within
 *   the range -32767 to 65535. If an escape sequence requires a value field and a value is
 *   not specified, a value of zero is assumed.
 *
 * zi Parameter Character - Any character from the ASCII table within the range 96-126 decimal
 *   ("`" through "~"). This character specifies the parameter to which the previous value field
 *   applies. This character is used when combining escape sequences.
 *
 * Zn Termination Character - Any character from the ASCII table within the range 64-94
 *   decimal ("@" through "^"). This character specifies the parameter to which the previous
 *   value field applies. This character terminates the escape sequence.
 *
 * [data] Binary Data is eight-bit data (for example, graphics data, downloaded fonts, etc.).
 *   The number of bytes of binary data is specified by the value field of the escape sequence.
 *   Binary data immediately follows the terminating character of the escape sequence.
 *
 *
 * these are the parameterized commands:
 *
 * _ (underline) is used a placeholder for the lowercase l ("ell").
 * this is used to distinguish it from the number 1 ("wun").
 * the hp tech ref manual uses a cursive "ell", but i can't do that here.
 *
 * why in the world would someone create a computer font that uses the same
 * character shape for the "ell" and the "wun"???
 *
 * these commands have no grouping code:
 *
 * <esc>(ID    - Primary Symbol Set       ID can be any printable sequence characters starting with a number
 * <esc>)ID    - Secondary Symbol Set     ID can be any printable sequence characters starting with a number
 *
 * <esc>(3@    - Select Default Font as Primary
 * <esc>)3@    - Select Default Font as Secondary
 * <esc>(#B    - Primary Stroke Weight
 * <esc>(#X    - Primary Font Selection by ID #
 * <esc>)#X    - Secondary Font Selection by ID #
 *
 * these commands have a grouping character:
 *
 * <esc>(f#W   - Define Symbol Set [data]
 * <esc>)s1P   - Secondary Spacing
 * <esc>)s#B   - Secondary Stroke Weight
 * <esc>(s#H   - Primary Pitch
 * <esc>)s#H   - Secondary Pitch
 * <esc>(s#P   - Primary Spacing
 * <esc>(s#S   - Primary Style
 * <esc>)s#S   - Secondary Style
 * <esc>(s#T   - Typeface, Primary
 * <esc>)s#T   - Typeface, Secondary
 * <esc>(s#V   - Primary Height
 * <esc>)s#V   - Secondary Height
 * <esc>(s#W   - Character Descriptor [Data]
 * <esc>)s#W   - Font Header [data]
 *
 * <esc>&a#C   - Horizontal Cursor Positioning (Columns)
 * <esc>&a#G   - Duplex Page Side Selection                #: 0 = next side, 1 = front side, 2 = back side
 * <esc>&a#H   - Horizontal Cursor Positioning (Decipoints)
 * <esc>&a#L   - Left Margin
 * <esc>&a#M   - Right Margin
 * <esc>&a#P   - Print Direction                           #:   0 - 0 rotation.
 *                                                             90 - 90 ccw rotation.
 *                                                            180 - 180 ccw rotation.
 *                                                            270 - 270 ccw rotation.
 * <esc>&a#R   - Vertical Cursor Positioning (Rows)
 * <esc>&a#V   - Vertical Cursor Positioning (Decipoints)
 * <esc>*b#M   - Set Compression Method                    #: 0 - Unencoded
 *                                                            1 - Run-length encoding
 *                                                            2 - Tagged Imaged File Format (TIFF) rev. 4.0
 *                                                            3 - Delta row compression
 *                                                            4 - Reserved
 *                                                            5 - Adaptive compression
 * <esc>*b#V   - ? [data]
 * <esc>*b#W   - Transfer Raster Data [data]
 * <esc>&b#W   - ? [data]
 * <esc>*b#Y   - Raster Y Offset
 * <esc>*c#A   - Horizontal Rectangle Size (PCL Units)
 * <esc>*c#B   - Vertical Rectangle Size (PCL Units)
 * <esc>*c#D   - Font ID (assign)
 * <esc>*c#E   - Character Code
 * <esc>*c#F   - Font Control
 * <esc>*c#H   - Horizontal Rectangle Size (Decipoints)
 * <esc>*c#P   - Fill Rectangular Area
 * <esc>*c#Q   - Pattern Control
 * <esc>*c#R   - Symbol Set ID Code
 * <esc>*c#S   - Symbol Set Control
 * <esc>*c#T   - Set Picture Frame Anchor Point
 * <esc>*c#X   - Horizontal Picture Frame Size (decipoints)
 * <esc>*c#Y   - Vertical Picture Frame Size (decipoints)
 * <esc>*c#V   - Vertical Rectangle Size (Decipoints)
 * <esc>*c#W   - User-Defined Pattern [data]
 * <esc>&d#D   - Enable Underline
 * <esc>&d@    - Disable Underline
 * <esc>&f#S   - Push/Pop Cursor Position
 * <esc>&f#X   - Macro Control
 * <esc>&f#Y   - Macro ID (assign)
 * <esc>&k#G   - Line Termination
 * <esc>&k#H   - Horizontal Motion Index
 * <esc>&n#W   - ? [data]
 * <esc>*p#R   - Set Pattern Reference Point
 * <esc>*p#X   - Horizontal Cursor Positioning (PCL Units)
 * <esc>&p#X   - Transparent Mode [data]
 * <esc>*p#Y   - Vertical Cursor Positioning (PCL Units)
 * <esc>*r#A   - Start Raster Graphics
 * <esc>*rB    - End Raster Graphics (old)
 * <esc>*rC    - End Raster Graphics (new)
 * <esc>&r#F   - Flush All Pages
 * <esc>*r#F   - Raster Graphics Presentation
 * <esc>&r#T   - Inquire Status Readback Entity
 * <esc>*r#T   - Raster Height
 * <esc>*r#S   - Raster Width
 * <esc>&s#T   - Set Status Readback Location Type
 * <esc>&s#U   - Set Status Readback Location Unit
 * <esc>&s#C   - End-Of-Line Wrap
 * <esc>*s#M   - Free Space
 * <esc>*s#X   - Echo
 * <esc>*t#R   - Raster Graphics Resolution
 * <esc>&u#D   - Unit Of Measure
 * <esc>*v#N   - Source Transparency Mode
 * <esc>*v#O   - Pattern Transparency mode
 * <esc>*v#T   - Select Current Pattern Command
 *
 * in the following commands, the _ represents an "ell"
 *
 * <esc>*_#W   - ? [data]
 * <esc>&_1T   - Job Separation
 * <esc>&_#A   - Page Size                   PAPER:
 *                                             #: 1 - Executive
 *                                                2 - Letter
 *                                                3 - Legal
 *                                                6 - Ledger
 *                                                26 - A4
 *                                                27 - A3
 *                                           ENVELOPES:
 *                                             #: 80 - Monarch
 *                                                81 - Com-10
 *                                                90 - International DL
 *                                                91 - International C5
 *                                                100 - International
 * <esc>&_#C   - Vertical Motion Index
 * <esc>&_#D   - Line Spacing
 * <esc>&_#E   - Top Margin
 * <esc>&_#F   - Text Length
 * <esc>&_#G   - Output Bin Selection        #: 1 = upper output bin
 *                                              2 = lower/rear output bin
 * <esc>&_#H   - Paper Source                #: 0 - Print the current page (paper source remains unchanged).
 *                                              1 - Feed paper from the a printer-specific tray.
 *                                              2 - Feed paper from manual input.
 *                                              3 - Feed envelope from manual input.
 *                                              4 - Feed paper from lower tray.
 *                                              5 - Feed from optional paper source.
 *                                              6 - Feed envelope from optional envelope feeder.
 * <esc>&_#L   - Perforation Skip
 * <esc>&_#O   - Page Orientation            #: 0 - Portrait
 *                                              1 - Landscape
 *                                              2 - Reverse Portrait
 *                                              3 - Reverse Landscape
 * <esc>&_#S   - Simplex/Duplex Print        #: 0 = simplex
 *                                              1 & 2 = duplex
 * <esc>&_#U   - Left Offset Registration
 * <esc>&_#X   - Number of Copies            #: number of copies
 * <esc>&_#Z   - Top Offset Registration
 *
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PCL5_Parser
  extends AbstractPCLParser
{

  /**
   * Constructor
   * @param printJob
   */
  public PCL5_Parser(PrintJob printJob)
  {
    super(printJob);
  }

  @Override
  protected boolean ICanParse()
    throws IOException
  {
    if (ComputeOffsets() == true)
    {
      // the pcl5 stream begins with an escape character (0x1b)
      if (_printJob.GetByteAt(_streamStart) == ESCAPE_BYTE)
      {
        return true;
      }
    }
    return false;
  }

  @Override
  public boolean ParseTheJob()
    throws IOException
  {
    if (ICanParse() == false)
    {
      return false;
    }

    ProcessJobHeader();
    _language = "PCL5";

    // process the stream header
    _printJob.SetCurrentOffset(_streamStart);
    while (_printJob.GetCurrentOffset() < (_endUEL - 1))
    {
      ProcessCommand();
      SkipToNextCommand();
    }

    AssignPropertiesToPrintJob();

    return true;
  }

  /**
   * process the command that starts at _printJob.GetJobChars()[_printJob.GetCurrentOffset()]
   */
  protected void ProcessCommand()
    throws IOException
  {
    // i should be pointing to <esc>
    if (_printJob.GetNext() != ESCAPE_BYTE)
    {
      System.out.println("Command does not begin with <esc> at offset " + (_printJob.GetCurrentOffset() - 1));
      return;
    }

    // get the next character
    char nextChar = _printJob.GetNextAsChar();

    // if this is also <esc>, then treat this as a non-command
    // (i've seen <esc><esc>)
    if (nextChar == ESCAPE_CHAR)
    {
      return;
    }

    _currentPCLCommand = String.valueOf(nextChar);

    // check to see if this is a two-character command
    if ((nextChar >= 48) && (nextChar <= 126))
    {
      // we're done - this is a two-character command
      // DispatchTwoCharacterCommand();     // there are no two-character commands we are interested in
      return;
    }

    // this is a parameterized command, and possibly combined commands.
    DispatchParameterizedCommand();

  }

  protected Float ExtractParmFromCommand()
  {
    String parmString = _currentPCLCommand.substring(2, _currentPCLCommand.length() - 1);
    return Float.valueOf(parmString);
  }

  protected void SkipPastCommandData()
    throws IOException
  {
    Float bytesOfData = ExtractParmFromCommand();
    _printJob.MoveCurrentOffset(bytesOfData.intValue());
  }

  /**
   * this method dispatches two-character commands. two-character commands cannot be combined.
   */
  /* there are no two-character commands we are interested in
    protected void DispatchTwoCharacterCommand()
    {
      // the method name is PCL_X where X is the 2nd character of the two character command
      // and the first (and only character) of _currentPCLCommand.
      String methodName = "PCL_" + ConvertToMethodString(_currentPCLCommand.charAt(0));

      try
      {
        Method pclMethod = (Method) pclMethods.get(methodName);
        if (pclMethod != null)
        {
          pclMethod.invoke(this,EMPTY_OBJECT_ARRAY);
        }
      }
      catch (Exception excp)
      {
        // do nothing
      }
    }
  */

  /**
   * this method dispatches parameterized commands, including parameterized commands
   * that have been combined.
   */

  char[] _DispatchParameterizedCommand_chars = new char[100]; // optimization

  protected void DispatchParameterizedCommand()
    throws IOException
  {
    String methodNameLeader = ConvertToMethodString(_currentPCLCommand.charAt(0));

    // get the next character of the command
    char nextChar = _printJob.GetNextAsChar();
    // is it a grouping character? the grouping character is optional.
    if ((nextChar >= 96) && (nextChar <= 126))
    {
      // this is a grouping value
      _currentPCLCommand += nextChar;
      methodNameLeader += "_" + ConvertToMethodString(nextChar);
    }

    String savePCLLeader = _currentPCLCommand; // used to construct multiple commands using the same leader

    // get the parameter and the command letter for each command
    boolean endOfCommand = false;
    do
    {
      int i = 0;
      do
      {
        // grab the current command character
        nextChar = _printJob.GetNextAsChar();
        if (Character.isUpperCase(nextChar) == true)
        {
          endOfCommand = true;
        }
        else
        {
          nextChar = Character.toUpperCase(nextChar);
        }
        _DispatchParameterizedCommand_chars[i++] = nextChar;
      }
      while ((nextChar < 64) || (nextChar > 94));

      String thisCommandTrailer = new String(_DispatchParameterizedCommand_chars, 0, i);

      _currentPCLCommand = savePCLLeader + thisCommandTrailer;

      String methodNameTrailer = ConvertToMethodString(nextChar);
      String methodName = "PCL_" + methodNameLeader + "_" + methodNameTrailer;

      try
      {
        Method pclMethod = pclMethods.get(methodName);
        if (pclMethod != null)
        {
          pclMethod.invoke(this, EMPTY_OBJECT_ARRAY);
        }
      }
      catch (Exception excp)
      {
        // do nothing
      }
    }
    while (endOfCommand == false);
  }

  protected String ConvertToMethodString(char theChar)
  {
    String methodString;
    switch (theChar)
    {
      case '@':
        methodString = "AtSign";
        break;
      case '(':
        methodString = "LeftParen";
        break;
      case ')':
        methodString = "RightParen";
        break;
      case '&':
        methodString = "Ampersand";
        break;
      case '*':
        methodString = "Star";
        break;
      case '=':
        methodString = "EqualSign";
        break;
      default:
        methodString = String.valueOf(theChar);
        break;
    }
    return methodString;
  }

  /**
   * this method skips the data between the commands
   */
  protected void SkipToNextCommand()
    throws IOException
  {
    if (_printJob.PeekNext() == ESCAPE_BYTE)
    {
      // i'm all ready to start the next command
      return;
    }
    while ((_printJob.MoreBytes() == true) && (_printJob.PeekNext() != ESCAPE_BYTE))
    {
      if (_printJob.GetNext() == FORM_FEED_BYTE)
      {
        _numberOfPages++;
      }
    }
  }

  // <esc>&a#G   - Duplex Page Side Selection
  protected void PCL_Ampersand_a_G()
  {
    switch (ExtractParmFromCommand().intValue())
    {
      case 0: // select next side
        _numberOfPages++;
        break;
      case 1: // select front side
        _numberOfPages++;
        break;
      case 2: // select back side
        _numberOfPages++;
        break;
      default: // do nothing
        break;
    }
  }

  // <esc>*b#V   - ? [data]
  protected void PCL_Star_b_V()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>*b#W   - Transfer Raster Data [data]
  protected void PCL_Star_b_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>*c#W   - User-Defined Pattern  [data]
  protected void PCL_Star_c_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>*g#W   - ?  [data]
  protected void PCL_Star_g_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>*o#W   - ?  [data]
  protected void PCL_Star_o_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>&p#X   - Transparent Mode [data]
  protected void PCL_Ampersand_p_X()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>&u#D   - Unit Of Measure
  protected void PCL_Ampersand_u_D()
  {
    _resolution = String.valueOf(ExtractParmFromCommand().intValue());
  }

  // <esc>*r#U   - Set Color
  protected void PCL_Star_r_U()
  {
    switch (ExtractParmFromCommand().intValue())
    {
      case -3:
      case 3:
        _color = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
        break;
      case 1:
        _color = PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME;
        break;
      default:
        _color = PrintJobPropertiesRow.VALUE_UNKNOWN;
        break;
    }
  }

  // <esc>*t#R   - Raster Graphics Resolution
  protected void PCL_Star_t_R()
  {
    if (_resolution.equals(PrintJobPropertiesRow.VALUE_UNKNOWN) == true)
    {
      _resolution = String.valueOf(ExtractParmFromCommand().intValue());
    }
  }

  // <esc>*v#W   - Set Color Palette
  protected void PCL_Star_v_W()
  {
    // assume this is a color print job
    _color = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
  }

  // <esc>&_#A  - Page Size
  protected void PCL_Ampersand_l_A()
  {
    switch (ExtractParmFromCommand().intValue())
    {
      case 1:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_EXECUTIVE;
        break;
      case 2:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LETTER;
        break;
      case 3:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LEGAL;
        break;
      case 6:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_LEDGER;
        break;
      case 26:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A4;
        break;
      case 27:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_A3;
        break;
      case 80:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_MONARCH;
        break;
      case 81:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_COM_10;
        break;
      case 90:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_DL;
        break;
      case 91:
        _paperSize = PrintJobPropertiesRow.VALUE_PAPER_SIZE_C5;
        break;
      default:
        _paperSize = PrintJobPropertiesRow.VALUE_UNKNOWN;
        break;
    }
  }

  // <esc>&_#H   - Paper Source
  protected void PCL_Ampersand_l_H()
  {
    // this command causes the current page to be printed
    switch (ExtractParmFromCommand().intValue())
    {
      case 0: // print the current page from the current paper source
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_CURRENT;
        //_numberOfPages++;
        break;
      case 1: // Feed paper from the a printer-specific tray.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_PRINTERSPECIFIC;
        //_numberOfPages++;
        break;
      case 2: // Feed paper from manual input.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_MANUALPAPER;
        //_numberOfPages++;
        break;
      case 3: // Feed envelope from manual input.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_MANUALENVELOPE;
        //_numberOfPages++;
        break;
      case 4: // Feed paper from lower tray.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_LOWERTRAY;
        //_numberOfPages++;
        break;
      case 5: // Feed from optional paper source.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_OPTIONALPAPERSOURCE;
        //_numberOfPages++;
        break;
      case 6: // Feed envelope from optional envelope feeder.
        _paperSource = PrintJobPropertiesRow.VALUE_PAPERSOURCE_OPTIONALENVELOPESOURCE;
        //_numberOfPages++;
        break;
      case 7: // I noticed that this comes through early on in the file, but
              // the PCL5 docs don't mention this case. i'll ignore it for now because
              // it makes the page count 1 page too high if i don't.
        _paperSource = PrintJobPropertiesRow.VALUE_UNKNOWN;
        break;
      default: // do nothing
        break;
    }
  }

  // <esc>&_#O   - Page Orientation        that's an "oh" and not a "zero"
  protected void PCL_Ampersand_l_O()
  {
    switch (ExtractParmFromCommand().intValue())
    {
      case 0:
        _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_PORTRAIT;
        break;
      case 1:
        _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_LANDSCAPE;
        break;
      case 2:
        _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_REVERSE_PORTRAIT;
        break;
      case 3:
        _orientation = PrintJobPropertiesRow.VALUE_ORIENTATION_REVERSE_LANDSCAPE;
        break;
      default:
        _orientation = PrintJobPropertiesRow.VALUE_UNKNOWN;
        break;
    }
  }

  // <esc>&_#S   - Simplex/Duplex Print
  protected void PCL_Ampersand_l_S()
  {
    // do nothing
    switch (ExtractParmFromCommand().intValue())
    {
      case 0:
        _paperSize = PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX;
        break;
      case 1:
        _paperSize = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
        break;
      case 2:
        _paperSize = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
        break;
      default:
        _paperSize = PrintJobPropertiesRow.VALUE_UNKNOWN;
        break;
    }
  }

  // <esc>&_#X   - Number of Copies
  protected void PCL_Ampersand_l_X()
  {
    _copies = String.valueOf(ExtractParmFromCommand().intValue());
  }

  // <esc>(f#W   - Define Symbol Set  [data]
  protected void PCL_LeftParen_f_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>)s#W   - Font Header [data]
  protected void PCL_RightParen_s_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>(ID  - Primary Symbol Set
  protected void PCL_LeftParen()
    throws IOException
  {
    SkipToNextCommand();
  }

  // <esc>)ID  - Secondary Symbol Set
  protected void PCL_RightParen()
    throws IOException
  {
    SkipToNextCommand();
  }

  // <esc>*_#W  - ? [data]               "_" is "ell" (i.e. lowercase "L")
  protected void PCL_Star_l_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>&b#W  - ? [data]
  protected void PCL_Ampersand_b_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>&n#W  - ? [data]
  protected void PCL_Ampersand_n_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  // <esc>(s#W   - Character Descriptor [Data]
  protected void PCL_LeftParen_s_W()
    throws IOException
  {
    SkipPastCommandData();
  }

  /* these are commented out

    // <esc>9 - Clear Horizontal Margins
    protected void PCL_9()
    {
      // do nothing
    }

    // <esc>E - Printer Reset
    protected void PCL_E()
    {
      // do nothing
    }

    // <esc>= - Half-Line Feed
    protected void PCL_EqualSign()
    {
      // do nothing
    }

    // <esc>Y - Display Functions, Enable
    protected void PCL_Y()
    {
      // do nothing
    }

    // <esc>Z - Display Functions, Disable
    protected void PCL_Z()
    {
      // do nothing
    }

    // <esc>&a#C   - Horizontal Cursor Positioning (Columns)
    protected void PCL_Ampersand_a_C()
    {
      // do nothing
    }


    // <esc>&a#H   - Horizontal Cursor Positioning (Decipoints)
    protected void PCL_Ampersand_a_H()
    {
      // do nothing
    }

    // <esc>&a#L   - Left Margin
    protected void PCL_Ampersand_a_L()
    {
      // do nothing
    }

    // <esc>&a#M   - Right Margin
    protected void PCL_Ampersand_a_M()
    {
      // do nothing
    }

    // <esc>&a#P   - Print Direction
    protected void PCL_Ampersand_a_P()
    {
      // do nothing
    }

    // <esc>&a#R   - Vertical Cursor Positioning (Rows)
    protected void PCL_Ampersand_a_R()
    {
      // do nothing
    }

    // <esc>&a#V   - Vertical Cursor Positioning (Decipoints)
    protected void PCL_Ampersand_a_V()
    {
      // do nothing
    }

    // <esc>*b#M   - Set Compression Method
    protected void PCL_Star_b_M()
    {
      // do nothing
    }

    // <esc>*b#Y   - Raster Y Offset
    protected void PCL_Star_b_Y()
    {
      // do nothing
    }

    // <esc>*c#A   - Horizontal Rectangle Size (PCL Units)
    protected void PCL_Star_c_A()
    {
      // do nothing
    }

    // <esc>*c#B   - Vertical Rectangle Size (PCL Units)
    protected void PCL_Star_c_B()
    {
      // do nothing
    }

    // <esc>*c#D   - Font ID (assign)
    protected void PCL_Star_c_D()
    {
      // do nothing
    }

    // <esc>*c#E   - Character Code
    protected void PCL_Star_c_E()
    {
      // do nothing
    }

    // <esc>*c#F   - Font Control
    protected void PCL_Star_c_F()
    {
      // do nothing
    }

    // <esc>*c#H   - Horizontal Rectangle Size (Decipoints)
    protected void PCL_Star_c_H()
    {
      // do nothing
    }

    // <esc>*c#P   - Fill Rectangular Area
    protected void PCL_Star_c_P()
    {
      // do nothing
    }

    // <esc>*c#Q   - Pattern Control
    protected void PCL_Star_c_Q()
    {
      // do nothing
    }

    // <esc>*c#R   - Symbol Set ID Code
    protected void PCL_Star_c_R()
    {
      // do nothing
    }

    // <esc>*c#S   - Symbol Set Control
    protected void PCL_Star_c_S()
    {
      // do nothing
    }

    // <esc>*c#T   - Set Picture Frame Anchor Point
    protected void PCL_Star_c_T()
    {
      // do nothing
    }

    // <esc>*c#X   - Horizontal Picture Frame Size (decipoints)
    protected void PCL_Star_c_X()
    {
      // do nothing
    }

    // <esc>*c#Y   - Vertical Picture Frame Size (decipoints)
    protected void PCL_Star_c_Y()
    {
      // do nothing
    }

    // <esc>*c#V   - Vertical Rectangle Size (Decipoints)
    protected void PCL_Star_c_V()
    {
      // do nothing
    }

    // <esc>&d#D   - Enable Underline
    protected void PCL_Ampersand_d_D()
    {
      // do nothing
    }

    // <esc>&d@    - Disable Underline
    protected void PCL_Ampersand_d_AtSign()
    {
      // do nothing
    }

    // <esc>&f#S   - Push/Pop Cursor Position
    protected void PCL_Ampersand_f_S()
    {
      // do nothing
    }

    // <esc>&f#Y   - Macro ID (assign)
    protected void PCL_Ampersand_f_Y()
      throws IOException
    {
      _lastMacroID = ExtractParmFromCommand().intValue();
    }

    // <esc>&f#X   - Macro Control
    protected void PCL_Ampersand_f_X()
      throws IOException
    {
      switch (ExtractParmFromCommand().intValue())
      {
        case  0: // Start macro definition (last ID specified)
                 break;
        case  1: // Stop macro definition
                 break;
        case  2: // Execute macro (last ID specified)
                 System.out.println("Warning: macro with id " + _lastMacroID + " invoked. cannot parse macros.\n\tparse properties may be incorrect.");
                 break;
        case  3: // Call macro (last ID specified)
                 System.out.println("Warning: macro with id " + _lastMacroID + " invoked. cannot parse macros.\n\tparse properties may be incorrect.");
                 break;
        case  4: // Enable macro for automatic overlay (last ID specified)
                 break;
        case  5: // Disable automatic overlay
                 break;
        case  6: // Delete all macros
                 break;
        case  7: // Delete all temporary macros
                 break;
        case  8: // Delete macro (last ID specified)
                 break;
        case  9: // Make macro temporary (last ID specified)
                 break;
        case 10: // Make macro permanent (last ID specified)
                 break;
        default: break;
      }
    }

    // <esc>&k#G   - Line Termination
    protected void PCL_Ampersand_k_G()
    {
      // do nothing
    }

    // <esc>&k#H   - Horizontal Motion Index
    protected void PCL_Ampersand_k_H()
    {
      // do nothing
    }

    // <esc>*p#R   - Set Pattern Reference Point
    protected void PCL_Star_p_R()
    {
      // do nothing
    }

    // <esc>*p#X   - Horizontal Cursor Positioning (PCL Units)
    protected void PCL_Star_p_X()
    {
      // do nothing
    }

    // <esc>*p#Y   - Vertical Cursor Positioning (PCL Units)
    protected void PCL_Star_p_Y()
    {
      // do nothing
    }

    // <esc>*r#A   - Start Raster Graphics
    protected void PCL_Star_r_A()
    {
      // do nothing
    }

    // <esc>*rB   -  End Raster Graphics (old)
    protected void PCL_Star_r_B()
    {
      // do nothing
    }

    // <esc>*rC    - End Raster Graphics (new)
    protected void PCL_Star_r_C()
    {
      // do nothing
    }

    // <esc>&r#F   - Flush All Pages
    protected void PCL_Ampersand_r_F()
      throws IOException
    {
      switch (ExtractParmFromCommand().intValue())
      {
        case  0: // Flush all complete pages
                 break;
        case  1: // Flush all pages
                 break;
        default: // do nothing
                 break;
      }
    }

    // <esc>*r#F   - Raster Graphics Presentation
    protected void PCL_Star_r_F()
    {
      // do nothing
    }

    // <esc>&r#T   - Inquire Status Readback Entity
    protected void PCL_Ampersand_r_T()
    {
      // do nothing
    }

    // <esc>*r#T   - Raster Height
    protected void PCL_Star_r_T()
    {
      // do nothing
    }

    // <esc>*r#S   - Raster Width
    protected void PCL_Star_r_S()
    {
      // do nothing
    }

    // <esc>&s#T   - Set Status Readback Location Type
    protected void PCL_Ampersand_s_T()
    {
      // do nothing
    }

    // <esc>&s#U   - Set Status Readback Location Unit
    protected void PCL_Ampersand_s_U()
    {
      // do nothing
    }

    // <esc>&s#C   - End-Of-Line Wrap
    protected void PCL_Ampersand_s_C()
      throws IOException
    {
      switch (ExtractParmFromCommand().intValue())
      {
        case  0: // Enables End-of-Line Wrap
                 break;
        case  1: // Disables End-of-Line Wrap
                 break;
        default: // do nothing
                 break;
      }
    }

    // <esc>*s#M   - Free Space
    protected void PCL_Star_s_M()
    {
      // do nothing
    }

    // <esc>*s#X   - Echo
    protected void PCL_Star_s_X()
    {
      // do nothing
    }

    // <esc>*v#N   - Source Transparency Mode
    protected void PCL_Star_v_N()
    {
      // do nothing
    }

    // <esc>*v#O   - Pattern Transparency mode            that's "oh" and not "zero"
    protected void PCL_Star_v_O()
    {
      // do nothing
    }

    // <esc>*v#T   - Select Current Pattern Command
    protected void PCL_Star_v_T()
    {
      // do nothing
    }

    // <esc>&_1T   - Job Separation
    protected void PCL_Ampersand_l_T()
    {
      // do nothing
    }

    // <esc>&_#C   - Vertical Motion Index
    protected void PCL_Ampersand_l_C()
    {
      // do nothing
    }

    // <esc>&_#D   - Line Spacing
    protected void PCL_Ampersand_l_D()
    {
      // do nothing
    }

    // <esc>&_#E   - Top Margin
    protected void PCL_Ampersand_l_E()
    {
      // do nothing
    }

    // <esc>&_#F   - Text Length
    protected void PCL_Ampersand_l_F()
    {
      // do nothing
    }

    // <esc>&_#L   - Perforation Skip
    protected void PCL_Ampersand_l_L()
    {
      // do nothing
    }

    // <esc>&_#G   - Output Bin Selection
    protected void PCL_Ampersand_l_G()
      throws IOException
    {
      switch (ExtractParmFromCommand().intValue())
      {
        case   1: // output to upper bin
                  break;
        case   2: // output to rear/lower bin
                  break;
        default:  // do nothing
                  break;
      }
    }

    // <esc>&_#U   - Left Offset Registration
    protected void PCL_Ampersand_l_U()
    {
      // do nothing
    }

    // <esc>&_#Z   - Top Offset Registration
    protected void PCL_Ampersand_l_Z()
    {
      // do nothing
    }

    // <esc>(3@    - Select Default Font as Primary
    protected void PCL_LeftParen_3_AtSign()
    {
      // do nothing
    }

    // <esc>(#B    - Primary Stroke Weight
    protected void PCL_LeftParen_B()
    {
      // do nothing
    }

    // <esc>(#X    - Primary Font Selection by ID #
    protected void PCL_LeftParen_X()
    {
      // do nothing
    }

    // <esc>(s#B   - ?
    protected void PCL_LeftParen_s_B()
    {
      // do nothing
    }

    // <esc>(s#H   - Primary Pitch
    protected void PCL_LeftParen_s_H()
    {
      // do nothing
    }

    // <esc>(s#P   - Primary Spacing
    protected void PCL_LeftParen_s_P()
    {
      // do nothing
    }

    // <esc>(s#S   - Primary Style
    protected void PCL_LeftParen_s_S()
    {
      // do nothing
    }

    // <esc>(s#T   - Typeface, Primary
    protected void PCL_LeftParen_s_T()
    {
      // do nothing
    }

    // <esc>(s#V   - Primary Height
    protected void PCL_LeftParen_s_V()
    {
      // do nothing
    }

    // <esc>)3@    - Select Default Font as Secondary
    protected void PCL_RightParen_3_AtSign()
    {
      // do nothing
    }

    // <esc>)#X    - Secondary Font Selection by ID #
    protected void PCL_RightParen_X()
    {
      // do nothing
    }

    // <esc>)s1P   - Secondary Spacing
    protected void PCL_RightParen_s_P()
    {
      // do nothing
    }

    // <esc>)s#B   - Secondary Stroke Weight
    protected void PCL_RightParen_s_B()
    {
      // do nothing
    }

    // <esc>)s#H   - Secondary Pitch
    protected void PCL_RightParen_s_H()
    {
      // do nothing
    }

    // <esc>)s#S   - Secondary Style
    protected void PCL_RightParen_s_S()
    {
      // do nothing
    }

    // <esc>)s#T   - Typeface, Secondary
    protected void PCL_RightParen_s_T()
    {
      // do nothing
    }

    // <esc>)s#V   - Secondary Height
    protected void PCL_RightParen_s_V()
    {
      // do nothing
    }
  */

  private String                         _currentPCLCommand;

  //private int      _lastMacroID = -1;

  private static HashMap<String, Method> pclMethods;

  static
  {
    pclMethods = new HashMap<>(25);
    Method[] declaredMethods = PCL5_Parser.class.getDeclaredMethods();
    for (int i = 0; i < declaredMethods.length; i++)
    {
      Method method = declaredMethods[i];
      String methodName = method.getName();
      if (methodName.startsWith("PCL_") == true)
      {
        pclMethods.put(methodName, method);
      }
    }
  }
}
