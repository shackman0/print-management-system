package shackelford.floyd.printmanagementsystem.printjobparser;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;

/**
 * <p>Title: PrintJob</p>
 * <p>Description:
 * an object representation of a print job
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJob
{

  public static final int  NOT_FOUND           = -1;

  private static final int SLIDING_BUFFER_SIZE = 100000;

  private static final int MAX_BUFFER_SIZE     = 0;     // always use the sliding buffer mode

  private static final int LOAD_PAD            = 0;     // the number of bytes to pad the left side of the buffer start

  /**
   * Construct a new PrintJob
  */
  public PrintJob(String fileName, String printJobNumber)
  {
    _fileName = fileName;
    _file = new File(_fileName);
    _fileLength = (int) _file.length();

    if (_fileLength > MAX_BUFFER_SIZE)
    {
      _createBufferSize = SLIDING_BUFFER_SIZE;
    }
    else
    {
      _createBufferSize = _fileLength;
    }

    // Create a buffer for the job data
    _fileBuffer = new byte[_createBufferSize];

    _printJobNumber = printJobNumber;

    _properties = new Properties();

    SetProperty(PrintJobPropertiesRow.PROPERTY_PRINT_JOB_NUMBER, _printJobNumber);
    SetProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_BYTES, String.valueOf(_fileLength));

    SetProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES, PrintJobPropertiesRow.VALUE_UNKNOWN);
    SetProperty(PrintJobPropertiesRow.PROPERTY_SIDES, PrintJobPropertiesRow.VALUE_UNKNOWN);
    SetProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE, PrintJobPropertiesRow.VALUE_UNKNOWN);
    SetProperty(PrintJobPropertiesRow.PROPERTY_COLOR, PrintJobPropertiesRow.VALUE_UNKNOWN);
    SetProperty(PrintJobPropertiesRow.PROPERTY_PRINTER, PrintJobPropertiesRow.VALUE_UNKNOWN);
  }

  public String GetFileName()
  {
    return _fileName;
  }

  public File GetFile()
  {
    return _file;
  }

  public int GetFileLength()
  {
    return _fileLength;
  }

  public int GetCurrentOffset()
  {
    return _currentOffset;
  }

  public String GetPrintJobNumber()
  {
    return _printJobNumber;
  }

  public void MoveCurrentOffset(int offsetDelta)
    throws IOException
  {
    SetCurrentOffset(_currentOffset + offsetDelta);
  }

  public void SetCurrentOffset(int newOffset)
    throws IOException
  {
    if (newOffset < 0)
    {
      new Exception("invalid new offset = " + newOffset).printStackTrace();
      newOffset = 0;
    }
    if (newOffset >= _fileLength)
    {
      new Exception("invalid new offset = " + newOffset).printStackTrace();
      newOffset = _fileLength;
    }
    PositionBuffer(newOffset, 0);
    _currentOffset = newOffset;
  }

  public byte GetPrev()
    throws IOException
  {
    _currentOffset--;
    PositionBuffer(_currentOffset, 1);
    return _fileBuffer[_currentOffset - _bufferStart];
  }

  public byte GetNext()
    throws IOException
  {
    PositionBuffer(_currentOffset, 1);
    byte aByte = _fileBuffer[_currentOffset - _bufferStart];
    _currentOffset++;
    return aByte;
  }

  public char GetNextAsChar()
    throws IOException
  {
    PositionBuffer(_currentOffset, 1);
    char aChar = (char) (_fileBuffer[_currentOffset - _bufferStart] & 0x00ff);
    _currentOffset++;
    return aChar;
  }

  public byte[] GetNextBytes(int length)
    throws IOException
  {
    byte[] theBytes = GetByteRange(_currentOffset, _currentOffset + length - 1);
    _currentOffset += length;
    return theBytes;
  }

  /**
   * get the byte being pointed to without advancing the file position
   */
  public byte PeekNext()
    throws IOException
  {
    PositionBuffer(_currentOffset, 1);
    return _fileBuffer[_currentOffset - _bufferStart];
  }

  public char PeekNextAsChar()
    throws IOException
  {
    PositionBuffer(_currentOffset, 1);
    return (char) (_fileBuffer[_currentOffset - _bufferStart] & 0x00ff);
  }

  public boolean MoreBytes()
  {
    return _currentOffset < _fileLength;
  }

  public Properties GetProperties()
  {
    return _properties;
  }

  public String GetProperty(String propertyName)
  {
    if (_properties == null)
    {
      return null;
    }
    return _properties.getProperty(propertyName);
  }

  public void SetProperty(String propertyName, String propertyValue)
  {
    if (_properties == null)
    {
      _properties = new Properties();
    }
    _properties.setProperty(propertyName, propertyValue);
  }

  public byte[] GetBuffer()
  {
    return _fileBuffer;
  }

  public byte GetByteAt(int offset)
    throws IOException
  {
    return GetByteRange(offset, offset)[0];
  }

  public char GetCharAt(int offset)
    throws IOException
  {
    return (char) (GetByteRange(offset, offset)[0] & 0x00ff);
  }

  public byte[] GetByteRange(int fileStartOffset, // inclusive
    int fileEndOffset)
    // inclusive
    throws IOException
  {
    if (fileStartOffset > fileEndOffset)
    {
      return null;
    }
    int length = fileEndOffset - fileStartOffset + 1;
    PositionBuffer(fileStartOffset, length);
    byte[] byteRange = new byte[length];
    int bufferOffset = fileStartOffset - _bufferStart;
    for (int i = 0; i < length; i++)
    {
      byteRange[i] = _fileBuffer[bufferOffset + i];
    }
    return byteRange;
  }

  /**
    Parses the print job. if the file can be parsed, returns true and
    all the properties will be set. If it cannot be parsed, then
    it returns false and no properties will be set.
  */
  public boolean Parse()
  {
    boolean rc;
    try
    {
      _inputFile = new RandomAccessFile(_file, "r");
      PositionBuffer(0, 0);
      rc = AbstractPrintJobLanguageParser.Parse(this);
      _inputFile.close();
      _inputFile = null;
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      return false;
    }
    return rc;
  }

  public int CountPattern(String pattern)
    throws IOException
  {
    return CountPattern(0, _fileLength, pattern.getBytes());
  }

  public int CountPattern(int startingIndex, // inclusive
    int endingIndex, // exclusive
    String pattern)
    throws IOException
  {
    return CountPattern(startingIndex, endingIndex, pattern.getBytes());
  }

  public int CountPattern(int startingIndex, // inclusive
    int endingIndex, // exclusive
    byte[] pattern)
    throws IOException
  {
    int count = 0;
    int currentIndex = startingIndex;
    do
    {
      currentIndex = FindPattern(currentIndex, endingIndex, pattern);
      if (currentIndex != NOT_FOUND)
      {
        currentIndex += pattern.length;
        count++;
      }
    }
    while ((currentIndex < endingIndex) && (currentIndex != NOT_FOUND));
    return count;
  }

  public int FindPattern(int startingIndex, // inclusive
    int endingIndex, // exclusive
    String pattern)
    throws IOException
  {
    return FindPattern(startingIndex, endingIndex, pattern.getBytes());
  }

  public int FindPattern(int startingIndex, // inclusive
    int endingIndex, // exclusive
    byte[] pattern)
    throws IOException
  {
    int compare = -1;
    int i;
    for (i = startingIndex; ((i <= (endingIndex - pattern.length)) && (compare != 0)); i++)
    {
      compare = CompareBytes(i, pattern);
    }
    if (compare != 0)
    {
      return NOT_FOUND;
    }
    return i - 1;
  }

  private void LoadBuffer(int fileOffset)
    throws IOException
  {
    if (fileOffset < 0)
    {
      fileOffset = 0;
    }
    else
      if (fileOffset > _fileLength)
      {
        fileOffset = _fileLength - _fileBuffer.length;
      }

    _inputFile.seek(fileOffset);
    int bytesRead = _inputFile.read(_fileBuffer);
    _bufferStart = fileOffset;
    _bufferEnd = _bufferStart + bytesRead;
  }

  private void PositionBuffer(int fileOffset, int length)
    throws IOException
  {
    if ((fileOffset < _bufferStart) || (fileOffset + length > _bufferEnd))
    {
      LoadBuffer(fileOffset - LOAD_PAD);
      _currentOffset = fileOffset;
    }
  }

  /**
   * = 0 means byte arrays contain the same first "length" of bytes
   * > 0 means a's bytes are "greater than" b's bytes
   * < 0 means a's bytes are "less than" b's bytes
   */
  private int CompareBytes(int fileOffset, byte[] pattern)
    throws IOException
  {
    PositionBuffer(fileOffset, pattern.length);
    int patternIndex = 0;
    for (int bufferIndex = (fileOffset - _bufferStart); bufferIndex < (fileOffset - _bufferStart + pattern.length); bufferIndex++, patternIndex++)
    {
      if (_fileBuffer[bufferIndex] != pattern[patternIndex])
      {
        // the i'th byte in "a" does not match the j'th byte in "b"
        return _fileBuffer[bufferIndex] - pattern[patternIndex];
      }
    }
    // they have the same "length" bytes
    return 0;
  }

  private final String     _fileName;

  private final String     _printJobNumber;

  private File             _file          = null;

  private int              _fileLength    = 0;

  private int              _createBufferSize;

  private RandomAccessFile _inputFile     = null;

  private Properties       _properties    = null;

  private byte[]           _fileBuffer    = null;

  private int              _currentOffset = 0;   // the current position in the file

  private int              _bufferStart   = 0;   // the position in the file corresponding to the start of the buffer

  private int              _bufferEnd     = 0;   // position in the file corresponding to the end of the buffer

  /**
  This is the unit test code. It tests each of the interfaces above.
  You'll need to set the test variables before you run this.
  */
  public static void main(String[] args)
  {
    String fileNameToParse = null;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-fileName") == true)
      {
        fileNameToParse = args[++index].trim();
        System.out.println("Parsing print job \"" + fileNameToParse + "\"");
      }
      else
      {
        System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
      }
    }

    if (fileNameToParse == null)
    {
      System.out.println("ERROR: -fileName was not specified");
      Usage();
      System.exit(1);
    }

    try
    {
      PrintJob printJob = new PrintJob(fileNameToParse, PrintJobPropertiesRow.VALUE_UNKNOWN);

      GregorianCalendar startCalendar = new GregorianCalendar();
      System.out.println("Parse begin @ " + startCalendar.get(Calendar.HOUR_OF_DAY) + ":" + startCalendar.get(Calendar.MINUTE) + ":" + startCalendar.get(Calendar.SECOND) + "."
        + startCalendar.get(Calendar.MILLISECOND));

      if (printJob.Parse() == true)
      {
        GregorianCalendar endCalendar = new GregorianCalendar();
        System.out.println("Parse end @ " + endCalendar.get(Calendar.HOUR_OF_DAY) + ":" + endCalendar.get(Calendar.MINUTE) + ":" + endCalendar.get(Calendar.SECOND) + "."
          + endCalendar.get(Calendar.MILLISECOND));

        float elapsedSeconds = (endCalendar.getTime().getTime() - startCalendar.getTime().getTime()) / 1000f;
        System.out.println("Total elapsed time = " + elapsedSeconds + " seconds");

        printJob.GetProperties().list(System.out);
      }
      else
      {
        System.out.println("Main: cannot parse the print job");
      }

    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      System.exit(-1);
    }

    System.exit(0);
  }

  private static void Usage()
  {
    System.err.println("Usage e.g.: java PrintJob " + "-printJob <print job file>");
  }

}
