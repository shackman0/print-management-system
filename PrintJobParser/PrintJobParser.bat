@echo off

rem This script launches the PrintJobParser as a stand-alone application.
rem You must modify this script when you install the
rem PrintJobParser application before this script will work.

rem usage PrintJobParser "file to parse" [-verbose]

rem notes:
rem 1. be sure to put the "file to parse" in double quotes as shown
rem 2. -verbose puts out a lot of output and it slows
rem    PrintJobParser down a whole lot.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 99continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:99continue

%JAVA%\bin\java -classpath .;%JAVA%\lib;.\PrintJobParser.jar; PrintJob -fileName %1 %2

:end

set JAVA=
