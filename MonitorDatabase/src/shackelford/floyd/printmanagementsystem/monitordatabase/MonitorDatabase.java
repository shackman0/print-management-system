package shackelford.floyd.printmanagementsystem.monitordatabase;

import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;




/**
 * <p>Title: MonitorDatabase</p>
 * <p>Description:
  this class provides interfaces to the Monitor Database.

  Note: When you are done with a ResultSet, you MUST call CloseResultSet(), passing
  in the result set you are done with. this frees up the corresponding sqlStatement[]
  for further use.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorDatabase
  extends AbstractSQLDatabase
{

  private CurrenciesTable         _currenciesTable;
  private InstallationNumberTable _installationNumberTable;
  private InstallationsTable      _installationsTable;
  private MonitorTable            _monitorTable;
  private ReceivablesNumberTable  _receivablesNumberTable;
  private ReceivablesTable        _receivablesTable;

  private static MonitorDatabase  __defaultMonitorDatabase = null;
  public static final String      DATABASE_NAME = "monitor_database";
  public static final String      DATABASE_PORT = "5432";
  public static final String      DATABASE_PROGRAM = "postgresql";

  /**
    this constructor creates a connection to the database.
    be sure to call the done() method when you are done with this instance.
  */

  public MonitorDatabase (
    String aDatabaseHost,    // ipaddr or hostname
    String aDatabaseUserID,
    String aDatabasePassword )
  {
    super (
      DATABASE_PROGRAM,
      aDatabaseHost,
      DATABASE_PORT,
      DATABASE_NAME,
      aDatabaseUserID,
      aDatabasePassword );

    _currenciesTable = new CurrenciesTable(this);
    _tablesMap.put(CurrenciesTable.TABLE_NAME,_currenciesTable);

    _installationNumberTable = new InstallationNumberTable(this);
    _tablesMap.put(InstallationNumberTable.TABLE_NAME,_installationNumberTable);

    _installationsTable = new InstallationsTable(this);
    _tablesMap.put(InstallationsTable.TABLE_NAME,_installationsTable);

    _monitorTable = new MonitorTable(this);
    _tablesMap.put(MonitorTable.TABLE_NAME,_monitorTable);

    _receivablesNumberTable = new ReceivablesNumberTable(this);
    _tablesMap.put(ReceivablesNumberTable.TABLE_NAME,_receivablesNumberTable);

    _receivablesTable = new ReceivablesTable(this);
    _tablesMap.put(ReceivablesTable.TABLE_NAME,_receivablesTable);
  }

  @Override
  public void ClearDefaultDatabase()
  {
    __defaultMonitorDatabase = null;
  }

  @Override
  public AbstractSQLDatabase GetDefaultDatabase()
  {
    return __defaultMonitorDatabase;
  }

  public static void ClearDefaultMonitorDatabase()
  {
    __defaultMonitorDatabase = null;
  }

  public static void SetDefaultMonitorDatabase(MonitorDatabase defaultDatabase)
  {
    __defaultMonitorDatabase = defaultDatabase;
  }

  public static MonitorDatabase GetDefaultMonitorDatabase()
  {
    return __defaultMonitorDatabase;
  }

  public CurrenciesTable GetCurrenciesTable()
  {
    return _currenciesTable;
  }

  public InstallationNumberTable GetInstallationNumberTable()
  {
    return _installationNumberTable;
  }

  public InstallationsTable GetInstallationsTable()
  {
    return _installationsTable;
  }

  public MonitorTable GetMonitorTable()
  {
    return _monitorTable;
  }

  public ReceivablesNumberTable GetReceivablesNumberTable()
  {
    return _receivablesNumberTable;
  }

  public ReceivablesTable GetReceivablesTable()
  {
    return _receivablesTable;
  }

}