package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: ReceivablesNumberRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivablesNumberRow
  extends AbstractSQL1Row
{

  private Integer                 _sql_last_receivables_number;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_last_receivables_number() };

  private static String[]         __keyColumnNames = { };

  public ReceivablesNumberRow()
  {
    super();
  }

  public ReceivablesNumberRow(ReceivablesNumberTable table)
  {
    super(table);
  }

  public ReceivablesNumberRow( ReceivablesNumberTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public ReceivablesNumberRow( ReceivablesNumberTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public Integer Get_last_receivables_number() { return new Integer(_sql_last_receivables_number.intValue()); }
  public void Set_last_receivables_number(Integer value) { _sql_last_receivables_number = new Integer(value.intValue()); }
  public void Set_last_receivables_number(String value) { _sql_last_receivables_number = new Integer(value); }
  public void Set_last_receivables_number(int value) { _sql_last_receivables_number = new Integer(value); }
  public static String Name_last_receivables_number() { return "last_receivables_number"; }


  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(ReceivablesNumberRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}