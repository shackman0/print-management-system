package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: ReceivablesTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivablesTable
  extends AbstractSQL3Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  private static final DecimalFormat     __formatter = new DecimalFormat("#,##0.00");

  private static final String            NEWLINE     = "\n";

  private static final String            TAB         = "\t";

  public static final String             TABLE_NAME  = "receivables_table";

  public ReceivablesTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, ReceivablesRow.class);
  }

  public ReceivablesStatistics GenerateReceivables(_GregorianCalendar startCal, _GregorianCalendar endCal, String installationID)
  {
    String predicate = null;

    if (installationID != null)
    {
      predicate = InstallationsRow.Name_installation_id() + " = '" + installationID.trim() + "'";
    }

    ReceivablesStatistics globalStats = new ReceivablesStatistics();
    try
    ( SQLStatement sqlStatement = GetMonitorDatabase().GetInstallationsTable().GetRowsAsResultSet(null, // from
        predicate, InstallationsRow.Name_installation_id(), // order by
        AbstractSQL1Table.NO_LIMIT);
      ResultSet resultSet = sqlStatement.GetResultSet();
    )
    {
      while (resultSet.next())
      {
        InstallationsRow installationsRow = new InstallationsRow(MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable(), resultSet);

        ReceivablesStatistics localStats = GenerateReceivablesForInstallation(installationsRow, startCal, endCal);

        globalStats._numberProcessed += localStats._numberProcessed;
        globalStats._numberSkipped += localStats._numberSkipped;
        globalStats._totalPerPageCharges += localStats._totalPerPageCharges;
        globalStats._totalPerPrintJobCharges += localStats._totalPerPrintJobCharges;
        globalStats._totalPercentageSalesCharges += localStats._totalPercentageSalesCharges;
      }
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
    }

    if (GlobalAttributes.__verbose == true)
    {
      System.out.println("Total Installations Processed = " + globalStats._numberProcessed + NEWLINE + "Total Installations Skipped = " + globalStats._numberSkipped + NEWLINE
        + "Total Per Page Charges = " + __formatter.format(globalStats._totalPerPageCharges) + NEWLINE + "Total Per Print Job Charges = " + __formatter.format(globalStats._totalPerPrintJobCharges)
        + NEWLINE + "Total Percentage Sales Charges = " + __formatter.format(globalStats._totalPercentageSalesCharges));
    }

    return globalStats;
  }

  public ReceivablesStatistics GenerateReceivablesForInstallation(InstallationsRow installationsRow, _GregorianCalendar startCal, _GregorianCalendar endCal)
  {
    ReceivablesStatistics stats = new ReceivablesStatistics();

    if (GlobalAttributes.__verbose == true)
    {
      System.out.println("Processing " + installationsRow.Get_installation_id() + " " + installationsRow.Get_installation_name());
    }

    if (installationsRow.StatusIsEnabled() == true)
    {
      if ((installationsRow.Get_per_page_charge_enabled().booleanValue() == true) || (installationsRow.Get_per_print_job_charge_enabled().booleanValue() == true)
        || (installationsRow.Get_total_sales_percentage_enabled().booleanValue() == true))
      {

        try
        {
          InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
            installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
          InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
        }
        catch (Exception excp)
        {
          excp.printStackTrace();
          if (GlobalAttributes.__verbose == true)
          {
            System.out.println(NEWLINE + TAB + "Skipping due to error.");
          }
          stats._numberSkipped++;
          return stats;
        }

        stats._numberProcessed++;

        InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();

        ReceivablesRow receivablesRow = new ReceivablesRow(MonitorDatabase.GetDefaultMonitorDatabase().GetReceivablesTable());
        receivablesRow.SetToNextReceivablesNumber();
        receivablesRow.Set_installation_id(installationRow.Get_installation_id());
        receivablesRow.Set_billing_period_start(startCal);
        receivablesRow.Set_billing_period_end(endCal);

        if (installationsRow.Get_per_page_charge_enabled().booleanValue() == true)
        {
          int numberOfPages = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable().GetNumberOfImpressionsForPeriod(startCal, endCal);
          receivablesRow.Set_per_page_charge(numberOfPages * installationsRow.Get_per_page_charge().doubleValue());
          stats._totalPerPageCharges += receivablesRow.Get_per_page_charge().doubleValue();

          if (GlobalAttributes.__verbose == true)
          {
            System.out.println(TAB + String.valueOf(numberOfPages) + " pages at " + __formatter.format(installationsRow.Get_per_page_charge().doubleValue()) + " per page = "
              + __formatter.format(stats._totalPerPageCharges));
          }
        }

        if (installationsRow.Get_per_print_job_charge_enabled().booleanValue() == true)
        {
          int numberOfPrintJobs = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable().GetNumberOfPrintJobsForPeriod(startCal, endCal);
          receivablesRow.Set_per_print_job_charge(numberOfPrintJobs * installationsRow.Get_per_print_job_charge().doubleValue());
          stats._totalPerPrintJobCharges += receivablesRow.Get_per_print_job_charge().doubleValue();

          if (GlobalAttributes.__verbose == true)
          {
            System.out.println(TAB + String.valueOf(numberOfPrintJobs) + " print jobs at " + __formatter.format(installationsRow.Get_per_print_job_charge().doubleValue()) + " per print job = "
              + __formatter.format(stats._totalPerPrintJobCharges));
          }
        }

        if (installationsRow.Get_total_sales_percentage_enabled().booleanValue() == true)
        {
          double totalSales = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable().GetTotalSalesForPeriod(startCal, endCal);
          receivablesRow.Set_percentage_of_revenues_charge(totalSales * (installationsRow.Get_total_sales_percentage().doubleValue() / 100d));
          stats._totalPercentageSalesCharges += receivablesRow.Get_percentage_of_revenues_charge().doubleValue();

          if (GlobalAttributes.__verbose == true)
          {
            System.out
              .println(TAB + installationsRow.Get_total_sales_percentage() + "% of sales of " + __formatter.format(totalSales) + " = " + __formatter.format(stats._totalPercentageSalesCharges));
          }
        }

        receivablesRow.Insert();

        InstallationDatabase.GetDefaultInstallationDatabase().Done();

      }
      else
      {
        if (GlobalAttributes.__verbose == true)
        {
          System.out.println(TAB + "Skipping. There are no charges enabled for this installation.");
        }
        stats._numberSkipped++;
      }
    }
    else
    {
      if (GlobalAttributes.__verbose == true)
      {
        System.out.println(TAB + "Skipping. Status = " + installationsRow.Get_status());
      }
      stats._numberSkipped++;
    }

    return stats;
  }

  private MonitorDatabase GetMonitorDatabase()
  {
    return (MonitorDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = ReceivablesRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }

}
