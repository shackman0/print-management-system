package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;



/**
 * <p>Title: InstallationsTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationsTable
  extends AbstractSQL4Table
{

  private static HashMap<String,String>          __columnToViewNamesMap;
  private static HashMap<String,String>          __viewToColumnNamesMap;
  private static Resources        __resources;

  public static final String TABLE_NAME = "installations_table";

  public InstallationsTable(AbstractSQLDatabase sqlDatabase)
  {
    super (sqlDatabase,TABLE_NAME,InstallationsRow.class);
  }

  public InstallationsRow GetInstallationsRow(Integer installationID)
  {
    String predicate = InstallationsRow.Name_installation_id() + " = '" + installationID.toString() + "'";
    return (InstallationsRow)GetFirstRow(predicate,null);
  }

  public String[] GetInstallationIDsAsStringArray()
  {
    Integer[] integers = GetInstallationIDs();
    String[] strings = new String[integers.length];
    for (int indx = 0; indx < strings.length; indx++)
    {
      strings[indx] = integers[indx].toString();
    }
    return strings;
  }

  public Integer[] GetInstallationIDs ()
  {
    return (Integer[])GetDistinctColumnValues(InstallationsRow.Name_installation_id(),null);
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(InstallationsRow.Name_installation_name()) == true)
    {
      return 150;
    }
    if (columnName.equals(InstallationsRow.Name_country()) == true)
    {
      return 60;
    }
    if (columnName.equals(InstallationsRow.Name_currency()) == true)
    {
      return 80;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String,String> GetColumnToViewNamesMap ()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String,String> GetViewToColumnNamesMap ()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = InstallationsRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames ( columnNames, __resources );

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames,viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames,columnNames);
  }

}