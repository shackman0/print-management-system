package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;



/**
 * <p>Title: ReceivablesNumberTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivablesNumberTable
  extends AbstractSQL1Table
{

  private static HashMap<String,String>          __columnToViewNamesMap;
  private static HashMap<String,String>          __viewToColumnNamesMap;
  private static Resources        __resources;

  public static final String      TABLE_NAME = "receivables_number_table";


  public ReceivablesNumberTable(AbstractSQLDatabase sqlDatabase)
  {
    super (sqlDatabase,TABLE_NAME,ReceivablesNumberRow.class);
  }

  public int GetNextReceivablesNumber()
  {
    int nextReceivablesNumber = 0;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.ExecuteUpdate("begin transaction");
      String sqlString = "select (" + ReceivablesNumberRow.Name_last_receivables_number() + " + 1) from " + GetTableName();
      try
      (ResultSet resultSet = sqlStatement.ExecuteQuery(sqlString);)
      {
        resultSet.next();
        nextReceivablesNumber = resultSet.getInt(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        MessageDialog.ShowErrorMessageDialog (
          null,
          excp,
          "resultSet.next()" );
      }
      sqlStatement.ExecuteUpdate("delete from receivables_number_table");
      sqlStatement.ExecuteUpdate("insert into " + GetTableName() + " (" + ReceivablesNumberRow.Name_last_receivables_number() + ") values ('" + String.valueOf(nextReceivablesNumber) + "')");
      sqlStatement.ExecuteUpdate("commit transaction");
    }
    
    return nextReceivablesNumber;

  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String,String> GetColumnToViewNamesMap ()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String,String> GetViewToColumnNamesMap ()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = ReceivablesNumberRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames ( columnNames, __resources );

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames,viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames,columnNames);
  }

}