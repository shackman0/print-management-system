package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: CurrenciesTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class CurrenciesTable
  extends AbstractSQL3Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "currencies_table";

  public CurrenciesTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, CurrenciesRow.class);
  }

  /**
    this operation returns a single row from the the currencies table
  */
  public CurrenciesRow GetCurrenciesRowUsingName(String currencyName)
  {
    String predicate = CurrenciesRow.Name_name() + " = '" + currencyName.trim() + "'";
    return (CurrenciesRow) (GetFirstRow(predicate, null));
  }

  /**
    this operation returns the next currency ID
  */
  public String GetNextCurrencyID()
  {
    String nextCurrencyNumber = "";
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.ExecuteUpdate("begin transaction");
      String sqlString = "select (last_currency_number + 1) from currencies_number_table";
      try
      (ResultSet resultSet = sqlStatement.ExecuteQuery(sqlString);)
      {
        resultSet.next(); // position us to the first row returned from the query
        nextCurrencyNumber = resultSet.getString(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
      }
  
      sqlStatement.ExecuteUpdate("delete from currencies_number_table");
      sqlStatement.ExecuteUpdate("insert into currencies_number_table (last_currency_number) values (" + nextCurrencyNumber + ")");
      sqlStatement.ExecuteUpdate("commit transaction");
    }
    
    return nextCurrencyNumber;

  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(CurrenciesRow.Name_name()) == true)
    {
      return 120;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String,String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String,String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = CurrenciesRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }

}
