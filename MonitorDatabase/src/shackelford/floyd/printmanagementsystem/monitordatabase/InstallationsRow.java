package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;


/**
 * <p>Title: InstallationsRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationsRow
  extends AbstractSQL4Row
{

  private Integer              _sql_installation_id;

  private String               _sql_installation_name;

  private String               _sql_country;

  private String               _sql_currency;

  private String               _sql_installation_server_address;

  private String               _sql_installation_server_user_id;

  private String               _sql_installation_server_password;

  private String               _sql_last_receivables_generated_dts;

  private _GregorianCalendar   _sql_license_expiration_date;

  private Integer              _sql_expiration_date_increment;

  private Boolean              _sql_up_front_fee_enabled;

  private Double               _sql_up_front_fee;

  private Boolean              _sql_monthly_fee_enabled;

  private Double               _sql_monthly_fee;

  private Boolean              _sql_annual_fee_enabled;

  private Double               _sql_annual_fee;

  private Boolean              _sql_per_page_charge_enabled;

  private Double               _sql_per_page_charge;

  private Boolean              _sql_per_print_job_charge_enabled;

  private Double               _sql_per_print_job_charge;

  private Boolean              _sql_total_sales_percentage_enabled;

  private Double               _sql_total_sales_percentage;

  private static DecimalFormat __formatter      = new DecimalFormat("#,##0.00");

  private static final Integer INTEGER_30       = new Integer(30);

  private static final String  DUMMY_DATE       = "2000-01-01";

  private static Field[]       __sqlFields;

  private static Field[]       __sqlKeyFields;

  private static String[]      __columnNames    = { Name_installation_id(), Name_installation_name(), Name_country(), Name_currency(), Name_installation_server_address(),
    Name_installation_server_user_id(), Name_installation_server_password(), Name_last_receivables_generated_dts(), Name_license_expiration_date(), Name_expiration_date_increment(),
    Name_up_front_fee_enabled(), Name_up_front_fee(), Name_monthly_fee_enabled(), Name_monthly_fee(), Name_annual_fee_enabled(), Name_annual_fee(), Name_per_page_charge_enabled(),
    Name_per_page_charge(), Name_per_print_job_charge_enabled(), Name_per_print_job_charge(), Name_total_sales_percentage_enabled(), Name_total_sales_percentage(),

    Name_status(), Name_updated_dts(), Name_created_dts() };

  private static String[]      __keyColumnNames = { Name_installation_id() };

  public InstallationsRow()
  {
    super();
  }

  public InstallationsRow(InstallationsTable table)
  {
    super(table);
  }

  public InstallationsRow(InstallationsTable table, ResultSet resultSet)
  {
    super(table, resultSet);
  }

  public InstallationsRow(InstallationsTable table, AbstractSQL1Row initRow)
  {
    super(table, initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    _sql_country = Resources.UNITED_STATES;
    _sql_last_receivables_generated_dts = DUMMY_DATE;
    _sql_license_expiration_date.add(Calendar.YEAR, 1);
    _sql_expiration_date_increment = INTEGER_30;
  }

  public void SetNewInstallationID()
  {
    Set_installation_id(GetMonitorDatabase().GetInstallationNumberTable().GetNextInstallationID());
  }

  private InstallationsTable GetInstallationsTable()
  {
    return (InstallationsTable) _sqlTable;
  }

  private MonitorDatabase GetMonitorDatabase()
  {
    return (MonitorDatabase) (GetInstallationsTable().GetSQLDatabase());
  }

  public Integer Get_installation_id()
  {
    return new Integer(_sql_installation_id.intValue());
  }

  public void Set_installation_id(Integer value)
  {
    _sql_installation_id = new Integer(value.intValue());
  }

  public void Set_installation_id(String value)
  {
    _sql_installation_id = new Integer(value);
  }

  public void Set_installation_id(int value)
  {
    _sql_installation_id = new Integer(value);
  }

  public static String Name_installation_id()
  {
    return "installation_id";
  }

  public String Get_installation_name()
  {
    return new String(_sql_installation_name);
  }

  public void Set_installation_name(String value)
  {
    _sql_installation_name = new String(value);
  }

  public static String Name_installation_name()
  {
    return "installation_name";
  }

  public String Get_country()
  {
    return new String(_sql_country);
  }

  public void Set_country(String value)
  {
    _sql_country = new String(value);
  }

  public static String Name_country()
  {
    return "country";
  }

  public String Get_currency()
  {
    return new String(_sql_currency);
  }

  public void Set_currency(String value)
  {
    _sql_currency = new String(value);
  }

  public static String Name_currency()
  {
    return "currency";
  }

  public String Get_installation_server_address()
  {
    return new String(_sql_installation_server_address);
  }

  public void Set_installation_server_address(String value)
  {
    _sql_installation_server_address = new String(value);
  }

  public static String Name_installation_server_address()
  {
    return "installation_server_address";
  }

  public String Get_installation_server_user_id()
  {
    return new String(_sql_installation_server_user_id);
  }

  public void Set_installation_server_user_id(String value)
  {
    _sql_installation_server_user_id = new String(value);
  }

  public static String Name_installation_server_user_id()
  {
    return "installation_server_user_id";
  }

  public String Get_installation_server_password()
  {
    return new String(_sql_installation_server_password);
  }

  public void Set_installation_server_password(String value)
  {
    _sql_installation_server_password = new String(value);
  }

  public static String Name_installation_server_password()
  {
    return "installation_server_password";
  }

  public String Get_last_receivables_generated_dts()
  {
    return new String(_sql_last_receivables_generated_dts);
  }

  public void Set_last_receivables_generated_dts(String value)
  {
    _sql_last_receivables_generated_dts = new String(value);
  }

  public static String Name_last_receivables_generated_dts()
  {
    return "last_receivables_generated_dts";
  }

  public _GregorianCalendar Get_license_expiration_date()
  {
    return new _GregorianCalendar(_sql_license_expiration_date);
  }

  public void Set_license_expiration_date(_GregorianCalendar value)
  {
    _sql_license_expiration_date = new _GregorianCalendar(value);
  }

  public static String Name_license_expiration_date()
  {
    return "license_expiration_date";
  }

  public Integer Get_expiration_date_increment()
  {
    return new Integer(_sql_expiration_date_increment.intValue());
  }

  public void Set_expiration_date_increment(Integer value)
  {
    _sql_expiration_date_increment = new Integer(value.intValue());
  }

  public void Set_expiration_date_increment(String value)
  {
    _sql_expiration_date_increment = new Integer(value);
  }

  public void Set_expiration_date_increment(int value)
  {
    _sql_expiration_date_increment = new Integer(value);
  }

  public static String Name_expiration_date_increment()
  {
    return "expiration_date_increment";
  }

  public Boolean Get_up_front_fee_enabled()
  {
    return new Boolean(_sql_up_front_fee_enabled.booleanValue());
  }

  public void Set_up_front_fee_enabled(Boolean value)
  {
    _sql_up_front_fee_enabled = new Boolean(value.booleanValue());
  }

  public void Set_up_front_fee_enabled(String value)
  {
    _sql_up_front_fee_enabled = new Boolean(value);
  }

  public void Set_up_front_fee_enabled(boolean value)
  {
    _sql_up_front_fee_enabled = new Boolean(value);
  }

  public static String Name_up_front_fee_enabled()
  {
    return "up_front_fee_enabled";
  }

  public Double Get_up_front_fee()
  {
    return new Double(_sql_up_front_fee.doubleValue());
  }

  public String GetFormatted_up_front_fee()
  {
    return __formatter.format(_sql_up_front_fee);
  }

  public void Set_up_front_fee(Double value)
  {
    _sql_up_front_fee = new Double(value.doubleValue());
  }

  public void Set_up_front_fee(String value)
  {
    _sql_up_front_fee = new Double(value);
  }

  public void Set_up_front_fee(double value)
  {
    _sql_up_front_fee = new Double(value);
  }

  public static String Name_up_front_fee()
  {
    return "up_front_fee";
  }

  public Boolean Get_monthly_fee_enabled()
  {
    return new Boolean(_sql_monthly_fee_enabled.booleanValue());
  }

  public void Set_monthly_fee_enabled(Boolean value)
  {
    _sql_monthly_fee_enabled = new Boolean(value.booleanValue());
  }

  public void Set_monthly_fee_enabled(String value)
  {
    _sql_monthly_fee_enabled = new Boolean(value);
  }

  public void Set_monthly_fee_enabled(boolean value)
  {
    _sql_monthly_fee_enabled = new Boolean(value);
  }

  public static String Name_monthly_fee_enabled()
  {
    return "monthly_fee_enabled";
  }

  public Double Get_monthly_fee()
  {
    return new Double(_sql_monthly_fee.doubleValue());
  }

  public String GetFormatted_monthly_fee()
  {
    return __formatter.format(_sql_monthly_fee);
  }

  public void Set_monthly_fee(Double value)
  {
    _sql_monthly_fee = new Double(value.doubleValue());
  }

  public void Set_monthly_fee(String value)
  {
    _sql_monthly_fee = new Double(value);
  }

  public void Set_monthly_fee(double value)
  {
    _sql_monthly_fee = new Double(value);
  }

  public static String Name_monthly_fee()
  {
    return "monthly_fee";
  }

  public Boolean Get_annual_fee_enabled()
  {
    return new Boolean(_sql_annual_fee_enabled.booleanValue());
  }

  public void Set_annual_fee_enabled(Boolean value)
  {
    _sql_annual_fee_enabled = new Boolean(value.booleanValue());
  }

  public void Set_annual_fee_enabled(String value)
  {
    _sql_annual_fee_enabled = new Boolean(value);
  }

  public void Set_annual_fee_enabled(boolean value)
  {
    _sql_annual_fee_enabled = new Boolean(value);
  }

  public static String Name_annual_fee_enabled()
  {
    return "annual_fee_enabled";
  }

  public Double Get_annual_fee()
  {
    return new Double(_sql_annual_fee.doubleValue());
  }

  public String GetFormatted_annual_fee()
  {
    return __formatter.format(_sql_annual_fee);
  }

  public void Set_annual_fee(Double value)
  {
    _sql_annual_fee = new Double(value.doubleValue());
  }

  public void Set_annual_fee(String value)
  {
    _sql_annual_fee = new Double(value);
  }

  public void Set_annual_fee(double value)
  {
    _sql_annual_fee = new Double(value);
  }

  public static String Name_annual_fee()
  {
    return "annual_fee";
  }

  public Boolean Get_per_page_charge_enabled()
  {
    return new Boolean(_sql_per_page_charge_enabled.booleanValue());
  }

  public void Set_per_page_charge_enabled(Boolean value)
  {
    _sql_per_page_charge_enabled = new Boolean(value.booleanValue());
  }

  public void Set_per_page_charge_enabled(String value)
  {
    _sql_per_page_charge_enabled = new Boolean(value);
  }

  public void Set_per_page_charge_enabled(boolean value)
  {
    _sql_per_page_charge_enabled = new Boolean(value);
  }

  public static String Name_per_page_charge_enabled()
  {
    return "per_page_charge_enabled";
  }

  public Double Get_per_page_charge()
  {
    return new Double(_sql_per_page_charge.doubleValue());
  }

  public String GetFormatted_per_page_charge()
  {
    return __formatter.format(_sql_per_page_charge);
  }

  public void Set_per_page_charge(Double value)
  {
    _sql_per_page_charge = new Double(value.doubleValue());
  }

  public void Set_per_page_charge(String value)
  {
    _sql_per_page_charge = new Double(value);
  }

  public void Set_per_page_charge(double value)
  {
    _sql_per_page_charge = new Double(value);
  }

  public static String Name_per_page_charge()
  {
    return "per_page_charge";
  }

  public Boolean Get_per_print_job_charge_enabled()
  {
    return new Boolean(_sql_per_print_job_charge_enabled.booleanValue());
  }

  public void Set_per_print_job_charge_enabled(Boolean value)
  {
    _sql_per_print_job_charge_enabled = new Boolean(value.booleanValue());
  }

  public void Set_per_print_job_charge_enabled(String value)
  {
    _sql_per_print_job_charge_enabled = new Boolean(value);
  }

  public void Set_per_print_job_charge_enabled(boolean value)
  {
    _sql_per_print_job_charge_enabled = new Boolean(value);
  }

  public static String Name_per_print_job_charge_enabled()
  {
    return "per_print_job_charge_enabled";
  }

  public Double Get_per_print_job_charge()
  {
    return new Double(_sql_per_print_job_charge.doubleValue());
  }

  public String GetFormatted_per_print_job_charge()
  {
    return __formatter.format(_sql_per_print_job_charge);
  }

  public void Set_per_print_job_charge(Double value)
  {
    _sql_per_print_job_charge = new Double(value.doubleValue());
  }

  public void Set_per_print_job_charge(String value)
  {
    _sql_per_print_job_charge = new Double(value);
  }

  public void Set_per_print_job_charge(double value)
  {
    _sql_per_print_job_charge = new Double(value);
  }

  public static String Name_per_print_job_charge()
  {
    return "per_print_job_charge";
  }

  public Boolean Get_total_sales_percentage_enabled()
  {
    return new Boolean(_sql_total_sales_percentage_enabled.booleanValue());
  }

  public void Set_total_sales_percentage_enabled(Boolean value)
  {
    _sql_total_sales_percentage_enabled = new Boolean(value.booleanValue());
  }

  public void Set_total_sales_percentage_enabled(String value)
  {
    _sql_total_sales_percentage_enabled = new Boolean(value);
  }

  public void Set_total_sales_percentage_enabled(boolean value)
  {
    _sql_total_sales_percentage_enabled = new Boolean(value);
  }

  public static String Name_total_sales_percentage_enabled()
  {
    return "total_sales_percentage_enabled";
  }

  public Double Get_total_sales_percentage()
  {
    return new Double(_sql_total_sales_percentage.doubleValue());
  }

  public String GetFormatted_total_sales_percentage()
  {
    return __formatter.format(_sql_total_sales_percentage);
  }

  public void Set_total_sales_percentage(Double value)
  {
    _sql_total_sales_percentage = new Double(value.doubleValue());
  }

  public void Set_total_sales_percentage(String value)
  {
    _sql_total_sales_percentage = new Double(value);
  }

  public void Set_total_sales_percentage(double value)
  {
    _sql_total_sales_percentage = new Double(value);
  }

  public static String Name_total_sales_percentage()
  {
    return "total_sales_percentage";
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(InstallationsRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields, __keyColumnNames);
  }

}
