package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Row;


/**
 * <p>Title: MonitorRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorRow
  extends AbstractSQL3Row
{

  public String                   _sql_passphrase;
  public String                   _sql_language;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_passphrase(),
                                    Name_language(),

                                    Name_updated_dts(),
                                    Name_created_dts() };

  private static String[]         __keyColumnNames = { };

  public MonitorRow()
  {
    super();
  }

  public MonitorRow(MonitorTable table)
  {
    super(table);
  }

  public MonitorRow( CurrenciesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public MonitorRow( CurrenciesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public String Get_passphrase() { return new String(_sql_passphrase); }
  public void Set_passphrase(String value) { _sql_passphrase = new String(value); }
  public static String Name_passphrase() { return "passphrase"; }

  public String Get_language() { return new String(_sql_language); }
  public void Set_language(String value) { _sql_language = new String(value); }
  public static String Name_language() { return "language"; }


  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(MonitorRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}