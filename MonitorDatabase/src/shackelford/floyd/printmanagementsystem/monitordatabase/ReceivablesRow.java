package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Row;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;


/**
 * <p>Title: ReceivablesRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivablesRow
  extends AbstractSQL3Row
{

  private Integer                 _sql_receivables_number;
  private Integer                 _sql_installation_id;
  private _GregorianCalendar      _sql_billing_period_start;
  private _GregorianCalendar      _sql_billing_period_end;
  private Double                  _sql_per_page_charge;
  private Double                  _sql_per_print_job_charge;
  private Double                  _sql_percentage_of_revenues_charge;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.000");

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_receivables_number(),
                                    Name_installation_id(),
                                    Name_billing_period_start(),
                                    Name_billing_period_end(),
                                    Name_per_page_charge(),
                                    Name_per_print_job_charge(),
                                    Name_percentage_of_revenues_charge(),

                                    Name_updated_dts(),
                                    Name_created_dts() };

  private static String[]         __keyColumnNames = { Name_receivables_number() };

  public ReceivablesRow()
  {
    super();
  }

  public ReceivablesRow(ReceivablesTable table)
  {
    super(table);
  }

  public ReceivablesRow( ReceivablesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public ReceivablesRow( ReceivablesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public void SetToNextReceivablesNumber()
  {
    Set_receivables_number(GetMonitorDatabase().GetReceivablesNumberTable().GetNextReceivablesNumber());
  }

  public Integer Get_receivables_number() { return new Integer(_sql_receivables_number.intValue()); }
  public void Set_receivables_number(Integer value) { _sql_receivables_number = new Integer(value.intValue()); }
  public void Set_receivables_number(String value) { _sql_receivables_number = new Integer(value); }
  public void Set_receivables_number(int value) { _sql_receivables_number = new Integer(value); }
  public static String Name_receivables_number() { return "receivables_number"; }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public _GregorianCalendar Get_billing_period_start() { return new _GregorianCalendar(_sql_billing_period_start); }
  public void Set_billing_period_start(_GregorianCalendar value) { _sql_billing_period_start = new _GregorianCalendar(value); }
  public static String Name_billing_period_start() { return "billing_period_start"; }

  public _GregorianCalendar Get_billing_period_end() { return new _GregorianCalendar(_sql_billing_period_end); }
  public void Set_billing_period_end(_GregorianCalendar value) { _sql_billing_period_end = new _GregorianCalendar(value); }
  public static String Name_billing_period_end() { return "billing_period_end"; }

  public Double Get_per_page_charge() { return new Double(_sql_per_page_charge.doubleValue()); }
  public String GetFormatted_per_page_charge() { return __formatter.format(_sql_per_page_charge); }
  public void Set_per_page_charge(Double value) { _sql_per_page_charge = new Double(value.doubleValue()); }
  public void Set_per_page_charge(String value) { _sql_per_page_charge = new Double(value); }
  public void Set_per_page_charge(double value) { _sql_per_page_charge = new Double(value); }
  public static String Name_per_page_charge() { return "per_page_charge"; }

  public Double Get_per_print_job_charge() { return new Double(_sql_per_print_job_charge.doubleValue()); }
  public String GetFormatted_per_print_job_charge() { return __formatter.format(_sql_per_print_job_charge); }
  public void Set_per_print_job_charge(Double value) { _sql_per_print_job_charge = new Double(value.doubleValue()); }
  public void Set_per_print_job_charge(String value) { _sql_per_print_job_charge = new Double(value); }
  public void Set_per_print_job_charge(double value) { _sql_per_print_job_charge = new Double(value); }
  public static String Name_per_print_job_charge() { return "per_print_job_charge"; }

  public Double Get_percentage_of_revenues_charge() { return new Double(_sql_percentage_of_revenues_charge.doubleValue()); }
  public String GetFormatted_percentage_of_revenues_charge() { return __formatter.format(_sql_percentage_of_revenues_charge); }
  public void Set_percentage_of_revenues_charge(Double value) { _sql_percentage_of_revenues_charge = new Double(value.doubleValue()); }
  public void Set_percentage_of_revenues_charge(String value) { _sql_percentage_of_revenues_charge = new Double(value); }
  public void Set_percentage_of_revenues_charge(double value) { _sql_percentage_of_revenues_charge = new Double(value); }
  public static String Name_percentage_of_revenues_charge() { return "percentage_of_revenues_charge"; }


  private MonitorDatabase GetMonitorDatabase()
  {
    return (MonitorDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(ReceivablesRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}