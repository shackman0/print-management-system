package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: InstallationNumberRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationNumberRow
  extends AbstractSQL1Row
{

  private Integer                 _sql_last_installation_number;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_last_installation_number() };

  private static String[]         __keyColumnNames = { };

  public InstallationNumberRow()
  {
    super();
  }

  public InstallationNumberRow(InstallationNumberTable table)
  {
    super(table);
  }

  public InstallationNumberRow( InstallationNumberTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public InstallationNumberRow( InstallationNumberTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    _sql_last_installation_number = new Integer(10000);
  }

  public Integer Get_last_installation_number() { return new Integer(_sql_last_installation_number.intValue()); }
  public void Set_last_installation_number(Integer value) { _sql_last_installation_number = new Integer(value.intValue()); }
  public void Set_last_installation_number(String value) { _sql_last_installation_number = new Integer(value); }
  public void Set_last_installation_number(int value) { _sql_last_installation_number = new Integer(value); }
  public static String Name_last_installation_number() { return "last_installation_number"; }


  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(InstallationNumberRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}