package shackelford.floyd.printmanagementsystem.monitordatabase;



/**
 * <p>Title: ReceivablesStatistics</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivablesStatistics
{

  public int                      _numberProcessed = 0;
  public int                      _numberSkipped = 0;
  public double                   _totalPerPageCharges = 0d;
  public double                   _totalPerPrintJobCharges = 0d;
  public double                   _totalPercentageSalesCharges = 0d;

}