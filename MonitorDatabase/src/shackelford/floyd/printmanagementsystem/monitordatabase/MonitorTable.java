package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: MonitorTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorTable
  extends AbstractSQL3Table
{

  private static MonitorRow              __cachedMonitorRow     = null;

  private static HashMap<String, String> __columnToViewNamesMap = null;

  private static HashMap<String, String> __viewToColumnNamesMap = null;

  private static Resources               __resources            = null;

  public static final String             TABLE_NAME             = "monitor_table";

  public MonitorTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, MonitorRow.class);
  }

  public MonitorRow GetMonitorRow()
  {
    return (MonitorRow) (GetFirstRow(null, null));
  }

  public static MonitorRow GetCachedMonitorRow()
  {
    if ((__cachedMonitorRow == null) || (__cachedMonitorRow.GetSQLTable().GetSQLDatabase() != MonitorDatabase.GetDefaultMonitorDatabase()))
    {
      __cachedMonitorRow = (MonitorRow) (MonitorDatabase.GetDefaultMonitorDatabase().GetMonitorTable().GetFirstRow(null, null));
    }
    return __cachedMonitorRow;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = MonitorRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }

}
