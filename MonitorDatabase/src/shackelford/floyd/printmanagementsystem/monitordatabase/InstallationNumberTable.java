package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;



/**
 * <p>Title: InstallationNumberTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationNumberTable
  extends AbstractSQL1Table
{

  private static HashMap<String,String>          __columnToViewNamesMap;
  private static HashMap<String,String>          __viewToColumnNamesMap;
  private static Resources        __resources;

  public static final String      TABLE_NAME = "installation_number_table";


  public InstallationNumberTable(AbstractSQLDatabase sqlDatabase)
  {
    super (sqlDatabase,TABLE_NAME,InstallationNumberRow.class);
  }

  /**
    this operation returns the next installation ID
  */
  public Integer GetNextInstallationID ()
  {

    String nextInstallationNumber = "00000";
    String nextInstallationID = null;

    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.ExecuteUpdate("begin transaction");
      String sqlString =
        "select (last_installation_number + 1) from installation_number_table";
      try
      (ResultSet resultSet = sqlStatement.ExecuteQuery (sqlString);)
      {
        resultSet.next();    // position us to the first row returned from the query
        nextInstallationNumber = resultSet.getString(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
      }
      sqlStatement.ExecuteUpdate("delete from installation_number_table");
      sqlStatement.ExecuteUpdate("insert into installation_number_table (last_installation_number) values (" + nextInstallationNumber + ")");
      sqlStatement.ExecuteUpdate("commit transaction");
  
      // calculate the sum of the digits
      int sum = 0;
      for (int i=0; i < nextInstallationNumber.length(); i++)
      {
        //sum += (new Integer(new String(new char[] {nextInstallationNumber.charAt(i)}))).intValue();
        sum += (nextInstallationNumber.charAt(i));
      }
  
      // jam modulo 10 of the sum in between the 1st and 2nd characters
      nextInstallationID =
        nextInstallationNumber.substring(0,1) +
        String.valueOf(sum%10) +
        nextInstallationNumber.substring(1,nextInstallationNumber.length());
    }
    
    return new Integer(nextInstallationID);

  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String,String> GetColumnToViewNamesMap ()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String,String> GetViewToColumnNamesMap ()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(MonitorDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = InstallationNumberRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames ( columnNames, __resources );

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames,viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames,columnNames);
  }

}