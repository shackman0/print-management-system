package shackelford.floyd.printmanagementsystem.monitordatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Row;



/**
 * <p>Title: CurrenciesRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class CurrenciesRow
  extends AbstractSQL3Row
{

  protected Integer               _sql_id;
  protected String                _sql_name;
  protected String                _sql_symbol;
  protected Double                _sql_multiplier;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.000");

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_id(),
                                    Name_name(),
                                    Name_symbol(),
                                    Name_multiplier(),

                                    Name_updated_dts(),
                                    Name_created_dts() };

  private static String[]         __keyColumnNames = {
                                    Name_id() };

  public CurrenciesRow()
  {
    super();
  }

  public CurrenciesRow(CurrenciesTable table)
  {
    super(table);
  }

  public CurrenciesRow( CurrenciesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public CurrenciesRow( CurrenciesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public void SetIDToNext()
  {
    Set_id(GetCurrenciesTable().GetNextCurrencyID());
  }

  public Integer Get_id() { return new Integer(_sql_id.intValue()); }
  public void Set_id(Integer value) { _sql_id = new Integer(value.intValue()); }
  public void Set_id(String value) { _sql_id = new Integer(value); }
  public void Set_id(int value) { _sql_id = new Integer(value); }
  public static String Name_id() { return "id"; }

  public String Get_name() { return new String(_sql_name); }
  public void Set_name(String value) { _sql_name = new String(value); }
  public static String Name_name() { return "name"; }

  public String Get_symbol() { return new String(_sql_symbol); }
  public void Set_symbol(String value) { _sql_symbol = new String(value); }
  public static String Name_symbol() { return "symbol"; }

  public Double Get_multiplier() { return new Double(_sql_multiplier.doubleValue()); }
  public String GetFormatted_multiplier() { return __formatter.format(_sql_multiplier); }
  public void Set_multiplier(Double value) { _sql_multiplier = new Double(value.doubleValue()); }
  public void Set_multiplier(String value) { _sql_multiplier = new Double(value); }
  public void Set_multiplier(double value) { _sql_multiplier = new Double(value); }
  public static String Name_multiplier() { return "multiplier"; }


  protected CurrenciesTable GetCurrenciesTable()
  {
    return (CurrenciesTable)_sqlTable;
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(CurrenciesRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}
