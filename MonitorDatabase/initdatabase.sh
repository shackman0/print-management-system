#! /bin/bash

# $Header: p:\CVS Repository/PayToPrint/MonitorDatabase/initdatabase.sh,v 1.1 2009/01/28 18:23:29 Floyd Shackelford Exp $

# NOTES:
# 1. This script must be run on the same machine running postgresql.
# 2. This script must be run by the postgres super user.
# 3. You must set the following variables correctly before running this script

# The variable PGSQLTOP must be set to point to the top of the
# PostgreSQL database directory

PGSQLTOP=/usr/local/pgsql

# set EXECTOP to where the executables are located

EXECTOP=/usr/local/pgsql/bin

# The variable PGSUPERUSER must be set to the user id of the postgres
# super user

PGSUPERUSER=postgres

# The variable CMS must be set to the user id of the Card Meter Systems
# user on the monitor server

CMS=CMS

# The variable CMSPASSWD must be set to the password of the PayToPrint
# CMS user on the monitor server

CMSPASSWD=CMSPASSWD

# ===== do not modify anything below this line =====

DATABASE=monitor_database
PGDATA=${PGSQLTOP}/data

echo PGSQLTOP=${PGSQLTOP}
echo EXECTOP=${EXECTOP}
echo PGSUPERUSER=${PGSUPERUSER}
echo CMS=${CMS}
echo CMSPASSWD=${CMSPASSWD}
echo DATABASE=${DATABASE}
echo PGDATA=${PGDATA}

echo dropping the ${DATABASE} \(ignore \"does not exist\" error\)
${EXECTOP}/dropdb ${DATABASE} ;
echo create the ${DATABASE}
${EXECTOP}/createdb ${DATABASE} ;

echo dropping the Card Meter Systems user \(ignore \"does not exist\" error\)
${EXECTOP}/dropuser ${CMS} ;
echo add the Card Meter Systems user
echo "${CMSPASSWD}
${CMSPASSWD}" | ${EXECTOP}/createuser --echo --no-createdb --no-adduser --pwprompt ${CMS} ;

echo create all the tables and indexes
${EXECTOP}/psql -f ./MonitorDatabase.sql ${DATABASE} ;

echo vacuum the database
${EXECTOP}/vacuumdb -d ${DATABASE} ;
${EXECTOP}/vacuumdb analyze -d ${DATABASE} ;

echo copying the pg_hba.conf file ...
cp -f ./pg_hba.conf ${PGDATA}/.

echo tell cron to run vacuumdb and do daily dumps
crontab -l >./new_crontab ;
cat ./crontab_file >>./new_crontab ;
crontab ./new_crontab ;
rm -f ./new_crontab ;

echo done!
