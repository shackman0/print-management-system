-- $Header: p:\CVS Repository/PayToPrint/MonitorDatabase/MonitorDatabase.sql,v 1.1 2009/01/28 18:23:28 Floyd Shackelford Exp $

-- The following tables make up the Monitor Database component.
-- The Monitor Database keeps track of all the installations.
-- This script assumes that the MonitorDatabase has already
-- been created using the createdb command.

create table installations_table
(
  installation_id                 int4          not null,
  installation_name               varchar(64)   not null,
  country                         varchar(32)   not null,
  currency                        varchar(32)   not null,
  installation_server_address     varchar(32)   not null,
  installation_server_user_id     varchar(8)    not null,
  installation_server_password    varchar(16)   not null,
  status                          char          not null,
  last_receivables_generated_dts  timestamp     null,
  license_expiration_date         date          not null,
  expiration_date_increment       int2          not null,
  up_front_fee_enabled            bool          not null,
  up_front_fee                    decimal       not null,
  monthly_fee_enabled             bool          not null,
  monthly_fee                     decimal       not null,
  annual_fee_enabled              bool          not null,
  annual_fee                      decimal       not null,
  per_page_charge_enabled         bool          not null,
  per_page_charge                 decimal       not null,
  per_print_job_charge_enabled    bool          not null,
  per_print_job_charge            decimal       not null,
  total_sales_percentage_enabled  bool          not null,
  total_sales_percentage          decimal       not null,
  created_dts                     timestamp     not null default text 'now',
  updated_dts                     timestamp     not null default text 'now'
);

grant all on installations_table to public;

create unique index installations_ndx_01 on installations_table
(
  installation_id
);

-- the installation_number_table has only one row in it, i.e. the last
-- number assigned to a new installation. because an installation
-- instance might never make it into the installations_table, there may
-- be gaps in the numbers found in the installations_table.

create table installation_number_table
(
  last_installation_number       int4         not null
);

grant all on installation_number_table to public;

-- this insert seeds the installation_number_table. it must be 10000.

insert into installation_number_table ( last_installation_number ) values ( 10000 );


-- the monitor_table has only one row in it. it is used to keep track of monitor
-- specific information.

create table monitor_table
(
  passphrase             varchar(64)       not null,
  language               varchar(32)       not null,
  created_dts            timestamp         not null default text 'now',
  updated_dts            timestamp         not null default text 'now'
);

grant all on monitor_table to public;

-- this insert seeds the monitor_table.

insert into monitor_table
(
  passphrase,
  language
)
values
(
  'changeme',
  'en_US'
);


-- the receivables table keeps track of receivables

create table receivables_table
(
  receivables_number               int4           not null,
  installation_id                  int4           not null,
  billing_period_start             date           not null,
  billing_period_end               date           not null,
  per_page_charge                  decimal        not null,
  per_print_job_charge             decimal        not null,
  percentage_of_revenues_charge    decimal        not null,
  created_dts                      timestamp      not null default text 'now',
  updated_dts                      timestamp      not null default text 'now'
);

grant all on receivables_table to public;

create unique index receivables_ndx_01 on receivables_table
(
  receivables_number
);

-- the receivables_number_table has only one row in it, i.e. the last
-- number assigned to a new receivable.

create table receivables_number_table
(
  last_receivables_number        int4         not null
);

grant all on receivables_number_table to public;

-- this insert seeds the receivables_number_table. you can start with any non-negative
-- value.

insert into receivables_number_table ( last_receivables_number ) values ( 0 );


-- the currencies table keeps track of currencies

create table currencies_table
(
  id                               int4           not null,
  name                             varchar(32)    not null,
  symbol                           char(4)        not null,
  multiplier                       decimal        not null,
  created_dts                      timestamp      not null default text 'now',
  updated_dts                      timestamp      not null default text 'now'
);

grant all on currencies_table to public;

-- this insert seeds the currencies_table.

-- i got these values from "The Universal Currency Converter" at
-- http://www.xe.net/ucc/full.shtml on 7 Dec 00 @ 0930 cst

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '1',
  'United States Dollar',
  'USD',
  '1.0'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '2',
  'Brazilian Real',
  'BRL',
  '1.972'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '3',
  'Mexican Peso',
  'MXP',
  '6.10244'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '4',
  'European Euro',
  'EUR',
  '1.1229'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '5',
  'Canadian Dollar',
  'CAD',
  '1.5302'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '6',
  'South Korean Won',
  'KRW',
  '1197.97'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '7',
  'Japanese Yen',
  'JPY',
  '110.49'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '8',
  'United Kingdom Pound',
  'GBP',
  '0.6930'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '9',
  'Australian Dollar',
  'AUD',
  '1.8285'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '10',
  'New Zealand Dollar',
  'NZD',
  '2.3417'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '11',
  'Danish Krone',
  'DKK',
  '8.3670'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '12',
  'Swiss Franc',
  'CHF',
  '1.7010'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '13',
  'Swedish Krona',
  'SEK',
  '9.6807'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '14',
  'Germany Deutsche Marks',
  'DEM',
  '2.20138'
);

insert into currencies_table
(
  id,
  name,
  symbol,
  multiplier
)
values
(
  '15',
  'French Francs',
  'FRF',
  '7.38311'
);

-- the currencies_number_table has only one row in it, i.e. the last
-- number assigned to a new receivable.

create table currencies_number_table
(
  last_currency_number           int4         not null
);

grant all on currencies_number_table to public;

-- this insert seeds the currencies_number_table. you can start with any number >= 100

insert into currencies_number_table ( last_currency_number ) values ( 100 );

