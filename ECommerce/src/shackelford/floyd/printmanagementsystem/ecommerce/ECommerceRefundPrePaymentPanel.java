package shackelford.floyd.printmanagementsystem.ecommerce;

import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.Resources;


/**
 * <p>Title: ECommerceRefundPrePaymentPanel</p>
 * <p>Description:
 * allows a user to refund some or all of his pre-payment
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommerceRefundPrePaymentPanel
  extends AbstractECommercePrePaymentPanel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -4930080401554407205L;
  

  private static Resources  __resources;

  /**
   * Constructor
   * @param callingPanel
   */
  public ECommerceRefundPrePaymentPanel(PanelListener callingPanel)
  {
    super(callingPanel);
  }

  @Override
  protected boolean FieldsValid()
  {
    if (super.FieldsValid() == false)
    {
      return false;
    }

    double amount = new Double(_amountField.getText()).doubleValue();
    return _selectedECommerce.DoECommerceRefund(amount);
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommerceRefundPrePaymentPanel.class.getName());
  }
}
