package shackelford.floyd.printmanagementsystem.ecommerce;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: ECommerceBankOfAmericaCreditCard</p>
 * <p>Description:
 * ecommerce implementation for a BoA credit card
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommerceBankOfAmericaCreditCard
  extends ECommerceCreditCard
{

  private static final String BANK_OF_AMERICA_AUTHORIZE_COMMAND  = "payment.mart";

  private static final String BANK_OF_AMERICA_SETTLEMENT_COMMAND = "settlement.mart";

  private static final String PAYTOPRINT_IOC_USER_NAME           = "PayToPrint";

  private static final String PAYTOPRINT_IOC_PASSWORD            = "X37R9IY";

  private static final String DEFAULT_ECOM_BILLTO_ONLINE_EMAIL   = "tbd@tbd.com";   // todo

  private static final String SETTLEMENT_INDICATOR               = "S";

  private static final String RETURN_INDICATOR                   = "R";

  private static final String HTTP_HOST                          = "HOST";

  private static final String HTTP_HOST_VALUE                    = "cart.bamart.com";

  private static final String HTTP_REFERER                       = "REFERER";

  private static final String HTTP_REFERER_VALUE                 = "www.tbd.com";

  private static final String YES                                = "Yes";

  //  private static final String NO = "No";

  // authorization form post (input) fields

  private static final String IOC_MERCHANT_ID                    = "ioc_merchant_id";

  private static final String IOC_ORDER_TOTAL_AMOUNT             = "ioc_order_total_amount";

  private static final String IOC_MERCHANT_SHOPPER_ID            = "ioc_merchant_shopper_id";

  private static final String IOC_MERCHANT_ORDER_ID              = "ioc_merchant_order_id";

  private static final String ECOM_BILLTO_POSTAL_NAME_FIRST      = "ecom_billto_postal_name_first";

  private static final String ECOM_BILLTO_POSTAL_NAME_LAST       = "ecom_billto_postal_name_last";

  private static final String ECOM_BILLTO_POSTAL_STREET_LINE1    = "ecom_billto_postal_street_line1";

  private static final String ECOM_BILLTO_POSTAL_STREET_LINE2    = "ecom_billto_postal_street_line2";

  private static final String ECOM_BILLTO_POSTAL_CITY            = "ecom_billto_postal_city";

  private static final String ECOM_BILLTO_POSTAL_STATEPROV       = "ecom_billto_postal_stateprov";

  private static final String ECOM_BILLTO_POSTAL_POSTALCODE      = "ecom_billto_postal_postalcode";

  private static final String ECOM_BILLTO_POSTAL_COUNTRYCODE     = "ecom_billto_postal_countrycode";

  private static final String ECOM_BILLTO_TELECOM_PHONE_NUMBER   = "ecom_billto_telecom_phone_number";

  private static final String ECOM_BILLTO_ONLINE_EMAIL           = "ecom_billto_online_email";

  private static final String ECOM_PAYMENT_CARD_NAME             = "ecom_payment_card_name";

  private static final String ECOM_PAYMENT_CARD_NUMBER           = "ecom_payment_card_number";

  private static final String ECOM_PAYMENT_CARD_EXPDATE_MONTH    = "ecom_payment_card_expdate_month";

  private static final String ECOM_PAYMENT_CARD_EXPDATE_YEAR     = "ecom_payment_card_expdate_year";

  //  private static final String IOC_CVV_INDICATOR = "ioc_cvv_indicator";
  //  private static final String ECOM_PAYMENT_CARD_VERFICATION = "ecom_payment_card_verification";

  // authorization form response fields: approved transaction

  //private static final String IOC_ORDER_TOTAL_AMOUNT = "ioc_order_total_amount";
  //private static final String IOC_MERCHANT_SHOPPER_ID = "ioc_merchant_shopper_id";
  //private static final String IOC_MERCHANT_ORDER_ID = "ioc_merchant_order_id";
  //  private static final String IOC_SHOPPER_ID = "ioc_shopper_id";
  //  private static final String IOC_AUTHORIZATION_AMOUNT = "ioc_authorization_amount";
  private static final String IOC_AUTHORIZATION_CODE             = "ioc_authorization_code";

  //  private static final String IOC_AVS_RESULT = "ioc_avs_result";
  private static final String IOC_ORDER_ID                       = "ioc_order_id";

  //  private static final String ECOM_TRANSACTION_COMPLETE = "ecom_transaction_complete";
  private static final String IOC_RESPONSE_CODE                  = "ioc_response_code";

  //  private static final String ECOM_PAYMENT_CARD_VERIFICATION_RC = "ecom_payment_card_verification_rc";

  // authorization form response fields: rejected transaction

  //private static final String IOC_ORDER_TOTAL_AMOUNT = "ioc_order_total_amount";
  //private static final String IOC_MERCHANT_SHOPPER_ID = "ioc_merchant_shopper_id";
  //private static final String IOC_MERCHANT_ORDER_ID = "ioc_merchant_order_id";
  //private static final String IOC_SHOPPER_ID = "ioc_shopper_id";
  //private static final String IOC_AVS_RESULT = "ioc_avs_result";
  //private static final String ECOM_TRANSACTION_COMPLETE = "ecom_transaction_complete";
  //private static final String IOC_RESPONSE_CODE = "ioc_response_code";
  private static final String IOC_REJECT_DESCRIPTION             = "ioc_reject_description";

  //private static final String ECOM_PAYMENT_CARD_VERIFICATION_RC = "ecom_payment_card_verification_rc";

  // authorization form response fields: invalid transaction

  //  private static final String IOC_INVALID_FIELDS = "ioc_invalid_fields";
  //  private static final String IOC_MISSING_FIELDS = "ioc_missing_fields";
  //private static final String IOC_MERCHANT_SHOPPER_ID = "ioc_merchant_shopper_id";
  //private static final String IOC_MERCHANT_ORDER_ID = "ioc_merchant_order_id";
  //private static final String IOC_SHOPPER_ID = "ioc_shopper_id";
  //private static final String ECOM_TRANSACTION_COMPLETE = "ecom_transaction_complete";
  //private static final String IOC_RESPONSE_CODE = "ioc_response_code";
  //private static final String IOC_REJECT_DESCRIPTION = "ioc_reject_description";

  // settlement form post (input) fields

  private static final String IOC_HANDSHAKE_ID                   = "ioc_handshake_id";

  //private static final String IOC_MERCHANT_ID = "ioc_merchant_id";
  private static final String IOC_USER_NAME                      = "ioc_User_Name";

  private static final String IOC_PASSWORD                       = "ioc_password";

  private static final String IOC_ORDER_NUMBER                   = "ioc_order_number";

  private static final String IOC_INDICATOR                      = "ioc_indicator";

  private static final String IOC_SETTLEMENT_AMOUNT              = "ioc_settlement_amount";

  //private static final String IOC_AUTHORIZATION_CODE = "ioc_authorization_code";
  //  private static final String IOC_EMAIL_FLAG = "ioc_email_flag";
  private static final String IOC_CLOSE_FLAG                     = "ioc_close_flag";

  private static final String IOC_INVOICE_NOTES                  = "ioc_invoice_notes";

  private static final String IOC_EMAIL_NOTES_FLAG               = "ioc_email_notes_flag";

  // settlement form response fields

  //private static final String IOC_HANDSHAKE_ID = "ioc_handshake_id";
  //  private static final String IOC_DUPLICATE_FLAG = "ioc_duplicate_flag";
  //  private static final String IOC_PROCESSED_FLAG = "ioc_processed_flag";
  //private static final String IOC_SETTLEMENT_AMOUNT = "ioc_settlement_amount";
  //private static final String IOC_INDICATOR = "ioc_indicator";
  //private static final String IOC_MERCHANT_ID = "ioc_merchant_id";
  //private static final String IOC_RESPONSE_CODE = "ioc_response_code";
  private static final String IOC_RESPONSE_DESC                  = "ioc_response_desc";

  public ECommerceBankOfAmericaCreditCard(JFrame parentFrame)
  {
    super(parentFrame);
  }

  @Override
  protected boolean DoCharge(MsgBox waitPanel)
  {
    // bank of america uses a two step process:
    // 1. get the transaction authorized
    // 2. settle the transaction (i.e. actually cause it to occur)

    Properties responseProps = AuthorizeChargeXaction();
    if (responseProps == null)
    {
      return false;
    }
    // look for the response code field
    String rc = responseProps.getProperty(IOC_RESPONSE_CODE);
    if (rc.trim().equals("0") == false)
    {
      String errorDescription = responseProps.getProperty(IOC_REJECT_DESCRIPTION) + " (rc=" + rc + ")";
      JOptionPane.showMessageDialog(GetParentFrame(), errorDescription, "Authorization Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    responseProps = SettleChargeXaction(responseProps);
    if (responseProps == null)
    {
      return false;
    }
    // look for the response code field
    rc = responseProps.getProperty(IOC_RESPONSE_CODE);
    if ((rc.trim().equals("0") == false) && (rc.trim().equals("2") == false)) // this is for test purposes only! remove for production
    {
      String errorDescription = responseProps.getProperty(IOC_RESPONSE_DESC) + " (rc=" + rc + ")";
      JOptionPane.showMessageDialog(GetParentFrame(), errorDescription, "Settlement Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  private Properties AuthorizeChargeXaction()
  {
    // construct the POST form of the command
    String urlString = InstallationTable.GetCachedInstallationRow().Get_cc_charge_url() + BANK_OF_AMERICA_AUTHORIZE_COMMAND;

    Properties dataProperties = new Properties();

    DecimalFormat formatter = new DecimalFormat("#,##0.00");

    try
    {
      dataProperties.setProperty(IOC_MERCHANT_ID, URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_ORDER_TOTAL_AMOUNT, URLEncoder.encode(formatter.format(GetXactionAmount()), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_MERCHANT_SHOPPER_ID, URLEncoder.encode(UserAccountTable.GetCachedUserAccountRow().Get_user_account_id().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_MERCHANT_ORDER_ID, URLEncoder.encode("0", LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_NAME_FIRST, URLEncoder.encode(_billingFirstNameTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_NAME_LAST, URLEncoder.encode(_billingLastNameTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_STREET_LINE1, URLEncoder.encode(_billingAddressStreet1TextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_STREET_LINE2, URLEncoder.encode(_billingAddressStreet2TextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_CITY, URLEncoder.encode(_billingAddressCityTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_STATEPROV, URLEncoder.encode(_billingAddressStateProvinceTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_POSTALCODE, URLEncoder.encode(_billingAddressPostalCodeTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_POSTAL_COUNTRYCODE, URLEncoder.encode(Resources.GetCountryCodes()[_billingAddressCountryComboBox.getSelectedIndex()].trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_BILLTO_TELECOM_PHONE_NUMBER, URLEncoder.encode(_billingPhoneNumberTextField.getText().trim(), LPDClient.UTF_8));
      String billToEmail = _emailTextField.getText().trim();
      if (billToEmail.length() == 0)
      {
        billToEmail = DEFAULT_ECOM_BILLTO_ONLINE_EMAIL;
      }
      //dataProperties.setProperty(ECOM_BILLTO_ONLINE_EMAIL,URLEncoder.encode(billToEmail));  // bofa doesn't handle url-encoded e-mail addrs properly
      dataProperties.setProperty(ECOM_BILLTO_ONLINE_EMAIL, billToEmail);
      dataProperties.setProperty(ECOM_PAYMENT_CARD_NAME, URLEncoder.encode(_nameOnCardTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_PAYMENT_CARD_NUMBER, URLEncoder.encode(_creditcardNumberTextField.getText().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_PAYMENT_CARD_EXPDATE_MONTH, URLEncoder.encode(_expirationDateMonthSpinField.getValue().toString().trim(), LPDClient.UTF_8));
      dataProperties.setProperty(ECOM_PAYMENT_CARD_EXPDATE_YEAR, URLEncoder.encode(_expirationDateYearSpinField.getValue().toString().trim(), LPDClient.UTF_8));

      //      int cvvIndicator = _cvvIndicatorComboBox.getSelectedIndex() + 1;
      //      if (cvvIndicator == 3)
      //      {
      //        cvvIndicator = 9;
      //      }
      //      dataProperties.setProperty(IOC_CVV_INDICATOR,URLEncoder.encode(String.valueOf(cvvIndicator)));
      //      dataProperties.setProperty(ECOM_PAYMENT_CARD_VERFICATION,URLEncoder.encode(_cvvValueTextField.getText().trim()));
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    // send the xaction and process the resulting html page
    String returnedPage = DoPOSTXaction(urlString, dataProperties, GetRequestProperties());
    Properties responseProps = ExtractNameValuePairsFromWebPage(returnedPage, "<BR>");

    return responseProps;
  }

  private Properties SettleChargeXaction(Properties authorizationProps)
  {
    // construct the POST form of the command
    String urlString = InstallationTable.GetCachedInstallationRow().Get_cc_charge_url() + BANK_OF_AMERICA_SETTLEMENT_COMMAND;

    Properties dataProperties = new Properties();

    // installation id + user account id + current time as milliseconds
    Calendar calendar = new GregorianCalendar();
    String handshakeID = InstallationTable.GetCachedInstallationRow().Get_installation_id() + ":" + UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + ":"
      + String.valueOf(calendar.getTime().getTime());

    String invoiceNotes = "installation id = \"" + InstallationTable.GetCachedInstallationRow().Get_installation_id() + "\", " + "user account id = \""
      + UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + "\", " + "date-time stamp = \"" + String.valueOf(calendar.getTime().getTime()) + "\"";

    try
    {
      dataProperties.setProperty(IOC_HANDSHAKE_ID, URLEncoder.encode(handshakeID, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_MERCHANT_ID, URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim(), LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_USER_NAME,URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_creditcardChargeUserName.trim(),LPDClient.UTF_8));
      dataProperties.setProperty(IOC_USER_NAME, PAYTOPRINT_IOC_USER_NAME);
      //dataProperties.setProperty(IOC_PASSWORD,URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_creditcardChargePassword.trim(),LPDClient.UTF_8));
      dataProperties.setProperty(IOC_PASSWORD, PAYTOPRINT_IOC_PASSWORD);
      dataProperties.setProperty(IOC_ORDER_NUMBER, URLEncoder.encode(authorizationProps.getProperty(IOC_ORDER_ID).trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_INDICATOR, URLEncoder.encode(SETTLEMENT_INDICATOR, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_SETTLEMENT_AMOUNT, URLEncoder.encode(String.valueOf(GetXactionAmount()).trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_AUTHORIZATION_CODE, URLEncoder.encode(authorizationProps.getProperty(IOC_AUTHORIZATION_CODE).trim(), LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_EMAIL_FLAG,URLEncoder.encode(,LPDClient.UTF_8));
      dataProperties.setProperty(IOC_CLOSE_FLAG, URLEncoder.encode(YES, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_INVOICE_NOTES, URLEncoder.encode(invoiceNotes, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_EMAIL_NOTES_FLAG, URLEncoder.encode(YES, LPDClient.UTF_8));
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    // send the xaction and process the resulting html page
    String returnedPage = DoPOSTXaction(urlString, dataProperties, GetRequestProperties());
    Properties responseProps = ExtractNameValuePairsFromWebPage(returnedPage, "<BR>");

    return responseProps;
  }

  Properties GetRequestProperties()
  {
    Properties requestProperties = new Properties();
    requestProperties.setProperty(HTTP_HOST, HTTP_HOST_VALUE);
    requestProperties.setProperty(HTTP_REFERER, HTTP_REFERER_VALUE);
    return requestProperties;
  }

  @Override
  protected boolean DoRefund(MsgBox waitPanel)
  {
    // bank of america uses a one step process:
    // 1. settle the transaction

    Properties refundProperties = new Properties();

    Properties responseProps = SettleRefundXaction(refundProperties);
    if (responseProps == null)
    {
      return false;
    }
    // look for the response code field
    String rc = responseProps.getProperty(IOC_RESPONSE_CODE);
    if ((rc.trim().equals("0") == false) && (rc.trim().equals("2") == false)) // this is for test purposes only! remove for production
    {
      String errorDescription = responseProps.getProperty(IOC_RESPONSE_DESC) + " (rc=" + rc + ")";
      JOptionPane.showMessageDialog(GetParentFrame(), errorDescription, "Settlement Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  private Properties SettleRefundXaction(Properties refundProperties)
  {
    // construct the POST form of the command
    String urlString = InstallationTable.GetCachedInstallationRow().Get_cc_charge_url() + BANK_OF_AMERICA_SETTLEMENT_COMMAND;

    Properties dataProperties = new Properties();

    // installation id + user account id + current time as milliseconds
    Calendar calendar = new GregorianCalendar();
    String handshakeID = InstallationTable.GetCachedInstallationRow().Get_installation_id() + ":" + UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + ":"
      + String.valueOf(calendar.getTime().getTime());

    try
    {
      dataProperties.setProperty(IOC_HANDSHAKE_ID, URLEncoder.encode(handshakeID, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_MERCHANT_ID, URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim(), LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_USER_NAME,URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_creditcardChargeUserName.trim(),LPDClient.UTF_8));
      dataProperties.setProperty(IOC_USER_NAME, PAYTOPRINT_IOC_USER_NAME);
      //dataProperties.setProperty(IOC_PASSWORD,URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_creditcardChargePassword.trim(),LPDClient.UTF_8));
      dataProperties.setProperty(IOC_PASSWORD, PAYTOPRINT_IOC_PASSWORD);
      dataProperties.setProperty(IOC_ORDER_NUMBER, URLEncoder.encode(refundProperties.getProperty(IOC_ORDER_ID).trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_INDICATOR, URLEncoder.encode(RETURN_INDICATOR, LPDClient.UTF_8));
      dataProperties.setProperty(IOC_SETTLEMENT_AMOUNT, URLEncoder.encode(String.valueOf(GetXactionAmount()).trim(), LPDClient.UTF_8));
      dataProperties.setProperty(IOC_AUTHORIZATION_CODE, URLEncoder.encode(refundProperties.getProperty(IOC_AUTHORIZATION_CODE).trim(), LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_EMAIL_FLAG,URLEncoder.encode(,LPDClient.UTF_8));
      dataProperties.setProperty(IOC_CLOSE_FLAG, URLEncoder.encode(YES, LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_INVOICE_NOTES,URLEncoder.encode(,LPDClient.UTF_8));
      //dataProperties.setProperty(IOC_EMAIL_NOTES_FLAG,URLEncoder.encode(YES,LPDClient.UTF_8));
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    // send the xaction and process the resulting html page
    String returnedPage = DoPOSTXaction(urlString, dataProperties, GetRequestProperties());
    Properties responseProps = ExtractNameValuePairsFromWebPage(returnedPage, "<BR>");

    return responseProps;
  }

}
