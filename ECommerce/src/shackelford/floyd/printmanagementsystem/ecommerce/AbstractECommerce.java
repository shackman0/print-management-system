package shackelford.floyd.printmanagementsystem.ecommerce;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.Security;
import java.text.MessageFormat;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AbstractECommerce</p>
 * <p>Description:
 * abstract class for the various ecommerce implementations
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractECommerce
{

  private double                _amount;

  private JPanel                _chargeDataEntryPanel;

  private JPanel                _refundDataEntryPanel;

  private double                _fee;

  private final double          _originalBalance;

  private final JFrame          _parentFrame;

  private PrePaymentHistoryRow  _prePaymentHistoryRow;

  private double                _xactionAmount;

  protected static final String CONFIRM_CHARGE_MSG   = "CONFIRM_CHARGE_MSG";

  protected static final String CONFIRM_CHARGE_TITLE = "CONFIRM_CHARGE_TITLE";

  protected static final String CONFIRM_REFUND_MSG   = "CONFIRM_REFUND_MSG";

  protected static final String CONFIRM_REFUND_TITLE = "CONFIRM_REFUND_TITLE";

  protected AbstractECommerce(JFrame parentFrame)
  {
    _parentFrame = parentFrame;
    _originalBalance = UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue();
    _chargeDataEntryPanel = null;
    _refundDataEntryPanel = null;
  }

  public JPanel GetChargeDataEntryPanel()
  {
    if (_chargeDataEntryPanel == null)
    {
      _chargeDataEntryPanel = BuildChargeDataEntryPanel();
    }
    return _chargeDataEntryPanel;
  }

  public JPanel GetRefundDataEntryPanel()
  {
    if (_refundDataEntryPanel == null)
    {
      _refundDataEntryPanel = BuildRefundDataEntryPanel();
    }
    return _refundDataEntryPanel;
  }

  protected InstallationTable GetInstallationTable()
  {
    return InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable();
  }

  protected UserAccountTable GetUserAccountTable()
  {
    return InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable();
  }

  protected double GetAmount()
  {
    return _amount;
  }

  protected double GetFee()
  {
    return _fee;
  }

  protected PrePaymentHistoryRow GetPrePaymentHistoryRow()
  {
    return _prePaymentHistoryRow;
  }

  protected JFrame GetParentFrame()
  {
    return _parentFrame;
  }

  protected double GetOriginalBalance()
  {
    return _originalBalance;
  }

  protected double GetXactionAmount()
  {
    return _xactionAmount;
  }

  protected abstract boolean EnabledForCharge();

  protected abstract JPanel BuildChargeDataEntryPanel();

  protected abstract boolean ProcessChargeDataEntryPanel();

  protected abstract String GetChargeWaitPanelTitle();

  protected abstract String GetChargeWaitPanelInitialMessage();

  protected abstract boolean CheckValidChargeAmount();

  protected abstract double CalculateChargeFee();

  protected abstract boolean DoCharge(MsgBox pleaseWaitMsgBox);

  protected abstract boolean EnabledForRefund();

  protected abstract JPanel BuildRefundDataEntryPanel();

  protected abstract boolean ProcessRefundDataEntryPanel();

  protected abstract String GetRefundWaitPanelTitle();

  protected abstract String GetRefundWaitPanelInitialMessage();

  protected abstract boolean CheckValidRefundAmount();

  protected abstract double CalculateRefundFee();

  protected abstract boolean DoRefund(MsgBox pleaseWaitMsgBox);

  protected abstract Resources GetChildResources();

  protected abstract String GetSource();

  /*
    @param amount >0 for pre payments and <0 for refunds
  */
  protected boolean UpdateInstallationDatabase(double amount)
  {
    _prePaymentHistoryRow = new PrePaymentHistoryRow(InstallationDatabase.GetDefaultInstallationDatabase().GetPrePaymentHistoryTable(), UserAccountTable.GetCachedUserAccountRow());
    _prePaymentHistoryRow.Set_amount(amount);
    _prePaymentHistoryRow.Set_source(GetSource());
    _prePaymentHistoryRow.Insert();

    double balance = UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue();
    balance += amount;
    if (balance < 0d)
    {
      balance = 0d;
    }

    UserAccountTable.GetCachedUserAccountRow().Set_pre_payment_balance(balance);
    UserAccountTable.GetCachedUserAccountRow().Update();

    return true;
  }

  /*
    This routine charges the specified amount plus any fees.
    rc = true means that the charge was successful
    rc = false means that either the user cancelled the charge or
         the xaction failed
  */
  public boolean DoECommerceCharge(double amount)
  {
    _amount = amount;
    if (CheckValidChargeAmount() == false)
    {
      return false;
    }
    if (ProcessChargeDataEntryPanel() == false)
    {
      return false;
    }
    if (CalculateChargeValues() == false)
    {
      return false;
    }
    if (ShowConfirmChargeDialog() == false)
    {
      return false;
    }
    if (_xactionAmount > 0d)
    {
      MsgBox pleaseWaitMsgBox = new MsgBox(GetChargeWaitPanelInitialMessage(), GetChargeWaitPanelTitle());
      pleaseWaitMsgBox.setVisible(true);

      if (DoCharge(pleaseWaitMsgBox) == false)
      {
        pleaseWaitMsgBox.dispose();
        return false;
      }
      pleaseWaitMsgBox.dispose();
    }
    if (UpdateInstallationDatabase(_amount) == false)
    {
      return false;
    }
    ShowChargeCompleteDialog();
    return true;
  }

  protected boolean CalculateChargeValues()
  {
    _fee = CalculateChargeFee();
    if (_fee < 0d)
    {
      _xactionAmount = 0d;
      return false;
    }
    _xactionAmount = _amount + _fee;
    _xactionAmount = Math.round(_xactionAmount * 100d) / 100d;
    return true;
  }

  protected boolean ShowConfirmChargeDialog()
  {
    MessageFormat msgFormat = new MessageFormat(GetChildResources().getProperty(AbstractECommerce.CONFIRM_CHARGE_MSG));
    Object[] substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), new Double(_amount), new Double(_fee), new Double(_amount + _fee),
      UserAccountTable.GetCachedUserAccountRow().Get_user_account_id(), GetSource() };
    String msgString = msgFormat.format(substitutionValues).trim();

    int rc = JOptionPane.showConfirmDialog(_parentFrame, msgString, GetChildResources().getProperty(AbstractECommerce.CONFIRM_CHARGE_TITLE), JOptionPane.YES_NO_OPTION);
    return ((rc == JOptionPane.YES_OPTION) == true);
  }

  protected void ShowChargeCompleteDialog()
  {
    ECommerceAddPrePaymentCompletePanel addCompletePanel = new ECommerceAddPrePaymentCompletePanel(_originalBalance, _amount, _prePaymentHistoryRow);
    addCompletePanel.setVisible(true);
  }

  /*
    This routine refunds the specified amount less any fees.
    rc = true means that the refund was successful
    rc = false means that either the user cancelled the refund or
         the xaction failed
  */
  public boolean DoECommerceRefund(double amount)
  {
    _amount = amount;
    if (CheckValidRefundAmount() == false)
    {
      return false;
    }
    if (ProcessRefundDataEntryPanel() == false)
    {
      return false;
    }
    if (CalculateRefundValues() == false)
    {
      return false;
    }
    if (ShowConfirmRefundDialog() == false)
    {
      return false;
    }
    if (_xactionAmount > 0d)
    {
      MsgBox pleaseWaitMsgBox = new MsgBox(GetRefundWaitPanelInitialMessage(), GetRefundWaitPanelTitle());
      pleaseWaitMsgBox.setVisible(true);
      if (DoRefund(pleaseWaitMsgBox) == false)
      {
        pleaseWaitMsgBox.dispose();
        return false;
      }
      pleaseWaitMsgBox.dispose();
    }
    if (UpdateInstallationDatabase(-_amount) == false)
    {
      return false;
    }
    ShowRefundCompleteDialog();
    return true;
  }

  protected boolean CalculateRefundValues()
  {
    _fee = CalculateRefundFee();
    if (_fee < 0d)
    {
      _xactionAmount = 0d;
      return false;
    }
    _xactionAmount = _amount - _fee;
    if (_xactionAmount < 0d)
    {
      _xactionAmount = 0d;
    }
    _xactionAmount = Math.round(_xactionAmount * 100d) / 100d;
    return true;
  }

  protected boolean ShowConfirmRefundDialog()
  {
    MessageFormat msgFormat = new MessageFormat(GetChildResources().getProperty(AbstractECommerce.CONFIRM_REFUND_MSG));
    Object[] substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), new Double(_amount), new Double(_fee), new Double(_amount - _fee),
      UserAccountTable.GetCachedUserAccountRow().Get_user_account_id(), GetSource() };
    String msgString = msgFormat.format(substitutionValues).trim();

    int rc = JOptionPane.showConfirmDialog(_parentFrame, msgString, GetChildResources().getProperty(AbstractECommerce.CONFIRM_REFUND_TITLE), JOptionPane.YES_NO_OPTION);
    return ((rc == JOptionPane.YES_OPTION) == true);
  }

  protected void ShowRefundCompleteDialog()
  {
    ECommerceRefundPrePaymentCompletePanel refundCompletePanel = new ECommerceRefundPrePaymentCompletePanel(_originalBalance, _amount, _prePaymentHistoryRow);
    refundCompletePanel.setVisible(true);
  }

  /*
    convenience methods follow
  */

  protected String DoGETXaction(String urlString)
  {
    String webPage = "";
    try
    {
      // invoke the site and execute the transaction
      URL url = new URL(urlString);
      try (BufferedReader urlReader = new BufferedReader(new InputStreamReader(url.openStream()));)
      {
        // read the entire page into a single string for processing
        String inputLine;
        while ((inputLine = urlReader.readLine()) != null)
        {
          webPage += (URLDecoder.decode(inputLine, LPDClient.UTF_8) + "\n");
        }
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(GetParentFrame(), excp, "Error Processing Response", JOptionPane.ERROR_MESSAGE);
      return null;
    }

    return webPage;
  }

  protected String DoPOSTXaction(String urlString, Properties dataProperties, Properties requestProperties)
  {
    String webPage = "";
    try
    {
      // invoke the site and send the xaction using the POST technique
      URL url = new URL(urlString);
      HttpURLConnection urlConnection = (HttpURLConnection) (url.openConnection());
      urlConnection.setDoOutput(true);
      urlConnection.setDoInput(true);
      urlConnection.setRequestMethod("POST");

      // create the values line first so we can use its length in the POST header
      String valuesLine = "";
      String separator = "&";
      for (Enumeration<?> propertyNames = dataProperties.propertyNames(); propertyNames.hasMoreElements();)
      {
        String propertyName = URLEncoder.encode(propertyNames.nextElement().toString(), LPDClient.UTF_8);
        String propertyValue = URLEncoder.encode(dataProperties.getProperty(propertyName), LPDClient.UTF_8);
        if (propertyNames.hasMoreElements() == false)
        {
          separator = "";
        }
        valuesLine += propertyName + "=" + propertyValue + separator;
      }

      // setup the POST header
      urlConnection.setRequestProperty("POST", url.getPath() + " HTTP/1.1");
      urlConnection.setRequestProperty("USER_AGENT", "Java1.3.1_01");
      urlConnection.setRequestProperty("ACCEPT", "*/*");
      urlConnection.setRequestProperty("CONTENT_LENGTH", String.valueOf(valuesLine.length()));
      urlConnection.setRequestProperty("CONTENT_TYPE", "application/x-www-form-urlencoded");
      urlConnection.setRequestProperty("CACHE_CONTROL", "no-cache");
      urlConnection.setRequestProperty("CONNECTION", "Keep-Alive");

      if (requestProperties != null)
      {
        for (Enumeration<?> propertyNames = requestProperties.propertyNames(); propertyNames.hasMoreElements();)
        {
          String propertyName = URLEncoder.encode(propertyNames.nextElement().toString(), LPDClient.UTF_8);
          String propertyValue = URLEncoder.encode(requestProperties.getProperty(propertyName), LPDClient.UTF_8);
          urlConnection.setRequestProperty(propertyName, propertyValue);
        }
      }

      urlConnection.setUseCaches(false);
      urlConnection.setAllowUserInteraction(false);

      // connect to the remote site
      urlConnection.connect();

      // write the values (name=value&name=value&...) line
      try (PrintWriter urlWriter = new PrintWriter(urlConnection.getOutputStream());)
      {
        urlWriter.println(valuesLine);
      }

      // read the response from the xaction into a single string for processing
      try (BufferedReader urlReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));)
      {
        String inputLine;
        while ((inputLine = urlReader.readLine()) != null)
        {
          webPage += (URLDecoder.decode(inputLine, LPDClient.UTF_8) + "\n");
        }
      }

      // disconnect from the remote site
      urlConnection.disconnect();

    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(GetParentFrame(), excp, "Error Processing Response", JOptionPane.ERROR_MESSAGE);
      return null;
    }
    return webPage;
  }

  protected Properties ExtractFormFieldsFromWebPage(String webPage)
  {
    // process the feedback from the xaction.
    // capture all the name/value pairs although we don't use them all
    Properties nameValuePairs = new Properties();
    int beginIndex = 0;
    int endIndex = 0;
    while ((beginIndex = webPage.indexOf("<input", beginIndex)) != -1)
    {
      // capture the name
      beginIndex = webPage.indexOf("name=", beginIndex) + 5;
      endIndex = webPage.indexOf(" ", beginIndex);
      String name = webPage.substring(beginIndex, endIndex);
      // capture the value
      beginIndex = webPage.indexOf("value=", endIndex) + 7;
      endIndex = webPage.indexOf("\"", beginIndex);
      String value = webPage.substring(beginIndex, endIndex);
      // save the name/value pair in our properties object
      nameValuePairs.put(name, value);
    }
    return nameValuePairs;
  }

  /**
   * this routine extracts name-value pairs from the web page. the web page
   * is expected to be in the form:
   *
   * <name>=<value><delimiter>...\n
   *
   * white space is ignored between the 3 fields. the <delimiter> field might
   * not be present with the last name-value entry.
   *
   * here's how we do it:
   *
   * 1. we turn all the delimiters into \n
   * 2. then we use the properties.load method to load in the properties from the
   *    web page string.
   */
  protected Properties ExtractNameValuePairsFromWebPage(String webPage, String delimiter)
  {
    Properties rawProperties = new Properties();
    webPage = webPage.trim();
    // turn all the delimiters into \n
    int index = webPage.indexOf(delimiter);
    while (index != -1)
    {
      webPage = webPage.substring(0, index) + "\n" + webPage.substring(index + delimiter.length());
      index = webPage.indexOf(delimiter);
    }
    // load the properties object from the webPage
    try
    {
      ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(webPage.getBytes());
      rawProperties.load(byteArrayInputStream);
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    // convert all the property names to lower case so i can find them
    Properties nameValuePairs = new Properties();
    for (Enumeration<?> enumerator = rawProperties.propertyNames(); enumerator.hasMoreElements();)
    {
      String propertyName = enumerator.nextElement().toString();
      String propertyValue = rawProperties.getProperty(propertyName);
      nameValuePairs.setProperty(propertyName.toLowerCase(), propertyValue);
    }
    return nameValuePairs;
  }

  static
  {
    // this allows us to use "https" URL connections
    Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
    // so that the system will find the "https" URL protocol handler
    System.setProperty("java.protocol.handler.pkgs", "com.sun.net.ssl.internal.www.protocol");
    // for debugging purposes only
    // System.setProperty("javax.net.debug","ssl,handshake,data,trustmanager");
  }

}
