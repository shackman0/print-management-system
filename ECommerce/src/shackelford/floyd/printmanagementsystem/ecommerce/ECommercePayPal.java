package shackelford.floyd.printmanagementsystem.ecommerce;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.MessageFormat;
import java.util.Properties;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import shackelford.floyd.printmanagementsystem.common.BrowserControl;
import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: ECommercePayPal</p>
 * <p>Description:
 * ecommerce implementation for PayPal
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommercePayPal
  extends AbstractECommerce
{

  private OverwritableTextField _paypalAccountNumberTextField;

  private JEditorPane           _paypalEditorPane;

  private JPasswordField        _paypalPasswordField;

  private static Resources      __resources;

  private static final String   PAYPAL_ACCOUNT_NUMBER             = "PAYPAL_ACCOUNT_NUMBER";

  private static final String   PAYPAL_ACCOUNT_NUMBER_TOOL_TIP    = "PAYPAL_ACCOUNT_NUMBER_TOOL_TIP";

  private static final String   PAYPAL_PASSWORD                   = "PAYPAL_PASSWORD";

  private static final String   PAYPAL_PASSWORD_TOOL_TIP          = "PAYPAL_PASSWORD_TOOL_TIP";

  private static final String   MINIMUM_AMOUNT_MSG                = "MINIMUM_AMOUNT_MSG";

  private static final String   AMOUNT_TOO_SMALL                  = "AMOUNT_TOO_SMALL";

  private static final String   AMOUNT_TOO_LARGE                  = "AMOUNT_TOO_LARGE";

  private static final String   AMOUNT_TOO_LARGE_TITLE            = "AMOUNT_TOO_LARGE_TITLE";

  private static final String   AMOUNT_TOO_SMALL_TITLE            = "AMOUNT_TOO_SMALL_TITLE";

  private static final String   ACCOUNT_NUMBER_MISSING            = "ACCOUNT_NUMBER_MISSING";

  private static final String   ACCOUNT_NUMBER_MISSING_TITLE      = "ACCOUNT_NUMBER_MISSING_TITLE";

  private static final String   PASSWORD_MISSING                  = "PASSWORD_MISSING";

  private static final String   PASSWORD_MISSING_TITLE            = "PASSWORD_MISSING_TITLE";

  private static final String   USER_ACCOUNT                      = "USER_ACCOUNT";

  private static final String   INSTALLATION_ID                   = "INSTALLATION_ID";

  private static final String   CHARGE_XACTION                    = "CHARGE_XACTION";

  private static final String   REFUND_XACTION                    = "REFUND_XACTION";

  private static final String   CHARGE_WAIT_PANEL_INITIAL_MESSAGE = "CHARGE_WAIT_PANEL_INITIAL_MESSAGE";

  private static final String   CHARGE_WAIT_PANEL_TITLE           = "CHARGE_WAIT_PANEL_TITLE";

  private static final String   REFUND_WAIT_PANEL_INITIAL_MESSAGE = "REFUND_WAIT_PANEL_INITIAL_MESSAGE";

  private static final String   REFUND_WAIT_PANEL_TITLE           = "REFUND_WAIT_PANEL_TITLE";

  private static final String   PAYPAL                            = "PayPal ";

  private static final String   BEGIN_ERROR                       = "begin error";

  /*
    these variables contain the strings used to communicate with the paypal web site.
  */
  private static final String   PAYPAL_LINK_HTML                  = "<A HREF=\"https://secure.paypal.com/affil/pal={0}\" target=\"_blank\"> "
                                                                    + "<IMG SRC=\"http://images.paypal.com/images/lgo/logo1.gif\" BORDER=\"0\" "
                                                                    + "ALT=\"Make payments with PayPal - it\'s fast, free and secure!\"></A>";

  // {0} = the installation's paypal account number (usually an e-mail address)

  private static final String   PAYPAL_WEBSCR_URL                 = "https://secure.paypal.com/cgi-bin/webscr";

  private static final int      PAYPAL_ACCOUNT_NUMBER_FIELD_WIDTH = 16;

  private static final int      PAYPAL_PASSWORD_FIELD_WIDTH       = 16;

  public ECommercePayPal(JFrame parentFrame)
  {
    super(parentFrame);
  }

  @Override
  protected String GetSource()
  {
    return PAYPAL + _paypalAccountNumberTextField.getText().trim();
  }

  @Override
  protected boolean EnabledForCharge()
  {
    if (InstallationTable.GetCachedInstallationRow().Get_paypal_charge_account().trim().length() == 0)
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildChargeDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // minimum amount message
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.CENTER;

    // MINIMUM_AMOUNT_MSG format:
    //   {0} = currency symbol
    //   {1,number,currency} = minimum amount

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MINIMUM_AMOUNT_MSG).toString());
    Object[] substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), InstallationTable.GetCachedInstallationRow().Get_paypal_charge_minimum_amount() };
    JLabel label = new JLabel(msgFormat.format(substitutionValues).trim(), SwingConstants.LEFT);
    aPanel.add(label, constraints);

    // paypal account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(PAYPAL_ACCOUNT_NUMBER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalAccountNumberTextField = new OverwritableTextField(PAYPAL_ACCOUNT_NUMBER_FIELD_WIDTH);
    _paypalAccountNumberTextField.setToolTipText(__resources.getProperty(PAYPAL_ACCOUNT_NUMBER_TOOL_TIP).toString());
    aPanel.add(_paypalAccountNumberTextField, constraints);

    // paypal password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(PAYPAL_PASSWORD).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalPasswordField = new JPasswordField(PAYPAL_PASSWORD_FIELD_WIDTH);
    _paypalPasswordField.setToolTipText(__resources.getProperty(PAYPAL_PASSWORD_TOOL_TIP).toString());
    aPanel.add(_paypalPasswordField, constraints);

    // open a paypal account
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 2;
    constraints.gridheight = 2;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.CENTER;

    _paypalEditorPane = new JEditorPane();
    _paypalEditorPane.addHyperlinkListener(new PayPalHyperlinkListener());
    _paypalEditorPane.setEditable(false);
    _paypalEditorPane.setContentType("text/html");

    msgFormat = new MessageFormat(PAYPAL_LINK_HTML);
    substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_paypal_charge_account().trim() };
    String finalHTML = msgFormat.format(substitutionValues).trim();
    _paypalEditorPane.setText(finalHTML);

    aPanel.add(_paypalEditorPane, constraints);

    return aPanel;
  }

  @Override
  protected boolean ProcessChargeDataEntryPanel()
  {
    // account number must be entered
    if (_paypalAccountNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(ACCOUNT_NUMBER_MISSING).toString(), __resources.getProperty(ACCOUNT_NUMBER_MISSING_TITLE).toString(),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // password must be entered
    if (new String(_paypalPasswordField.getPassword()).trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(PASSWORD_MISSING).toString(), __resources.getProperty(PASSWORD_MISSING_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidChargeAmount()
  {
    double minimumAmount = InstallationTable.GetCachedInstallationRow().Get_paypal_charge_minimum_amount().doubleValue();
    if (minimumAmount > GetAmount())
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_SMALL).toString(), __resources.getProperty(AMOUNT_TOO_SMALL_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateChargeFee()
  {
    double fee = InstallationTable.GetCachedInstallationRow().Get_paypal_charge_xaction_fee().doubleValue()
      + ((InstallationTable.GetCachedInstallationRow().Get_paypal_charge_xaction_fee_pcent().doubleValue() * GetAmount()) / 100.0d);

    return fee;
  }

  @Override
  protected String GetChargeWaitPanelTitle()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_TITLE).toString();
  }

  @Override
  protected String GetChargeWaitPanelInitialMessage()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_INITIAL_MESSAGE).toString();
  }

  @Override
  protected boolean DoCharge(MsgBox waitPanel)
  {
    return DoPayPalXaction(_paypalAccountNumberTextField.getText(), new String(_paypalPasswordField.getPassword()), InstallationTable.GetCachedInstallationRow().Get_paypal_charge_account().trim(),
      __resources.getProperty(CHARGE_XACTION).toString());
  }

  @Override
  protected boolean EnabledForRefund()
  {
    if (InstallationTable.GetCachedInstallationRow().Get_paypal_refund_account().trim().length() == 0)
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildRefundDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // paypal account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(PAYPAL_ACCOUNT_NUMBER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalAccountNumberTextField = new OverwritableTextField(PAYPAL_ACCOUNT_NUMBER_FIELD_WIDTH);
    _paypalAccountNumberTextField.setToolTipText(__resources.getProperty(PAYPAL_ACCOUNT_NUMBER_TOOL_TIP).toString());
    aPanel.add(_paypalAccountNumberTextField, constraints);

    return aPanel;
  }

  @Override
  protected boolean ProcessRefundDataEntryPanel()
  {
    // account number must be entered
    if (_paypalAccountNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(ACCOUNT_NUMBER_MISSING).toString(), __resources.getProperty(ACCOUNT_NUMBER_MISSING_TITLE).toString(),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidRefundAmount()
  {
    if (GetAmount() <= 0d)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_SMALL).toString(), __resources.getProperty(AMOUNT_TOO_SMALL_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (GetAmount() > UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue())
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_LARGE).toString(), __resources.getProperty(AMOUNT_TOO_LARGE_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateRefundFee()
  {
    double fee = InstallationTable.GetCachedInstallationRow().Get_paypal_refund_xaction_fee().doubleValue()
      + ((InstallationTable.GetCachedInstallationRow().Get_paypal_charge_xaction_fee_pcent().doubleValue() * GetAmount()) / 100.0d);

    return fee;
  }

  @Override
  protected String GetRefundWaitPanelTitle()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_TITLE).toString();
  }

  @Override
  protected String GetRefundWaitPanelInitialMessage()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_INITIAL_MESSAGE).toString();
  }

  @Override
  protected boolean DoRefund(MsgBox waitPanel)
  {
    return DoPayPalXaction(InstallationTable.GetCachedInstallationRow().Get_paypal_refund_account().trim(), InstallationTable.GetCachedInstallationRow().Get_paypal_refund_passphrase().trim(),
      _paypalAccountNumberTextField.getText(), __resources.getProperty(REFUND_XACTION).toString());
  }

  private boolean CheckPayPalPage(String webPage)
  {
    if (webPage == null)
    {
      return false;
    }

    // look for "begin error": if there is one, then we have an error
    int beginIndex;
    if ((beginIndex = webPage.indexOf(BEGIN_ERROR)) != -1)
    {
      beginIndex = webPage.indexOf("<b>", beginIndex) + 3;
      int endIndex = webPage.indexOf("</b>", beginIndex);
      String errorText = webPage.substring(beginIndex, endIndex);
      JOptionPane.showMessageDialog(GetParentFrame(), errorText, "PayPal Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  private boolean DoPayPalXaction(String payerAccount, String payerPassword, String payeeAccount, String itemDescription)
  {
    Properties properties = null;

    // tell paypal we want to initiate a xaction
    // it returns the send money page
    properties = new Properties();
    properties.setProperty("cmd", "_xclick");
    properties.setProperty("business", payeeAccount);
    properties.setProperty("item_name", itemDescription);
    properties.setProperty("item_number",
      __resources.getProperty(USER_ACCOUNT).toString() + "=" + UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + ";" + __resources.getProperty(INSTALLATION_ID).toString() + "="
        + UserAccountTable.GetCachedUserAccountRow().Get_installation_id());
    properties.setProperty("quantity", "1");
    properties.setProperty("amount", String.valueOf(GetXactionAmount()));

    String sendMoneyPage = DoPOSTXaction(PAYPAL_WEBSCR_URL, properties, null);
    if (sendMoneyPage == null)
    {
      return false;
    }
    DebugWindow.DisplayText("*** sendMoneyPage ***\n" + sendMoneyPage);
    Properties sendMoneyPageFields = ExtractFormFieldsFromWebPage(sendMoneyPage);
    sendMoneyPageFields.list(System.out);

    // fill out the "send money" form and send it
    // it returns the "confirmation" page
    properties.setProperty("cmd", "_xclick-user-submit");
    properties.setProperty("login_email", payerAccount);
    properties.setProperty("login_password", payerPassword);
    properties.remove("CONTEXT");
    Object context = sendMoneyPageFields.getProperty("CONTEXT");
    if (context != null)
    {
      properties.setProperty("CONTEXT", context.toString());
    }

    String sendMoneyConfirmationPage = DoPOSTXaction(PAYPAL_WEBSCR_URL, properties, null);
    if (sendMoneyConfirmationPage == null)
    {
      return false;
    }
    if (CheckPayPalPage(sendMoneyConfirmationPage) == false)
    {
      return false;
    }
    DebugWindow.DisplayText("*** sendMoneyConfirmationPage ***\n" + sendMoneyConfirmationPage);
    Properties sendMoneyConfirmationPageFields = ExtractFormFieldsFromWebPage(sendMoneyConfirmationPage);
    sendMoneyConfirmationPageFields.list(System.out);

    // fill out the "confirmation" form; do not provide a shipping address, tell it not
    // to warn us about not providing a shipping address, and send it
    // it returns the "xaction complete" page
    properties.setProperty("cmd", "_xclick-payment-confirm-submit");
    properties.setProperty("item_name", itemDescription);
    properties.setProperty("shipping_address_present", "0");
    properties.setProperty("warn_shipping", "1"); // "1" means don't warn us about shipping
    properties.remove("CONTEXT");
    context = sendMoneyConfirmationPageFields.getProperty("CONTEXT");
    if (context != null)
    {
      properties.setProperty("CONTEXT", context.toString());
    }

    String xactionCompletePage = DoPOSTXaction(PAYPAL_WEBSCR_URL, properties, null);
    if (xactionCompletePage == null)
    {
      return false;
    }
    if (CheckPayPalPage(xactionCompletePage) == false)
    {
      return false;
    }
    System.out.println("*** xactionCompletePage ***\n" + xactionCompletePage);
    Properties xactionCompletePageFields = ExtractFormFieldsFromWebPage(xactionCompletePage);
    xactionCompletePageFields.list(System.out);

    return true;
  }

  private class PayPalHyperlinkListener
    implements HyperlinkListener
  {
    public PayPalHyperlinkListener()
    {
      super();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent event)
    {
      if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
      {
        BrowserControl.displayURL(event.getURL().toString());
      }
    }
  }

  @Override
  protected Resources GetChildResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommercePayPal.class.getName());
  }

}
