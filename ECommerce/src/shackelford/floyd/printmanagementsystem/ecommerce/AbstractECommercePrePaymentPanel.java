package shackelford.floyd.printmanagementsystem.ecommerce;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.ActionRadioButton;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AbstractECommercePrePaymentPanel</p>
 * <p>Description:
 * abstract panel for the various pre-payment implementations
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractECommercePrePaymentPanel
  extends AbstractEntry1Panel
{

  /**
   * Eclipse generated value 
   */
  private static final long serialVersionUID = 756960878539495508L;
  

  protected NumericTextField    _amountField                  = new NumericTextField();

  protected JLabel              _currencySymbolLabel          = new JLabel();

  protected JLabel              _prePaymentBalanceLabel       = new JLabel();

  protected ECommerceCreditCard _creditcard;

  protected JPanel              _creditcardPanel;

  protected ECommerceEGold      _egold;

  protected JPanel              _egoldPanel;

  protected ECommercePayPal     _paypal;

  protected JPanel              _paypalPanel;

  protected AbstractECommerce   _selectedECommerce;

  protected ActionRadioButton   _useCreditCardRadioButton;

  protected ActionRadioButton   _useEGoldRadioButton;

  protected ActionRadioButton   _usePayPalRadioButton;

  protected static final String CURRENT_PRE_PAYMENT_BALANCE   = "CURRENT_PRE_PAYMENT_BALANCE";

  protected static final String AMOUNT                        = "AMOUNT";

  protected static final String AMOUNT_TIP                    = "AMOUNT_TIP";

  protected static final String PAYMENT_MECHANISM             = "PAYMENT_MECHANISM";

  protected static final String USE_PAYPAL                    = "USE_PAYPAL";

  protected static final String USE_PAYPAL_TIP                = "USE_PAYPAL_TIP";

  protected static final String USE_EGOLD                     = "USE_EGOLD";

  protected static final String USE_EGOLD_TIP                 = "USE_EGOLD_TIP";

  protected static final String USE_CREDIT_CARD               = "USE_CREDIT_CARD";

  protected static final String USE_CREDIT_CARD_TIP           = "USE_CREDIT_CARD_TIP";

  protected static final String INVALID_AMOUNT_MSG            = "INVALID_AMOUNT_MSG";

  protected static final String INVALID_AMOUNT_TITLE          = "INVALID_AMOUNT_TITLE";

  protected static final String NO_ECOMMERCE_CAPABILITY_MSG   = "NO_ECOMMERCE_CAPABILITY_MSG";

  protected static final String NO_ECOMMERCE_CAPABILITY_TITLE = "NO_ECOMMERCE_CAPABILITY_TITLE";

  protected static final int    AMOUNT_FIELD_WIDTH            = 6;

  /**
   * Constructor
   * @param callingPanel
   */
  public AbstractECommercePrePaymentPanel(PanelListener callingPanel)
  {
    super();

    AddPanelListener(callingPanel);

    _creditcard = new ECommerceBankOfAmericaCreditCard(this);
    _paypal = new ECommercePayPal(this);
    _egold = new ECommerceEGold(this);

    Initialize(null, InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable(), UserAccountTable.GetCachedUserAccountRow());
  }

  @Override
  protected Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // current pre payment balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(CURRENT_PRE_PAYMENT_BALANCE).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    northPanel.add(_prePaymentBalanceLabel, constraints);

    // amount to refund
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(AMOUNT).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    northPanel.add(new JLabel(InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountField.setColumns(AMOUNT_FIELD_WIDTH);

    northPanel.add(_amountField, constraints);

    // payment mechanism
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(PAYMENT_MECHANISM).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _useCreditCardRadioButton = new ActionRadioButton(new UseCreditCardAction());
    _useCreditCardRadioButton.setSelected(false);
    _useCreditCardRadioButton.setEnabled(_creditcard.EnabledForRefund());
    northPanel.add(_useCreditCardRadioButton, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _usePayPalRadioButton = new ActionRadioButton(new UsePayPalAction());
    _usePayPalRadioButton.setSelected(false);
    _usePayPalRadioButton.setEnabled(_paypal.EnabledForRefund());
    northPanel.add(_usePayPalRadioButton, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _useEGoldRadioButton = new ActionRadioButton(new UseEGoldAction());
    _useEGoldRadioButton.setSelected(false);
    _useEGoldRadioButton.setEnabled(_egold.EnabledForRefund());
    northPanel.add(_useEGoldRadioButton, constraints);

    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_useCreditCardRadioButton);
    buttonGroup.add(_usePayPalRadioButton);
    buttonGroup.add(_useEGoldRadioButton);

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    centerPanel.setBorder(BorderFactory.createEtchedBorder());

    _creditcardPanel = _creditcard.GetRefundDataEntryPanel();
    _paypalPanel = _paypal.GetRefundDataEntryPanel();
    _egoldPanel = _egold.GetRefundDataEntryPanel();

    JPanel initialPanel = null;

    if (_creditcard.EnabledForRefund() == true)
    {
      _useCreditCardRadioButton.setSelected(true);
      _selectedECommerce = _creditcard;
      initialPanel = _creditcardPanel;
    }
    else
      if ((_paypal != null) && (_paypal.EnabledForRefund() == true))
      {
        _usePayPalRadioButton.setSelected(true);
        _selectedECommerce = _paypal;
        initialPanel = _paypalPanel;
      }
      else
        if ((_egold != null) && (_egold.EnabledForRefund() == true))
        {
          _useEGoldRadioButton.setSelected(true);
          _selectedECommerce = _egold;
          initialPanel = _egoldPanel;
        }
        else
        {
          _selectedECommerce = null;
          MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(NO_ECOMMERCE_CAPABILITY_MSG), GetResources().getProperty(NO_ECOMMERCE_CAPABILITY_TITLE));
        }
    if (initialPanel != null)
    {
      centerPanel.add(initialPanel);
    }

    return centerPanel;
  }

  @Override
  protected boolean FieldsValid()
  {
    double amount = 0d;
    try
    {
      amount = Double.parseDouble(_amountField.getText());
    }
    catch (NumberFormatException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_AMOUNT_MSG), GetResources().getProperty(INVALID_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    if (amount <= 0d)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_AMOUNT_MSG), GetResources().getProperty(INVALID_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    _prePaymentBalanceLabel.setText(GetUserAccountRow().Get_pre_payment_balance().toString());
    _currencySymbolLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol());
    _amountField.setText("0.00");
  }

  @Override
  protected void AssignFieldsToRow()
  {
    // do nothing
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return GetUserAccountTable().GetInstallationDatabase();
  }

  protected UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable) _sqlTable;
  }

  protected UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  protected InstallationTable GetInstallationTable()
  {
    return GetInstallationDatabase().GetInstallationTable();
  }

  protected void UseCreditCardAction()
  {
    JPanel panel = (JPanel) _centerPanel;
    Rectangle rect = panel.getVisibleRect();
    panel.removeAll();
    panel.repaint(rect);
    panel.add(_creditcardPanel);
    panel.validate();
    _selectedECommerce = _creditcard;
    pack();
  }

  protected class UseCreditCardAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -484656215821039115L;

    /**
     * 
     */
    

    public UseCreditCardAction()
    {
      putValue(Action.NAME, GetResources().getProperty(USE_CREDIT_CARD));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(USE_CREDIT_CARD_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      UseCreditCardAction();
    }
  }

  protected void UsePayPalAction()
  {
    JPanel panel = (JPanel) _centerPanel;
    Rectangle rect = panel.getVisibleRect();
    panel.removeAll();
    panel.repaint(rect);
    panel.add(_paypalPanel);
    panel.validate();
    _selectedECommerce = _paypal;
    pack();
  }

  protected class UsePayPalAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 1789146204644183839L;

    /**
     * 
     */
    

    public UsePayPalAction()
    {
      putValue(Action.NAME, GetResources().getProperty(USE_PAYPAL));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(USE_PAYPAL_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      UsePayPalAction();
    }
  }

  protected void UseEGoldAction()
  {
    JPanel panel = (JPanel) _centerPanel;
    Rectangle rect = panel.getVisibleRect();
    panel.removeAll();
    panel.repaint(rect);
    panel.add(_egoldPanel);
    panel.validate();
    _selectedECommerce = _egold;
    pack();
  }

  protected class UseEGoldAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 6561451161509567161L;

    /**
     * 
     */
    

    public UseEGoldAction()
    {
      putValue(Action.NAME, GetResources().getProperty(USE_EGOLD));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(USE_EGOLD_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      UseEGoldAction();
    }
  }

}
