package shackelford.floyd.printmanagementsystem.ecommerce;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.MessageFormat;
import java.util.Calendar;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerDateModel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;




/**
 * <p>Title: ECommerceCreditCard</p>
 * <p>Description:
 * ecommerce implementation for a credit card
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class ECommerceCreditCard
  extends AbstractECommerce
{

  protected OverwritableTextField      _billingFirstNameTextField;
  protected OverwritableTextField      _billingLastNameTextField;
  protected OverwritableTextField      _billingAddressStreet1TextField;
  protected OverwritableTextField      _billingAddressStreet2TextField;
  protected OverwritableTextField      _billingAddressCityTextField;
  protected OverwritableTextField      _billingAddressStateProvinceTextField;
  protected OverwritableTextField      _billingAddressPostalCodeTextField;
  protected OverwritableTextField      _billingPhoneNumberTextField;
  protected OverwritableTextField      _emailTextField;
  protected OverwritableTextField      _creditcardNumberTextField;
  protected JSpinner                   _expirationDateMonthSpinField;
  protected JSpinner                   _expirationDateYearSpinField;
  protected OverwritableTextField      _nameOnCardTextField;
  protected _JComboBox                _billingAddressCountryComboBox;
  //protected OverwritableTextField      _cvvValueTextField;
  //protected MyJComboBox                _cvvIndicatorComboBox;
  //protected MyJComboBox                _creditCardTypeComboBox;

  private static Resources             __resources;
  protected static String[]            __cvvIndicators;
  //protected static String[]            __creditCardTypes;

  protected static final String        CREDITCARD_NUMBER = "CREDITCARD_NUMBER";
  protected static final String        CREDITCARD_NUMBER_TOOL_TIP = "CREDITCARD_NUMBER_TOOL_TIP";
  protected static final String        EXPIRATION_DATE = "EXPIRATION_DATE";
  protected static final String        MONTH = "MONTH";
  protected static final String        MONTH_TOOL_TIP = "MONTH_TOOL_TIP";
  protected static final String        YEAR = "YEAR";
  protected static final String        YEAR_TOOL_TIP = "YEAR_TOOL_TIP";
  protected static final String        NAME_ON_CARD = "NAME_ON_CARD";
  protected static final String        NAME_ON_CARD_TOOL_TIP = "NAME_ON_CARD_TOOL_TIP";
  protected static final String        BILLING_FIRST_NAME = "BILLING_FIRST_NAME";
  protected static final String        BILLING_FIRST_NAME_TOOL_TIP = "BILLING_FIRST_NAME_TOOL_TIP";
  protected static final String        BILLING_LAST_NAME = "BILLING_LAST_NAME";
  protected static final String        BILLING_LAST_NAME_TOOL_TIP = "BILLING_LAST_NAME_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_STREET_1 = "BILLING_ADDRESS_STREET_1";
  protected static final String        BILLING_ADDRESS_STREET_1_TOOL_TIP = "BILLING_ADDRESS_STREET_1_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_STREET_2 = "BILLING_ADDRESS_STREET_2";
  protected static final String        BILLING_ADDRESS_STREET_2_TOOL_TIP = "BILLING_ADDRESS_STREET_2_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_CITY = "BILLING_ADDRESS_CITY";
  protected static final String        BILLING_ADDRESS_CITY_TOOL_TIP = "BILLING_ADDRESS_CITY_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_STATE_PROVINCE = "BILLING_ADDRESS_STATE_PROVINCE";
  protected static final String        BILLING_ADDRESS_STATE_PROVINCE_TOOL_TIP = "BILLING_ADDRESS_STATE_PROVINCE_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_POSTAL_CODE = "BILLING_ADDRESS_POSTAL_CODE";
  protected static final String        BILLING_ADDRESS_POSTAL_CODE_TOOL_TIP = "BILLING_ADDRESS_POSTAL_CODE_TOOL_TIP";
  protected static final String        BILLING_ADDRESS_COUNTRY = "BILLING_ADDRESS_COUNTRY";
  protected static final String        BILLING_ADDRESS_COUNTRY_TOOL_TIP = "BILLING_ADDRESS_COUNTRY_TOOL_TIP";
  protected static final String        BILLING_PHONE_NUMBER = "BILLING_PHONE_NUMBER";
  protected static final String        BILLING_PHONE_NUMBER_TOOL_TIP = "BILLING_PHONE_NUMBER_TOOL_TIP";
  protected static final String        BILLING_EMAIL = "BILLING_EMAIL";
  protected static final String        BILLING_EMAIL_TOOL_TIP = "BILLING_EMAIL_TOOL_TIP";
  protected static final String        CVV_INDICATOR = "CVV_INDICATOR";
  protected static final String        CVV_INDICATOR_TOOL_TIP = "CVV_INDICATOR_TOOL_TIP";
  protected static final String        CVV_VALUE = "CVV_VALUE";
  protected static final String        CVV_VALUE_TOOL_TIP = "CVV_VALUE_TOOL_TIP";

  protected static final String        MINIMUM_AMOUNT_MSG = "MINIMUM_AMOUNT_MSG";
  protected static final String        AMOUNT_TOO_SMALL = "AMOUNT_TOO_SMALL";
  protected static final String        AMOUNT_TOO_SMALL_TITLE = "AMOUNT_TOO_SMALL_TITLE";
  protected static final String        AMOUNT_TOO_LARGE = "AMOUNT_TOO_LARGE";
  protected static final String        AMOUNT_TOO_LARGE_TITLE = "AMOUNT_TOO_LARGE_TITLE";
  protected static final String        CREDITCARD_NUMBER_MISSING = "CREDITCARD_NUMBER_MISSING";
  protected static final String        CREDITCARD_NUMBER_MISSING_TITLE = "CREDITCARD_NUMBER_MISSING_TITLE";
  protected static final String        NAME_MISSING = "NAME_MISSING";
  protected static final String        NAME_MISSING_TITLE = "NAME_MISSING_TITLE";
  protected static final String        BILLING_FIRST_NAME_MISSING = "BILLING_FIRST_NAME_MISSING";
  protected static final String        BILLING_FIRST_NAME_MISSING_TITLE = "BILLING_FIRST_NAME_MISSING_TITLE";
  protected static final String        BILLING_LAST_NAME_MISSING = "BILLING_LAST_NAME_MISSING";
  protected static final String        BILLING_LAST_NAME_MISSING_TITLE = "BILLING_LAST_NAME_MISSING_TITLE";
  protected static final String        BILLING_ADDRESS_STREET_1_MISSING = "BILLING_ADDRESS_STREET_1_MISSING";
  protected static final String        BILLING_ADDRESS_STREET_1_MISSING_TITLE = "BILLING_ADDRESS_STREET_1_MISSING_TITLE";
  protected static final String        BILLING_ADDRESS_CITY_MISSING = "BILLING_ADDRESS_CITY_MISSING";
  protected static final String        BILLING_ADDRESS_CITY_MISSING_TITLE = "BILLING_ADDRESS_CITY_MISSING_TITLE";
  protected static final String        BILLING_ADDRESS_STATE_PROVINCE_MISSING = "BILLING_ADDRESS_STATE_PROVINCE_MISSING";
  protected static final String        BILLING_ADDRESS_STATE_PROVINCE_MISSING_TITLE = "BILLING_ADDRESS_STATE_PROVINCE_MISSING_TITLE";
  protected static final String        BILLING_ADDRESS_POSTAL_CODE_MISSING = "BILLING_ADDRESS_POSTAL_CODE_MISSING";
  protected static final String        BILLING_ADDRESS_POSTAL_CODE_MISSING_TITLE = "BILLING_ADDRESS_POSTAL_CODE_MISSING_TITLE";
  protected static final String        INVALID_EXPIRATION_MONTH = "INVALID_EXPIRATION_MONTH";
  protected static final String        INVALID_EXPIRATION_MONTH_TITLE = "INVALID_EXPIRATION_MONTH_TITLE";
  protected static final String        INVALID_EXPIRATION_YEAR = "INVALID_EXPIRATION_YEAR";
  protected static final String        INVALID_EXPIRATION_YEAR_TITLE = "INVALID_EXPIRATION_YEAR_TITLE";
  protected static final String        CHARGE_WAIT_PANEL_INITIAL_MESSAGE = "CHARGE_WAIT_PANEL_INITIAL_MESSAGE";
  protected static final String        CHARGE_WAIT_PANEL_TITLE = "CHARGE_WAIT_PANEL_TITLE";
  protected static final String        REFUND_WAIT_PANEL_INITIAL_MESSAGE = "REFUND_WAIT_PANEL_INITIAL_MESSAGE";
  protected static final String        REFUND_WAIT_PANEL_TITLE = "REFUND_WAIT_PANEL_TITLE";
  protected static final String        CVV_PRESENT = "CVV_PRESENT";
  protected static final String        CVV_NOT_PRESENT = "CVV_NOT_PRESENT";
  protected static final String        CVV_UNREADABLE = "CVV_UNREADABLE";
  //protected static final String        CREDIT_CARD_TYPE = "CREDIT_CARD_TYPE";
  //protected static final String        VISA = "VISA";
  //protected static final String        MASTERCARD = "MASTERCARD";
  //protected static final String        DISCOVER = "DISCOVER";
  //protected static final String        DINERS_CLUB = "DINERS_CLUB";
  //protected static final String        AMERICAN_EXPRESS = "AMERICAN_EXPRESS";

  protected static final String        CREDITCARD = "Credit Card ";

  protected static final int           CREDITCARD_FIELD_WIDTH = 19;
  protected static final int           CITY_FIELD_WIDTH = 19;
  protected static final int           STATE_FIELD_WIDTH = 4;
  protected static final int           POSTAL_CODE_FIELD_WIDTH = 10;
  protected static final int           CVV_VALUE_FIELD_WIDTH = 4;
  protected static final int           MONTH_FIELD_WIDTH = 2;
  protected static final int           YEAR_FIELD_WIDTH = 4;
  protected static final int           LAST_NAME_FIELD_WIDTH = 20;
  protected static final int           FIRST_NAME_FIELD_WIDTH = 20;
  protected static final int           NAME_FIELD_WIDTH = FIRST_NAME_FIELD_WIDTH + LAST_NAME_FIELD_WIDTH;
  protected static final int           ADDRESS_FIELD_WIDTH = 40;
  protected static final int           PHONE_NUMBER_FIELD_WIDTH = 20;
  protected static final int           EMAIL_FIELD_WIDTH = 20;


  public ECommerceCreditCard(JFrame parentFrame)
  {
    super(parentFrame);
  }

  @Override
  protected String GetSource ()
  {
    return CREDITCARD + _creditcardNumberTextField.getText().trim();
  }

  @Override
  protected boolean EnabledForCharge()
  {
    if (InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim().length() == 0)
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildChargeDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // minimum amount message
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.CENTER;

    // MINIMUM_AMOUNT_MSG format:
    //   {0} = currency symbol
    //   {1,number,currency} = minimum amount

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MINIMUM_AMOUNT_MSG));
    Object[] substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_currency_symbol(),
                                                 InstallationTable.GetCachedInstallationRow().Get_cc_charge_minimum_amount() };
    JLabel label = new JLabel(msgFormat.format(substitutionValues).trim(), SwingConstants.LEFT);
    aPanel.add(label, constraints);
/*
    // credit card type
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CREDIT_CARD_TYPE),JLabel.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditCardTypeComboBox = new JComboBox(creditCardTypes);
    _creditCardTypeComboBox.setEditable(false);
    aPanel.add(_creditCardTypeComboBox,constraints);
*/
    // credit card number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CREDITCARD_NUMBER),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.REMAINDER;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardNumberTextField = new OverwritableTextField(CREDITCARD_FIELD_WIDTH);
    _creditcardNumberTextField.setToolTipText(__resources.getProperty(CREDITCARD_NUMBER_TOOL_TIP));
    aPanel.add (_creditcardNumberTextField,constraints);

    // expiration date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(EXPIRATION_DATE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(MONTH),SwingConstants.LEFT),
      constraints );

    constraints.gridx = 2;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _expirationDateMonthSpinField = new JSpinner();
    _expirationDateMonthSpinField.setToolTipText(__resources.getProperty(MONTH_TOOL_TIP));

    SpinnerDateModel spinnerDateModel = new SpinnerDateModel();
    _expirationDateMonthSpinField.setModel(spinnerDateModel);
    spinnerDateModel.setCalendarField(Calendar.MONTH);
    spinnerDateModel.setStart(1);
    spinnerDateModel.setEnd(12);
    spinnerDateModel.setValue(1);

    aPanel.add (_expirationDateMonthSpinField,constraints);

    constraints.gridx = 3;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(YEAR),SwingConstants.LEFT),
      constraints );

    constraints.gridx = 4;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _expirationDateYearSpinField = new JSpinner();
    _expirationDateYearSpinField.setToolTipText(__resources.getProperty(YEAR_TOOL_TIP));

    spinnerDateModel = new SpinnerDateModel();
    _expirationDateYearSpinField.setModel(spinnerDateModel);
    Calendar calendar = Calendar.getInstance();
    spinnerDateModel.setCalendarField(Calendar.YEAR);
    spinnerDateModel.setStart(calendar.get(Calendar.YEAR));
    spinnerDateModel.setEnd(calendar.get(Calendar.YEAR) + 4);
    spinnerDateModel.setValue(calendar.get(Calendar.YEAR));

    aPanel.add (_expirationDateYearSpinField,constraints);

    // name on credit card
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(NAME_ON_CARD),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _nameOnCardTextField = new OverwritableTextField(NAME_FIELD_WIDTH);
    _nameOnCardTextField.setToolTipText(__resources.getProperty(NAME_ON_CARD_TOOL_TIP));
    aPanel.add (_nameOnCardTextField,constraints);

    // billing first name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_FIRST_NAME),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingFirstNameTextField = new OverwritableTextField(FIRST_NAME_FIELD_WIDTH);
    _billingFirstNameTextField.setToolTipText(__resources.getProperty(BILLING_FIRST_NAME_TOOL_TIP));
    aPanel.add (_billingFirstNameTextField,constraints);

    // billing last name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_LAST_NAME),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingLastNameTextField = new OverwritableTextField(LAST_NAME_FIELD_WIDTH);
    _billingLastNameTextField.setToolTipText(__resources.getProperty(BILLING_LAST_NAME_TOOL_TIP));
    aPanel.add (_billingLastNameTextField,constraints);

    // billing address street 1
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_STREET_1),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressStreet1TextField = new OverwritableTextField(ADDRESS_FIELD_WIDTH);
    _billingAddressStreet1TextField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_STREET_1_TOOL_TIP));
    aPanel.add (_billingAddressStreet1TextField,constraints);

    // billing address street 2
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_STREET_2),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressStreet2TextField = new OverwritableTextField(ADDRESS_FIELD_WIDTH);
    _billingAddressStreet2TextField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_STREET_2_TOOL_TIP));
    aPanel.add (_billingAddressStreet2TextField,constraints);

    // billing address city
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_CITY),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressCityTextField = new OverwritableTextField(CITY_FIELD_WIDTH);
    _billingAddressCityTextField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_CITY_TOOL_TIP));
    aPanel.add (_billingAddressCityTextField,constraints);

    // billing address state / province
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_STATE_PROVINCE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressStateProvinceTextField = new OverwritableTextField(STATE_FIELD_WIDTH);
    _billingAddressStateProvinceTextField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_STATE_PROVINCE_TOOL_TIP));
    aPanel.add (_billingAddressStateProvinceTextField,constraints);

    // billing address postal code
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_POSTAL_CODE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressPostalCodeTextField = new OverwritableTextField(POSTAL_CODE_FIELD_WIDTH);
    _billingAddressPostalCodeTextField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_POSTAL_CODE_TOOL_TIP));
    aPanel.add (_billingAddressPostalCodeTextField,constraints);

    // billing address country
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS_COUNTRY),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressCountryComboBox = new _JComboBox(Resources.GetCountries());
    _billingAddressCountryComboBox.setToolTipText(__resources.getProperty(BILLING_ADDRESS_COUNTRY_TOOL_TIP));
    aPanel.add (_billingAddressCountryComboBox,constraints);

    // billing address phone number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_PHONE_NUMBER),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingPhoneNumberTextField = new OverwritableTextField(PHONE_NUMBER_FIELD_WIDTH);
    _billingPhoneNumberTextField.setToolTipText(__resources.getProperty(BILLING_PHONE_NUMBER_TOOL_TIP));
    aPanel.add (_billingPhoneNumberTextField,constraints);

    // billing e-mail
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(BILLING_EMAIL),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _emailTextField = new OverwritableTextField(EMAIL_FIELD_WIDTH);
    _emailTextField.setToolTipText(__resources.getProperty(BILLING_EMAIL_TOOL_TIP));
    aPanel.add (_emailTextField,constraints);

/*
    // cvv indicator
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CVV_INDICATOR),JLabel.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _cvvIndicatorComboBox = new JComboBox(cvvIndicators);
    _cvvIndicatorComboBox.setToolTipText(__resources.getProperty(CVV_INDICATOR_TOOL_TIP));
    aPanel.add (_cvvIndicatorComboBox,constraints);

    // cvv value
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CVV_VALUE),JLabel.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 6;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _cvvValueTextField = new OverwritableTextField(CVV_VALUE_FIELD_WIDTH);
    _cvvValueTextField.setToolTipText(__resources.getProperty(CVV_VALUE_TOOL_TIP));
    aPanel.add (_cvvValueTextField,constraints);
*/

    return aPanel;
  }

  @Override
  protected boolean ProcessChargeDataEntryPanel()
  {
    // credit card number must be entered
    if (_creditcardNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(CREDITCARD_NUMBER_MISSING),
        __resources.getProperty(CREDITCARD_NUMBER_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // month must be between 1 and 12, inclusive
    int expMonth = (Integer)(_expirationDateMonthSpinField.getValue());
    if ( ( expMonth < 1 ) || ( expMonth > 12 ) )
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(INVALID_EXPIRATION_MONTH),
        __resources.getProperty(INVALID_EXPIRATION_MONTH_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // year must be this year or up to 4 years later
    int expYear = (Integer)(_expirationDateYearSpinField.getValue());
    int currentYear = Calendar.getInstance().get(Calendar.YEAR);
    if ( ( expYear < currentYear ) || ( expYear > (currentYear + 4) ) )
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(INVALID_EXPIRATION_YEAR),
        __resources.getProperty(INVALID_EXPIRATION_YEAR_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // name must be entered
    if (_nameOnCardTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(NAME_MISSING),
        __resources.getProperty(NAME_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _nameOnCardTextField.requestFocus();
      return false;
    }
    // billing info must be entered
    if (_billingFirstNameTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_FIRST_NAME_MISSING),
        __resources.getProperty(BILLING_FIRST_NAME_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingFirstNameTextField.requestFocus();
      return false;
    }
    if (_billingLastNameTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_LAST_NAME_MISSING),
        __resources.getProperty(BILLING_LAST_NAME_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingLastNameTextField.requestFocus();
      return false;
    }
    if (_billingAddressStreet1TextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_ADDRESS_STREET_1_MISSING),
        __resources.getProperty(BILLING_ADDRESS_STREET_1_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingAddressStreet1TextField.requestFocus();
      return false;
    }
    if (_billingAddressCityTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_ADDRESS_CITY_MISSING),
        __resources.getProperty(BILLING_ADDRESS_CITY_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingAddressCityTextField.requestFocus();
      return false;
    }
    if (_billingAddressStateProvinceTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_ADDRESS_STATE_PROVINCE_MISSING),
        __resources.getProperty(BILLING_ADDRESS_STATE_PROVINCE_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingAddressStateProvinceTextField.requestFocus();
      return false;
    }
    if (_billingAddressPostalCodeTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(BILLING_ADDRESS_POSTAL_CODE_MISSING),
        __resources.getProperty(BILLING_ADDRESS_POSTAL_CODE_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      _billingAddressPostalCodeTextField.requestFocus();
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidChargeAmount()
  {
    double minimumAmount = InstallationTable.GetCachedInstallationRow().Get_cc_charge_minimum_amount().doubleValue();
    if (minimumAmount > GetAmount())
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(AMOUNT_TOO_SMALL),
        __resources.getProperty(AMOUNT_TOO_SMALL_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateChargeFee()
  {
    double fee;
    fee =
      InstallationTable.GetCachedInstallationRow().Get_cc_charge_xaction_fee().doubleValue() +
      ( ( InstallationTable.GetCachedInstallationRow().Get_cc_charge_xaction_fee_pcent().doubleValue() * GetAmount() ) / 100.0d );

    return fee;
  }

  @Override
  protected String GetChargeWaitPanelTitle()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_TITLE);
  }

  @Override
  protected String GetChargeWaitPanelInitialMessage()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_INITIAL_MESSAGE);
  }

  @Override
  protected boolean EnabledForRefund()
  {
    if (InstallationTable.GetCachedInstallationRow().Get_cc_refund_account().trim().length() == 0)
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildRefundDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;
/*
    // credit card type
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CREDIT_CARD_TYPE),JLabel.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditCardTypeComboBox = new JComboBox(creditCardTypes);
    _creditCardTypeComboBox.setEditable(false);
    aPanel.add(_creditCardTypeComboBox,constraints);
*/
    // credit card number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add (
      new JLabel(__resources.getProperty(CREDITCARD_NUMBER),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.REMAINDER;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardNumberTextField = new OverwritableTextField(CREDITCARD_FIELD_WIDTH);
    _creditcardNumberTextField.setToolTipText(__resources.getProperty(CREDITCARD_NUMBER_TOOL_TIP));
    aPanel.add (_creditcardNumberTextField,constraints);

    return aPanel;
  }

  @Override
  protected boolean ProcessRefundDataEntryPanel()
  {
    // credit card number must be entered
    if (_creditcardNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(CREDITCARD_NUMBER_MISSING),
        __resources.getProperty(CREDITCARD_NUMBER_MISSING_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidRefundAmount()
  {
    if (GetAmount() <= 0d)
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(AMOUNT_TOO_SMALL),
        __resources.getProperty(AMOUNT_TOO_SMALL_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (GetAmount() > UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue())
    {
      JOptionPane.showMessageDialog (
        GetParentFrame(),
        __resources.getProperty(AMOUNT_TOO_LARGE),
        __resources.getProperty(AMOUNT_TOO_LARGE_TITLE),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateRefundFee()
  {
    double fee =
      InstallationTable.GetCachedInstallationRow().Get_cc_charge_xaction_fee().doubleValue() +
      ( ( InstallationTable.GetCachedInstallationRow().Get_cc_charge_xaction_fee_pcent().doubleValue() * GetAmount() ) / 100.0d );

    return fee;
  }

  @Override
  protected String GetRefundWaitPanelTitle()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_TITLE);
  }

  @Override
  protected String GetRefundWaitPanelInitialMessage()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_INITIAL_MESSAGE);
  }

  @Override
  protected Resources GetChildResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommerceCreditCard.class.getName());
/*
    if (creditCardTypes == null)
    {
      creditCardTypes = new String[] {
                              __resources.getProperty(VISA),
                              __resources.getProperty(MASTERCARD),
                              __resources.getProperty(DISCOVER),
                              __resources.getProperty(DINERS_CLUB),
                              __resources.getProperty(AMERICAN_EXPRESS) };
    }
*/
    __cvvIndicators = new String[] {
                            __resources.getProperty(CVV_PRESENT),
                            __resources.getProperty(CVV_NOT_PRESENT),
                            __resources.getProperty(CVV_UNREADABLE) };
  }

}