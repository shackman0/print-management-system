package shackelford.floyd.printmanagementsystem.ecommerce;

import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.Resources;


/**
 * <p>Title: ECommerceAddPrePaymentPanel</p>
 * <p>Description:
 * panel to let the user add a pre-payment to his account or stored value card
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommerceAddPrePaymentPanel
  extends AbstractECommercePrePaymentPanel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 5468030621158372187L;
  

  private static Resources  __resources;

  /**
   * Constructor
   * @param callingPanel
   */
  public ECommerceAddPrePaymentPanel(PanelListener callingPanel)
  {
    super(callingPanel);
  }

  @Override
  protected boolean FieldsValid()
  {
    if (super.FieldsValid() == false)
    {
      return false;
    }

    double amount = new Double(_amountField.getText()).doubleValue();
    return _selectedECommerce.DoECommerceCharge(amount);
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommerceAddPrePaymentPanel.class.getName());
  }
}
