package shackelford.floyd.printmanagementsystem.ecommerce;

import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.PrintingMsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;



/**
 * <p>Title: ECommerceAddPrePaymentCompletePanel</p>
 * <p>Description:
 * tells the user that his pre-payment has been accepted and applied to his account
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommerceAddPrePaymentCompletePanel
  extends PrintingMsgBox
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -8607963314899347515L;
  
  private	double                  _amount;
  private double                  _originalBalance;
  private	PrePaymentHistoryRow    _prePaymentHistoryRow = null;
  private JLabel                  _userAccountIDLabel = new JLabel();
  private JLabel                  _originalBalanceLabel = new JLabel();
  private JLabel                  _amountLabel = new JLabel();
  private JLabel                  _newBalanceLabel = new JLabel();
  private JLabel                  _dateTimeStampLabel = new JLabel();

  private static Resources        __resources;

  private static final String     USER_ACCOUNT_ID = "USER_ACCOUNT_ID";
//  private static final String     USER_ACCOUNT_NAME = "USER_ACCOUNT_NAME";
  private static final String     PREVIOUS_BALANCE = "PREVIOUS_BALANCE";
  private static final String     AMOUNT_ADDED = "AMOUNT_ADDED";
  private static final String     NEW_BALANCE = "NEW_BALANCE";
  private static final String     MSG = "MSG";
  private static final String     TIME_STAMP = "TIME_STAMP";


  /**
   * Constructor
   * @param originalBalance
   * @param amount
   * @param prePaymentHistoryRow
   */
  public ECommerceAddPrePaymentCompletePanel (
    double                 originalBalance,
    double                 amount,
    PrePaymentHistoryRow   prePaymentHistoryRow )
  {
    super();

    _originalBalance = originalBalance;
    _amount = amount;
    _prePaymentHistoryRow = prePaymentHistoryRow;

    Initialize(__resources.getProperty(MSG),__resources.getProperty(TITLE));
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(USER_ACCOUNT_ID).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _userAccountIDLabel,
      constraints );

    // previous balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(PREVIOUS_BALANCE).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _originalBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _originalBalanceLabel,
      constraints );

    // amount added
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(AMOUNT_ADDED).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _amountLabel,
      constraints );

    // new balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(NEW_BALANCE).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _newBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _newBalanceLabel,
      constraints );

    // date-time stamp
    constraints.gridy += 1;

    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(TIME_STAMP).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _dateTimeStampLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _dateTimeStampLabel,
      constraints );

    return centerPanel;
  }

  @Override
  protected void AssignDataToFields()
  {
    super.AssignDataToFields();

    _userAccountIDLabel.setText(_prePaymentHistoryRow.Get_user_account_id());
    _originalBalanceLabel.setText(String.valueOf(_originalBalance));
    _amountLabel.setText(String.valueOf(_amount));
    _newBalanceLabel.setText(String.valueOf(_originalBalance - _amount));
    _dateTimeStampLabel.setText(_prePaymentHistoryRow.Get_created_dts());
  }

  @Override
  protected int PrintAction_Print (
    Graphics2D  g2D,
    PageFormat  pageFormat,
    int         pageNumber )
  {
    // remember: page numbers are zero (0) relative!
    if (pageNumber > 0)
    {
      return Printable.NO_SUCH_PAGE;
    }

    g2D.translate(pageFormat.getImageableX(),pageFormat.getImageableY());

    float x1 = 0.0f;
    float	x2 = 200.0f;
    float y = 0.0f;
    float incr = 20.0f;

    // title
    y += incr;
    g2D.drawString(__resources.getProperty(TITLE),x1,y);

    // message 1
    y += incr;
    g2D.drawString(__resources.getProperty(MSG),x1,y);

    // acct id
    y += (incr * 2.0f);
    g2D.drawString(__resources.getProperty(USER_ACCOUNT_ID),x1,y);
    g2D.drawString(_prePaymentHistoryRow.Get_user_account_id(),x2,y);

    // previous balance
    y += incr;
    g2D.drawString(__resources.getProperty(PREVIOUS_BALANCE),x1,y);
    g2D.drawString(String.valueOf(_originalBalance),x2,y);

    // amount added
    y += incr;
    g2D.drawString(__resources.getProperty(AMOUNT_ADDED),x1,y);
    g2D.drawString(String.valueOf(_amount),x2,y);

    // new balance
    y += incr;
    g2D.drawString(__resources.getProperty(NEW_BALANCE),x1,y);
    g2D.drawString(String.valueOf(_originalBalance + _amount),x2,y);

    // dts
    y += incr;
    g2D.drawString(__resources.getProperty(TIME_STAMP),x1,y);
    g2D.drawString(_prePaymentHistoryRow.Get_created_dts(),x2,y);

    return Printable.PAGE_EXISTS;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommerceAddPrePaymentCompletePanel.class.getName());
  }

}