package shackelford.floyd.printmanagementsystem.ecommerce;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;

import shackelford.floyd.printmanagementsystem.common.BrowserControl;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: ECommerceEGold</p>
 * <p>Description:
 * ecommerce implementation for E Gold
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ECommerceEGold
  extends AbstractECommerce
{

  private OverwritableTextField          _egoldAccountNumberTextField;

  private JEditorPane                    _egoldEditorPane;

  private JPasswordField                 _egoldPasswordField;

  private _JComboBox                     _metalsComboBox;

  private static Resources               __resources;

  private static HashMap<String, String> __currencyToEGoldCurrencyCodeMap;

  private static final String            EGOLD_ACCOUNT_NUMBER              = "EGOLD_ACCOUNT_NUMBER";

  private static final String            EGOLD_ACCOUNT_NUMBER_TOOL_TIP     = "EGOLD_ACCOUNT_NUMBER_TOOL_TIP";

  private static final String            EGOLD_PASSWORD                    = "EGOLD_PASSWORD";

  private static final String            EGOLD_PASSWORD_TOOL_TIP           = "EGOLD_PASSWORD_TOOL_TIP";

  private static final String            CHOOSE_METAL                      = "CHOOSE_METAL";

  //  private static final String     CHOOSE_METAL_TOOL_TIP = "CHOOSE_METAL_TOOL_TIP";
  private static final String            MINIMUM_AMOUNT_MSG                = "MINIMUM_AMOUNT_MSG";

  private static final String            AMOUNT_TOO_SMALL                  = "AMOUNT_TOO_SMALL";

  private static final String            AMOUNT_TOO_SMALL_TITLE            = "AMOUNT_TOO_SMALL_TITLE";

  private static final String            AMOUNT_TOO_LARGE                  = "AMOUNT_TOO_LARGE";

  private static final String            AMOUNT_TOO_LARGE_TITLE            = "AMOUNT_TOO_LARGE_TITLE";

  private static final String            ACCOUNT_NUMBER_MISSING            = "ACCOUNT_NUMBER_MISSING";

  private static final String            ACCOUNT_NUMBER_MISSING_TITLE      = "ACCOUNT_NUMBER_MISSING_TITLE";

  private static final String            PASSWORD_MISSING                  = "PASSWORD_MISSING";

  private static final String            PASSWORD_MISSING_TITLE            = "PASSWORD_MISSING_TITLE";

  private static final String            CHARGE_MEMO                       = "CHARGE_MEMO";

  private static final String            REFUND_MEMO                       = "REFUND_MEMO";

  private static final String            SIGN_UP_NOW                       = "SIGN_UP_NOW";

  private static final String            CHARGE_WAIT_PANEL_INITIAL_MESSAGE = "CHARGE_WAIT_PANEL_INITIAL_MESSAGE";

  private static final String            CHARGE_WAIT_PANEL_TITLE           = "CHARGE_WAIT_PANEL_TITLE";

  private static final String            REFUND_WAIT_PANEL_INITIAL_MESSAGE = "REFUND_WAIT_PANEL_INITIAL_MESSAGE";

  private static final String            REFUND_WAIT_PANEL_TITLE           = "REFUND_WAIT_PANEL_TITLE";

  private static final String            EGOLD                             = "EGold ";

  private static final String            EGOLD_SPEND_SCRIPT                = "acct/confirm.asp";

  private static final String[]          METALS                            = { "Gold", "Silver", "Platinum", "Palladium" };

  private static final int               EGOLD_ACCOUNT_NUMBER_FIELD_WIDTH  = 8;

  private static final int               EGOLD_PASSWORD_FIELD_WIDTH        = 16;

  private static final String            EGOLD_LINK_HTML                   = "<a href=\"https://www.e-gold.com/newacct/\">" + "<img src=\"http://www.e-gold.com/gif/logo.gif\" "
                                                                             + "width=\"105\" height=\"45\" name=\"logo\" alt=\"e-gold logo\" vspace=\"0\" hspace=\"0\" border=\"0\"></a>";

  private static final String[]          CURRENCIES                        = { "USD", "CAD", "FRF", "CHF", "GBP", "DEM", "AUD", "JPY", "grams", "troy ounces" };

  private static final String[]          EGOLD_CURRENCY_CODES              = { "1", // US Dollars
    "2", // Canadian Dollars
    "33", // French Francs
    "41", // Swiss Francs
    "44", // British Pounds
    "49", // Deutsche Marks
    "61", // Australian Dollars
    "81", // Japanese Yen
    "8888", // grams
    "9999"                                                                };                                                                                                              // ounces (troy)

  public ECommerceEGold(JFrame parentFrame)
  {
    super(parentFrame);
  }

  @Override
  protected String GetSource()
  {
    return EGOLD + _egoldAccountNumberTextField.getText().trim();
  }

  @Override
  protected boolean EnabledForCharge()
  {
    if ((InstallationTable.GetCachedInstallationRow().Get_egold_charge_account().trim().length() == 0)
      || (ECommerceEGold.__currencyToEGoldCurrencyCodeMap.get(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().trim()) == null))
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildChargeDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // minimum amount message
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.CENTER;

    // MINIMUM_AMOUNT_MSG format:
    //   {0} = currency symbol
    //   {1} = minimum amount

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MINIMUM_AMOUNT_MSG).toString());
    Object[] substitutionValues = new Object[] { InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), InstallationTable.GetCachedInstallationRow().Get_egold_charge_minimum_amount() };
    JLabel label = new JLabel(msgFormat.format(substitutionValues).trim(), SwingConstants.LEFT);
    aPanel.add(label, constraints);

    // egold account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(EGOLD_ACCOUNT_NUMBER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldAccountNumberTextField = new OverwritableTextField(EGOLD_ACCOUNT_NUMBER_FIELD_WIDTH);
    _egoldAccountNumberTextField.setToolTipText(__resources.getProperty(EGOLD_ACCOUNT_NUMBER_TOOL_TIP).toString());
    aPanel.add(_egoldAccountNumberTextField, constraints);

    // egold password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(EGOLD_PASSWORD).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldPasswordField = new JPasswordField(EGOLD_PASSWORD_FIELD_WIDTH);
    _egoldPasswordField.setToolTipText(__resources.getProperty(EGOLD_PASSWORD_TOOL_TIP).toString());
    aPanel.add(_egoldPasswordField, constraints);

    // metal type
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(CHOOSE_METAL).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _metalsComboBox = new _JComboBox(METALS);
    _metalsComboBox.setEditable(false);
    aPanel.add(_metalsComboBox, constraints);

    // open an e-gold account
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 2;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(SIGN_UP_NOW).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldEditorPane = new JEditorPane();
    _egoldEditorPane.addHyperlinkListener(new EGoldHyperlinkListener());
    _egoldEditorPane.setEditable(false);
    _egoldEditorPane.setContentType("text/html");

    _egoldEditorPane.setText(EGOLD_LINK_HTML);

    aPanel.add(_egoldEditorPane, constraints);

    return aPanel;
  }

  @Override
  protected boolean ProcessChargeDataEntryPanel()
  {
    // e-gold account number must be entered
    if (_egoldAccountNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(ACCOUNT_NUMBER_MISSING).toString(), __resources.getProperty(ACCOUNT_NUMBER_MISSING_TITLE).toString(),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    // e-gold password must be entered
    if (new String(_egoldPasswordField.getPassword()).trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(PASSWORD_MISSING).toString(), __resources.getProperty(PASSWORD_MISSING_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidChargeAmount()
  {
    double minimumAmount = InstallationTable.GetCachedInstallationRow().Get_egold_charge_minimum_amount().doubleValue();
    if (minimumAmount > GetAmount())
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_SMALL).toString(), __resources.getProperty(AMOUNT_TOO_SMALL_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateChargeFee()
  {
    double fee = InstallationTable.GetCachedInstallationRow().Get_egold_charge_xaction_fee().doubleValue()
      + ((InstallationTable.GetCachedInstallationRow().Get_egold_charge_xaction_fee_pcent().doubleValue() * GetAmount()) / 100.0d);

    return fee;
  }

  @Override
  protected String GetChargeWaitPanelTitle()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_TITLE).toString();
  }

  @Override
  protected String GetChargeWaitPanelInitialMessage()
  {
    return __resources.getProperty(CHARGE_WAIT_PANEL_INITIAL_MESSAGE).toString();
  }

  @Override
  protected boolean DoCharge(MsgBox waitPanel)
  {
    // construct the GET form of the command
    String urlString;
    try
    {
      urlString = InstallationTable.GetCachedInstallationRow().Get_egold_charge_url() + EGOLD_SPEND_SCRIPT + "?AccountID="
        + URLEncoder.encode(_egoldAccountNumberTextField.getText().trim(), LPDClient.UTF_8) + "&Passphrase="
        + URLEncoder.encode((new String(_egoldPasswordField.getPassword())).toString(), LPDClient.UTF_8) + "&Payee_Account="
        + URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_egold_charge_account().trim(), LPDClient.UTF_8) + "&Amount="
        + URLEncoder.encode(String.valueOf(GetAmount()).trim(), LPDClient.UTF_8) + "&PAY_IN="
        + __currencyToEGoldCurrencyCodeMap.get(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().trim()).toString() + "&WORTH_OF="
        + URLEncoder.encode(_metalsComboBox.getSelectedItem().toString(), LPDClient.UTF_8) + "&Memo=" + URLEncoder.encode(__resources.getProperty(CHARGE_MEMO).toString(), LPDClient.UTF_8)
        + "&IGNORE_RATE_CHANGE=true" + "&PAYMENT_ID="
        + URLEncoder.encode(UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + "-" + UserAccountTable.GetCachedUserAccountRow().Get_installation_id(), LPDClient.UTF_8);
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    // send the xaction and process the resulting html page
    Properties nameValuePairs = ExtractFormFieldsFromWebPage(DoGETXaction(urlString));

    if (nameValuePairs == null)
    {
      return false;
    }

    // look for an ERROR field
    String error = nameValuePairs.get("ERROR").toString();
    if (error != null)
    {
      // clean up the html formatting from the error string
      String cleanError = error.substring(63, (error.length() - 35));
      JOptionPane.showMessageDialog(GetParentFrame(), cleanError, "E-Gold Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  @Override
  protected boolean EnabledForRefund()
  {
    if ((InstallationTable.GetCachedInstallationRow().Get_egold_charge_account().trim().length() == 0)
      || (ECommerceEGold.__currencyToEGoldCurrencyCodeMap.get(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().trim()) == null))
    {
      return false;
    }
    return true;
  }

  @Override
  protected JPanel BuildRefundDataEntryPanel()
  {
    JPanel aPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;
    constraints.gridy = 0;

    // egold account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(EGOLD_ACCOUNT_NUMBER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldAccountNumberTextField = new OverwritableTextField(EGOLD_ACCOUNT_NUMBER_FIELD_WIDTH);
    _egoldAccountNumberTextField.setToolTipText(__resources.getProperty(EGOLD_ACCOUNT_NUMBER_TOOL_TIP).toString());
    aPanel.add(_egoldAccountNumberTextField, constraints);

    // metal type
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    aPanel.add(new JLabel(__resources.getProperty(CHOOSE_METAL).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _metalsComboBox = new _JComboBox(METALS);
    _metalsComboBox.setEditable(false);
    aPanel.add(_metalsComboBox, constraints);

    return aPanel;
  }

  @Override
  protected boolean ProcessRefundDataEntryPanel()
  {
    // e-gold account number must be entered
    if (_egoldAccountNumberTextField.getText().trim().length() == 0)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(ACCOUNT_NUMBER_MISSING).toString(), __resources.getProperty(ACCOUNT_NUMBER_MISSING_TITLE).toString(),
        JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected boolean CheckValidRefundAmount()
  {
    if (GetAmount() <= 0d)
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_SMALL).toString(), __resources.getProperty(AMOUNT_TOO_SMALL_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    if (GetAmount() > UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue())
    {
      JOptionPane.showMessageDialog(GetParentFrame(), __resources.getProperty(AMOUNT_TOO_LARGE).toString(), __resources.getProperty(AMOUNT_TOO_LARGE_TITLE).toString(), JOptionPane.ERROR_MESSAGE);
      return false;
    }
    return true;
  }

  @Override
  protected double CalculateRefundFee()
  {
    double fee;
    fee = InstallationTable.GetCachedInstallationRow().Get_egold_refund_xaction_fee().doubleValue()
      + ((InstallationTable.GetCachedInstallationRow().Get_egold_refund_xaction_fee_pcent().doubleValue() * GetAmount()) / 100.0d);
    return fee;
  }

  @Override
  protected String GetRefundWaitPanelTitle()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_TITLE).toString();
  }

  @Override
  protected String GetRefundWaitPanelInitialMessage()
  {
    return __resources.getProperty(REFUND_WAIT_PANEL_INITIAL_MESSAGE).toString();
  }

  @Override
  protected boolean DoRefund(MsgBox waitPanel)
  {
    // construct the GET form of the command
    String urlString;
    try
    {
      urlString = InstallationTable.GetCachedInstallationRow().Get_egold_charge_url() + EGOLD_SPEND_SCRIPT + "?AccountID="
        + URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_egold_refund_account().trim(), LPDClient.UTF_8) + "&Passphrase="
        + URLEncoder.encode(InstallationTable.GetCachedInstallationRow().Get_egold_refund_passphrase().trim(), LPDClient.UTF_8) + "&Payee_Account="
        + URLEncoder.encode(_egoldAccountNumberTextField.getText().trim(), LPDClient.UTF_8) + "&Amount=" + URLEncoder.encode(String.valueOf(GetAmount()).trim(), LPDClient.UTF_8) + "&PAY_IN="
        + __currencyToEGoldCurrencyCodeMap.get(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().trim()).toString() + "&WORTH_OF="
        + URLEncoder.encode(_metalsComboBox.getSelectedItem().toString(), LPDClient.UTF_8) + "&Memo=" + URLEncoder.encode(__resources.getProperty(REFUND_MEMO).toString(), LPDClient.UTF_8)
        + "&IGNORE_RATE_CHANGE=true" + "&PAYMENT_ID="
        + URLEncoder.encode(UserAccountTable.GetCachedUserAccountRow().Get_user_account_id() + "-" + UserAccountTable.GetCachedUserAccountRow().Get_installation_id(), LPDClient.UTF_8);
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    // send the xaction and process the resulting html page
    Properties nameValuePairs = ExtractFormFieldsFromWebPage(DoGETXaction(urlString));

    if (nameValuePairs == null)
    {
      return false;
    }

    // look for an ERROR field
    String error = nameValuePairs.get("ERROR").toString();
    if (error != null)
    {
      // clean up the html formatting from the error string
      String cleanError = error.substring(63, (error.length() - 35));
      JOptionPane.showMessageDialog(GetParentFrame(), cleanError, "E-Gold Error", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  private class EGoldHyperlinkListener
    implements HyperlinkListener
  {
    public EGoldHyperlinkListener()
    {
      super();
    }

    @Override
    public void hyperlinkUpdate(HyperlinkEvent event)
    {
      if (event.getEventType() == HyperlinkEvent.EventType.ACTIVATED)
      {
        BrowserControl.displayURL(event.getURL().toString());
      }
    }
  }

  private static HashMap<String, String> CreateMap(String[] keys, String[] values)
  {
    HashMap<String, String> hashMap = new HashMap<>(keys.length);
    for (int i = 0; i < keys.length; i++)
    {
      hashMap.put(keys[i], values[i]);
    }
    return hashMap;
  }

  @Override
  protected Resources GetChildResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ECommerceEGold.class.getName());
    __currencyToEGoldCurrencyCodeMap = CreateMap(CURRENCIES, EGOLD_CURRENCY_CODES);
  }

}
