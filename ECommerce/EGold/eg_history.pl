#!/usr/bin/perl
#
# perl script to retreive e-gold account history
# from maintenance release e-gold system
# version 1.0
# free for use, modification, redistribution
#
#Disclaimer
#The Author [e-gold Ltd.] accepts no
#responsibility for damages to persons, property or data incurred
#through the use of this script(s). To the maximum extent permitted
#by law, in no event shall the Author [e-gold Ltd.] be
#liable for any damages whatsoever (including, without limitation,
#damages for loss of business profits, business interruption, loss of
#business information, or other pecuniary loss) arising out of the use
#or inability to use this software, even if the Author has been advised
#of the possibility of such damages.
#
#This product is supplied as-is, with no warranties express or implied.
#Use this software at your own risk.
#
#
if ($#ARGV < 0) {
   &command_help;
}
# pick up account number we will be operating on...
$account = $ARGV[0];
shift;
require "getopts.pl";
# parse the arguments...
&Getopts ("s:e:b:f:m:r:t:") || &command_help;
# default dates are today...
($todaysec,$todaymin,$todayhour,$todaydom,$todaymonth,$todayyear) 
	= localtime(time);
# convert to one relative numbers the e-gold site uses
# and set up our default time frame of today -> today
$startday = $todaydom;
$startmonth = $todaymonth + 1;
$startyear = $todayyear + 1900;
$endday = $todaydom;
$endmonth = $todaymonth + 1;
$endyear = $todayyear + 1900;
# if dates provided, use them instead...
if ($opt_s){
   ($startmonth, $startday, $startyear) = split(/\//, $opt_s, 3);
}
if ($opt_e){
   ($endmonth, $endday, $endyear) = split(/\//, $opt_e, 3);
}
# get batch number filter if present
$batchfilter = "";
if ($opt_b){
   $batchfilter = $opt_b;
}
# get to/from account number number filter if present
$counterfilter = "";
if ($opt_f){
   $counterfilter = $opt_f;
}
# get to/from account number number filter if present
$metalfilter = "";
if ($opt_m){
   if(index($opt_m,"gold") > 0){
	$metalfilter = "1";
	}
   if(index($opt_m,"silver") > 0){
	$metalfilter = "2";
	}
   if(index($opt_m,"platinum") > 0){
	$metalfilter = "3";
	}
   if(index($opt_m,"palladium") > 0){
	$metalfilter = "4";
	}
}
# get merchant reference number if present
$paymentidfilter = "";
if ($opt_r){
   $paymentidfilter = $opt_r;
}
# get list of transaction types we check
$paymentsreceived = 1;
$paymentsmade = 1;
$inexchanges = 1;
$outexchanges = 1;
$bailments = 1;
$redemptions = 1;
$fees = 1;
$incentives = 1;
if ($opt_t){
   if(index($opt_t, "paymentsreceived") < 0){
	$paymentsreceived = 0;
	}
   if(index($opt_t, "paymentsmade") < 0){
	$paymentsmade = 0;
	}
   if(index($opt_t, "inexchanges") < 0){
	$inexchanges = 0;
	}
   if(index($opt_t, "outexchanges") < 0){
	$outexchanges = 0;
	}
   if(index($opt_t, "bailments") < 0){
	$bailments = 0;
	}
   if(index($opt_t, "redemptions") < 0){
	$redemptions = 0;
	}
   if(index($opt_t, "fees") < 0){
	$fees = 0;
	}
   if(index($opt_t, "incentives") < 0){
	$incentives = 0;
	}
}
#print "Days: $days, Newest: $newest, Trans: $trans\n";

sub command_help
{
   print "Usage: $0 account_number [-s startingdate] [-e endingdate] [-b batch_number] [-f account_number] [-m metal_name] [-r ref_number] [-t'transactiontype']\n";
   print "  -s starting date, default = today\n";
   print "  -e ending date, default = today\n";
   print "  -b match batch number, default = all batches\n";
   print "  -f to/from given account number, default = all\n";
   print "  -m only given metal type, default = all\n";
   print "  -r match given merchant reference number, default = all\n";
   print "  -t display given transaction types, default = all\n";
   print "  enter dates in format mm/dd/yyyy\n";
   print "  metal names are: gold, silver, platinum, palladium\n";
   print "  transactiontypes:\n";
   print "   paymentsreceived\n";
   print "   paymentsmade\n";
   print "   inexchanges\n";
   print "   outexchanges\n";
   print "   bailments\n";
   print "   redemptions\n";
   print "   fees\n";
   print "   incentives\n\n";
   print "  example: $0 100666 -s 1/1/2000 -e 12/31/2000 -t paymentsreceived,paymentsmade\n";
   exit 1;
}
# prompt user for password
print "\nPassphrase for $account?";
if ( system('stty -echo') != 0) {
	die "Error setting terminal to not echo\n";
}
$pp = <STDIN>;
chop($pp);
system('stty echo');
print "\n";

#build up the arguments for the history command
$curlargs = "initial=1&startmonth=$startmonth";
$curlargs .= "&startday=$startday";
$curlargs .= "&startyear=$startyear";
$curlargs .= "&endmonth=$endmonth";
$curlargs .= "&endday=$endday";
$curlargs .= "&endyear=$endyear";
if($paymentsmade > 0 ){
	$curlargs .= "&paymentsmade=1";
}
if($paymentsreceived > 0 ){
	$curlargs .= "&paymentsreceived=1";
}
if($inexchanges > 0 ){
	$curlargs .= "&inexchanges=1";
}
if($outexchanges > 0 ){
	$curlargs .= "&outexchanges=1";
}
if($bailments > 0 ){
	$curlargs .= "&bailments=1";
}
if($redemptions > 0 ){
	$curlargs .= "&redemptions=1";
}
if($fees > 0 ){
	$curlargs .= "&fees=1";
}
if($incentives > 0 ){
	$curlargs .= "&incentives=1";
}
if(length($batchfilter) > 0 ){
	$curlargs .= "&batchfilter=$batchfilter";
}
if(length($counterfilter) > 0 ){
	$curlargs .= "&counterfilter=$counterfilter";
}
if(length($paymentidfilter) > 0 ){
	$curlargs .= "&paymentidfilter=$paymentidfilter";
}
if(length($metalfilter) > 0 ){
	$curlargs .= "&metalfilter=$metalfilter";
}
$curlargs .= "&PassPhrase=$pp";
$curlargs .= "&AccountID=$account";
# print "\n$curlargs\n";
$sysstring = "curl -s -d ";
$sysstring .= '"';
$sysstring .= $curlargs;
$sysstring .= "&CURPAGE=0";
$sysstring .= '"';
$sysstring .= " https://www.e-gold.com/acct/historycsv.asp";
#print $sysstring;
open(foo, "$sysstring|");
while(<foo>){
  $lineout = $_;
 # if we get back some kind of HTML, the e-gold server is unhappy with us...
 if($lineout =~ /<html>/){
   print "\nError occurred.\n";
   close foo;
 }
 else {
  print $lineout;
 }
}
close foo;
# all done
print "\n";
