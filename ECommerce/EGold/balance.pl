#!/usr/bin/perl
#
# perl script to display account balances
# from maintenance release e-gold system
# version 1.0
# free for use, modification, redistribution
#
#Disclaimer
#The Author [e-gold Ltd.] accepts no
#responsibility for damages to persons, property or data incurred
#through the use of this script(s). To the maximum extent permitted
#by law, in no event shall the Author [e-gold Ltd.] be
#liable for any damages whatsoever (including, without limitation,
#damages for loss of business profits, business interruption, loss of
#business information, or other pecuniary loss) arising out of the use
#or inability to use this software, even if the Author has been advised
#of the possibility of such damages.
#
#This product is supplied as-is, with no warranties express or implied.
#Use this software at your own risk.
#
#
if ($#ARGV < 0) {
   &command_help;
}
# pick up account number we will be getting balance on...
$account= $ARGV[0];
sub command_help
{
   print "Usage: $0 account_number\n";
   print "  will display e-gold account balance for given account.\n";
   print "\nexample:\n";
   print "$0 101574\n";
   exit 1;
}
# prompt user for password
print "\nPassphrase for $accountfrom?";
if ( system('stty -echo') != 0) {
	die "Error setting terminal to not echo\n";
}
$pp = <STDIN>;
chop($pp);
system('stty echo');
print "\n";


#build up the arguments for the preview command
$curlargs  = "PassPhrase=$pp";
$curlargs .= "&AccountID=$account";
$sysstring = "curl -s -d ";
$sysstring .= '"';
$sysstring .= $curlargs;
$sysstring .= '"';
$sysstring .= " https://www.e-gold.com/acct/balance.asp";
#print $sysstring;
# read everything into one buffer
undef $/;
open(foo, "$sysstring|");
while(<foo>){
  # pull out the hidden fields...the pattern starts with "hidden name=" and
  # ends with ">"
  while( /hidden name=(.*?)>/gs ) {
	# see if there was some kind of error
	if(index($1, "ERROR") == 0) {
		$errstring = $1;
		$errstring =~ ( /value="(.*)"/ );
		print "Error on balance is: $1\n";
		exit -1;
	}
	# print out the information we care about from the preview
	if(index($1, "_Ounces") > 0) {
		$matching = $1;
		$ozstring = $matching;
		$ozstring =~ ( /value="(.*)"/ );
		$ozstring = $1;
		$namestring = $matching;
		$namestring =~ ( /(.*)_Ounces value/ );
		print "$1 balance of $account: $ozstring oz.\n";
	}
  }
}
close foo;
# all done
print "\n";
