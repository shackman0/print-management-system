#!/usr/bin/perl
#
# perl script to preview a spend
# from maintenance release e-gold system
# version 1.0
# free for use, modification, redistribution
#
#Disclaimer
#The Author [e-gold Ltd.] accepts no
#responsibility for damages to persons, property or data incurred
#through the use of this script(s). To the maximum extent permitted
#by law, in no event shall the Author [e-gold Ltd.] be
#liable for any damages whatsoever (including, without limitation,
#damages for loss of business profits, business interruption, loss of
#business information, or other pecuniary loss) arising out of the use
#or inability to use this software, even if the Author has been advised
#of the possibility of such damages.
#
#This product is supplied as-is, with no warranties express or implied.
#Use this software at your own risk.
#
#
if ($#ARGV < 4) {
   &command_help;
}
# pick up account number we will be spending from...
$accountfrom = $ARGV[0];
shift;
# pick up account number we will be spending to...
$accountto = $ARGV[0];
shift;
# pick up amount
$amount = $ARGV[0];
shift;
# pick up units
$units = uc($ARGV[0]);
shift;
$unitsok = 0;
if(index($units,"USD") == 0){
	$units = 1;
	$unitsok = 1;
}
if(index($units,"OZ") == 0){
	$units = 9999;
	$unitsok = 1;
}
if(index($units,"G") == 0){
	$units = 8888;
	$unitsok = 1;
}
if(index($units,"CAD") == 0){
	$units = 2;
	$unitsok = 1;
}
if(index($units,"FFR") == 0){
	$units = 33;
	$unitsok = 1;
}
if(index($units,"CHF") == 0){
	$units = 41;
	$unitsok = 1;
}
if(index($units,"GBP") == 0){
	$units = 44;
	$unitsok = 1;
}
if(index($units,"DEM") == 0){
	$units = 49;
	$unitsok = 1;
}
if(index($units,"AUD") == 0){
	$units = 61;
	$unitsok = 1;
}
if(index($units,"JPY") == 0){
	$units = 81;
	$unitsok = 1;
}
if($unitsok == 0) {
	die "$units is Invalid, choose oz, g, usd, gbp, aud, jpy, dem, cad, ffr, chr";
}
# pick up e-metal type
$metal = uc($ARGV[0]);
shift;
$metalok = 0;
if(index($metal,"GOLD") == 0){
	$metal = "Gold";
	$metalok = 1;
}
if(index($metal,"SILVER") == 0){
	$metal = "Silver";
	$metalok = 1;
}
if(index($metal,"PLATINUM") == 0){
	$metal = "Platinum";
	$metalok = 1;
}
if(index($metal,"PALLADIUM") == 0){
	$metal = "Palladium";
	$metalok = 1;
}
if($metalok == 0) {
	die "$metal is invalid, choose gold, silver, platinum, or palladium";
}
sub command_help
{
   print "Usage: $0 from to amount units metal\n";
   print "  units can be one of oz, g, usd, aud, cad, dem, gbp, jpy, chf, ffr\n";
   print "  metal can be one of gold, silver, platinum, palladium\n";
   print "\nexample:\n";
   print "$0 101574 100998 1 usd gold\n";
   print "to preview a spend of 1 US dollar equivalent of gold from\n";
   print " account 101574 to account 100998\n";
   exit 1;
}
# prompt user for password
print "\nPassphrase for $accountfrom?";
if ( system('stty -echo') != 0) {
	die "Error setting terminal to not echo\n";
}
$pp = <STDIN>;
chop($pp);
system('stty echo');
print "\n";


#build up the arguments for the preview command
$curlargs = "Payee_Account=$accountto";
$curlargs .= "&Amount=$amount";
$curlargs .= "&PAY_IN=$units";
$curlargs .= "&WORTH_OF=$metal";
$curlargs .= "&Memo=automatic spend";
$curlargs .= "&PassPhrase=$pp";
$curlargs .= "&AccountID=$accountfrom";
# print "\n$curlargs\n";
$sysstring = "curl -s -d ";
$sysstring .= '"';
$sysstring .= $curlargs;
$sysstring .= "&CURPAGE=0";
$sysstring .= '"';
$sysstring .= " https://www.e-gold.com/acct/verify.asp";
#print $sysstring;
# read everything into one buffer
undef $/;
open(foo, "$sysstring|");
while(<foo>){
  # pull out the hidden fields...the pattern starts with "hidden name=" and
  # ends with ">"
  while( /hidden name=(.*?)>/gs ) {
	# see if there was some kind of error
	if(index($1, "ERROR") == 0) {
		$errstring = $1;
		$errstring =~ ( /value="(.*)"/ );
		print "Error on preview is: $1\n";
		exit -1;
	}
	# print out the information we care about from the preview
	if(index($1, "ACTUAL_PAYMENT_OUNCES") == 0) {
		$ozstring = $1;
		$ozstring =~ ( /value="(.*)"/ );
		print "Successful Preview.\n";
		print "Weight of this payment would be: $1 oz.\n";
	}
  }
}
close foo;
# all done
print "\n";
