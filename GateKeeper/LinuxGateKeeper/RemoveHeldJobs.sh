#! /bin/bash

#
# Filename:    LinuxGateKeeper_RemoveHeldJobs.sh
#
# Description:
#              This is a (bourne) shell script.
#              this script searches all /var/spool/lpd subdirectories for
#              JGKhold.nnn files.
#
#              for every JGKhold.nnn file that is older than the specified
#              number of minutes, the print job and hold file are removed.
#
#              the example below shows it running every 10 minutes. you should
#              add something like this to the root crontab as follows:
#
#              0,10,20,30,40,50 * * * * /usr/local/LinuxGateKeeper/RemoveHeldJobs.sh
#
#              the example below shows it running every 15 minutes. you should
#              add something like this to the root crontab as follows:
#
#              0,15,30,45 * * * * /usr/local/LinuxGateKeeper/RemoveHeldJobs.sh
#

# set EXPIRATIONMINUTES to how old the held job needs to be in order
# for it to qualify for removal.

EXPIRATIONMINUTES=60

SPOOLDIR=/var/spool/lpd/
HOLDFILEPREFIX=LGKhold

HOLDFILES=`find ${SPOOLDIR} -amin +${EXPIRATIONMINUTES} -type f -name "${HOLDFILEPREFIX}.*" -print`;

for HOLDFILE in ${HOLDFILES}
do
  let POSITION=${#SPOOLDIR};
  let LENGTH="${#HOLDFILE} - ${POSITION} - ${#HOLDFILEPREFIX} - 5";
  PRINTER=${HOLDFILE:${POSITION}:${LENGTH}};

  let POSITION="${#HOLDFILE} - 3";
  JOBNUM=${HOLDFILE:${POSITION}};

#   echo lprm -P${PRINTER} ${JOBNUM}
   /usr/bin/lprm -P${PRINTER} ${JOBNUM};

#   echo rm -f ${HOLDFILE}
   /bin/rm -f ${HOLDFILE};
done

