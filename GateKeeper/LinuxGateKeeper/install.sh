#! /bin/bash

#
# Be sure you have write permission in the /usr/local directory.
# you may want to "su root" to run this script.
#
#
# Be sure to edit the file ./variables before running this script
#

echo clean up any DOS residuals

dos2unix ./LinuxGateKeeper.policy            >/dev/null
dos2unix ./LinuxGateKeeper_Config            >/dev/null
dos2unix ./LinuxGateKeeper_crontab           >/dev/null
dos2unix ./LinuxGateKeeper_lpd.perms         >/dev/null
dos2unix ./LinuxGateKeeper_printcap          >/dev/null
dos2unix ./LinuxGateKeeper_smb.conf          >/dev/null
dos2unix ./LinuxGateKeeperPrintJobFilter.sh  >/dev/null
dos2unix ./RemoveHeldJobs.sh                 >/dev/null
dos2unix ./variables                         >/dev/null


. ./variables # this sets INSTALLDIR

echo create the installation directory
mkdir -p ${INSTALLDIR}

echo copy files to the appropriate locations
cp -p * ${INSTALLDIR}/.

chmod a+rwx  ${INSTALLDIR}
chmod a+rw   ${INSTALLDIR}/*
chmod a-x    ${INSTALLDIR}/*
chmod a+x    ${INSTALLDIR}/*.sh

echo update the /etc/lpd.perms file so we can lpd remotely
cat LinuxGateKeeper_lpd.perms /etc/lpd.perms >./lpd.perms
mv -f ./lpd.perms /etc/lpd.perms
/usr/sbin/lpc reread

echo tell cron to clean up the /tmp/filter.debug.nnn files and to run the RemoveHeldJobs.sh script periodically

crontab -l >./new_crontab ;
cat ./LinuxGateKeeper_crontab >>./new_crontab ;
crontab ./new_crontab ;
rm -f ./new_crontab ;

cd ${INSTALLDIR}

echo done!
