# This is an example printcap entry for use
# with the PayToPrint LinuxGatekeeper software.
#
# You must have LPRng and ifhp installed
#
# First:
#
# set up 1 output queue for each printer you want controlled by this print server
#
# Second:
#
# set up 1 input queue for each print client and for each printer the print client
# wants access to. that means that you may have to define as many as (C x P) input queues,
# where C is the number of print clients accessing this print server and P is the number of
# printers supported by this print server. this may seem like a lot (and i suppose it is),
# but this configuration allows each client to have its own input queue so that each client
# can print independent of the other clients. this way, if one client's job is held up,
# it doesn't hold up any of the other client's jobs.

# the following is an output queue named "lpq". this queue prints to a printer with host name
# "kyocera". you should be able to use this printcap entry as is and only change the "kyocera"
# part of the ":lp=kyocera%9100" entry to the hostname of the printer you want this queue to print to.

lpq:
  :client
  :force_localhost=0

lpq:
  :server
  :sd=/var/spool/lpd/%P
  :done_jobs=0
  :mx=0
  :mc=0
  :sh
  :lp=kyocera%9100


# the following are the common entries used by all input queues. it is put here to make it
# easier to create all those input queues.
#
# The entry ":lpd_bounce" causes all filters to be run before sending
# the job to the output queue "lpq@localhost". In particular the "as" filter is run.
#
# The entry ":as=|/usr/local/LinuxGateKeeper/LinuxGateKeeperPrintJobFilter.sh"
# causes the program that checks with the Installation Server for permission
# to print to be run.
#
# if you want to customize any of the features for a particular input queue, you can remove
# that option from the common entries and put one in each input queue. for instance, you may
# want to adjust the number of done_jobs tracked by a queue by input_queue.

.common
  :lpd_bounce=1
  :as=|/usr/local/LinuxGateKeeper/LinuxGateKeeperPrintJobFilter.sh
  :done_jobs=5
  :mx=0
  :mc=0
  :sh
  :lpd_port=515
  :reuse_addr=1
  :retry_econnrefused=1
  :retry_nolink=1
  :socket_linger=1
  :remote_support=1
  :originate_port=512 1023

# the following is an input queue. the input queue checks for permission to print before
# passing it on to the output queue. remember that every print client needs its own input
# queue for each output queue you want made available to that print client.
#
# every input queue consists of client and server configuration statements. the input queue
# server uses the .common configuration.
#
# The entry ":force_localhost" turns on the force_localhost flag
# which forces lpr to use the local server and not to route the job to some
# other machine for processing. this must be on for lpd_bounce to work properly.
#
# The entry ":lp=lpq@localhost" causes the print job to be spooled to the output
# queue for eventual printing. you should change the "lpgkq" part to point to the
# appropriate output queue. if you have only one output queue, you could move the
# ":lp=..." line in with the other .common entries as well.

lp00:
  :client
  :force_localhost=1

lp00:
  :server
  :tc=.common
  :sd=/var/spool/lpd/%P
  :lp=lpq@localhost

