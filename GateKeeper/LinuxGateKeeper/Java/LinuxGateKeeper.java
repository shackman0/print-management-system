import java.io.*;
import java.lang.*;

/**
   This is the java class (application) which gets permission to print .
*/
public class LinuxGateKeeper
{

  private static final int        PERMISSION_GRANTED           = 0;
  private static final int        PERMISSION_DENIED            = 3;
  private static final int        HOLD                         = 6;

  private static final String     CONFIGURATION_FILE_NAME = "LinuxGateKeeper_Config";
  private static final String     INSTALLATION_SERVER_HOSTNAME = "INSTALLATION_SERVER_HOSTNAME";
  private static final String     INSTALLATION_SERVER_USER_ID = "INSTALLATION_SERVER_USER_ID";
  private static final String     INSTALLATION_SERVER_PASSWORD = "INSTALLATION_SERVER_PASSWORD";
  private static final String     INSTALLATION_ID = "INSTALLATION_ID";

  private static final String     RC_HOLD = "RC=hold ";
  private static final String     RC_RELEASE = "RC=release ";
  private static final String     RC_CANCEL = "RC=cancel ";

  private static final String     ACCOUNT_ID_PARM = "ACCT=";
  private static final String     DESCRIPTION_PARM = "DESC=";
  private static final String     PASSPHRASE_PARM = "PASS=";
  private static final String     PRINT_JOB_NUMBER_PARM = "JOBNUM=";

  private static final String     INSTALLDIR = "-INSTALLDIR";
  private static final String     USER = "-USER";
  private static final String     HOST = "-HOST";

  /**
    main takes its input args and the config file and then constructs
    a PrintJob. The values for the PrintJob are set and then permission
    is requested.

    exit values:
      0 = Permission to print is given.
      3 = Permission was denied by the Server
      6 = put the job on hold
  */
  public static void main(String args[])
  {
    GlobalAttributes.__applicationName = LinuxGateKeeper.class.getName();
    GlobalAttributes.__mainClass = LinuxGateKeeper.class;

    String controlFileName = null;
    String spoolDir = null;
    String dataFileName = null;
    String installDir = null;
    String hostName = null;
    String hostPrintJobNumber = null;
    String printer = null;
    String userName = null;
    int rc = 2;
    String parmString = null;

    try
    {

      // Put in the Name Value pairs from args

      // a - printcap af (accounting file name)
      // b - job size (in K bytes)
      // c - binary file (l format for print file)
      // d - printcap cd or sd entry
      // e - print job data file name (currently being processed)
      // f - print job original name when spooled for printing (N info from control file)
      // h - print job originating host (H info from control file)
      // i - indent request (I info from control file)
      // j - job number in spool queue
      // k - print job control file name
      // l - printcap pl (page length)
      // m - printcap co
      // n - user name (L info from control file)
      // p - remote printer (when processing for bounce queue)
      // r - remote host (when processing for bounce queue)
      // s - printcap sf (status file)
      // t - time in common UNIX format
      // w - printcap pw (page width)
      // x - printcap px (page x dimension)
      // y - printcap py (page y dimension)
      // F - print file format
      // P - printer name
      // S - printcap cm (comment field)
      // Capital letter - Corresponding line from control file
      // {key} - printcap value for key

      for(int index=0; index<args.length; ++index)
      {
        // capture the file name from the command line
        if(args[index].startsWith("-k"))
        {
          controlFileName = args[index].substring(2);
          dataFileName = "d" + controlFileName.substring(1);
        }
        else
        // capture the hostname on which the print job was created
        if(args[index].startsWith(HOST))
        {
          hostName = args[++index];
        }
        else
        // capture the username which created the print job
        if(args[index].startsWith(USER))
        {
          userName = args[++index];
        }
        else
        // capture the print job spool queue number
        if(args[index].startsWith("-j"))
        {
          hostPrintJobNumber = args[index].substring(2);
        }
        else
        // capture the printer name
        if(args[index].startsWith("-P"))
        {
          printer = args[index].substring(2);
        }
        else
        // capture the install directory
        if(args[index].startsWith(INSTALLDIR))
        {
          installDir =  args[index].substring(INSTALLDIR.length());
        }
        else
        // capture the spool directory
        if(args[index].startsWith("-d"))
        {
          spoolDir =  args[index].substring(2);
        }
      }

      // Read in the Name Value pairs from the config file
      Preferences config = new Preferences();
      //config.Load(installDir + File.pathSeparator + CONFIGURATION_FILE_NAME);
      config.Load(installDir + "/" + CONFIGURATION_FILE_NAME);

      System.out.println("INSTALLATION_SERVER_HOSTNAME = " + config.getProperty(INSTALLATION_SERVER_HOSTNAME.trim()));
      System.out.println("INSTALLATION_SERVER_USER_ID = " + config.getProperty(INSTALLATION_SERVER_USER_ID.trim()));
      System.out.println("INSTALLATION_SERVER_PASSWORD found = " + new Boolean(config.getProperty(INSTALLATION_SERVER_PASSWORD) != null));
      System.out.println("INSTALLATION_ID = " + config.getProperty(INSTALLATION_ID.trim()));

      // create a gatekeeper
      GateKeeper gateKeeper = new GateKeeper (
               config.getProperty(INSTALLATION_SERVER_HOSTNAME.trim()),
               config.getProperty(INSTALLATION_SERVER_USER_ID.trim()),
               config.getProperty(INSTALLATION_SERVER_PASSWORD.trim()),
               new Integer(config.getProperty(INSTALLATION_ID).trim()),
               hostName );

      // get a new print job number
      String printJobNumber = String.valueOf(InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobNumberTable().GetNextPrintJobNumber());

      // tell the user we are working on his print job
      gateKeeper.DisplayPrintJobBeingProcessedMsgBox (
                   printJobNumber,
                   printer );

      // Create the print job object
      PrintJob printJob = new PrintJob (
                                spoolDir + "/" + dataFileName,
                                printJobNumber );
      // parse the print job
      rc = printJob.Parse() == true ? 0 : 2;
      printJob.GetProperties().setProperty(PrintJobPropertiesRow.PROPERTY_PRINTER,printer);
      printJob.GetProperties().setProperty(PrintJobPropertiesRow.PROPERTY_CLIENT_ADDRESS,hostName);
      printJob.GetProperties().setProperty(PrintJobPropertiesRow.PROPERTY_USER_NAME,userName);
      printJob.GetProperties().setProperty(PrintJobPropertiesRow.PROPERTY_HOST_PRINT_JOB_NUMBER,hostPrintJobNumber);

      gateKeeper.DismissPrintJobBeingProcessedMsgBox (
                   printJobNumber,
                   printer );

      if (rc == 0)
      {
        // ask for permission to release it
        rc = gateKeeper.ReleasePrintJob (printJob.GetProperties());
      }

      if ( (rc == 0) ||
           (rc == 1) )
      {
        // replace white space with underscored so the bash script can treat the value as a single parameter
        parmString =
          PRINT_JOB_NUMBER_PARM + printJob.GetProperties().getProperty(PrintJobPropertiesRow.PROPERTY_PRINT_JOB_NUMBER) + " " +
          ACCOUNT_ID_PARM + printJob.GetProperties().getProperty(PrintJobPropertiesRow.PROPERTY_USER_ACCOUNT_ID).replace(' ','_') + " " +
          DESCRIPTION_PARM + printJob.GetProperties().getProperty(PrintJobPropertiesRow.PROPERTY_DESCRIPTION).replace(' ','_');
      }

    }
    catch (Exception excp)
    {
      excp.printStackTrace(System.out);
      rc = 2;
    }

    if(rc == 0)
    {
      parmString = RC_RELEASE + parmString;
      System.out.println(parmString);  // the .sh script relies on this string
      System.exit(PERMISSION_GRANTED);
    }
    else
    if(rc == 1)
    {
      parmString = RC_HOLD + parmString;
      System.out.println(parmString);  // the .sh script relies on this string
      System.exit(PERMISSION_GRANTED);
    }
    else
    {
      System.out.println(RC_CANCEL);  // the .sh script relies on this string
      System.exit(PERMISSION_DENIED);
    }
  }
}
