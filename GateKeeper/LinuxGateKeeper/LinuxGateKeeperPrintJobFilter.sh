#! /bin/bash

#
# Be sure to edit the file variables so it has the correct value
# NOTE: if you change INSTALLDIR in variables you must also change the next line
#
INSTALLDIR=/usr/local/LinuxGateKeeper

#
# Filename:    LinuxGateKeeperPrintJobFilter.sh
#
# Description:
#              This is a (bourne) shell script.
#              It is used as an "as" filter
#              by LPRng. That is, it is called out in the printcap file as a
#              program which must be run to process a print job.
#
#              Depending upon whether the job was previously put on hold or
#              if this is the first time through for this print job, this
#              script does the following:
#
#              first time through:
#
#              This script asks for permission to print
#              from LinuxGateKeeper. If permission is granted this script
#              exits with a 0.  Otherwise it exits with a 3 or a 6 which causes lpd to
#              NOT print and remove the job from the print queue
#              effectively aborting the print job or it puts the job on hold,
#              respectively.
#
#              job previously put on hold:
#
#              a test is made for the <print queue>/hold.<job number> file. if
#              it exists, then we know that the job was prevously run through this
#              script and it was put on hold. we don't run held jobs through the
#              permission process again because the job is being released by a
#              release station that handles the permission process before it
#              releases the job.
#
# Args:        The input arguments are specified in the printcap file, but are not
#              processed here; that is left to LinuxGateKeeper.
#
# Exit:        0 = printing permission granted
#              3 = printing permission denied
#              6 = hold printing for later release
#

# capture the job number and the spool dir from the parm line
#
# the -j parm has the job number in the form -jnnn
# where nnn is the job number.
#
# the -d parm has the spool directory in the form -dxxx
# where xxx is the path to the spool directory

LOCALJOBNUM=
SPOOLDIR=
CONTROLFILE=

for PARM in "$@"
do
  case ${PARM} in
    -j*) LOCALJOBNUM=${PARM:2} ;;   # skip past the -j part
    -d*) SPOOLDIR=${PARM:2} ;;  # skip past the -d part
    -k*) CONTROLFILE=${PARM:2} ;; #skip past the -k part
  esac
done

DEBUGFILE=
DEBUG=
# uncomment these following 3 lines to enable debugging output
DEBUGFILE="/tmp/filter.debug.${LOCALJOBNUM}"
DEBUG="1>>${DEBUGFILE}"

. ${INSTALLDIR}/variables # this sets INSTALLDIR and JDKLIB
CLASSPATH=${INSTALLDIR}/LinuxGateKeeper.jar:${JDKLIB}:

export CLASSPATH

if [ ${DEBUGFILE} ]
then
  # write out details about the job
  rm -f ${DEBUGFILE}
  touch ${DEBUGFILE}
  eval echo "JDKTOP = " ${JDKTOP} ${DEBUG}
  eval echo "JDKLIB = " ${JDKLIB} ${DEBUG}
  eval echo "JDKBIN = " ${JDKBIN} ${DEBUG}
  eval echo "$0 : $@" ${DEBUG}
  eval echo $0 ": Number of args = " $# ${DEBUG}
  eval echo "CLASSPATH = " ${CLASSPATH} ${DEBUG}
  eval echo "PWD = " ${PWD} ${DEBUG}
  eval echo "USER = " ${USER} ${DEBUG}
  eval echo "INSTALLDIR = ${INSTALLDIR}" ${DEBUG}
  eval echo "SPOOLDIR = ${SPOOLDIR}" ${DEBUG}
  eval echo "LOCALJOBNUM = ${LOCALJOBNUM}" ${DEBUG}
  eval echo "CONTROLFILE = ${CONTROLFILE}" ${DEBUG}
fi

# check to see if this is a previously held job that is being released
#
# if there is a file named LGKhold.nnn in the spool directory,
# then we know that this job was previously held and we
# don't run it through the permissions filter again, we
# just release it.

HOLDFILE="${SPOOLDIR}/LGKhold.${LOCALJOBNUM}";

if [ -f ${HOLDFILE} ]
then
  if [ ${DEBUGFILE} ]
  then
    eval echo "This is a previously held job - job released" ${DEBUG}
  fi
  # proceed to print
  # remove the hold file, we don't need it any more
  `rm -f ${HOLDFILE}`;
  # send stdin to stdout
  cat -
  exit 0 # This return is called JSUCC in the LPRng documentation
fi

#
# call LinuxGateKeeper to get permission to print
#
# LinuxGateKeeper returns the following name=value pairs in its stdout
#
# RC="hold" or "release" or "cancel" (sans quotes, required)
# ACCT="user account id" (in quotes due to possible imbedded whitespace, required)
# DESC="print job description" (in quotes due to possible imbedded whitespace, optional)
#

RETURN_VAL=

if [ ${DEBUGFILE} ]
then

  eval echo  "${JDKBIN}/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=${INSTALLDIR}/LinuxGateKeeper.policy LinuxGateKeeper -INSTALLDIR${INSTALLDIR} $*" ${DEBUG}
  RETURN_VAL=`${JDKBIN}/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=${INSTALLDIR}/LinuxGateKeeper.policy LinuxGateKeeper -INSTALLDIR${INSTALLDIR} $* 2>&1 | tee -a ${DEBUGFILE}`

else

  eval echo  "${JDKBIN}/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=${INSTALLDIR}/LinuxGateKeeper.policy LinuxGateKeeper -INSTALLDIR${INSTALLDIR} $*" ${DEBUG}
  RETURN_VAL=`${JDKBIN}/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=${INSTALLDIR}/LinuxGateKeeper.policy LinuxGateKeeper -INSTALLDIR${INSTALLDIR} $*`

fi

# evaluate the information returned from the release check

RC=
ACCT=
DESC=
JOBNUM=

for PARM in ${RETURN_VAL}
do
  case ${PARM} in
    RC\=*) RC=${PARM:3} ;;          # skip past the "RC=" part
    ACCT\=*) ACCT=${PARM:5} ;;      # skip past the "ACCT=" part
    DESC\=*) DESC=${PARM:5} ;;      # skip past the "DESC=" part
    JOBNUM\=*) JOBNUM=${PARM:7} ;;  # skip past the "JOBNUM=" part
  esac
done

if [ $DEBUGFILE ]
then
  eval echo "RETURN_VAL=${RETURN_VAL}" ${DEBUG}
  eval echo "RC=${RC}" ${DEBUG}
  eval echo "ACCT=${ACCT}" ${DEBUG}
  eval echo "DESC=${DESC}" ${DEBUG}
  eval echo "JOBNUM=${JOBNUM}" ${DEBUG}
fi

if [[ ${RC} == "release" ]]
then
  if [ ${DEBUGFILE} ]
  then
    eval echo "Job released" ${DEBUG}
  fi
  # add the ACCT and DESC fields to the job's control file's -Z option line
  `sed s/LGKacct/${ACCT}/      ${CONTROLFILE}        >${CONTROLFILE}.temp1`;
  `sed s/LGKjobnum/${JOBNUM}/  ${CONTROLFILE}.temp1  >${CONTROLFILE}.temp2`;
  `sed s/LGKdesc/${DESC}/      ${CONTROLFILE}.temp2  >${CONTROLFILE}`;
  `rm -f ${CONTROLFILE}.temp?`;
  # proceed to print
  # send stdin to stdout
  cat -
  exit 0 # This return is called JSUCC in the LPRng documentation
fi

if [[ ${RC} == "hold" ]]
then
  if [ ${DEBUGFILE} ]
  then
    eval echo "Job held" ${DEBUG}
  fi
  # tells me the next time through this script that the job was previously put on hold
  `touch ${HOLDFILE}`;
  # add the ACCT and DESC fields to the job's control file's -Z option line
  `sed s/LGKacct/${ACCT}/      ${CONTROLFILE}        >${CONTROLFILE}.temp1`;
  `sed s/LGKjobnum/${JOBNUM}/  ${CONTROLFILE}.temp1  >${CONTROLFILE}.temp2`;
  `sed s/LGKdesc/${DESC}/      ${CONTROLFILE}.temp2  >${CONTROLFILE}`;
  `rm -f ${CONTROLFILE}.temp?`;
  # tell LPRng to put the job on hold
  exit 6 # This return is called JHOLD in the LPRng documentation
fi

#abort print job
if [ ${DEBUGFILE} ]; then
  eval echo "Job aborted" ${DEBUG}
fi

exit 3 # This return is called JREMOVE in the LPRng dcoumentation

