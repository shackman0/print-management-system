@echo off

rem This script launches the GateKeeper_text.exe program to test JNI.
rem You must modify this script before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\j2sdk1.3.1_01

rem Set the JVM_DLL variable to the name of the JVM.DLL file to use.

set JVM_DLL=jvm.dll

rem Set the SERVER variable to the hostname or the IP address
rem of the Installation Server. Put the value in between the double
rem quotes.

set SERVER="208.20.204.154"

rem Set the USERID variable to the user id to use to log on to
rem the Installation database.

set USERID="PT"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Installation database.

set PASSWORD="ptpasswd"

rem Set the CLIENT variable to the hostname or the IP address
rem of the Client computert. Put the value in between the double
rem quotes.

set CLIENT="localhost"

rem Set the INSTALLATION_ID variable to the id of the installation

set INSTALLATION_ID="150004"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

if not %CLIENT%. == . goto 5continue

echo ERROR: The CLIENT variable is not set.
echo   The CLIENT variable must be properly set before running this
echo   script.

goto end

:5continue

if not %JVM_DLL%. == . goto 99continue

echo ERROR: The JVM_DLL variable is not set.
echo   The JVM_DLL variable must be properly set before running this
echo   script.

goto end

:99continue

set OLDPATH=%PATH%
set PATH=%PATH%;%JAVA%\BIN;%JAVA%\JRE\BIN\CLASSIC;
set CLASSPATH=%JAVA%\lib;.\postgresql.jar;.\GateKeeper.jar;

.\GateKeeper_test.exe -jvmDLL jvm.dll -dbAddr %SERVER% -dbUser %USERID% -dbPasswd %PASSWORD% -instID %INSTALLATION_ID% -clientAddr %CLIENT%

:end

set PATH=%OLDPATH%
set OLDPATH=
set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set CLIENT=
set INSTALLATION_ID=
set CLASSPATH=
