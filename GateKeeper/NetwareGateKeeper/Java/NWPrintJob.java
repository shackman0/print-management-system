import java.io.*;

/**
	This class represents a PrintJob in NetWare.
	Author: Floyd Shackelford

$Header: p:\CVS Repository/PayToPrint/GateKeeper/NetwareGateKeeper/Java/NWPrintJob.java,v 1.1 2009/01/28 18:23:19 Floyd Shackelford Exp $

*/
class NWPrintJob
{
	/**
		This is the size of a chunk to read out of the file.
	*/
	private final static int FILE_CHUNK_SIZE = 8192;
	
	/**
		This is the NetWare job number
	*/
	private int jobNumber;
		
	/**
		This is the NetWare file name (with path)
	*/
	private String fileName;
		
	/**
		This is the file size in bytes
	*/
	private int fileSize;

	/**
		This is the IP address of the station that submitted
		this print job.
	*/
	private String ipAddress;
		
	/**
		Construct a new PrintJob with all the relevent information.
	*/
	public NWPrintJob(int jobNum, String fileName, int fileSize, String ipAddr)
	{
		this.jobNumber = jobNum;
		this.fileName = fileName;
		this.fileSize = fileSize;
		this.ipAddress = ipAddr;
	}
		
	/**
		Returns this print jobs NetWare job number
	*/
	int GetJobNumber()
	{
		return jobNumber;
	}
	
	/**
		Returns the full pathname of the queue file
	*/	
	String GetFileName()
	{
		return fileName;
	}
		
	/**
		Returns the IP address of the submitter of the job,
		or "unavailable" if the submitter is unknown.
	*/
	String GetSubmitterAddress()
	{
		return ipAddress;
	}
		
	/**
		Returns the size of the print job in bytes.
	*/
	int GetFileSize()
	{
		return fileSize;
	}
		
	/**
		Returns an array of bytes representing the job data.
	*/
	byte [] GetJobData() throws Exception
	{
		FileInputStream infile;
		byte [] jobBytes;
		int offset = 0;
		
		// Create a buffer for the job data
		try
		{
			jobBytes = new byte[fileSize];
		}
		catch (Exception error)
		{
			throw new Exception(
				"Couldn't allocate buffer for print job:" + error);
		}
						
		if (jobBytes == null)
		{
			throw new Exception("Couldn't allocate buffer for print job");
		}
		
		// Open the file
		try
		{
			infile = new FileInputStream(fileName);
		}
		catch (IOException error)
		{
			throw new Exception("Couldn't open print file: " + error);
		}

		// Read the data into the file.
		// NOTE: There is a call on FileInputStream that requires
		// only a byte[].  That method fails under NetWare 5.0
		// java based on JDK 1.1.5 with a page fault.  Therefore,
		// we do it piece by piece which seems to work.
		try
		{
			for (offset = 0; offset < fileSize; offset += FILE_CHUNK_SIZE)
			{
				infile.read(jobBytes, offset, FILE_CHUNK_SIZE);
			}
		}
		catch (IOException error)
		{
			infile.close();
			throw new Exception("Couldn't read " + 
				fileSize + " bytes from " + fileName + 
				".  Error was " + error);
		}
			
		// Close the file
		infile.close();

		// Return the data
		return jobBytes;
	}
}
