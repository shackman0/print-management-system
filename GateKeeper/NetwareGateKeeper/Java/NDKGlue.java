/**
	This class is an interface between Java and legacy NetWare libraries
	written for C.

$Header: p:\CVS Repository/PayToPrint/GateKeeper/NetwareGateKeeper/Java/NDKGlue.java,v 1.1 2009/01/28 18:23:19 Floyd Shackelford Exp $

*/
class NDKGlue
{
	/**
		Name of the NLM to load to support GateKeeper operation
	*/
	private static final String NDK_GLUE_LIBRARY_NAME = "Gatekpr";

	/**
		The constructor loads the NLM that contains all of
		our native methods.
	*/
	NDKGlue(Preferences prefs) throws Exception
	{
		int errorCode;

		System.loadLibrary(NDK_GLUE_LIBRARY_NAME);
		InitializeNDKGlue(prefs.get("gkname").toString(), prefs.get("gkpass").toString());
	}
	
	/**
		This method sets some necessary parameters and
		initializes the NetWare libaries.
		@parm gkName String containing the gatekeeper user name.
		@parm gkPass String containing the gatekeeper password.
	**/
	public native void InitializeNDKGlue(String gkName, 
			String gkPass);
		
	/**
		This method gets the next job in the named queue.
		It returns null in the case that no jobs are pending 
		on the queue.
	*/
	public native NWPrintJob GetNextJob(String queueName);


	/**
		This method removes the print job with the given
		job number from the named queue.  It returns the
		NetWare return code from the call.
	*/
	public native int RemoveJobFromQueue(String queueName, 
													int jobNumber);


	/**
		This method moves the print job with the given job
		number from sourceQ to targetQ.  It returns the
		return code from the NetWare call.
	*/
	public native int SwitchQueuesForJob(int jobNumber,
													String sourceQ,
													String targetQ);

	/**
		The main method unit tests the NDKGlue code.
	*/
	public static void main(String args[])
	{
		NWPrintJob job = null;
		NDKGlue glue = null;
		String inputQName = "inputQueue.sys";
		String outputQName = "outputQueue.sys";
		Preferences prefs = null;
		
		try
		{
			prefs = new Preferences("gkprefs.txt");
		}
		catch (Exception error)
		{
			System.err.println("Can't open prefs.txt");
			System.exit(0);
		}
		
		if (args.length == 2)
		{
			inputQName = args[0];
			outputQName = args[1];
		}
		
		try
		{
			glue = new NDKGlue(prefs);
			System.out.println("NDKGlue created.");
		}
		catch (Exception error)
		{
			System.err.println("Couldn't create instance of NDKGlue: "
			+ error);
			System.exit(1);
		}

		job = glue.GetNextJob(inputQName);
		if (job == null)
		{
			System.out.println("No jobs pending");
		}
		else
		{
			System.out.println("Job on input Q");
			System.out.println("  Job Number: " 
					+ job.GetJobNumber());
			System.out.println("  Job File: " 
					+ job.GetFileName());
			System.out.println("  Job Size: " 
					+ job.GetFileSize());
			System.out.println("  IP of submitter: " 
					+ job.GetSubmitterAddress());
			System.out.println();
			
			System.out.println("Status of removing job: " +
				glue.RemoveJobFromQueue(inputQName,
						job.GetJobNumber()));
		}
	
		job = glue.GetNextJob(inputQName);
		if (job == null)
		{
			System.out.println("No jobs pending");
		}
		else
		{
			System.out.println("Job on input Q: " + job.GetJobNumber());
			System.out.println("Status of moving job: " +
					glue.SwitchQueuesForJob(job.GetJobNumber(),
								inputQName,
								outputQName));
	
		}
	}
}
