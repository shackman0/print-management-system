#include <errno.h>
#include <nwqueue.h>
#include <nwdsapi.h>
#include <nwdsdsa.h>
#include <nwconn.h>
#include <nwqueue.h>
#include <process.h>
#include <nwclxcon.h>
#include <conio.h>
#include <string.h>
#include <stdlib.h>
#include <nwdsname.h>
#include <nwbindry.h>


NWCONN_HANDLE gConnection;

void checkError(int errorCode, char *contextString)
{
   if (errorCode != 0)
   {
      ConsolePrintf("Error #%d (%2x) occurred in the context '%s'\n",
		    errorCode, errorCode, contextString);
      exit(errorCode);
   }
}

int setContextToRoot(NWDSContextHandle *context)
{
   int errorCode;
   static char *contextName = "[Root]";
	
   *context = NWDSCreateContext();
   if (*context == (NWDSContextHandle) ERR_CONTEXT_CREATION)
   {
      ConsolePrintf("Couldn't create Context.\n");
      return errno;
   }
	
   ConsolePrintf("Setting context to %s\n", contextName);
	
   errorCode = NWDSSetContext(*context, DCK_NAME_CONTEXT, contextName);
   checkError(errorCode, "Setting context to root");
}

LONG getQueueNumberFromName
(
	char *queueName, 
	NWDSContextHandle context 
)
{
   char canonName[100];
   NWDSCCODE returnCode;
   LONG queueID;
   LONG queueStatus;
   LONG currentEntries;
   LONG currentServers;
   uint16 objType;
	
   returnCode = NWDSMapNameToID(context, 
				gConnection,
				queueName,
				&queueID);

   if (returnCode != 0)
   {
      ConsolePrintf("Couldn't locate queue named '%s'.\n", queueName);
      ConsolePrintf("Error being returned from NWDSMapNameToID is %d (%4x)\n",
		    returnCode, returnCode);
      exit(returnCode);
   }

   ConsolePrintf("Queue ID # of '%s' is %lx\n", queueName, queueID);

   queueID = (LONG) NWLongSwap(queueID);
	
   returnCode = NWQGetStatus (queueID, 
			      &queueStatus, &currentEntries, &currentServers);
   checkError(returnCode, "Getting queue staus");
   ConsolePrintf("Status of queue id #%lx:  %4lx  #Entries: %ld  #Servers: %ld\n",
		 queueID, queueStatus, currentEntries, currentServers);
		
   return queueID;
}

/* Get the address of the submitter of a print job */
int getAddressFromPrintJob(NWQJobRec_t *jobInfo,
			    char ipAddress[24])
{
   NWCCTranAddr buffer;

   NWCCGetConnInfo(jobInfo->client.clientConnNum,
		   NWCC_INFO_TRAN_ADDR, 
		   sizeof(buffer),
		   &buffer);
   if (buffer.type == NWCC_TRAN_TYPE_TCP ||
       buffer.type == NWCC_TRAN_TYPE_TCP6)
   {
      strncpy(ipAddress, (char *) buffer.buffer, buffer.len);
      ipAddress[buffer.len] = 0;
      return 0;
   }

   return 1;
}


void cleanup(void)
{
  ConsolePrintf("Cleaning up prior to exit.\n");
  /* Set status of queue to "unserviced" */
  /* Free RMI resources */
}


void initializeNLM()
{
   NWCallsInit(NULL,NULL);
   atexit(cleanup);
}

/*
  Command-line parameters:
  argv[0] - Name of this NLM
  argv[1] - Fully distinguished Name of the input queue
  argv[2] - Fully distinguished Name of the output queue
*/
void main(int argc, char *argv[])
{
   NWDSCCODE returnCode;
   NWDSContextHandle context;
   LONG inputQID, outputQID;
   LONG newJobNumber;
   WORD jobCount, jobNumber;
   NWQJobRec_t jobInfo;
   unsigned sleepTime = 5000;
   int allowedToPrint = 0;
   char clientIPAddress[24];
  
   /* Initialize */
   /* call function to set up NLM netware-specific parameters */
   initializeNLM(argc, argv);

   /* Read our configuration file */
/*  readConfiguration(argc, argv); */

   // Contact resource server
   // end contact resource server
   while (1)
   {
   returnCode = setContextToRoot(&context);

   returnCode = NWCCOpenConnByName(0, "TRACKER5", 
				   NWCC_NAME_FORMAT_BIND, NWCC_OPEN_UNLICENSED, NWCC_TRAN_TYPE_WILD, &gConnection);
   	
   checkError(returnCode, "Getting connection handle.");
   	
   ConsolePrintf("Connection handle is: %x\n", gConnection);

   inputQID = getQueueNumberFromName(argv[1], context);
   outputQID = getQueueNumberFromName(argv[2], context);

      returnCode = GetQueueJobList((long) inputQID, &jobCount, &jobNumber, 1);
      checkError(returnCode, "Couldn't get job list from queue");

      while (jobCount > 0)
      {
	 /* There is a job in the queue */
	 ConsolePrintf("Processing Job Number: %d\n", jobNumber & 0x0fff);

	 /* Get a new JIPrintJob */

	 returnCode = NWQGetJobEntry(inputQID,
				     jobNumber,
				     &jobInfo);

	 /* Get IP address of submitting station */
	 getAddressFromPrintJob(&jobInfo,
				clientIPAddress);

	 /* Set parameters on JIPrintJob */

	 /* Get path name to Queue */
	 /*
	   getPathFromDirectoryEntry(inputPath);
	   strcat(inputPath, jobInfo.assocFileName);

	   fileHandle = open(inputPath, O_RDONLY);
	   read(fileHandle);
	  
	   close(fileHandle);
	 */

	 allowedToPrint = 0; /* !allowedToPrint; */

	 ConsolePrintf("Job #%d ", jobNumber & 0xfff);

	 if (allowedToPrint)
	 {
	    ConsolePrintf("allowed to print.\n");
	    returnCode = NWQChangeJobQueue(inputQID,
					   jobNumber,
					   outputQID,
					   &newJobNumber);
	    checkError(returnCode, "Changing job from input to output queue");
	 }
	 else
	 {
	    ConsolePrintf("denied printing.\n");
	    returnCode = NWQRemoveJob(inputQID, jobNumber);
	    checkError(returnCode, "Removing job from queue");
	    ConsolePrintf("Removed entry #%d from queue.\n", jobNumber & 0xfff);
	 }

	 returnCode = GetQueueJobList((long) inputQID, &jobCount, &jobNumber, 1);
	 checkError(returnCode, "Couldn't get job list from queue");

      }

  NWCCCloseConn(gConnection);
      /* Now pause for a while */
      delay(sleepTime);
   }
}
