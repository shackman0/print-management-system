/**
	NDKGlue.c

	This file contains the implementation of the native methods of the
	NDKGlue.java file.  In order for this file to build correctly, the
	NDKGlue.h file must be present.	This file is dependent upon 
	NDKGlue.java.	If NDKGlue.java's interface is modified, then
	NDKGlue.h needs to be re-generated with the command:
	javah -jni NDKGlue

$Header: p:\CVS Repository/PayToPrint/GateKeeper/NetwareGateKeeper/C/NDKGlue.c,v 1.1 2009/01/28 18:23:19 Floyd Shackelford Exp $

**/

#include <nwdsdsa.h>
#include <nwdsapi.h>
#include <nwconn.h>
#include <nwconnec.h>
#include <nwclxcon.h>
#include <errno.h>
#include <nwqueue.h>
#include <advanced.h>
#include <stdlib.h>
#include <nwqms.h>
#include <stat.h>
#include <nwdp_lib.h>
#include <nwdp_atr.h>
#include <nwdp_att.h>
#include <nwdp_wko.ogh>
#include <string.h>
#include <stdlib.h>
#include "NDKGlue.h"


/* #define DEBUGGING */

/**
	Maximum number of connection to process at one time 
	when searching for a connected user.
**/
#define MAX_CONNEX 100L

/**
	Maximum length of a file/path name in NetWare
**/
#define MAX_PATH_LENGTH 1024
typedef char PathName[MAX_PATH_LENGTH];

/**
	Format of path to queue file.  The %lx is the
	queueID, while the %s is the filename extracted
	from a NWQueueJobStruct.
**/
#define FORMAT_OF_Q_PATH "SYS:\\QUEUES\\%lx.QDR\\%s"

/**
	Size of a chunk of banner page data to read in in one
	gulp while searching for job submitter's name.
**/
#define MAX_BANNER_LENGTH 512

/**
	Maximum length of a user name in NetWare
**/
#define MAX_NAME_LENGTH 50

/**
	Tag string to search for in a banner page to locate the
	submitter's name.
**/
#define OWNER_TAG "Job Owner"

/**
	This is the name of the root context
**/
#define ROOT_CONTEXT "[Root]"

/**
	This is the maximum length of an IP address
	xxx.xxx.xxx.xxx + nul terminator
**/
#define MAX_IP_ADDRESS_LENGTH 16

/**
	Here is a type that is meant to hold IP address strings.
**/
typedef char IPAddress[MAX_IP_ADDRESS_LENGTH];

/**
	This is the type that receives the network number
	in a call to GetInternetAddress
**/
#define NETWORK_NUMBER_SIZE 4
typedef unsigned char NetworkNumber[NETWORK_NUMBER_SIZE];

/**
	This is the type that receives the node address
	in a call to GetInternetAddress
**/
#define NODE_ADDRESS_SIZE 6
typedef char NodeAddress[NODE_ADDRESS_SIZE];

/**
	This is what the IP Address is set to if one can't
	be determined.
**/
#define IP_ADDRESS_UNAVAILABLE "unavailable"

/**
	This is the name of the Java class that represents a print job
**/
#define PRINT_JOB_CLASS_NAME "PrintJob"

/**
	This is the JNI code name for a constructor
**/
#define CONSTRUCTOR_METHOD_NAME "<init>"

/**
	This is the signature of the PrintJob constructor as
	given by the javap program.  If the PrintJob constructor
	changes, this string must be replaced.
	Use the command: javap -s PrintJob to generate the string.
**/
#define PRINT_JOB_CONSTRUCTOR_SIGNATURE \
			"(ILjava/lang/String;ILjava/lang/String;)V"
			
/**
	This is the thread count used by NetWare to keep track of
	whether or not this NLM is in use.
**/
extern long GateKeeper_NLM_threadCnt;

long GateKeeper_NLM_threadCnt = 0;

/**
	This is the name and password of the gatekeeper user.
	These values are set during initialization.
**/
char gateKeeperName[MAX_NAME_LENGTH] = "";
char gateKeeperPass[MAX_NAME_LENGTH] = "";

/**
	getQueueInfoFromName

	Given the fully-qualified ( not necessarily canonical) name of a
	print queue, along with a valid context and connection, this
	function determines the queue's ID, its status, the number of
	entries, and the number of registered servers on that queue.

	If any errors occur during the process of determining this,
	the error code is returned and the information in the OUT
	parameters may not be valid.
**/
static NWDSCCODE getQueueInfoFromName(
	char *queueName,				/* Name of queue (IN) */
	NWDSContextHandle context,	/* Current context (IN) */
	NWCONN_HANDLE connection,	/* Current connection (IN) */
	LONG *queueID,					/* ID of named queue (OUT) */
	LONG *queueStatus,			/* Status of named queue (OUT) */
	LONG *currentEntries,		/* # of jobs on named queue (OUT) */
	LONG *currentServers )		/* # of registered servers
											on named queue */
{
	NWDSCCODE returnCode;			/* Standard return code */
	LONG serverIDlist[25];		/* Required parameter, ignored. */
	LONG serverConnList[25];	/* Required parameter, ignored. */
	
	/* Convert name to ID using system call */
	returnCode = NWDSMapNameToID( context, 
											connection,
											queueName,
											queueID );

	/* If successful, read the information about the queue */
	if ( returnCode == N_SUCCESS )
	{
		returnCode = (NWDSCCODE) NWReadQueueCurrentStatus2( connection, 
			*queueID, queueStatus, currentEntries, currentServers,
			serverIDlist, serverConnList );
	}
	else
	{
		ConsolePrintf( "Couldn't get ID for queue name '%s'\n",
			queueName );
	}
	
	return returnCode;
}


/*
	setContextToRoot creates a new context and sets it to
	the root context.	 It returns 0 on success, or the
	NetWare error code on failure.
	
	On success, the passed-in context handle is set to the
	newly created context.
*/
static int setContextToRoot( NWDSContextHandle *context )
{
	int errorCode;	/* Standard return code */
	
	/* Create the new context */
	*context = NWDSCreateContext( );
	
	if ( *context == ( NWDSContextHandle ) ERR_CONTEXT_CREATION )
	{
		ConsolePrintf( "Couldn't set context to '%s'\n", ROOT_CONTEXT );
		/* Set the return code to the global NetWare error number */
		errorCode = errno;
	}
	else	
	{
		/* Context was created successfully.  Set it to [Root]. */
		errorCode = 
			NWDSSetContext( *context, DCK_NAME_CONTEXT, ROOT_CONTEXT );
	}

#ifdef DEBUGGING
	ConsolePrintf("Return code from SetContext: %x\n", errorCode);
#endif

	return errorCode;
}


/*
  This function establishes a context and connection for the
  current call.  It should be the first call made upon entry
  to a native method.  Although there is some overhead in doing this
  for each call from Java, it is better than building up a big
  list of connections that have not been released.
*/
static int setContextAndConnection ( NWDSContextHandle *context,
												 NWCONN_HANDLE *connection )
{
	int returnCode;
	char serverName[MAX_DN_CHARS+1];

	/* Increment reference count for this library 
		since we're in use. */
	++GateKeeper_NLM_threadCnt;

	/* Establish Root context */
	returnCode = setContextToRoot( context );

	if ( returnCode != N_SUCCESS )
	{
		return returnCode;
	}
	
	/* Create a connection to the server we're running on. */
	
	GetFileServerName( GetDefaultFileServerID( ), serverName );
					
	returnCode = 
			NWCCOpenConnByName( 0, serverName, 
									  NWCC_NAME_FORMAT_BIND, 
									  NWCC_OPEN_UNLICENSED, 
									  NWCC_TRAN_TYPE_WILD, 
									  connection );

	if ( returnCode != N_SUCCESS )
	{
		ConsolePrintf( "Error %x creating connection to server '%s'\n",
			returnCode, serverName );
		return returnCode;
	}
#ifdef DEBUGGING
	else
	{
		ConsolePrintf( "Successully created connection.\n");
	}
#endif
	
	/* Log In as the GateKeeper user so we have access to the queues. */
	returnCode = NWDSLogin( *context, 0, gateKeeperName, 
					gateKeeperPass, 0 );
			
	if ( returnCode != N_SUCCESS )
	{
		ConsolePrintf( "Error %x logging in as user '%s'.\n",
				returnCode, gateKeeperName );
		return returnCode;
	}
#ifdef DEBUGGING
	else
	{
		ConsolePrintf( "Successully logged in as user '%s'.\n",
				gateKeeperName);
	}
#endif
	

	returnCode = NWDSAuthenticate( *connection, 0, NULL );

	if ( returnCode != N_SUCCESS )
	{
		ConsolePrintf( "Error %x authenticating GateKeeper Login.\n",
			returnCode );
		return returnCode;
	}
#ifdef DEBUGGING
	else
	{
		ConsolePrintf( "Successully authenticated.\n");
	}
#endif
	
		
	return returnCode;
}	 


/*
  This function releases the NetWare resources allocated by
  a previous call to setConectAndConnection.
*/
static void releaseContextAndConnection ( NWDSContextHandle context,
											  NWCONN_HANDLE connection )
{
	NWDSLogout( context );
	NWCCCloseConn( connection );
	NWDSFreeContext( context ) ;
	
	/* Decrement reference count for this library 
		since we're no longer in use. */
	--GateKeeper_NLM_threadCnt;
}


/*
	This function determins the NetWare ID of the submitter
	of a print job given the filename of the print job.
	It would have been nice to use stat() to get the ownerID,
	but it turns out that the owner of the queue file is not
	the submitter of the job, but rather the owner of the
	NDPS Printer Agent that put the job in the queue.
	
	The way this function works is to assume (i.e. 'require')
	that the job has a banner page and that that page is one
	of the standard ones furnished by Novell.
	
	If the user ID cannot be determined, this function returns
	0.
*/
static LONG getOwnerIDFromFileName
(
	PathName fileName,			/* Name of file to find owner of */
	NWDSContextHandle context,	/* Current Context */ 
	NWCONN_HANDLE connection	/* Current connection */
)
{
	LONG ownerID = 0;		/* ID of file owner, to be returned */
	int returnCode;		/* Standard return code */

	FILE *input;			/* FILE to read banner from */
	char buffer[MAX_BANNER_LENGTH+1];	/* Buffer to hold banner */
	char *owner = NULL;	/* Pointer to owner's name in the banner */
	
	int index;
	long fileOffset = 0;
	
	input = fopen( fileName, "rb" );
#ifdef DEBUGGING
	ConsolePrintf("Opened file '%s': %p\n", fileName, input);
#endif

	while (!feof( input ))
	{
		/* Read a block of data */
		
		fread( buffer, MAX_BANNER_LENGTH, 1, input );
		
		/* Replace nuls with spaces so string searches work */
		
		for ( index = 0; index < MAX_BANNER_LENGTH; ++index )
		{
			if ( buffer[index] == 0 ) buffer[index] = ' ';
		}

		/* Terminate the buffer to avoid run-away searches. */
		buffer[MAX_BANNER_LENGTH - 1] = 0;
		
		/* Scan buffer for "Job Owner:" */
		owner = strstr( buffer, OWNER_TAG );
		
		/*
			If we found the tag, AND it's early enough in the buffer
			that it doesn't run off the end of the buffer, then we
			can extract the user name.
		*/
		if ( owner != NULL && 
			(( owner - buffer ) <= 
				( MAX_BANNER_LENGTH - strlen( OWNER_TAG ) - MAX_NAME_LENGTH )) )
		{
			/* The owner name starts immediately after the tag */
			owner += strlen( OWNER_TAG );
			
			/* The owner name always begins with a . */
			while (*owner != '.')
			{
				++owner;
			}
			
			/*
				This string isn't nul delimited.  Find the first character that
				can't be part of a NetWare user name and terminate the string there.
			*/			
			for ( index = 0; ( &owner[index] - buffer ) < MAX_BANNER_LENGTH; ++index )
			{
				if (( isalnum( owner[index] ) == FALSE ) && 
						owner[index] != '.' && 
						owner[index] != '_' )
				{
					break;
				}
			}
			
			owner[index] = 0;
			break;
		}
		else
		{
			/*
				Tag wasn't in this chunk of the file.  Back up a little
				so the next read will overlap this one by the length of
				the tag + the max user name.
			*/
			if (!feof(input))
			{
				fseek( input, -( (long) strlen( OWNER_TAG ) + MAX_NAME_LENGTH ), SEEK_CUR );
			}
			else
			{
				ConsolePrintf("Owner name not found in file '%s'.\n", 
						fileName);
			}
		}
	}
	
	fclose( input );
	
	if ( owner != NULL )
	{
	
#ifdef DEBUGGING
		ConsolePrintf( "Owner: '%s'\n", owner );
#endif

		returnCode = NWDSMapNameToID( context, 
												connection,
												owner,
												&ownerID );
											
		if ( returnCode != N_SUCCESS )
		{
			ConsolePrintf( "Couldn't get ID of '%s'\n", owner );
			ConsolePrintf( "Error returned was %x\n", returnCode );
			ownerID = 0;	/* Just to make sure */
		}
		else
		{
			ownerID = (LONG) NWLongSwap( ownerID );
#ifdef DEBUGGING
			ConsolePrintf("Sucessfully got ID of '%s'\n", owner);
#endif
		}
	}
	
	return ownerID;
}


/**
	Given a NetWare object ID, this function scans all the 
	currently active connections for that ID to find the
	first one with a valid IP address.
	That address is then placed into the given ipAddress
	string  If there is no such conenction, then the ipAddress
	is set to "unavailable".
**/
static int getAddressFromObjectID
(
	LONG objectID,			/* Object to locate the address of */
	IPAddress ipAddress	/* IP Address of object's connection */
)
{
	int returnCode;		/* Standard return code */
	
   NetworkNumber networkNumber;
   NodeAddress nodeAddress;

	/* Parameters for call to GetConnectionList */   
   LONG numberReturned;
   LONG connectionList[MAX_CONNEX];
   
   int index;	/* Generic index */

	/* Initialize IP Address */
	strcpy( ipAddress, IP_ADDRESS_UNAVAILABLE );
	
	/* Check objectID for validity. */
	if ( objectID == 0 )
	{
		return !N_SUCCESS;
	}
	
	/* Get list of connections for this object */
	returnCode = GetConnectionList ( objectID, 0, 
			&numberReturned, connectionList, MAX_CONNEX );
			
	if ( returnCode == N_SUCCESS )
	{
	
#ifdef DEBUGGING
		ConsolePrintf( "Got %ld connection number(s):", numberReturned );
		for ( index = 0; index < numberReturned; ++index )
		{
			ConsolePrintf( "  %ld", connectionList[index] );
		}
		
		ConsolePrintf( "\n" );
#endif

		/* Scan connection list for active connection */
		for ( index = 0; index < numberReturned; ++index )
		{
			returnCode = GetInternetAddress( (unsigned short) connectionList[index], 
														(char *) networkNumber, nodeAddress );

			/* Any non-zero value indicates an IP address */
			if ( 	(networkNumber[0] != 0) || 
					(networkNumber[1] != 0) ||
					(networkNumber[2] != 0) || 
					(networkNumber[3] != 0) )
			{
				/* Format the address */
				sprintf( ipAddress, "%d.%d.%d.%d", 
					networkNumber[0],
					networkNumber[1],
					networkNumber[2],
					networkNumber[3] );

#ifdef DEBUGGING					
				ConsolePrintf( "\nIP address of object: %s\n", ipAddress );
#endif
				/* Stop looking as soon as we find one */
				break;
			}
		}
	}
	else
	{
		ConsolePrintf( "Couldn't get connection list: %x\n", returnCode );
	}
	
	return returnCode;
}


/*
	This function builds a new Java PrintJob object given the
	information for that job.
*/
static jobject constructPrintJob
(
	JNIEnv *env,			/* Ubiquitous Java environment (IN) */
	LONG jobNumber,		/* Job number of job (IN) */
	PathName fileName,	/* Name of Q file job is stored in (IN) */
	LONG fileSize,			/* Size of Q file in bytes (IN) */
	IPAddress ipAddress	/* IP address of Job submitter (IN) */
)
{
	jobject job;				/* The PrintJob to be returned */
	jstring javaFileName;	/* Java String rep of fileName parameter */
	jstring javaIPAddress;	/* Java String rep of IP address parameter */
	
	jint javaFileSize = (jint) fileSize;	/* Java int rep of fileSize parameter */
	jint javaJobNumber = (jint) jobNumber;	/* Java int rep of jobNumber parameter */
	
	jclass clazz;		/* Descriptor of class to create (i.e. PrintJob) */
	jmethodID mid;		/* Method ID of constructor */

#ifdef DEBUGGING
	ConsolePrintf( "Creating PrintJob\n" );
	ConsolePrintf( "  Job #%lx\n", jobNumber );
	ConsolePrintf( "  File name: '%s'\n", fileName );
	ConsolePrintf( "  File size: %ld\n", fileSize );
	ConsolePrintf( "  IP: '%s'\n", ipAddress );
#endif

	/* Convert strings into Java strings */	
	javaFileName = (*env)->NewStringUTF( env, fileName );
	javaIPAddress = (*env)->NewStringUTF( env, ipAddress );
	
	/* Set up to call constructor */
	clazz = (*env)->FindClass( env, PRINT_JOB_CLASS_NAME );
	mid = (*env)->GetMethodID( env, clazz, CONSTRUCTOR_METHOD_NAME, 
			 PRINT_JOB_CONSTRUCTOR_SIGNATURE );
			
	/* Construct new object */
	job = (*env)->NewObject( env, clazz, mid, javaJobNumber, javaFileName,
								javaFileSize, javaIPAddress );

	return job;
}


/*
	This function returns a new Java PrintJob object for the job
	with the given jobNumber from the given queue.  NULL is returned
	if an error occurs or if there is no job with that number pending.
*/
jobject getJob
(
	JNIEnv *env,					/* Ubiquitous Java environment */
	LONG queueID,					/* ID of queue to get job from */
	LONG jobNumber,				/* Number of job to get */
	NWCONN_HANDLE connection,	/* Current connection */
	NWDSContextHandle context	/* Root context */
)
{
	NWCCODE returnCode;			/* Standard return code */

	PathName fullPathFileName;	/* Will hold name of Q file */
	LONG fileSize;
	LONG ownerID;
	IPAddress ipAddr;
	NWQueueJobStruct jobInfo;
	jobject job = NULL;
    
	/* Get info for job with given jobNumber */
	returnCode = NWReadQueueJobEntry2( connection, queueID, jobNumber, &jobInfo );
	
	if ( returnCode != N_SUCCESS )
	{
		ConsolePrintf("Error #%x reading entry for job #%ld\n", 
			returnCode, jobNumber);
		return job;
	}
	
	returnCode = NWGetQueueJobFileSize2( connection, queueID, 
			jobNumber, &fileSize );

	if ( returnCode != N_SUCCESS )
	{
		ConsolePrintf("Error #%x getting size for job #%ld\n", 
			returnCode, jobNumber);
		return job;
	}
	
	/* Create fully qualified file name from queue ID and job filename */
	sprintf( fullPathFileName, FORMAT_OF_Q_PATH, 
			NWLongSwap( queueID ), jobInfo.jobFileName );
			
	/* Use file name to get NetWare ID of owner of this job. */
	ownerID = getOwnerIDFromFileName( fullPathFileName, context, connection );
		
#ifdef DEBUGGING
		ConsolePrintf( "Owner ID of file is %ld\n", ownerID );
#endif

	returnCode = (unsigned int) getAddressFromObjectID( ownerID, ipAddr );

	/* The job is valid even if getAddressFromObjectID fails. */
	
#ifdef DEBUGGING
	ConsolePrintf( "IP address of that user is %s\n", ipAddr );
#endif

	/* 
		Now we have all the necessary parameters.  
		Construct the PrintJob.
	*/
	job = constructPrintJob( env, jobNumber, 
			fullPathFileName, fileSize, ipAddr );
	
	return job;
}

/**
	Class:     NDKGlue
	Method:    GetNextJob
	Signature: (Ljava/lang/String;)LPrintJob;

	This function returns the Java PrintJob object representing
	the next print job on the named queue, or null if there is
	no object pending.
*/
JNIEXPORT
jobject
JNICALL
Java_NDKGlue_GetNextJob
(
	JNIEnv *env,
	jobject glue,
	jstring javaQueueName
)
{
	NWDSContextHandle context;	/* Context of this process */
	NWCONN_HANDLE connection;	/* Connection handle to the server */

	LONG queueID;
	LONG queueStatus;
	LONG currentEntries;
	LONG currentServers;

	QueueJobListReply jobList;
	LONG jobNumber;
	jobject job = NULL;
	
	NWDSCCODE returnCode;
	
	char *queueName = NULL;
	jboolean isCopy;

#ifdef DEBUGGING
	ConsolePrintf("In getJob\n");
#endif

	returnCode = setContextAndConnection( &context, &connection );

	if ( returnCode == N_SUCCESS )
	{
		/* Convert java queue name into a C string */
		queueName = 
			(char *) (*env)->GetStringUTFChars( env, 
														  javaQueueName, 
														  &isCopy );
		  
		returnCode = getQueueInfoFromName( queueName, 
													 context,
													 connection,
													 &queueID,
													 &queueStatus,
													 &currentEntries,
													 &currentServers );

		/* Release memory for Java String if necessary */
		if ( isCopy != JNI_FALSE )
		{
			(*env)->ReleaseStringUTFChars( env, javaQueueName, queueName );
		}
	
		if ( returnCode != N_SUCCESS )
		{
			ConsolePrintf( "Couldn't get info for queue: %x.\n", returnCode );
			return job;
		}
		
		returnCode = 
			(int) NWGetQueueJobList2 ( connection, queueID, 1, &jobList );
			
		if ( returnCode == N_SUCCESS && ( jobList.totalQueueJobs > 0 ))
		{
			jobNumber = jobList.jobNumberList[0];
			job = getJob( env, queueID, jobNumber, connection, context );
		}
	}

	releaseContextAndConnection( context, connection );

#ifdef DEBUGGING
	ConsolePrintf("Leaving getJob, job = %p\n", job);
#endif

	return job;
}


/**
   This method removes the print job with the given
   job number from the named queue.  It returns the
   NetWare return code from the call.
**/
JNIEXPORT
jint
JNICALL
Java_NDKGlue_RemoveJobFromQueue
(
	JNIEnv *env,
	jobject glue,
	jstring javaQueueName,
	jint javaJobNumber
)
{
	NWDSContextHandle context;	/* Context of this process */
	NWCONN_HANDLE connection;	/* Connection handle to the server */

	LONG queueID;
	LONG queueStatus;
	LONG currentEntries;
	LONG currentServers;

	LONG jobNumber = (LONG) javaJobNumber;

	NWDSCCODE returnCode;
	
	char *queueName = NULL;
	jboolean isCopy;

	returnCode = setContextAndConnection( &context, &connection );

	if ( returnCode == N_SUCCESS )
	{
		/* Convert java queue name into a C string */
		queueName = 
			(char *) (*env)->GetStringUTFChars( env,
														  javaQueueName, 
														  &isCopy );
		  
		returnCode = getQueueInfoFromName( queueName, 
													 context,
													 connection,
													 &queueID,
													 &queueStatus,
													 &currentEntries,
													 &currentServers );
					
		if ( isCopy != JNI_FALSE )
		{
			(*env)->ReleaseStringUTFChars( env, javaQueueName, queueName );
		}
	
		returnCode = (int) NWRemoveJobFromQueue2( connection, queueID, jobNumber );
	}

	releaseContextAndConnection( context, connection );

	return (jint) returnCode;
}


/**
	This method moves the print job with the given job
	number from sourceQ to targetQ.  It returns the
	return code from the NetWare call.
**/
JNIEXPORT
jint
JNICALL
Java_NDKGlue_SwitchQueuesForJob
(
	JNIEnv *env,
	jobject glue, 
	jint javaJobNumber,
	jstring javaSourceQ,
	jstring javaDestQ
)
{
	NWDSContextHandle context;	/* Context of this process */
	NWCONN_HANDLE connection;	/* Connection handle to the server */

	LONG sourceQID;
	LONG destQID;
	LONG queueStatus;
	LONG currentEntries;
	LONG currentServers;

	LONG jobNumber = (LONG) javaJobNumber;
	LONG newJobNumber;

	NWDSCCODE returnCode;
	
	char *queueName = NULL;
	jboolean isCopy;

	returnCode = setContextAndConnection( &context, &connection );

	if ( returnCode == N_SUCCESS )
	{
		/* Convert java queue name into a C string */
		queueName = 
			(char *) (*env)->GetStringUTFChars( env, 
														  javaSourceQ, &isCopy );
		  
		/* Get ID of source queue */
		returnCode = getQueueInfoFromName( queueName, 
													 context,
													 connection,
													 &sourceQID,
													 &queueStatus,
													 &currentEntries,
													 &currentServers );
					
		if ( isCopy != JNI_FALSE )
		{
			(*env)->ReleaseStringUTFChars( env, javaSourceQ, queueName );
		}
		/* Now do the same for the destination queue */
	
		queueName = 
			(char *) (*env)->GetStringUTFChars( env, 
														  javaDestQ, &isCopy );
		  
		/* Get ID of source queue */
		returnCode = getQueueInfoFromName( queueName, 
													 context,
													 connection,
													 &destQID,
													 &queueStatus,
													 &currentEntries,
													 &currentServers );
					
		if ( isCopy != JNI_FALSE )
		{
			(*env)->ReleaseStringUTFChars( env, javaDestQ, queueName );
		}
	
		  
		returnCode = NWQChangeJobQueue( (LONG) NWLongSwap( sourceQID ),
													(LONG) NWLongSwap( jobNumber ),
													(LONG) NWLongSwap( destQID ),
											 		&newJobNumber );

	}

	releaseContextAndConnection( context, connection );

	return (jint) returnCode;
}


JNIEXPORT
void
JNICALL
Java_NDKGlue_InitializeNDKGlue
(
	JNIEnv *env,
	jobject glue,
	jstring gkName,
	jstring gkPass
)
{
	char *tempString;	/* Used to convert from jstring to C string */
	jboolean isCopy;
	
#ifdef DEBUGGING
	ConsolePrintf("Initializing NDKGlue.\n");
#endif
	NWCallsInit(NULL, NULL);

	tempString = 
		(char *) (*env)->GetStringUTFChars( env,
													  gkName, 
													  &isCopy );
		  
	strcpy(gateKeeperName, tempString);
					
	if ( isCopy != JNI_FALSE )
	{
		(*env)->ReleaseStringUTFChars( env, gkName, tempString );
	}

	tempString = 
		(char *) (*env)->GetStringUTFChars( env,
													  gkPass, 
													  &isCopy );
		  
	strcpy(gateKeeperPass, tempString);
					
	if ( isCopy != JNI_FALSE )
	{
		(*env)->ReleaseStringUTFChars( env, gkPass, tempString );
	}
}


void main( void )
{
	ExitThread( TSR_THREAD, 0 ); /*so shlib's symbols remain resident in symbol table*/
}
