@echo off

echo the build_c.bat script needs to be completed before execution
goto end99

rem This script builds the C portion of the NetWare GateKeeper application.

rem You must pass in the location of the JDK, or set the JDK variable below.

rem Set the JDK variable to point to where the Java Development Kit
rem and supporting files are located. Use an absolute path.

set JDK=\J2SDK1.3.1_01

set C_PATH="???"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

rem check to see if the location of JDK was passed in

if %1. == . goto 1continue

set JDK=%1
goto 2continue

:1continue

if not %JDK%. == . goto 2continue

echo ERROR: The JDK variable is not set.
echo   The JDK variable must be properly set before running this
echo   script.

goto end

:2continue

set OLDPATH=%PATH%
set PATH=%CPP_PATH%\bin;%PATH%

rem clean up the target directory
if exist .\Obj\*.obj del /f /q .\Obj\*.obj >nul

echo Building NetWare GateKeeper C files now ...

???

:end

set JDK=
set CPP_PATH=
set PATH=%OLDPATH%

:end99
