@echo off

echo Use MS Visual Studio to build the gatekeeper
goto end99

rem This script builds the C++ portion of the GateKeeper application.

rem You must pass in the location of the JDK, or set the JDK variable below.

rem Set the JDK variable to point to where the Java Development Kit
rem and supporting files are located. Use an absolute path.

set JDK=\J2SDK1.3.1_01

set CPP_PATH="\Program Files\Borland C++ v5.5"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

rem check to see if the location of JDK was passed in

if %1. == . goto 1continue

set JDK=%1
goto 2continue

:1continue

if not %JDK%. == . goto 2continue

echo ERROR: The JDK variable is not set.
echo   The JDK variable must be properly set before running this
echo   script.

goto end

:2continue

set OLDPATH=%PATH%
set PATH=%CPP_PATH%\bin;%PATH%

rem clean up the target directory
if exist .\Obj\*.obj del /f /q .\Obj\*.obj >nul

echo Building GateKeeper C++ files now ...

bcc32.exe -c -g5 -I%CPP_PATH%\Include -I%JDK%\include -I%JDK%\include\win32 -n.\Obj -WC -w .\Cpp\Gatekeeper.cpp
bcc32.exe -c -g5 -I%CPP_PATH%\Include -I%JDK%\include -I%JDK%\include\win32 -n.\Obj -WC -w .\Cpp\Gatekeeper_test.cpp
ilink32.exe -ap -Tpe -E5 -w -j%CPP_PATH%\Lib;.\Obj -L. c0x32.obj GateKeeper.obj GateKeeper_test.obj, GateKeeper_test, , cw32.lib import32.lib jvm_omf.lib

rem delete extraneous compiler and linker output files:
if exist .\Obj\*.tds del .\Obj\*.tds
if exist Gatekeeper_test.il? del Gatekeeper_test.il?
if exist Gatekeeper_test.map del Gatekeeper_test.map
if exist Gatekeeper_test.tds del Gatekeeper_test.tds

:end

set JDK=
set CPP_PATH=
set PATH=%OLDPATH%

:end99
