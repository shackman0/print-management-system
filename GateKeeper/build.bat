@echo off

rem This script builds the GateKeeper application.
rem You must pass in the location of JAVA, or set the JAVA variable below.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\j2sdk1.3.1_01

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

rem check to see if the location of JAVA was passed in

if %1. == . goto 1continue

set JAVA=%1
goto 2continue

:1continue

if not %JAVA%. == . goto 2continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:2continue

call .\build_java.bat %JAVA%
call .\build_ms_cpp.bat %JAVA%

cd .\LinuxGateKeeper
call .\build.bat %JAVA%
cd ..

rem cd .\NetwareGateKeeper
rem call .\build.bat %JAVA%
rem cd ..

:end

