@echo off

rem This script launches the GateKeeper java test.
rem You must modify this script before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem Set the SERVER variable to the hostname or the IP address
rem of the Installation Server. Put the value in between the double
rem quotes.

set SERVER="pay2print"

rem Set the USERID variable to the user id to use to log on to
rem the Installation database.

set USERID="PTP"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Installation database.

set PASSWORD="PTPPASSWD"

rem Set the CLIENT variable to the hostname or the IP address
rem of the Client computer. Put the value in between the double
rem quotes.

set CLIENT="localhost"

rem Set the INSTALLATION_ID variable to the id of the installation

set INSTALLATION_ID="120001"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

if not %CLIENT%. == . goto 99continue

echo ERROR: The CLIENT variable is not set.
echo   The CLIENT variable must be properly set before running this
echo   script.

goto end

:99continue

%JAVA%\bin\java -classpath .;%JAVA%\lib;.\postgresql.jar;.\GateKeeper.jar; -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=GateKeeper.policy GateKeeper -dbaddr %SERVER% -dbuser %USERID% -dbpasswd %PASSWORD% -instID %INSTALLATION_ID% -clientAddr %CLIENT%

:end

set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set CLIENT=
set INSTALLATION_ID=
