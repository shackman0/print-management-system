@echo off

rem This script builds the C++ portion of the GateKeeper application.

rem You must pass in the location of the JDK, or set the JDK variable below.

rem Set the JDK variable to point to where the Java Development Kit
rem and supporting files are located. Use an absolute path.

set JDK=\j2sdk1.3.1_01

set CPP_PATH=\Program Files\Microsoft Visual Studio\VC98

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %VSCommonDir%. == . goto 0continue

rem
rem Root of Visual Developer Studio Common files.
set VSCommonDir=C:\PROGRA~1\MICROS~9\Common

rem
rem Root of Visual Developer Studio installed files.
rem
set MSDevDir=C:\PROGRA~1\MICROS~9\Common\msdev98

rem
rem Root of Visual C++ installed files.
rem
set MSVCDir=C:\PROGRA~1\MICROS~9\VC98

rem
rem VcOsDir is used to help create either a Windows 95 or Windows NT specific path.
rem
set VcOsDir=WIN95
if "%OS%" == "Windows_NT" set VcOsDir=WINNT

rem
echo Setting environment for using Microsoft Visual C++ tools.
rem

if "%OS%" == "Windows_NT" set PATH=%MSDevDir%\BIN;%MSVCDir%\BIN;%VSCommonDir%\TOOLS\%VcOsDir%;%VSCommonDir%\TOOLS;%PATH%
if "%OS%" == "" set PATH="%MSDevDir%\BIN";"%MSVCDir%\BIN";"%VSCommonDir%\TOOLS\%VcOsDir%";"%VSCommonDir%\TOOLS";"%windir%\SYSTEM";"%PATH%"
set INCLUDE=%MSVCDir%\ATL\INCLUDE;%MSVCDir%\INCLUDE;%MSVCDir%\MFC\INCLUDE;%INCLUDE%
set LIB=%MSVCDir%\LIB;%MSVCDir%\MFC\LIB;%LIB%


:0continue

rem check to see if the location of JDK was passed in

if %1. == . goto 1continue

set JDK=%1
goto 2continue

:1continue

if not %JDK%. == . goto 2continue

echo ERROR: The JDK variable is not set.
echo   The JDK variable must be properly set before running this
echo   script.

goto end

:2continue

set OLDPATH=%PATH%
set PATH="%CPP_PATH%\bin";%PATH%

rem clean up the target directory
if exist .\Obj\*.obj del /f /q .\Obj\*.obj >nul

echo Building GateKeeper C++ files now ...

cl.exe /c /I"%CPP_PATH%\Include" /I"%JDK%\include" /I"%JDK%\include\win32" /Fo.\Obj\Gatekeeper.obj .\Cpp\Gatekeeper.cpp
cl.exe /c /I"%CPP_PATH%\Include" /I"%JDK%\include" /I"%JDK%\include\win32" /Fo.\Obj\Gatekeeper_test.obj .\Cpp\Gatekeeper_test.cpp
link.exe jvm.lib .\Obj\*.obj /nologo /subsystem:console /incremental:yes /pdb:"Debug/GateKeeper_test.pdb" /debug /machine:I386 /out:"GateKeeper_test.exe" /pdbtype:sept /libpath:"c:\j2sdk1.3.1_01\lib" 

rem delete extraneous compiler and linker output files:
if exist .\Obj\*.tds del .\Obj\*.tds
if exist Gatekeeper_test.il? del Gatekeeper_test.il?
if exist Gatekeeper_test.map del Gatekeeper_test.map
if exist Gatekeeper_test.tds del Gatekeeper_test.tds

:end

set JDK=
set CPP_PATH=
set PATH=%OLDPATH%
