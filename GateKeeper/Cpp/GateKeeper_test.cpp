#include "GateKeeper.h"
#include <ostream.h>
#include <string.h>     // ms
#include <stdlib.h>     // ms

static STRING_UTF8 JVM_DLL = "-jvmDLL";
static STRING_UTF8 INSTALLATION_IP_ADDR = "-dbAddr";
static STRING_UTF8 DATABASE_USER_ID = "-dbUser";
static STRING_UTF8 DATABASE_PASSWORD = "-dbPasswd";
static STRING_UTF8 INSTALLATION_ID = "-instID";
static STRING_UTF8 CLIENT_IP_ADDR = "-clientAddr";

static STRING_UTF8 jvmDLL_utf8 = NULL;
static STRING_UTF8 installationIPAddr_utf8 = NULL;
static STRING_UTF8 databaseUserID_utf8 = NULL;
static STRING_UTF8 databasePasswd_utf8 = NULL;
static STRING_UTF8 installationID_utf8 = NULL;
static STRING_UTF8 clientIPAddr_utf8 = NULL;


#define NUM_OF_PROPS 7

void Usage ( void )
{
  cout << "Usage:\n"
          "\tGateKeeper_test "
          "-javaDLLPath <java dll full path> "
          "-instIPAddr <installation IP Address or host name> "
          "-dbUserID <database user id>"
          "-dbPasswd <database password>"
          "-instID <installation ID>"
          "-clientIPAddr <client IP address or host name>\n";
}

void DoUTF8Test()
{

  PROPERTY_UTF8 properties_utf8[NUM_OF_PROPS];

  properties_utf8[0].key = GateKeeper::PRINT_JOB_NUMBER_UTF8;
  properties_utf8[0].value = "123";

  properties_utf8[1].key = GateKeeper::NUMBER_OF_PAGES_UTF8;
  properties_utf8[1].value = "7";

  properties_utf8[2].key = GateKeeper::SIDES_UTF8;
  properties_utf8[2].value = "s";

  properties_utf8[3].key = GateKeeper::SIZE_UTF8;
  properties_utf8[3].value = "8.5x11";

  properties_utf8[4].key = GateKeeper::COLOR_UTF8;
  properties_utf8[4].value = "m";

  properties_utf8[5].key = GateKeeper::PRINTER_UTF8;
  properties_utf8[5].value = "Printer1";

  properties_utf8[6].key = "Extra";
  properties_utf8[6].value = "extra value";

  bool rc = GateKeeper::ReleasePrintJob (
                          installationIPAddr_utf8,
                          databaseUserID_utf8,
                          databasePasswd_utf8,
                          installationID_utf8,
                          clientIPAddr_utf8,
                          properties_utf8,
                          NUM_OF_PROPS );

  cout << "ReleasePrintJob rc = ";
  cout << (rc == 0 ? "false" : "true");
  cout << "\n";
}

void DoUnicodeTest()
{
  PROPERTY_UNICODE properties_unicode[NUM_OF_PROPS];

  properties_unicode[0].key = GateKeeper::PRINT_JOB_NUMBER_UNICODE;
  properties_unicode[0].value = L"123";

  properties_unicode[1].key = GateKeeper::NUMBER_OF_PAGES_UNICODE;
  properties_unicode[1].value = L"7";

  properties_unicode[2].key = GateKeeper::SIDES_UNICODE;
  properties_unicode[2].value = L"s";

  properties_unicode[3].key = GateKeeper::SIZE_UNICODE;
  properties_unicode[3].value = L"8.5x11";

  properties_unicode[4].key = GateKeeper::COLOR_UNICODE;
  properties_unicode[4].value = L"m";

  properties_unicode[5].key = GateKeeper::PRINTER_UNICODE;
  properties_unicode[5].value = L"Printer1";

  properties_unicode[6].key = L"Extra";
  properties_unicode[6].value = L"extra value";

  int  rc = GateKeeper::ReleasePrintJob (
                          installationIPAddr_utf8,
                          databaseUserID_utf8,
                          databasePasswd_utf8,
                          installationID_utf8,
                          clientIPAddr_utf8,
                          properties_unicode,
                          NUM_OF_PROPS );

  cout << "ReleasePrintJob rc = ";
  cout << rc;
  cout << "\n";
}

void main (int argc, char** argv )
{

  if (argc < 13)
  {
    cout << "Missing arguments\n";
    Usage();
    exit(1);
  }

  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i],JVM_DLL) == 0)
    {
      i++;
      jvmDLL_utf8 = argv[i];
    }
    else
    if (strcmp(argv[i],INSTALLATION_IP_ADDR) == 0)
    {
      i++;
      installationIPAddr_utf8 = argv[i];
    }
    else
    if (strcmp(argv[i],DATABASE_USER_ID) == 0)
    {
      i++;
      databaseUserID_utf8 = argv[i];
    }
    else
    if (strcmp(argv[i],DATABASE_PASSWORD) == 0)
    {
      i++;
      databasePasswd_utf8 = argv[i];
    }
    else
    if (strcmp(argv[i],INSTALLATION_ID) == 0)
    {
      i++;
      installationID_utf8 = argv[i];
    }
    else
    if (strcmp(argv[i],CLIENT_IP_ADDR) == 0)
    {
      i++;
      clientIPAddr_utf8 = argv[i];
    }
    else
    {
      cout << "Ignoring unknown parameter: \"";
      cout << argv[i];
      cout << "\"\n";
    }
  }

  if (jvmDLL_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << JVM_DLL;
    cout << "\n";
    Usage();
    exit(1);
  }
  else
  if (installationIPAddr_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << INSTALLATION_IP_ADDR;
    cout << "\n";
    Usage();
    exit(1);
  }
  else
  if (databaseUserID_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << DATABASE_USER_ID;
    cout << "\n";
    Usage();
    exit(1);
  }
  else
  if (databasePasswd_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << DATABASE_PASSWORD;
    cout << "\n";
    Usage();
    exit(1);
  }
  else
  if (installationID_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << INSTALLATION_ID;
    cout << "\n";
    Usage();
    exit(1);
  }
  else
  if (clientIPAddr_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << CLIENT_IP_ADDR;
    cout << "\n";
    Usage();
    exit(1);
  }


  bool rc = GateKeeper::Init ( jvmDLL_utf8 );

  cout << "GateKeeper::Init(";
  cout << jvmDLL_utf8;
  cout << ") rc = ";
  cout << (rc == 0 ? "false" : "true");
  cout << "\n";

  if (rc == false)
  {
    cerr << "Failed to initialize the JVM properly.\n";
    exit(1);
  }


  DoUTF8Test();
  DoUnicodeTest();

  GateKeeper::Done();
  cout << "Done!\n";
}

