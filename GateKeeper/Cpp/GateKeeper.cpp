#include <windows.h>
#include <memory.h>     // ms
//#include <mem.h>        // borland
#include <ostream.h>
#include <jni.h>
#include "GateKeeper.h"


#define NUMBER_OF_JVM_OPTIONS 3


JavaVM*           jvm = NULL;
JNIEnv*           env = NULL;

jclass            gateKeeper_class;
jmethodID         releasePrintJob_mid;

jclass            properties_class;
jmethodID         properties_mid;
jmethodID         setProperty_mid;

STRING_UTF8 GateKeeper::PRINT_JOB_NUMBER_UTF8 = "PRINT_JOB_NUMBER";
STRING_UTF8 GateKeeper::NUMBER_OF_PAGES_UTF8  = "NUMBER_OF_PAGES";
STRING_UTF8 GateKeeper::SIDES_UTF8            = "SIDES";
STRING_UTF8 GateKeeper::SIZE_UTF8             = "SIZE";
STRING_UTF8 GateKeeper::COLOR_UTF8            = "COLOR";
STRING_UTF8 GateKeeper::PRINTER_UTF8          = "PRINTER";

STRING_UNICODE GateKeeper::PRINT_JOB_NUMBER_UNICODE = L"PRINT_JOB_NUMBER";
STRING_UNICODE GateKeeper::NUMBER_OF_PAGES_UNICODE  = L"NUMBER_OF_PAGES";
STRING_UNICODE GateKeeper::SIDES_UNICODE            = L"SIDES";
STRING_UNICODE GateKeeper::SIZE_UNICODE             = L"SIZE";
STRING_UNICODE GateKeeper::COLOR_UNICODE            = L"COLOR";
STRING_UNICODE GateKeeper::PRINTER_UNICODE          = L"PRINTER";



bool GateKeeper::Init ( STRING_UTF8 javaDLL )
                          // javaDLLPath = full path to the JVM DLL file
{

  if (jvm == NULL)
  {

    HANDLE jvmDLLHandle = LoadLibrary(javaDLL);

    if (jvmDLLHandle == NULL)
    {
      cout << "Unable to Load Library \"";
      cout << javaDLL;
      cout << "\"\n";
      return false;
    }

    JavaVMInitArgs jvmArgs;
    memset(&jvmArgs, 0, sizeof(jvmArgs));
    jvmArgs.version = JNI_VERSION_1_2;
    jvmArgs.ignoreUnrecognized = TRUE;

    JavaVMOption jvmOptions[NUMBER_OF_JVM_OPTIONS];
    jvmOptions[0].optionString = "-Djdbc.drivers=org.postgresql.Driver";
    jvmOptions[1].optionString = "-Djava.security.policy=GateKeeper.policy";
    jvmOptions[2].optionString = "-Djava.class.path=.\\GateKeeper.jar;.\\postgresql.jar;";

    jvmArgs.nOptions = NUMBER_OF_JVM_OPTIONS;
    jvmArgs.options = jvmOptions;

    long rc = JNI_CreateJavaVM(&jvm, (void**) &env, &jvmArgs);
    if (rc == JNI_ERR)
    {
      jvm = NULL;
      cerr << "Error creating JVM.\n";
      return false;
    }

    gateKeeper_class = env->FindClass("GateKeeper");

    if (gateKeeper_class == 0)
    {
      jvm = NULL;
      cerr << "Can't find class GateKeeper.\n";
      return false;
    }

    releasePrintJob_mid =
      env->GetStaticMethodID (
             gateKeeper_class,
             "ReleasePrintJob",
             "(Ljava/lang/String;"      // String dbAddress
             "Ljava/lang/String;"       // String dbUserID
             "Ljava/lang/String;"       // String dbPasswd
             "Ljava/lang/String;"       // String installationID
             "Ljava/lang/String;"       // String clientAddress
             "Ljava/util/Properties;)"  // Properties properties
             "Z" );                     // return value: jboolean

    if (releasePrintJob_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method GateKeeper.ReleasePrintJob().\n";
      return false;
    }

    properties_class = env->FindClass("java/util/Properties");

    if (properties_class == 0)
    {
      jvm = NULL;
      cerr << "Can't find class java/util/Properties.\n";
      return false;
    }

    properties_mid =
      env->GetMethodID (
             properties_class,
             "<init>",
             "()V" );     // no parameters, return value: void

    if (properties_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method Properties.<init>.\n";
      return false;
    }

    setProperty_mid =
      env->GetMethodID (
             properties_class,
             "setProperty",
             "(Ljava/lang/String;"    // String key
             "Ljava/lang/String;)"    // String value
             "Ljava/lang/Object;" );  // return value: Object

    if (setProperty_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method Properties.setProperty().\n";
      return false;
    }

  }

  return true;
}


int  ReleasePrintJob_j (
       jstring dbAddress_j,
       jstring dbUserID_j,
       jstring dbPasswd_j,
       jstring installationID_j,
       jstring clientAddress_j,
       jobject properties_j )
{

  if (jvm == NULL)
  {
    return false;
  }

  // call the Java operation "ReleasePrintJob" and capture the result code
  jboolean rc_j = env->CallStaticBooleanMethod (
                         gateKeeper_class,
                         releasePrintJob_mid,
                         dbAddress_j,
                         dbUserID_j,
                         dbPasswd_j,
                         installationID_j,
                         clientAddress_j,
                         properties_j );

  // have the compiler convert the java boolean to a c boolean
  bool rc = (rc_j != 0);

  // return the result code to the caller
  return rc;
}


int  GateKeeper::ReleasePrintJob (
                   STRING_UTF8   dbAddress_utf8,
                   STRING_UTF8   dbUserID_utf8,
                   STRING_UTF8   dbPasswd_utf8,
                   STRING_UTF8   installationID_utf8,
                   STRING_UTF8   clientAddress_utf8,
                   PROPERTY_UTF8 properties_utf8[],
                   int           numberOfProperties )
{

  if (jvm == NULL)
  {
    return false;
  }

  // convert C Strings into Java Strings
  jstring dbAddress_j = env->NewStringUTF (dbAddress_utf8);
  jstring dbUserID_j = env->NewStringUTF (dbUserID_utf8);
  jstring dbPasswd_j = env->NewStringUTF (dbPasswd_utf8);
  jstring installationID_j = env->NewStringUTF (installationID_utf8);
  jstring clientAddress_j = env->NewStringUTF (clientAddress_utf8);

  // convert the C Properties array into a Java Properties object
  jobject properties_j = env->NewObject (
                                properties_class,
                                properties_mid );
  for (int i = 0; i < numberOfProperties; i++)
  {
    jstring key_j = env->NewStringUTF (properties_utf8[i].key);
    jstring value_j = env->NewStringUTF (properties_utf8[i].value);
    env->CallObjectMethod (
           properties_j,
           setProperty_mid,
           key_j,
           value_j );
  }

  return ReleasePrintJob_j (
           dbAddress_j,
           dbUserID_j,
           dbPasswd_j,
           installationID_j,
           clientAddress_j,
           properties_j );

}

int  GateKeeper::ReleasePrintJob (
                   STRING_UTF8      dbAddress_utf8,
                   STRING_UTF8      dbUserID_utf8,
                   STRING_UTF8      dbPasswd_utf8,
                   STRING_UTF8      installationID_utf8,
                   STRING_UTF8      clientAddress_utf8,
                   PROPERTY_UNICODE properties_unicode[],
                   int              numberOfProperties )
{

  if (jvm == NULL)
  {
    return false;
  }

  // convert C Strings into Java Strings
  jstring dbAddress_j = env->NewStringUTF (dbAddress_utf8);
  jstring dbUserID_j = env->NewStringUTF (dbUserID_utf8);
  jstring dbPasswd_j = env->NewStringUTF (dbPasswd_utf8);
  jstring installationID_j = env->NewStringUTF (installationID_utf8);
  jstring clientAddress_j = env->NewStringUTF (clientAddress_utf8);

  // convert the C Properties array into a Java Properties object
  jobject properties_j = env->NewObject (
                                properties_class,
                                properties_mid );
  for (int i = 0; i < numberOfProperties; i++)
  {
    jstring key_j = env->NewString(properties_unicode[i].key, wcslen(properties_unicode[i].key));
    jstring value_j = env->NewString(properties_unicode[i].value, wcslen(properties_unicode[i].value));
    env->CallObjectMethod (
           properties_j,
           setProperty_mid,
           key_j,
           value_j );
  }

  return ReleasePrintJob_j (
           dbAddress_j,
           dbUserID_j,
           dbPasswd_j,
           installationID_j,
           clientAddress_j,
           properties_j );

}

int  GateKeeper::ReleasePrintJob (
                   STRING_UNICODE   dbAddress_unicode,
                   STRING_UNICODE   dbUserID_unicode,
                   STRING_UNICODE   dbPasswd_unicode,
                   STRING_UNICODE   installationID_unicode,
                   STRING_UNICODE   clientAddress_unicode,
                   PROPERTY_UNICODE properties_unicode[],
                   int              numberOfProperties )
{

  if (jvm == NULL)
  {
    return false;
  }

  // convert C Strings into Java Strings
  jstring dbAddress_j = env->NewString (dbAddress_unicode, wcslen(dbAddress_unicode));
  jstring dbUserID_j = env->NewString (dbUserID_unicode, wcslen(dbUserID_unicode));
  jstring dbPasswd_j = env->NewString (dbPasswd_unicode, wcslen(dbPasswd_unicode));
  jstring installationID_j = env->NewString (installationID_unicode, wcslen(installationID_unicode));
  jstring clientAddress_j = env->NewString (clientAddress_unicode, wcslen(clientAddress_unicode));

  // convert the C Properties array into a Java Properties object
  jobject properties_j = env->NewObject (
                                properties_class,
                                properties_mid );
  for (int i = 0; i < numberOfProperties; i++)
  {
    jstring key_j = env->NewString(properties_unicode[i].key, wcslen(properties_unicode[i].key));
    jstring value_j = env->NewString(properties_unicode[i].value, wcslen(properties_unicode[i].value));
    env->CallObjectMethod (
           properties_j,
           setProperty_mid,
           key_j,
           value_j );
  }

  return ReleasePrintJob_j (
           dbAddress_j,
           dbUserID_j,
           dbPasswd_j,
           installationID_j,
           clientAddress_j,
           properties_j );

}


void GateKeeper::Done ( void )
{
  if (jvm != NULL)
  {
    jvm->DestroyJavaVM();
    jvm = NULL;
  }
}
