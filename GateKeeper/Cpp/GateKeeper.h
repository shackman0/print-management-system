#include <stdlib.h>


/*
to compile this program:

  set JDK=<path to java development kit v1.3>
  cl -I%JDK%\include -I%JDK%\include\win32 GateKeeper.cpp %JDK%\lib\jvm.lib

to use this class:

  1. call Init() to initialize the JVM environment
  2. call ReleasePrintJob() 1+ times
  3. call Done() when you are done with calling ReleasePrintJob()
     (probably at program shutdown)

You MUST call Init() before calling ReleasePrintJob() to init the JVM.
You MUST call Done() before you end your program to quiesce the Java Virtual Machine.

*/

typedef char*  STRING_UTF8;
typedef struct PROPERTY_UTF8 { STRING_UTF8 key; STRING_UTF8 value; } PROPERTY_UTF8;

typedef wchar_t*  STRING_UNICODE;
typedef struct    PROPERTY_UNICODE { STRING_UNICODE key; STRING_UNICODE value; } PROPERTY_UNICODE;

class GateKeeper
{

public:

  /*
  a print job properties list must contain at least these properties:
  */
  static STRING_UTF8 PRINT_JOB_NUMBER_UTF8;
  static STRING_UTF8 NUMBER_OF_PAGES_UTF8;
  static STRING_UTF8 SIDES_UTF8;
  static STRING_UTF8 SIZE_UTF8;
  static STRING_UTF8 COLOR_UTF8;
  static STRING_UTF8 PRINTER_UTF8;

  static STRING_UNICODE PRINT_JOB_NUMBER_UNICODE;
  static STRING_UNICODE NUMBER_OF_PAGES_UNICODE;
  static STRING_UNICODE SIDES_UNICODE;
  static STRING_UNICODE SIZE_UNICODE;
  static STRING_UNICODE COLOR_UNICODE;
  static STRING_UNICODE PRINTER_UNICODE;


  /*
  call Init() first to initialize the JVM environment.
  pass in the name of the java dll to be used (jdk1.3's dll is called "jvm.dll")
  */
  static bool Init ( STRING_UTF8 javaDLL );

  /*
  call ReleasePrintJob every time you want to see if it's ok to release
  a print job to the printer.
  */
  static int  ReleasePrintJob (
                STRING_UTF8   dbAddress,
                STRING_UTF8   dbUserID,
                STRING_UTF8   dbPasswd,
                STRING_UTF8   installationID,
                STRING_UTF8   clientAddress,
                PROPERTY_UTF8 properties[],
                int           numberOfProperties );

  static int  ReleasePrintJob (
                STRING_UTF8      dbAddress,
                STRING_UTF8      dbUserID,
                STRING_UTF8      dbPasswd,
                STRING_UTF8      installationID,
                STRING_UTF8      clientAddress,
                PROPERTY_UNICODE properties[],
                int              numberOfProperties );

  static int  ReleasePrintJob (
                STRING_UNICODE   dbAddress,
                STRING_UNICODE   dbUserID,
                STRING_UNICODE   dbPasswd,
                STRING_UNICODE   installationID,
                STRING_UNICODE   clientAddress,
                PROPERTY_UNICODE properties[],
                int              numberOfProperties );

  /*
  if you call Init() you must call Done() when you are
  ready to shut down your program to quiesce the Java Virtual Machine.
  */
  static void Done ( void );

};

