package shackelford.floyd.printmanagementsystem.gatekeeper.netware;

import java.io.FileInputStream;
import java.io.IOException;


/**
 * <p>Title: NWPrintJob</p>
 * <p>Description:
  This class represents a PrintJob in NetWare.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
class NWPrintJob
{
  /**
  	This is the size of a chunk to read out of the file.
  */
  private final static int FILE_CHUNK_SIZE = 8192;

  /**
  	This is the NetWare job number
  */
  private final int        jobNumber;

  /**
  	This is the NetWare file name (with path)
  */
  private final String     fileName;

  /**
  	This is the file size in bytes
  */
  private final int        fileSize;

  /**
  	This is the IP address of the station that submitted
  	this print job.
  */
  private final String     ipAddress;

  /**
  	Construct a new PrintJob with all the relevent information.
  */
  public NWPrintJob(int jobNum, String printJobFileName, int printJobFileSize, String printJobIPAddr)
  {
    this.jobNumber = jobNum;
    this.fileName = printJobFileName;
    this.fileSize = printJobFileSize;
    this.ipAddress = printJobIPAddr;
  }

  /**
  	Returns this print jobs NetWare job number
  */
  int GetJobNumber()
  {
    return jobNumber;
  }

  /**
  	Returns the full pathname of the queue file
  */
  String GetFileName()
  {
    return fileName;
  }

  /**
  	Returns the IP address of the submitter of the job,
  	or "unavailable" if the submitter is unknown.
  */
  String GetSubmitterAddress()
  {
    return ipAddress;
  }

  /**
  	Returns the size of the print job in bytes.
  */
  int GetFileSize()
  {
    return fileSize;
  }

  /**
  	Returns an array of bytes representing the job data.
  */
  byte[] GetJobData()
    throws Exception
  {
    // Create a buffer for the job data
    byte[] jobBytes = new byte[fileSize];
    try
    (FileInputStream infile = new FileInputStream(fileName);)
    {
      int offset = 0;
  
      // Read the data into the file.
      // NOTE: There is a call on FileInputStream that requires
      // only a byte[].  That method fails under NetWare 5.0
      // java based on JDK 1.1.5 with a page fault.  Therefore,
      // we do it piece by piece which seems to work.
      try
      {
        for (offset = 0; offset < fileSize; offset += FILE_CHUNK_SIZE)
        {
          infile.read(jobBytes, offset, FILE_CHUNK_SIZE);
        }
      }
      catch (IOException error)
      {
        throw new Exception("Couldn't read " + fileSize + " bytes from " + fileName + ".  Error was " + error);
      }
    }
    
    // Return the data
    return jobBytes;
  }
}
