package shackelford.floyd.printmanagementsystem.gatekeeper.netware;



/**
 * <p>Title: NWPrintQueue</p>
 * <p>Description:
  This class is a wrapper around the old-style Queue functions for NetWare.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
class NWPrintQueue
{
	/**
		This is the name of the queue.  Since queue ID numbers
		are volatile, we always go and get the queue ID using
		the name before every queue operation.
	*/
	private String name;

	/**
		This is the glue instance that allows us to call into
		NetWare and get the various information.
	*/
	private NDKGlue glue;

	/**
		The constructor takes the name of the existing NetWare
		print queue.  If the queue doesn't exist, an exception
		is thrown.
		@param queueName The name of the NetWare queue
		@param glue An NDKGlue instance
	*/
	public NWPrintQueue(String queueName, NDKGlue queueGlue)
			throws Exception
	{
		this.name = queueName;
		this.glue = queueGlue;
	}


	/**
		Returns true if a job is pending on this queue, false
		otherwise.
	*/
	public boolean IsJobPending()
	{
		return (glue.GetNextJob(name) != null);
	}

	/**
		Returns the next PrintJob in the queue (in order that
		it is pending), or NULL if there is no job pending on
		the queue.
	*/
	public NWPrintJob GetNextJob()
	{
		NWPrintJob returnValue = glue.GetNextJob(name);

		return returnValue;
	}

	/**
		Removes the given job from the queue.  Throws an
		exception of the job does not exist on the queue.

		@param job The PrintJob to remove from the queue.
	*/
	public void RemoveJob(NWPrintJob job)	throws Exception
	{
		int returnCode =
			glue.RemoveJobFromQueue(name,
										job.GetJobNumber());

		if (returnCode != 0)
		{
			throw new Exception("Error #" + returnCode +
				" occurred removing job " +
				job.GetJobNumber()  +
				" from queue '" +
				name + "'");
		}
	}

	/**
		Move the given job from this queue to the one provided.
		Throws an exception if the job does not exist, or if
		there is any sort of error performing the move.

		@param job The PrintJob to move
		@param targetQ The PrintQueue into which to move the job.
	*/
	public void MoveJobToNewQueue(NWPrintJob job, NWPrintQueue targetQ)
			throws Exception
	{
		int returnCode =
				glue.SwitchQueuesForJob(job.GetJobNumber(),
						name,	targetQ.name);

		if (returnCode != 0)
		{
			throw new Exception("Error #" + returnCode +
				" occurred moving job " +
				job.GetJobNumber()  +
				" from queue '" +
				name + "' to queue '" +
				targetQ.name + "'");
		}
	}
}
