package shackelford.floyd.printmanagementsystem.gatekeeper.netware;

import shackelford.floyd.printmanagementsystem.common.Preferences;
import shackelford.floyd.printmanagementsystem.printjobparser.PrintJob;


/**
 * <p>Title: NWGateKeeper</p>
 * <p>Description:
  This is the main class for the GateKeeper under NetWare.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
class NWGateKeeper
  implements Runnable
{
  /**
    Class that allows calling into NLM functionality
  */
  private NDKGlue             glue;

  /**
    Queue from which jobs are taken and checked.
  */
  private NWPrintQueue        inputQ;

  /**
    Queue to which jobs are moved if they are approved
  */
  private NWPrintQueue        outputQ;

  /**
    The Preferences read from the configuration file.
  */
  private Preferences         prefs;

  /**
    Time to sleep between checks of the input queue
  */
  private final static int    DELAY_IN_MILLISECONDS = 1000;

  /**
    Name of the preferences file
  */
  private final static String PREFS_FILE_NAME       = "gkprefs.txt";

  //  private final static String DATABASE_ADDRESS = "DATABASE_ADDRESS";
  //  private final static String DATABASE_USER_ID = "DATABASE_USER_ID";
  //  private final static String DATABASE_PASSWORD = "DATABASE_PASSWORD";
  //  private final static String INSTALLATION_ID = "INSTALLATION_ID";

  /**
    Constructor.  Takes the name of the input and
    output queues.
  */
  public NWGateKeeper(String inputQName, String outputQName)
  {

    // Read the preferences from the configuration file.
    try
    {
      prefs = new Preferences(PREFS_FILE_NAME);
    }
    catch (Exception error)
    {
      System.err.println("Couldn't read preferences: " + error);
      System.exit(404);
    }

    // Create the glue
    try
    {
      glue = new NDKGlue(prefs);
    }
    catch (Exception error)
    {
      System.err.println("Couldn't create system interface.");
      System.err.println("Reason is: " + error);
      System.exit(401);
    }

    // Create the input queue
    try
    {
      inputQ = new NWPrintQueue(inputQName, glue);
    }
    catch (Exception error)
    {
      System.err.println("Couldn't find queue named '" + inputQName + "'");
      System.exit(402);
    }

    // Create the output queue
    try
    {
      outputQ = new NWPrintQueue(outputQName, glue);
    }
    catch (Exception error)
    {
      System.err.println("Couldn't find queue named '" + outputQName + "'");
      System.exit(403);
    }

  }

  /**
  This method checks to see if we are allowed
  to print.  It returns true if we can print,
  false otherwise.
  */
  protected boolean IsAllowedToPrint(NWPrintJob job)
  {
    // parse the print job
    PrintJob printJob = new PrintJob(job.GetFileName(), String.valueOf(job.GetJobNumber()));
    if (printJob.Parse() == false)
    {
      System.err.println("cannot parse print job #" + printJob.GetPrintJobNumber() + " at " + printJob.GetFileName());
      return false;
    }

    // todo1 try to release the print job for printing
    //    if (GateKeeper.ReleasePrintJob (
    //          prefs.getProperty(DATABASE_ADDRESS),
    //          prefs.getProperty(DATABASE_USER_ID),
    //          prefs.getProperty(DATABASE_PASSWORD),
    //          prefs.getProperty(INSTALLATION_ID),
    //          job.GetSubmitterAddress(),
    //          printJob.GetProperties() ) == false)
    //    {
    //      System.err.println("cannot release print job #" + printJob.GetPrintJobNumber() + " at " + printJob.GetFileName());
    //      return false;
    //    }

    return true;
  }

  /**
    This is the main loop.  Once in here, the program
    runs until interrupted or unloaded from an outside
    source.  It will also stop if a job gets stuck in
    the input queue such that it cannot be processed.
  */
  @Override
  public void run()
  {
    boolean done = false;
    NWPrintJob job = null;

    while (!done)
    {
      /* If a job is pending */
      if (inputQ.IsJobPending())
      {
        /* Get it */
        job = inputQ.GetNextJob();

        /* If it should be printed */
        if (IsAllowedToPrint(job))
        {
          /* Move it to the output queue */
          try
          {
            inputQ.MoveJobToNewQueue(job, outputQ);
            System.err.println("Job " + job.GetJobNumber() + " approved.");
          }
          catch (Exception error)
          {
            System.err.println("Couldn't move job " + job.GetJobNumber() + " to output queue.");
            System.err.println("Exception: " + error);
            done = true;
          }
        }
        else
        {
          /* Job is denied.  Remove it. */
          System.err.println("Job " + job.GetJobNumber() + " is NOT approved. Removing ...");
          try
          {
            inputQ.RemoveJob(job);
          }
          catch (Exception error)
          {
            System.err.println("Couldn't remove job " + job.GetJobNumber() + " from input queue.");
            System.err.println("Exception: " + error);
            done = true;
          }
        }

        // Clean up system resources
        job = null;
        System.gc();
      }
      else
      {
        // No job pending.  Sleep a spell.
        try
        {
          Thread.sleep(DELAY_IN_MILLISECONDS);
        }
        catch (InterruptedException interrupt)
        {
          done = true;
        }
      }
    }
  }

  /**
    Usage: java NWGateKeeper <inputQueueName> <outputQueueName>
  */
  public static void main(String args[])
  {
    NWGateKeeper gateKeeper = null;

    // Check arguments
    if (args.length != 2)
    {
      System.err.println("Usage: " + "java NWGateKeeper " + "<inputQueueName> <outputQueueName>");
      System.err.println("Example: java NWGateKeeper input.sys output.sys");
      System.exit(1);
    }

    // Create new GateKeeper object
    gateKeeper = new NWGateKeeper(args[0], args[1]);

    // Start it.
    gateKeeper.run();
  }
}
