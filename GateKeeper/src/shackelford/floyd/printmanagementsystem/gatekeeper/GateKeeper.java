package shackelford.floyd.printmanagementsystem.gatekeeper;

import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.Properties;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.guiserver.GUIServer;
import shackelford.floyd.printmanagementsystem.guiserver.PrintJobInformationEntryDialog;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PageChargesTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobOnHoldHeaderTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: GateKeeper</p>
 * <p>Description:
To use this class, simply call the static method ReleasePrintJob.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GateKeeper
{

  private boolean              _didDone          = false;

  private GUIServer            _guiServer        = null;

  @SuppressWarnings("unused")
  private String               _clientAddress    = null;

  private static DecimalFormat __formatter       = new DecimalFormat("#,##0.00");

  public static final int      RELEASE_PRINT_JOB = 0;

  public static final int      HOLD_PRINT_JOB    = 1;

  public static final int      CANCEL_PRINT_JOB  = 2;

  /**
    Use this constructor to create your interface instance to the GUI Server and
    the Installation Database.

    @param dbAddr the ip address or hostname of the installation database server
    @param dbUserID the user id to be used to log in to the installation database
    @param dbPasswd the password to use to log in to the installation database
    @param installationID the id of the installation which owns this gatekeeper
    @param clientAddress the ip address or hostname of the print client computer
  */
  public GateKeeper(String dbAddr, String dbUserID, String dbPasswd, Integer installationID, String clientAddress)
    throws Exception
  {

    GlobalAttributes.__verbose = false;
    GlobalAttributes.__dbAddr = dbAddr;
    GlobalAttributes.__dbUserID = dbUserID;
    GlobalAttributes.__dbPasswd = dbPasswd;

    _clientAddress = clientAddress.trim();

    System.setSecurityManager(new RMISecurityManager());

    try
    {
      String guiServerURL = "rmi://" + clientAddress.trim() + "/GUIServer";
      _guiServer = ((GUIServer) Naming.lookup(guiServerURL)).NewGUIServer();
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
      throw new Exception(" " + excp);
    }

    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(GlobalAttributes.__dbAddr, GlobalAttributes.__dbUserID, GlobalAttributes.__dbPasswd, installationID));
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      throw excp;
    }

    _didDone = false;

    //CheckIfThisIPAddrIsRegistered();
    CheckIfInstallationIsEnabled();
  }

  public int ReleasePrintJob(Properties printJobProperties)
  {
    PageChargesTable pageChargesTable = InstallationDatabase.GetDefaultInstallationDatabase().GetPageChargesTable();
    RulesTable rulesTable = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable();
    PrintJobHeaderTable printJobHeaderTable = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable();
    PrintJobOnHoldHeaderTable printJobOnHoldHeaderTable = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobOnHoldHeaderTable();

    int rc = CANCEL_PRINT_JOB;

    Double printJobCost = new Double(0d);

    RulesRow chargeForPrinting = rulesTable.GetRowForRuleName(RulesRow.CHARGE_FOR_PRINTING);
    if (chargeForPrinting.RuleIsYes() == true)
    {
      try
      {
        printJobCost = pageChargesTable.CostThePrintJob(printJobProperties);
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
        Done();
        return CANCEL_PRINT_JOB;
      }
    }
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_COST, __formatter.format(printJobCost));

    String printJobNumber = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PRINT_JOB_NUMBER);
    String numberOfPages = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES);
    //    String sides = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_SIDES);
    //    String size = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE);
    //    String color = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_COLOR);
    String printer = printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PRINTER);

    Properties dialogProperties = null;

    RulesRow anonymousPrinting = rulesTable.GetRowForRuleName(RulesRow.ANONYMOUS_PRINTING);
    if (anonymousPrinting.RuleIsYes() == true)
    {
      printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_USER_ACCOUNT_ID, anonymousPrinting.GetRuleParm(0));
      printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_DESCRIPTION, "anonymous");
      printJobHeaderTable.StoreThePrintJob(printJobProperties);
      DisplayPrintJobReleasedMsgBox(printJobNumber, printer, InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), "0.00", __formatter.format(printJobCost));
      rc = RELEASE_PRINT_JOB;
    }
    else
    {
      try
      {
        dialogProperties = DisplayPrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, __formatter.format(printJobCost));
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
        Done();
        return CANCEL_PRINT_JOB;
      }

      if (dialogProperties.getProperty(AbstractDialog.CANCEL).equals("true") == true)
      {
        rc = CANCEL_PRINT_JOB;
      }
      else
      {
        printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_DESCRIPTION, dialogProperties.getProperty(PrintJobInformationEntryDialog.DESCRIPTION));
        String userAccountID = dialogProperties.getProperty(PrintJobInformationEntryDialog.USER_ACCOUNT_ID);
        printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_USER_ACCOUNT_ID, userAccountID);

        if ((dialogProperties.getProperty(PrintJobInformationEntryDialog.HOLD).equals("true") == true)
          || (dialogProperties.getProperty(PrintJobInformationEntryDialog.GUEST_HOLD).equals("true") == true))
        {
          try
          {
            printJobOnHoldHeaderTable.StoreThePrintJob(printJobProperties);
            DisplayPrintJobHeldMsgBox(printJobNumber, printer);
          }
          catch (Exception excp)
          {
            excp.printStackTrace();
            Done();
            return CANCEL_PRINT_JOB;
          }
          rc = HOLD_PRINT_JOB;
        }
        else
          if (dialogProperties.getProperty(AbstractDialog.OK).equals("true") == true)
          {
            UserAccountRow userAccountRow = UserAccountTable.GetCachedUserAccountRow(userAccountID);
            if (printJobCost.doubleValue() <= userAccountRow.Get_pre_payment_balance().doubleValue())
            {
              userAccountRow.DeductFromPrePaymentBalance(printJobCost.doubleValue());
              userAccountRow.Update();
              printJobHeaderTable.StoreThePrintJob(printJobProperties);
              try
              {
                DisplayPrintJobReleasedMsgBox(printJobNumber, printer, InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), userAccountRow.GetFormatted_pre_payment_balance(),
                  __formatter.format(printJobCost));
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
              }
              rc = RELEASE_PRINT_JOB;
            }
            else
            {
              try
              {
                DisplayInsufficientPrePaymentBalanceMsgBox(printJobNumber, printJobCost.toString(), userAccountRow.Get_pre_payment_balance().toString(), printer);
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
                Done();
                return CANCEL_PRINT_JOB;
              }
              rc = CANCEL_PRINT_JOB;
            }
          }
          else
            if (dialogProperties.getProperty(PrintJobInformationEntryDialog.GUEST).equals("true") == true)
            {
              try
              {
                dialogProperties = DisplayGuestPrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, printJobCost.toString());
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
                Done();
                return CANCEL_PRINT_JOB;
              }
              if (dialogProperties.getProperty(AbstractDialog.CANCEL).equals("true") == true)
              {
                rc = CANCEL_PRINT_JOB;
              }
              else
                if (dialogProperties.getProperty(AbstractDialog.OK).equals("true") == true)
                {
                  //            String nameOnCard = dialogProperties.getProperty(GuestPrintJobInformationEntryDialog.NAME_ON_CARD);
                  //            String billingAddress = dialogProperties.getProperty(GuestPrintJobInformationEntryDialog.BILLING_ADDRESS);
                  //            String creditCardNumber = dialogProperties.getProperty(GuestPrintJobInformationEntryDialog.CREDIT_CARD_NUMBER);
                  //            String expirationDate = dialogProperties.getProperty(GuestPrintJobInformationEntryDialog.EXPIRATION_DATE);

                  RulesRow ruleRow = rulesTable.GetRowForRuleName(RulesRow.ALLOW_GUEST_PRINT);
                  printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_USER_ACCOUNT_ID, ruleRow.GetRuleParm(0));

                  // do an e-commerce transaction here
                  printJobHeaderTable.StoreThePrintJob(printJobProperties);
                  rc = RELEASE_PRINT_JOB;
                }
            }
      }
    }

    Done();

    return rc;
  }

  /**
    the finalize method is here just in case the creator forgot to call
    the done() method
  */

  @Override
  protected void finalize()
  {
    try
    {
      Done();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation notifies the servers that you no longer need their resources.
  */
  private void Done()
  {
    if (_didDone == false)
    {
      _guiServer = null;
      try
      {
        InstallationDatabase.GetDefaultInstallationDatabase().Done();
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
      }
      _didDone = true;
    }
  }

  /* the following interfaces target the GUI Server */

  /**
  This operation displays the PrintJobInformationEntryDialog. It returns a
  properties object containing the state of the dialog fields at exit. the following
  fields are returned:
    USER_ACCOUNT_NUMBER
    USER_ACCOUNT_PASSPHRASE
    HOLD
    OK
    CANCEL
  Installation specific fields may also be included.
  This is a blocking call.
  */
  private Properties DisplayPrintJobInformationEntryDialog(String printJobNumber, String numberOfPages, String printer, String printJobCost)
    throws Exception
  {
    try
    {
      return _guiServer.DisplayPrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), printJobCost);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
      throw new Exception(" " + excp);
    }
  }

  /**
  This operation displays the PrintJobInformationEntryDialog. It returns a
  properties object containing the state of the dialog fields at exit. the following
  fields are returned:
    NAME_ON_CARD
    BILLING_ADDRESS
    CREDIT_CARD_NUMBER
    EXPIRATION_MONTH
    EXPIRATION_YEAR
    OK
    CANCEL
  Installation specific fields may also be included.
  This is a blocking call.
  */
  private Properties DisplayGuestPrintJobInformationEntryDialog(String printJobNumber, String numberOfPages, String printer, String printJobCost)
    throws Exception
  {
    try
    {
      return _guiServer.DisplayGuestPrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), printJobCost);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
      throw new Exception(" " + excp);
    }
  }

  /**
  This operation displays the PrintJobReleasedMsgBox.
  This is a non-blocking call.
  */
  private void DisplayPrintJobReleasedMsgBox(String printJobNumber, String printer, String currencySymbol, String newBalance, String jobCost)
  {
    try
    {
      _guiServer.DisplayPrintJobReleasedMsgBox(printJobNumber, printer, currencySymbol, newBalance, jobCost);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation displays the PrintJobHeldMsgBox.
  This is a non-blocking call.
  */
  private void DisplayPrintJobHeldMsgBox(String printJobNumber, String printer)
  {
    try
    {
      _guiServer.DisplayPrintJobHeldMsgBox(printJobNumber, printer);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation displays the DisplayPrintJobBeingProcessedMsgBox.
  This is a non-blocking call.
  */
  public void DisplayPrintJobBeingProcessedMsgBox(String printJobNumber, String printer)
  {
    try
    {
      _guiServer.DisplayPrintJobBeingProcessedMsgBox(printJobNumber, printer);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation displays the DismissPrintJobBeingProcessedMsgBox.
  This is a non-blocking call.
  */
  public void DismissPrintJobBeingProcessedMsgBox(String printJobNumber, String printer)
  {
    try
    {
      _guiServer.DismissPrintJobBeingProcessedMsgBox(printJobNumber, printer);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation displays the InsufficientPrePaymentBalanceMsgBox.
  This is a non-blocking call.
  */
  private void DisplayInsufficientPrePaymentBalanceMsgBox(String printJobNumber, String printJobCost, String prePaymentBalance, String printer)
  {
    try
    {
      _guiServer.DisplayInsufficientPrePaymentBalanceMsgBox(printJobNumber, InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), printJobCost, prePaymentBalance, printer);
    }
    catch (RemoteException excp)
    {
      excp.printStackTrace();
    }
  }

  /* the following interfaces target the Installation Database */

  void CheckIfThisIPAddrIsRegistered()
    throws Exception
  {
    String localHostIPAddr = InetAddress.getLocalHost().getHostAddress().trim();
    GatekeeperRow gatekeeperRow = InstallationDatabase.GetDefaultInstallationDatabase().GetGatekeeperTable().GetRowForIPAddress(localHostIPAddr);
    if (gatekeeperRow == null)
    {
      throw new Exception(localHostIPAddr + " is not a Gatekeeper address registered for this installation.");
    }
  }

  /**
  This operation returns true if the installation is in the database and enabled.
  it returns false otherwise.
  */
  void CheckIfInstallationIsEnabled()
    throws Exception
  {
    if (InstallationTable.GetCachedInstallationRow().StatusIsEnabled() == false)
    {
      throw new Exception(InstallationTable.GetCachedInstallationRow().Get_installation_id() + " is not enabled");
    }
  }

  /**
  This is the unit test code. It tests each of the interfaces above.
  You'll need to set the test variables before you run this.
  */
  private static final Integer PRINT_JOB_NUMBER = new Integer(12345);

  //  private static final String  USER_ACCOUNT_NUMBER = "100201";

  public static void main(String[] args)
  {
    GlobalAttributes.__applicationName = GateKeeper.class.getName();
    GlobalAttributes.__mainClass = GateKeeper.class;

    String dbAddress = null;
    Integer installationID = null;
    String dbUserID = null;
    String dbPassword = null;
    String clientAddress = null;
    String testsToRun = "b"; // b = both, g = gui, d = database

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-dbAddr") == true)
      {
        dbAddress = args[++index].trim();
        System.out.println("Database Address = \"" + dbAddress + "\"");
      }
      else
        if (args[index].equalsIgnoreCase("-runTest") == true)
        {
          testsToRun = args[++index];
          testsToRun = testsToRun.trim();
          System.out.println("Running \"" + testsToRun + "\" tests.");
        }
        else
          if (args[index].equalsIgnoreCase("-instID") == true)
          {
            installationID = new Integer(args[++index]);
            System.out.println("Installation ID = \"" + installationID.toString() + "\"");
          }
          else
            if (args[index].equalsIgnoreCase("-dbUser") == true)
            {
              dbUserID = args[++index];
              dbUserID = dbUserID.trim();
              System.out.println("Database User ID = \"" + dbUserID + "\"");
            }
            else
              if (args[index].equalsIgnoreCase("-dbPasswd") == true)
              {
                dbPassword = args[++index];
                dbPassword = dbPassword.trim();
                System.out.println("Database Password found");
              }
              else
                if (args[index].equalsIgnoreCase("-clientAddr") == true)
                {
                  clientAddress = args[++index];
                  clientAddress = clientAddress.trim();
                  System.out.println("Client Address = \"" + clientAddress + "\"");
                }
                else
                {
                  System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
                }
    }

    if (installationID == null)
    {
      System.out.println("ERROR: -instID was not specified");
      Usage();
      System.exit(1);
    }

    if (dbAddress == null)
    {
      System.out.println("ERROR: -dbAddr was not specified");
      Usage();
      System.exit(1);
    }

    if (dbUserID == null)
    {
      System.out.println("ERROR: -dbUser was not specified");
      Usage();
      System.exit(1);
    }

    if (dbPassword == null)
    {
      System.out.println("ERROR: -dbPasswd was not specified");
      Usage();
      System.exit(1);
    }

    if (clientAddress == null)
    {
      System.out.println("ERROR: -clientAddr was not specified");
      Usage();
      System.exit(1);
    }

    System.out.println("===== Start of Test =====");

    try
    {
      String[] bindings = Naming.list(clientAddress);
      System.out.println("Servers currently bound at " + clientAddress);
      for (int i = 0; i < bindings.length; i++)
      {
        System.out.println("  " + bindings[i]);
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    GateKeeper gateKeeper = null;
    try
    {
      gateKeeper = new GateKeeper(dbAddress, dbUserID, dbPassword, installationID, clientAddress);
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      System.exit(1);
    }

    if ((testsToRun.equals("b") == true) || (testsToRun.equals("g") == true))
    {
      RunGUITests(gateKeeper);
    }

    if ((testsToRun.equals("b") == true) || (testsToRun.equals("d") == true))
    {
      RunDatabaseTests(gateKeeper, dbAddress, dbUserID, dbPassword, installationID);
    }

    // end of test
    System.out.println("===== End of Test =====");
    try
    {
      gateKeeper.Done();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }
    gateKeeper = null;

    System.exit(0);
  }

  private static void Usage()
  {
    System.err.println("Usage e.g.: java " + "-Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=GateKeeper.policy GateKeeper "
      + "-dbAddr <database hostname or ip addr> -dbUser <database user id> -dbPasswd <database passwd> " + "-instID <installation ID> -clientAddr <client hostname or ip addr> -runTest <b | d | g>");
  }

  private static void RunGUITests(GateKeeper gateKeeper)
  {

    System.out.println("Testing DisplayPrintJobBeingProcessedMsgBox now.\n" + "This is a non-blocking call.");
    try
    {
      gateKeeper.DisplayPrintJobBeingProcessedMsgBox(PRINT_JOB_NUMBER.toString(), "My Printer"); // printer
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("Testing DisplayPrintJobInformationEntryDialog now.\n" + "This is a blocking call. You need to respond to the dialog\n" + "on the client machine before this test can proceed.");
    try
    {
      Properties properties = gateKeeper.DisplayPrintJobInformationEntryDialog(PRINT_JOB_NUMBER.toString(), "8", // numberOfPages,
        "My Printer", // printer,
        "1.23"); // printJobCost

      System.out.println("Returned values are:");
      Enumeration<?> propertyNames = properties.keys();
      while (propertyNames.hasMoreElements())
      {
        String propertyName = propertyNames.nextElement().toString();
        System.out.println("  " + propertyName + ": " + properties.getProperty(propertyName));
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("Testing DisplayGuestPrintJobInformationEntryDialog now.\n" + "This is a blocking call. You need to respond to the dialog\n"
      + "on the client machine before this test can proceed.");
    try
    {
      Properties properties = gateKeeper.DisplayGuestPrintJobInformationEntryDialog(PRINT_JOB_NUMBER.toString(), "8", // numberOfPages,
        "My Printer", // printer
        "1.23"); // printJobCost

      System.out.println("Returned values are:");
      Enumeration<?> propertyNames = properties.keys();
      while (propertyNames.hasMoreElements())
      {
        String propertyName = propertyNames.nextElement().toString();
        System.out.println("  " + propertyName + ": " + properties.getProperty(propertyName));
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("Testing DisplayPrintJobReleasedMsgBox now.\n" + "This is a non-blocking call.");
    try
    {
      gateKeeper.DisplayPrintJobReleasedMsgBox(PRINT_JOB_NUMBER.toString(), "My Printer", // printer
        "$", // currency symbol
        "5.00", // new balance
        "1.00"); // job cost
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("Testing DisplayInsufficientPrePaymentBalanceMsgBox now.\n" + "This is a non-blocking call.");
    try
    {
      gateKeeper.DisplayInsufficientPrePaymentBalanceMsgBox(PRINT_JOB_NUMBER.toString(), "1.23", // printJobCost
        "4.56", // prePaymentBalance
        "My Printer"); // printer
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("Testing DismissPrintJobBeingProcessedMsgBox now.\n" + "This is a non-blocking call.");
    try
    {
      gateKeeper.DismissPrintJobBeingProcessedMsgBox("Test 123", // printJobNumber,
        "My Printer"); // printer
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

  }

  private static void RunDatabaseTests(GateKeeper gateKeeper, String dbAddress, String dbUserID, String dbPassword, Integer installationID)
  {

    try
    {
      System.out.println("Testing CheckIfInstallationIsEnabled now");
      gateKeeper.CheckIfInstallationIsEnabled();
      System.out.println("\tThis installation is enabled");
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    try
    {
      System.out.println("Testing CheckIfThisIPAddrIsRegistered now.");
      gateKeeper.CheckIfThisIPAddrIsRegistered();
      System.out.println("\tThis IP address is registered");
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    // build a properties object for the following interfaces tests
    Properties printJobProperties = new Properties();
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_PRINT_JOB_NUMBER, PRINT_JOB_NUMBER.toString());
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES, "17");
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE, PrintJobPropertiesRow.VALUE_PAPER_SIZE_LETTER);
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_SIDES, PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX);
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_PRINTER, "MyPrinter");
    printJobProperties.setProperty(PrintJobPropertiesRow.PROPERTY_COLOR, PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME);
    printJobProperties.setProperty("Dummy", "dummy");

    // delete the old one if its in the database
    InstallationDatabase installationDatabase = null;
    try
    {
      installationDatabase = new InstallationDatabase(dbAddress, dbUserID, dbPassword, installationID);
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      System.exit(1);
    }

    installationDatabase.GetPrintJobHeaderTable().RemoveRowForPrintJobNumber(PRINT_JOB_NUMBER);
    installationDatabase.GetPrintJobPropertiesTable().RemoveRowsForPrintJobNumber(PRINT_JOB_NUMBER);
    installationDatabase.Done();

    System.out.println("Testing CostThePrintJob.");
    try
    {
      System.out.println("Print Job Cost = " + String.valueOf(installationDatabase.GetPageChargesTable().CostThePrintJob(printJobProperties)));
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

  }

}
