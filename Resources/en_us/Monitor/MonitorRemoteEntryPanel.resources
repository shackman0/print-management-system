# The string to the left of the "=" is the name of the property (i.e. the property "key").
# Property names are application specific and may NOT be changed.

# The string to the right of the "=" is the value of the property.
# Property values are language specific and may be changed.

CURRENT_RELEASE_NUMBER      = Current Release Number:
CURRENT_RELEASE_NUMBER_TIP  = Enter the installation's current release number

ECOMMERCE_TAB                                  = E-Commerce
SITE_DATA_TAB                                  = Site Data
RULES_TAB                                      = Rules

SMTP_HOST                                      = SMTP Host:
SMTP_HOST_TIP                                  = Enter the installation\'s SMTP host for outgoing e-mail
ADMINISTRATOR_E_MAIL_ADDRESS                   = Admin E-Mail:
ADMINISTRATOR_E_MAIL_ADDRESS_TIP               = Administrator\'s e-mail address

AUTOMATIC                                      = Automatic
AUTOMATIC_TIP                                  = Update the installation's software automatically
CANCEL                                         = Cancel
CANCEL_TIP                                     = Do NOT save the data to the database and close the panel
COMMA                                          = Comma
COMMA_TIP                                      = Make comma \",\" the separator symbol
CREATED                                        = Created:
UPDATED                                        = Last Modified:
CURRENCY_SYMBOL                                = Currency Symbol:
DECIMAL_SEPARATOR                              = Decimal separator:
DISABLED                                       = Disabled
DISABLED_TIP                                   = Disable this installation
ENABLED                                        = Enabled
ENABLED_TIP                                    = Enable this installation
ENABLED_DISABLED                               = Enabled/Disabled:
INSTALLATION_ID                                = Installation ID:
INSTALLATION_NAME                              = Installation Name:
INSTALLATION_NAME_TIP                          = Enter the name of the installation here
INSTALLATION_PASSPHRASE                        = Administrator\'s password:
INSTALLATION_PASSPHRASE_TIP                    = Enter the installation's password here
INTERIM_EXPIRATION_DATE                        = Interim License Exipiration Date:
INTERIM_EXPIRATION_DATE_TIP                    = Select a date after which interim license will expire
LANGUAGE                                       = Language:
MANUAL                                         = Manual
MANUAL_TIP                                     = Update the installation's software manually
MAX_NUMBER_OF_CLIENTS                          = Maximum Number of Clients:
MAX_NUMBER_OF_CLIENTS_TIP                      = Enter the maximum number of clients this installation may support
MAX_NUMBER_OF_GATEKEEPERS                      = Maximum Number of Print Servers:
MAX_NUMBER_OF_GATEKEEPERS_TIP                  = Enter the maximum number of Print Servers this installation may support
PERCENT_SIGN                                   = %
PERIOD                                         = Period
PERIOD_TIP                                     = Make period \".\" the separator symbol
PLUS_SIGN                                      = +
SAVE                                           = Save
SAVE_TIP                                       = Save the data to the database and close the panel.
STATUS                                         = Status:
THOUSANDS_SEPARATOR                            = Thousands separator:
TITLE                                          = Remote Entry Panel - Monitor
UPGRADE_PREFERENCE                             = Upgrade preference:
ECOMMERCE                                      = E-Commerce
EGOLD                                          = E-Gold
PAYPAL                                         = PayPal
CREDITCARD                                     = Credit Card
CHARGE_URL                                     = Charge URL:
CHARGE_ACCOUNT_NUMBER                          = Charge Account Number:
CHARGE_PASSPHRASE                              = Charge Password:
CHARGE_MIN_AMOUNT                              = Charge Minimum Amount:
CHARGE_XACTION_FEE                             = Charge Transaction Fee:
CHARGE_XACTION_FEE_PERCENT                     = Charge Transaction Fee Percent:
REFUND_URL                                     = Refund URL:
REFUND_ACCOUNT_NUMBER                          = Refund Account Number:
REFUND_PASSPHRASE                              = Refund Password:
REFUND_XACTION_FEE                             = Refund Transaction Fee:
REFUND_XACTION_FEE_PERCENT                     = Refund Transaciton Fee Percent:

EGOLD_CHARGE_URL_TIP                           = Enter the URL to the E-Gold service for the specified E-Gold Charge Account Number
EGOLD_CHARGE_ACCOUNT_NUMBER_TIP                = Enter the E-Gold Account Number into which to deposit pre-payments
EGOLD_CHARGE_PASSPHRASE_TIP                    = Enter the Password for the E-Gold Charge Account Number
EGOLD_CHARGE_MIN_AMOUNT_TIP                    = Enter the minimum amount of an E-Gold charge transaction
EGOLD_CHARGE_XACTION_FEE_TIP                   = Enter a fixed amount to charge with each E-Gold charge transaction
EGOLD_CHARGE_XACTION_FEE_PERCENT_TIP           = Enter a percentage to charge with each E-Gold charge transaction
EGOLD_REFUND_URL_TIP                           = Enter the URL to the E-Gold service for the E-Gold Refund Account Number
EGOLD_REFUND_ACCOUNT_NUMBER_TIP                = Enter the E-Gold Account Number from which to refund pre-payments
EGOLD_REFUND_PASSPHRASE_TIP                    = Enter the Password for the E-Gold Refund Account Number
EGOLD_REFUND_XACTION_FEE_TIP                   = Enter a fixed amount to charge with each E-Gold refund transaction
EGOLD_REFUND_XACTION_FEE_PERCENT_TIP           = Enter a percentage to charge with each E-Gold refund transaction

PAYPAL_CHARGE_URL_TIP                          = Enter the URL to the PayPal service for the specified PayPal Charge Account Number
PAYPAL_CHARGE_ACCOUNT_NUMBER_TIP               = Enter the PayPal Account Number into which to deposit pre-payments
PAYPAL_CHARGE_PASSPHRASE_TIP                   = Enter the Password for the PayPal Charge Account Number
PAYPAL_CHARGE_MIN_AMOUNT_TIP                   = Enter the minimum amount of an PayPal charge transaction
PAYPAL_CHARGE_XACTION_FEE_TIP                  = Enter a fixed amount to charge with each PayPal charge transaction
PAYPAL_CHARGE_XACTION_FEE_PERCENT_TIP          = Enter a percentage to charge with each PayPal charge transaction
PAYPAL_REFUND_URL_TIP                          = Enter the URL to the PayPal service for the PayPal Refund Account Number
PAYPAL_REFUND_ACCOUNT_NUMBER_TIP               = Enter the PayPal Account Number from which to refund pre-payments
PAYPAL_REFUND_PASSPHRASE_TIP                   = Enter the Password for the PayPal Refund Account Number
PAYPAL_REFUND_XACTION_FEE_TIP                  = Enter a fixed amount to charge with each PayPal refund transaction
PAYPAL_REFUND_XACTION_FEE_PERCENT_TIP          = Enter a percentage to charge with each PayPal refund transaction

CREDITCARD_CHARGE_URL_TIP                      = Enter the URL to the Credit Card service for the specified Credit Card Charge Account Number
CREDITCARD_CHARGE_ACCOUNT_NUMBER_TIP           = Enter the Credit Card Account Number into which to deposit pre-payments
CREDITCARD_CHARGE_PASSPHRASE_TIP               = Enter the Password for the Credit Card Charge Account Number
CREDITCARD_CHARGE_MIN_AMOUNT_TIP               = Enter the minimum amount of an Credit Card charge transaction
CREDITCARD_CHARGE_XACTION_FEE_TIP              = Enter a fixed amount to charge with each Credit Card charge transaction
CREDITCARD_CHARGE_XACTION_FEE_PERCENT_TIP      = Enter a percentage to charge with each Credit Card charge transaction
CREDITCARD_REFUND_URL_TIP                      = Enter the URL to the Credit Card service for the Credit Card Refund Account Number
CREDITCARD_REFUND_ACCOUNT_NUMBER_TIP           = Enter the Credit Card Account Number from which to refund pre-payments
CREDITCARD_REFUND_PASSPHRASE_TIP               = Enter the Password for the Credit Card Refund Account Number
CREDITCARD_REFUND_XACTION_FEE_TIP              = Enter a fixed amount to charge with each Credit Card refund transaction
CREDITCARD_REFUND_XACTION_FEE_PERCENT_TIP      = Enter a percentage to charge with each Credit Card refund transaction

PREVIOUS      = Prev
PREVIOUS_TIP  = Show previous row in list
NEXT          = Next
NEXT_TIP      = Show next row in list

SAVE_AND_NEW     = Save & New
SAVE_AND_NEW_TIP = Save the data to the database and clear the form for a new installation

BLANK_INSTALLATION_NAME_MSG   = The Installation Name field is blank. You must specify an installation name.
BLANK_INSTALLATION_NAME_TITLE = Blank Installation Name

