package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Properties;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Row;

/**
 * <p>Title: PrintJobHeaderRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobHeaderRow
  extends AbstractSQL2Row
{

  private Integer                 _sql_installation_id;
  private Integer                 _sql_print_job_number;
  private String                  _sql_user_account_id;
  private Integer                 _sql_number_of_pages;
  private String                  _sql_paper_size;
  private String                  _sql_sides;
  private String                  _sql_printer;
  private String                  _sql_color;
  private Double                  _sql_cost;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.000");

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_print_job_number(),
                                    Name_user_account_id(),
                                    Name_number_of_pages(),
                                    Name_paper_size(),
                                    Name_sides(),
                                    Name_printer(),
                                    Name_color(),
                                    Name_cost(),

                                    Name_created_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_user_account_id(),
                                    Name_print_job_number() };

  public static final String      SIDES_SIMPLEX_CODE = "s";
  public static final String      SIDES_SIMPLEX_NAME = PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX;
  public static final String      SIDES_DUPLEX_CODE = "d";
  public static final String      SIDES_DUPLEX_NAME = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;

  public static final String      COLOR_MONOCHROME_CODE = "m";
  public static final String      COLOR_MONOCHROME_NAME = PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME;
  public static final String      COLOR_COLOR_CODE = "c";
  public static final String      COLOR_COLOR_NAME = PrintJobPropertiesRow.VALUE_COLOR_COLOR;



  public PrintJobHeaderRow()
  {
    super();
  }

  public PrintJobHeaderRow(PrintJobHeaderTable table)
  {
    super(table);
  }

  public PrintJobHeaderRow( PrintJobHeaderTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public PrintJobHeaderRow( PrintJobHeaderTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public PrintJobHeaderRow( PrintJobHeaderTable table, Properties printJobProperties )
  {
    super(table);
    SetValues(printJobProperties);
  }

  public void SetValues(Properties printJobProperties)
  {
    Set_print_job_number(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PRINT_JOB_NUMBER));
    Set_user_account_id(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_USER_ACCOUNT_ID));
    Set_number_of_pages(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES));
    Set_paper_size(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE));
    Set_sides(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_SIDES));
    Set_printer(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_PRINTER));
    Set_color(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_COLOR));
    Set_cost(printJobProperties.getProperty(PrintJobPropertiesRow.PROPERTY_COST));
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetPrintJobHeaderTable().GetInstallationDatabase().GetInstallationID());
    Set_sides(SIDES_SIMPLEX_NAME);
    Set_color(COLOR_MONOCHROME_NAME);
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public Integer Get_print_job_number() { return new Integer(_sql_print_job_number.intValue()); }
  public void Set_print_job_number(Integer value) { _sql_print_job_number = new Integer(value.intValue()); }
  public void Set_print_job_number(String value) { _sql_print_job_number = new Integer(value); }
  public void Set_print_job_number(int value) { _sql_print_job_number = new Integer(value); }
  public static String Name_print_job_number() { return "print_job_number"; }

  public String Get_user_account_id() { return new String(_sql_user_account_id); }
  public void Set_user_account_id(String value) { _sql_user_account_id = new String(value); }
  public static String Name_user_account_id() { return "user_account_id"; }

  public Integer Get_number_of_pages() { return new Integer(_sql_number_of_pages.intValue()); }
  public void Set_number_of_pages(Integer value) { _sql_number_of_pages = new Integer(value.intValue()); }
  public void Set_number_of_pages(String value) { _sql_number_of_pages = new Integer(value); }
  public void Set_number_of_pages(int value) { _sql_number_of_pages = new Integer(value); }
  public static String Name_number_of_pages() { return "number_of_pages"; }

  public String Get_paper_size() { return new String(_sql_paper_size); }
  public void Set_paper_size(String value) { _sql_paper_size = new String(value); }
  public static String Name_paper_size() { return "paper_size"; }

  public String Get_sides()
  {
    String value = null;
    if (_sql_sides.equals(SIDES_SIMPLEX_CODE) == true)
    {
      value = SIDES_SIMPLEX_NAME;
    }
    else
    if (_sql_sides.equals(SIDES_DUPLEX_CODE) == true)
    {
      value = SIDES_DUPLEX_NAME;
    }
    else
    {
      new Exception("Get_sides:Invalid _sql_sides=\""+_sql_sides+"\"").printStackTrace();
      value = new String(_sql_sides);       // fail safe
    }
    return value;
  }
  public String GetInternal_sides() { return new String(_sql_sides); }
  public void Set_sides(String value)
  {
    if (value.equals(SIDES_SIMPLEX_NAME) == true)
    {
      _sql_sides = SIDES_SIMPLEX_CODE;
    }
    else
    if (value.equals(SIDES_DUPLEX_NAME) == true)
    {
      _sql_sides = SIDES_DUPLEX_CODE;
    }
    else
    {
      if (value.length() > 0)
      {
        new Exception("Set_sides:Invalid sides=\""+value+"\"").printStackTrace();
      }
      _sql_sides = new String(value);        // fail safe
    }
  }
  public static String Name_sides() { return "sides"; }

  public String Get_printer() { return new String(_sql_printer); }
  public void Set_printer(String value) { _sql_printer = new String(value); }
  public static String Name_printer() { return "printer"; }

  public String Get_color()
  {
    String value = null;
    if (_sql_color.equals(COLOR_MONOCHROME_CODE) == true)
    {
      value = COLOR_MONOCHROME_NAME;
    }
    else
    if (_sql_color.equals(COLOR_COLOR_CODE) == true)
    {
      value = COLOR_COLOR_NAME;
    }
    else
    {
      new Exception("Get_color:Invalid _sql_color=\""+_sql_sides+"\"").printStackTrace();
      value = new String(_sql_color);       // fail safe
    }
    return value;
  }
  public String GetInternal_color() { return new String(_sql_color); }
  public void Set_color(String value)
  {
    if (value.equals(COLOR_MONOCHROME_NAME) == true)
    {
      _sql_color = COLOR_MONOCHROME_CODE;
    }
    else
    if (value.equals(COLOR_COLOR_NAME) == true)
    {
      _sql_color = COLOR_COLOR_CODE;
    }
    else
    {
      if (value.length() > 0)
      {
        new Exception("Set_color:Invalid color=\""+value+"\"").printStackTrace();
      }
      _sql_color = new String(value);        // fail safe
    }
  }
  public static String Name_color() { return "color"; }

  public Double Get_cost() { return new Double(_sql_cost.doubleValue()); }
  public String GetFormatted_cost() { return __formatter.format(_sql_cost); }
  public void Set_cost(Double value) { _sql_cost = new Double(value.doubleValue()); }
  public void Set_cost(String value) { _sql_cost = new Double(value); }
  public void Set_cost(double value) { _sql_cost = new Double(value); }
  public static String Name_cost() { return "cost"; }


  public PrintJobHeaderTable GetPrintJobHeaderTable()
  {
    return (PrintJobHeaderTable)_sqlTable;
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(PrintJobHeaderRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}