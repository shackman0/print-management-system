package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Row;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;



/**
 * <p>Title: PageChargesRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PageChargesRow
  extends AbstractSQL3Row
{

  private Integer                 _sql_installation_id;
  private Integer                 _sql_page_charge_number;
  private Double                  _sql_per_page_charge;
  private String                  _sql_paper_size;
  private String                  _sql_sides;
  private String                  _sql_color;
  private String                  _sql_printer;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.000");

  public static final int         PAPER_SIZE_WIDTH = 32;
  public static final int         SIDES_WIDTH = 1;
  public static final int         COLOR_WIDTH = 1;
  public static final int         PRINTER_WIDTH = 32;

  public static final String      SIDES_SIMPLEX_CODE = PrintJobHeaderRow.SIDES_SIMPLEX_CODE;
  public static final String      SIDES_DUPLEX_CODE = PrintJobHeaderRow.SIDES_DUPLEX_CODE;
  public static final String      COLOR_COLOR_CODE = PrintJobHeaderRow.COLOR_COLOR_CODE;
  public static final String      COLOR_MONOCHROME_CODE = PrintJobHeaderRow.COLOR_MONOCHROME_CODE;

  public static final String      SIDES_SIMPLEX_NAME = PrintJobPropertiesRow.VALUE_SIDES_SIMPLEX;
  public static final String      SIDES_DUPLEX_NAME = PrintJobPropertiesRow.VALUE_SIDES_DUPLEX;
  public static final String      COLOR_COLOR_NAME = PrintJobPropertiesRow.VALUE_COLOR_COLOR;
  public static final String      COLOR_MONOCHROME_NAME = PrintJobPropertiesRow.VALUE_COLOR_MONOCHROME;

  public static final String      ANY_PRINTER_CODE = SQLUtilities.HighValues(PRINTER_WIDTH);
  public static final String      ANY_SIDES_CODE = SQLUtilities.HighValues(SIDES_WIDTH);
  public static final String      ANY_SIZE_CODE = SQLUtilities.HighValues(PAPER_SIZE_WIDTH);
  public static final String      ANY_COLOR_CODE = SQLUtilities.HighValues(COLOR_WIDTH);

  public static final String      ANY_PRINTER_NAME = "Any Printer";
  public static final String      ANY_SIDES_NAME = "Any Sides";
  public static final String      ANY_SIZE_NAME = "Any Size";
  public static final String      ANY_COLOR_NAME = "Any Color";

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_page_charge_number(),
                                    Name_per_page_charge(),
                                    Name_paper_size(),
                                    Name_sides(),
                                    Name_color(),
                                    Name_printer(),

                                    Name_created_dts(),
                                    Name_updated_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_page_charge_number() };

  public PageChargesRow()
  {
    super();
  }

  public PageChargesRow(PageChargesTable table)
  {
    super(table);
  }

  public PageChargesRow( PageChargesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public PageChargesRow( PageChargesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();

    Set_installation_id(GetInstallationDatabase().GetInstallationID());
    Set_paper_size(ANY_SIZE_NAME);
    Set_sides(ANY_SIDES_NAME);
    Set_color(ANY_COLOR_NAME);
    Set_printer(ANY_PRINTER_NAME);
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public Integer Get_page_charge_number() { return new Integer(_sql_page_charge_number.intValue()); }
  public void Set_page_charge_number(Integer value) { _sql_page_charge_number = new Integer(value.intValue()); }
  public void Set_page_charge_number(String value) { _sql_page_charge_number = new Integer(value); }
  public void Set_page_charge_number(int value) { _sql_page_charge_number = new Integer(value); }
  public static String Name_page_charge_number() { return "page_charge_number"; }
  public void SetNewPageChargeNumber()
  {
    Set_page_charge_number(GetInstallationDatabase().GetPageChargeNumberTable().GetNextPageChargeNumber());
  }

  public Double Get_per_page_charge() { return new Double(_sql_per_page_charge.doubleValue()); }
  public String GetFormatted_per_page_charge() { return __formatter.format(_sql_per_page_charge); }
  public void Set_per_page_charge(Double value) { _sql_per_page_charge = new Double(value.doubleValue()); }
  public void Set_per_page_charge(String value) { _sql_per_page_charge = new Double(value); }
  public void Set_per_page_charge(double value) { _sql_per_page_charge = new Double(value); }
  public static String Name_per_page_charge() { return "per_page_charge"; }

  public String Get_paper_size()
  {
    String value = new String(_sql_paper_size);
    if (value.equals(ANY_SIZE_CODE) == true)
    {
      value = ANY_SIZE_NAME;
    }
    return value;
  }
  public void Set_paper_size(String value)
  {
    if (value.equals(ANY_SIZE_NAME) == true)
    {
      value = ANY_SIZE_CODE;
    }
    _sql_paper_size = new String(value);
  }
  public static String Name_paper_size() { return "paper_size"; }
  public boolean SizeIsAny()
  {
    return _sql_paper_size.equals(ANY_SIZE_CODE) == true;
  }

  public String Get_sides()
  {
    String value = new String(_sql_sides);
    if (value.equals(SIDES_SIMPLEX_CODE) == true)
    {
      value = SIDES_SIMPLEX_NAME;
    }
    else
    if (value.equals(SIDES_DUPLEX_CODE) == true)
    {
      value = SIDES_DUPLEX_NAME;
    }
    else
    if (value.equals(ANY_SIDES_CODE) == true)
    {
      value = ANY_SIDES_NAME;
    }
    return value;
  }
  public void Set_sides(String value)
  {
    if (value.equals(SIDES_SIMPLEX_NAME) == true)
    {
      value = SIDES_SIMPLEX_CODE;
    }
    else
    if (value.equals(SIDES_DUPLEX_NAME) == true)
    {
      value = SIDES_DUPLEX_CODE;
    }
    else
    if (value.equals(ANY_SIDES_NAME) == true)
    {
      value = ANY_SIDES_CODE;
    }
    _sql_sides = new String(value);
  }
  public static String Name_sides() { return "sides"; }
  public boolean SidesIsSimplex()
  {
    return _sql_sides.equals(SIDES_SIMPLEX_CODE) == true;
  }
  public boolean SidesIsDuplex()
  {
    return _sql_sides.equals(SIDES_DUPLEX_CODE) == true;
  }
  public boolean SidesIsAny()
  {
    return _sql_sides.equals(ANY_SIDES_CODE) == true;
  }


  public String Get_color()
  {
    String value = new String(_sql_color);
    if (value.equals(COLOR_COLOR_CODE) == true)
    {
      value = COLOR_COLOR_NAME;
    }
    else
    if (value.equals(COLOR_MONOCHROME_CODE) == true)
    {
      value = COLOR_MONOCHROME_NAME;
    }
    else
    if (value.equals(ANY_COLOR_CODE) == true)
    {
      value = ANY_COLOR_NAME;
    }
    return value;
  }
  public void Set_color(String value)
  {
    if (value.equals(COLOR_COLOR_NAME) == true)
    {
      value = COLOR_COLOR_CODE;
    }
    else
    if (value.equals(COLOR_MONOCHROME_NAME) == true)
    {
      value = COLOR_MONOCHROME_CODE;
    }
    else
    if (value.equals(ANY_COLOR_NAME) == true)
    {
      value = ANY_COLOR_CODE;
    }
    _sql_color = new String(value);
  }
  public static String Name_color() { return "color"; }
  public boolean ColorIsMonochrome()
  {
    return (_sql_color.equals(COLOR_MONOCHROME_CODE) == true);
  }
  public boolean ColorIsColor()
  {
    return (_sql_color.equals(COLOR_COLOR_CODE) == true);
  }
  public boolean ColorIsAny()
  {
    return (_sql_color.equals(ANY_COLOR_CODE) == true);
  }


  public String Get_printer()
  {
    String value = new String(_sql_printer);
    if (value.equals(ANY_PRINTER_CODE) == true)
    {
      value = ANY_PRINTER_NAME;
    }
    return value;
  }
  public void Set_printer(String value)
  {
    if (value.equals(ANY_PRINTER_NAME) == true)
    {
      value = ANY_PRINTER_CODE;
    }
    _sql_printer = new String(value);
  }
  public static String Name_printer() { return "printer"; }
  public boolean PrinterIsAny()
  {
    return (_sql_printer.equals(ANY_PRINTER_CODE) == true);
  }


  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  public PageChargesTable GetPageChargesTable()
  {
    return (PageChargesTable)_sqlTable;
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(PageChargesRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}