package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: RulesTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class RulesTable
  extends AbstractSQL3Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "rules_table";

  public RulesTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, RulesRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  /**
    this operation removes a rule from the rules table
    for a specified rule
  */
  public void RemoveRowForRuleName(String ruleName)
  {
    RemoveRow(GetRowForRuleName(ruleName));
  }

  /**
    this operation returns a rule.
    returns null if row not found.
  */
  public RulesRow GetRowForRuleName(String ruleName)
  {
    String predicate = RulesRow.Name_rule_name() + " = '" + ruleName.trim() + "'";
    return (RulesRow) GetFirstRow(predicate, null);
  }

  public RulesRow[] GetRules()
  {
    RulesRow[] ruleRows = new RulesRow[RulesRow.NUMBER_OF_RULES];
    for (int i = 0; i < RulesRow.NUMBER_OF_RULES; i++)
    {
      RulesRow ruleRow = GetRowForRuleName(RulesRow.RULE_NAMES[i]);
      if (ruleRow == null)
      {
        ruleRow = (RulesRow) (CreateRow());
        ruleRow.Set_installation_id(GetInstallationDatabase().GetInstallationID());
        ruleRow.Set_rule_name(RulesRow.RULE_NAMES[i]);
        ruleRow.Set_rule_value(RulesRow.RULE_DEFAULT_VALUES[i]);
      }

      ruleRows[i] = ruleRow;
    }
    return ruleRows;
  }

  /*
    public ArrayList GetRulesRowsAsArrayList ()
    {
      ArrayList arrayList = new ArrayList(10);

      SQLStatement sqlStatement = GetRulesRowsAsResultSet();
      ResultSet resultSet = sqlStatement.GetResultSet();

      try
      {
        while (resultSet.next())
        {
          RulesRow ruleRow = new RulesRow(this,resultSet);
          arrayList.add(ruleRow);
        }
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "resultSet.next()", JOptionPane.ERROR_MESSAGE);
      }
      sqlStatement.MakeAvailable();

      arrayList.trimToSize();

      return arrayList;
    }

    public SQLStatement GetRulesRowsAsResultSet ()
    {
      String predicate =
        RulesRow.Name_installation_id() + " = '" + GetInstallationDatabase().GetInstallationID().toString() + "'";
      String orderBy = RulesRow.Name_rule_name();

      SQLStatement sqlStatement = GetRowsAsResultSet (
                                    null,       // from, null = use this table's name
                                    predicate,
                                    orderBy,
                                    NO_LIMIT );

      return sqlStatement;
    }
  */

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(RulesRow.Name_rule_name()) == true)
    {
      return 100;
    }
    else
      if (columnName.equals(RulesRow.Name_rule_value()) == true)
      {
        return 100;
      }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = RulesRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
