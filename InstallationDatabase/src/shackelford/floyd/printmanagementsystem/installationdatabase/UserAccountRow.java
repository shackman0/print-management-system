package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;


/**
 * <p>Title: UserAccountRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountRow
  extends AbstractSQL4Row
{

  private Integer                 _sql_installation_id;
  private String                  _sql_user_account_id;
  private String                  _sql_user_name;
  private String                  _sql_user_passphrase;
  private String                  _sql_additional_info;
  private String                  _sql_language;
  private Double                  _sql_pre_payment_balance;
  private Boolean                 _sql_require_new_password;
  private String                  _sql_e_mail_address;
  private String                  _sql_home_address;
  private String                  _sql_phone_number;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.00");

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_user_account_id(),
                                    Name_user_name(),
                                    Name_user_passphrase(),
                                    Name_additional_info(),
                                    Name_language(),
                                    Name_pre_payment_balance(),
                                    Name_require_new_password(),
                                    Name_e_mail_address(),
                                    Name_home_address(),
                                    Name_phone_number(),

                                    Name_status(),
                                    Name_created_dts(),
                                    Name_updated_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_user_account_id() };

  public UserAccountRow()
  {
    super();
  }

  public UserAccountRow(UserAccountTable table)
  {
    super(table);
  }

  public UserAccountRow( UserAccountTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public UserAccountRow( UserAccountTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetInstallationDatabase().GetInstallationID());
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public String Get_user_account_id() { return new String(_sql_user_account_id); }
  public void Set_user_account_id(String value) { _sql_user_account_id = new String(value); }
  public static String Name_user_account_id() { return "user_account_id"; }
  public void SetNewUserAccountID()
  {
    Set_user_account_id(GetInstallationDatabase().GetUserAccountNumberTable().GetNextUserAccountID());
  }

  public String Get_user_name() { return new String(_sql_user_name); }
  public void Set_user_name(String value) { _sql_user_name = new String(value); }
  public static String Name_user_name() { return "user_name"; }

  public String Get_user_passphrase() { return new String(_sql_user_passphrase); }
  public void Set_user_passphrase(String value) { _sql_user_passphrase = new String(value); }
  public static String Name_user_passphrase() { return "user_passphrase"; }

  public String Get_additional_info() { return new String(_sql_additional_info); }
  public void Set_additional_info(String value) { _sql_additional_info = new String(value); }
  public static String Name_additional_info() { return "additional_info"; }

  public String Get_language() { return new String(_sql_language); }
  public void Set_language(String value) { _sql_language = new String(value); }
  public static String Name_language() { return "language"; }

  public Double Get_pre_payment_balance() { return new Double(_sql_pre_payment_balance.doubleValue()); }
  public String GetFormatted_pre_payment_balance() { return __formatter.format(_sql_pre_payment_balance); }
  public void Set_pre_payment_balance(Double value) { _sql_pre_payment_balance = new Double(value.doubleValue()); }
  public void Set_pre_payment_balance(String value) { _sql_pre_payment_balance = new Double(value); }
  public void Set_pre_payment_balance(double value) { _sql_pre_payment_balance = new Double(value); }
  public static String Name_pre_payment_balance() { return "pre_payment_balance"; }
  public void AddToPrePaymentBalance(double adjustment)
  {
    Set_pre_payment_balance(Get_pre_payment_balance().doubleValue() + adjustment);
  }
  public void DeductFromPrePaymentBalance(double adjustment)
  {
    Set_pre_payment_balance(Get_pre_payment_balance().doubleValue() - adjustment);
  }

  public Boolean Get_require_new_password() { return new Boolean(_sql_require_new_password.booleanValue()); }
  public void Set_require_new_password(Boolean value) { _sql_require_new_password = new Boolean(value.booleanValue()); }
  public void Set_require_new_password(String value) { _sql_require_new_password = new Boolean(value); }
  public void Set_require_new_password(boolean value) { _sql_require_new_password = new Boolean(value); }
  public static String Name_require_new_password() { return "require_new_password"; }

  public String Get_e_mail_address() { return new String(_sql_e_mail_address); }
  public void Set_e_mail_address(String value) { _sql_e_mail_address = new String(value); }
  public static String Name_e_mail_address() { return "e_mail_address"; }

  public String Get_home_address() { return new String(_sql_home_address); }
  public void Set_home_address(String value) { _sql_home_address = new String(value); }
  public static String Name_home_address() { return "home_address"; }

  public String Get_phone_number() { return new String(_sql_phone_number); }
  public void Set_phone_number(String value) { _sql_phone_number = new String(value); }
  public static String Name_phone_number() { return "phone_number"; }


  public UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable)_sqlTable;
  }

  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  public InstallationTable GetInstallationTable()
  {
    return (InstallationTable)_sqlTable;
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(UserAccountRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}