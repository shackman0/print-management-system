package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Properties;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: PrintJobPropertiesTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobPropertiesTable
  extends AbstractSQL1Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "print_job_properties_table";

  public PrintJobPropertiesTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, PrintJobPropertiesRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public void InsertRowsFromProperties(PrintJobHeaderRow printJobHeaderRow, Properties printJobProperties)
  {
    PrintJobPropertiesRow printJobPropertiesRow = new PrintJobPropertiesRow(this, printJobHeaderRow);

    printJobPropertiesRow.Set_installation_id(printJobHeaderRow.Get_installation_id());
    printJobPropertiesRow.Set_print_job_number(printJobHeaderRow.Get_print_job_number());

    Enumeration<Object> propertyNames = printJobProperties.keys();
    while (propertyNames.hasMoreElements())
    {
      String propertyName = propertyNames.nextElement().toString();
      String propertyValue;
      try
      {
        propertyValue = URLDecoder.decode(printJobProperties.getProperty(propertyName), LPDClient.UTF_8);
      }
      catch (UnsupportedEncodingException ex)
      {
        throw new RuntimeException(ex);
      }

      printJobPropertiesRow.Set_property_name(propertyName);
      printJobPropertiesRow.Set_property_value(propertyValue);

      printJobPropertiesRow.Insert();
    }
  }

  /**
    this operation returns the number of rows in the print job properties table
  */
  public int RowCountForPrintJobNumber(Integer printJobNumber)
  {
    String predicate = PrintJobPropertiesRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
    return RowCount(predicate);
  }

  /**
    this operation removes print job Properties from the print job Properties table
    for a specific print job
  */
  public void RemoveRowsForPrintJobNumber(Integer printJobNumber)
  {
    String predicate = PrintJobPropertiesRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
    RemoveRows(predicate);
  }

  /*
    public ArrayList GetRowsForPrintJobNumberAsArrayList ( String printJobNumber )
    {
      String predicate = PrintJobPropertiesRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
      String orderBy = PrintJobPropertiesRow.Name_property_name();
      return GetRowsAsArrayList (
               null,    // from, null = use this table's name
               predicate,
               orderBy,
               0 );     // limit, 0 = unlimited
    }


    public SQLStatement GetRowsForPrintJobNumberAsResultSet ( String printJobNumber )
    {
      String predicate = PrintJobPropertiesRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
      String orderBy = PrintJobPropertiesRow.Name_property_name();
      return GetRowsAsResultSet (
               null,    // from, null = use this table's name
               predicate,
               orderBy,
               0 );     // limit, 0 = unlimited
    }
  */

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(PrintJobPropertiesRow.Name_property_value()) == true)
    {
      return 100;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = PrintJobPropertiesRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
