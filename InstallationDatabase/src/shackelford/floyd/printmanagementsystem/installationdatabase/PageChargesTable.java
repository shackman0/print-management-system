package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;



/**
 * <p>Title: PageChargesTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PageChargesTable
  extends AbstractSQL3Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "page_charges_table";

  public PageChargesTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, PageChargesRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public PageChargesRow GetPageChargesRow(Integer pageChargeNumber)
  {
    String predicate = PageChargesRow.Name_page_charge_number() + " = '" + pageChargeNumber.toString() + "'";
    return (PageChargesRow) GetFirstRow(predicate, null);
  }

  public int GetNextPageChargeNumber()
  {
    int nextPageChargeNumber = -1;


    try
    (  SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();
       ResultSet resultSet = sqlStatement.ExecuteQuery("select max(" + PageChargesRow.Name_page_charge_number() + ") + 1 from " + TABLE_NAME + " " + "where " + PageChargesRow.Name_installation_id()
         + " = '" + GetInstallationDatabase().GetInstallationID().toString() + "'");)
    {
      resultSet.next(); // position us to the first row returned from the query
      nextPageChargeNumber = resultSet.getInt(1);
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
    }

    return nextPageChargeNumber;
  }

  /**
   *  This operation determines the cost of the print job and returns it to the caller.
   *  An exception is thrown if no suitable print charge record if found. In this case,
   *  the print job cost could not be calculated and the print job should not be printed.
   *
   *  This is a blocking call.
   *
   *  @param properties All the print job properties need to be placed in this properties
   *    object. The following minimum print job properties must be set:
   *      PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES;
   *      PrintJobPropertiesRow.PROPERTY_PAPER_SIZE;
   *      PrintJobPropertiesRow.PROPERTY_SIDES;
   *      PrintJobPropertiesRow.PROPERTY_PRINTER;
   *      PrintJobPropertiesRow.PROPERTY_COLOR;
   */
  public Double CostThePrintJob(Properties properties)
    throws Exception
  {
    String paperSize = properties.getProperty(PrintJobPropertiesRow.PROPERTY_PAPER_SIZE);
    String sides = properties.getProperty(PrintJobPropertiesRow.PROPERTY_SIDES);
    String printer = properties.getProperty(PrintJobPropertiesRow.PROPERTY_PRINTER);
    String color = properties.getProperty(PrintJobPropertiesRow.PROPERTY_COLOR);

    String orderBy = PageChargesRow.Name_printer() + ", " + PageChargesRow.Name_paper_size() + ", " + PageChargesRow.Name_sides() + ", " + PageChargesRow.Name_color();

    boolean usingDefaultCharge = false;
    Double totalCost = null;

    // look for an exact match
    String predicate = CostThePrintJobPredicate(paperSize, false, sides, false, color, false, printer, false);
    PageChargesRow pageChargesRow = GetPageChargesRow(predicate,orderBy);
    if (pageChargesRow == null)
    {
      // look for a match allowing for any color
      predicate = CostThePrintJobPredicate(paperSize, false, sides, false, color, true, printer, false);
      pageChargesRow = GetPageChargesRow(predicate,orderBy);
    }
    if (pageChargesRow == null)
    {
      // look for a match allowing for any color, any sides
      predicate = CostThePrintJobPredicate(paperSize, false, sides, true, color, true, printer, false);
      pageChargesRow = GetPageChargesRow(predicate,orderBy);
    }
    if (pageChargesRow == null)
    {
      // look for a match allowing for any color, any sides, any size
      predicate = CostThePrintJobPredicate(paperSize, true, sides, true, color, true, printer, false);
      pageChargesRow = GetPageChargesRow(predicate,orderBy);
    }
    if (pageChargesRow == null)
    {
      // look for a match allowing for any color, any sides, any size, any printer.
      // this is the "default" charge row.
      predicate = CostThePrintJobPredicate(paperSize, true, sides, true, color, true, printer, true);
      pageChargesRow = GetPageChargesRow(predicate,orderBy);
      usingDefaultCharge = true;
    }
    if (pageChargesRow == null)
    {
      /* Error: we didn't even find the default row of all high_values */
      throw new Exception("Default Print Charge Row not found in database.");
    }

    /* check to see if we found the default row, i.e. everything's equal to high_values. */
    if (usingDefaultCharge == true)
    {
      /* insert a page charge row for this printer for future use */
      pageChargesRow.Set_printer(printer);
      pageChargesRow.SetNewPageChargeNumber();
      pageChargesRow.Insert();
    }

    double numberOfPages = Double.valueOf(properties.getProperty(PrintJobPropertiesRow.PROPERTY_NUMBER_OF_PAGES)).doubleValue();
    double perPageCost = pageChargesRow.Get_per_page_charge().doubleValue();

    totalCost = new Double(perPageCost * numberOfPages);

    return totalCost;
  }

  PageChargesRow GetPageChargesRow(String predicate, String orderBy) 
    throws SQLException
  {
    try
    ( SQLStatement sqlStatement = GetRowsAsResultSet(null /* from */, predicate, orderBy, AbstractSQL1Table.NO_LIMIT);
      ResultSet resultSet = sqlStatement.GetResultSet();
    )
    {
      if (resultSet.next() == true)
      {
        return new PageChargesRow(this, resultSet);
      }
    }
    
    return null;
  }
  
  String CostThePrintJobPredicate(String paperSize, boolean anyPaperSize, String sides, boolean anySides, String color, boolean anyColor, String printer, boolean anyPrinter)
  {
    String predicate = "";

    paperSize = paperSize.substring(0, Math.min(paperSize.length(), PageChargesRow.PAPER_SIZE_WIDTH));
    predicate += "( (paper_size = '" + paperSize.trim().toLowerCase() + "') ";

    if (anyPaperSize == true)
    {
      predicate += "or (paper_size = '" + PageChargesRow.ANY_SIZE_CODE + "') ";
    }

    sides = sides.substring(0, Math.min(sides.length(), PageChargesRow.SIDES_WIDTH));
    predicate += ") and ( (sides = '" + sides.trim().toLowerCase() + "') ";

    if (anySides == true)
    {
      predicate += "or (sides = '" + PageChargesRow.ANY_SIDES_CODE + "') ";
    }

    color = color.substring(0, Math.min(color.length(), PageChargesRow.COLOR_WIDTH));
    predicate += ") and ( (color = '" + color.trim().toLowerCase() + "') ";

    if (anyColor == true)
    {
      predicate += "or (color = '" + PageChargesRow.ANY_COLOR_CODE + "') ";
    }

    printer = printer.substring(0, Math.min(printer.length(), PageChargesRow.PRINTER_WIDTH));
    predicate += ") and ( (printer = '" + printer.trim().toLowerCase() + "') ";

    if (anyPrinter == true)
    {
      predicate += "or (printer = '" + PageChargesRow.ANY_PRINTER_CODE + "') ";
    }
    predicate += ") ";

    return predicate;
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(PageChargesRow.Name_color()) == true)
    {
      return 60;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = PageChargesRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
