package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Row;



/**
 * <p>Title: PrePaymentHistoryRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrePaymentHistoryRow
  extends AbstractSQL2Row
{

  private Integer                 _sql_installation_id;
  private String                  _sql_user_account_id;
  private Double                  _sql_amount;
  private String                  _sql_source;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.000");

  public static int               CREDIT_CARD_NUMBER_WIDTH = 19;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;

  private static String[]         __columnNames = {
                                    Name_user_account_id(),
                                    Name_amount(),
                                    Name_source(),

                                    Name_created_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_user_account_id(),
                                    Name_created_dts() };

  public PrePaymentHistoryRow()
  {
    super();
  }

  public PrePaymentHistoryRow(PrePaymentHistoryTable table)
  {
    super(table);
  }

  public PrePaymentHistoryRow( PrePaymentHistoryTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public PrePaymentHistoryRow( PrePaymentHistoryTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetInstallationDatabase().GetInstallationID());
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public String Get_user_account_id() { return new String(_sql_user_account_id); }
  public void Set_user_account_id(String value) { _sql_user_account_id = new String(value); }
  public static String Name_user_account_id() { return "user_account_id"; }

  public Double Get_amount() { return new Double(_sql_amount.doubleValue()); }
  public String GetFormatted_amount() { return __formatter.format(_sql_amount); }
  public void Set_amount(Double value) { _sql_amount = new Double(value.doubleValue()); }
  public void Set_amount(String value) { _sql_amount = new Double(value); }
  public void Set_amount(double value) { _sql_amount = new Double(value); }
  public static String Name_amount() { return "amount"; }

  public String Get_source() { return new String(_sql_source); }
  public void Set_source(String value) { _sql_source = new String(value); }
  public static String Name_source() { return "source"; }


  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(PrePaymentHistoryRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}