package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: PrintJobOnHoldHeaderRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobOnHoldHeaderRow
  extends PrintJobHeaderRow
{

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_print_job_number(),
                                    Name_user_account_id(),
                                    Name_number_of_pages(),
                                    Name_paper_size(),
                                    Name_sides(),
                                    Name_printer(),
                                    Name_color(),
                                    Name_cost(),

                                    Name_created_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_user_account_id(),
                                    Name_print_job_number() };

  public PrintJobOnHoldHeaderRow()
  {
    super();
  }

  public PrintJobOnHoldHeaderRow(PrintJobOnHoldHeaderTable table)
  {
    super(table);
  }

  public PrintJobOnHoldHeaderRow( PrintJobOnHoldHeaderTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public PrintJobOnHoldHeaderRow( PrintJobOnHoldHeaderTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  public void MovePrintJobOnHoldHeader()
  {
    GetPrintJobOnHoldHeaderTable().MovePrintJobOnHoldHeader(this);
  }

  public PrintJobOnHoldHeaderTable GetPrintJobOnHoldHeaderTable()
  {
    return (PrintJobOnHoldHeaderTable)_sqlTable;
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(PrintJobOnHoldHeaderRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}