package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: UserAccountNumberRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountNumberRow
  extends AbstractSQL1Row
{

  private Integer                 _sql_installation_id;
  private Integer                 _sql_last_user_account_number;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_last_user_account_number(),
                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id() };

  public UserAccountNumberRow()
  {
    super();
  }

  public UserAccountNumberRow(UserAccountNumberTable table)
  {
    super(table);
  }

  public UserAccountNumberRow( UserAccountNumberTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public UserAccountNumberRow( UserAccountNumberTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetInstallationDatabase().GetInstallationID());
    Set_last_user_account_number(10000);
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public Integer Get_last_user_account_number() { return new Integer(_sql_last_user_account_number.intValue()); }
  public void Set_last_user_account_number(Integer value) { _sql_last_user_account_number = new Integer(value.intValue()); }
  public void Set_last_user_account_number(String value) { _sql_last_user_account_number = new Integer(value); }
  public void Set_last_user_account_number(int value) { _sql_last_user_account_number = new Integer(value); }
  public static String Name_last_user_account_number() { return "last_user_account_number"; }


  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(UserAccountNumberRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}