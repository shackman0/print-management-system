package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: InstallationTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationTable
  extends AbstractSQL4Table
{

  private static InstallationRow         __cachedInstallationRow = null;

  private static HashMap<String, String> __columnToViewNamesMap  = null;

  private static HashMap<String, String> __viewToColumnNamesMap  = null;

  private static Resources               __resources             = null;

  public static final String             TABLE_NAME              = "installation_table";

  public InstallationTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, InstallationRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  /**
   * this method seeds all the tables associated with a new installation.
   * if the "seed" row is already in the database, we just leave it as is
   * and go on to the next "seed" row.
   */
  @Override
  public boolean InsertRow(AbstractSQL1Row row)
  {
    InstallationRow installationRow = (InstallationRow) row;

    try
    (SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.BeginTransaction();
  
      if (super.InsertRow(row) == false)
      {
        sqlStatement.RollbackTransaction();
        return false;
      }
    
      if (GetInstallationDatabase().GetPageChargesTable().GetPageChargesRow(new Integer(0)) == null)
      {
        PageChargesRow pageChargesRow = (PageChargesRow) (GetInstallationDatabase().GetPageChargesTable().CreateRow());
        pageChargesRow.Set_installation_id(installationRow.Get_installation_id());
        if (pageChargesRow.Insert() == false)
        {
          sqlStatement.RollbackTransaction();
          return false;
        }
      }
  
      if (GetInstallationDatabase().GetPageChargeNumberTable().GetFirstRow(null, null) == null)
      {
        PageChargeNumberRow pageChargeNumberRow = (PageChargeNumberRow) (GetInstallationDatabase().GetPageChargeNumberTable().CreateRow());
        pageChargeNumberRow.Set_installation_id(installationRow.Get_installation_id());
        if (pageChargeNumberRow.Insert() == false)
        {
          sqlStatement.RollbackTransaction();
          return false;
        }
      }

      if (GetInstallationDatabase().GetPrintJobNumberTable().GetFirstRow(null, null) == null)
      {
        PrintJobNumberRow printJobNumberRow = (PrintJobNumberRow) (GetInstallationDatabase().GetPrintJobNumberTable().CreateRow());
        printJobNumberRow.Set_installation_id(installationRow.Get_installation_id());
        if (printJobNumberRow.Insert() == false)
        {
          sqlStatement.RollbackTransaction();
          return false;
        }
      }
  
      if (GetInstallationDatabase().GetUserAccountNumberTable().GetFirstRow(null, null) == null)
      {
        UserAccountNumberRow userAccountNumberRow = (UserAccountNumberRow) (GetInstallationDatabase().GetUserAccountNumberTable().CreateRow());
        userAccountNumberRow.Set_installation_id(installationRow.Get_installation_id());
        if (userAccountNumberRow.Insert() == false)
        {
          sqlStatement.RollbackTransaction();
          return false;
        }
      }
  
      sqlStatement.CommitTransaction();
    }

    return true;
  }

  /**
    this operation deletes everything associated with the installation from
    the installation database
  */
  @Override
  public boolean RemoveRow(AbstractSQL1Row row)
  {
    InstallationRow installationRow = (InstallationRow) row;
    int rc = 0;
    try
    (SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.BeginTransaction();
  
      HashMap<String, AbstractSQL1Table> tablesMap = GetInstallationDatabase().GetTablesMap();
      Collection<AbstractSQL1Table> tablesCollection = tablesMap.values();
      Iterator<AbstractSQL1Table> tableIterator = tablesCollection.iterator();
      while ((rc >= 0) && (tableIterator.hasNext() == true))
      {
        AbstractSQL1Table sqlTable = (tableIterator.next());
        String sqlString = "delete from " + sqlTable.GetTableName() + " " + "where " + InstallationRow.Name_installation_id() + " = '" + installationRow.Get_installation_id() + "'";
        rc = sqlStatement.ExecuteUpdate(sqlString);
        if (rc < 0)
        {
          DebugWindow.DisplayText(sqlString);
          DebugWindow.DisplayText("sql rc=" + rc);
        }
      }
  
      sqlStatement.CommitTransaction();
    }

    return (rc >= 0);
  }

  public static boolean StateOK()
  {
    if (GetCachedInstallationRow() == null)
    {
      JOptionPane.showMessageDialog(null, "Your installation is not in the database.", "Installation Not Found", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (GetCachedInstallationRow().StatusIsEnabled() == false)
    {
      JOptionPane.showMessageDialog(null, "Your installation is not enabled.", "Installation Not Enabled", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (GetCachedInstallationRow().InterimLicenseOK() == false)
    {
      JOptionPane.showMessageDialog(null, "Your interim license has expired.", "Interim License Expired", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    if (GetCachedInstallationRow().LicenseOK() == false)
    {
      JOptionPane.showMessageDialog(null, "Your license has expired.", "License Expired", JOptionPane.ERROR_MESSAGE);
      return false;
    }

    return true;
  }

  public static InstallationRow GetCachedInstallationRow()
  {
    if ((__cachedInstallationRow == null) || (__cachedInstallationRow.GetSQLTable().GetSQLDatabase() != InstallationDatabase.GetDefaultInstallationDatabase()))
    {
      __cachedInstallationRow = (InstallationRow) (InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable().GetFirstRow(null, null));
    }
    return __cachedInstallationRow;
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(InstallationRow.Name_installation_name()) == true)
    {
      return 60;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = InstallationRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
