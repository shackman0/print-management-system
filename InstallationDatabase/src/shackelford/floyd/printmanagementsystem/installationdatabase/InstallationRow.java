package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;




/**
 * <p>Title: InstallationRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationRow
  extends AbstractSQL4Row
{

  private Integer                 _sql_installation_id;
  private String                  _sql_installation_name;
  private String                  _sql_installation_passphrase;
  private String                  _sql_smtp_host;
  private String                  _sql_administrator_e_mail_address;
  private String                  _sql_upgrade_preference;
  private _GregorianCalendar      _sql_interim_expiration_date;
  private _GregorianCalendar      _sql_license_expiration_date;
  private Integer                 _sql_max_number_of_clients;
  private Integer                 _sql_max_number_of_gatekeepers;
  private Integer                 _sql_current_release_number;
  private String                  _sql_language;
  private String                  _sql_egold_charge_url;
  private String                  _sql_egold_charge_account;
  private String                  _sql_egold_charge_passphrase;
  private Double                  _sql_egold_charge_minimum_amount;
  private Double                  _sql_egold_charge_xaction_fee;
  private Double                  _sql_egold_charge_xaction_fee_pcent;
  private String                  _sql_egold_refund_url;
  private String                  _sql_egold_refund_account;
  private String                  _sql_egold_refund_passphrase;
  private Double                  _sql_egold_refund_xaction_fee;
  private Double                  _sql_egold_refund_xaction_fee_pcent;
  private String                  _sql_paypal_charge_url;
  private String                  _sql_paypal_charge_account;
  private String                  _sql_paypal_charge_passphrase;
  private Double                  _sql_paypal_charge_minimum_amount;
  private Double                  _sql_paypal_charge_xaction_fee;
  private Double                  _sql_paypal_charge_xaction_fee_pcent;
  private String                  _sql_paypal_refund_url;
  private String                  _sql_paypal_refund_account;
  private String                  _sql_paypal_refund_passphrase;
  private Double                  _sql_paypal_refund_xaction_fee;
  private Double                  _sql_paypal_refund_xaction_fee_pcent;
  private String                  _sql_cc_charge_url;
  private String                  _sql_cc_charge_account;
  private String                  _sql_cc_charge_passphrase;
  private Double                  _sql_cc_charge_minimum_amount;
  private Double                  _sql_cc_charge_xaction_fee;
  private Double                  _sql_cc_charge_xaction_fee_pcent;
  private String                  _sql_cc_refund_url;
  private String                  _sql_cc_refund_account;
  private String                  _sql_cc_refund_passphrase;
  private Double                  _sql_cc_refund_xaction_fee;
  private Double                  _sql_cc_refund_xaction_fee_pcent;
  private String                  _sql_currency_symbol;
  private String                  _sql_thousands_separator;
  private String                  _sql_decimal_separator;

  private static DecimalFormat    __formatter = new DecimalFormat("#,##0.00");

  public static final String      UPDATE_AUTOMATIC_CODE = "a";
  public static final String      UPDATE_MANUAL_CODE = "m";
  public static final String      UPDATE_AUTOMATIC_NAME = "Automatic";
  public static final String      UPDATE_MANUAL_NAME = "Manual";
  public static final String      PERIOD = ".";
  public static final String      COMMA = ",";

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_installation_id(),
                                    Name_installation_name(),
                                    Name_installation_passphrase(),
                                    Name_smtp_host(),
                                    Name_administrator_e_mail_address(),
                                    Name_upgrade_preference(),
                                    Name_interim_expiration_date(),
                                    Name_license_expiration_date(),
                                    Name_max_number_of_clients(),
                                    Name_max_number_of_gatekeepers(),
                                    Name_current_release_number(),
                                    Name_language(),
                                    Name_egold_charge_url(),
                                    Name_egold_charge_account(),
                                    Name_egold_charge_passphrase(),
                                    Name_egold_charge_minimum_amount(),
                                    Name_egold_charge_xaction_fee(),
                                    Name_egold_charge_xaction_fee_pcent(),
                                    Name_egold_refund_url(),
                                    Name_egold_refund_account(),
                                    Name_egold_refund_passphrase(),
                                    Name_egold_refund_xaction_fee(),
                                    Name_egold_refund_xaction_fee_pcent(),
                                    Name_paypal_charge_url(),
                                    Name_paypal_charge_account(),
                                    Name_paypal_charge_passphrase(),
                                    Name_paypal_charge_minimum_amount(),
                                    Name_paypal_charge_xaction_fee(),
                                    Name_paypal_charge_xaction_fee_pcent(),
                                    Name_paypal_refund_url(),
                                    Name_paypal_refund_account(),
                                    Name_paypal_refund_passphrase(),
                                    Name_paypal_refund_xaction_fee(),
                                    Name_paypal_refund_xaction_fee_pcent(),
                                    Name_cc_charge_url(),
                                    Name_cc_charge_account(),
                                    Name_cc_charge_passphrase(),
                                    Name_cc_charge_minimum_amount(),
                                    Name_cc_charge_xaction_fee(),
                                    Name_cc_charge_xaction_fee_pcent(),
                                    Name_cc_refund_url(),
                                    Name_cc_refund_account(),
                                    Name_cc_refund_passphrase(),
                                    Name_cc_refund_xaction_fee(),
                                    Name_cc_refund_xaction_fee_pcent(),
                                    Name_currency_symbol(),
                                    Name_thousands_separator(),
                                    Name_decimal_separator(),

                                    Name_status(),
                                    Name_created_dts(),
                                    Name_updated_dts() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id() };

  public InstallationRow()
  {
    super();
  }

  public InstallationRow(InstallationTable table)
  {
    super(table);
  }

  public InstallationRow( InstallationTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public InstallationRow( InstallationTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();

    Set_installation_id(GetInstallationDatabase().GetInstallationID());

    Set_upgrade_preference(UPDATE_MANUAL_NAME);

    _GregorianCalendar calendar = new _GregorianCalendar();
    calendar.add(Calendar.MONTH,2);
    Set_interim_expiration_date(calendar);

    calendar = new _GregorianCalendar();
    calendar.add(Calendar.MONTH,13);
    Set_license_expiration_date(calendar);

    Set_language(Resources.ENGLISH_US);

    Set_max_number_of_clients(100);
    Set_max_number_of_gatekeepers(10);

    Set_current_release_number(GlobalAttributes.AUTOMATIC_UPGRADE_NUMBER);

    Set_egold_charge_url("https://www.e-gold.com/");
    Set_egold_charge_account("");
    Set_egold_charge_passphrase("");
    Set_egold_charge_minimum_amount(5.00);
    Set_egold_charge_xaction_fee(0.50);
    Set_egold_charge_xaction_fee_pcent(1.29);

    Set_egold_refund_url("https://www.e-gold.com/");
    Set_egold_refund_account("");
    Set_egold_refund_passphrase("");
    Set_egold_refund_xaction_fee(0.50);
    Set_egold_refund_xaction_fee_pcent(1.29);

    Set_paypal_charge_url("https://www.paypal.com/");
    Set_paypal_charge_account("");
    Set_paypal_charge_passphrase("");
    Set_paypal_charge_minimum_amount(5.00);
    Set_paypal_charge_xaction_fee(0.50);
    Set_paypal_charge_xaction_fee_pcent(1.29);

    Set_paypal_refund_url("https://www.paypal.com/");
    Set_paypal_refund_account("");
    Set_paypal_refund_passphrase("");
    Set_paypal_refund_xaction_fee(0.50);
    Set_paypal_refund_xaction_fee_pcent(1.29);

    Set_cc_charge_url("https://cart.bamart.com/");
    Set_cc_charge_account("");
    Set_cc_charge_passphrase("");
    Set_cc_charge_minimum_amount(5.00);
    Set_cc_charge_xaction_fee(1.50);
    Set_cc_charge_xaction_fee_pcent(2.39);

    Set_cc_refund_url("https://cart.bamart.com/");
    Set_cc_refund_account("");
    Set_cc_refund_passphrase("");
    Set_cc_refund_xaction_fee(1.50);
    Set_cc_refund_xaction_fee_pcent(2.39);

    Set_thousands_separator(COMMA);
    Set_decimal_separator(PERIOD);
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public String Get_installation_name() { return new String(_sql_installation_name); }
  public void Set_installation_name(String value) { _sql_installation_name = new String(value); }
  public static String Name_installation_name() { return "installation_name"; }

  public String Get_installation_passphrase() { return new String(_sql_installation_passphrase); }
  public void Set_installation_passphrase(String value) { _sql_installation_passphrase = new String(value); }
  public static String Name_installation_passphrase() { return "installation_passphrase"; }

  public String Get_smtp_host() { return new String(_sql_smtp_host); }
  public void Set_smtp_host(String value) { _sql_smtp_host = new String(value); }
  public static String Name_smtp_host() { return "smtp_host"; }

  public String Get_administrator_e_mail_address() { return new String(_sql_administrator_e_mail_address); }
  public void Set_administrator_e_mail_address(String value) { _sql_administrator_e_mail_address = new String(value); }
  public static String Name_administrator_e_mail_address() { return "administrator_e_mail_address"; }

  public String Get_upgrade_preference()
  {
    String value = null;
    if (_sql_upgrade_preference.equals(UPDATE_MANUAL_CODE) == true)
    {
      value = UPDATE_MANUAL_NAME;
    }
    else
    if (_sql_upgrade_preference.equals(UPDATE_AUTOMATIC_CODE) == true)
    {
      value = UPDATE_AUTOMATIC_NAME;
    }
    else
    {
      new Exception("Get_upgrade_preference:Invalid _sql_upgrade_preference=\""+_sql_upgrade_preference+"\"").printStackTrace();
      value = new String(_sql_upgrade_preference);       // fail safe
    }
    return value;
  }
  public void Set_upgrade_preference(String value)
  {
    if (value.equals(UPDATE_MANUAL_NAME) == true)
    {
      _sql_upgrade_preference = UPDATE_MANUAL_CODE;
    }
    else
    if (value.equals(UPDATE_AUTOMATIC_NAME) == true)
    {
      _sql_upgrade_preference = UPDATE_AUTOMATIC_CODE;
    }
    else
    {
      if (value.length() > 0)
      {
        new Exception("Set_upgrade_preference:Invalid upgrade_preference=\""+value+"\"").printStackTrace();
      }
      _sql_upgrade_preference = new String(value);        // fail safe
    }
  }
  public String GetInternal_upgrade_preference() { return new String(_sql_upgrade_preference); }
  public static String Name_upgrade_preference() { return "upgrade_preference"; }
  public boolean UpgradePreferenceIsManual()
  {
    return _sql_upgrade_preference.equals(UPDATE_MANUAL_CODE) == true;
  }
  public boolean UpgradePreferenceIsAutomatic()
  {
    return _sql_upgrade_preference.equals(UPDATE_AUTOMATIC_CODE) == true;
  }

  public _GregorianCalendar Get_interim_expiration_date() { return new _GregorianCalendar(_sql_interim_expiration_date); }
  public void Set_interim_expiration_date(_GregorianCalendar value) { _sql_interim_expiration_date = new _GregorianCalendar(value); }
  public static String Name_interim_expiration_date() { return "interim_expiration_date"; }
  public boolean InterimLicenseOK()
  {
    return (DaysUntilInterimLicenseExpiration() > 0);
  }
  public int DaysUntilInterimLicenseExpiration()
  {
    _GregorianCalendar today = new _GregorianCalendar();
    return today.DaysBefore(_sql_interim_expiration_date);
  }

  public _GregorianCalendar Get_license_expiration_date() { return new _GregorianCalendar(_sql_license_expiration_date); }
  public void Set_license_expiration_date(_GregorianCalendar value) { _sql_license_expiration_date = new _GregorianCalendar(value); }
  public static String Name_license_expiration_date() { return "license_expiration_date"; }
  public boolean LicenseOK()
  {
    return (DaysUntilLicenseExpiration() > 0);
  }
  public int DaysUntilLicenseExpiration()
  {
    _GregorianCalendar today = new _GregorianCalendar();
    return today.DaysBefore(_sql_license_expiration_date);
  }

  public Integer Get_max_number_of_clients() { return new Integer(_sql_max_number_of_clients.intValue()); }
  public void Set_max_number_of_clients(Integer value) { _sql_max_number_of_clients = new Integer(value.intValue()); }
  public void Set_max_number_of_clients(String value) { _sql_max_number_of_clients = new Integer(value); }
  public void Set_max_number_of_clients(int value) { _sql_max_number_of_clients = new Integer(value); }
  public static String Name_max_number_of_clients() { return "max_number_of_clients"; }

  public Integer Get_max_number_of_gatekeepers() { return new Integer(_sql_max_number_of_gatekeepers.intValue()); }
  public void Set_max_number_of_gatekeepers(Integer value) { _sql_max_number_of_gatekeepers = new Integer(value.intValue()); }
  public void Set_max_number_of_gatekeepers(String value) { _sql_max_number_of_gatekeepers = new Integer(value); }
  public void Set_max_number_of_gatekeepers(int value) { _sql_max_number_of_gatekeepers = new Integer(value); }
  public static String Name_max_number_of_gatekeepers() { return "max_number_of_gatekeepers"; }

  public Integer Get_current_release_number() { return new Integer(_sql_current_release_number.intValue()); }
  public void Set_current_release_number(Integer value) { _sql_current_release_number = new Integer(value.intValue()); }
  public void Set_current_release_number(String value) { _sql_current_release_number = new Integer(value); }
  public void Set_current_release_number(int value) { _sql_current_release_number = new Integer(value); }
  public static String Name_current_release_number() { return "current_release_number"; }

  public String Get_language() { return new String(_sql_language); }
  public void Set_language(String value) { _sql_language = new String(value); }
  public static String Name_language() { return "language"; }

  public String Get_egold_charge_url() { return new String(_sql_egold_charge_url); }
  public void Set_egold_charge_url(String value) { _sql_egold_charge_url = new String(value); }
  public static String Name_egold_charge_url() { return "egold_charge_url"; }

  public String Get_egold_charge_account() { return new String(_sql_egold_charge_account); }
  public void Set_egold_charge_account(String value) { _sql_egold_charge_account = new String(value); }
  public static String Name_egold_charge_account() { return "egold_charge_account"; }

  public String Get_egold_charge_passphrase() { return new String(_sql_egold_charge_passphrase); }
  public void Set_egold_charge_passphrase(String value) { _sql_egold_charge_passphrase = new String(value); }
  public static String Name_egold_charge_passphrase() { return "egold_charge_passphrase"; }

  public Double Get_egold_charge_minimum_amount() { return new Double(_sql_egold_charge_minimum_amount.doubleValue()); }
  public String GetFormatted_egold_charge_minimum_amount() { return __formatter.format(_sql_egold_charge_minimum_amount); }
  public void Set_egold_charge_minimum_amount(Double value) { _sql_egold_charge_minimum_amount = new Double(value.doubleValue()); }
  public void Set_egold_charge_minimum_amount(String value) { _sql_egold_charge_minimum_amount = new Double(value); }
  public void Set_egold_charge_minimum_amount(double value) { _sql_egold_charge_minimum_amount = new Double(value); }
  public static String Name_egold_charge_minimum_amount() { return "egold_charge_minimum_amount"; }

  public Double Get_egold_charge_xaction_fee() { return new Double(_sql_egold_charge_xaction_fee.doubleValue()); }
  public String GetFormatted_egold_charge_xaction_fee() { return __formatter.format(_sql_egold_charge_xaction_fee); }
  public void Set_egold_charge_xaction_fee(Double value) { _sql_egold_charge_xaction_fee = new Double(value.doubleValue()); }
  public void Set_egold_charge_xaction_fee(String value) { _sql_egold_charge_xaction_fee = new Double(value); }
  public void Set_egold_charge_xaction_fee(double value) { _sql_egold_charge_xaction_fee = new Double(value); }
  public static String Name_egold_charge_xaction_fee() { return "egold_charge_xaction_fee"; }

  public Double Get_egold_charge_xaction_fee_pcent() { return new Double(_sql_egold_charge_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_egold_charge_xaction_fee_pcent() { return __formatter.format(_sql_egold_charge_xaction_fee_pcent); }
  public void Set_egold_charge_xaction_fee_pcent(Double value) { _sql_egold_charge_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_egold_charge_xaction_fee_pcent(String value) { _sql_egold_charge_xaction_fee_pcent = new Double(value); }
  public void Set_egold_charge_xaction_fee_pcent(double value) { _sql_egold_charge_xaction_fee_pcent = new Double(value); }
  public static String Name_egold_charge_xaction_fee_pcent() { return "egold_charge_xaction_fee_pcent"; }

  public String Get_egold_refund_url() { return new String(_sql_egold_refund_url); }
  public void Set_egold_refund_url(String value) { _sql_egold_refund_url = new String(value); }
  public static String Name_egold_refund_url() { return "egold_refund_url"; }

  public String Get_egold_refund_account() { return new String(_sql_egold_refund_account); }
  public void Set_egold_refund_account(String value) { _sql_egold_refund_account = new String(value); }
  public static String Name_egold_refund_account() { return "egold_refund_account"; }

  public String Get_egold_refund_passphrase() { return new String(_sql_egold_refund_passphrase); }
  public void Set_egold_refund_passphrase(String value) { _sql_egold_refund_passphrase = new String(value); }
  public static String Name_egold_refund_passphrase() { return "egold_refund_passphrase"; }

  public Double Get_egold_refund_xaction_fee() { return new Double(_sql_egold_refund_xaction_fee.doubleValue()); }
  public String GetFormatted_egold_refund_xaction_fee() { return __formatter.format(_sql_egold_refund_xaction_fee); }
  public void Set_egold_refund_xaction_fee(Double value) { _sql_egold_refund_xaction_fee = new Double(value.doubleValue()); }
  public void Set_egold_refund_xaction_fee(String value) { _sql_egold_refund_xaction_fee = new Double(value); }
  public void Set_egold_refund_xaction_fee(double value) { _sql_egold_refund_xaction_fee = new Double(value); }
  public static String Name_egold_refund_xaction_fee() { return "egold_refund_xaction_fee"; }

  public Double Get_egold_refund_xaction_fee_pcent() { return new Double(_sql_egold_refund_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_egold_refund_xaction_fee_pcent() { return __formatter.format(_sql_egold_refund_xaction_fee_pcent); }
  public void Set_egold_refund_xaction_fee_pcent(Double value) { _sql_egold_refund_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_egold_refund_xaction_fee_pcent(String value) { _sql_egold_refund_xaction_fee_pcent = new Double(value); }
  public void Set_egold_refund_xaction_fee_pcent(double value) { _sql_egold_refund_xaction_fee_pcent = new Double(value); }
  public static String Name_egold_refund_xaction_fee_pcent() { return "egold_refund_xaction_fee_pcent"; }

  public String Get_paypal_charge_url() { return new String(_sql_paypal_charge_url); }
  public void Set_paypal_charge_url(String value) { _sql_paypal_charge_url = new String(value); }
  public static String Name_paypal_charge_url() { return "paypal_charge_url"; }

  public String Get_paypal_charge_account() { return new String(_sql_paypal_charge_account); }
  public void Set_paypal_charge_account(String value) { _sql_paypal_charge_account = new String(value); }
  public static String Name_paypal_charge_account() { return "paypal_charge_account"; }

  public String Get_paypal_charge_passphrase() { return new String(_sql_paypal_charge_passphrase); }
  public void Set_paypal_charge_passphrase(String value) { _sql_paypal_charge_passphrase = new String(value); }
  public static String Name_paypal_charge_passphrase() { return "paypal_charge_passphrase"; }

  public Double Get_paypal_charge_minimum_amount() { return new Double(_sql_paypal_charge_minimum_amount.doubleValue()); }
  public String GetFormatted_paypal_charge_minimum_amount() { return __formatter.format(_sql_paypal_charge_minimum_amount); }
  public void Set_paypal_charge_minimum_amount(Double value) { _sql_paypal_charge_minimum_amount = new Double(value.doubleValue()); }
  public void Set_paypal_charge_minimum_amount(String value) { _sql_paypal_charge_minimum_amount = new Double(value); }
  public void Set_paypal_charge_minimum_amount(double value) { _sql_paypal_charge_minimum_amount = new Double(value); }
  public static String Name_paypal_charge_minimum_amount() { return "paypal_charge_minimum_amount"; }

  public Double Get_paypal_charge_xaction_fee() { return new Double(_sql_paypal_charge_xaction_fee.doubleValue()); }
  public String GetFormatted_paypal_charge_xaction_fee() { return __formatter.format(_sql_paypal_charge_xaction_fee); }
  public void Set_paypal_charge_xaction_fee(Double value) { _sql_paypal_charge_xaction_fee = new Double(value.doubleValue()); }
  public void Set_paypal_charge_xaction_fee(String value) { _sql_paypal_charge_xaction_fee = new Double(value); }
  public void Set_paypal_charge_xaction_fee(double value) { _sql_paypal_charge_xaction_fee = new Double(value); }
  public static String Name_paypal_charge_xaction_fee() { return "paypal_charge_xaction_fee"; }

  public Double Get_paypal_charge_xaction_fee_pcent() { return new Double(_sql_paypal_charge_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_paypal_charge_xaction_fee_pcent() { return __formatter.format(_sql_paypal_charge_xaction_fee_pcent); }
  public void Set_paypal_charge_xaction_fee_pcent(Double value) { _sql_paypal_charge_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_paypal_charge_xaction_fee_pcent(String value) { _sql_paypal_charge_xaction_fee_pcent = new Double(value); }
  public void Set_paypal_charge_xaction_fee_pcent(double value) { _sql_paypal_charge_xaction_fee_pcent = new Double(value); }
  public static String Name_paypal_charge_xaction_fee_pcent() { return "paypal_charge_xaction_fee_pcent"; }

  public String Get_paypal_refund_url() { return new String(_sql_paypal_refund_url); }
  public void Set_paypal_refund_url(String value) { _sql_paypal_refund_url = new String(value); }
  public static String Name_paypal_refund_url() { return "paypal_refund_url"; }

  public String Get_paypal_refund_account() { return new String(_sql_paypal_refund_account); }
  public void Set_paypal_refund_account(String value) { _sql_paypal_refund_account = new String(value); }
  public static String Name_paypal_refund_account() { return "paypal_refund_account"; }

  public String Get_paypal_refund_passphrase() { return new String(_sql_paypal_refund_passphrase); }
  public void Set_paypal_refund_passphrase(String value) { _sql_paypal_refund_passphrase = new String(value); }
  public static String Name_paypal_refund_passphrase() { return "paypal_refund_passphrase"; }

  public Double Get_paypal_refund_xaction_fee() { return new Double(_sql_paypal_refund_xaction_fee.doubleValue()); }
  public String GetFormatted_paypal_refund_xaction_fee() { return __formatter.format(_sql_paypal_refund_xaction_fee); }
  public void Set_paypal_refund_xaction_fee(Double value) { _sql_paypal_refund_xaction_fee = new Double(value.doubleValue()); }
  public void Set_paypal_refund_xaction_fee(String value) { _sql_paypal_refund_xaction_fee = new Double(value); }
  public void Set_paypal_refund_xaction_fee(double value) { _sql_paypal_refund_xaction_fee = new Double(value); }
  public static String Name_paypal_refund_xaction_fee() { return "paypal_refund_xaction_fee"; }

  public Double Get_paypal_refund_xaction_fee_pcent() { return new Double(_sql_paypal_refund_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_paypal_refund_xaction_fee_pcent() { return __formatter.format(_sql_paypal_refund_xaction_fee_pcent); }
  public void Set_paypal_refund_xaction_fee_pcent(Double value) { _sql_paypal_refund_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_paypal_refund_xaction_fee_pcent(String value) { _sql_paypal_refund_xaction_fee_pcent = new Double(value); }
  public void Set_paypal_refund_xaction_fee_pcent(double value) { _sql_paypal_refund_xaction_fee_pcent = new Double(value); }
  public static String Name_paypal_refund_xaction_fee_pcent() { return "paypal_refund_xaction_fee_pcent"; }

  public String Get_cc_charge_url() { return new String(_sql_cc_charge_url); }
  public void Set_cc_charge_url(String value) { _sql_cc_charge_url = new String(value); }
  public static String Name_cc_charge_url() { return "cc_charge_url"; }

  public String Get_cc_charge_account() { return new String(_sql_cc_charge_account); }
  public void Set_cc_charge_account(String value) { _sql_cc_charge_account = new String(value); }
  public static String Name_cc_charge_account() { return "cc_charge_account"; }

  public String Get_cc_charge_passphrase() { return new String(_sql_cc_charge_passphrase); }
  public void Set_cc_charge_passphrase(String value) { _sql_cc_charge_passphrase = new String(value); }
  public static String Name_cc_charge_passphrase() { return "cc_charge_passphrase"; }

  public Double Get_cc_charge_minimum_amount() { return new Double(_sql_cc_charge_minimum_amount.doubleValue()); }
  public String GetFormatted_cc_charge_minimum_amount() { return __formatter.format(_sql_cc_charge_minimum_amount); }
  public void Set_cc_charge_minimum_amount(Double value) { _sql_cc_charge_minimum_amount = new Double(value.doubleValue()); }
  public void Set_cc_charge_minimum_amount(String value) { _sql_cc_charge_minimum_amount = new Double(value); }
  public void Set_cc_charge_minimum_amount(double value) { _sql_cc_charge_minimum_amount = new Double(value); }
  public static String Name_cc_charge_minimum_amount() { return "cc_charge_minimum_amount"; }

  public Double Get_cc_charge_xaction_fee() { return new Double(_sql_cc_charge_xaction_fee.doubleValue()); }
  public String GetFormatted_cc_charge_xaction_fee() { return __formatter.format(_sql_cc_charge_xaction_fee); }
  public void Set_cc_charge_xaction_fee(Double value) { _sql_cc_charge_xaction_fee = new Double(value.doubleValue()); }
  public void Set_cc_charge_xaction_fee(String value) { _sql_cc_charge_xaction_fee = new Double(value); }
  public void Set_cc_charge_xaction_fee(double value) { _sql_cc_charge_xaction_fee = new Double(value); }
  public static String Name_cc_charge_xaction_fee() { return "cc_charge_xaction_fee"; }

  public Double Get_cc_charge_xaction_fee_pcent() { return new Double(_sql_cc_charge_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_cc_charge_xaction_fee_pcent() { return __formatter.format(_sql_cc_charge_xaction_fee_pcent); }
  public void Set_cc_charge_xaction_fee_pcent(Double value) { _sql_cc_charge_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_cc_charge_xaction_fee_pcent(String value) { _sql_cc_charge_xaction_fee_pcent = new Double(value); }
  public void Set_cc_charge_xaction_fee_pcent(double value) { _sql_cc_charge_xaction_fee_pcent = new Double(value); }
  public static String Name_cc_charge_xaction_fee_pcent() { return "cc_charge_xaction_fee_pcent"; }

  public String Get_cc_refund_url() { return new String(_sql_cc_refund_url); }
  public void Set_cc_refund_url(String value) { _sql_cc_refund_url = new String(value); }
  public static String Name_cc_refund_url() { return "cc_refund_url"; }

  public String Get_cc_refund_account() { return new String(_sql_cc_refund_account); }
  public void Set_cc_refund_account(String value) { _sql_cc_refund_account = new String(value); }
  public static String Name_cc_refund_account() { return "cc_refund_account"; }

  public String Get_cc_refund_passphrase() { return new String(_sql_cc_refund_passphrase); }
  public void Set_cc_refund_passphrase(String value) { _sql_cc_refund_passphrase = new String(value); }
  public static String Name_cc_refund_passphrase() { return "cc_refund_passphrase"; }

  public Double Get_cc_refund_xaction_fee() { return new Double(_sql_cc_refund_xaction_fee.doubleValue()); }
  public String GetFormatted_cc_refund_xaction_fee() { return __formatter.format(_sql_cc_refund_xaction_fee); }
  public void Set_cc_refund_xaction_fee(Double value) { _sql_cc_refund_xaction_fee = new Double(value.doubleValue()); }
  public void Set_cc_refund_xaction_fee(String value) { _sql_cc_refund_xaction_fee = new Double(value); }
  public void Set_cc_refund_xaction_fee(double value) { _sql_cc_refund_xaction_fee = new Double(value); }
  public static String Name_cc_refund_xaction_fee() { return "cc_refund_xaction_fee"; }

  public Double Get_cc_refund_xaction_fee_pcent() { return new Double(_sql_cc_refund_xaction_fee_pcent.doubleValue()); }
  public String GetFormatted_cc_refund_xaction_fee_pcent() { return __formatter.format(_sql_cc_refund_xaction_fee_pcent); }
  public void Set_cc_refund_xaction_fee_pcent(Double value) { _sql_cc_refund_xaction_fee_pcent = new Double(value.doubleValue()); }
  public void Set_cc_refund_xaction_fee_pcent(String value) { _sql_cc_refund_xaction_fee_pcent = new Double(value); }
  public void Set_cc_refund_xaction_fee_pcent(double value) { _sql_cc_refund_xaction_fee_pcent = new Double(value); }
  public static String Name_cc_refund_xaction_fee_pcent() { return "cc_refund_xaction_fee_pcent"; }

  public String Get_currency_symbol() { return new String(_sql_currency_symbol); }
  public void Set_currency_symbol(String value) { _sql_currency_symbol = new String(value); }
  public static String Name_currency_symbol() { return "currency_symbol"; }

  public String Get_thousands_separator() { return new String(_sql_thousands_separator); }
  public void Set_thousands_separator(String value) { _sql_thousands_separator = new String(value); }
  public static String Name_thousands_separator() { return "thousands_separator"; }
  public boolean ThousandsSeparatorIsPeriod()
  {
    return _sql_thousands_separator.equals(PERIOD) == true;
  }
  public boolean ThousandsSeparatorIsComma()
  {
    return _sql_thousands_separator.equals(COMMA) == true;
  }

  public String Get_decimal_separator() { return new String(_sql_decimal_separator); }
  public void Set_decimal_separator(String value) { _sql_decimal_separator = new String(value); }
  public static String Name_decimal_separator() { return "decimal_separator"; }
  public boolean DecimalSeparatorIsPeriod()
  {
    return _sql_decimal_separator.equals(PERIOD) == true;
  }
  public boolean DecimalSeparatorIsComma()
  {
    return _sql_decimal_separator.equals(COMMA) == true;
  }


  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(InstallationRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}