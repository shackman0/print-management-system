package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: GatekeeperTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GatekeeperTable
  extends AbstractSQL2Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "gatekeeper_table";

  public GatekeeperTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, GatekeeperRow.class);
  }

  protected GatekeeperTable(AbstractSQLDatabase sqlDatabase, String tableName, Class<? extends AbstractSQL1Row> rowClass)
  {
    super(sqlDatabase, tableName, rowClass);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public GatekeeperRow GetRowForIPAddress(String ipAddress)
  {
    String predicate = "( " + GatekeeperRow.Name_ip_address() + " = '" + ipAddress.trim() + "' )";
    return (GatekeeperRow) GetFirstRow(predicate, null);
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(GatekeeperRow.Name_ip_address()) == true)
    {
      return 60;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = GatekeeperRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
