package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: ClientRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ClientRow
  extends GatekeeperRow
{

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_ip_address(),

                                    Name_created_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_ip_address() };

  public ClientRow()
  {
    super();
  }

  public ClientRow(ClientTable table)
  {
    super(table);
  }

  public ClientRow( ClientTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public ClientRow( ClientTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(ClientRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}