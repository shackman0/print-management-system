package shackelford.floyd.printmanagementsystem.installationdatabase;

import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;




/**
 * <p>Title: InstallationDatabase</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InstallationDatabase
  extends AbstractSQLDatabase
{

  private Integer                           _installationID;
  private ClientTable                       _clientTable;
  private GatekeeperTable                   _gatekeeperTable;
  private InstallationTable                 _installationTable;
  private PageChargeNumberTable             _pageChargeNumberTable;
  private PageChargesTable                  _pageChargesTable;
  private PrePaymentHistoryTable            _prePaymentHistoryTable;
  private PrintJobHeaderTable               _printJobHeaderTable;
  private PrintJobNumberTable               _printJobNumberTable;
  private PrintJobOnHoldHeaderTable         _printJobOnHoldHeaderTable;
  private PrintJobPropertiesTable           _printJobPropertiesTable;
  private RulesTable                        _rulesTable;
  private UserAccountNumberTable            _userAccountNumberTable;
  private UserAccountTable                  _userAccountTable;

  private static InstallationDatabase       __defaultInstallationDatabase = null;

  public static final String                DATABASE_NAME = "installation_database";
  public static final String                DATABASE_PORT = "5432";
  public static final String                DATABASE_PROGRAM = "postgresql";

  /**
    this constructor creates a connection to the database.
    be sure to call the done() method when you are done with this instance.
  */

  public InstallationDatabase (
    String   databaseAddress,      // ipaddr or hostname
    String   databaseUserID,
    String   databasePassword,
    Integer  installationID )
  {
    super (
      DATABASE_PROGRAM,
      databaseAddress,
      DATABASE_PORT,
      DATABASE_NAME,
      databaseUserID,
      databasePassword );

    _installationID = installationID;

    _clientTable = new ClientTable(this);
    _tablesMap.put(ClientTable.TABLE_NAME,_clientTable);

    _gatekeeperTable = new GatekeeperTable(this);
    _tablesMap.put(GatekeeperTable.TABLE_NAME,_gatekeeperTable);

    _installationTable = new InstallationTable(this);
    _tablesMap.put(InstallationTable.TABLE_NAME,_installationTable);

    _pageChargeNumberTable = new PageChargeNumberTable(this);
    _tablesMap.put(PageChargeNumberTable.TABLE_NAME,_pageChargeNumberTable);

    _pageChargesTable = new PageChargesTable(this);
    _tablesMap.put(PageChargesTable.TABLE_NAME,_pageChargesTable);

    _prePaymentHistoryTable = new PrePaymentHistoryTable(this);
    _tablesMap.put(PrePaymentHistoryTable.TABLE_NAME,_prePaymentHistoryTable);

    _printJobHeaderTable = new PrintJobHeaderTable(this);
    _tablesMap.put(PrintJobHeaderTable.TABLE_NAME,_printJobHeaderTable);

    _printJobNumberTable = new PrintJobNumberTable(this);
    _tablesMap.put(PrintJobNumberTable.TABLE_NAME,_printJobNumberTable);

    _printJobOnHoldHeaderTable = new PrintJobOnHoldHeaderTable(this);
    _tablesMap.put(PrintJobOnHoldHeaderTable.TABLE_NAME,_printJobOnHoldHeaderTable);

    _printJobPropertiesTable = new PrintJobPropertiesTable(this);
    _tablesMap.put(PrintJobPropertiesTable.TABLE_NAME,_printJobPropertiesTable);

    _rulesTable = new RulesTable(this);
    _tablesMap.put(RulesTable.TABLE_NAME,_rulesTable);

    _userAccountNumberTable = new UserAccountNumberTable(this);
    _tablesMap.put(UserAccountNumberTable.TABLE_NAME,_userAccountNumberTable);

    _userAccountTable = new UserAccountTable(this);
    _tablesMap.put(UserAccountTable.TABLE_NAME,_userAccountTable);

  }

  @Override
  public void ClearDefaultDatabase()
  {
    __defaultInstallationDatabase = null;
  }

  @Override
  public AbstractSQLDatabase GetDefaultDatabase()
  {
    return __defaultInstallationDatabase;
  }

  public static void ClearDefaultInstallationDatabase()
  {
    __defaultInstallationDatabase = null;
  }

  public static void SetDefaultInstallationDatabase(InstallationDatabase defaultDatabase)
  {
    __defaultInstallationDatabase = defaultDatabase;
  }

  public static InstallationDatabase GetDefaultInstallationDatabase()
  {
    return __defaultInstallationDatabase;
  }

  public String AddInstallationIDToPredicate(String predicate)
  {
    if (predicate == null)
    {
      predicate = "";
    }
    else
    if (predicate.trim().length() > 0)
    {
      predicate += " and ";
    }

    predicate += "( installation_id = '" + _installationID.toString() + "' )";

    return predicate;
  }

  public Integer GetInstallationID()
  {
    return _installationID;
  }

  public ClientTable GetClientTable()
  {
    return _clientTable;
  }
  public GatekeeperTable GetGatekeeperTable()
  {
    return _gatekeeperTable;
  }

  public InstallationTable GetInstallationTable()
  {
    return _installationTable;
  }

  public PageChargeNumberTable GetPageChargeNumberTable()
  {
    return _pageChargeNumberTable;
  }

  public PageChargesTable GetPageChargesTable()
  {
    return _pageChargesTable;
  }

  public PrePaymentHistoryTable GetPrePaymentHistoryTable()
  {
    return _prePaymentHistoryTable;
  }

  public PrintJobHeaderTable GetPrintJobHeaderTable()
  {
    return _printJobHeaderTable;
  }

  public PrintJobNumberTable GetPrintJobNumberTable()
  {
    return _printJobNumberTable;
  }

  public PrintJobOnHoldHeaderTable GetPrintJobOnHoldHeaderTable()
  {
    return _printJobOnHoldHeaderTable;
  }

  public PrintJobPropertiesTable GetPrintJobPropertiesTable()
  {
    return _printJobPropertiesTable;
  }

  public RulesTable GetRulesTable()
  {
    return _rulesTable;
  }

  public UserAccountTable GetUserAccountTable()
  {
    return _userAccountTable;
  }

  public UserAccountNumberTable GetUserAccountNumberTable()
  {
    return _userAccountNumberTable;
  }

}