package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: UserAccountTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountTable
  extends AbstractSQL4Table
{

  private static UserAccountRow          __cachedUserAccountRow = null;

  private static HashMap<String, String> __columnToViewNamesMap = null;

  private static HashMap<String, String> __viewToColumnNamesMap = null;

  private static Resources               __resources            = null;

  public static final String             TABLE_NAME             = "user_account_table";

  public UserAccountTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, UserAccountRow.class);
  }

  @Override
  protected void SetExcludeDisabledItemsPredicate()
  {
    super.SetExcludeDisabledItemsPredicate();
    _excludeDisabledItemsPredicate += " and ( " + _tableName + "." + AbstractSQL4Row.Name_status() + " <> '" + AbstractSQL4Row.STATUS_CLOSED_CODE + "' )";
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public UserAccountRow GetRowForUserAccountID(String userAccountID)
  {
    String predicate = UserAccountRow.Name_user_account_id() + " = '" + userAccountID.trim() + "'";
    return (UserAccountRow) GetFirstRow(predicate, null);
  }

  /**
    this operation removes a user account row from the user account table
  */
  public void RemoveRowForUserAccountID(String userAccountID)
  {
    RemoveRow(GetRowForUserAccountID(userAccountID));
  }

  public static void SetCachedUserAccountRow(String userAccountID)
  {
    if ((__cachedUserAccountRow == null) || (__cachedUserAccountRow.GetSQLTable().GetSQLDatabase() != InstallationDatabase.GetDefaultInstallationDatabase())
      || (__cachedUserAccountRow.Get_user_account_id().equals(userAccountID) == false))
    {
      __cachedUserAccountRow = InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable().GetRowForUserAccountID(userAccountID);
    }
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(UserAccountRow.Name_home_address()) == true)
    {
      return 100;
    }
    else
      if (columnName.equals(UserAccountRow.Name_user_name()) == true)
      {
        return 60;
      }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  public static UserAccountRow GetCachedUserAccountRow(String userAccountID)
  {
    SetCachedUserAccountRow(userAccountID);
    return __cachedUserAccountRow;
  }

  public static UserAccountRow GetCachedUserAccountRow()
  {
    return __cachedUserAccountRow;
  }

  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = UserAccountRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }

}
