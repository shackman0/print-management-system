package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;


/**
 * <p>Title: PrintJobHeaderTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobHeaderTable
  extends AbstractSQL2Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "print_job_header_table";

  public PrintJobHeaderTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, PrintJobHeaderRow.class);
  }

  protected PrintJobHeaderTable(AbstractSQLDatabase sqlDatabase, String tableName, Class<? extends AbstractSQL1Row> rowClass)
  {
    super(sqlDatabase, tableName, rowClass);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public PrintJobHeaderRow GetRowForPrintJobNumber(Integer printJobNumber)
  {
    String predicate = PrintJobHeaderRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
    return (PrintJobHeaderRow) GetFirstRow(predicate, null);
  }

  public void RemoveRowForPrintJobNumber(Integer printJobNumber)
  {
    String predicate = PrintJobHeaderRow.Name_print_job_number() + " = '" + printJobNumber.toString() + "'";
    RemoveRows(predicate);
  }

  /**
    this operation returns an array list of printers known by the system.
  */
  public String[] GetPrintersAsStringArray()
  {
    ArrayList<String> arrayList = GetPrintersAsArrayList();
    String[] stringArray = new String[arrayList.size()];
    arrayList.toArray(stringArray);
    return stringArray;
  }

  /**
    this operation returns an array list of printers known by the system.
    A printer name consisting of HighValues (i.e. all z's) is replaced by
    the string "Any Printer".
  */
  public ArrayList<String> GetPrintersAsArrayList()
  {
    ArrayList<String> arrayList = new ArrayList<>(50);

    try
    ( SQLStatement sqlStatement = GetPrintersAsResultSet();
      ResultSet resultSet = sqlStatement.GetResultSet();)
    {
      while (resultSet.next())
      {
        String printer = resultSet.getString(1);
        if (printer.equals(PageChargesRow.ANY_PRINTER_CODE) == true)
        {
          printer = PageChargesRow.ANY_PRINTER_NAME;
        }
        arrayList.add(new String(printer));
      }
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
    }

    arrayList.trimToSize();

    return arrayList;
  }

  /**
    this operation returns a list of printers known by the system
  */
  public SQLStatement GetPrintersAsResultSet()
  {
    SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();

    String sqlString = "select " + "DISTINCT " + PrintJobHeaderRow.Name_printer() + " " + "from " + TABLE_NAME + " " + "where " + PrintJobHeaderRow.Name_installation_id() + " = '"
      + GetInstallationDatabase().GetInstallationID().toString() + "' " + "order by 1";

    sqlStatement.ExecuteQuery(sqlString);

    return sqlStatement;
  }

  public void StoreThePrintJob(Properties printJobProperties)
  {
    PrintJobHeaderRow printJobHeaderRow = new PrintJobHeaderRow(this, printJobProperties);
    printJobHeaderRow.Insert();

    GetInstallationDatabase().GetPrintJobPropertiesTable().InsertRowsFromProperties(printJobHeaderRow, printJobProperties);
  }

  public int GetNumberOfPrintJobsForPeriod(_GregorianCalendar startDate, // inclusive
    _GregorianCalendar endDate) // inclusive
  {
    int count = 0;
    try
    ( SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "select count(*) " + "from " + GetTableName() + " " + "where " + PrintJobHeaderRow.Name_installation_id() + " = '" + GetInstallationDatabase().GetInstallationID().toString()
          + "' and " + AbstractSQL2Row.Name_created_dts() + " >= '" + startDate.GetDateNumericString() + "' and " + AbstractSQL2Row.Name_created_dts() + " <= '" + endDate.GetDateNumericString() + "' ";
      sqlStatement.ExecuteQuery(sqlString);
      try
      (ResultSet resultSet = sqlStatement.GetResultSet();)
      {
        resultSet.next();
        count = resultSet.getInt(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        MessageDialog.ShowErrorMessageDialog(null, excp, "resultSet.next() or resultSet.getInt(1)");
      }
    }

    return count;
  }

  public int GetNumberOfImpressionsForPeriod(_GregorianCalendar startDate, // inclusive
    _GregorianCalendar endDate) // inclusive
  {
    int count = 0;
    try
    (SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "select sum(" + PrintJobHeaderRow.Name_number_of_pages() + ") " + "from " + GetTableName() + " " + "where " + PrintJobHeaderRow.Name_installation_id() + " = '"
        + GetInstallationDatabase().GetInstallationID().toString() + "' and " + AbstractSQL2Row.Name_created_dts() + " >= '" + startDate.GetDateNumericString() + "' and "
        + AbstractSQL2Row.Name_created_dts() + " <= '" + endDate.GetDateNumericString() + "' ";
  
      sqlStatement.ExecuteQuery(sqlString);
  
      try
      (ResultSet resultSet = sqlStatement.GetResultSet();)
      {
        resultSet.next();
        count = resultSet.getInt(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        MessageDialog.ShowErrorMessageDialog(null, excp, "resultSet.next() or resultSet.getInt(1)");
      }
    }

    return count;
  }

  public double GetTotalSalesForPeriod(_GregorianCalendar startDate, // inclusive
    _GregorianCalendar endDate) // inclusive
  {
    double cost = 0;
    try
    (SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "select sum(" + PrintJobHeaderRow.Name_cost() + ") " + "from " + GetTableName() + " " + "where " + PrintJobHeaderRow.Name_installation_id() + " = '"
        + GetInstallationDatabase().GetInstallationID().toString() + "' and " + AbstractSQL2Row.Name_created_dts() + " >= '" + startDate.GetDateNumericString() + "' and "
        + AbstractSQL2Row.Name_created_dts() + " <= '" + endDate.GetDateNumericString() + "' ";
  
      sqlStatement.ExecuteQuery(sqlString);
  
      try
      (ResultSet resultSet = sqlStatement.GetResultSet();)
      {
        resultSet.next();
        cost = resultSet.getDouble(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        MessageDialog.ShowErrorMessageDialog(null, excp, "resultSet.next() or resultSet.getInt(1)");
      }
    }

    return cost;
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(PrintJobHeaderRow.Name_color()) == true)
    {
      return 60;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = PrintJobHeaderRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
