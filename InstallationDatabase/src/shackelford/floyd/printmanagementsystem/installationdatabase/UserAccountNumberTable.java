package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: UserAccountNumberTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountNumberTable
  extends AbstractSQL1Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "user_account_number_table";

  public UserAccountNumberTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, UserAccountNumberRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  /**
    this operation returns the next user account ID
  */
  public String GetNextUserAccountID()
  {
    String nextUserAccountNumber = "00000";
    try
    (SQLStatement sqlStatement = GetInstallationDatabase().GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.BeginTransaction();
      String sqlString = "select (" + UserAccountNumberRow.Name_last_user_account_number() + " + 1) from " + TABLE_NAME + " " + "where " + UserAccountNumberRow.Name_installation_id() + " = '"
        + GetInstallationDatabase().GetInstallationID().toString() + "'";
      try
      (ResultSet resultSet = sqlStatement.ExecuteQuery(sqlString);)
      {
        resultSet.next(); // position us to the first row returned from the query
        nextUserAccountNumber = resultSet.getString(1);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
      }
      sqlStatement.CloseResultSet();
      sqlString = "update " + TABLE_NAME + " " + "set " + UserAccountNumberRow.Name_last_user_account_number() + " = '" + nextUserAccountNumber + "' " + "where "
        + UserAccountNumberRow.Name_installation_id() + " = '" + GetInstallationDatabase().GetInstallationID().toString() + "'";
      sqlStatement.ExecuteUpdate(sqlString);
      sqlStatement.CommitTransaction();
    }

    // calculate the sum of the digits
    int sum = 0;
    for (int i = 0; i < nextUserAccountNumber.length(); i++)
    {
      //sum += (new Integer(new String(new char[] {nextUserAccountNumber.charAt(i)}))).intValue();
      sum += nextUserAccountNumber.charAt(i);
    }

    // jam modulo 10 of the sum in between the 3rd and 4th characters
    String nextUserAccountID = nextUserAccountNumber.substring(0, 3) + String.valueOf(sum % 10) + nextUserAccountNumber.substring(3, nextUserAccountNumber.length());

    return nextUserAccountID;

  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = UserAccountNumberRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
