package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: PrePaymentHistoryTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrePaymentHistoryTable
  extends AbstractSQL2Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "pre_payment_history_table";

  public PrePaymentHistoryTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, PrePaymentHistoryRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  public int RowCountForUserAccount(String userAccountID)
  {
    String predicate = PrePaymentHistoryRow.Name_user_account_id() + " = '" + SQLUtilities.FixSQLString(userAccountID.trim()) + "'";
    return RowCount(predicate);
  }

  /**
    this operation removes pre payment history rows from the pre payment history table
    for a specified user account
  */
  public void RemoveRowsForUserAccount(String userAccountID)
  {
    String predicate = PrePaymentHistoryRow.Name_user_account_id() + " = '" + SQLUtilities.FixSQLString(userAccountID.trim()) + "'";
    RemoveRows(predicate);
  }

  /*
    public ArrayList GetRowsForUserAccountAsArrayList ( String userAccountID )
    {
      String predicate = PrePaymentHistoryRow.Name_user_account_id() + " = '" + SQLUtilities.FixSQLString(userAccountID.trim()) + "'";
      return GetRowsAsArrayList (
               null,    // from, null = use this table's name
               predicate,
               null,    // orderBy, null = don't order rows
               0 );     // limit, 0 = unlimited
    }


    public SQLStatement GetRowsForUserAccountAsResultSet ( String userAccountID )
    {
      String predicate = PrePaymentHistoryRow.Name_user_account_id() + " = '" + SQLUtilities.FixSQLString(userAccountID.trim()) + "'";
      return GetRowsAsResultSet (
               null,    // from, null = use this table's name
               predicate,
               null,    // orderBy, null = don't order rows
               0 );     // limit, 0 = unlimited
    }
  */

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(PrePaymentHistoryRow.Name_source()) == true)
    {
      return 100;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = PrePaymentHistoryRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
