package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;


/**
 * <p>Title: PrintJobPropertiesRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobPropertiesRow
  extends AbstractSQL1Row
{

  private Integer                 _sql_installation_id;
  private Integer                 _sql_print_job_number;
  private String                  _sql_property_name;
  private String                  _sql_property_value;

  public static final String      PROPERTY_CLIENT_ADDRESS = "Client IP Address";
  public static final String      PROPERTY_USER_NAME = "User Name";
  public static final String      PROPERTY_DOCUMENT_NAME = "Document Name";

  public static final String      PROPERTY_BITS_PER_PIXEL = "Bits per Pixel";
  public static final String      PROPERTY_COLLATE = "Collate";
  public static final String      PROPERTY_COLOR = "Color";
  public static final String      PROPERTY_COPIES = "Copies";
  public static final String      PROPERTY_COST = "Cost";
  public static final String      PROPERTY_DESCRIPTION = "Description";
  public static final String      PROPERTY_DUPLEX = "Duplex";
  public static final String      PROPERTY_ECONOMODE = "Economode";
  public static final String      PROPERTY_FINISH = "Finish";
  public static final String      PROPERTY_HOLD = "Hold";
  public static final String      PROPERTY_HOLE_PUNCH = "Hole Punch";
  public static final String      PROPERTY_HOST_PRINT_JOB_NUMBER = "Host Print Job Number";
  public static final String      PROPERTY_JOB_NAME = "Job Name";
  public static final String      PROPERTY_LANGUAGE = "Language";
  public static final String      PROPERTY_NUMBER_OF_BYTES = "Number Of Bytes";
  public static final String      PROPERTY_NUMBER_OF_PAGES = "Number Of Pages";
  public static final String      PROPERTY_ORIENTATION = "Orientation";
  public static final String      PROPERTY_OUTBIN = "Outbin";
  public static final String      PROPERTY_PAGE_PROTECT = "Page Protect";
  public static final String      PROPERTY_PAGES_PER_PAGE = "Pages Per Page";
  public static final String      PROPERTY_PAPER = "Paper";
  public static final String      PROPERTY_PAPER_LENGTH = "Paper Length";
  public static final String      PROPERTY_PAPER_SIZE = "Paper Size";
  public static final String      PROPERTY_PAPER_SOURCE = "Paper Source";
  public static final String      PROPERTY_PAPER_TYPE = "Paper Type";
  public static final String      PROPERTY_PAPER_WIDTH = "Paper Width";
  public static final String      PROPERTY_PRINT_JOB_NUMBER = "Print Job Number";
  public static final String      PROPERTY_PRINTER = "Printer";
  public static final String      PROPERTY_PRINT_QUALITY = "Print Quality";
  public static final String      PROPERTY_RENDER_MODE = "Rendermode";
  public static final String      PROPERTY_RET = "Ret";
  public static final String      PROPERTY_SCALE = "Scale";
  public static final String      PROPERTY_SIDES = "Sides";
  public static final String      PROPERTY_SORT = "Sort";
  public static final String      PROPERTY_STAPLE = "Staple";
  public static final String      PROPERTY_RESOLUTION = "Resolution";
  public static final String      PROPERTY_USER_ACCOUNT_ID = "Account ID";

  public static final String      VALUE_SIDES_SIMPLEX = "1";
  public static final String      VALUE_SIDES_DUPLEX = "2";

  public static final String      VALUE_ORIENTATION_PORTRAIT = "portrait";
  public static final String      VALUE_ORIENTATION_LANDSCAPE = "landscape";
  public static final String      VALUE_ORIENTATION_REVERSE_PORTRAIT = "reverse portrait";
  public static final String      VALUE_ORIENTATION_REVERSE_LANDSCAPE = "reverse landscape";

  public static final String      VALUE_COLOR_COLOR = "color";
  public static final String      VALUE_COLOR_MONOCHROME = "black & white";

  public static final String      VALUE_RENDER_MODE_COLOR = "color";
  public static final String      VALUE_RENDER_MODE_GRAYSCALE = "grayscale";

  public static final String      VALUE_PAPER_SIZE_EXECUTIVE = "executive";
  public static final String      VALUE_PAPER_SIZE_LETTER = "letter";
  public static final String      VALUE_PAPER_SIZE_LEGAL = "legal";
  public static final String      VALUE_PAPER_SIZE_LEDGER = "ledger";
  public static final String      VALUE_PAPER_SIZE_A4 = "a4";
  public static final String      VALUE_PAPER_SIZE_A3 = "a3";
  public static final String      VALUE_PAPER_SIZE_MONARCH = "monarch";
  public static final String      VALUE_PAPER_SIZE_COM_10 = "com-10";
  public static final String      VALUE_PAPER_SIZE_DL = "dl";
  public static final String      VALUE_PAPER_SIZE_C5 = "c5";
  public static final String      VALUE_PAPER_SIZE_JB4 = "jb4";
  public static final String      VALUE_PAPER_SIZE_JB5 = "jb5";
  public static final String      VALUE_PAPER_SIZE_B5 = "b5";
  public static final String      VALUE_PAPER_SIZE_J = "j";
  public static final String      VALUE_PAPER_SIZE_JJ = "jj";
  public static final String      VALUE_PAPER_SIZE_A5 = "a5";
  public static final String      VALUE_PAPER_SIZE_A6 = "a6";
  public static final String      VALUE_PAPER_SIZE_JB6 = "jb6";

  public static final String      VALUE_PAPERSOURCE_CURRENT = "current";
  public static final String      VALUE_PAPERSOURCE_PRINTERSPECIFIC = "printer specific";
  public static final String      VALUE_PAPERSOURCE_MANUALPAPER = "manual paper";
  public static final String      VALUE_PAPERSOURCE_MANUALENVELOPE = "manual envelope";
  public static final String      VALUE_PAPERSOURCE_LOWERTRAY = "lower tray";
  public static final String      VALUE_PAPERSOURCE_OPTIONALPAPERSOURCE = "optional paper source";
  public static final String      VALUE_PAPERSOURCE_OPTIONALENVELOPESOURCE = "optional envelope source";
  public static final String      VALUE_PAPERSOURCE_MUTLIPURPOSETRAY = "Multi-Purpose Tray";
  public static final String      VALUE_PAPERSOURCE_UPPERTRAY = "Upper Tray";
  public static final String      VALUE_PAPERSOURCE_THIRDTRAY = "Third Tray";

  public static final String      VALUE_DESTINATION_DEFAULTBIN = "default bin";
  public static final String      VALUE_DESTINATION_FACEDOWNBIN = "face down bin";
  public static final String      VALUE_DESTINATION_FACEUPBIN = "face up bin";
  public static final String      VALUE_DESTINATION_JOBOFFSETBIN = "job offset bin";


  public static final String      VALUE_YES = "yes";
  public static final String      VALUE_NO = "no";
  public static final String      VALUE_UNKNOWN = "unknown";
  public static final String      VALUE_NOT_SPECIFIED = "not specified";

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_print_job_number(),
                                    Name_property_name(),
                                    Name_property_value(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_print_job_number(),
                                    Name_property_name() };

  public PrintJobPropertiesRow()
  {
    super();
  }

  public PrintJobPropertiesRow(PrintJobPropertiesTable table)
  {
    super(table);
  }

  public PrintJobPropertiesRow( PrintJobPropertiesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public PrintJobPropertiesRow( PrintJobPropertiesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetInstallationDatabase().GetInstallationID());
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public Integer Get_print_job_number() { return new Integer(_sql_print_job_number.intValue()); }
  public void Set_print_job_number(Integer value) { _sql_print_job_number = new Integer(value.intValue()); }
  public void Set_print_job_number(String value) { _sql_print_job_number = new Integer(value); }
  public void Set_print_job_number(int value) { _sql_print_job_number = new Integer(value); }
  public static String Name_print_job_number() { return "print_job_number"; }

  public String Get_property_name() { return new String(_sql_property_name); }
  public void Set_property_name(String value) { _sql_property_name = new String(value); }
  public static String Name_property_name() { return "property_name"; }

  public String Get_property_value() { return new String(_sql_property_value); }
  public void Set_property_value(String value) { _sql_property_value = new String(value); }
  public static String Name_property_value() { return "property_value"; }


  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(PrintJobPropertiesRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }
}