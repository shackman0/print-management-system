package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL3Row;



/**
 * <p>Title: RulesRow</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class RulesRow
  extends AbstractSQL3Row
{

  private Integer                 _sql_installation_id;
  private String                  _sql_rule_name;
  private String                  _sql_rule_value;

  public static final String      ADMIN_AUTO_LOGOFF = "Admin Auto Logoff";
    // yes = installation administrator applications will log the user out after a specified
    //       number of seconds of inactivity
    //       parm 0: number of seconds of inactivity before automatic timeout
    // no = installation administrative users must explicitely log off

  public static final String      ALLOW_GUEST_PRINT = "Allow Guest Print";
    // yes = a Guest button appears on the GUIServer PrintJobBeingProcessed panel that allows
    //       a guest user to pay for a print job with his credit card
    //       parm 0: guest user account id
    // no = the Guest button does not appear on the panel

  public static final String      ALLOW_GUEST_PRINT_HOLD = "Allow Guest Print Hold";
    // yes = a Guest Hold button appears on the GUIServer PrintJobBeingProcessed panel that allows
    //       a guest to hold the print job for release from a pay accepter (where he deposits
    //       bills, coins, or uses his credit card) or release from the administrator's station
    //       where he pays the administrator with cash over the counter
    //       parm 0: guest user account id
    // no = the Guest Hold button does not appear on the GUIServer panel

  public static final String      ALLOW_PRINT_HOLD = "Allow Print Job Hold";
    // yes = a Hold button appears on the GUIServer PrintJobBeingProcessed panel that allows
    //       the user to hold the print job for later release
    // no = the Hold button does not appear on the panel

  public static final String      ALLOW_USER_CREATED_ACCOUNT = "Allow User Created Account";
    // yes = a New User Account button appears on the GUIServer PrintJobBeingProcessed panel that allows
    //       a user to create his own user account
    // no = the New User Account button does not appear on the panel

  public static final String      ANONYMOUS_PRINTING = "Anonymous Printing";
    // yes = do not prompt for a user account id for each print job. only record the job
    //       info in the database. the CHARGE_FOR_PRINTING and ALLOW_GUEST_PRINT settings
    //       are ignored.
    // no = prompt for the user account id of the person printing the job.

  public static final String      CHARGE_FOR_PRINTING = "Charge For Printing";
    // yes = charge the user for printing
    // no = do not charge the user for printing. this allows the product to be used
    //      for tracking-only purposes.

  public static final String      FLEXIBLE_USER_ACCOUNT_IDS = "Flexible User Account IDs";
    // no = use only system generated user account ids (i.e. make the user account id
    //      field protected from change by the user)
    // yes = allow the user to edit the system generated user id field if he wants to
    //       when creating a new user account. (note: once the user account is created,
    //       the user account id is "frozen")

  public static final String      GUEST_ACCOUNT_ONLY = "Guest Account Only";
    // yes = use the guest account for all print jobs. when a print job
    //       is submitted, the user is only prompted for a description on the
    //       gui server's print job information entry panel.
    //       parm 0: guest user account id
    // no = allow other user accounts

  public static final String      HOLD_ALL_JOBS = "Hold All Jobs";
    // yes = a user can only hold a job when it is printed. the user must go to a release
    //       station to release the job. note: be sure to have ALLOW_PRINT_HOLD set to
    //       "yes", otherwise you'll only be able to cancel print jobs.
    // no = a user may release a jobs when it's printed.

  public static final String      REQUIRE_USER_PASSWORD = "Require User Password";
    // yes = the user must supply a password whenever he has to supply his user
    //       account id to identify himself
    // no = the user will be able to identify himself solely by supplying his
    //      user account id. no password will be requested.

  public static final String      USER_AUTO_LOGOFF = "User Auto Logoff";
    // yes = user applications will log the user out after a specified number of seconds
    //       of inactivity
    //       parm 0: number of seconds of inactivity before automatic timeout
    // no = a user must explicitely log off

  public static final String[]    RULE_NAMES = {
                                    ADMIN_AUTO_LOGOFF,
                                    ALLOW_GUEST_PRINT,
                                    ALLOW_GUEST_PRINT_HOLD,
                                    ALLOW_PRINT_HOLD,
                                    ALLOW_USER_CREATED_ACCOUNT,
                                    ANONYMOUS_PRINTING,
                                    CHARGE_FOR_PRINTING,
                                    FLEXIBLE_USER_ACCOUNT_IDS,
                                    GUEST_ACCOUNT_ONLY,
                                    HOLD_ALL_JOBS,
                                    REQUIRE_USER_PASSWORD,
                                    USER_AUTO_LOGOFF };

  public static final String      NO = "no";
  public static final String      YES = "yes";
  private static final String     GUEST_ID = ";guest_id";
  private static final String     ANONYMOUS_ID = ";anonymous_id";
  private static final String     AUTO_LOGOFF_SECONDS = ";120";

  public static final String[]    RULE_DEFAULT_VALUES = {
                                    NO + AUTO_LOGOFF_SECONDS,    // ADMIN_AUTO_LOGOFF
                                    NO + GUEST_ID,               // ALLOW_GUEST_PRINT
                                    NO + GUEST_ID,               // ALLOW_GUEST_PRINT_HOLD
                                    NO,                          // ALLOW_PRINT_HOLD
                                    NO,                          // ALLOW_USER_CREATED_ACCOUNT
                                    NO + ANONYMOUS_ID,           // ANONYMOUS_PRINTING
                                    YES,                         // CHARGE_FOR_PRINTING
                                    NO,                          // FLEXIBLE_USER_ACCOUNT_IDS
                                    NO + GUEST_ID,               // GUEST_ACCOUNT_ONLY
                                    NO,                          // HOLD_ALL_JOBS
                                    YES,                         // REQUIRE_USER_PASSWORD
                                    YES + AUTO_LOGOFF_SECONDS }; // USER_AUTO_LOGOFF

  public static final int         NUMBER_OF_RULES = RULE_NAMES.length;

  private static Field[]          __sqlFields;
  private static Field[]          __sqlKeyFields;
  private static String[]         __columnNames = {
                                    Name_rule_name(),
                                    Name_rule_value(),

                                    Name_created_dts(),
                                    Name_updated_dts(),

                                    Name_installation_id() };

  private static String[]         __keyColumnNames = {
                                    Name_installation_id(),
                                    Name_rule_name() };

  public RulesRow()
  {
    super();
  }

  public RulesRow(RulesTable table)
  {
    super(table);
  }

  public RulesRow( RulesTable table, ResultSet resultSet )
  {
    super(table, resultSet);
  }

  public RulesRow( RulesTable table, AbstractSQL1Row initRow )
  {
    super(table,initRow);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    Set_installation_id(GetInstallationDatabase().GetInstallationID());
  }

  public String GetRuleValue()
  {
    StringTokenizer tokenizer = new StringTokenizer(Get_rule_value(),";");
    return tokenizer.nextToken().toLowerCase();
  }

  public boolean RuleIsYes()
  {
    return (GetRuleValue().equalsIgnoreCase(YES) == true);
  }

  public boolean RuleIsNo()
  {
    return (GetRuleValue().equalsIgnoreCase(NO) == true);
  }

  /**
   * rule values are strings with the following format: "yes_or_no;parm_0;parm_1;...;parm_N"
   */
  public String GetRuleParm(int parmNumber)
  {
    StringTokenizer tokenizer = new StringTokenizer(Get_rule_value(),";");
    String parmValue = tokenizer.nextToken();   // discard the rule value's "yes" or "no"
    try
    {
      for (int i = 0; i < parmNumber; i++)
      {
        tokenizer.nextToken();
      }
      parmValue = tokenizer.nextToken();
    }
    catch (NoSuchElementException excp)
    {
      excp.printStackTrace();
      parmValue = null;
    }
    return parmValue;
  }

  public Integer Get_installation_id() { return new Integer(_sql_installation_id.intValue()); }
  public void Set_installation_id(Integer value) { _sql_installation_id = new Integer(value.intValue()); }
  public void Set_installation_id(String value) { _sql_installation_id = new Integer(value); }
  public void Set_installation_id(int value) { _sql_installation_id = new Integer(value); }
  public static String Name_installation_id() { return "installation_id"; }

  public String Get_rule_name() { return new String(_sql_rule_name); }
  public void Set_rule_name(String value) { _sql_rule_name = new String(value); }
  public static String Name_rule_name() { return "rule_name"; }

  public String Get_rule_value() { return new String(_sql_rule_value); }
  public void Set_rule_value(String value) { _sql_rule_value = new String(value); }
  public static String Name_rule_value() { return "rule_value"; }


  public InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase)(_sqlTable.GetSQLDatabase());
  }

  @Override
  public Field[] GetSQLFields()
  {
    return __sqlFields;
  }

  @Override
  public Field[] GetSQLKeyFields()
  {
    return __sqlKeyFields;
  }

  @Override
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  public static String[] GetStaticColumnNames()
  {
    return __columnNames;
  }

  static
  {
    __sqlFields = ConstructSQLFieldsArray(RulesRow.class);
    if (__sqlFields.length != __columnNames.length)
    {
      new Exception("sqlFields.length = " + __sqlFields.length + " but columnNames.length = " + __columnNames.length).printStackTrace();
    }
    __sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields,__keyColumnNames);
  }

}