package shackelford.floyd.printmanagementsystem.installationdatabase;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQLDatabase;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common.SQLUtilities;


/**
 * <p>Title: PrintJobNumberTable</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobNumberTable
  extends AbstractSQL1Table
{

  private static HashMap<String, String> __columnToViewNamesMap;

  private static HashMap<String, String> __viewToColumnNamesMap;

  private static Resources               __resources;

  public static final String             TABLE_NAME = "print_job_number_table";

  public PrintJobNumberTable(AbstractSQLDatabase sqlDatabase)
  {
    super(sqlDatabase, TABLE_NAME, PrintJobNumberRow.class);
  }

  @Override
  protected String ConstructPredicate(String predicate)
  {
    return super.ConstructPredicate(GetInstallationDatabase().AddInstallationIDToPredicate(predicate));
  }

  /**
    this operation returns the next print job number
  */
  @SuppressWarnings("resource")
  public int GetNextPrintJobNumber()
  {
    int nextPrintJobNumber = -1;

    SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();

    try
    {
      sqlStatement.ExecuteUpdate("begin transaction");
      ResultSet resultSet = sqlStatement.ExecuteQuery("select (" + PrintJobNumberRow.Name_last_print_job_number() + " + 1) " + " from " + TABLE_NAME + " " + "where "
        + PrintJobNumberRow.Name_installation_id() + " = '" + GetInstallationDatabase().GetInstallationID().toString() + "'");
      resultSet.next(); // position us to the first row returned from the query
      nextPrintJobNumber = resultSet.getInt(1);
      sqlStatement.CloseResultSet();
      sqlStatement.ExecuteUpdate("update " + TABLE_NAME + " " + "set " + PrintJobNumberRow.Name_last_print_job_number() + " = '" + nextPrintJobNumber + "' " + "where "
        + PrintJobNumberRow.Name_installation_id() + " = '" + GetInstallationDatabase().GetInstallationID().toString() + "'");
      sqlStatement.ExecuteUpdate("commit transaction");
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "GetNextPrintJobNumber()", JOptionPane.ERROR_MESSAGE);
    }

    sqlStatement.MakeAvailable();

    return nextPrintJobNumber;
  }

  protected InstallationDatabase GetInstallationDatabase()
  {
    return (InstallationDatabase) _sqlDatabase;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  public HashMap<String, String> GetColumnToViewNamesMap()
  {
    return new HashMap<>(__columnToViewNamesMap);
  }

  @Override
  public HashMap<String, String> GetViewToColumnNamesMap()
  {
    return new HashMap<>(__viewToColumnNamesMap);
  }

  static
  {
    __resources = Resources.CreateResources(InstallationDatabase.DATABASE_NAME, TABLE_NAME);

    String[] columnNames = PrintJobNumberRow.GetStaticColumnNames();
    String[] viewNames = CreateViewNames(columnNames, __resources);

    __columnToViewNamesMap = SQLUtilities.CreateMap(columnNames, viewNames);
    __viewToColumnNamesMap = SQLUtilities.CreateMap(viewNames, columnNames);
  }
}
