#!/usr/bin/perl -w -U

$dev="/backup/rw_cdrom";
$backup="/mnt/backup";
$out="2>&1 >>/var/log/backitup.log";

`umount $backup`;

print "checking integrity of $dev ...\n";
`/sbin/e2fsck -p $dev`;

print "mounting $dev at $backup ...\n";
`mount $dev`;

use POSIX qw(strftime);
$backup_time = strftime "%Y%m%d_%H%M%S", localtime;
$dir_name = "Backup_$backup_time";

print "Checking size needed for the backup ... ";
$backupDirs = "/home /etc";
@junk = split /\s+/,`du -k --total $backupDirs | tail -1`;
$spaceNeeded = $junk[0];
print "Space needed: $spaceNeeded.\n";

print "Checking space available on the backup device ... ";
@junk = split /\s+/,`df -k $backup | tail -1`;
$spaceAvailable = $junk[3];
print "Space available: $spaceAvailable.\n";

if ($spaceNeeded > $spaceAvailable)
{
  print "Writing $dev to CD-RW";
  `/usr/local/sbin/writeCD.pl $out`;
  if ($? != 0)
  {
    print "writeCD.pl rc=$?; aborting now ...\n";
    exit -1;
  }
  print "Checking space available on the backup device ... ";
  @junk = split /\s+/,`df -k $backup | tail -1`;
  $spaceAvailable = $junk[3];
  print "Space available: $spaceAvailable.\n";

  if ($spaceNeeded > $spaceAvailable)
  {
    print "Still no room on $backup, aborting ...\n";
    exit -1;
  }
}

print "Beginning data backup at $backup_time\n";
`mkdir -p $backup/$dir_name`;
foreach $d (split /\s+/,$backupDirs)
{
  print "cp -a $d $backup/$dir_name/.\n";
  `cp -a $d $backup/$dir_name/.`;
  `ls -al $backup/$dir_name $out`;
}

$backup_time = strftime "%Y%m%d_%H%M%S", gmtime;
print "Backup finished at $backup_time\n";

print "dismounting $dev from $backup ...\n";
`umount $dev`;

print "${0} done!\n";

