-- $Header: p:\CVS Repository/PayToPrint/InstallationDatabase/InstallationDatabase.sql,v 1.1 2009/01/28 18:23:20 Floyd Shackelford Exp $

-- NOTE: "cc" is used as an abreviation for credit_card

create table installation_table
(
  installation_id                      int4           not null,
  installation_name                    varchar(64)    not null,
  installation_passphrase              varchar(64)    not null,
  upgrade_preference                   char           not null,
  status                               char           not null,
  interim_expiration_date              date           not null,
  license_expiration_date              date           not null,
  max_number_of_clients                int4           not null,
  max_number_of_gatekeepers            int4           not null,
  smtp_host                            varchar(32)    not null,
  administrator_e_mail_address         varchar(32)    not null,
  current_release_number               int4           not null,
  language                             varchar(32)    not null,
  currency_symbol                      char(4)        not null,
  thousands_separator                  char           not null,
  decimal_separator                    char           not null,
  egold_charge_url                     varchar(64)    not null,
  egold_charge_account                 varchar(24)    not null,
  egold_charge_passphrase              varchar(64)    not null,
  egold_charge_minimum_amount          decimal        not null,
  egold_charge_xaction_fee             decimal        not null,
  egold_charge_xaction_fee_pcent       decimal        not null,
  egold_refund_url                     varchar(64)    not null,
  egold_refund_account                 varchar(24)    not null,
  egold_refund_passphrase              varchar(64)    not null,
  egold_refund_xaction_fee             decimal        not null,
  egold_refund_xaction_fee_pcent       decimal        not null,
  paypal_charge_url                    varchar(64)    not null,
  paypal_charge_account                varchar(24)    not null,
  paypal_charge_passphrase             varchar(64)    not null,
  paypal_charge_minimum_amount         decimal        not null,
  paypal_charge_xaction_fee            decimal        not null,
  paypal_charge_xaction_fee_pcent      decimal        not null,
  paypal_refund_url                    varchar(64)    not null,
  paypal_refund_account                varchar(24)    not null,
  paypal_refund_passphrase             varchar(64)    not null,
  paypal_refund_xaction_fee            decimal        not null,
  paypal_refund_xaction_fee_pcent      decimal        not null,
  cc_charge_url                        varchar(64)    not null,
  cc_charge_account                    varchar(24)    not null,
  cc_charge_passphrase                 varchar(64)    not null,
  cc_charge_minimum_amount             decimal        not null,
  cc_charge_xaction_fee                decimal        not null,
  cc_charge_xaction_fee_pcent          decimal        not null,
  cc_refund_url                        varchar(64)    not null,
  cc_refund_account                    varchar(24)    not null,
  cc_refund_passphrase                 varchar(64)    not null,
  cc_refund_xaction_fee                decimal        not null,
  cc_refund_xaction_fee_pcent          decimal        not null,
  created_dts                          timestamp      not null default text 'now',
  updated_dts                          timestamp      not null default text 'now'
);

grant all on installation_table to public;

create unique index installation_ndx_01 on installation_table
(
  installation_id
);

-- the page_charges_table is used to calculate the cost of a print job
-- a new page charge id is calculated as "max(page_charge_id) + 1"

create table page_charges_table
(
  installation_id            int4           not null,
  page_charge_number         int4           not null,
  per_page_charge            decimal        not null,
  paper_size                 varchar(32)    not null,
  sides                      char           not null,
  color                      char           not null,
  printer                    varchar(32)    not null,
  created_dts                timestamp      not null default text 'now',
  updated_dts                timestamp      not null default text 'now'
);


grant all on page_charges_table to public;

create unique index page_charges_ndx_01 on page_charges_table
(
  installation_id,
  page_charge_number
);

create unique index page_charges_ndx_02 on page_charges_table
(
  installation_id,
  printer,
  paper_size,
  sides,
  color
);


-- the page_charge_number_table has only one row in it, i.e. the last page
-- charge number assigned to a page charge instance. because a page charge
-- instance may be deleted, there may be gaps in the numbers found in the
-- page_charge_number_table.

create table page_charge_number_table
(
  installation_id           int4         not null,
  last_page_charge_number   int4         not null
);

grant all on page_charge_number_table to public;

create unique index page_charge_number_ndx_01 on page_charge_number_table
(
  installation_id,
  last_page_charge_number
);

-- this insert seed the page_charge_number_table.

-- insert into page_charge_number_table ( installation_id, last_page_charge_number ) values ( '120001', 1 );


-- the print_job_header_table is the "master" job table. it contains one row for
-- each print job. it records the time the print job was created and the
-- id assigned by the tracker system to the print job.

create table print_job_header_table
(
  installation_id    int4          not null,
  print_job_number   int4          not null,
  user_account_id    varchar(32)   not null,
  number_of_pages    int2          not null,
  paper_size         varchar(32)   not null,
  sides              char          not null,
  printer            varchar(32)   not null,
  color              char          not null,
  cost               numeric(10,3) not null,
  created_dts        timestamp     not null default text 'now'
);


grant all on print_job_header_table to public;

create unique index print_job_header_ndx_01 on print_job_header_table
(
  installation_id,
  print_job_number
);

create unique index print_job_header_ndx_02 on print_job_header_table
(
  installation_id,
  user_account_id,
  created_dts
);


-- the print_job_on_hold_header_table is the "master" job table for held jobs. it contains one row for
-- each held print job. it records the time the print job was created and the
-- id assigned by the tracker system to the print job.

create table print_job_on_hold_header_table
(
  -- no new fields
) inherits (print_job_header_table);


grant all on print_job_on_hold_header_table to public;

create unique index print_job_on_hold_header_ndx_01 on print_job_on_hold_header_table
(
  installation_id,
  print_job_number
);

create unique index print_job_on_hold_header_ndx_02 on print_job_on_hold_header_table
(
  installation_id,
  user_account_id,
  created_dts
);


-- the print_job_properties_table stores all the properties associated with
-- a print job. print_job_header_table has a 1->(0..M) relationship to the
-- print_job_properties_table on the print_job_number column.

create table print_job_properties_table
(
  installation_id        int4             not null,
  print_job_number       int4             not null,
  property_name          varchar(64)      not null,
  property_value         varchar(64)      not null default ''
);

grant all on print_job_properties_table to public;

create index print_job_properties_ndx_01 on print_job_properties_table
(
  installation_id,
  print_job_number,
  property_name
);


-- the print_job_number_table has only one row in it, i.e. the last print
-- job number assigned to a print job instance. because a print job
-- instance may never make it into the print_job_header_table, there may
-- be gaps in the job id's found in the print_job_header_table.

create table print_job_number_table
(
  installation_id         int4         not null,
  last_print_job_number   int4         not null
);

grant all on print_job_number_table to public;

create unique index print_job_number_ndx_01 on print_job_number_table
(
  installation_id,
  last_print_job_number
);

-- this insert seed the print_job_number_table.

-- insert into print_job_number_table ( last_print_job_number ) values ( 1 );


-- The following tables make up the User Account Database component.
-- The User Account Database keeps track of all the users for an installation.

-- the user_account_table is the "master" user table. it contains one row for
-- each user.

create table user_account_table
(
  installation_id       int4             not null,
  user_account_id       varchar(32)      not null,
  user_name             varchar(64)      not null,
  user_passphrase       varchar(64)      not null,
  additional_info       varchar(1024)    not null,
  status                char             not null,
  language              varchar(32)      not null,
  pre_payment_balance   decimal          not null,
  require_new_password  bool             not null,
  e_mail_address        varchar(32)      not null,
  home_address          varchar(256)     not null,
  phone_number          varchar(32)      not null,
  created_dts           timestamp        not null default text 'now',
  updated_dts           timestamp        not null default text 'now'
);

grant all on user_account_table to public;

create unique index user_account_ndx_01 on user_account_table
(
  installation_id,
  user_account_id
);

-- the user_account_number_table has only one row in it, i.e. the last user
-- account number assigned to a a user.

create table user_account_number_table
(
  installation_id             int4         not null,
  last_user_account_number    int4         not null
);

grant all on user_account_number_table to public;

-- this insert seed the user_account_number_table. it must be 10000.

-- insert into user_account_number_table ( last_user_account_number ) values ( 10000 );


-- the pre_payment_history_table keeps track of all deposits and refunds to
-- a user's pre_payment_balance.

create table pre_payment_history_table
(
  installation_id          int4          not null,
  user_account_id          varchar(32)   not null,
  amount                   decimal       not null,
  source                   varchar(60)   not null,      -- "cash" or "account type: account number"
  created_dts              timestamp     not null default text 'now'
);

grant all on pre_payment_history_table to public;

create unique index pre_payment_history_ndx_01 on pre_payment_history_table
(
  installation_id,
  user_account_id,
  created_dts
);

-- the gatekeeper list keeps track of all the gatekeeper machine's ip addresses

create table gatekeeper_table
(
  installation_id        int4            not null,
  ip_address             varchar(15)     not null,
  created_dts            timestamp       not null default text 'now'
);

grant all on gatekeeper_table to public;

create unique index gatekeeper_ndx_01 on gatekeeper_table
(
  installation_id,
  ip_address
);

-- the client list keeps track of all the client machine's ip addresses

create table client_table
(
  installation_id        int4            not null,
  ip_address             varchar(15)     not null,
  created_dts            timestamp       not null default text 'now'
);

grant all on client_table to public;

create unique index client_ndx_01 on client_table
(
  installation_id,
  ip_address
);


create table rules_table
(
  installation_id       int4         not null,
  rule_name             varchar(32)  not null,
  rule_value            varchar(256) not null,
  created_dts           timestamp    not null default text 'now',
  updated_dts           timestamp    not null default text 'now'
);

grant all on rules_table to public;

create unique index rules_ndx_01 on rules_table
(
  installation_id,
  rule_name
);

-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Flexible User Account IDs', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Allow Print Job Hold', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Allow Guest Print', 'no;guest_id');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Allow User Created Account', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Charge For Printing', 'yes');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Anonymous Printing', 'no;anonymous_id');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Guest Print Hold', 'no;guest_id');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Hold All Jobs', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'User Auto Logoff', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Admin Auto Logoff', 'no');
-- insert into rules_table (installation_id, rule_name, rule_value) values ('120001', 'Guest Account Only', 'no;guest_id');

