package shackelford.floyd.printmanagementsystem.expirationupdater;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.monitordatabase.InstallationsRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;

/**
 * <p>Title: ExpirationUpdater</p>
 * <p>Description:
 * updates the expiration date of the Installations
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ExpirationUpdater
{

  private static String       DBADDR_OPTION                  = "-dbAddr";

  private static String       DBUSER_OPTION                  = "-dbUser";

  private static String       DBPASSWD_OPTION                = "-dbPasswd";

  private static String       DAYSBEFORE_OPTION              = "-daysBefore";

  public static final int     EXIT_OK                        = 0;

  public static final int     EXIT_ERROR_COMMAND_LINE_PARMS  = -1;

  public static final int     EXIT_ERROR_MISSING_RESDIR      = -2;

  public static final int     EXIT_ERROR_MISSING_DBADDR      = -3;

  public static final int     EXIT_ERROR_MISSING_DBUSERID    = -4;

  public static final int     EXIT_ERROR_MISSING_DBPASSWD    = -5;

  public static final int     EXIT_ERROR_DATABASE_CONNECT    = -6;

  public static final int     EXIT_ERROR_MISSING_INST_ID     = -7;

  public static final int     EXIT_ERROR_INVALID_DAYS_BEFORE = -8;

  private static final String NEWLINE                        = "\n";

  private static final String TAB                            = "\t";

  @SuppressWarnings("resource")
  public static void main(String[] args)
  {

    int daysBeforeExpiration = 5;

    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase(DAYSBEFORE_OPTION) == true)
      {
        daysBeforeExpiration = new Integer(args[++index]).intValue();
      }
      else
        if (args[index].equalsIgnoreCase(DBADDR_OPTION) == true)
        {
          GlobalAttributes.__dbAddr = args[++index];
          System.out.println("Monitor Database Addr = \"" + GlobalAttributes.__dbAddr + "\"");
        }
        else
          if (args[index].equalsIgnoreCase(DBUSER_OPTION) == true)
          {
            GlobalAttributes.__dbUserID = args[++index];
            System.out.println("Monitor Database User ID = \"" + GlobalAttributes.__dbUserID + "\"");
          }
          else
            if (args[index].equalsIgnoreCase(DBPASSWD_OPTION) == true)
            {
              GlobalAttributes.__dbPasswd = args[++index];
              System.out.println("Monitor Database Password found");
            }
            else
            {
              System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
            }
    }

    if (daysBeforeExpiration <= 0)
    {
      System.out.println(DAYSBEFORE_OPTION + " must be > 0");
      Usage();
      System.exit(EXIT_ERROR_INVALID_DAYS_BEFORE);
    }
    else
    {
      System.out.println("Days before interim expiration = " + new Integer(daysBeforeExpiration));
    }
    if (GlobalAttributes.__dbAddr == null)
    {
      System.out.println(DBADDR_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBADDR);
    }

    if (GlobalAttributes.__dbUserID == null)
    {
      System.out.println(DBUSER_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBUSERID);
    }

    if (GlobalAttributes.__dbPasswd == null)
    {
      System.out.println(DBPASSWD_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBPASSWD);
    }

    try
    {
      _GregorianCalendar todayCalendar = new _GregorianCalendar();

      System.out.println("Today's date is " + todayCalendar.GetDateString() + NEWLINE);

      MonitorDatabase.SetDefaultMonitorDatabase(new MonitorDatabase(GlobalAttributes.__dbAddr, GlobalAttributes.__dbUserID, GlobalAttributes.__dbPasswd));

      MonitorDatabase.GetDefaultMonitorDatabase().ConnectDatabase();

      SQLStatement sqlStatement = MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable().GetRowsAsResultSet(null, // from
        null, // predicate
        InstallationsRow.Name_installation_id(), // order by
        AbstractSQL1Table.NO_LIMIT);
      ResultSet resultSet = sqlStatement.GetResultSet();

      while (resultSet.next())
      {
        InstallationsRow installationsRow = new InstallationsRow(MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable(), resultSet);

        System.out.println(NEWLINE + "Processing " + installationsRow.Get_installation_id() + " " + installationsRow.Get_installation_name().trim() + NEWLINE + TAB + "status = "
          + installationsRow.Get_status() + NEWLINE + TAB + "license expiration date = " + installationsRow.Get_license_expiration_date().GetDateString());

        if (installationsRow.StatusIsEnabled() == true)
        {
          try
          {
            InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
              installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
            InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
          }
          catch (Exception excp)
          {
            System.out.println(TAB + "Error: could not establish a connection.");
            //excp.printStackTrace();
            continue;
          }

          InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();

          if (installationRow == null)
          {
            System.out.println(TAB + "Error: could not find installation row in remote database.");
            continue;
          }

          System.out.println(TAB + "interim expiration date = " + installationRow.Get_interim_expiration_date().GetDateString());

          _GregorianCalendar licenseExpDateCalendar = new _GregorianCalendar();
          licenseExpDateCalendar.setTime(installationsRow.Get_license_expiration_date().getTime());

          if (licenseExpDateCalendar.DaysBefore(todayCalendar) >= 0)
          {
            if (installationsRow.StatusIsEnabled() == true)
            {
              // the installation's license has expired.
              installationsRow.Set_status(AbstractSQL4Row.STATUS_DISABLED_NAME);
              installationsRow.Update();
              installationRow.Set_status(AbstractSQL4Row.STATUS_DISABLED_NAME);
              installationRow.Update();
              System.out.println("\tDisabling " + installationRow.Get_installation_id() + " - their license expired on " + licenseExpDateCalendar.GetDateString());
            }
            else
            {
              // the remote installation row shows the installation is already disabled, skipping
              System.out.println(TAB + "skipped");
              continue;
            }
          }
          else
          {
            java.util.Date interimExpDate = installationRow.Get_interim_expiration_date().getTime();
            _GregorianCalendar interimExpDateCalendar = new _GregorianCalendar();
            interimExpDateCalendar.setTime(interimExpDate);

            if (interimExpDateCalendar.DaysAfter(todayCalendar) <= daysBeforeExpiration)
            {
              if (interimExpDateCalendar.DaysBefore(todayCalendar) > 0)
              {
                interimExpDate = todayCalendar.getTime();
              }
              int daysToIncrement = installationsRow.Get_expiration_date_increment().intValue();
              _GregorianCalendar newInterimExpDateCalendar = new _GregorianCalendar();
              newInterimExpDateCalendar.setTime(interimExpDate);
              newInterimExpDateCalendar.add(Calendar.DATE, daysToIncrement);
              if (newInterimExpDateCalendar.after(licenseExpDateCalendar) == true)
              {
                newInterimExpDateCalendar = licenseExpDateCalendar;
              }
              _GregorianCalendar newInterimExpDate = new _GregorianCalendar(String.valueOf(newInterimExpDateCalendar.get(Calendar.YEAR)) + "-"
                + String.valueOf(newInterimExpDateCalendar.get(Calendar.MONTH) + 1) + "-" + String.valueOf(newInterimExpDateCalendar.get(Calendar.DAY_OF_MONTH)));
              System.out.println("\tChanging " + installationRow.Get_installation_id() + "'s interim expiration date from " + installationRow.Get_interim_expiration_date() + " to "
                + newInterimExpDate);
              installationRow.Set_interim_expiration_date(newInterimExpDate);
              installationRow.Update();
            }
            else
            {
              // the interim license is still valid
              System.out.println(TAB + "the interim license is still valid");
            }
          }

          InstallationDatabase.GetDefaultInstallationDatabase().Done();
        }
        else
        {
          // the local installation row shows the installation is already disabled, skipping
          System.out.println(TAB + "skipped");
        }
      }
      sqlStatement.CloseResultSet();

      MonitorDatabase.GetDefaultMonitorDatabase().Done();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
    }

    System.exit(EXIT_OK);
  }

  private static void Usage()
  {
    System.err.println("Usage e.g.: java -Djdbc.drivers=org.postgresql.Driver ExpirationUpdater " + DBADDR_OPTION + " <database hostname or ip addr> " + DBUSER_OPTION + " <database user id> "
      + DBPASSWD_OPTION + " <database passwd> " + "[" + DAYSBEFORE_OPTION + " <days before interim expiration date>] ");
  }

}
