#! /bin/bash

# ExpirationUpdater

# This script launches the ExpirationUpdater java program.

# Set the JDKTOP variable to point to where the Java Virtual Machine
# and supporting files are located. Use an absolute path.
# be sure to preserve the trailing slashes as shown.

LOGFILE=/var/log/ExpirationUpdater.log
INSTALLDIR=/usr/local/ExpirationUpdater/
JDKTOP=/usr/local/java2/
JDKLIB=${JDKTOP}/lib/
CLASSPATH=${INSTALLDIR}ExpirationUpdater.jar:${JDKLIB}:
DBADDR=localhost
DBUSER=CMS
DBPASSWD=CMSPASSWD
DAYSBEFORE=5


(${JDKTOP}bin/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver ExpirationUpdater -dbAddr ${DBADDR} -dbUser ${DBUSER} -dbPasswd ${DBPASSWD} -daysBefore ${DAYSBEFORE} -verbose 1>${LOGFILE} 2>&1)

cat ${LOGFILE} | mail -s "ExpirationUpdater report" floyds@4peakstech.com a.ames@cardmeter.com

