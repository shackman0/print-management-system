@echo off

rem This script launches the ExpirationUpdater application.
rem You must modify this script when you install the
rem ExpirationUpdater application before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=D:\j2sdk1.3

rem Set the SERVER variable to the hostname or the IP address
rem of the Monitor Server. Put the value in between the double
rem quotes.

set SERVER="192.168.1.200"

rem Set the USERID variable to the user id to use to log on to
rem the Monitor database.

set USERID="CMS"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Monitor database.

set PASSWORD="cmspasswd"

rem Set the DAYSBEFORE variable to the number of days from now that
rem the interim expiration date for installations expires and should
rem be extended.

set DAYSBEFORE=30


rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

:99continue

%JAVA%\bin\java -classpath .;%JAVA%\lib;.\ExpirationUpdater.jar; -Djdbc.drivers=org.postgresql.Driver ExpirationUpdater -verbose -daysBefore %DAYSBEFORE% -dbAddr %SERVER% -dbUser %USERID% -dbPasswd %PASSWORD%

:end

set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set DAYSBEFORE=