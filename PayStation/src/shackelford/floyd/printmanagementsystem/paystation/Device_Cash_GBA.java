package shackelford.floyd.printmanagementsystem.paystation;

import java.io.IOException;


/**
 * <p>Title: Device_Cash_GBA</p>
 * <p>Description:
 * device driver for the GBA cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Cash_GBA
  extends AbstractPaymentDevice
  implements CashPaymentDeviceInterface
{

  protected static final byte STX                      = (byte) 0x02;       // start text

  protected static final byte ETX                      = (byte) 0x03;       // end text

  protected static final byte ACK                      = (byte) 0x06;       // acknowledge

  protected static final byte NAK                      = (byte) 0x15;       // negative acknowledge

  private static final byte   ACK_NUM_MASK             = (byte) 0x0f;

  private static final byte   MSG_TYPE_MASK            = (byte) 0x70;

  private static final byte   MSG_TYPE_MASTER_TO_SLAVE = (byte) (0x01 << 4);

  //  private static final byte       MSG_TYPE_SLAVE_TO_MASTER = (byte)(0x02 << 4);
  private static final byte   ACK_NUM_0                = (byte) 0x00;

  private static final byte   ACK_NUM_1                = (byte) 0x01;

  private static final byte   BIT_0_MASK               = (byte) 0x01;

  private static final byte   BIT_1_MASK               = (byte) 0x02;

  private static final byte   BIT_2_MASK               = (byte) 0x04;

  private static final byte   BIT_3_MASK               = (byte) 0x08;

  private static final byte   BIT_4_MASK               = (byte) 0x10;

  private static final byte   BIT_5_MASK               = (byte) 0x20;

  private static final byte   BIT_6_MASK               = (byte) 0x40;

  //  private static final byte       BIT_7_MASK = (byte)0x80;
  private static final byte   BITS_345_MASK            = (byte) 0x38;

  private static final String BILL_TYPE_PREFIX         = "BILL_TYPE_";

  public Device_Cash_GBA()
  {
    super();
    Initialize(new Protocol_Cash_GBA(this));
  }

  @Override
  protected boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    // enable the acceptable bill types
    MessageOut messageOut = new MessageOut();
    for (int indx = 0; indx < 8; indx++)
    {
      String billValue = _paymentProtocol.GetResources().getProperty(BILL_TYPE_PREFIX + String.valueOf(indx));
      if (billValue != null)
      {
        messageOut.SetCommandNoteTypeEnabled((byte) indx, true);
      }
    }
    messageOut.Write();

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    return true;
  }

  @Override
  public double GetCashValue()
  {
    MessageOut messageOut = new MessageOut();
    messageOut.Write();

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    String billValue = _paymentProtocol.GetResources().getProperty(BILL_TYPE_PREFIX + String.valueOf(messageIn.GetBillType()));

    return new Double(billValue).doubleValue();
  }

  @Override
  public void AcceptCash()
  {
    MessageOut messageOut = new MessageOut();
    messageOut.SetCommandStack();
    messageOut.Write();

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  @Override
  public void RejectCash()
  {
    MessageOut messageOut = new MessageOut();
    messageOut.SetCommandReturn();
    messageOut.Write();

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  private abstract class AbstractMessage
  {

    protected byte[] _data;

    protected byte   _ackNum_msgType = MSG_TYPE_MASTER_TO_SLAVE;

    // ack num: bits 0-3, msg type: bits 4-6, unused: bit 7
    protected byte   _checksum;

    protected byte   _length;

    public AbstractMessage()
    {
      // do nothing
    }

    public byte GetMsgType()
    {
      return (byte) (_ackNum_msgType & MSG_TYPE_MASK);
    }

    public byte[] GetData()
    {
      return _data;
    }

    public byte GetChecksum()
    {
      return _checksum;
    }

    protected String GetAsString()
    {
      SetNextAckNum();
      SetChecksum();

      String messageAsString = new String(STX + String.valueOf(_length) + _ackNum_msgType + _data + ETX + _checksum);
      return messageAsString;
    }

    protected void SetChecksum()
    {
      _checksum = CalculateChecksum();
    }

    protected byte CalculateChecksum()
    {
      int checksum = _length ^ _ackNum_msgType;
      for (int indx = 0; indx < _data.length; indx++)
      {
        checksum ^= _data[indx];
      }
      return (byte) checksum;
    }

    protected void SetNextAckNum()
    {
      byte ackNum = (byte) (_ackNum_msgType & ACK_NUM_MASK);
      if (ackNum == ACK_NUM_0)
      {
        _ackNum_msgType = ACK_NUM_1;
      }
      else
      {
        _ackNum_msgType = ACK_NUM_0;
      }
      _ackNum_msgType |= MSG_TYPE_MASTER_TO_SLAVE;
    }

  }

  private class MessageOut
    extends AbstractMessage
  {

    public MessageOut()
    {
      super();
      _length = 8;
      _data = new byte[] { 0x00, 0x11, 0x00 };
    }

    public void Write()
    {
      try
      {
        _outputStream.write(GetAsString().getBytes());
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
    }

    // @parm noteType = 0 - 6 for note types 1 - 7
    // @parm enable = true/false

    public void SetCommandNoteTypeEnabled(byte noteType, boolean enable)
    {
      byte enableMask = 0x00;
      if (enable == true)
      {
        enableMask = (byte) (0x01 << noteType);
      }
      _data[0] |= enableMask;
    }

    public void SetCommandStack()
    {
      _data[1] |= BIT_5_MASK;
    }

    public void SetCommandReturn()
    {
      _data[1] |= BIT_6_MASK;
    }

  }

  class MessageIn
    extends AbstractMessage
  {

    public MessageIn()
    {
      super();
      // do nothing
    }

    /**
     * all responses are of the form "SLCxEZ"
     * where:
     *   S = STX (1 byte)
     *   L = length (1 byte)
     *   C = ack number & msg type (1 byte)
     *   x = 5 bytes of data (0..255 bytes)
     *   E = ETX (1 byte)
     *   Z = checksum (1 byte)
     */
    public void Read()
    {
      byte[] inputBytes = new byte[100];
      int bytesRead = 0;
      try
      {
        bytesRead = _inputStream.read(inputBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
      if (bytesRead > 0)
      {
        _length = inputBytes[1];
        _ackNum_msgType = inputBytes[2];
        // substract the SLC & EZ bytes from the length
        int dataLength = _length - 5;
        _data = new byte[dataLength];
        for (int indx = 0; indx < _data.length; indx++)
        {
          _data[indx] = inputBytes[indx + 3];
        }
        _checksum = inputBytes[_length - 1];
      }
    }

    public boolean Idling()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_0_MASK);
    }

    public boolean Accepting()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_1_MASK);
    }

    public boolean Escrowed()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_2_MASK);
    }

    public boolean Stacking()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_3_MASK);
    }

    public boolean Stacked()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_4_MASK);
    }

    public boolean Returning()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_5_MASK);
    }

    public boolean Returned()
    {
      return ((_data[0] & BIT_0_MASK) == BIT_6_MASK);
    }

    public boolean Cheated()
    {
      return ((_data[1] & BIT_0_MASK) == BIT_0_MASK);
    }

    public boolean BillRejected()
    {
      return ((_data[1] & BIT_0_MASK) == BIT_1_MASK);
    }

    public boolean BillJammed()
    {
      return ((_data[1] & BIT_0_MASK) == BIT_2_MASK);
    }

    public boolean StackerFull()
    {
      return ((_data[1] & BIT_0_MASK) == BIT_3_MASK);
    }

    public boolean LockableCashBox()
    {
      return ((_data[1] & BIT_0_MASK) == BIT_4_MASK);
    }

    public boolean Initializing()
    {
      return ((_data[2] & BIT_0_MASK) == BIT_0_MASK);
    }

    public boolean ReceivedInvalidCommand()
    {
      return ((_data[2] & BIT_0_MASK) == BIT_1_MASK);
    }

    public boolean DeviceFailure()
    {
      return ((_data[2] & BIT_0_MASK) == BIT_2_MASK);
    }

    public int GetBillType()
    {
      return (_data[2] & BITS_345_MASK);
    }

  }

}
