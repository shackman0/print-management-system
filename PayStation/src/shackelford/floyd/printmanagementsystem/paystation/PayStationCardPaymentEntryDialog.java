package shackelford.floyd.printmanagementsystem.paystation;

import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;



/**
 * <p>Title: PayStationCardPaymentEntryDialog</p>
 * <p>Description:
 * walks the user through a stored value card payment
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PayStationCardPaymentEntryDialog
  extends PayStationCashPaymentEntryDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = -8504000637004486590L;
  protected NumericTextField                _amountField = new NumericTextField();
  protected JLabel                          _amountOnCardLabel = new JLabel();
  protected double                          _amountOnCard = 0d;

  private static Resources                  __resources;

  protected static final String             AMOUNT_TIP = "AMOUNT_TIP";
  protected static final String             AMOUNT_ON_CARD = "AMOUNT_ON_CARD";
  public static final String                CREDIT = "CREDIT";
  protected static final String             CREDIT_TIP = "CREDIT_TIP";

  protected static final String             MISSING_AMOUNT_MSG = "MISSING_AMOUNT_MSG";
  protected static final String             MISSING_AMOUNT_TITLE = "MISSING_AMOUNT_TITLE";
  protected static final String             AMOUNT_GREATER_THAN_CARD_MSG = "AMOUNT_GREATER_THAN_CARD_MSG";
  protected static final String             AMOUNT_GREATER_THAN_CARD_TITLE = "AMOUNT_GREATER_THAN_CARD_TITLE";
  protected static final String             AMOUNT_GREATER_THAN_BALANCE_MSG = "AMOUNT_GREATER_THAN_BALANCE_MSG";
  protected static final String             AMOUNT_GREATER_THAN_BALANCE_TITLE = "AMOUNT_GREATER_THAN_BALANCE_TITLE";
  protected static final String             AMT_GREATER_THAN_MAX_CARD_AMT_MSG = "AMT_GREATER_THAN_MAX_CARD_AMT_MSG";
  protected static final String             AMT_GREATER_THAN_MAX_CARD_AMT_TITLE = "AMT_GREATER_THAN_MAX_CARD_AMT_TITLE";
  protected static final String             AMT_LESS_THAN_MIN_CARD_AMT_MSG = "AMT_LESS_THAN_MIN_CARD_AMT_MSG";
  protected static final String             AMT_LESS_THAN_MIN_CARD_AMT_TITLE = "AMT_LESS_THAN_MIN_CARD_AMT_TITLE";

  protected static final int                CURRENCY_FIELD_WIDTH = 6;


  /**
   * you must call Initialize() after this constructor
   */
  public PayStationCardPaymentEntryDialog (
    AbstractPaymentProtocol  paymentProtocol,
    double                   amountOnCard )
  {
    super(paymentProtocol);
    _amountOnCard = amountOnCard;
  }

  @Override
  protected void BuildAmountLine(JPanel centerPanel, GridBagConstraints constraints)
  {
    // amount on card
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(AMOUNT_ON_CARD),SwingConstants.RIGHT),
      constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountOnCardLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _amountOnCardLabel,
      constraints );

    // amount
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
        new JLabel(GetResources().getProperty(AMOUNT), SwingConstants.RIGHT),
        constraints );

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountField.SetFormat(__formatter);
    _amountField.setColumns(CURRENCY_FIELD_WIDTH);
    _amountField.setToolTipText(GetResources().getProperty(AMOUNT_TIP));
    _amountField.setEnabled(true);

    centerPanel.add (
      _amountField,
      constraints );
  }

  @Override
  protected void AddSouthPanelButtons(Box buttonBox)
  {
    _creditButton = new ActionButton(new CreditAction());
    buttonBox.add(_creditButton);

    super.AddSouthPanelButtons(buttonBox);
  }

  @Override
  protected void AssignFields()
  {
    super.AssignFields();

    _amountField.setText(__formatter.format(0d));
    SetCurrencyLabel(_amountOnCardLabel, _amountOnCard);
  }

  @Override
  protected boolean PanelValid()
  {
    String amountString = new String(_amountField.getText().trim());

    if (amountString.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(MISSING_AMOUNT_MSG),
        GetResources().getProperty(MISSING_AMOUNT_TITLE) );
      _amountField.requestFocus();
      return false;
    }

    Number parsedObj = null;
    try
    {
      parsedObj = (Number) __formatter.parseObject(amountString);
    }
    catch (ParseException excp)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_AMOUNT_MSG),
        GetResources().getProperty(INVALID_AMOUNT_TITLE) );
      _amountField.requestFocus();
      return false;
    }

    double amountDouble = parsedObj.doubleValue();

    // super.PanelValid() requires the amountLabel to be set correctly
    SetAmountLabel(amountDouble);

    if (super.PanelValid() == false)
    {
      return false;
    }

    if (_okButton.isSelected() == true)
    {
      double newCardAmount = _amountOnCard - amountDouble;
      if (newCardAmount < 0d)
      {
        MessageDialog.ShowErrorMessageDialog (
          this,
          GetResources().getProperty(AMOUNT_GREATER_THAN_CARD_MSG),
          GetResources().getProperty(AMOUNT_GREATER_THAN_CARD_TITLE) );
        _amountField.requestFocus();
        return false;
      }
      if (newCardAmount < GetCardPaymentProtocol().GetMinCardAmount())
      {
        MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(AMT_LESS_THAN_MIN_CARD_AMT_MSG));
        Object[] substitutionValues = new Object[] { new Double(GetCardPaymentProtocol().GetMinCardAmount()) };
        String msgString = msgFormat.format(substitutionValues).trim();

        MessageDialog.ShowErrorMessageDialog (
          this,
          msgString,
          GetResources().getProperty(AMT_LESS_THAN_MIN_CARD_AMT_TITLE) );
        _amountField.requestFocus();
        return false;
      }
    }
    else
    if (_creditButton.isSelected() == true)
    {
      double prePaymentBalance = UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue();
      if (amountDouble > prePaymentBalance)
      {
        MessageDialog.ShowErrorMessageDialog (
          this,
          GetResources().getProperty(AMOUNT_GREATER_THAN_BALANCE_MSG),
          GetResources().getProperty(AMOUNT_GREATER_THAN_BALANCE_TITLE) );
        _amountField.requestFocus();
        return false;
      }
      double newCardAmount = _amountOnCard + amountDouble;
      if (newCardAmount > GetCardPaymentProtocol().GetMaxCardAmount())
      {
        MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(AMT_GREATER_THAN_MAX_CARD_AMT_MSG));
        Object[] substitutionValues = new Object[] { new Double(GetCardPaymentProtocol().GetMaxCardAmount()) };
        String msgString = msgFormat.format(substitutionValues).trim();

        MessageDialog.ShowErrorMessageDialog (
          this,
          msgString,
          GetResources().getProperty(AMT_GREATER_THAN_MAX_CARD_AMT_TITLE) );
        _amountField.requestFocus();
        return false;
      }
    }

    return true;
  }

  protected AbstractCardPaymentProtocol GetCardPaymentProtocol()
  {
    return (AbstractCardPaymentProtocol)_paymentProtocol;
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(CREDIT,new Boolean(_creditButton.isSelected()).toString());
    props.setProperty(AMOUNT,new String(_amountField.getText().trim()));
    return props;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  protected boolean CreditAction()
  {
    _creditButton.setSelected(true);
    boolean rc = PanelValid();
    if (rc == true)
    {
      NotifyDialogListeners();
      dispose();
    }
    return rc;
  }

  protected class CreditAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 4693730881744381382L;

    public CreditAction ()
    {
      putValue(Action.NAME, GetResources().getProperty(CREDIT).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(CREDIT_TIP).toString());
    }

    @Override
    public void actionPerformed ( ActionEvent event )
    {
      CreditAction();
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PayStationCardPaymentEntryDialog.class.getName());
  }

}