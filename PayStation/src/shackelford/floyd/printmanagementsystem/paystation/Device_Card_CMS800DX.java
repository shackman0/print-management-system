package shackelford.floyd.printmanagementsystem.paystation;

import java.text.DecimalFormat;
import java.util.StringTokenizer;

import shackelford.floyd.printmanagementsystem.common.MessageDialog;



/**
 * <p>Title: Device_Card_CMS800DX</p>
 * <p>Description:
 * device driver for the CMS 800DX card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Card_CMS800DX
  extends AbstractPaymentDevice
  implements CardPaymentDeviceInterface
{

  protected static final DecimalFormat      __formatter = new DecimalFormat("000.00");

  protected static final String             GET_CARD_PRESENT = "cp\r";
  protected static final String             GET_CARD_VALUE = "xv\r";
  protected static final String             GET_CREDIT_CARD_NUMBER = "xa\r";
  protected static final String             GET_DEBIT_GROUP = "db\r";
  protected static final String             VERIFY_CARD_PRESENT = "vc\r";
  protected static final String             WRITE_BACK_VALUE = "wb ";    // the trailing space is part of the command - leave it there
  protected static final String             CARD_VALUE_RESPONSE = "xv";  // manual says "xa";
  protected static final String             CREDIT_CARD_RESPONSE = "vc";

  protected static final String             DELIMITERS = "> $\r\n";
  protected static final String             CARRIAGE_RETURN_STRING = "\r";
  protected static final byte               CARRIAGE_RETURN_BYTE = '\r';
  public static final double                CREDIT_CARD = -1d;
  public static final double                ERROR_OCCURED = -2d;


  public Device_Card_CMS800DX()
  {
    super();
    Initialize(new Protocol_Card_CMS800DX(this));
  }

  @Override
  protected boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    // when we first connect to the card reader, it sends us some garbage. let's discard it.
    Read();

    // now, let's see if we can write to it
    if (Write(GET_CARD_PRESENT) == false)
    {
      return false;
    }

    // let's see if we can read from it
    if (Read() == null)
    {
      return false;
    }

    return true;
  }

  public void Set_maxCardAmount(String maxCardAmount)
  {
    GetCardPaymentProtocol().SetMaxCardAmount(new Double(maxCardAmount).doubleValue());
  }

  public void Set_minCardAmount(String minCardAmount)
  {
    GetCardPaymentProtocol().SetMinCardAmount(new Double(minCardAmount).doubleValue());
  }

  public AbstractCardPaymentProtocol GetCardPaymentProtocol()
  {
    return (AbstractCardPaymentProtocol)_paymentProtocol;
  }

  @Override
  public boolean CheckForCardPresent()
  {
    String inputString = null;

    if (Write(GET_CARD_PRESENT) == false)
    {
      return false;
    }

    inputString = Read();

    if (inputString == null)
    {
      return false;
    }

    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // discard the echoed command
    tokenizer.nextToken();

    // get the CARD_PRESENT response and discard it
    tokenizer.nextToken();

    // get the value
    String token = tokenizer.nextToken();

    // check if the card is present
    boolean cardPresent = (token.equals("255") == true);   //  0 = no card; 255 = card present

    return cardPresent;
  }

  /**
   * a return value of >= 0 is the value on the debit card
   * a return value of -1 means that the card is a credit card and has no value
   * a return value of -2 means an error occured
   */
  @Override
  public double GetCardValue()
  {
    String inputString = null;
    double cardValue = ERROR_OCCURED;

    if (Write(GET_CARD_VALUE) == false)
    {
      return ERROR_OCCURED;
    }

    inputString = Read();

    if (inputString == null)
    {
      return ERROR_OCCURED;
    }
    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // discard the echoed command
    tokenizer.nextToken();

    // get the response type
    String token = tokenizer.nextToken();

    if (token.equalsIgnoreCase(CARD_VALUE_RESPONSE) == true)
    {
      // get the card value
      token = tokenizer.nextToken();
      cardValue = new Double(token).doubleValue();
    }
    else
    if (token.equalsIgnoreCase(CREDIT_CARD_RESPONSE) == true)
    {
      // it's a credit card and has no value
      cardValue = CREDIT_CARD;
    }
    else
    {
      // oops. what's this?
      new Exception("Unexpected response from reader = \"" + token + "\"").printStackTrace();
      return ERROR_OCCURED;
    }

    return cardValue;
  }

  @Override
  public void AddValueToCard(double amountToAdd)
  {
    WriteBackValue( GetCardValue() + amountToAdd);
  }

  @Override
  public void DeductValueFromCard(double amountToDeduct)
  {
    WriteBackValue( GetCardValue() - amountToDeduct);
  }

  @Override
  public void EjectCard()
  {
    // do nothing
  }

  public boolean VerifyCardPresent()
  {
    String inputString = null;

    if (Write(VERIFY_CARD_PRESENT) == false)
    {
      return false;
    }

    inputString = Read();

    if (inputString == null)
    {
      return false;
    }

    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // get the VERIFY_CARD_PRESENT response and discard it
    String token = tokenizer.nextToken();

    // check if the card is present
    boolean cardPresent = token.equals("255");   //  0 = no card; 255 = card present

    return cardPresent;
  }

  /**
   * a return value of null means an error occured
   */
  public String GetCreditCardNumber()
  {
    String inputString = null;
    String creditCardNumber = null;

    if (Write(GET_CREDIT_CARD_NUMBER) == false)
    {
      return null;
    }

    inputString = Read();

    if (inputString == null)
    {
      return null;
    }

    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // discard the echoed command
    tokenizer.nextToken();

    // discard the response type
    tokenizer.nextToken();

    // read the credit card number
    creditCardNumber = tokenizer.nextToken();

    return creditCardNumber;
  }

  /**
   * a return value of -1 means an error occured
   */
  public int GetDebitGroup()
  {
    String inputString = null;
    int debitGroup = -1;

    if (Write(GET_DEBIT_GROUP) == false)
    {
      return -1;
    }

    inputString = Read();

    if (inputString == null)
    {
      return -1;
    }

    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // discard the echoed command
    tokenizer.nextToken();

    // discard the response type
    tokenizer.nextToken();

    String token = tokenizer.nextToken();
    debitGroup = new Integer(token).intValue();

    return debitGroup;
  }

  public boolean WriteBackValue(double newValue)
  {
    // write the value to the card
    String formattedValue = __formatter.format(newValue);
    String outputString = WRITE_BACK_VALUE + formattedValue + CARRIAGE_RETURN_STRING;
    Write(outputString);

    // see if it could write the value ...
    String inputString = Read();

    if (inputString == null)
    {
      return false;
    }

    StringTokenizer tokenizer = new StringTokenizer(inputString,DELIMITERS);

    // discard the echoed command
    tokenizer.nextToken();

    // discard the echoed value
    tokenizer.nextToken();

    // the rest of the string should say something like "Can not write the value"
    String tokenString = "";
    while (tokenizer.hasMoreTokens() == true)
    {
      tokenString += tokenizer.nextToken();
    }
    if (tokenString.equalsIgnoreCase("cannotwritethevalue") == true)
    {
      MessageDialog.ShowInfoMessageDialog (
        null,
        "Could not write the value \"" + formattedValue + "\" to the card",
        "Write Back Error" );
      return false;
    }

    // verify the amount
    double cardValue = GetCardValue();
    return (cardValue == newValue);
  }

}