package shackelford.floyd.printmanagementsystem.paystation;




/**
 * <p>Title: Device_Cash_XCP</p>
 * <p>Description:
 * device driver for the XCP cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Cash_XCP
  extends AbstractPaymentDevice
  implements CashPaymentDeviceInterface
{


  public Device_Cash_XCP()
  {
    super();
    Initialize(new Protocol_Cash_XCP(this));
  }

  @Override
  protected boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    return true;
  }

  @Override
  public double GetCashValue()
  {
    return 0d;
  }

  @Override
  public void AcceptCash()
  {
    // do nothing
  }

  @Override
  public void RejectCash()
  {
    // do nothing
  }

}