package shackelford.floyd.printmanagementsystem.paystation;

import shackelford.floyd.printmanagementsystem.common.Resources;


/**
 * <p>Title: Protocol_Card_Schlumberger</p>
 * <p>Description:
 * protocol for the Schlumberger card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Protocol_Card_Schlumberger
  extends AbstractCardPaymentProtocol
{

  private static Resources        __resources;

  public Protocol_Card_Schlumberger(Device_Card_Schlumberger paymentDevice)
  {
    super(paymentDevice);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(Protocol_Card_Schlumberger.class.getName());
  }
}



