package shackelford.floyd.printmanagementsystem.paystation;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.HelpMenu;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;
import shackelford.floyd.printmanagementsystem.releasestation.ReleaseStationMainPanel;


/**
 * <p>Title: PayStationMainPanel</p>
 * <p>Description:
 * The main panel of the Pay Station. 
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PayStationMainPanel
  extends AbstractEntry4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID         = -745583033146664485L;

  private final JLabel        _userAccountIDLabel      = new JLabel();

  private final JLabel        _userNameLabel           = new JLabel();

  private final JLabel        _prePaymentLabel         = new JLabel();

  private ActionButton[]      _deviceButtons           = null;

  private static Resources    __resources;

  private static final String FILE                     = "FILE";

  private static final String FILE_EXIT                = "FILE_EXIT";

  private static final String FILE_EXIT_TIP            = "FILE_EXIT_TIP";

  private static final String FILE_RELEASE_STATION     = "FILE_RELEASE_STATION";

  private static final String FILE_RELEASE_STATION_TIP = "FILE_RELEASE_STATION_TIP";

  private static final String SELECT_ONE               = "SELECT_ONE";

  private static final String USER_ACCOUNT_ID          = "USER_ACCOUNT_ID";

  private static final String USER_NAME                = "USER_NAME";

  private static final String PRE_PAYMENT_BALANCE      = "PRE_PAYMENT_BALANCE";

  /**
   * Constructor
   */
  public PayStationMainPanel()
  {
    super();
    Initialize(null, InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable(), UserAccountTable.GetCachedUserAccountRow());
  }

  @Override
  public void Initialize(ListPanelCursor listPanel, AbstractSQL1Table sqlTable, AbstractSQL1Row sqlRow)
  {
    _listPanel = listPanel;
    _sqlTable = sqlTable;
    _sqlRow = sqlRow;

    JMenuBar menuBar = new JMenuBar();
    menuBar.add(CreateFileMenu());
    menuBar.add(new HelpMenu(this));
    setJMenuBar(menuBar);

    super.Initialize(listPanel, sqlTable, sqlRow);
  }

  protected JMenu CreateFileMenu()
  {
    JMenu fileMenu = new JMenu(GetResources().getProperty(FILE));

    fileMenu.add(new ActionMenuItem(new FileReleaseStationAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileExitAction()));

    return fileMenu;
  }

  @Override
  protected Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account ID
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(USER_ACCOUNT_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    northPanel.add(_userAccountIDLabel, constraints);

    // user Name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(USER_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    northPanel.add(_userNameLabel, constraints);

    // pre payment balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    northPanel.add(new JLabel(GetResources().getProperty(PRE_PAYMENT_BALANCE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentLabel.setHorizontalAlignment(SwingConstants.LEFT);
    northPanel.add(_prePaymentLabel, constraints);

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    Box centerPanel = Box.createVerticalBox();

    centerPanel.add(Box.createVerticalStrut(10));
    Box lineBox = Box.createHorizontalBox();
    lineBox.add(Box.createHorizontalGlue());
    lineBox.add(new JLabel(GetResources().getProperty(SELECT_ONE), SwingConstants.CENTER));
    lineBox.add(Box.createHorizontalGlue());
    centerPanel.add(lineBox);

    // show a button for each device
    AbstractPaymentDevice[] paymentDevices = AbstractPaymentDevice.GetPaymentDevices();
    _deviceButtons = new ActionButton[paymentDevices.length];
    for (int indx = 0; indx < paymentDevices.length; indx++)
    {
      centerPanel.add(Box.createVerticalStrut(5));
      lineBox = Box.createHorizontalBox();
      lineBox.add(Box.createHorizontalGlue());
      _deviceButtons[indx] = new ActionButton(new DeviceAction(paymentDevices[indx]));
      lineBox.add(_deviceButtons[indx]);
      lineBox.add(Box.createHorizontalGlue());
      centerPanel.add(lineBox);
    }

    centerPanel.add(Box.createVerticalStrut(10));

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    _saveButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  public void AssignRowToFields()
  {
    super.AssignRowToFields();

    _userAccountIDLabel.setText(GetUserAccountRow().Get_user_account_id());
    _userNameLabel.setText(GetUserAccountRow().Get_user_name());

    _prePaymentLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + GetUserAccountRow().GetFormatted_pre_payment_balance());
  }

  @Override
  public void AssignFieldsToRow()
  {
    super.AssignFieldsToRow();
  }

  protected UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  protected UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable) _sqlTable;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  @Override
  public void EndOfPanel(char action)
  {
    AssignRowToFields();
  }

  @Override
  public void PanelStateChange(char action)
  {
    AssignRowToFields();
  }

  //  private boolean ChargeIsEnabled()
  //  {
  //    return
  //      ( (InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim().length() > 0) ||
  //        (InstallationTable.GetCachedInstallationRow().Get_egold_charge_account().trim().length() > 0) ||
  //        (InstallationTable.GetCachedInstallationRow().Get_paypal_charge_account().trim().length() > 0) );
  //  }

  protected class DeviceAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long     serialVersionUID = -8712427581996067561L;

    private AbstractPaymentDevice _device          = null;

    public DeviceAction(AbstractPaymentDevice device)
    {
      _device = device;
      putValue(Action.NAME, _device.GetPaymentProtocol().GetButtonLabel());
      putValue(Action.SHORT_DESCRIPTION, _device.GetPaymentProtocol().GetButtonToolTip());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _device.GetPaymentProtocol().ProcessPayment();
      AssignRowToFields();
    }
  }

  void FileExitAction()
  {
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
    dispose();
  }

  private class FileExitAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8739411689571161826L;

    public FileExitAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_EXIT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_EXIT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileExitAction();
    }
  }

  void FileReleaseStationAction()
  {
    ReleaseStationMainPanel panel = new ReleaseStationMainPanel();
    panel.AddPanelListener(this);
    panel.setVisible(true);
    panel.RefreshList();
    invalidate();
  }

  private class FileReleaseStationAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8173018768564264895L;

    public FileReleaseStationAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_RELEASE_STATION));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_RELEASE_STATION_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileReleaseStationAction();
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PayStationMainPanel.class.getName());
  }

}
