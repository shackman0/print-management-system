package shackelford.floyd.printmanagementsystem.paystation;




/**
 * <p>Title: Device_Card_JamexPaymentController</p>
 * <p>Description:
 * device driver for the Jamex card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Card_JamexPaymentController
  extends AbstractJamexPaymentControllerDevice
  implements CardPaymentDeviceInterface
{

  public Device_Card_JamexPaymentController()
  {
    super();
    Initialize(new Protocol_Card_JamexPaymentController(this));
  }

  @Override
  public boolean CheckForCardPresent()
  {
    return (SystemStatusRequest() == SYSTEM_STATUS_VALUE_ON_CARD);
  }

  @Override
  public double GetCardValue()
  {
    return ReadValue();
  }

  @Override
  public void AddValueToCard(double amountToAdd)
  {
    AddValue(amountToAdd);
  }

  @Override
  public void DeductValueFromCard(double amountToDeduct)
  {
    DeductValue(amountToDeduct);
  }

  @Override
  public void EjectCard()
  {
    ReturnCardOrCoin();
  }

}