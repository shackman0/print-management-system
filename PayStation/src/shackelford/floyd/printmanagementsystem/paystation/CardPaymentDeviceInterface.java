package shackelford.floyd.printmanagementsystem.paystation;




/**
 * <p>Title: CardPaymentDeviceInterface</p>
 * <p>Description:
 * interface for stored value card readers
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface CardPaymentDeviceInterface
{

  public boolean CheckForCardPresent();

  public double GetCardValue();

  public void AddValueToCard(double amountToAdd);

  public void DeductValueFromCard(double amountToDeduct);

  public void EjectCard();

}