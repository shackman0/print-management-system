package shackelford.floyd.printmanagementsystem.paystation;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractLoginDialog;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: PayStationLoginDialog</p>
 * <p>Description:
 * the user must log in before he can use the pay station.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PayStationLoginDialog
  extends AbstractLoginDialog
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID              = 6405206474615179246L;

  private ActionButton        _guestButton                  = null;

  private JPasswordField      _passphraseField              = null;

  private JTextField          _userAccountIDField           = null;

  private RulesRow            _guestHoldRuleRow             = null;

  private RulesRow            _requireUserPasswordRuleRow   = null;

  static Resources            __resources;

  public static final String  GUEST                         = "GUEST";

  private static final String GUEST_TIP                     = "GUEST_TIP";

  private static final String PASSPHRASE                    = "PASSPHRASE";

  private static final String PASSPHRASE_TIP                = "PASSPHRASE_TIP";

  public static final String  USER_ACCOUNT_ID               = "USER_ACCOUNT_ID";

  private static final String USER_ACCOUNT_ID_TIP           = "USER_ACCOUNT_ID_TIP";

  private static final String USER_ACCOUNT_ID_BLANK_MSG     = "USER_ACCOUNT_ID_BLANK_MSG";

  private static final String USER_ACCOUNT_ID_BLANK_TITLE   = "USER_ACCOUNT_ID_BLANK_TITLE";

  private static final String INVALID_PASSPHRASE_MSG        = "INVALID_PASSPHRASE_MSG";

  private static final String INVALID_PASSPHRASE_TITLE      = "INVALID_PASSPHRASE_TITLE";

  private static final String INVALID_USER_ACCOUNT_ID_MSG   = "INVALID_USER_ACCOUNT_ID_MSG";

  private static final String INVALID_USER_ACCOUNT_ID_TITLE = "INVALID_USER_ACCOUNT_ID_TITLE";

  /**
   * Constructor
   * @param owner
   */
  public PayStationLoginDialog(Frame owner)
  {
    super(owner, true);

    _guestHoldRuleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.ALLOW_GUEST_PRINT_HOLD);
    _requireUserPasswordRuleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.REQUIRE_USER_PASSWORD);

    Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account ID
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(USER_ACCOUNT_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDField = new JTextField(FIELD_WIDTH);
    _userAccountIDField.setToolTipText(__resources.getProperty(USER_ACCOUNT_ID_TIP));
    centerPanel.add(_userAccountIDField, constraints);

    // password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true))
    {
      centerPanel.add(new JLabel(__resources.getProperty(PASSPHRASE), SwingConstants.RIGHT), constraints);
    }

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphraseField = new JPasswordField(FIELD_WIDTH);
    _passphraseField.setText("");
    _passphraseField.setToolTipText(__resources.getProperty(PASSPHRASE_TIP));

    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true))
    {
      centerPanel.add(_passphraseField, constraints);
    }

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    // for now, until we figure out how to accomodate guests
    _guestButton.setVisible(false);

    return container;
  }

  @Override
  protected void AddSouthPanelButtons(Box buttonBox)
  {
    super.AddSouthPanelButtons(buttonBox);
    _guestButton = new ActionButton(new GuestAction());

    if ((_guestHoldRuleRow != null) && (_guestHoldRuleRow.RuleIsYes() == true))
    {
      buttonBox.add(_guestButton);
    }
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(USER_ACCOUNT_ID, new String(_userAccountIDField.getText()));
    props.setProperty(PASSPHRASE, new String(_passphraseField.getPassword()));
    props.setProperty(GUEST, new Boolean(_guestButton.isSelected()).toString());
    return props;
  }

  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    String userAccountID = new String(_userAccountIDField.getText().trim());
    String passphrase = new String(_passphraseField.getPassword()).trim();

    if (userAccountID.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(USER_ACCOUNT_ID_BLANK_MSG), __resources.getProperty(USER_ACCOUNT_ID_BLANK_TITLE));
      _userAccountIDField.requestFocus();
      return false;
    }

    if (UserAccountTable.GetCachedUserAccountRow(userAccountID) == null)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_USER_ACCOUNT_ID_MSG), __resources.getProperty(INVALID_USER_ACCOUNT_ID_TITLE));
      _userAccountIDField.setText("");
      _userAccountIDField.requestFocus();
      _passphraseField.setText("");
      return false;
    }

    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true) && (UserAccountTable.GetCachedUserAccountRow().Get_user_passphrase().equals(passphrase) == false))
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PASSPHRASE_MSG), __resources.getProperty(INVALID_PASSPHRASE_TITLE));
      _passphraseField.setText("");
      _passphraseField.requestFocus();
      return false;
    }

    if (UserAccountTable.GetCachedUserAccountRow().StatusIsEnabled() == false)
    {
      MessageDialog.ShowErrorMessageDialog(null, "Your user account is not enabled.", "User Account Not Enabled");
      return false;
    }

    return true;
  }

  protected void GuestAction()
  {
    _guestButton.setSelected(true);
    _userAccountIDField.setText(_guestHoldRuleRow.GetRuleParm(0));
    NotifyDialogListeners();
    dispose();
  }

  private class GuestAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 7960036119816024324L;

    public GuestAction()
    {
      putValue(Action.NAME, __resources.getProperty(GUEST));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(GUEST_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      GuestAction();
    }
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PayStationLoginDialog.class.getName());
  }

}
