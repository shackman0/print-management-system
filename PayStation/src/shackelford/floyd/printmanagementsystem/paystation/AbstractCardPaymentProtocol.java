package shackelford.floyd.printmanagementsystem.paystation;

import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Properties;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AbstractCardPaymentProtocol</p>
 * <p>Description:
 * abstract protocol for card readers.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractCardPaymentProtocol
  extends AbstractPaymentProtocol
{

  /**
   * _cardPaymentDevice: the physical device that reads the user's stored value card
   */
  protected CardPaymentDeviceInterface _cardPaymentDevice;

  /**
   * _maxCardAmount: the maximum add value amount
   */
  protected double                     _maxCardAmount         = UNDEFINED_DOUBLE;

  /**
   * _minCardAmount: the minimum add value amount
   */
  protected double                     _minCardAmount         = UNDEFINED_DOUBLE;

  /*
   * resource keys
   */
  protected static final String        NOT_A_DEBIT_CARD_MSG   = "NOT_A_DEBIT_CARD_MSG";

  protected static final String        NOT_A_DEBIT_CARD_TITLE = "NOT_A_DEBIT_CARD_TITLE";

  
  /**
   * Constructor
   * @param cardPaymentDevice
   */
  public AbstractCardPaymentProtocol(CardPaymentDeviceInterface cardPaymentDevice)
  {
    super();
    _cardPaymentDevice = cardPaymentDevice;
  }

  public double GetMaxCardAmount()
  {
    return _maxCardAmount;
  }

  public void SetMaxCardAmount(double maxCardAmount)
  {
    _maxCardAmount = maxCardAmount;
  }

  public double GetMinCardAmount()
  {
    return _minCardAmount;
  }

  public void SetMinCardAmount(double minCardAmount)
  {
    _minCardAmount = minCardAmount;
  }

  @Override
  public void ProcessPayment()
  {
    // let's get a valid card into the reader ...
    boolean validCardInReader = false;
    do
    {
      boolean cardInReader = _cardPaymentDevice.CheckForCardPresent();
      while (cardInReader == false)
      {
        // tell the user to put his card in the reader
        int rc = JOptionPane.showConfirmDialog(null, // parent component
          GetResources().getProperty(INSERT_PAYMENT_INSTRUMENT_MSG), GetResources().getProperty(INSERT_PAYMENT_INSTRUMENT_TITLE), JOptionPane.OK_CANCEL_OPTION, JOptionPane.INFORMATION_MESSAGE);

        // see which key the user pressed
        if (rc == JOptionPane.CANCEL_OPTION)
        {
          // the user cancelled out of the dialog, so we're done
          return;
        }

        // check to see if the card is in the reader. if it is, we can leave this loop
        cardInReader = _cardPaymentDevice.CheckForCardPresent();

      }

      // check to see if this is a credit card or a debit card
      double cardValue = _cardPaymentDevice.GetCardValue();
      if (cardValue >= 0d)
      {
        validCardInReader = true;
      }
      else
      {
        // this is not a valid debit card
        MessageDialog.ShowErrorMessageDialog(null, GetResources().getProperty(NOT_A_DEBIT_CARD_MSG), GetResources().getProperty(NOT_A_DEBIT_CARD_TITLE));
      }
    }
    while (validCardInReader == false);

    double cardValue = _cardPaymentDevice.GetCardValue();

    // display the payment entry panel
    PayStationCardPaymentEntryDialog dialog = new PayStationCardPaymentEntryDialog(this, cardValue);
    dialog.Initialize();
    Properties dialogProperties = dialog.WaitOnDialog();

    // see which key the user pressed
    if (dialogProperties.getProperty(AbstractDialog.CANCEL).equalsIgnoreCase(AbstractDialog.TRUE) == true)
    {
      // the user cancelled out of the dialog, so we're done
      return;
    }

    // the user must have hit the OK key, so continue processing ...
    // get the amount the user wants to move
    Number parsedObj = null;
    try
    {
      parsedObj = (Number) __formatter.parseObject(dialogProperties.getProperty(PayStationCashPaymentEntryDialog.AMOUNT));
    }
    catch (ParseException excp)
    {
      throw new RuntimeException(excp);
    }

    double transactionAmount = parsedObj.doubleValue();

    // see which way the user wants to move this money ...
    // OK means move the money from the card into the user's pre payment balance
    //   in which case we just leave the transactionAmount alone
    // CREDIT means move the money from the user's pre payment balance into the debit card
    //   in which case, we'll change the sign of the transactionAmount
    if (dialogProperties.getProperty(AbstractDialog.OK).equalsIgnoreCase(AbstractDialog.TRUE) == true)
    {
      _cardPaymentDevice.DeductValueFromCard(transactionAmount);
    }
    else
      if (dialogProperties.getProperty(PayStationCardPaymentEntryDialog.CREDIT).equalsIgnoreCase(AbstractDialog.TRUE) == true)
      {
        _cardPaymentDevice.AddValueToCard(transactionAmount);
        transactionAmount = -transactionAmount;
      }

    UpdateDatabase(transactionAmount);

    double newCardValue = _cardPaymentDevice.GetCardValue();

    _cardPaymentDevice.EjectCard();

    // tell the user the transaction is complete
    do
    {
      // tell the user to remove his card
      MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(TRANSACTION_COMPLETE_MSG));
      Object[] substitutionValues = new Object[] { UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance(), new Double(newCardValue) };
      String msgString = msgFormat.format(substitutionValues).trim();

      MessageDialog.ShowInfoMessageDialog(null, msgString, GetResources().getProperty(TRANSACTION_COMPLETE_TITLE));

    }
    while (_cardPaymentDevice.CheckForCardPresent() == true);

  }

}
