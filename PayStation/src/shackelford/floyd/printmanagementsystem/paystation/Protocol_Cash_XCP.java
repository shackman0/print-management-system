package shackelford.floyd.printmanagementsystem.paystation;

import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: Protocol_Cash_XCP</p>
 * <p>Description:
 * protocol for the XCP cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Protocol_Cash_XCP
  extends AbstractCashPaymentProtocol
{

  private static Resources        __resources;

  /**
   * Constructor
   * @param paymentDevice
   */
  public Protocol_Cash_XCP(Device_Cash_XCP  paymentDevice)
  {
    super(paymentDevice);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(Protocol_Cash_XCP.class.getName());
  }
}