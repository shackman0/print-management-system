package shackelford.floyd.printmanagementsystem.paystation;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;



/**
 * <p>Title: PayStationCashPaymentEntryDialog</p>
 * <p>Description:
 * walks the user through the pre-payment process and accepts a pre-payment.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PayStationCashPaymentEntryDialog
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = -7979300828316011508L;
  protected Double                          _previousBalance = null;
  protected JLabel                          _amountLabel = new JLabel();
  protected JLabel                          _prePaymentBalanceLabel = new JLabel();
  protected JLabel                          _newPrePaymentBalanceLabel = new JLabel();
  protected ActionButton                    _creditButton = null;
  protected AbstractPaymentProtocol         _paymentProtocol = null;

  private static Resources                  __resources;
  protected static final DecimalFormat      __formatter = new DecimalFormat("#,##0.00");

  protected static final String             PRE_PAYMENT_BALANCE = "PRE_PAYMENT_BALANCE";
  protected static final String             NEW_PRE_PAYMENT_BALANCE = "NEW_PRE_PAYMENT_BALANCE";
  public static final String                AMOUNT = "AMOUNT";

  protected static final String             DEPOSIT_MONEY_NOW_MSG = "DEPOSIT_MONEY_NOW_MSG";
  protected static final String             INVALID_AMOUNT_MSG = "INVALID_AMOUNT_MSG";
  protected static final String             INVALID_AMOUNT_TITLE = "INVALID_AMOUNT_TITLE";

  protected static final int                AMOUNT_WIDTH = 60;


  /**
   * you must call Initialize() after this constructor
   */
  public PayStationCashPaymentEntryDialog(AbstractPaymentProtocol paymentProtocol)
  {
    super (null,true);
    _paymentProtocol = paymentProtocol;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // instructions
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(DEPOSIT_MONEY_NOW_MSG),SwingConstants.LEFT),
      constraints);

    // current balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(PRE_PAYMENT_BALANCE),SwingConstants.RIGHT),
      constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentBalanceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    centerPanel.add (
      _prePaymentBalanceLabel,
      constraints );

    // this is separated out so child classes can change this behavior
    BuildAmountLine(centerPanel,constraints);

    // new balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(NEW_PRE_PAYMENT_BALANCE),SwingConstants.RIGHT),
      constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _newPrePaymentBalanceLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    centerPanel.add (
      _newPrePaymentBalanceLabel,
      constraints );

    return centerPanel;
  }

  protected void BuildAmountLine(JPanel centerPanel, GridBagConstraints constraints)
  {
    // amount
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
        new JLabel(GetResources().getProperty(AMOUNT), SwingConstants.RIGHT),
        constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountLabel.setHorizontalAlignment(SwingConstants.RIGHT);
    centerPanel.add (
      _amountLabel,
      constraints );
  }

  @Override
  protected void AssignFields()
  {
    SetCurrencyLabel ( _prePaymentBalanceLabel, UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue() );
    SetAmountLabel(0d);
  }

  /**
   * this method if for use by the payment devices to update the amount deposited field
   * as cash is deposited by the user.
   */
  public void SetAmountLabel(double amount)
  {
    SetCurrencyLabel ( _amountLabel, amount );
    SetCurrencyLabel ( _newPrePaymentBalanceLabel, amount + UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue() );
  }

  protected void SetCurrencyLabel(JLabel label, double amount)
  {
    label.setText(
      InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + " " +
      __formatter.format(amount) );
    Dimension dimension = label.getPreferredSize();
    dimension.width = AMOUNT_WIDTH;
    label.setPreferredSize(dimension);
    label.invalidate();
  }

  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    // get the amount, stripping off the leading currency symbol
    String amountString = new String(_amountLabel.getText().trim().substring(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().length()).trim());

    Number parsedObj = null;
    try
    {
      parsedObj = (Number) __formatter.parseObject(amountString);
    }
    catch (ParseException excp)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_AMOUNT_MSG),
        GetResources().getProperty(INVALID_AMOUNT_TITLE) );
      return false;
    }

    double amountDouble = parsedObj.doubleValue();

    if (amountDouble <= 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_AMOUNT_MSG),
        GetResources().getProperty(INVALID_AMOUNT_TITLE) );
      return false;
    }

    return true;
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    // get the amount, stripping off the leading currency symbol
    props.setProperty (
      AMOUNT,
      _amountLabel.getText().trim().substring(InstallationTable.GetCachedInstallationRow().Get_currency_symbol().length()).trim() );
    return props;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PayStationCashPaymentEntryDialog.class.getName());
  }

}