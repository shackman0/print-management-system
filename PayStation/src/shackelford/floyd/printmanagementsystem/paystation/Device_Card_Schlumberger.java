package shackelford.floyd.printmanagementsystem.paystation;

import java.io.IOException;
import java.text.DecimalFormat;


/**
 * <p>Title: Device_Card_Schlumberger</p>
 * <p>Description:
 * device driver for the Schlumberger card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Card_Schlumberger
  extends AbstractPaymentDevice
  implements CardPaymentDeviceInterface
{

  // all messages are constructed as follows:  STX data ETX LRC

  protected static final DecimalFormat __formatter                = new DecimalFormat("000.00");

  protected static final byte          STX                        = 0x02;                          // start text

  protected static final byte          ETX                        = 0x03;                          // end text

  protected static final byte          ACK                        = 0x06;                          // acknowledge

  protected static final byte          NAK                        = 0x15;                          // negative acknowledge

  protected static final String        ACK_STRING                 = new String(new byte[] { ACK });

  protected static final String        NAK_STRING                 = new String(new byte[] { NAK });

  protected static final byte          DISPLAY_MESSAGE            = 'D';

  protected static final byte          EJECT                      = 'E';

  protected static final byte          FACILITY_READ              = 'F';

  protected static final byte          KILL                       = 'K';

  protected static final byte          QUERY                      = 'Q';

  protected static final byte          READ_CARD                  = 'R';

  protected static final byte          STORE                      = 'S';

  protected static final byte          UPDATE_CARD                = 'U';

  protected static final byte          VIEW                       = 'V';

  protected static final byte          WRITE_CARD                 = 'W';

  protected static final byte          NO_CARD_IN_READER          = 0x00;

  protected static final byte          CARD_WAITING_TO_BE_REMOVED = 0x02;

  protected static final byte          CARD_FULLY_SEATED          = 0x0c;

  protected static final int           DELAY                      = 100;

  public Device_Card_Schlumberger()
  {
    super();
    Initialize(new Protocol_Card_Schlumberger(this));
  }

  @Override
  protected boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    return true;
  }

  @Override
  public boolean CheckForCardPresent()
  {
    MessageIn messageIn = Query();
    return (messageIn.GetData().getBytes()[0] == CARD_FULLY_SEATED);
  }

  @Override
  public double GetCardValue()
  {
    MessageIn messageIn = ReadCard();
    String cardValueString = messageIn.GetData().substring(0, 5);
    return new Double(cardValueString).doubleValue();
  }

  @Override
  public void AddValueToCard(double amountToAdd)
  {
    double cardValue = GetCardValue();
    cardValue += amountToAdd;
    UpdateCard(cardValue);
  }

  @Override
  public void DeductValueFromCard(double amountToDeduct)
  {
    double cardValue = GetCardValue();
    cardValue -= amountToDeduct;
    UpdateCard(cardValue);
  }

  @Override
  public void EjectCard()
  {
    Eject();
  }

  // the following methods are the "low level" methods that implement
  // the various card reader commands

  /**
   * the response comes back as "ASDEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   D = the display command (1 byte)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public void DisplayMessage(String messageString)
  {
    new MessageOut(DISPLAY_MESSAGE, messageString).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);
  }

  /**
   * the response comes back as "A"
   * where:
   *   A = ACK or NAK (1 byte)
   */
  public void Eject()
  {
    new MessageOut(EJECT, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  /**
   * the response comes back as "ASRxxxxxffffEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   R = the read command (1 byte)
   *   xxxxx = the value on the card (with implied decimal 000.00) (5 bytes)
   *   ffff = the facility or site code (4 bytes)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public MessageIn FacilityRead()
  {
    MessageOut messageOut = new MessageOut(FACILITY_READ, "");
    Write(messageOut.GetAsString());

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);

    return messageIn;
  }

  /**
   * the response comes back as "A"
   * where:
   *   A = ACK or NAK (1 byte)
   */
  public void Kill()
  {
    new MessageOut(KILL, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  /**
   * the response comes back as "ASQsEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   Q = the read command (1 byte)
   *   s = the status (1 byte)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   *
   * where s can assume the following values:
   *   0x00 = no card in reader
   *   0x02 = card waiting to be removed
   *   0x0c = card fully seated
   *   0x?? = reader jam or malfunction
   */
  public MessageIn Query()
  {
    MessageOut messageOut = new MessageOut(QUERY, "");
    Write(messageOut.GetAsString());

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);

    return messageIn;
  }

  /**
   * the response comes back as "ASRxxxxxEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   R = the read command (1 byte)
   *   xxxxx = the value on the card (with implied decimal 000.00) (5 bytes)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public MessageIn ReadCard()
  {
    new MessageOut(READ_CARD, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);

    return messageIn;
  }

  /**
   * the response comes back as "ASTEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   T = the store (S) command (1 byte)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public void Store(double value)
  {
    String formattedValue = __formatter.format(value);
    formattedValue = formattedValue.substring(0, 3) + formattedValue.substring(4, 6);

    new MessageOut(STORE, formattedValue).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);
  }

  /**
   * the response comes back as "ASUEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   U = the update command (1 byte)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public void UpdateCard(double value)
  {
    String formattedValue = __formatter.format(value);
    formattedValue = formattedValue.substring(0, 3) + formattedValue.substring(4, 6);

    new MessageOut(UPDATE_CARD, formattedValue).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);
  }

  /**
   * the response comes back as "ASVxxxxxEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   V = the view command (1 byte)
   *   xxxxx = the value in the device's memory (with implied decimal 000.00) (5 bytes)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public MessageIn View()
  {
    new MessageOut(VIEW, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);

    return messageIn;
  }

  /**
   * the response comes back as "ASWEL"
   * where:
   *   A = ACK or NAK (1 byte)
   *   S = STX (1 byte)
   *   W = the write command (1 byte)
   *   E = ETX (1 byte)
   *   L = LRC (1 byte)
   */
  public void WriteCard(double value)
  {
    String formattedValue = __formatter.format(value);
    formattedValue = formattedValue.substring(0, 3) + formattedValue.substring(4, 6);

    new MessageOut(WRITE_CARD, formattedValue).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    Write(ACK);
  }

  abstract class AbstractMessage
  {
    protected byte   _command = 0;

    protected String _data    = null;

    protected byte   _lrc     = 0;

    public AbstractMessage()
    {
      // do nothing
    }

    public byte GetCommand()
    {
      return _command;
    }

    public String GetData()
    {
      return _data;
    }

    public byte GetLRC()
    {
      return _lrc;
    }

    protected String GetAsString()
    {
      SetLRC();
      String messageAsString = new String(STX + _command + _data + ETX + _lrc);
      return messageAsString;
    }

    protected void SetLRC()
    {
      _lrc = CalculateLRC();
    }

    protected byte CalculateLRC()
    {
      byte lrc = 0;
      byte[] bytes = _data.getBytes();
      for (int indx = 0; indx < bytes.length; indx++)
      {
        lrc ^= bytes[indx];
      }
      lrc ^= ETX;
      return lrc;
    }

  }

  private class MessageOut
    extends AbstractMessage
  {

    public MessageOut(byte command, String data)
    {
      super();
      _command = command;
      _data = data;
    }

    public void Write()
    {
      try
      {
        _outputStream.write(GetAsString().getBytes());
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
    }

  }

  private class MessageIn
    extends AbstractMessage
  {

    public MessageIn()
    {
      super();
      // do nothing
    }

    /**
     * all responses are of the form "ASCxEL"
     * where:
     *   A = ACK or NAK (1 byte)
     *     if NAK, then the response ends and SCxEL are not returned
     *     if ACK, then SCxEL are returned
     *   S = STX (1 byte)
     *   C = command character (1 byte)
     *   x = 0 or more bytes of data (format specific to the command) (0...n bytes)
     *   E = ETX (1 byte)
     *   L = LRC (1 byte)
     */
    public void Read()
    {
      String inputString = NAK_STRING;
      byte[] inputBytes = new byte[100];
      int bytesRead = 0;
      try
      {
        bytesRead = _inputStream.read(inputBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
      if (bytesRead > 0)
      {
        inputString = new String(inputBytes, 0, bytesRead);

        if (inputString.substring(0, 1).getBytes()[0] == ACK)
        {
          _command = inputString.substring(2, 3).getBytes()[0];
          _data = inputString.substring(3, inputString.length() - 2);
          _lrc = inputString.substring(inputString.length() - 1, inputString.length()).getBytes()[0];
        }
      }
    }

  }

}
