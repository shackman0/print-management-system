package shackelford.floyd.printmanagementsystem.paystation;

import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: Protocol_Cash_GBA</p>
 * <p>Description:
 * protocol for the GBA cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Protocol_Cash_GBA
  extends AbstractCashPaymentProtocol
{

  private static Resources        __resources;

  public Protocol_Cash_GBA(Device_Cash_GBA paymentDevice)
  {
    super(paymentDevice);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(Protocol_Cash_GBA.class.getName());
  }
}