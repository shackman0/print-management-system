package shackelford.floyd.printmanagementsystem.paystation;

import java.util.Properties;

import javax.swing.SwingWorker;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;



/**
 * <p>Title: AbstractCashPaymentProtocol</p>
 * <p>Description:
 * abstract protocol for cash acceptors
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractCashPaymentProtocol
  extends AbstractPaymentProtocol
{

  protected CashPaymentDeviceInterface      _cashPaymentDevice;
  protected boolean                         _updateCashLabel;
    // this is a semaphore used to tell the update cash label loop when to quit
    //   true = keep looping
    //   false = stop looping
  protected boolean                         _workerRunning;
    // this is a semaphore used to tell the main thread when the worker thread
    // is running so that the main thread doesn't proceed with processing the
    // dialog until the worker thread quiesces
    //   true = the worker thread is still running - wait
    //   false = the worker thread is no longer running - you may proceed

  protected static final int                POLL_DELAY = 100;

  public AbstractCashPaymentProtocol(CashPaymentDeviceInterface cashPaymentDevice)
  {
    super();
    _cashPaymentDevice = cashPaymentDevice;
  }

  @Override
  public void ProcessPayment()
  {
    // create the payment entry panel
    final PayStationCashPaymentEntryDialog dialog = new PayStationCashPaymentEntryDialog(this);
    dialog.Initialize();

    // launch the thread that updates how much cash the user has deposited into
    // the cash payment device
    _updateCashLabel = true;
    _workerRunning = true;
    new SwingWorker<Object,Object>()
    {
      @Override
      public Object doInBackground()
      {
        UpdateCashLabel(dialog,POLL_DELAY);
        return null;
      }
    }.execute();

    // display the dialog and wait for the user to exit the dialog
    Properties dialogProperties = dialog.WaitOnDialog();

    // stop the worker thread. it no longer needs to send the updates to the dialog
    _updateCashLabel = false;

    while (_workerRunning == true)
    {
      // do nothing
    }

    // see which key the user pressed
    if (dialogProperties.getProperty(AbstractDialog.CANCEL).equalsIgnoreCase(AbstractDialog.TRUE) == true)
    {
      // the user canceled out of the dialog, so we're done
      // reject the cash the user deposited
      _cashPaymentDevice.RejectCash();
      return;
    }

    // the user must have hit the OK key, so continue processing ...

    // get the amount the user deposited into the cash payment device
    double transactionAmount = _cashPaymentDevice.GetCashValue();

    // accept the cash deposited
    _cashPaymentDevice.AcceptCash();

    UpdateDatabase(transactionAmount);

    // tell the user the transaction is complete
    MessageDialog.ShowInfoMessageDialog (
      null,
      GetResources().getProperty(TRANSACTION_COMPLETE_MSG),
      GetResources().getProperty(TRANSACTION_COMPLETE_TITLE) );
  }

  protected void UpdateCashLabel (
    PayStationCashPaymentEntryDialog entryDialog,
    int                              sleepInterval )
  {
    try
    {
      while (_updateCashLabel == true)
      {
        entryDialog.SetAmountLabel(_cashPaymentDevice.GetCashValue());
        Thread.sleep(sleepInterval);
      }
    }
    catch (InterruptedException excp)
    {
      // do nothing
      // this is one of the ways we can be notified to stop
    }
    finally
    {
      _workerRunning = false;
    }
  }

}