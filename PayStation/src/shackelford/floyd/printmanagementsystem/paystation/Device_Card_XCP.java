package shackelford.floyd.printmanagementsystem.paystation;

import java.io.IOException;
import java.text.DecimalFormat;


/**
 * <p>Title: Device_Card_XCP</p>
 * <p>Description:
 * device driver for the XCP card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Card_XCP
  extends AbstractPaymentDevice
  implements CardPaymentDeviceInterface
{

  protected static final DecimalFormat __formatter                 = new DecimalFormat("000.00");

  protected static final byte          STX                         = 0x02;                       // start text

  protected static final byte          ETX                         = 0x03;                       // end text

  protected static final byte          ACK                         = 0x06;                       // acknowledge

  protected static final byte          NAK                         = 0x15;                       // negative acknowledge

  protected static final byte          DATA_PACKET_CHECKSUM_ERROR  = (byte) 0xff;

  protected static final byte          UNRECOGNIZED_COMMAND_PACKET = (byte) 0xfe;

  protected static final byte          INCOMPLETE_COMMAND_PACKET   = (byte) 0xfd;

  protected static final byte          COMMAND_READ_VALUE          = (byte) 0x80;

  protected static final byte          COMMAND_DEBIT_VALUE         = (byte) 0x83;

  protected static final byte          COMMAND_CREDIT_VALUE        = (byte) 0x84;

  protected static final byte          COMMAND_CARD_IN_STATUS      = (byte) 0xa0;

  protected static final byte          COMMAND_EJECT_CARD          = (byte) 0xa1;

  protected static final int           DELAY                       = 100;

  /**
   * Constructor
   */
  public Device_Card_XCP()
  {
    super();
    Initialize(new Protocol_Card_XCP(this));
  }

  @Override
  protected boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    return true;
  }

  @Override
  public boolean CheckForCardPresent()
  {
    MessageIn messageIn = CardInStatus();
    return (messageIn.GetData().getBytes()[0] == ACK);
  }

  @Override
  public double GetCardValue()
  {
    MessageIn messageIn = ReadValue();
    return new Double(messageIn.GetData()).doubleValue();
  }

  @Override
  public void AddValueToCard(double amountToAdd)
  {
    CreditValue(amountToAdd);
  }

  @Override
  public void DeductValueFromCard(double amountToDeduct)
  {
    DebitValue(amountToDeduct);
  }

  @Override
  public void EjectCard()
  {
    Eject();
  }

  // the following methods are the "low level" methods that implement
  // the various card reader commands

  public MessageIn ReadValue()
  {
    new MessageOut(COMMAND_READ_VALUE, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    return messageIn;
  }

  public void DebitValue(double value)
  {
    String formattedValue = __formatter.format(value);
    formattedValue = formattedValue.substring(0, 3) + formattedValue.substring(4, 6);

    new MessageOut(COMMAND_DEBIT_VALUE, formattedValue).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  public void CreditValue(double value)
  {
    String formattedValue = __formatter.format(value);
    formattedValue = formattedValue.substring(0, 3) + formattedValue.substring(4, 6);

    new MessageOut(COMMAND_CREDIT_VALUE, formattedValue).Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  /**
   * returns ACK if card in
   */
  public MessageIn CardInStatus()
  {
    new MessageOut(COMMAND_CARD_IN_STATUS, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();

    return messageIn;
  }

  public void Eject()
  {
    new MessageOut(COMMAND_EJECT_CARD, "").Write();

    Delay(DELAY);

    MessageIn messageIn = new MessageIn();
    messageIn.Read();
  }

  abstract class AbstractMessage
  {
    protected byte   _slaveDeviceAddress = 0;

    protected byte   _command            = 0;

    protected String _data               = null;

    protected byte   _checksum           = 0;

    public AbstractMessage()
    {
      // do nothing
    }

    public byte GetCommand()
    {
      return _command;
    }

    public String GetData()
    {
      return _data;
    }

    public byte GetSlaveDeviceAddress()
    {
      return _slaveDeviceAddress;
    }

    public byte GetChecksum()
    {
      return _checksum;
    }

    protected String GetAsString()
    {
      SetChecksum();
      String messageAsString = new String(STX + _slaveDeviceAddress + _command + _data.length() + _data + ETX + _checksum);
      return messageAsString;
    }

    protected void SetChecksum()
    {
      _checksum = CalculateChecksum();
    }

    protected byte CalculateChecksum()
    {
      int checksum = STX ^ _slaveDeviceAddress ^ _command ^ _data.length() ^ ETX;
      byte[] bytes = _data.getBytes();
      for (int indx = 0; indx < bytes.length; indx++)
      {
        checksum ^= bytes[indx];
      }
      return (byte) checksum;
    }

  }

  private class MessageOut
    extends AbstractMessage
  {

    public MessageOut(byte command, String data)
    {
      super();
      _command = command;
      _data = data;
    }

    public void Write()
    {
      try
      {
        _outputStream.write(GetAsString().getBytes());
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
    }

  }

  private class MessageIn
    extends AbstractMessage
  {

    public MessageIn()
    {
      super();
      // do nothing
    }

    /**
     * all responses are of the form "SACLxEZ"
     * where:
     *   S = STX (1 byte)
     *   A = address (1 byte)
     *   C = command character (1 byte)
     *   L = data length (0..255) (1 byte)
     *   x = L bytes of data (0..255 bytes)
     *   E = ETX (1 byte)
     *   Z = checksum (1 byte)
     */
    public void Read()
    {
      String inputString = null;
      byte[] inputBytes = new byte[100];
      int bytesRead = 0;
      try
      {
        bytesRead = _inputStream.read(inputBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
      if (bytesRead > 0)
      {
        inputString = new String(inputBytes, 0, bytesRead);

        if (inputString.substring(0, 1).getBytes()[0] == ACK)
        {
          _command = inputString.substring(2, 3).getBytes()[0];
          byte dataLength = inputString.substring(3, 4).getBytes()[0];
          _data = inputString.substring(4, 4 + dataLength);
          _checksum = inputString.substring(inputString.length() - 1, inputString.length()).getBytes()[0];
        }
      }
    }

  }

}
