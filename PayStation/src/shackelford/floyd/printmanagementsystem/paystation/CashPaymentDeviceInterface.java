package shackelford.floyd.printmanagementsystem.paystation;




/**
 * <p>Title: CashPaymentDeviceInterface</p>
 * <p>Description:
 * interface for device drivers
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface CashPaymentDeviceInterface
{

  public double GetCashValue();

  /**
   * accept the cash
   */
  public void AcceptCash();

  /**
   * reject the cash and return it to the user
   */
  public void RejectCash();

}