package shackelford.floyd.printmanagementsystem.paystation;

import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: Protocol_Card_JamexPaymentController</p>
 * <p>Description:
 * protocol for the Jamex card reader
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Protocol_Card_JamexPaymentController
  extends AbstractCardPaymentProtocol
{


  private static Resources        __resources;

  public Protocol_Card_JamexPaymentController(Device_Card_JamexPaymentController paymentDevice)
  {
    super(paymentDevice);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(Protocol_Card_JamexPaymentController.class.getName());
  }

}