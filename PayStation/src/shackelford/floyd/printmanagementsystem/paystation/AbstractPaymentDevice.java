package shackelford.floyd.printmanagementsystem.paystation;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Properties;

import shackelford.floyd.printmanagementsystem.common.AbstractSerialPort;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;


/**
 * <p>Title: AbstractPaymentDevice</p>
 * <p>Description:
 * abstract class for connecting to a payment device
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPaymentDevice
  extends AbstractSerialPort
{

  protected AbstractPaymentProtocol        _paymentProtocol;

  protected static AbstractPaymentDevice[] __devices          = null;

  protected static final String            DEVICE_PREFIX      = "DEVICE_";

  /**
   * CONFIGURATION_FILE: the configuration file defines all the payment devices connected to this pay station
   */
  protected static final String            CONFIGURATION_FILE = "." + File.separator + "PayStation.configuration";

  protected static final double            UNDEFINED_DOUBLE   = -1d;

  /**
   * Constructor
   */
  public AbstractPaymentDevice()
  {
    super();
  }

  public void Initialize(AbstractPaymentProtocol paymentProtocol)
  {
    _paymentProtocol = paymentProtocol;
  }

  public AbstractPaymentProtocol GetPaymentProtocol()
  {
    return _paymentProtocol;
  }

  public static AbstractPaymentDevice[] GetPaymentDevices()
  {
    return __devices;
  }

  public void Set_buttonLabel(String buttonLabel)
  {
    GetPaymentProtocol().SetButtonLabel(buttonLabel);
  }

  public void Set_buttonToolTip(String buttonToolTip)
  {
    GetPaymentProtocol().SetButtonToolTip(buttonToolTip);
  }

  static
  {
    Properties configuration = null;
    // load in the configuration file
    try
    {
      configuration = new Properties();
      try
      (FileInputStream in = new FileInputStream(CONFIGURATION_FILE);)
      {
        configuration.load(in);
      }
    }
    catch (IOException excp)
    {
      excp.printStackTrace();
      MessageDialog.ShowErrorMessageDialog(null, "Cannot Load the Configuration File. Please ensure that \"" + CONFIGURATION_FILE + "\" is in the PayStation directory.",
        "Cannot Load Configuration File");
      System.exit(PayStation.EXIT_ERROR_CANNOT_LOAD_CONFIG_FILE);
    }
    // create and initialize a device object for each device defined in the configuration file
    ArrayList<AbstractPaymentDevice> devices = new ArrayList<>(10);
    //    int devicesIndex = 0;
    DEVICE_LOOP: for (int indx = 0; indx <= 9; indx++)
    {
      String devicePrefix = DEVICE_PREFIX + String.valueOf(indx);
      // if this device exists, then capture its settings
      String deviceName = configuration.getProperty(devicePrefix);
      if (deviceName != null)
      {
        // see if we support the specified device: look for its class
        Class<?> deviceClass = null;
        try
        {
          deviceClass = Class.forName(deviceName);
        }
        catch (ClassNotFoundException excp)
        {
          excp.printStackTrace();
          MessageDialog.ShowErrorMessageDialog(null, "Invalid Device \"" + deviceName + "\" declared for " + devicePrefix + ". Ignoring this device.", "Invalid Device Declared");
          // we don't support this device type. go on to the next device.
          continue DEVICE_LOOP;
        }
        // create an instance of the specified device
        AbstractPaymentDevice paymentDevice = null;
        try
        {
          paymentDevice = (AbstractPaymentDevice) (deviceClass.newInstance());
        }
        catch (Exception excp)
        {
          excp.printStackTrace();
          MessageDialog.ShowErrorMessageDialog(null, "Internal Error. Cannot create instance of Device \"" + deviceName + "\" declared for " + devicePrefix + ". Ignoring this device.",
            "Internal Error");
          continue DEVICE_LOOP;
        }
        devices.add(paymentDevice);
        // initialize the paymentDevice's variables
        Enumeration<?> propertyNames = configuration.propertyNames();
        while (propertyNames.hasMoreElements() == true)
        {
          String propertyName = propertyNames.nextElement().toString();
          // just get the settings for the current device and assign them to the device instance
          if (propertyName.startsWith(devicePrefix) == true)
          {
            String fieldName = propertyName.substring(devicePrefix.length());
            if (fieldName.length() > 0)
            {
              String setMethodName = "Set" + fieldName;
              Method setMethod = null;
              try
              {
                setMethod = deviceClass.getMethod(setMethodName, new Class[] { String.class });
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
                MessageDialog.ShowErrorMessageDialog(null, "Cannot find method " + deviceClass.getName() + "." + setMethodName + "() for " + propertyName + ". Ignoring this device.",
                  "Invalid Field Name");
                devices.remove(paymentDevice);
                continue DEVICE_LOOP;
              }
              // the the field to the proeprty value
              String propertyValue = configuration.getProperty(propertyName);
              try
              {
                setMethod.invoke(paymentDevice, propertyValue);
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
                MessageDialog.ShowErrorMessageDialog(null, "Internal Error. Cannot assign value \"" + propertyValue + "\" to field " + fieldName + " for device " + deviceName
                  + ". Ignoring this device.", "Internal Error");
                devices.remove(paymentDevice);
                continue DEVICE_LOOP;
              }
            }
          }
        }
      }
    }
    devices.trimToSize();
    // see if we ended up with any valid devices
    if (devices.size() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(null, "There are no devices defined in \"" + CONFIGURATION_FILE + "\".\nYou must have at least 1 device defined.", "No Devices Defined");
      System.exit(PayStation.EXIT_ERROR_INVALID_CONFIG_FILE);
    }
    __devices = new AbstractPaymentDevice[devices.size()];
    devices.toArray(__devices);
    // initialize the devices
    for (int indx = 0; indx < __devices.length; indx++)
    {
      __devices[indx].InitializeDevice();
    }
  }

}
