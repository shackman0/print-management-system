package shackelford.floyd.printmanagementsystem.paystation;

import java.io.IOException;
import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;



/**
 * <p>Title: AbstractJamexPaymentControllerDevice</p>
 * <p>Description:
 * abstract device driver for Jamex devices.
 * see "JPC Communication Specification" dated 21 Oct 1999, revised 31 May 2000.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractJamexPaymentControllerDevice
  extends AbstractPaymentDevice
{

  protected AbstractJamexPaymentControllerDevice      _this;

  protected static int[]                    __crcTable = new int[256];
  protected static final DecimalFormat      __formatter = new DecimalFormat("000.000");

  protected static final byte               SYSTEM_STATUS_REQUEST = 'A';
  protected static final byte               READ_VALUE_REQUEST = 'C';
  protected static final byte               ADD_VALUE_REQUEST = 'D';
  protected static final byte               DEDUCT_VALUE_REQUEST = 'E';
  protected static final byte               RETURN_CARD_COIN_REQUEST = 'H';
  protected static final byte               SET_OPTIONS_REQUEST = 'I';
  protected static final byte               DISPLAY_MESSAGE_REQUEST = 'J';
  protected static final byte               SET_TIME_DATE_REQUEST = 'M';

  protected static final byte               SYSTEM_STATUS_RESPONSE = 'a';
  protected static final byte               READ_VALUE_RESPONSE = 'c';
  protected static final byte               ADD_VALUE_RESPONSE = 'd';
  protected static final byte               DEDUCT_VALUE_RESPONSE = 'e';
  protected static final byte               RETURN_CARD_COIN_RESPONSE = 'h';
  protected static final byte               SET_OPTIONS_RESPONSE = 'i';
  protected static final byte               DISPLAY_MESSAGE_RESPONSE = 'j';
  protected static final byte               SET_TIME_DATE_RESPONSE = 'm';

  protected static final int                RC_OPERATION_COMPLETED_SUCCESSFULLY = 0;
  protected static final int                RC_OPERATION_COMPLETED_SUCCESSFULLY_IN_BYPASS_MODE = 1;
  protected static final int                RC_VALUE_NOT_ACCEPTABLE = 98;
  protected static final int                RC_OPTION_NOT_ENABLED = 99;

  protected static final byte               SYSTEM_STATUS_VALUE_NO_VALUE = 0;
  protected static final byte               SYSTEM_STATUS_VALUE_ON_CARD = 3;
  protected static final byte               SYSTEM_STATUS_VALUE_IN_CASH = 4;

  protected static final byte               DISABLE_RETURN_BUTTON = 0;
  protected static final byte               ENABLE_RETURN_BUTTON = 1;

  protected static final String             FORTY_EIGHT_SPACES = "                                                ";
  protected static final int                WRITE_READ_DELAY = 300;  // the jpc guarantees a response < 1 second

  protected static final byte               HEADER_ADDRESS = '0';
  protected static final byte               START_OF_HEADER = 0x01;
  protected static final byte               START_OF_TEXT = 0x02;
  protected static final byte               END_OF_TEXT = 0x03;

  protected static final int                NUMBER_OF_MSG_HEADER_BYTES = 4;
  protected static final int                NUMBER_OF_MSG_TRAILER_BYTES = 5;
  protected static final int                NUMBER_OF_CRC_BYTES = 4;

  protected AbstractJamexPaymentControllerDevice()
  {
    super();
    _this = this;
 }

  @Override
  public boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    SystemStatusRequest();

    return true;
  }

  public byte SystemStatusRequest()
  {
    Message message = new Message(SYSTEM_STATUS_REQUEST);
    message.WriteAndRead();

    return message._msgBody[0];
  }

  public double ReadValue()
  {
    Message message = new Message(READ_VALUE_REQUEST);
    message.WriteAndRead();

    String messageBody = new String(message._msgBody);

    // the read status is bytes 0-1. if the read operation is not
    // successful, then the remaining data is not valid.
    int status = (new Integer(messageBody.substring(0,1))).intValue();

    double amount;
    if (status == RC_OPERATION_COMPLETED_SUCCESSFULLY)
    {
      // the value in cash or on the card is bytes 6-11.
      // all monetary values are 6 digits, with 3 digits to the left of the decimal
      // and 3 digits to the right of the decimal.
      amount = (new Double(messageBody.substring(6,12))).doubleValue() / 1000d;
    }
    else
    {
      amount = 0d;
      MessageDialog.ShowErrorMessageDialog (
        null,
        (this.getClass().getName() + ".ReadValue() rc=" + status),
        ("Error: " + this.getClass().getName() + ".ReadValue()") );
    }
    DebugWindow.DisplayText(messageBody.substring(6,12) + " converts to " + amount);
    return amount;
  }

  public void AddValue(double valueToAdd)
  {
    Message message = new Message(ADD_VALUE_REQUEST,FormatValue(valueToAdd));
    message.WriteAndRead();

    String messageInString = new String(message._msgBody);

    // the status is bytes 0-1
    int status = (new Integer(messageInString.substring(0,1))).intValue();
    if (status != RC_OPERATION_COMPLETED_SUCCESSFULLY)
    {
      MessageDialog.ShowErrorMessageDialog (
        null,
        (this.getClass().getName() + ".AddValue() rc=" + status),
        ("Error: " + this.getClass().getName() + ".AddValue()") );
    }
  }

  public void DeductValue(double valueToDeduct)
  {
    Message message = new Message(DEDUCT_VALUE_REQUEST,FormatValue(valueToDeduct));
    message.WriteAndRead();

    String messageInString = new String(message._msgBody);

    // the status is bytes 0-1
    int status = (new Integer(messageInString.substring(0,1))).intValue();
    if (status != RC_OPERATION_COMPLETED_SUCCESSFULLY)
    {
      MessageDialog.ShowErrorMessageDialog (
        null,
        (this.getClass().getName() + ".DeductValue() rc=" + status),
        ("Error: " + this.getClass().getName() + ".DeductValue()") );
    }
  }

  public void ReturnCardOrCoin()
  {
    do
    {
      Message message = new Message(RETURN_CARD_COIN_REQUEST);
      message.WriteAndRead();

      String messageBody = new String(message._msgBody);

      // the status is bytes 0-1
      int status = (new Integer(messageBody.substring(0,1))).intValue();
      if (status != RC_OPERATION_COMPLETED_SUCCESSFULLY)
      {
        MessageDialog.ShowErrorMessageDialog (
          null,
          (this.getClass().getName() + ".ReturnCardOrCoin() rc=" + status),
          ("Error: " + this.getClass().getName() + ".ReturnCardOrCoin()") );
        break;
      }
    }
    while (ReadValue() != 0d);
  }

  public void SetOptions(byte enableReturnButton)
  {
    Message message = new Message(SET_OPTIONS_REQUEST, new byte[] { enableReturnButton, 0 } );
    message.WriteAndRead();
  }

  public void DisplayMessage(String messageString)
  {
    messageString = (messageString.trim() + FORTY_EIGHT_SPACES).substring(0,48);

    Message message = new Message(DISPLAY_MESSAGE_REQUEST, messageString.getBytes() );
    message.WriteAndRead();
  }

  public void SetTimeDate (
    String    time,       // hhmm
    String    date )      // mmddyy
  {
    String timeDate = time + date;

    Message message = new Message(SET_TIME_DATE_REQUEST, timeDate.getBytes() );
    message.WriteAndRead();
  }

  protected byte[] FormatValue(double value)
  {
    String valueString = __formatter.format(value);
    String formattedValue = valueString.substring(0,3) + valueString.substring(4,7);
    return formattedValue.getBytes();
  }

  protected class MessageHeader
  {
    private byte[]                _bytes = new byte[] {
                                                 START_OF_HEADER,
                                                 HEADER_ADDRESS,
                                                 0x00,  // command or response filled in during object construction
                                                 START_OF_TEXT };

    public MessageHeader()
    {
      // do nothing
    }

    public MessageHeader(byte command)
    {
      _bytes[2] = command;
    }

    public void SetCommandCode(byte command)
    {
      _bytes[2] = command;
    }

    public byte GetCommandCode()
    {
      return _bytes[2];
    }

    public byte GetResponseCode()
    {
      return GetCommandCode();
    }

    public byte[] GetBytes()
    {
      return _bytes;
    }

    public void Read()
    {
      int numberRead = 0;
      byte[] readBytes = new byte[NUMBER_OF_MSG_HEADER_BYTES];
      try
      {
        numberRead = _inputStream.read(readBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }

      if (numberRead != NUMBER_OF_MSG_HEADER_BYTES)
      {
        DebugWindow.DisplayText("Error: expected " + NUMBER_OF_MSG_HEADER_BYTES + " bytes, read " + numberRead + " bytes");
      }
      _bytes = new byte[numberRead];
      for (int i = 0; i < _bytes.length; i++)
      {
        _bytes[i] = readBytes[i];
      }
    }

  }

  protected class MessageTrailer
  {
    private byte[]                _bytes = new byte[] {
                                                 END_OF_TEXT,
                                                 0x00,        // crc byte 0
                                                 0x00,        // crc byte 1
                                                 0x00,        // crc byte 2
                                                 0x00 };      // crc byte 3

    public MessageTrailer()
    {
      // do nothing
    }

    public byte[] GetBytes()
    {
      return _bytes;
    }

    public void Read()
    {
      int numberRead = 0;
      byte[] readBytes = new byte[NUMBER_OF_MSG_TRAILER_BYTES];
      try
      {
        numberRead = _inputStream.read(readBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }

      if (numberRead != NUMBER_OF_MSG_TRAILER_BYTES)
      {
        DebugWindow.DisplayText("Error: expected " + NUMBER_OF_MSG_TRAILER_BYTES + " bytes, read " + numberRead + " bytes");
      }
      _bytes = new byte[numberRead];
      for (int i = 0; i < _bytes.length; i++)
      {
        _bytes[i] = readBytes[i];
      }
    }

    public void SetCRC(byte[] crcBytes)
    {
      for (int indx = 0; indx < NUMBER_OF_CRC_BYTES; indx++)
      {
        _bytes[indx+1] = crcBytes[indx];
      }
    }
  }

  protected class Message
  {
    public MessageHeader          _msgHeader;
    public byte[]                 _msgBody = new byte[0];
    public MessageTrailer         _msgTrailer;

    public Message ()
    {
      Initialize((byte)0,new byte[0]);
    }

    public Message (byte command)
    {
      Initialize(command,new byte[0]);
    }

    public Message (
      byte    command,
      byte[]  msgBody )
    {
      Initialize(command,msgBody);
    }

    protected void Initialize (
      byte    command,
      byte[]  msgBody )
    {
      _msgHeader = new MessageHeader(command);
      _msgBody = msgBody;
      _msgTrailer = new MessageTrailer();
      SetCRC();
    }

    public void WriteAndRead()
    {
      Write();
      Delay(WRITE_READ_DELAY);
      Read();
    }

    public void Write()
    {
      int headerLength = _msgHeader.GetBytes().length;
      int bodyLength = _msgBody.length;
      int trailerLength = _msgTrailer.GetBytes().length;

      byte[] bytes = new byte[headerLength + bodyLength + trailerLength];

      int j = 0;
      for (int i = 0; i < headerLength; i++)
      {
        bytes[j++] = _msgHeader.GetBytes()[i];
      }

      for (int i = 0; i < bodyLength; i++)
      {
        bytes[j++] = _msgBody[i];
      }

      for (int i = 0; i < trailerLength; i++)
      {
        bytes[j++] = _msgTrailer.GetBytes()[i];
      }

      try
      {
        DebugWindow.DisplayText("write \"" + new String(bytes) + "\" (" + bytes.length + ") " + ConvertBytesToHexString(bytes) + " ... ");
        _outputStream.write(bytes);
        DebugWindow.DisplayText("done!");
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }
    }

    public void Read()
    {
      _this.WaitForTheData();

      DebugWindow.DisplayText("read header ... ");
      _msgHeader.Read();
      DebugWindow.DisplayText("\"" + new String(_msgHeader.GetBytes()) + "\" (" + _msgHeader.GetBytes().length + ") " + ConvertBytesToHexString(_msgHeader.GetBytes()));

      int responseBodySize = 0;
      switch (_msgHeader.GetResponseCode())
      {
        case SYSTEM_STATUS_RESPONSE:
               responseBodySize = 6;
               break;
        case READ_VALUE_RESPONSE:
               responseBodySize = 39;
               break;
        case ADD_VALUE_RESPONSE:
               responseBodySize = 2;
               break;
        case DEDUCT_VALUE_RESPONSE:
               responseBodySize = 2;
               break;
        case RETURN_CARD_COIN_RESPONSE:
               responseBodySize = 2;
               break;
        case SET_OPTIONS_RESPONSE:
               responseBodySize = 0;
               break;
        case DISPLAY_MESSAGE_RESPONSE:
               responseBodySize = 0;
               break;
        case SET_TIME_DATE_RESPONSE:
        default:
               responseBodySize = 0;
               break;
      }

      DebugWindow.DisplayText("read body ... ");
      int numberRead = 0;
      byte[] readBytes = new byte[responseBodySize];
      try
      {
        numberRead = _inputStream.read(readBytes);
      }
      catch (IOException excp)
      {
        excp.printStackTrace();
      }

      if (numberRead != responseBodySize)
      {
        DebugWindow.DisplayText("Error: expected " + responseBodySize + " bytes, read " + numberRead + " bytes");
        responseBodySize = numberRead;
      }
      _msgBody = new byte[responseBodySize];
      for (int i = 0; i < _msgBody.length; i++)
      {
        _msgBody[i] = readBytes[i];
      }
      DebugWindow.DisplayText("\"" + new String(_msgBody) + "\" (" + _msgBody.length + ") " + ConvertBytesToHexString(_msgBody));

      DebugWindow.DisplayText("read trailer ... ");
      _msgTrailer.Read();
      DebugWindow.DisplayText("\"" + new String(_msgTrailer.GetBytes()) + "\" (" + _msgTrailer.GetBytes().length + ") " + ConvertBytesToHexString(_msgTrailer.GetBytes()));

    }

    public byte[] GetBytes()
    {
      byte[] headerBytes = _msgHeader.GetBytes();
      byte[] trailerBytes = _msgTrailer.GetBytes();

      byte[] allBytes = new byte[headerBytes.length + _msgBody.length + trailerBytes.length];

      int jndx = 0;
      for (int indx = 0; indx < headerBytes.length; indx++)
      {
        allBytes[jndx++] = headerBytes[indx];
      }
      for (int indx = 0; indx < _msgBody.length; indx++)
      {
        allBytes[jndx++] = _msgBody[indx];
      }
      for (int indx = 0; indx < trailerBytes.length; indx++)
      {
        allBytes[jndx++] = trailerBytes[indx];
      }

      return allBytes;
    }

    public void SetCRC()
    {
      _msgTrailer.SetCRC(CalcCRC());
    }

    public byte[] CalcCRC()
    {
      byte[] bytes = GetBytes();

      int accumulator = 0xffff;
      int crcIndex;
      int data;

      for (int indx = 0; indx < (bytes.length - NUMBER_OF_CRC_BYTES); indx++) // ignore the crc bytes
      {
        data = bytes[indx];
        crcIndex = ((accumulator >> 8) ^ data) & 0xff;
        accumulator = ((accumulator << 8) ^ __crcTable[crcIndex]) & 0xffff;
      }

      accumulator ^= 0xffff;
/*
      int accumulator = 65535;
      int crcIndex;
      int data;

      for (int indx = 0; indx < (bytes.length - NUMBER_OF_CRC_BYTES); indx++) // ignore the crc bytes
      {
        data = bytes[indx];
        crcIndex = ((accumulator / 256) ^ data) & 255;
        accumulator = ((accumulator * 256) ^ __crcTable[crcIndex]) & 65535;
      }

      accumulator ^= 65535;
*/

      String crcString = "0000" + Integer.toHexString(accumulator);
      crcString = crcString.substring(crcString.length() - NUMBER_OF_CRC_BYTES,crcString.length()).toUpperCase();

      return crcString.getBytes();
    }

  }

  protected static String ConvertBytesToHexString(byte[] bytes)
  {
    String hexString = "";
    for (int i = 0; i < bytes.length; i++)
    {
      hexString += Integer.toHexString(bytes[i]) + " ";
    }
    return hexString;
  }

  static
  {
    // generate the CRC-16 table
    int data;
    int accumulator;

    for (int indx = 0; indx < 256; indx++)
    {
      data = indx;
      accumulator = 0;
      data <<= 8;
      for (int jndx = 8; jndx > 0; jndx--)
      {
        if (((data ^ accumulator) & 0x8000) == 0x8000)
        {
          accumulator = ((accumulator << 1) ^ 0x1021) & 0xffff;
        }
        else
        {
          accumulator = (accumulator << 1) & 0xffff;
        }
        data <<= 1;
      }
/*
      data = indx;
      accumulator = 0;
      data *= 256;
      for (int jndx = 8; jndx > 0; jndx--)
      {
        if (((data ^ accumulator) & 32768) != 0)
        {
          accumulator = ((accumulator * 2) ^ 4129) & 65535;
        }
        else
        {
          accumulator = (accumulator * 2) & 65535;
        }
        data *= 2;
      }
*/
      __crcTable[indx] = accumulator;

      if (GlobalAttributes.__verbose == true)
      {
        String crcString = "0000" + Integer.toHexString(accumulator);
        crcString = crcString.substring(crcString.length() - NUMBER_OF_CRC_BYTES,crcString.length());
        DebugWindow.DisplayText("__crcTable["+Integer.toHexString(indx)+"] = \""+crcString+"\"");
      }
    }
  }

}