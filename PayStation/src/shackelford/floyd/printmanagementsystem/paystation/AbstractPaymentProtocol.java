package shackelford.floyd.printmanagementsystem.paystation;

import java.text.DecimalFormat;

import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;



/**
 * <p>Title: AbstractPaymentProtocol</p>
 * <p>Description:
 * the abstract class for the protocol for interfacing to physical payment devices
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPaymentProtocol
{

  protected String                     _buttonLabel = UNDEFINED_STRING;
  protected String                     _buttonToolTip = UNDEFINED_STRING;
  protected AbstractPaymentDevice      _paymentDevice = null;

  /*
   * resource keys
   */
  protected static final String        INSERT_PAYMENT_INSTRUMENT_MSG = "INSERT_PAYMENT_INSTRUMENT_MSG";
  protected static final String        INSERT_PAYMENT_INSTRUMENT_TITLE = "INSERT_PAYMENT_INSTRUMENT_TITLE";
  protected static final String        TRANSACTION_COMPLETE_MSG = "TRANSACTION_COMPLETE_MSG";
  protected static final String        TRANSACTION_COMPLETE_TITLE = "TRANSACTION_COMPLETE_TITLE";

  protected static final String        UNDEFINED_STRING =  "undefined";
  protected static final double        UNDEFINED_DOUBLE = -1d;

  protected static final DecimalFormat      __formatter = new DecimalFormat("#,##0.00");


  /**
   * Constructor
   */
  public AbstractPaymentProtocol()
  {
    super();
  }
  
  /**
   * use this method to process a user payment
   */
  public abstract void ProcessPayment();

  protected abstract Resources GetResources();

  protected void UpdateDatabase (double  transactionAmount)  // < 0 for refunds
  {
    // modify the user's account
    double prePaymentBalance = UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().doubleValue();
    UserAccountTable.GetCachedUserAccountRow().Set_pre_payment_balance(prePaymentBalance + transactionAmount);
    UserAccountTable.GetCachedUserAccountRow().Update();

    // add a pre payment history record
    PrePaymentHistoryRow prePaymentHistoryRow =
      new PrePaymentHistoryRow (
            InstallationDatabase.GetDefaultInstallationDatabase().GetPrePaymentHistoryTable(),
            UserAccountTable.GetCachedUserAccountRow() );
    prePaymentHistoryRow.Set_amount(transactionAmount);
    prePaymentHistoryRow.Set_source(this.getClass().getName());
    prePaymentHistoryRow.Insert();
  }

  public String GetButtonLabel()
  {
    return new String(_buttonLabel);
  }

  public void SetButtonLabel(String value)
  {
    _buttonLabel = new String(value);
  }

  public String GetButtonToolTip()
  {
    return new String(_buttonToolTip);
  }

  public void SetButtonToolTip(String value)
  {
    _buttonToolTip = new String(value);
  }

}