package shackelford.floyd.printmanagementsystem.paystation;

import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: Protocol_Cash_JamexPaymentController</p>
 * <p>Description:
 * protocol for the Jamex cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Protocol_Cash_JamexPaymentController
  extends AbstractCashPaymentProtocol
{

  private static Resources        __resources;


  public Protocol_Cash_JamexPaymentController(Device_Cash_JamexPaymentController paymentDevice)
  {
    super(paymentDevice);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(Protocol_Cash_JamexPaymentController.class.getName());
  }

}