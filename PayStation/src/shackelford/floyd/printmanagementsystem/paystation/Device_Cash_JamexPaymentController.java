package shackelford.floyd.printmanagementsystem.paystation;




/**
 * <p>Title: Device_Cash_JamexPaymentController</p>
 * <p>Description:
 * device driver for the Jamex cash acceptor
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Device_Cash_JamexPaymentController
  extends AbstractJamexPaymentControllerDevice
  implements CashPaymentDeviceInterface
{


  public Device_Cash_JamexPaymentController()
  {
    super();
    Initialize(new Protocol_Cash_JamexPaymentController(this));
  }

  @Override
  public boolean InitializeDevice()
  {
    if (super.InitializeDevice() == false)
    {
      return false;
    }

    return true;
  }

  @Override
  public double GetCashValue()
  {
    return ReadValue();
  }

  @Override
  public void AcceptCash()
  {
    DeductValue(GetCashValue());
  }

  @Override
  public void RejectCash()
  {
    ReturnCardOrCoin();
  }

}