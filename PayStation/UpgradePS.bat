@echo off

rem This script upgrades the ReleaseStation application.

set INSTALLDIR=\program files\DigiNet_LX

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

cd "%INSTALLDIR%"

"%INSTALLDIR%\Info-Zip\unzip.exe" -o "%INSTALLDIR%\ReleaseStation.zip"

del "%INSTALLDIR%\ReleaseStation.zip"

cd "%INSTALLDIR%\ReleaseStation"

.\ReleaseStation.bat
