package shackelford.floyd.printmanagementsystem.useraccount;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.AutoExitEventQueue;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MultiLineToolTipUI;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SplashPanel;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.common.WaitCursorEventQueue;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;
import shackelford.floyd.printmanagementsystem.upgradeserver.Upgrader;


/**
 * <p>Title: UserAccount</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccount
  implements PanelListener
{

  private static int          __fontSizeAdjustment                  = 0;

  public static final int     MAJOR_VERSION                         = 1;

  public static final int     MINOR_VERSION                         = 0;

  public static final int     FIX_LEVEL                             = 0;

  private static final int    WAIT_CURSOR_DELAY                     = 500;                                      // milliseconds

  private static final String METAL_LOOK_AND_FEEL                   = "javax.swing.plaf.metal.MetalLookAndFeel";

  public static final int     EXIT_OK                               = 0;

  public static final int     EXIT_LOGIN_CANCELLED                  = 1;

  public static final int     EXIT_ERROR_MISSING_RESDIR             = -2;

  public static final int     EXIT_ERROR_MISSING_DBADDR             = -3;

  public static final int     EXIT_ERROR_MISSING_DBUSERID           = -4;

  public static final int     EXIT_ERROR_MISSING_DBPASSWD           = -5;

  public static final int     EXIT_ERROR_DATABASE_CONNECT           = -6;

  public static final int     EXIT_ERROR_MISSING_INST_ID            = -7;

  public static final int     EXIT_ERROR_INSTALLATION_DISABLED      = -8;

  public static final int     EXIT_ERROR_CANNOT_GET_LOCAL_HOST_ADDR = -9;

  public static final int     EXIT_ERROR_NOT_VALID_CLIENT           = -10;

  private static final String NEW_LINE                              = "\n";

  private static final String DOUBLE_QUOTE                          = "\"";

  @Override
  public void EndOfPanel(char action)
  {
    // launch this application again ...
    SystemUtilities.LaunchApplication(GlobalAttributes.__applicationName);
    // ... because we're getting out of here
    System.exit(EXIT_OK);
  }

  @Override
  public void PanelStateChange(char action)
  {
    // do nothing
  }

  @SuppressWarnings("unused")
  public static void main(String[] args)
    throws Exception
  {

    GlobalAttributes.SetMajorVersion(MAJOR_VERSION);
    GlobalAttributes.SetMinorVersion(MINOR_VERSION);
    GlobalAttributes.SetFixLevel(FIX_LEVEL);

    GlobalAttributes.__applicationName = UserAccount.class.getName();
    GlobalAttributes.__mainClass = UserAccount.class;
    Integer installationID = null;

    MultiLineToolTipUI.initialize();
    MultiLineToolTipUI.setDisplayAcceleratorKey(false);

    String splashComments = NEW_LINE + NEW_LINE;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-verbose") == true)
      {
        GlobalAttributes.__verbose = true;
        splashComments += "Running verbose" + NEW_LINE;
      }
      else
        if (args[index].equalsIgnoreCase("-dbAddr") == true)
        {
          GlobalAttributes.__dbAddr = args[++index];
          splashComments += "Database Address = " + DOUBLE_QUOTE + GlobalAttributes.__dbAddr + DOUBLE_QUOTE + NEW_LINE;
        }
        else
          if (args[index].equalsIgnoreCase("-instID") == true)
          {
            installationID = new Integer(args[++index]);
            splashComments += "Installation ID = " + DOUBLE_QUOTE + installationID + DOUBLE_QUOTE + NEW_LINE;
          }
          else
            if (args[index].equalsIgnoreCase("-dbUser") == true)
            {
              GlobalAttributes.__dbUserID = args[++index];
              splashComments += "Database User ID = " + DOUBLE_QUOTE + GlobalAttributes.__dbUserID + DOUBLE_QUOTE + NEW_LINE;
            }
            else
              if (args[index].equalsIgnoreCase("-dbPasswd") == true)
              {
                GlobalAttributes.__dbPasswd = args[++index];
                splashComments += "Database Password found" + NEW_LINE;
              }
              else
                if (args[index].equalsIgnoreCase("-resDir") == true)
                {
                  GlobalAttributes.__resourcesDirectory = args[++index];
                  splashComments += "Resource files are located under directory " + DOUBLE_QUOTE + GlobalAttributes.__resourcesDirectory + DOUBLE_QUOTE + NEW_LINE;
                }
                else
                  if (args[index].equalsIgnoreCase("-fontSizeAdj") == true)
                  {
                    __fontSizeAdjustment = new Integer(args[++index]).intValue();
                    splashComments += "Adjusting font by " + __fontSizeAdjustment + NEW_LINE;
                  }
                  else
                  {
                    splashComments += "Ignoring unknown parameter: " + DOUBLE_QUOTE + args[index] + DOUBLE_QUOTE + NEW_LINE;
                  }
    }

    if (installationID == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -instID was not specified" + NEW_LINE + Usage(), "ERROR: -instID was not specified");
      System.exit(EXIT_ERROR_MISSING_INST_ID);
    }

    if (GlobalAttributes.__dbAddr == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbAddr was not specified" + NEW_LINE + Usage(), "ERROR: -dbAddr was not specified");
      System.exit(EXIT_ERROR_MISSING_DBADDR);
    }

    if (GlobalAttributes.__dbUserID == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbUser was not specified" + NEW_LINE + Usage(), "ERROR: -dbUser was not specified");
      System.exit(EXIT_ERROR_MISSING_DBUSERID);
    }

    if (GlobalAttributes.__dbPasswd == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbPasswd was not specified" + NEW_LINE + Usage(), "ERROR: -dbPasswd was not specified");
      System.exit(EXIT_ERROR_MISSING_DBPASSWD);
    }

    if (GlobalAttributes.__resourcesDirectory == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -resDir was not specified" + NEW_LINE + Usage(), "ERROR: -resDir was not specified");
      System.exit(EXIT_ERROR_MISSING_RESDIR);
    }

    // set up the wait cursor to be displayed whenever the user has to wait for a response
    EventQueue waitCursorEventQueue = new WaitCursorEventQueue(WAIT_CURSOR_DELAY); // milliseconds
    Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitCursorEventQueue);

    // adjust the font size if specified
    if (__fontSizeAdjustment != 0)
    {
      UIManager.setLookAndFeel(METAL_LOOK_AND_FEEL);
      SwingUtilities.updateComponentTreeUI(new JFrame());

      UIDefaults uiDefaults = UIManager.getLookAndFeelDefaults();
      for (Enumeration<?> enumerator = uiDefaults.keys(); enumerator.hasMoreElements();)
      {
        Object key = enumerator.nextElement();
        if (key.toString().endsWith(".font") == true)
        {
          FontUIResource currentFontResource = (FontUIResource) (uiDefaults.get(key));
          FontUIResource newFontResource = new FontUIResource(currentFontResource.getName(), currentFontResource.getStyle(), currentFontResource.getSize() + __fontSizeAdjustment);
          uiDefaults.put(key, newFontResource);
        }
      }
    }

    SplashPanel splashPanel = new SplashPanel(splashComments);
    splashPanel.setVisible(true);

    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(GlobalAttributes.__dbAddr, GlobalAttributes.__dbUserID, GlobalAttributes.__dbPasswd, installationID));
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "new InstallationDatabase()", JOptionPane.ERROR_MESSAGE);
      System.exit(EXIT_ERROR_DATABASE_CONNECT);
    }

    if (InstallationTable.StateOK() == false)
    {
      MessageDialog.ShowErrorMessageDialog(null, "Your installation is not enabled.", "Installation Not Enabled");
      System.exit(EXIT_ERROR_INSTALLATION_DISABLED);
    }

    // set up the language to the installation's preference
    Resources.SetLanguageDirectory(InstallationTable.GetCachedInstallationRow().Get_language());

    String localHostIPAddr = null;
    try
    {
      localHostIPAddr = InetAddress.getLocalHost().getHostAddress();
    }
    catch (java.net.UnknownHostException excp)
    {
      MessageDialog.ShowErrorMessageDialog(null, excp, "Cannot Determine Localhost IP Address");
      System.exit(EXIT_ERROR_CANNOT_GET_LOCAL_HOST_ADDR);
    }

    ClientRow clientRow = (ClientRow) (InstallationDatabase.GetDefaultInstallationDatabase().GetClientTable().GetRowForIPAddress(localHostIPAddr));
    if (clientRow == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, localHostIPAddr + " is not a client address registered for this installation.", "Unregistered Client");
      System.exit(EXIT_ERROR_NOT_VALID_CLIENT);
    }

    // see if we need to upgrade - if we upgrade, we don't return from this call
    Upgrader.CheckForUpgrade(InstallationTable.GetCachedInstallationRow(), GlobalAttributes.__dbAddr, GlobalAttributes.__applicationName);

    // go ahead and let the user log in
    if (LoginOK(splashPanel) == true)
    {
      // set up the language to the user's preference
      Resources.SetLanguageDirectory(UserAccountTable.GetCachedUserAccountRow().Get_language());

      // set up to auto logoff if appropriate
      RulesRow userAutoLogoff = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.USER_AUTO_LOGOFF);
      if (userAutoLogoff.RuleIsYes() == true)
      {
        // set up the auto exit feature. if the user doesn't do anything for
        // the specified number of milliseconds, then the system automatically exits.
        int delay = new Integer(userAutoLogoff.GetRuleParm(0)).intValue() * 1000;
        EventQueue autoExitEventQueue = new AutoExitEventQueue(delay);
        Toolkit.getDefaultToolkit().getSystemEventQueue().push(autoExitEventQueue);
      }

      if (UserAccountTable.GetCachedUserAccountRow().Get_require_new_password().booleanValue() == true)
      {
        splashPanel.dispose();
        // the user must specify a new password before proceeding further
        new UserAccountChangePassphrasePanelManager();
      }
      else
      {
        // display the main panel
        UserAccountMainPanel mainPanel = new UserAccountMainPanel();
        mainPanel.AddPanelListener(new UserAccount());
        splashPanel.dispose();
        mainPanel.setVisible(true);
        mainPanel.toFront();
      }
    }
    else
    {
      System.exit(EXIT_LOGIN_CANCELLED);
    }
  }

  private static String Usage()
  {
    return "Usage:" + NEW_LINE + "java" + NEW_LINE + "  -Djdbc.drivers=org.postgresql.Driver" + NEW_LINE + "  UserAccount" + NEW_LINE + "  -dbAddr <database hostname or ip addr>" + NEW_LINE
      + "  -dbUser <database user id>" + NEW_LINE + "  -dbPasswd <database passwd>" + NEW_LINE + "  -instID <installation ID>" + NEW_LINE + "  -resDir <resources directory>" + NEW_LINE
      + "  [-fontSizeAdj <[-]font size adj>] [-verbose]";
  }

  private static boolean LoginOK(Frame owner)
  {
    boolean loginOK = false;
    Properties dialogProperties = null;
    UserAccountLoginDialog dialog = new UserAccountLoginDialog(owner);
    dialogProperties = dialog.WaitOnDialog();

    loginOK = (new Boolean(dialogProperties.getProperty(AbstractDialog.OK))).booleanValue();
    return loginOK;
  }

} // end of UserAccount
