package shackelford.floyd.printmanagementsystem.useraccount;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.MessageFormat;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: UserAccountEntryDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountEntryDialog
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long           serialVersionUID                  = 1L;

  private final UserAccountRow        _userAccountRow;

  private final OverwritableTextField _nameField                        = new OverwritableTextField();

  private final JPasswordField        _passphrase1Field                 = new JPasswordField();

  private final JPasswordField        _passphrase2Field                 = new JPasswordField();

  private final _JComboBox            _languageComboBox                 = new _JComboBox();

  private final OverwritableTextField _userAccountIDField               = new OverwritableTextField();

  private final OverwritableTextField _eMailAddressField                = new OverwritableTextField();

  private final OverwritableTextField _homeAddressField                 = new OverwritableTextField();

  private final OverwritableTextField _phoneNumberField                 = new OverwritableTextField();

  private final JTextArea             _additionalInfoArea               = new JTextArea();

  private final RulesRow              _flexibleUserAccountIDsRuleRow;

  private static Resources            __resources;

  public static final String          USER_ACCOUNT_ID                   = "USER_ACCOUNT_ID";

  //  private static final String     USER_ACCOUNT_ID_TIP = "USER_ACCOUNT_ID_TIP";
  public static final String          ACCOUNT_NAME                      = "ACCOUNT_NAME";

  //  private static final String     ACCOUNT_NAME_TIP = "ACCOUNT_NAME_TIP";
  //  private static final String     PASSPHRASE = "PASSPHRASE";
  public static final String          ACCOUNT_PASSWORD1                 = "ACCOUNT_PASSWORD1";

  //  private static final String     ACCOUNT_PASSWORD1_TIP = "ACCOUNT_PASSWORD1_TIP";
  public static final String          ACCOUNT_PASSWORD2                 = "ACCOUNT_PASSWORD2";

  //  private static final String     ACCOUNT_PASSWORD2_TIP = "ACCOUNT_PASSWORD2_TIP";
  public static final String          LANGUAGE                          = "LANGUAGE";

  //  private static final String     LANGUAGE_TIP = "LANGUAGE_TIP";
  public static final String          E_MAIL_ADDRESS                    = "E_MAIL_ADDRESS";

  //  private static final String     E_MAIL_ADDRESS_TIP = "E_MAIL_ADDRESS_TIP";
  public static final String          HOME_ADDRESS                      = "HOME_ADDRESS";

  //  private static final String     HOME_ADDRESS_TIP = "HOME_ADDRESS_TIP";
  public static final String          PHONE_NUMBER                      = "PHONE_NUMBER";

  //  private static final String     PHONE_NUMBER_TIP = "PHONE_NUMBER_TIP";
  public static final String          ADDITIONAL_INFO                   = "ADDITIONAL_INFO";

  //  private static final String     ADDITIONAL_INFO_TIP = "ADDITIONAL_INFO_TIP";

  private static final String         TAKES_EFFECT_NEXT_LOGIN_MSG       = "TAKES_EFFECT_NEXT_LOGIN_MSG";

  private static final String         USER_ACCOUNT_ALREADY_EXISTS_MSG   = "USER_ACCOUNT_ALREADY_EXISTS_MSG";

  private static final String         USER_ACCOUNT_ALREADY_EXISTS_TITLE = "USER_ACCOUNT_ALREADY_EXISTS_TITLE";

  public static final String          NON_MATCHING_PASSPHRASES_MSG      = "NON_MATCHING_PASSPHRASES_MSG";

  public static final String          NON_MATCHING_PASSPHRASES_TITLE    = "NON_MATCHING_PASSPHRASES_TITLE";

  public static final String          BLANK_USER_ACCOUNT_ID_MSG         = "BLANK_USER_ACCOUNT_ID_MSG";

  public static final String          BLANK_USER_ACCOUNT_ID_TITLE       = "BLANK_USER_ACCOUNT_ID_TITLE";

  public static final String          BLANK_NAME_MSG                    = "BLANK_NAME_MSG";

  public static final String          BLANK_NAME_TITLE                  = "BLANK_NAME_TITLE";

  public static final String          MSG_1                             = "MSG_1";

  public static final String          MSG_2                             = "MSG_2";

  public UserAccountEntryDialog()
  {
    super(null, true);
    _userAccountRow = new UserAccountRow(GetUserAccountTable());
    _flexibleUserAccountIDsRuleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.FLEXIBLE_USER_ACCOUNT_IDS);
    if (_flexibleUserAccountIDsRuleRow.RuleIsNo() == true)
    {
      _userAccountRow.SetNewUserAccountID();
    }
    super.Initialize();
  }

  @Override
  protected Container CreateNorthPanel()
  {
    Box northPanel = Box.createVerticalBox();

    MessageFormat msg1Format = new MessageFormat(__resources.getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { _userAccountRow.Get_user_account_id() };
    String msg1String = msg1Format.format(substitutionValues).trim();
    int cols = 40;
    int rows = (msg1String.length() / cols) + 1;

    JTextArea textArea = new JTextArea(msg1String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(northPanel.getBackground());
    northPanel.add(textArea);

    String msg2String = __resources.getProperty(MSG_2).trim();
    rows = (msg2String.length() / cols);

    textArea = new JTextArea(msg2String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(northPanel.getBackground());
    northPanel.add(textArea);

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(USER_ACCOUNT_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDField.setColumns(8);
    _userAccountIDField.setEditable(_flexibleUserAccountIDsRuleRow.RuleIsYes());

    centerPanel.add(_userAccountIDField, constraints);

    // account name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _nameField.setColumns(FIELD_WIDTH);
    centerPanel.add(_nameField, constraints);

    // account password1
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_PASSWORD1), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphrase1Field.setColumns(FIELD_WIDTH);
    centerPanel.add(_passphrase1Field, constraints);

    // account password2
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_PASSWORD2), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphrase2Field.setColumns(FIELD_WIDTH);
    centerPanel.add(_passphrase2Field, constraints);

    // language
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LANGUAGE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _languageComboBox.setEditable(false);

    centerPanel.add(_languageComboBox, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(new JLabel(__resources.getProperty(TAKES_EFFECT_NEXT_LOGIN_MSG), SwingConstants.LEFT), constraints);

    return centerPanel;
  }

  @Override
  protected void AssignFields()
  {
    _userAccountIDField.setText(_userAccountRow.Get_user_account_id());

    String[] languages = Resources.GetLanguages();
    String language = _userAccountRow.Get_language();
    _languageComboBox.RecreateComboBox(languages, language, false);
  }

  @Override
  protected boolean PanelValid()
  {
    String userAccountID = _userAccountIDField.getText().trim();

    if (userAccountID.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_USER_ACCOUNT_ID_MSG), __resources.getProperty(BLANK_USER_ACCOUNT_ID_TITLE));
      return false;
    }

    if (GetUserAccountTable().GetRowForUserAccountID(userAccountID) != null)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(USER_ACCOUNT_ALREADY_EXISTS_MSG), __resources.getProperty(USER_ACCOUNT_ALREADY_EXISTS_TITLE));
      _userAccountIDField.requestFocus();
      return false;
    }

    String name = _nameField.getText().trim();
    if (name.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_NAME_MSG), __resources.getProperty(BLANK_NAME_TITLE));
      _nameField.requestFocus();
      return false;
    }

    String passphrase1 = new String(_passphrase1Field.getPassword());
    String passphrase2 = new String(_passphrase2Field.getPassword());
    if (passphrase1.equals(passphrase2) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(NON_MATCHING_PASSPHRASES_MSG), __resources.getProperty(NON_MATCHING_PASSPHRASES_TITLE));
      _passphrase1Field.setText("");
      _passphrase1Field.requestFocus();
      _passphrase2Field.setText("");
      return false;
    }

    return true;
  }

  @Override
  protected boolean OKAction()
  {
    boolean rc = PanelValid();
    if (rc == true)
    {
      _userAccountRow.Set_user_account_id(_userAccountIDField.getText());
      _userAccountRow.Set_user_name(_nameField.getText());

      _userAccountRow.Set_user_passphrase(new String(_passphrase1Field.getPassword()));

      _userAccountRow.Set_additional_info(_additionalInfoArea.getText());
      _userAccountRow.Set_language(_languageComboBox.getSelectedItem().toString());
      _userAccountRow.Set_e_mail_address(_eMailAddressField.getText());
      _userAccountRow.Set_home_address(_homeAddressField.getText());
      _userAccountRow.Set_phone_number(_phoneNumberField.getText());

      _userAccountRow.Update();

      _okButton.setSelected(true);
      NotifyDialogListeners();
      dispose();
    }
    return rc;
  }

  UserAccountTable GetUserAccountTable()
  {
    return InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable();
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(UserAccountEntryDialog.class.getName());
  }

}
