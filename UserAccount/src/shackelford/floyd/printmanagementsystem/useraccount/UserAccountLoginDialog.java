package shackelford.floyd.printmanagementsystem.useraccount;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.MessageFormat;
import java.util.GregorianCalendar;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractLoginDialog;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: UserAccountLoginDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountLoginDialog
  extends AbstractLoginDialog
{

  /**
   * 
   */
  private static final long    serialVersionUID              = 1L;

  private final JPasswordField _passphraseField              = new JPasswordField();

  OverwritableTextField        _userAccountIDField           = new OverwritableTextField();

  private ActionButton         _newUserAccountButton         = null;

  private ActionButton         _eMailPassphraseButton        = null;

  private RulesRow             _requireUserPasswordRuleRow   = null;

  static Resources             __resources;

  public static final String   E_MAIL_PASSPHRASE             = "E_MAIL_PASSPHRASE";

  private static final String  E_MAIL_PASSPHRASE_TIP         = "E_MAIL_PASSPHRASE_TIP";

  public static final String   PASSPHRASE                    = "PASSPHRASE";

  private static final String  PASSPHRASE_TIP                = "PASSPHRASE_TIP";

  public static final String   USER_ACCOUNT_ID               = "USER_ACCOUNT_ID";

  private static final String  USER_ACCOUNT_ID_TIP           = "USER_ACCOUNT_ID_TIP";

  public static final String   NEW_USER_ACCOUNT              = "NEW_USER_ACCOUNT";

  private static final String  NEW_USER_ACCOUNT_TIP          = "NEW_USER_ACCOUNT_TIP";

  private static final String  PASSPHRASE_MSG                = "PASSPHRASE_MSG";

  private static final String  USER_ACCOUNT_ID_BLANK_MSG     = "USER_ACCOUNT_ID_BLANK_MSG";

  private static final String  USER_ACCOUNT_ID_BLANK_TITLE   = "USER_ACCOUNT_ID_BLANK_TITLE";

  private static final String  INVALID_PASSPHRASE_MSG        = "INVALID_PASSPHRASE_MSG";

  private static final String  INVALID_PASSPHRASE_TITLE      = "INVALID_PASSPHRASE_TITLE";

  private static final String  INVALID_USER_ACCOUNT_ID_MSG   = "INVALID_USER_ACCOUNT_ID_MSG";

  private static final String  INVALID_USER_ACCOUNT_ID_TITLE = "INVALID_USER_ACCOUNT_ID_TITLE";

  private static final String  MESSAGE_SENT_MSG              = "MESSAGE_SENT_MSG";

  private static final String  MESSAGE_SENT_TITLE            = "MESSAGE_SENT_TITLE";

  private static final String  NO_EMAIL_ADDRESS_MSG          = "NO_EMAIL_ADDRESS_MSG";

  private static final String  NO_EMAIL_ADDRESS_TITLE        = "NO_EMAIL_ADDRESS_TITLE";

  private static final String  SMTP_MAIL                     = "smtp";

  public UserAccountLoginDialog(Frame owner)
  {
    super(owner, true);

    _requireUserPasswordRuleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.REQUIRE_USER_PASSWORD);

    Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account ID
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(USER_ACCOUNT_ID).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDField.setColumns(FIELD_WIDTH);
    _userAccountIDField.setToolTipText(__resources.getProperty(USER_ACCOUNT_ID_TIP).toString());
    centerPanel.add(_userAccountIDField, constraints);

    // password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true))
    {
      centerPanel.add(new JLabel(__resources.getProperty(PASSPHRASE).toString(), SwingConstants.RIGHT), constraints);
    }

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphraseField.setColumns(FIELD_WIDTH);
    _passphraseField.setText("");
    _passphraseField.setToolTipText(__resources.getProperty(PASSPHRASE_TIP));
    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true))
    {
      centerPanel.add(_passphraseField, constraints);
    }

    return centerPanel;
  }

  @Override
  protected void AddSouthPanelButtons(Box buttonBox)
  {
    super.AddSouthPanelButtons(buttonBox);

    _eMailPassphraseButton = new ActionButton(new EMailPassphraseAction(this));
    if (InstallationTable.GetCachedInstallationRow().Get_smtp_host().length() > 0)
    {
      buttonBox.add(_eMailPassphraseButton);
    }

    _newUserAccountButton = new ActionButton(new NewUserAccountAction());
    RulesRow ruleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.ALLOW_USER_CREATED_ACCOUNT);
    if ((ruleRow != null) && (ruleRow.RuleIsYes() == true))
    {
      buttonBox.add(_newUserAccountButton);
    }
  }

  @Override
  protected boolean PanelValid()
  {
    String userAccountID = new String(_userAccountIDField.getText().trim());
    String passphrase = new String(_passphraseField.getPassword());

    if (UserAccountTable.GetCachedUserAccountRow(userAccountID) == null)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_USER_ACCOUNT_ID_MSG), __resources.getProperty(INVALID_USER_ACCOUNT_ID_TITLE));
      _userAccountIDField.setText("");
      _userAccountIDField.requestFocus();
      _passphraseField.setText("");
      return false;
    }

    if ((_requireUserPasswordRuleRow != null) && (_requireUserPasswordRuleRow.RuleIsYes() == true) && (UserAccountTable.GetCachedUserAccountRow().Get_user_passphrase().equals(passphrase) == false))
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PASSPHRASE_MSG), __resources.getProperty(INVALID_PASSPHRASE_TITLE));
      _passphraseField.setText("");
      _passphraseField.requestFocus();
      return false;
    }

    if (UserAccountTable.GetCachedUserAccountRow().StatusIsEnabled() == false)
    {
      MessageDialog.ShowErrorMessageDialog(null, "Your user account is not enabled.", "User Account Not Enabled");
      return false;
    }

    return true;
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  void NewUserAccountAction()
  {
    UserAccountEntryDialog entryDialog = new UserAccountEntryDialog();
    Properties properties = entryDialog.WaitOnDialog();
    _userAccountIDField.setText(properties.getProperty(UserAccountEntryDialog.USER_ACCOUNT_ID));
    _passphraseField.requestFocus();
  }

  private class NewUserAccountAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 3412769066375132288L;

    /**
     * 
     */
    

    public NewUserAccountAction()
    {
      putValue(Action.NAME, __resources.getProperty(NEW_USER_ACCOUNT));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(NEW_USER_ACCOUNT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      NewUserAccountAction();
    }
  }

  private class EMailPassphraseAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long            serialVersionUID = 1L;

    private final UserAccountLoginDialog _dialog;

    public EMailPassphraseAction(UserAccountLoginDialog aDialog)
    {
      putValue(Action.NAME, __resources.getProperty(E_MAIL_PASSPHRASE).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(E_MAIL_PASSPHRASE_TIP).toString());
      _dialog = aDialog;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {

      String userAccountID = _userAccountIDField.getText().trim();
      if (userAccountID.length() == 0)
      {
        // user account id must be filled in for this to work
        MessageDialog.ShowErrorMessageDialog(_dialog, __resources.getProperty(USER_ACCOUNT_ID_BLANK_MSG).toString(), __resources.getProperty(USER_ACCOUNT_ID_BLANK_TITLE).toString());
        return;
      }

      UserAccountRow userAccountRow = GetUserAccountTable().GetRowForUserAccountID(userAccountID);

      if (userAccountRow == null)
      {
        // this user account id is not in the database
        MessageDialog.ShowErrorMessageDialog(_dialog, __resources.getProperty(INVALID_USER_ACCOUNT_ID_MSG).toString(), __resources.getProperty(INVALID_USER_ACCOUNT_ID_TITLE).toString());
        return;
      }

      if (userAccountRow.Get_e_mail_address().length() == 0)
      {
        // the e-mail address is blank
        MessageDialog.ShowErrorMessageDialog(_dialog, __resources.getProperty(NO_EMAIL_ADDRESS_MSG).toString(), __resources.getProperty(NO_EMAIL_ADDRESS_TITLE).toString());
        return;
      }

      try
      {
        // create some properties and get the default Session
        Properties props = new Properties();
        props.put("mail.smtp.host", InstallationTable.GetCachedInstallationRow().Get_smtp_host());
        Session session = Session.getDefaultInstance(props, null);

        // create a message
        String replyTo = InstallationTable.GetCachedInstallationRow().Get_administrator_e_mail_address();
        String from = replyTo;
        java.util.Date sentDate = new GregorianCalendar().getTime();

        MessageFormat msgFormat = new MessageFormat(__resources.getProperty(PASSPHRASE_MSG));
        Object[] substitutionValues = new Object[] { userAccountID, userAccountRow.Get_user_passphrase() };
        String messageBody = msgFormat.format(substitutionValues).trim();

        String subject = __resources.getProperty(PASSPHRASE);
        InternetAddress[] toList = { new InternetAddress(userAccountRow.Get_e_mail_address()) };
        Address replyToList[] = { new InternetAddress(replyTo) };

        Message newMessage = new MimeMessage(session);
        newMessage.setFrom(new InternetAddress(from));
        newMessage.setReplyTo(replyToList);
        newMessage.setRecipients(Message.RecipientType.TO, toList);
        newMessage.setSubject(subject);
        newMessage.setSentDate(sentDate);
        newMessage.setText(messageBody);

        // Send the message
        Transport transport = session.getTransport(SMTP_MAIL);
        transport.connect(InstallationTable.GetCachedInstallationRow().Get_smtp_host(), null, null);
        transport.sendMessage(newMessage, toList);

        // message sent
        msgFormat = new MessageFormat(__resources.getProperty(MESSAGE_SENT_MSG));
        substitutionValues = new Object[] { userAccountID, userAccountRow.Get_e_mail_address() };
        String message = msgFormat.format(substitutionValues).trim();

        MessageDialog.ShowInfoMessageDialog(_dialog, message, __resources.getProperty(MESSAGE_SENT_TITLE).toString());
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
      }
    }
  }

  UserAccountTable GetUserAccountTable()
  {
    return InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable();
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(UserAccountLoginDialog.class.getName());
  }

}
