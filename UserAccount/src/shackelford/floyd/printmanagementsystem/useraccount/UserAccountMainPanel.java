package shackelford.floyd.printmanagementsystem.useraccount;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.administrator.AdministratorPrePaymentsPanel;
import shackelford.floyd.printmanagementsystem.administrator.AdministratorPrintJobsPanel;
import shackelford.floyd.printmanagementsystem.administrator.AdministratorUserAccountEntryPanel;
import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.HelpMenu;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.ecommerce.ECommerceAddPrePaymentPanel;
import shackelford.floyd.printmanagementsystem.ecommerce.ECommerceRefundPrePaymentPanel;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;
import shackelford.floyd.printmanagementsystem.releasestation.ReleaseStationMainPanel;


/**
 * <p>Title: UserAccountMainPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountMainPanel
  extends AbstractEntry4Panel
{

  /**
   * 
   */
  private static final long   serialVersionUID            = 1L;

  private final JLabel        _userAccountIDLabel         = new JLabel();

  private final JLabel        _prePaymentLabel            = new JLabel();

  private final JLabel        _statusLabel                = new JLabel();

  private final JLabel        _eMailAddressLabel          = new JLabel();

  private final JLabel        _userNameLabel              = new JLabel();

  private final JLabel        _languageLabel              = new JLabel();

  private ActionMenuItem      _editCloseAccountMenuItem;

  //  private ActionMenuItem          _filePrintJobsMenuItem;
  //  private ActionMenuItem          _filePrePaymentsMenuItem;

  static Resources            __resources;

  private static final String FILE                        = "FILE";

  private static final String FILE_EXIT                   = "FILE_EXIT";

  private static final String FILE_EXIT_TIP               = "FILE_EXIT_TIP";

  private static final String FILE_OPEN                   = "FILE_OPEN";

  private static final String FILE_OPEN_TIP               = "FILE_OPEN_TIP";

  private static final String FILE_RELEASE_STATION        = "FILE_RELEASE_STATION";

  private static final String FILE_RELEASE_STATION_TIP    = "FILE_RELEASE_STATION_TIP";

  private static final String FILE_PRE_PAYMENTS           = "FILE_PRE_PAYMENTS";

  private static final String FILE_PRE_PAYMENTS_TIP       = "FILE_PRE_PAYMENTS_TIP";

  private static final String FILE_PRINT_JOBS             = "FILE_PRINT_JOBS";

  private static final String FILE_PRINT_JOBS_TIP         = "FILE_PRINT_JOBS_TIP";

  private static final String EDIT                        = "EDIT";

  private static final String EDIT_CHANGE_PASSPHRASE      = "EDIT_CHANGE_PASSPHRASE";

  private static final String EDIT_CHANGE_PASSPHRASE_TIP  = "EDIT_CHANGE_PASSPHRASE_TIP";

  private static final String EDIT_ADD_PRE_PAYMENT        = "EDIT_ADD_PRE_PAYMENT";

  private static final String EDIT_ADD_PRE_PAYMENT_TIP    = "EDIT_ADD_PRE_PAYMENT_TIP";

  private static final String EDIT_REFUND_PRE_PAYMENT     = "EDIT_REFUND_PRE_PAYMENT";

  private static final String EDIT_REFUND_PRE_PAYMENT_TIP = "EDIT_REFUND_PRE_PAYMENT_TIP";

  private static final String EDIT_DISABLE_ACCOUNT        = "EDIT_DISABLE_ACCOUNT";

  private static final String EDIT_DISABLE_ACCOUNT_TIP    = "EDIT_DISABLE_ACCOUNT_TIP";

  private static final String EDIT_CLOSE_ACCOUNT          = "EDIT_CLOSE_ACCOUNT";

  private static final String EDIT_CLOSE_ACCOUNT_TIP      = "EDIT_CLOSE_ACCOUNT_TIP";

  //  private static final String     REPORT = "REPORT";
  //  private static final String     REPORT_PRINT_JOBS = "REPORT_PRINT_JOBS";
  //  private static final String     REPORT_PRINT_JOBS_TIP = "REPORT_PRINT_JOBS_TIP";
  //  private static final String     REPORT_PRE_PAYMENTS = "REPORT_PRE_PAYMENTS";
  //  private static final String     REPORT_PRE_PAYMENTS_TIP = "REPORT_PRE_PAYMENTS_TIP";
  private static final String USER_ACCOUNT_ID             = "USER_ACCOUNT_ID";

  private static final String USER_NAME                   = "USER_NAME";

  private static final String E_MAIL_ADDRESS              = "E_MAIL_ADDRESS";

  //  private static final String     STATUS = "STATUS";
  private static final String LANGUAGE                    = "LANGUAGE";

  private static final String PRE_PAYMENT_BALANCE         = "PRE_PAYMENT_BALANCE";

  private static final String DISABLE_CONFIRMATION_MSG    = "DISABLE_CONFIRMATION_MSG";

  private static final String DISABLE_CONFIRMATION_TITLE  = "DISABLE_CONFIRMATION_TITLE";

  private static final String DISABLE_COMPLETE_MSG        = "DISABLE_COMPLETE_MSG";

  private static final String DISABLE_COMPLETE_TITLE      = "DISABLE_COMPLETE_TITLE";

  private static final String CLOSE_CONFIRMATION_MSG      = "CLOSE_CONFIRMATION_MSG";

  private static final String CLOSE_CONFIRMATION_TITLE    = "CLOSE_CONFIRMATION_TITLE";

  private static final String CLOSE_COMPLETE_MSG          = "CLOSE_COMPLETE_MSG";

  private static final String CLOSE_COMPLETE_TITLE        = "CLOSE_COMPLETE_TITLE";

  //  private static final String     TAKES_EFFECT_NEXT_LOGIN_MSG = "TAKES_EFFECT_NEXT_LOGIN_MSG";

  public UserAccountMainPanel()
  {
    super();
    Initialize(null, InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable(), UserAccountTable.GetCachedUserAccountRow());
  }

  @Override
  public void Initialize(ListPanelCursor listPanel, AbstractSQL1Table sqlTable, AbstractSQL1Row sqlRow)
  {
    _listPanel = listPanel;
    _sqlTable = sqlTable;
    _sqlRow = sqlRow;

    JMenuBar menuBar = new JMenuBar();
    menuBar.add(CreateFileMenu());
    menuBar.add(CreateEditMenu());
    //menuBar.add(CreateReportMenu());
    menuBar.add(new HelpMenu(this));
    setJMenuBar(menuBar);

    super.Initialize(listPanel, sqlTable, sqlRow);
  }

  protected JMenu CreateFileMenu()
  {
    JMenu fileMenu = new JMenu(GetResources().getProperty(FILE));

    fileMenu.add(new ActionMenuItem(new FilePrePaymentsAction()));
    fileMenu.add(new ActionMenuItem(new FilePrintJobsAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileOpenAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileReleaseStationAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileExitAction()));

    return fileMenu;
  }

  protected JMenu CreateEditMenu()
  {
    JMenu editMenu = new JMenu(GetResources().getProperty(EDIT));

    editMenu.add(new ActionMenuItem(new EditChangePassphraseAction()));

    ActionMenuItem editAddPrePayment = new ActionMenuItem(new EditAddPrePaymentAction());
    editAddPrePayment.setEnabled(ChargeIsEnabled());
    editMenu.add(editAddPrePayment);

    ActionMenuItem editRefundPrePayment = new ActionMenuItem(new EditRefundPrePaymentAction());
    editRefundPrePayment.setEnabled(RefundIsEnabled());
    editMenu.add(editRefundPrePayment);
    editMenu.addSeparator();

    editMenu.add(new ActionMenuItem(new EditDisableAccountAction()));

    _editCloseAccountMenuItem = new ActionMenuItem(new EditCloseAccountAction());
    _editCloseAccountMenuItem.setEnabled(GetUserAccountRow().Get_pre_payment_balance().doubleValue() == 0d);
    editMenu.add(_editCloseAccountMenuItem);

    return editMenu;
  }

  /*
    protected JMenu CreateReportMenu()
    {
      JMenu reportMenu = new JMenu(GetResources().getProperty(REPORT));

      reportMenu.add(new ActionMenuItem(new ReportPrintJobsAction(this)));
      reportMenu.add(new ActionMenuItem(new ReportPrePaymentsAction(this)));

      return reportMenu;
    }
  */

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    Font valueFont = centerPanel.getFont();
    valueFont = new Font(valueFont.getName(), Font.BOLD, valueFont.getSize() + 2);

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account ID
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(USER_ACCOUNT_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _userAccountIDLabel.setFont(valueFont);
    centerPanel.add(_userAccountIDLabel, constraints);

    // user Name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(USER_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _userNameLabel.setFont(valueFont);
    centerPanel.add(_userNameLabel, constraints);

    // status
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(STATUS), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _statusLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _statusLabel.setFont(valueFont);
    centerPanel.add(_statusLabel, constraints);

    // language
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(LANGUAGE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _languageLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _languageLabel.setFont(valueFont);
    centerPanel.add(_languageLabel, constraints);

    // pre payment balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(PRE_PAYMENT_BALANCE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _prePaymentLabel.setFont(valueFont);
    centerPanel.add(_prePaymentLabel, constraints);

    // e-mail address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(E_MAIL_ADDRESS), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _eMailAddressLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _eMailAddressLabel.setFont(valueFont);
    centerPanel.add(_eMailAddressLabel, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    _saveButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  public void AssignRowToFields()
  {
    super.AssignRowToFields();

    _userAccountIDLabel.setText(GetUserAccountRow().Get_user_account_id());
    _userNameLabel.setText(GetUserAccountRow().Get_user_name());

    _languageLabel.setText(GetUserAccountRow().Get_language());
    _prePaymentLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + GetUserAccountRow().GetFormatted_pre_payment_balance());
    _eMailAddressLabel.setText(GetUserAccountRow().Get_e_mail_address());
  }

  @Override
  public void AssignFieldsToRow()
  {
    super.AssignFieldsToRow();
  }

  private void RefreshPanel()
  {
    AssignRowToFields();
  }

  protected UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  protected UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable) _sqlTable;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  @Override
  public void EndOfPanel(char action)
  {
    RefreshPanel();
  }

  @Override
  public void PanelStateChange(char action)
  {
    RefreshPanel();
  }

  boolean ChargeIsEnabled()
  {
    return ((InstallationTable.GetCachedInstallationRow().Get_cc_charge_account().trim().length() > 0) || (InstallationTable.GetCachedInstallationRow().Get_egold_charge_account().trim().length() > 0) || (InstallationTable
      .GetCachedInstallationRow().Get_paypal_charge_account().trim().length() > 0));
  }

  boolean RefundIsEnabled()
  {
    return ((InstallationTable.GetCachedInstallationRow().Get_cc_refund_account().trim().length() > 0) || (InstallationTable.GetCachedInstallationRow().Get_egold_refund_account().trim().length() > 0) || (InstallationTable
      .GetCachedInstallationRow().Get_paypal_refund_account().trim().length() > 0));
  }

  void FileExitAction()
  {
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
    dispose();
  }

  private class FileExitAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -3598318346553414880L;

    /**
     * 
     */
    

    public FileExitAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_EXIT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_EXIT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileExitAction();
    }
  }

  protected void FilePrePaymentsAction()
  {
    AdministratorPrePaymentsPanel prePaymentsPanel = new AdministratorPrePaymentsPanel(GetUserAccountRow().Get_user_account_id());
    prePaymentsPanel.setVisible(true);
  }

  protected class FilePrePaymentsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 4220198481802005089L;

    /**
     * 
     */
    

    public FilePrePaymentsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRE_PAYMENTS).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRE_PAYMENTS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrePaymentsAction();
    }
  }

  protected void FilePrintJobsAction()
  {
    AdministratorPrintJobsPanel printJobsPanel = new AdministratorPrintJobsPanel(GetUserAccountRow().Get_user_account_id());
    printJobsPanel.setVisible(true);
  }

  protected class FilePrintJobsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8662107221058042862L;

    /**
     * 
     */
    

    public FilePrintJobsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRINT_JOBS).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRINT_JOBS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintJobsAction();
    }
  }

  void FileOpenAction()
  {
    AdministratorUserAccountEntryPanel panel = new AdministratorUserAccountEntryPanel(null, GetUserAccountTable(), GetUserAccountRow());
    panel.AddPanelListener(this);
    panel.setVisible(true);
    invalidate();
  }

  private class FileOpenAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 2364086631960739844L;

    /**
     * 
     */
    

    public FileOpenAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_OPEN));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_OPEN_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileOpenAction();
    }
  }

  void FileReleaseStationAction()
  {
    ReleaseStationMainPanel releaseStation = new ReleaseStationMainPanel();
    releaseStation.AddPanelListener(this);
    releaseStation.setVisible(true);
    releaseStation.RefreshList();
    invalidate();
  }

  private class FileReleaseStationAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -4837878154755939918L;

    /**
     * 
     */
    

    public FileReleaseStationAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_RELEASE_STATION));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_RELEASE_STATION_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileReleaseStationAction();
    }
  }

  void EditChangePassphraseAction()
  {
    UserAccountChangePassphrasePanel panel = new UserAccountChangePassphrasePanel(null, InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable(),
      UserAccountTable.GetCachedUserAccountRow());
    panel.setVisible(true);
    invalidate();
  }

  private class EditChangePassphraseAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -2048078445641734715L;

    /**
     * 
     */
    

    public EditChangePassphraseAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_CHANGE_PASSPHRASE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_CHANGE_PASSPHRASE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditChangePassphraseAction();
    }
  }

  void EditAddPrePaymentAction()
  {
    ECommerceAddPrePaymentPanel panel = new ECommerceAddPrePaymentPanel(this);
    panel.AddPanelListener(this);
    panel.setVisible(true);
    invalidate();
  }

  private class EditAddPrePaymentAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 537280727349480927L;

    /**
     * 
     */
    

    public EditAddPrePaymentAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_ADD_PRE_PAYMENT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_ADD_PRE_PAYMENT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditAddPrePaymentAction();
    }
  }

  void EditRefundPrePaymentAction()
  {
    ECommerceRefundPrePaymentPanel panel = new ECommerceRefundPrePaymentPanel(this);
    panel.AddPanelListener(this);
    panel.setVisible(true);
    invalidate();
  }

  private class EditRefundPrePaymentAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 1647194278769663230L;

    /**
     * 
     */
    

    public EditRefundPrePaymentAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_REFUND_PRE_PAYMENT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_REFUND_PRE_PAYMENT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditRefundPrePaymentAction();
    }
  }

  void EditDisableAccountAction()
  {
    setVisible(false);

    int rc = JOptionPane.showConfirmDialog(this, GetResources().getProperty(DISABLE_CONFIRMATION_MSG), GetResources().getProperty(DISABLE_CONFIRMATION_TITLE), JOptionPane.YES_NO_OPTION);
    if (rc == JOptionPane.YES_OPTION)
    {
      GetUserAccountRow().Set_status(AbstractSQL4Row.STATUS_DISABLED_CODE);
      GetUserAccountRow().Update();
      JOptionPane.showConfirmDialog(this, GetResources().getProperty(DISABLE_COMPLETE_MSG), GetResources().getProperty(DISABLE_COMPLETE_TITLE), JOptionPane.OK_OPTION);
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
    else
    {
      setVisible(true);
    }
  }

  private class EditDisableAccountAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 1214085055141802759L;

    public EditDisableAccountAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_DISABLE_ACCOUNT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_DISABLE_ACCOUNT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditDisableAccountAction();
    }
  }

  void EditCloseAccountAction()
  {
    setVisible(false);

    int rc = JOptionPane.showConfirmDialog(this, GetResources().getProperty(CLOSE_CONFIRMATION_MSG), GetResources().getProperty(CLOSE_CONFIRMATION_TITLE), JOptionPane.YES_NO_OPTION);
    if (rc == JOptionPane.YES_OPTION)
    {
      GetUserAccountRow().Set_status(AbstractSQL4Row.STATUS_CLOSED_CODE);
      GetUserAccountRow().Update();
      JOptionPane.showConfirmDialog(this, GetResources().getProperty(CLOSE_COMPLETE_MSG), GetResources().getProperty(CLOSE_COMPLETE_TITLE), JOptionPane.OK_OPTION);
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
    else
    {
      setVisible(true);
    }
  }

  private class EditCloseAccountAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -71101200386606341L;

    public EditCloseAccountAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_CLOSE_ACCOUNT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_CLOSE_ACCOUNT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditCloseAccountAction();
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(UserAccountMainPanel.class.getName());
  }

}
