package shackelford.floyd.printmanagementsystem.useraccount;

import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;




/**
 * <p>Title: UserAccountChangePassphrasePanelManager</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountChangePassphrasePanelManager
  implements PanelListener
{


  public UserAccountChangePassphrasePanelManager()
  {
    UserAccountChangePassphrasePanel entryPanel = new UserAccountChangePassphrasePanel();
    entryPanel.AddPanelListener(this);
    entryPanel.setVisible(true);
  }

  @Override
  public void EndOfPanel (char action)
  {
    if (UserAccountTable.GetCachedUserAccountRow().Get_require_new_password().booleanValue() == true)
    {
      if (action == PanelNotifier.ACTION_OK)
      {
        UserAccountTable.GetCachedUserAccountRow().Set_require_new_password(false);
        UserAccountTable.GetCachedUserAccountRow().Update();

        UserAccountMainPanel mainPanel = new UserAccountMainPanel();
        mainPanel.AddPanelListener(new UserAccount());
        mainPanel.setVisible(true);
      }
      else
      {
        // launch this application again ...
        SystemUtilities.LaunchApplication(GlobalAttributes.__applicationName);
        // ... because we're getting out of here
        System.exit(UserAccount.EXIT_OK);
      }
    }
  }

  @Override
  public void PanelStateChange (char action)
  {
    // do nothing
  }

}
