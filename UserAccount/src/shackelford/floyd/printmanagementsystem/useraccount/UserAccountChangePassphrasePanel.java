package shackelford.floyd.printmanagementsystem.useraccount;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: UserAccountChangePassphrasePanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UserAccountChangePassphrasePanel
  extends AbstractEntry1Panel
{

  /**
   * 
   */
  private static final long    serialVersionUID                      = 1L;

  private final JPasswordField _currentPassphrase                    = new JPasswordField();

  private final JPasswordField _newUserAccountPassphrase             = new JPasswordField();

  private final JPasswordField _newUserAccountPassphraseAgain        = new JPasswordField();

  private static Resources     __resources;

  private static final String  CURRENT_USER_ACCOUNT_PASSPHRASE       = "CURRENT_USER_ACCOUNT_PASSPHRASE";

  private static final String  CURRENT_USER_ACCOUNT_PASSPHRASE_TIP   = "CURRENT_USER_ACCOUNT_PASSPHRASE_TIP";

  private static final String  INVALID_PASSPHRASE_MSG                = "INVALID_PASSPHRASE_MSG";

  private static final String  INVALID_PASSPHRASE_TITLE              = "INVALID_PASSPHRASE_TITLE";

  private static final String  NEW_USER_ACCOUNT_PASSPHRASE           = "NEW_USER_ACCOUNT_PASSPHRASE";

  private static final String  NEW_USER_ACCOUNT_PASSPHRASE_TIP       = "NEW_USER_ACCOUNT_PASSPHRASE_TIP";

  private static final String  NEW_USER_ACCOUNT_PASSPHRASE_AGAIN     = "NEW_USER_ACCOUNT_PASSPHRASE_AGAIN";

  private static final String  NEW_USER_ACCOUNT_PASSPHRASE_AGAIN_TIP = "NEW_USER_ACCOUNT_PASSPHRASE_AGAIN_TIP";

  private static final String  NON_MATCHING_PASSPHRASE_MSG           = "NON_MATCHING_PASSPHRASE_MSG";

  private static final String  NON_MATCHING_PASSPHRASE_TITLE         = "NON_MATCHING_PASSPHRASE_TITLE";

  private static final int     PASSPHRASE_COLUMNS                    = 20;

  public UserAccountChangePassphrasePanel()
  {
    super();
  }

  public UserAccountChangePassphrasePanel(ListPanelCursor listPanel, UserAccountTable userAccountTable, UserAccountRow userAccountRow)
  {
    super();
    Initialize(listPanel, userAccountTable, userAccountRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // current password
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENT_USER_ACCOUNT_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _currentPassphrase.setColumns(PASSPHRASE_COLUMNS);
    _currentPassphrase.setHorizontalAlignment(SwingConstants.LEFT);
    _currentPassphrase.setToolTipText(__resources.getProperty(CURRENT_USER_ACCOUNT_PASSPHRASE_TIP));
    centerPanel.add(_currentPassphrase, constraints);

    // new password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_USER_ACCOUNT_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newUserAccountPassphrase.setColumns(PASSPHRASE_COLUMNS);
    _newUserAccountPassphrase.setHorizontalAlignment(SwingConstants.LEFT);
    _newUserAccountPassphrase.setToolTipText(__resources.getProperty(NEW_USER_ACCOUNT_PASSPHRASE_TIP));
    centerPanel.add(_newUserAccountPassphrase, constraints);

    // new user account passphrase again
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_USER_ACCOUNT_PASSPHRASE_AGAIN), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newUserAccountPassphraseAgain.setColumns(PASSPHRASE_COLUMNS);
    _newUserAccountPassphraseAgain.setHorizontalAlignment(SwingConstants.LEFT);
    _newUserAccountPassphraseAgain.setToolTipText(__resources.getProperty(NEW_USER_ACCOUNT_PASSPHRASE_AGAIN_TIP));
    centerPanel.add(_newUserAccountPassphraseAgain, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    String passphrase = new String(_currentPassphrase.getPassword());
    String newPassphrase = new String(_newUserAccountPassphrase.getPassword());
    String newPassphraseAgain = new String(_newUserAccountPassphraseAgain.getPassword());

    // check to see if the current passphrase is valid
    if (passphrase.equals(GetUserAccountRow().Get_user_passphrase()) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PASSPHRASE_TITLE), __resources.getProperty(INVALID_PASSPHRASE_MSG));
      _currentPassphrase.setText("");
      _currentPassphrase.requestFocus();
      return false;
    }

    // check to see if the new passphrases match
    if (newPassphrase.equals(newPassphraseAgain) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(NON_MATCHING_PASSPHRASE_TITLE), __resources.getProperty(NON_MATCHING_PASSPHRASE_MSG));
      _newUserAccountPassphrase.setText("");
      _newUserAccountPassphraseAgain.setText("");
      _newUserAccountPassphrase.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    // do nothing
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetUserAccountRow().Set_user_passphrase(new String(_newUserAccountPassphrase.getPassword()));
  }

  private UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(UserAccountChangePassphrasePanel.class.getName());
  }
}
