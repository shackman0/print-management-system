///////////////////////////////////////////////////////////////////////////////////////////////////////
// StandardTools.cpp: implementation of the CMS Standard Tools library.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "StandardTools.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_CONSOLE_COLOR					(FOREGROUND_BLUE | FOREGROUND_GREEN | FOREGROUND_RED)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Win32 API Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CSystemTimer Class Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	void CSystemTimer::StopTimer()
	{
		__int64	nKernelStopTime;
		__int64	nUserStopTime;

		// Get the stop times
		GetTimes( nKernelStopTime, nUserStopTime, m_bThreadTimer );

		// Compute the elapsed times
		m_dKernelTime	= ConvertToSeconds( nKernelStopTime - m_nKernelStartTime );
		m_dUserTime		= ConvertToSeconds( nUserStopTime - m_nUserStartTime );
		m_dClockTime	= ConvertToSeconds( ::GetTickCount() - m_dwStartTickCount );
	}

	void CSystemTimer::GetTotalTimes( const bool& bUseThreadTimes )
	{
		__int64	nKernelTime;
		__int64	nUserTime;

		// Clear out the values
		Reset();

		// Get the stop times
		GetTimes( nKernelTime, nUserTime, bUseThreadTimes );

		// Convert to Seconds
		m_dKernelTime	= ConvertToSeconds( nKernelTime );
		m_dUserTime		= ConvertToSeconds( nUserTime );
	}

	void CSystemTimer::Reset()
	{
		m_nKernelStartTime	= 0;
		m_nUserStartTime	= 0;
		m_dwStartTickCount	= 0;
		m_dKernelTime		= 0.0;
		m_dUserTime			= 0.0;
		m_dClockTime		= 0.0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	void CSystemTimer::GetTimes( __int64& nKernelTime, __int64& nUserTime, 
		const bool& bUseThreadTimes )
	{
		FILETIME	ftCreationTime;
		FILETIME	ftStopTime;
		FILETIME	ftKernelTime;
		FILETIME	ftUserTime;

		if ( bUseThreadTimes == true )
		{
			DEBUG_FAIL( ::GetThreadTimes(::GetCurrentThread(), &ftCreationTime, &ftStopTime, 
				&ftKernelTime, &ftUserTime) );
		}
		else
		{
			DEBUG_FAIL( ::GetProcessTimes(::GetCurrentProcess(), &ftCreationTime, &ftStopTime, 
				&ftKernelTime, &ftUserTime) );
		}

		::CopyMemory( &nKernelTime, &ftKernelTime, sizeof(nKernelTime) );
		::CopyMemory( &nUserTime, &ftUserTime, sizeof(nUserTime) );
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CConsole Class Implementation
///////////////////////////////////////////////////////////////////////////////////////////////////////

	#ifdef USE_CMS_CONSOLE

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Variables
	///////////////////////////////////////////////////////////////////////////////////////////////////

	DWORD						CConsole::m_dwConsoleCount	= 0;
	BOOL						CConsole::m_bCreated		= FALSE;
	HANDLE						CConsole::m_hError			= INVALID_HANDLE_VALUE;
	HANDLE						CConsole::m_hInput			= INVALID_HANDLE_VALUE;
	HANDLE						CConsole::m_hOutput			= INVALID_HANDLE_VALUE;

	IMPLEMENT_STATIC_THREAD_LOCK(CConsole);

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	bool CConsole::WaitForKeyPress( const DWORD& dwTimeOut )
	{
		bool				bContinue			= true;
		bool				bReturnMe			= false;
		DWORD				dwRecordsWaiting	= 0;
		DWORD				dwRecordsRead		= 0;
		DWORD				dwRecordsAllocated	= 0;
		PINPUT_RECORD		pIR					= NULL;
		unsigned register	nCounter			= 0;

		// Check assumptions
		_ASSERTE( IsInputConsoleValid() );
		_ASSERTE( m_dwConsoleCount > 0 );

		// Clear out the buffer
		DEBUG_FAIL( ::FlushConsoleInputBuffer(m_hInput) );

		// While events are coming, the timeout occurs, or told to stop
		while ( (::WaitForSingleObject(m_hInput, dwTimeOut) == WAIT_OBJECT_0) &&
				(bContinue == true) )
		{
			if ( ::GetNumberOfConsoleInputEvents(m_hInput, &dwRecordsWaiting) )
			{
				if ( dwRecordsWaiting > dwRecordsAllocated )
				{
					if ( pIR != NULL )
					{
						delete [] pIR;
					}
					pIR = new INPUT_RECORD[dwRecordsWaiting];
					dwRecordsAllocated = dwRecordsWaiting;
				}

				if ( ::PeekConsoleInput(m_hInput, pIR, dwRecordsAllocated, &dwRecordsRead) )
				{
					for (	nCounter = 0 ;
							(nCounter < dwRecordsRead) && (bReturnMe == false) ; 
							nCounter++ )
					{
						// If it is a key event then return
						if ( (pIR[nCounter]).EventType == KEY_EVENT )
						{
							bReturnMe = true;
							bContinue = false;
						}
					}

					// If there was not a key event, then flush
					if ( bReturnMe == false )
					{
						DEBUG_FAIL( ::FlushConsoleInputBuffer(m_hInput) );
					}
				}
				else
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("Could not peek at waiting events!\n") );
					bContinue = false;
				}
			}
			else
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("Could not retrieve number of waiting events!\n") );
				bContinue = false;
			}
		}

		// Free the memory (if applicable)
		if ( pIR != NULL )
		{
			delete [] pIR;
		}

		// Return the result
		return bReturnMe;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	void CConsole::WriteConsoleText( HANDLE hConsole, LPCTSTR lpszFormat, va_list& args )
	{
		// Make it thread safe
		STATIC_LOCK_THREAD();

		static TCHAR	lpszBuffer[4096];
		static int		nChars;

		// Format the text using the variable arguments
		nChars = _vsntprintf( lpszBuffer, _TLEN(lpszBuffer), lpszFormat, args );
		_ASSERTE( nChars >= 0 );

		// Write the text
		WriteConsole( hConsole, lpszBuffer, static_cast<DWORD>(nChars) );
	}

	void CConsole::WriteConsole( HANDLE hConsole, LPCTSTR lpszText, const DWORD& dwLength )
	{
		static DWORD	dwCharsWritten;

		// Check assumptions
		_ASSERTE( IsHandleValid(hConsole) );

		// Write the console
		DEBUG_FAIL( ::WriteConsole( m_hOutput, lpszText, dwLength, &dwCharsWritten, NULL) );

		// Make sure all the chars were written
		_ASSERTE( dwCharsWritten == dwLength );
	}

	WORD CConsole::GetTextColor( HANDLE hConsole )
	{
		static CThreadLock					lock;
		static CONSOLE_SCREEN_BUFFER_INFO	BufferInfo;
		WORD								wReturnMe;

		// Check assumptions
		_ASSERTE( IsHandleValid(hConsole) );

		// Control the scope of the thread locking
		{
			// Make it thread safe
			CTempLock	temp(&lock);
			if ( ::GetConsoleScreenBufferInfo(hConsole, &BufferInfo) )
			{
				wReturnMe = BufferInfo.wAttributes;
			}
			else
			{
				wReturnMe = DEFAULT_CONSOLE_COLOR;
			}
		}

		return wReturnMe;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////

	#endif // #ifdef USE_CMS_CONSOLE

///////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // #ifdef _WIN32
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of StandardTools.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
