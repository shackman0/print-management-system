///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL6Parser.cpp: implementation of the CPCL6Parser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL6Parser.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Known paper size matchings
///////////////////////////////////////////////////////////////////////////////////////////////////////

// These should be listed in order of frequency of use
const PaperSizeInfo	CPCL6Parser::PCL6FormNames[] =
{
	{ "LETTER",		DMPAPER_LETTER				},
	{ "LEGAL",		DMPAPER_LEGAL				},
	{ "A4",			DMPAPER_A4					},
	{ "LEDGER",		DMPAPER_LEDGER				},
	{ "EXEC",		DMPAPER_EXECUTIVE			},
	{ "A3",			DMPAPER_A3					},
	{ "A5",			DMPAPER_A5					},
	{ "B4",			DMPAPER_B4					},
	{ "JIS B4",		DMPAPER_B4					},
	{ "B5",			DMPAPER_B5					},
	{ "JIS B5",		DMPAPER_B5					},
	{ "COM10",		DMPAPER_ENV_10				},
	{ "DL",			DMPAPER_ENV_DL				},
	{ "C5",			DMPAPER_ENV_C5				},
	{ "B5 ENV",		DMPAPER_ENV_B5				},
	{ "MONARCH",	DMPAPER_ENV_MONARCH			},
	//{ "",			DMPAPER_CSHEET				},
	//{ "",			DMPAPER_DSHEET				},
	//{ "",			DMPAPER_ESHEET				},
	//{ "",			DMPAPER_LETTERSMALL			},
	//{ "",			DMPAPER_TABLOID				},
	//{ "",			DMPAPER_STATEMENT			},
	//{ "",			DMPAPER_A4SMALL				},
	//{ "",			DMPAPER_FOLIO				},
	//{ "",			DMPAPER_10X14				},
	//{ "",			DMPAPER_11X17				},
	//{ "",			DMPAPER_NOTE				},
	//{ "",			DMPAPER_ENV_9				},
	//{ "",			DMPAPER_ENV_11				},
	//{ "",			DMPAPER_ENV_12				},
	//{ "",			DMPAPER_ENV_14				},
	//{ "",			DMPAPER_ENV_C65				},
	//{ "",			DMPAPER_ENV_C6				},
	//{ "",			DMPAPER_ENV_B4				},
	//{ "",			DMPAPER_ENV_B6				},
	//{ "",			DMPAPER_ENV_ITALY			},
	//{ "",			DMPAPER_TABLOID				},
	//{ "",			DMPAPER_ENV_PERSONAL		},
	//{ "",			DMPAPER_FANFOLD_US			},
	//{ "",			DMPAPER_FANFOLD_STD_GERMAN	},
	//{ "",			DMPAPER_FANFOLD_LGL_GERMAN	},
	//{ "",			DMPAPER_STATEMENT			},
	{ NULL,			0							}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Parse Operation
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL6Parser::Parse( CPrintParserData* pParserData )
{
	bool	bParsed		= false;	// Assume failure

	// Check Assumptions
	_ASSERTE( pParserData != NULL );
	_ASSERTE( m_pParserData == NULL );

	// Set the values
	m_pParserData	= pParserData;
	m_dwPageCount	= m_pParserData->GetPageCount();
	m_nCopies		= m_pParserData->GetCopies();

	// Verify the job is actually PCL6
	if ( VerifyPCL6() )
	{
		// Set success
		bParsed = true;

		// Process the job
		CPCL6Attribute*			pNew = NULL;

		try
		{
			// Until the end of the buffer is reached
			while ( m_pParserData->GetBytesRemaining() )
			{
				if ( pNew == NULL )
				{
					pNew = new CPCL6Attribute;
				}

				if ( pNew->ReadFromBuffer(*m_pParserData) )
				{
					// This will set pNew to be NULL
					// and the AttributeList destructor
					// will do all the clean up.
					m_AttributeList.AddAttribute( pNew );
				}
				else
				{
					// Do the Operator
					HandleOperator();
				}
			}
		}
		catch( CPrintParserException& e )
		{
#ifdef USE_DEBUG_OUTPUT
			DEBUG_TEXT( _T("PCL6 Parser Caught Exception: %s\n"), e.GetErrorMessage() );
#endif
			m_pParserData->SetErrorInfo( e.GetErrorMessage() );
		}

		// Set the values
		if ( m_nCopies )
		{
			m_pParserData->SetCopies( m_nCopies );
		}
		if ( m_dwPageCount )
		{
			m_pParserData->SetPageCount( m_dwPageCount );
		}

		// Do some clean up if the last attribute was not added to
		// the attribute list
		if ( pNew != NULL )
		{
			delete pNew;
		}
	}

	// Clear the values
	m_pParserData	= NULL;
	m_dwPageCount	= 0;
	m_nCopies		= 0;

	// Return Success or Failure
	return bParsed;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Process Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Verifies whether or not the data is actually PCL6, and sets the appropriate Endian-ness
bool CPCL6Parser::VerifyPCL6()
{
	bool	bReturnMe	= false;
	BYTE	nHeaderByte	= m_pParserData->ReadByte();

	switch(	nHeaderByte )
	{
	// Little Endian Header
	case PCL_HEADER_LITTLEENDIAN:
		bReturnMe = true;
		m_pParserData->SetByteOrder( CPrintJobBuffer::ByteOrderLittleEndian );
		break;

	// Big Endian Header
	case PCL_HEADER_BIGENDIAN:
		bReturnMe = true;
		m_pParserData->SetByteOrder( CPrintJobBuffer::ByteOrderBigEndian );
		DEBUG_TEXT( _T("PCL6 is using BigEndian!!!\n") );
		break;

	// Not PCL6!
	default:
		// Put the byte back
		m_pParserData->BackUp( sizeof(BYTE) );
		DEBUG_PCL6_TEXT( _T("Not a PCL6 file!!!\n") );
		break;
	}

	// Skip to the end of the header
	if ( bReturnMe == true )
	{
		while( m_pParserData->ReadByte() != PCL_HEADER_END )
			;	// Do nothing
	}

	return bReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operator Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

// PCL6 Operator:	BeginPage
void CPCL6Parser::OperatorBeginPage()
{
	CPCL6Attribute*	pAttribute	= NULL;

	// Look up the orientation attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_ORIENTATION );
	if ( pAttribute != NULL )
	{
		ProcessOrientation( pAttribute );
	}

	// Look up the media size attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_MEDIASIZE );
	if ( pAttribute != NULL )
	{
		ProcessMediaSize( pAttribute );
	}

	// Look up the custom media size attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_CUSTOMMEDIASIZE );
	if ( pAttribute != NULL )
	{
		CPCL6Attribute*	pUnits	= NULL;
		pUnits = m_AttributeList.FindAttribute( PCL_ATTRIB_CUSTOMMEDIASIZEUNITS );
		if ( pUnits != NULL )
		{
			ProcessCustomMediaSize( pAttribute, pUnits );
		}
	}

	// Look up the duplex attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_DUPLEXPAGEMODE );
	if ( pAttribute != NULL )
	{
		ProcessDuplexing( pAttribute );
	}
	else
	{
		// Look up the simplex attribute
		pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_SIMPLEXPAGEMODE );
		if ( pAttribute != NULL )
		{
			m_pParserData->SetDuplex( DMDUP_SIMPLEX );
		}
	}
}

// PCL6 Operator:	BeginSession
void CPCL6Parser::OperatorBeginSession()
{
	CPCL6Attribute*	pUnitsAttribute		= NULL;
	CPCL6Attribute* pMeasureAttribute	= NULL;

	// Look up the UnitsPerMeasure attribute
	pUnitsAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_UNITSPERMEASURE );
	if ( pUnitsAttribute != NULL )
	{
		// Look up the Measure attribute
		pMeasureAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_MEASURE );
		if ( pMeasureAttribute != NULL )
		{
			// Pass the attributes to the ProcessResolution function
			ProcessResolution( pUnitsAttribute, pMeasureAttribute );
		}
	}
}

// PCL6 Operator:	EndPage
void CPCL6Parser::OperatorEndPage()
{
	static bool		bMismatchedCopies	= false;
	CPCL6Attribute*	pAttribute	= NULL;
	WORD			nCopies;

	// Look up the copies attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_PAGECOPIES );
	if ( pAttribute != NULL )
	{
		// Ensure the data type is what is expected
		if ( pAttribute->GetDataType() != CPCL6DataType::PCL_DATATYPE_UINT16 )
		{
			throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid DataType for attribute PageCopies") );
		}

		// Read the value
		nCopies = pAttribute->GetData().nUInt16;
	}
	else
	{
		// If PageCopies attribute is not set, the printer assumes
		// a value of 1 (this was tested on an HP LaserJet 4000 TN).
		nCopies	= 1;
	}

	// If nCopies is 0, nothing will print
	if ( nCopies != 0 )
	{
		if ( bMismatchedCopies == false)
		{
			// If the copies have not yet been set
			if ( m_nCopies != 0 )
			{
				// If the copies have been set, and match the current value
				if ( m_nCopies == nCopies )
				{
					// Copies have been set, and do match the current value
					// So, just count the new page
					m_dwPageCount++;
				}
				else
				{
					// The copies are now mismatched
					bMismatchedCopies = true;

					// Count each copy of a previous page as a seperate page
					m_dwPageCount *= m_nCopies;

					// Count each copy of the current page as a seperate page
					m_dwPageCount += nCopies;

					// Set the copies to be 1
					m_nCopies = 1;

					// Display some debug text
					DEBUG_TEXT( _T("Found Mismatched Copy Count!\n") );
				}
			}
			else
			{
				// Set the copies, and count the new page
				m_nCopies = nCopies;
				m_dwPageCount++;
			}
		}
		else
		{
			// If the copies are mismatched, treat each copy as a seperate page
			m_dwPageCount += nCopies;
		}
	}
}

// PCL6 Operator:	SetColorSpace
void CPCL6Parser::OperatorSetColorSpace()
{
	CPCL6Attribute*	pAttribute	= NULL;

	// Look up the copies attribute
	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_COLORSPACE );
	if ( pAttribute != NULL )
	{
		ProcessColorSpace( pAttribute );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Process Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Sets the Color Space based on the parsed value
void CPCL6Parser::ProcessColorSpace( const CPCL6Attribute* pColorSpace )
{
	// Check assumed values
	_ASSERTE( pColorSpace != NULL );

	// Verify it is the needed attribute
	_ASSERTE( pColorSpace->GetAttributeID() == PCL_ATTRIB_COLORSPACE );

	// Ensure the data type is what is expected
	if ( pColorSpace->GetDataType() !=  CPCL6DataType::PCL_DATATYPE_UBYTE )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute ColorSpace") );
	}

	// Process the value
	pcl6_ColorSpace		eCS = static_cast<pcl6_ColorSpace>(pColorSpace->GetData().nByte);
	switch( eCS )
	{
	case pcl6_eGray:
		m_pParserData->SetColor( DMCOLOR_MONOCHROME );
		break;

	case pcl6_eRGB:
	case pcl6_eSRGB:
		m_pParserData->SetColor( DMCOLOR_COLOR );
		break;

	default:
		DEBUG_TEXT( _T("INVALID ColorSpace: %d\n"), eCS );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute ColorSpace") );
		break;
	}
}

// Sets the MediaSize based on the parsed value
void CPCL6Parser::ProcessCustomMediaSize( const CPCL6Attribute* pSize, const CPCL6Attribute* pUnits )
{
	// Verify that the attributes are the ones needed
	_ASSERTE( pSize->GetAttributeID() == PCL_ATTRIB_CUSTOMMEDIASIZE );
	_ASSERTE( pUnits->GetAttributeID() == PCL_ATTRIB_CUSTOMMEDIASIZEUNITS );

	float nLength	= 0;
	float nWidth	= 0;

	// Get the XY values
	switch( pSize->GetDataType() )
	{
	case CPCL6DataType::PCL_DATATYPE_UINT16_XY:
		nLength	= (float)(pSize->GetData().nUInt16XY[ CPCL6DataType::PCL_XY_Y_VALUE ]);
		nWidth	= (float)(pSize->GetData().nUInt16XY[ CPCL6DataType::PCL_XY_X_VALUE ]);
		break;

	case CPCL6DataType::PCL_DATATYPE_REAL32_XY:
		nLength	= pSize->GetData().nReal32XY[ CPCL6DataType::PCL_XY_Y_VALUE ];
		nWidth	= pSize->GetData().nReal32XY[ CPCL6DataType::PCL_XY_X_VALUE ];
		break;

	default:
		DEBUG_TEXT( _T("Invalid datatype for CustomMediaSize\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute CustomMediaSize") );
		break;
	}

	// Ensure the data type is what is expected
	if ( pUnits->GetDataType() != CPCL6DataType::PCL_DATATYPE_UBYTE )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute Units") );
	}

	// And convert to Tenths of a Millimeter (which is what DEVMODE wants)
	switch( pUnits->GetData().nByte )
	{
	case pcl6_eInch:
		m_pParserData->SetPaperLength( InchesToTenthsOfAMillimeter(nLength) );
		m_pParserData->SetPaperWidth( InchesToTenthsOfAMillimeter(nWidth) );
		break;

	case pcl6_eMillimeter:
		m_pParserData->SetPaperLength( MillimetersToTenthsOfAMillimeter(nLength) );
		m_pParserData->SetPaperWidth( MillimetersToTenthsOfAMillimeter(nWidth) );
		break;

	case pcl6_eTenthsOfAMillimeter:
		m_pParserData->SetPaperLength( TenthsOfAMillimeterToTenthsOfAMillimeter(nLength) );
		m_pParserData->SetPaperWidth( TenthsOfAMillimeterToTenthsOfAMillimeter(nWidth) );
		break;

	default:
		DEBUG_TEXT( _T("Invalid value for Units!\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute Units") );
		break;
	}
}

// Sets the duplexing based on the parsed value
void CPCL6Parser::ProcessDuplexing( const CPCL6Attribute* pDuplexing )
{
	// Check assumed values
	_ASSERTE( pDuplexing != NULL );

	// Verify it is the needed attribute
	_ASSERTE( pDuplexing->GetAttributeID() == PCL_ATTRIB_DUPLEXPAGEMODE );

	// Ensure the data type is what is expected
	if ( pDuplexing->GetDataType() != CPCL6DataType::PCL_DATATYPE_UBYTE )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute DuplexPageMode") );
	}

	// Process the value
	pcl6_DuplexPageMode		eDPM = static_cast<pcl6_DuplexPageMode>(pDuplexing->GetData().nByte);

	switch( eDPM )
	{
	case pcl6_eDuplexHorizontalBinding:
		m_pParserData->SetDuplex( DMDUP_HORIZONTAL );
		break;

	case pcl6_eDuplexVerticalBinding:
		m_pParserData->SetDuplex( DMDUP_VERTICAL );
		break;

	default:
		DEBUG_TEXT( _T("INVALID duplexing!\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute DuplexPageMode") );
		break;
	}
}

// Sets the MediaSize based on the parsed Value
void CPCL6Parser::ProcessMediaSize( const CPCL6Attribute* pMediaSize )
{
	// Verify it is the needed attribute
	_ASSERTE( pMediaSize->GetAttributeID() == PCL_ATTRIB_MEDIASIZE );

	// Handle it based on its datatype
	switch( pMediaSize->GetDataType() )
	{
	case CPCL6DataType::PCL_DATATYPE_UBYTE:

		switch( pMediaSize->GetData().nByte )
		{
		case pcl6_eDefaultPaperSize:	// Added in 2.1
		case pcl6_eLetter:
			m_pParserData->SetPaperSize( DMPAPER_LETTER );					break;

		case pcl6_eLegal:
			m_pParserData->SetPaperSize( DMPAPER_LEGAL );					break;

		case pcl6_eA4:		
			m_pParserData->SetPaperSize( DMPAPER_A4 );						break;

		case pcl6_eExec:		
			m_pParserData->SetPaperSize( DMPAPER_EXECUTIVE );				break;

		case pcl6_eLedger:			
			m_pParserData->SetPaperSize( DMPAPER_LEDGER );					break;

		case pcl6_eA3:				
			m_pParserData->SetPaperSize( DMPAPER_A3 );						break;

		case pcl6_eCOM10Envelope:		
			m_pParserData->SetPaperSize( DMPAPER_ENV_10 );					break;

		case pcl6_eMonarchEnvelope:		
			m_pParserData->SetPaperSize( DMPAPER_ENV_MONARCH );				break;

		case pcl6_eC5Envelope:			
			m_pParserData->SetPaperSize( DMPAPER_ENV_C5 );					break;

		case pcl6_eDLEnvelope:			
			m_pParserData->SetPaperSize( DMPAPER_ENV_DL );					break;

		case pcl6_eB5Envelope:			
			m_pParserData->SetPaperSize( DMPAPER_ENV_B5 );					break;

		case pcl6_eB5Paper:			
			m_pParserData->SetPaperSize( DMPAPER_B5 );						break;

		case pcl6_eJPostcard:			
			m_pParserData->SetPaperSize( DMPAPER_JAPANESE_POSTCARD );		break;

		case pcl6_eA5:					
			m_pParserData->SetPaperSize( DMPAPER_A5 );						break;

		// These two may not be the right DM values
		case pcl6_eJB4:					
			m_pParserData->SetPaperSize( DMPAPER_B4 );						break; 

		case pcl6_eJB5:				
			m_pParserData->SetPaperSize( DMPAPER_B5 );						break;

		// These three required being compiled for Windows 2000 or higher
#if(WINVER >= 0x0500)
		case pcl6_eJDoublePostcard:	
			m_pParserData->SetPaperSize( DMPAPER_DBL_JAPANESE_POSTCARD );	break;

		case pcl6_eA6:				
			m_pParserData->SetPaperSize( DMPAPER_A6 );						break;

		case pcl6_eJB6:					
			m_pParserData->SetPaperSize( DMPAPER_B6_JIS );					break;

#else
		case pcl6_eJDoublePostcard:		
			DEBUG_TEXT( _T("Win2k DEVMODE value: pcl6_eJDoublePostcard") );	break;

		case pcl6_eA6:					
			DEBUG_TEXT( _T("Win2k DEVMODE value: pcl6_eA6") );				break;

		case pcl6_eJB6:					
			DEBUG_TEXT( _T("Win2k DEVMODE value: pcl6_eJB6") );				break;
#endif
		case pcl6_JIS8K:		
			DEBUG_TEXT( _T("Unknown PaperSize: pcl6_JIS8K") );				break;

		case pcl6_JIS16K:		
			DEBUG_TEXT( _T("Unknown PaperSize: pcl6_JIS16K") );				break;

		case pcl6_JISExec:		
			DEBUG_TEXT( _T("Unknown PaperSize: pcl6_JISExec") );			break;

		default:
			DEBUG_TEXT( _T("INVALID MediaSize: %d\n"), pMediaSize->GetData().nByte );
			throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute MediaSize") );
			break;
		}
		break;

	// Array of bytes
	case CPCL6DataType::PCL_DATATYPE_UBYTE_ARRAY:
		{
			// Declare some variables
			LPCSTR		pBuffer		= (LPCSTR)pMediaSize->GetData().pByte;
			DWORD		dwSize		= pMediaSize->GetSize();
			LPSTR		pFormName	= new char[dwSize+1];

			// Create a NULL terminated string
			::ZeroMemory( pFormName, dwSize+1 );
			::CopyMemory( pFormName, pBuffer, dwSize );

			// Set the paper size string
			m_pParserData->SetPaperSizeString( PCL6FormNames, pFormName );

			// Release the buffer
			delete [] pFormName;
		}
		break;

	// Any other
	default:
		DEBUG_TEXT( _T("Invalid DataType for MediaSize: %d\n"), pMediaSize->GetDataType() );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute MediaSize") );
		break;
	}
}

// Sets the orientation based on the parsed value
void CPCL6Parser::ProcessOrientation( const CPCL6Attribute* pOrientation )
{
	// Check assumed values
	_ASSERTE( pOrientation != NULL );

	// Verify it is the needed attribute
	_ASSERTE( pOrientation->GetAttributeID() == PCL_ATTRIB_ORIENTATION );

	// Ensure the data type is what is expected
	if ( pOrientation->GetDataType() != CPCL6DataType::PCL_DATATYPE_UBYTE )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute Orientation") );
	}

	// Process the value
	pcl6_Orientation eO = (pcl6_Orientation)pOrientation->GetData().nByte;

	switch( eO )
	{
	case pcl6_eDefaultOrientation:		// Added in 2.1
	case pcl6_ePortraitOrientation:
	case pcl6_eReversePortrait:
		m_pParserData->SetOrientation( DMORIENT_PORTRAIT );
		break;

	case pcl6_eLandscapeOrientation:
	case pcl6_eReverseLandscape:
		m_pParserData->SetOrientation( DMORIENT_LANDSCAPE );
		break;

	default:
		DEBUG_TEXT( _T("INVALID Orientation!\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute Orientation") );
		break;
	}
}

// Sets the printer resolution information
void CPCL6Parser::ProcessResolution( const CPCL6Attribute* pSize, const CPCL6Attribute* pUnits )
{
	// Check assumed values
	_ASSERTE( pSize != NULL );
	_ASSERTE( pUnits != NULL );

	// Verify that the attributes are the ones needed
	_ASSERTE( pSize->GetAttributeID() == PCL_ATTRIB_UNITSPERMEASURE );
	_ASSERTE( pUnits->GetAttributeID() == PCL_ATTRIB_MEASURE );

	float	nXResolution;
	float	nYResolution;

	// Get the XY values
	switch( pSize->GetDataType() )
	{
	case CPCL6DataType::PCL_DATATYPE_UINT16_XY:
		nXResolution	= (float)(pSize->GetData().nUInt16XY[CPCL6DataType::PCL_XY_X_VALUE ]);
		nYResolution	= (float)(pSize->GetData().nUInt16XY[CPCL6DataType::PCL_XY_Y_VALUE ]);
		break;

	case CPCL6DataType::PCL_DATATYPE_REAL32_XY:
		nXResolution	= pSize->GetData().nReal32XY[ CPCL6DataType::PCL_XY_X_VALUE ];
		nYResolution	= pSize->GetData().nReal32XY[ CPCL6DataType::PCL_XY_Y_VALUE ];
		break;

	default:
		DEBUG_TEXT( _T("Invalid datatype for UnitsPerMeasure\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute UnitsPerMeasure") );
		break;
	}

	// Ensure the data type is what is expected
	if ( pUnits->GetDataType() != CPCL6DataType::PCL_DATATYPE_UBYTE )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid datatype for attribute Measure") );
	}

	// Get the units and convert to inches (which is what DEVMODE wants)
	switch( pUnits->GetData().nByte )
	{
	case pcl6_eInch:
		m_pParserData->SetPrintQuality( InchesToInches(nXResolution) );
		m_pParserData->SetYResolution( InchesToInches(nYResolution) );
		break;
	case pcl6_eMillimeter:
		m_pParserData->SetPrintQuality( MillimetersToInches(nXResolution) );
		m_pParserData->SetYResolution( MillimetersToInches(nYResolution) );
		break;
	case pcl6_eTenthsOfAMillimeter:
		m_pParserData->SetPrintQuality( TenthsOfAMillimeterToInches(nXResolution) );
		m_pParserData->SetYResolution( TenthsOfAMillimeterToInches(nYResolution) );
		break;
	default:
		DEBUG_TEXT( _T("Invalid value for Measure!\n") );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid value for attribute Measure") );
		break;
	}
}

// Handles Embedded Data
void CPCL6Parser::ProcessEmbeddedData()
{
#ifdef _DEBUG
	CPCL6Attribute*	pAttribute	= NULL;

	pAttribute = m_AttributeList.FindAttribute( PCL_ATTRIB_BLOCKBYTELENGTH );
	if ( pAttribute != NULL )
	{
		DEBUG_TEXT( _T("BlockByteLength = %d\n"), pAttribute->GetData().dwUInt32 );

		// Just to observe this behavior once
		_ASSERTE( false && _T("Got BlockByteLength PCL6 Attribute!") );
	}
#endif

	switch( m_pParserData->ReadByte() )
	{
	case PCL_EMBEDDED_DATA_BYTE:
		m_pParserData->SkipAhead( m_pParserData->ReadByte() );
		break;
	case PCL_EMBEDDED_DATA:
		m_pParserData->SkipAhead( m_pParserData->ReadUInt32() );
		break;
	default:
		m_pParserData->BackUp( sizeof(BYTE) );
		DEBUG_TEXT( _T("No embedded data!\n") );
		break;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline bool CPCL6Parser::HandleOperator()
{
	pcl6_Operator	nOp = static_cast<pcl6_Operator>( m_pParserData->ReadByte() );

	DEBUG_PCL6_TEXT( _T("Attribute List for Operator 0x%02x...\n"), nOp );

	switch ( nOp )
	{
	case pcl6_opArcPath:
		OperatorArcPath();				DEBUG_PCL6_TEXT( _T("\tArcPath\n") );				break;

	case pcl6_opBeginChar:
		OperatorBeginChar();			DEBUG_PCL6_TEXT( _T("\tBeginChar\n") );				break;

	case pcl6_opBeginFontHeader:
		OperatorBeginFontHeader();		DEBUG_PCL6_TEXT( _T("\tBeginFontHeader\n") );		break;

	case pcl6_opBeginImage:
		OperatorBeginImage();			DEBUG_PCL6_TEXT( _T("\tBeginImage\n") );			break;

	case pcl6_opBeginPage:
		OperatorBeginPage();			DEBUG_PCL6_TEXT( _T("\tBeginPage\n") );				break;

	case pcl6_opBeginRastPattern:
		OperatorBeginRastPattern();		DEBUG_PCL6_TEXT( _T("\tBeginRastPattern\n") );		break;

	case pcl6_opBeginScan:
		OperatorBeginScan();			DEBUG_PCL6_TEXT( _T("\tBeginScan\n") );				break;

	case pcl6_opBeginSession:
		OperatorBeginSession();			DEBUG_PCL6_TEXT( _T("\tBeginSession\n") );			break;

	case pcl6_opBeginStream:
		OperatorBeginStream();			DEBUG_PCL6_TEXT( _T("\tBeginStream\n") );			break;

	case pcl6_opBeginUserDefinedLineCaps:		// Added in 2.1
		OperatorBeginUserDefinedLineCaps();
		DEBUG_PCL6_TEXT( _T("\tBeginUserDefinedLineCaps\n") );
		break;

	case pcl6_opBezierPath:
		OperatorBezierPath();			DEBUG_PCL6_TEXT( _T("\tBezierPath\n") );			break;
	
	case pcl6_opBezierRelPath:
		OperatorBezierRelPath();		DEBUG_PCL6_TEXT( _T("\tBezierRelPath\n") );			break;

	case pcl6_opChord:
		OperatorChord();				DEBUG_PCL6_TEXT( _T("\tChord\n") );					break;

	case pcl6_opChordPath:
		OperatorChordPath();			DEBUG_PCL6_TEXT( _T("\tChordPath\n") );				break;

	case pcl6_opCloseDataSource:
		OperatorCloseDataSource();		DEBUG_PCL6_TEXT( _T("\tCloseDataSource\n") );		break;

	case pcl6_opCloseSubPath:
		OperatorCloseSubPath();			DEBUG_PCL6_TEXT( _T("\tCloseSubPath\n") );			break;

	case pcl6_opComment:
		OperatorComment();				DEBUG_PCL6_TEXT( _T("\tComment\n") );				break;

	case pcl6_opEllipse:
		OperatorEllipse();				DEBUG_PCL6_TEXT( _T("\tEllipse\n") );				break;

	case pcl6_opEllipsePath:
		OperatorEllipsePath();			DEBUG_PCL6_TEXT( _T("\tEllipsePath\n") );			break;

	case pcl6_opEndChar:
		OperatorEndChar();				DEBUG_PCL6_TEXT( _T("\tEndChar\n") );				break;

	case pcl6_opEndFontHeader:
		OperatorEndFontHeader();		DEBUG_PCL6_TEXT( _T("\tEndFontHeader\n") );			break;
	
	case pcl6_opEndImage:
		OperatorEndImage();				DEBUG_PCL6_TEXT( _T("\tEndImage\n") );				break;

	case pcl6_opEndPage:
		OperatorEndPage();				DEBUG_PCL6_TEXT( _T("\tEndPage\n") );				break;

	case pcl6_opEndRastPattern:
		OperatorEndRastPattern();		DEBUG_PCL6_TEXT( _T("\tEndRastPattern\n") );		break;

	case pcl6_opEndScan:
		OperatorEndScan();				DEBUG_PCL6_TEXT( _T("\tEndScan\n") );				break;

	case pcl6_opEndSession:
		OperatorEndSession();			DEBUG_PCL6_TEXT( _T("\tEndSession\n") );			break;

	case pcl6_opEndStream:
		OperatorEndStream();			DEBUG_PCL6_TEXT( _T("\tEndStream\n") );				break;

	case pcl6_opEndUserDefinedLineCaps:		// Added in 2.1
		OperatorEndUserDefinedLineCaps();
		DEBUG_PCL6_TEXT( _T("\tEndUserDefinedLineCaps\n") );
		break;

	case pcl6_opExecStream:
		OperatorExecStream();			DEBUG_PCL6_TEXT( _T("\tExecStream\n") );			break;

	case pcl6_opLinePath:
		OperatorLinePath();				DEBUG_PCL6_TEXT( _T("\tLinePath\n") );				break;

	case pcl6_opLineRelPath:
		OperatorLineRelPath();			DEBUG_PCL6_TEXT( _T("\tLineRelPath\n") );			break;

	case pcl6_opNewPath:
		OperatorNewPath();				DEBUG_PCL6_TEXT( _T("\tNewPath\n") );				break;

	case pcl6_opOpenDataSource:
		OperatorOpenDataSource();		DEBUG_PCL6_TEXT( _T("\tOpenDataSource\n") );		break;

	case pcl6_opPaintPath:
		OperatorPaintPath();			DEBUG_PCL6_TEXT( _T("\tPaintPath\n") );				break;

	case pcl6_opPie:
		OperatorPie();					DEBUG_PCL6_TEXT( _T("\tPie\n") );					break;

	case pcl6_opPiePath:
		OperatorPiePath();				DEBUG_PCL6_TEXT( _T("\tPiePath\n") );				break;

	case pcl6_opPopGS:
		OperatorPopGS();				DEBUG_PCL6_TEXT( _T("\tPopGS\n") );					break;

	case pcl6_opPushGS:	
		OperatorPushGS();				DEBUG_PCL6_TEXT( _T("\tPushGS\n") );				break;

	case pcl6_opReadChar:	
		OperatorReadChar();				DEBUG_PCL6_TEXT( _T("\tReadChar\n") );				break;

	case pcl6_opReadFontHeader:		
		OperatorReadFontHeader();		DEBUG_PCL6_TEXT( _T("\tReadFontHeader\n") );		break;

	case pcl6_opReadImage:			
		OperatorReadImage();			DEBUG_PCL6_TEXT( _T("\tReadImage\n") );				break;

	case pcl6_opReadRastPattern:	
		OperatorReadRastPattern();		DEBUG_PCL6_TEXT( _T("\tReadRastPattern\n") );		break;

	case pcl6_opReadStream:			
		OperatorReadStream();			DEBUG_PCL6_TEXT( _T("\tReadStream\n") );			break;

	case pcl6_opRectangle:			
		OperatorRectangle();			DEBUG_PCL6_TEXT( _T("\tRectangle\n") );				break;

	case pcl6_opRectanglePath:			
		OperatorRectanglePath();		DEBUG_PCL6_TEXT( _T("\tRectanglePath\n") );			break;

	case pcl6_opRemoveFont:			
		OperatorRemoveFont();			DEBUG_PCL6_TEXT( _T("\tRemoveFont\n") );			break;

	case pcl6_opRemoveStream:		
		OperatorRemoveStream();			DEBUG_PCL6_TEXT( _T("\tRemoveStream\n") );			break;

	case pcl6_opRoundRectangle:		
		OperatorRoundRectangle();		DEBUG_PCL6_TEXT( _T("\tRoundRectangle\n") );		break;

	case pcl6_opRoundRectanglePath:
		OperatorRoundRectanglePath();	DEBUG_PCL6_TEXT( _T("\tRoundRectanglePath\n") );	break;

	case pcl6_opScanLineRel:		
		OperatorScanLineRel();			DEBUG_PCL6_TEXT( _T("\tScanLineRel\n") );			break;

	case pcl6_opSetClipReplace:		
		OperatorSetClipReplace();		DEBUG_PCL6_TEXT( _T("\tSetClipReplace\n") );		break;

	case pcl6_opSetBrushSource:		
		OperatorSetBrushSource();		DEBUG_PCL6_TEXT( _T("\tSetBrushSource\n") );		break;

	case pcl6_opSetCharAngle:		
		OperatorSetCharAngle();			DEBUG_PCL6_TEXT( _T("\tSetCharAngle\n") );			break;

	case pcl6_opSetCharAttributes:	
		OperatorSetCharAttributes();	DEBUG_PCL6_TEXT( _T("\tSetCharAttributes\n") );		break;

	case pcl6_opSetCharBoldValue:		
		OperatorSetCharBoldValue();		DEBUG_PCL6_TEXT( _T("\tSetCharBoldValue\n") );		break;

	case pcl6_opSetCharScale:			
		OperatorSetCharScale();			DEBUG_PCL6_TEXT( _T("\tSetCharScale\n") );			break;

	case pcl6_opSetCharShear:
		OperatorSetCharShear();			DEBUG_PCL6_TEXT( _T("\tSetCharShear\n") );			break;

	case pcl6_opSetCharSubMode:			
		OperatorSetCharSubMode();		DEBUG_PCL6_TEXT( _T("\tSetCharSubMode\n") );		break;

	case pcl6_opSetClipIntersect:		
		OperatorSetClipIntersect();		DEBUG_PCL6_TEXT( _T("\tSetClipIntersect\n") );		break;

	case pcl6_opSetClipMode:			
		OperatorSetClipMode();			DEBUG_PCL6_TEXT( _T("\tSetClipMode\n") );			break;

	case pcl6_opSetClipRectangle:		
		OperatorSetClipRectangle();		DEBUG_PCL6_TEXT( _T("\tSetClipRectangle\n") );		break;

	case pcl6_opSetClipToPage:			
		OperatorSetClipToPage();		DEBUG_PCL6_TEXT( _T("\tSetClipToPage\n") );			break;

	case pcl6_opSetColorSpace:		
		OperatorSetColorSpace();		DEBUG_PCL6_TEXT( _T("\tSetColorSpace\n") );			break;

	case pcl6_opSetColorTreatment:		// Added in 2.1
		OperatorSetColorTreatment();	DEBUG_PCL6_TEXT( _T("\tSetColorTreatment\n") );		break;

	case pcl6_opSetCursor:			
		OperatorSetCursor();			DEBUG_PCL6_TEXT( _T("\tSetCursor\n") );				break;

	case pcl6_opSetCursorRel:		
		OperatorSetCursorRel();			DEBUG_PCL6_TEXT( _T("\tSetCursorRel\n") );			break;

	case pcl6_opSetDefaultGS:			// Added in 2.1
		OperatorSetDefaultGS();			DEBUG_PCL6_TEXT( _T("\tSetDefaultGS\n") );			break;

	case pcl6_opSetHalftoneMethod:	
		OperatorSetHalftoneMethod();	DEBUG_PCL6_TEXT( _T("\tSetHalftoneMethod\n") );		break;

	case pcl6_opSetFillMode:			
		OperatorSetFillMode();			DEBUG_PCL6_TEXT( _T("\tSetFillMode\n") );			break;

	case pcl6_opSetFont:			
		OperatorSetFont();				DEBUG_PCL6_TEXT( _T("\tSetFont\n") );				break;

	case pcl6_opSetLineCap:			
		OperatorSetLineCap();			DEBUG_PCL6_TEXT( _T("\tSetLineCap\n") );			break;

	case pcl6_opSetLineDash:		
		OperatorSetLineDash();			DEBUG_PCL6_TEXT( _T("\tSetLineDash\n") );			break;

	case pcl6_opSetLineJoin:		
		OperatorSetLineJoin();			DEBUG_PCL6_TEXT( _T("\tSetLineJoin\n") );			break;

	case pcl6_opSetMiterLimit:		
		OperatorSetMiterLimit();		DEBUG_PCL6_TEXT( _T("\tSetMiterLimit\n") );			break;

	case pcl6_opSetPageDefaultCTM:	
		OperatorSetPageDefaultCTM();	DEBUG_PCL6_TEXT( _T("\tSetPageDefaultCTM\n") );		break;

	case pcl6_opSetPageOrigin:		
		OperatorSetPageOrigin();		DEBUG_PCL6_TEXT( _T("\tSetPageOrigin\n") );			break;

	case pcl6_opSetPageRotation:	
		OperatorSetPageRotation();		DEBUG_PCL6_TEXT( _T("\tSetPageRotation\n") );		break;

	case pcl6_opSetPageScale:		
		OperatorSetPageScale();			DEBUG_PCL6_TEXT( _T("\tSetPageScale\n") );			break;

	case pcl6_opSetPathToClip:		
		OperatorSetPathToClip();		DEBUG_PCL6_TEXT( _T("\tSetPathToClip\n") );			break;

	case pcl6_opSetPatternTxMode:	
		OperatorSetPatternTxMode();		DEBUG_PCL6_TEXT( _T("\tSetPatternTxMode\n") );		break;

	case pcl6_opSetPenSource:		
		OperatorSetPenSource();			DEBUG_PCL6_TEXT( _T("\tSetPenSource\n") );			break;

	case pcl6_opSetPenWidth:		
		OperatorSetPenWidth();			DEBUG_PCL6_TEXT( _T("\tSetPenWidth\n") );			break;

	case pcl6_opSetROP:				
		OperatorSetROP();				DEBUG_PCL6_TEXT( _T("\tSetROP\n") );				break;

	case pcl6_opSetSourceTxMode:	
		OperatorSetSourceTxMode();		DEBUG_PCL6_TEXT( _T("\tSetSourceTxMode\n") );		break;

	case pcl6_opText:				
		OperatorText();					DEBUG_PCL6_TEXT( _T("\tText\n") );					break;

	case pcl6_opTextPath:			
		OperatorTextPath();				DEBUG_PCL6_TEXT( _T("\tTextPath\n") );				break;

	// RFU Values (Should be recognized as operators)
	case pcl6_opRFU1:
	case pcl6_opRFU2:
	case pcl6_opRFU3:
	case pcl6_opRFU4:
	case pcl6_opRFU5:
	case pcl6_opRFU6:
	case pcl6_opRFU7:

/*
	// These were removed in 2.1
	case pcl6_opRFU8:
	case pcl6_opRFU9:
	case pcl6_opRFU10:
	case pcl6_opRFU11:
	case pcl6_opRFU12:
	case pcl6_opRFU13:
	case pcl6_opRFU14:
	case pcl6_opRFU15:
	case pcl6_opRFU16:
	case pcl6_opRFU17:
	case pcl6_opRFU18:
	case pcl6_opRFU19:
	case pcl6_opRFU20:
	case pcl6_opRFU21:
*/
	case pcl6_opRFU22:
	case pcl6_opRFU23:
	case pcl6_opRFU24:
	case pcl6_opRFU25:
	case pcl6_opRFU26:
	case pcl6_opRFU27:
	case pcl6_opRFU28:
	case pcl6_opRFU29:
	case pcl6_opRFU30:
	case pcl6_opRFU31:
	case pcl6_opRFU32:
	case pcl6_opRFU33:
	case pcl6_opRFU34:
	case pcl6_opRFU35:
	case pcl6_opRFU36:
	case pcl6_opRFU37:
		OperatorRFU();
		DEBUG_PCL6_TEXT( _T("\tRFU Operator 0x%02x\n"), nOp );
		break;
	
	default:
		DEBUG_TEXT( _T("INVALID Operator: 0x%02x\n"), nOp );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid Operator") );
		break;
	}

	DEBUG_PCL6_DUMP( m_AttributeList );

	m_AttributeList.Clear();
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL6Parser.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////