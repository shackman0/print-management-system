///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintParser.cpp: implementation of the CPrintParser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PrintParser.h"
#include "PCL6Parser.h"
#include "PCL5Parser.h"
#include "PSParser.h"
#include "PJLParser.h"
#include "EMFParser.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define TRY_PARSERS_UNTIL_SUCCESS(parserclass,datatype)						\
	{																		\
		parserclass theParser;												\
		if ( theParser.Parse(pParserData) )									\
		{																	\
			DEBUG_PARSER( _T("Parsed Using: ") _T(#parserclass) _T("\n") );	\
			pParserData->SetDataType((datatype));							\
			return true;													\
		}																	\
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Parse Operation
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPrintParser::Parse( CPrintParserData* pParserData )
{
	// Check basic assumptions
	_ASSERTE( pParserData != NULL );

	// Declare variables
	LPBYTE	pBuffer		= pParserData->GetBuffer();
	DWORD	dwLength	= pParserData->GetBufferLength();
	DWORD	dwParsed	= 0;
	bool	bReturnMe	= true;

#ifdef USE_PRINTPARSER_DEBUG
	DWORD dwCounter		= 0;
#endif

	while ( (dwLength > dwParsed) && (bReturnMe == true) )
	{
		pParserData->SetBuffer( pBuffer + dwParsed, (dwLength - dwParsed) );

		dwParsed += RunParser( pParserData, bReturnMe );

#ifdef USE_PRINTPARSER_DEBUG
		DEBUG_PARSER( _T("Job #%d: Pages: %d, Copies: %d\n"),
			dwCounter++, pParserData->GetPageCount(), pParserData->GetCopies() );
#endif
	}

	return bReturnMe;
}

DWORD CPrintParser::RunParser( CPrintParserData* pParserData, bool& bReturnMe )
{
	// Check basic assumptions
	_ASSERTE( pParserData != NULL );

	// Declare variables
	LPSTR	pBuffer		= (LPSTR)pParserData->GetBuffer();
	DWORD	dwLength	= pParserData->GetBufferLength();
	DWORD	dwCurrent	= 0;
	LPSTR	pCurrent	= (LPSTR)pBuffer;
	DWORD	dwParsed	= 0;

	// Check to see if there is any PJL by searching for the UEL
	pCurrent = (LPSTR)CPrintJobBuffer::FindPattern( (LPBYTE)pCurrent, dwLength, 
		(LPBYTE)PJL_UEL, CONST_LENGTH(PJL_UEL) );

	if ( pCurrent != NULL )
	{
		// There is PJL
		LPSTR	pEndPJL		= NULL;
		LPSTR	pBeginJob	= NULL;
		LPSTR	pEndJob		= NULL;

		// Find the "Enter Job" command
		pBeginJob	= strstr( pCurrent, PJL_COMMAND_ENTER );

		if ( pBeginJob != NULL )
		{
			// The job data starts after this command
			pBeginJob	= strstr( pBeginJob, PJL_TERMINATOR ) + CONST_LENGTH(PJL_TERMINATOR);

			// If there is an extra terminator character, skip over it
			if ( *pBeginJob == PJL_TERMINATOR_2 )
			{
				pBeginJob++;
			}
		}
		else
		{
			LPSTR	pTemp	= pCurrent;

			// There is no "Enter Job" command, so try to find the end of the PJL and use that
			do
			{
				pTemp = strstr( pTemp, PJL_TERMINATOR );
				if ( pTemp != NULL )
				{
					pTemp += CONST_LENGTH(PJL_TERMINATOR);

					if ( strstr(pTemp, PJL_TAG) != pTemp )
					{
						if ( CONST_LENGTH(PJL_UEL) <= (dwLength - (pTemp - pBuffer)) )
						{
							pBeginJob	= pTemp;
						}
						else
						{
							// At the end of the file
							pEndJob		= pCurrent;
							pBeginJob	= pBuffer;
						}
					}
				}
				else
				{
					pEndJob		= pCurrent;
					pBeginJob	= pBuffer;
				}
			}
			while ( pBeginJob == NULL );
		}

		// Mark the end of the initial PJL
		pEndPJL		= pBeginJob;

		if ( pEndJob == NULL )
		{
			// The job data ends just before another UEL
			pEndJob		= (LPSTR)CPrintJobBuffer::FindPattern( (LPBYTE)pBeginJob, 
				(dwLength - (pBeginJob - pBuffer)), (LPBYTE)PJL_UEL, CONST_LENGTH(PJL_UEL) );
		}

		// If there is no other UEL, just use the end of the buffer
		if ( pEndJob == NULL )
		{
			DEBUG_PARSER( _T("No UEL at the end of the PJL!\n") );
			pEndJob = (LPSTR)(pBuffer + dwLength);
		}

		if ( pBeginJob != pEndJob )
		{
			// Update the parser data with the new buffer information
			pParserData->SetBuffer( (BYTE*)pBeginJob, (pEndJob - pBeginJob) );

			// Track how many bytes were parsed
			dwParsed	= (pEndJob - pBuffer);

			// Running through the various parsers
			bReturnMe	= ParseJob( pParserData );
			
			// Update the parser data to point to the PJL at the beginning of the job
			pParserData->SetBuffer( (LPBYTE)pBuffer, (pEndPJL - pBuffer) );

			// Parse the PJL
			ParsePJL( pParserData );
		}
		else
		{
			dwParsed	= dwLength;
		}
	}
	else
	{
		// There is no PJL
		DEBUG_PARSER( _T("There is no PJL!\n") );
		bReturnMe	= ParseJob( pParserData );
		dwParsed	= dwLength;
	}

	return dwParsed;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Utility Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Parses the PJL
bool CPrintParser::ParsePJL( CPrintParserData* pParserData )
{
	CPJLParser		pjlParser;

	return pjlParser.Parse( pParserData );
}

// Try all of the individual parsers
bool CPrintParser::ParseJob( CPrintParserData* pParserData )
{
	TRY_PARSERS_UNTIL_SUCCESS( CPCL6Parser,	_T("PCL6")			);
	TRY_PARSERS_UNTIL_SUCCESS( CPCL5Parser, _T("PCL5")			);
	TRY_PARSERS_UNTIL_SUCCESS( CEMFParser,	_T("EMF")			);
	TRY_PARSERS_UNTIL_SUCCESS( CPSParser,	_T("PostScript")	);

	// If this point is reached, none of the parsers were successful
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintParser.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
