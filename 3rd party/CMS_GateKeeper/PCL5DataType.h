///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL5DataType.h: interface for the CPCL5DataType class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL5DATATYPE_H__FFD1B45C_EC98_11D3_A431_00105A1C588D__INCLUDED_)
#define AFX_PCL5DATATYPE_H__FFD1B45C_EC98_11D3_A431_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintJobBuffer.h"
#include "PrintParserBase.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define HASH_PCL5(x,y,z)				( ((x) << 16) + ((y) << 8) + (z) )

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL5DataType Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPCL5DataType  
{
// Data Structure
private:
	typedef struct
	{
		char	chrCommand;
		char	chrGroup;
		long	nValue;
		char	chrTerminator;
	} PCL5Data;

// Construction / Destruction
public:
	CPCL5DataType();
	~CPCL5DataType();

// Data Access
public:
	const long&			GetValue() const;
	const PCL5Data&		GetData() const;
	const bool&			DoContinue() const;
	const DWORD			GetHashValue() const;

// Operations
public:
	void				Clear();
	bool				IsParameterizedCommand() const;
	bool				IsTwoCharCommand() const;
	bool				ReadFromBuffer( CPrintJobBuffer* pBuffer );
	
// Comparison Functions
public:
	bool				DoesHeaderMatch( const char& chrCommand, const char& chrGroup ) const;
	bool				DoesTerminatorMatch( const char& chrTerm ) const;

// Static Operations
private:
	static bool			IsNumeric( const char& chrNumeric );
	static char			GetTerminatorValue( const char& chrParam );
	// Test the validity of different pieces of a PCL5 Command
	static bool			IsTwoCharCommand( const char& chrChar );
	static bool			IsParameterizedCommand( const char& chrChar );
	static bool			IsGroupValue( const char& chrChar );
	static bool			IsParameterValue( const char& chrChar );
	static bool			IsTerminatorValue( const char& chrChar );

// Attributes
private:
	PCL5Data	m_Data;
	bool		m_bContinue;

// Enumerated Type
public:
	enum
	{
		// These values are found in the PCL5 documentation
		PCL5_ESCAPE						= 0x1b,				// ASCII char: <ESC>

		// PCL5 Command Type #1 : Two Character Sequences
		//		Have the form of <ESC>X
		//		Where X is an ASCII value between 48-126
		PCL5_TWOCHAR_MIN				= ((char)(48)),		// ASCII char: 0
		PCL5_TWOCHAR_MAX				= ((char)(126)),	// ASCII char: ~

		// PCL5 Command Type #2 : Parameterized Sequences
		//		Have the form of <ESC>X y z1 # z2 # z3 ... # Zn [Data]
		//		Where:
		//			X		= Parameterized Character (33 - 47 ASCII)
		//			y		= Group Character (96 - 126 ASCII)
		//			#		= Numeric Value
		//			zi		= Parameter character (specifies to what the # applies) (96 - 126 ASCII)
		//			Zn		= Termination character (pecifies to what the # applies) (64 - 94 ASCII)
		//			[Data]	= Binary Data
		//
		//		Note: That zi and Zn are corresponding sets, just offset by 32.  Zn means the end
		//		of the command.  zi means that there is more.
		//
		//		Note: y, #, zi are optional depending on the command
		PCL5_PARAMETERIZED_MIN			= ((char)(33)),		// ASCII char: !
		PCL5_PARAMETERIZED_MAX			= ((char)(47)),		// ASCII char: /
		PCL5_GROUP_MIN					= ((char)(96)),		// ASCII char: '
		PCL5_GROUP_MAX					= ((char)(126)),	// ASCII char: ~
		PCL5_PARAMETER_MIN				= ((char)(96)),		// ASCII char: '
		PCL5_PARAMETER_MAX				= ((char)(126)),	// ASCII char: ~
		PCL5_TERMINATOR_MIN				= ((char)(64)),		// ASCII char: @
		PCL5_TERMINATOR_MAX				= ((char)(94))		// ASCII char: ^
	};
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL5DataType Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPCL5DataType::CPCL5DataType()
	{
		Clear();
	}

	inline CPCL5DataType::~CPCL5DataType()
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline const long& CPCL5DataType::GetValue() const
	{
		return m_Data.nValue;
	}

	inline const CPCL5DataType::PCL5Data& CPCL5DataType::GetData() const
	{
		return m_Data;
	}

	inline const bool& CPCL5DataType::DoContinue() const
	{
		return m_bContinue;
	}

	inline const DWORD CPCL5DataType::GetHashValue() const
	{
		return HASH_PCL5( m_Data.chrCommand, m_Data.chrGroup, m_Data.chrTerminator );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CPCL5DataType::Clear()
	{
		m_bContinue = false;
		::ZeroMemory( &m_Data, sizeof(m_Data) );
	}

	inline bool CPCL5DataType::IsParameterizedCommand() const
	{
		return IsParameterizedCommand( m_Data.chrCommand );
	}

	inline bool CPCL5DataType::IsTwoCharCommand() const
	{
		return IsTwoCharCommand( m_Data.chrCommand );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Comparison Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CPCL5DataType::DoesHeaderMatch( const char& chrCommand, const char& chrGroup ) const
	{
		return ( (chrCommand == m_Data.chrCommand) && (chrGroup == m_Data.chrGroup) );
	}
	
	inline bool CPCL5DataType::DoesTerminatorMatch( const char& chrTerm ) const
	{
		return ( chrTerm == m_Data.chrTerminator );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CPCL5DataType::IsNumeric( const char& chrNumeric )
	{
		// Return true if the character is from 0-9, a decimal point, or a positive/negative sign
		return ( isdigit(chrNumeric) || (chrNumeric == '-') || 
				(chrNumeric == '+') || (chrNumeric == '.') );
	}

	inline char CPCL5DataType::GetTerminatorValue( const char& chrParam )
	{
		_ASSERTE( IsParameterValue(chrParam) );
		return chrParam - (PCL5_PARAMETER_MAX - PCL5_TERMINATOR_MAX);
	}

	// Test the validity of different pieces of a PCL5 Command
	inline bool CPCL5DataType::IsTwoCharCommand( const char& chrChar )
	{
		return IsBetween( chrChar, static_cast<char>(PCL5_TWOCHAR_MIN),
			static_cast<char>(PCL5_TWOCHAR_MAX) );
	}

	inline bool CPCL5DataType::IsParameterizedCommand( const char& chrChar )
	{
		return IsBetween( chrChar, static_cast<char>(PCL5_PARAMETERIZED_MIN), 
			static_cast<char>(PCL5_PARAMETERIZED_MAX) );
	}

	inline bool CPCL5DataType::IsGroupValue( const char& chrChar )
	{
		return IsBetween( chrChar, static_cast<char>(PCL5_GROUP_MIN), 
			static_cast<char>(PCL5_GROUP_MAX) );
	}

	inline bool CPCL5DataType::IsParameterValue( const char& chrChar )
	{
		return IsBetween( chrChar, static_cast<char>(PCL5_PARAMETER_MIN), 
			static_cast<char>(PCL5_PARAMETER_MAX) );
	}

	inline bool CPCL5DataType::IsTerminatorValue( const char& chrChar )
	{
		return IsBetween( chrChar, static_cast<char>(PCL5_TERMINATOR_MIN),
			static_cast<char>(PCL5_TERMINATOR_MAX) );
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PCL5DATATYPE_H__FFD1B45C_EC98_11D3_A431_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL5DataType.h
///////////////////////////////////////////////////////////////////////////////////////////////////////