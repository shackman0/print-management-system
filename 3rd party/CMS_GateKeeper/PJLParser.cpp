///////////////////////////////////////////////////////////////////////////////////////////////////////
// PJLParser.cpp: implementation of the CPJLParser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PJLParser.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Known paper size matchings
///////////////////////////////////////////////////////////////////////////////////////////////////////

// These should be listed in order of frequency of use
const PaperSizeInfo CPJLParser::PJLFormNames[] =
{
	{ "LETTER",		DMPAPER_LETTER					},
	{ "LEGAL",		DMPAPER_LEGAL					},
	{ "A4",			DMPAPER_A4						},
	{ "LEDGER",		DMPAPER_LEDGER					},
	{ "A3",			DMPAPER_A3						},
	{ "JISB4",		DMPAPER_B4						},
	{ "JISB5",		DMPAPER_B5						},
	{ "EXECUTIVE",	DMPAPER_EXECUTIVE				},
	{ "COM10",		DMPAPER_ENV_10					},
	{ "MONARCH",	DMPAPER_ENV_MONARCH				},
	{ "DL",			DMPAPER_ENV_DL					},
	{ "C5",			DMPAPER_ENV_C5					},
	{ "B5",			DMPAPER_B5						},
	{ "JPOST",		DMPAPER_JAPANESE_POSTCARD		},
	{ "A5",			DMPAPER_A5						},
#if(WINVER >= 0x0500)
	{ "JPOSTD",		DMPAPER_DBL_JAPANESE_POSTCARD	},
#endif
	// For future expansion

	//{ "JISEXEC",					},
	//{ "",			DMPAPER_ENV_B5				},
	//{ "",			DMPAPER_CSHEET				},
	//{ "",			DMPAPER_DSHEET				},
	//{ "",			DMPAPER_ESHEET				},
	//{ "",			DMPAPER_LETTERSMALL			},
	//{ "",			DMPAPER_TABLOID				},
	//{ "",			DMPAPER_STATEMENT			},
	//{ "",			DMPAPER_A4SMALL				},
	//{ "",			DMPAPER_FOLIO				},
	//{ "",			DMPAPER_10X14				},
	//{ "",			DMPAPER_11X17				},
	//{ "",			DMPAPER_NOTE				},
	//{ "",			DMPAPER_ENV_9				},
	//{ "",			DMPAPER_ENV_11				},
	//{ "",			DMPAPER_ENV_12				},
	//{ "",			DMPAPER_ENV_14				},
	//{ "",			DMPAPER_ENV_C65				},
	//{ "",			DMPAPER_ENV_C6				},
	//{ "",			DMPAPER_ENV_B4				},
	//{ "",			DMPAPER_ENV_B6				},
	//{ "",			DMPAPER_ENV_ITALY			},
	//{ "",			DMPAPER_TABLOID				},
	//{ "",			DMPAPER_ENV_PERSONAL		},
	//{ "",			DMPAPER_FANFOLD_US			},
	//{ "",			DMPAPER_FANFOLD_STD_GERMAN	},
	//{ "",			DMPAPER_FANFOLD_LGL_GERMAN	},
	//{ "",			DMPAPER_STATEMENT			},
	{ NULL,			0								}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Parsing Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPJLParser::Parse( CPrintParserData* pParserData )
{
	// Check basic assumptions
	_ASSERTE( pParserData != NULL );

	// Assign the member variables
	m_pParserData = pParserData;

	// Declare the variables
	LPCSTR	lpEndOfCommand	= NULL;
	DWORD	dwSizeOfCommand	= 0;

	// Parse each item until the end is reached
	while ( m_pParserData->GetBytesRemaining() )
	{
		// Break it into PJL commands
		lpEndOfCommand	= strstr( (LPCSTR)m_pParserData->GetCurrentBuffer(), PJL_TERMINATOR );
		dwSizeOfCommand	= (lpEndOfCommand - (LPCSTR)m_pParserData->GetCurrentBuffer());

		try
		{
			// Process each PJL command
			if ( CHECK_STRING( (LPCSTR)m_pParserData->GetCurrentBuffer(), PJL_COMMAND_SET ) )
			{
				ProcessSetCommand( (LPCSTR)(m_pParserData->GetCurrentBuffer() + 
					CONST_LENGTH(PJL_COMMAND_SET)) );
			}
		}
		catch( CPrintParserException& e )
		{
#ifdef USE_DEBUG_OUTPUT
			DEBUG_TEXT( _T("PJL Parser Caught Exception: %s\n"), e.GetErrorMessage() );
#endif
			m_pParserData->SetErrorInfo( e.GetErrorMessage() );
		}

		// Move on to the next command
		m_pParserData->SkipAhead( dwSizeOfCommand + CONST_LENGTH(PJL_TERMINATOR) );
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Command Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CPJLParser::ProcessSetCommand( LPCSTR lpCommand )
{
	// Skip past any white space
	while ( isspace(*lpCommand) )
	{
		lpCommand++;
	}

	// Test each property to see if it has desired information.
	// If it does, process it and then quit.

	// Handle Duplex
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_DUPLEX) )
	{
		m_pParserData->SetDuplex( (ReadBooleanValue(lpCommand) ? DMDUP_VERTICAL : DMDUP_SIMPLEX) );
		return;
	}

	// Handle Number of Copies
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_QUANTITY) || 
			CHECK_STRING(lpCommand, PJL_PROPERTY_COPIES) )
	{
		short	nCopies = (short)ReadLongValue(lpCommand);

		// If the copies are set inside of the print job data and the PJL also
		// sets the number of copies, then the printer will actually print out
		// the product of the two values.
		if ( m_pParserData->IsCopiesSet() )
		{
			m_pParserData->SetCopies( nCopies * m_pParserData->GetCopies() );
		}
		else
		{
			m_pParserData->SetCopies( nCopies );
		}
		return;
	}

	// Handle Resolution
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_RESOLUTION) )
	{
		m_pParserData->SetPrintQuality( (short)ReadLongValue(lpCommand) );
		return;
	}

	// Handle Orientation
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_ORIENTATION) )
	{
		HandleOrientation( lpCommand );
		return;
	}

	// Handle Render Mode
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_RENDERMODE) )
	{
		HandleRenderMode( lpCommand );
		return;
	}

	// Handle Paper
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_PAPER) )
	{
		HandlePaper( lpCommand );
		return;
	}

	// Handle Finish
	if ( CHECK_STRING(lpCommand, PJL_PROPERTY_FINISH) )
	{
		HandleFinish( lpCommand );
		return;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Utility Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CPJLParser::HandleFinish( LPCSTR lpString )
{
	// Skip to the value
	SkipToValue( lpString );

	// Try every expected value, until a match is found.
	// Then, process the match, and exit.
	// If no match is found, then throw an exception.

	// Try stapled
	if ( CHECK_STRING(lpString, PJL_VALUE_FINISH_STAPLE) )
	{
		m_pParserData->SetStaple( true );
		return;
	}

	// Try none
	if ( CHECK_STRING(lpString, PJL_VALUE_FINISH_NONE) )
	{
		m_pParserData->SetStaple( false );
		return;
	}

	throw PRINT_PARSER_EXCEPTION( _T("PJL: Invalid value for Finish") );
}

void CPJLParser::HandleOrientation( LPCSTR lpString )
{
	// Skip to the value
	SkipToValue( lpString );

	// Try every expected value, until a match is found.
	// Then, process the match, and exit.
	// If no match is found, then throw an exception.

	// Try portrait
	if ( CHECK_STRING(lpString, PJL_VALUE_ORIENTATION_PORTRAIT) )
	{
		m_pParserData->SetOrientation( DMORIENT_PORTRAIT );
		return;
	}

	// Try landscape
	if ( CHECK_STRING(lpString, PJL_VALUE_ORIENTATION_LANDSCAPE) )
	{
		m_pParserData->SetOrientation( DMORIENT_LANDSCAPE );
		return;
	}

	throw PRINT_PARSER_EXCEPTION( _T("PJL: Invalid value for Orientation") );
}

void CPJLParser::HandlePaper( LPCSTR lpString )
{
	// Skip to the value
	SkipToValue( lpString );

	// Find the end of the paper name
	LPCSTR	pEndOfPaperName	= strstr( lpString, PJL_TERMINATOR );

	// If it was not found, then throw an exception
	if ( pEndOfPaperName == NULL )
	{
		throw PRINT_PARSER_EXCEPTION( _T("PJL: Invalid PaperName") );
	}

	// Declare the rest of the variables
	DWORD	dwSize			= (pEndOfPaperName - lpString);
	LPSTR	pFormName		= new char[dwSize+1];

	// Create a NULL terminated string
	::ZeroMemory( pFormName, dwSize+1 );
	::CopyMemory( pFormName, lpString, dwSize );

	// Set the paper size string
	m_pParserData->SetPaperSizeString( PJLFormNames, pFormName );

	// Release the buffer
	delete [] pFormName;
}

void CPJLParser::HandleRenderMode( LPCSTR lpString )
{
	// Skip to the value
	SkipToValue( lpString );

	// Try every expected value, until a match is found.
	// Then, process the match, and exit.
	// If no match is found, then throw an exception.

	// Try color
	if ( CHECK_STRING(lpString, PJL_VALUE_RENDERMODE_COLOR) )
	{
		m_pParserData->SetColor( DMCOLOR_COLOR );
		return;
	}

	// Try monochrome
	if ( CHECK_STRING(lpString, PJL_VALUE_ORIENTATION_LANDSCAPE) )
	{
		m_pParserData->SetColor( DMCOLOR_MONOCHROME );
		return;
	}

	throw PRINT_PARSER_EXCEPTION( _T("PJL: Invalid RenderMode") );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PJLParser.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
