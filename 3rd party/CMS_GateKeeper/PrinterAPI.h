///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrinterAPI.h: interface for the CPrinterAPI class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTERAPI_H__0C38424D_7B9D_11D4_A469_00105A1C588D__INCLUDED_)
#define AFX_PRINTERAPI_H__0C38424D_7B9D_11D4_A469_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "CMSException.h"

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Compiler Options
///////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma warning( disable : 4290 ) 

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrinterAPI  
{
// Attributes
private:
	HANDLE		m_hPrinter;
	DWORD		m_dwDocumentID;

// Construction / Destruction
public:
	CPrinterAPI()
	{
		m_hPrinter		= INVALID_HANDLE_VALUE;
		m_dwDocumentID	= 0;
	}

	~CPrinterAPI()
	{
		ClosePrinter();
	}

// Operations
public:
	void	OpenPrinter( LPTSTR lpszPrinterName, LPPRINTER_DEFAULTS pDefaults = NULL ) throw(CWinException&);
	void	ClosePrinter();

	void	SetPrinter( DWORD dwLevel, LPBYTE pPrinter, DWORD dwCommand ) throw(CWinException&);

	bool	ReadPrinter( LPVOID lpBuffer, const DWORD& dwBufferSize, LPDWORD pBytesRead ) throw(CWinException&);
	void	WritePrinter( LPVOID lpBuffer, const DWORD& dwBufferSize, LPDWORD pBytesWritten ) throw(CWinException&);

	void	GetPrintJobData( const DWORD& dwJobId, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&);
	void	SetPrintJobData( const DWORD& dwJobID, const DWORD& dwLevel, LPBYTE lpData ) throw(CWinException&);
	void	SetPrintJobCommand( const DWORD& dwJobId, const DWORD& dwCommand ) throw(CWinException&);

	DWORD	StartDoc( const DWORD& dwLevel, LPBYTE lpDocInfo ) throw(CWinException&);
	void	EndDoc() throw(CWinException&);

// Operators
public:
	operator HANDLE()												{ return m_hPrinter; };

// Static Member Functions
public:
	static void	GetPrinterData( HANDLE hPrinter, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&);
	static void	GetPrintJobData( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&);

	static void	SetPrintJobData( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwLevel, LPBYTE lpData ) throw(CWinException&);
	static void SetPrintJobCommand( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwCommand ) throw(CWinException&);

	static DWORD StartDoc( HANDLE hPrinter, const DWORD& dwLevel, LPBYTE lpDocInfo ) throw(CWinException&);
	static void EndDoc( HANDLE hPrinter ) throw(CWinException&);
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline void CPrinterAPI::OpenPrinter( LPTSTR lpszPrinterName, LPPRINTER_DEFAULTS pDefaults ) throw(CWinException&)
{
	ClosePrinter();
	_ASSERTE( m_hPrinter == INVALID_HANDLE_VALUE );
	THROW_WIN_FAIL( ::OpenPrinter(lpszPrinterName, &m_hPrinter, pDefaults) );
}

inline void CPrinterAPI::ClosePrinter()
{
	if ( m_hPrinter != INVALID_HANDLE_VALUE )
	{
		EndDoc();
		DEBUG_FAIL( ::ClosePrinter(m_hPrinter) );
		m_hPrinter = INVALID_HANDLE_VALUE;
	}
}

inline void CPrinterAPI::SetPrinter( DWORD dwLevel, LPBYTE pPrinter, DWORD dwCommand ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	THROW_WIN_FAIL( ::SetPrinter(m_hPrinter, dwLevel, pPrinter, dwCommand) );
}

inline bool CPrinterAPI::ReadPrinter( LPVOID lpBuffer, const DWORD& dwBufferSize, LPDWORD pBytesRead ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	THROW_WIN_FAIL( ::ReadPrinter(m_hPrinter, lpBuffer, dwBufferSize, pBytesRead) );
	return true;
}

inline void CPrinterAPI::WritePrinter( LPVOID lpBuffer, const DWORD& dwBufferSize, LPDWORD pBytesWritten ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	THROW_WIN_FAIL( ::WritePrinter(m_hPrinter, lpBuffer, dwBufferSize, pBytesWritten) );
}

inline void CPrinterAPI::GetPrintJobData( const DWORD& dwJobId, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	GetPrintJobData( m_hPrinter, dwJobId, dwLevel, data );
}

inline void CPrinterAPI::SetPrintJobData( const DWORD& dwJobID, const DWORD& dwLevel, LPBYTE lpData ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	SetPrintJobData( m_hPrinter, dwJobID, dwLevel, lpData );
}

inline void CPrinterAPI::SetPrintJobCommand( const DWORD& dwJobId, const DWORD& dwCommand ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	SetPrintJobCommand( m_hPrinter, dwJobId, dwCommand );
}

inline DWORD CPrinterAPI::StartDoc( const DWORD& dwLevel, LPBYTE lpDocInfo ) throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	if ( m_dwDocumentID == 0 )
	{
		m_dwDocumentID = StartDoc( m_hPrinter, dwLevel, lpDocInfo );
	}
	return m_dwDocumentID;
}

inline void CPrinterAPI::EndDoc() throw(CWinException&)
{
	_ASSERTE( m_hPrinter != INVALID_HANDLE_VALUE );
	if ( m_dwDocumentID != 0 )
	{
		EndDoc( m_hPrinter );
		m_dwDocumentID = 0;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Static Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline void CPrinterAPI::GetPrinterData( HANDLE hPrinter, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&)
{
	DWORD	dwNeeded = 0;

	// Check to get the size of the buffer needed
	::GetPrinter( hPrinter, dwLevel, NULL, 0, &dwNeeded );

	// Only proceed if we get the correct error
	if ( ::GetLastError() == ERROR_INSUFFICIENT_BUFFER )
	{
		// Allocate the buffer
		data.SetSize( dwNeeded );
		data.Zero();

		// Now get the data
		THROW_WIN_FAIL( ::GetPrinter(hPrinter, dwLevel, data, data.GetSize(), &dwNeeded) );
	}
	ELSE_THROW_WIN_FAIL();
}

inline void CPrinterAPI::GetPrintJobData( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwLevel, CBuffer& data ) throw(CWinException&)
{
	DWORD	dwNeeded = 0;

	// Check to get the size of the buffer needed
	::GetJob( hPrinter, dwJobId, dwLevel, NULL, 0, &dwNeeded );

	// Only proceed if we get the correct error
	if ( ::GetLastError() == ERROR_INSUFFICIENT_BUFFER )
	{
		// Allocate the buffer
		data.SetSize( dwNeeded );
		data.Zero();

		// Now get the data
		THROW_WIN_FAIL( ::GetJob(hPrinter, dwJobId, dwLevel, data, data.GetSize(), &dwNeeded) );
	}
	ELSE_THROW_WIN_FAIL();
}

inline void CPrinterAPI::SetPrintJobData( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwLevel, LPBYTE lpData ) throw(CWinException&)
{
	THROW_WIN_FAIL( ::SetJob(hPrinter, dwJobId, dwLevel, lpData, 0) );
}

inline void CPrinterAPI::SetPrintJobCommand( HANDLE hPrinter, const DWORD& dwJobId, const DWORD& dwCommand ) throw(CWinException&)
{
	THROW_WIN_FAIL( ::SetJob(hPrinter, dwJobId, 0, NULL, dwCommand) );
}

inline DWORD CPrinterAPI::StartDoc( HANDLE hPrinter, const DWORD& dwLevel, LPBYTE lpDocInfo ) throw(CWinException&)
{
	DWORD dwReturnMe;

	THROW_WIN_FAIL( (dwReturnMe = ::StartDocPrinter(hPrinter, dwLevel, lpDocInfo)) );
	return dwReturnMe;
}

inline void CPrinterAPI::EndDoc( HANDLE hPrinter ) throw(CWinException&)
{
	THROW_WIN_FAIL( ::EndDocPrinter(hPrinter) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobData Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename DATA_BLOCK_TYPE>
class CPrintJobData : protected DATA_BLOCK_TYPE
{
// Construction / Destruction
public:
	CPrintJobData()											{  };
	virtual ~CPrintJobData()								{  };

// Operations
public:
	void GetJobData( HANDLE hPrinter, DWORD dwJobId, DWORD dwLevel )
	{
		CPrinterAPI::GetPrintJobData( hPrinter, dwJobId, dwLevel, (*this) );
	}

	void SetJobData( HANDLE hPrinter, DWORD dwJobId, DWORD dwLevel )
	{
		CPrinterAPI::SetPrintJobData( hPrinter, dwJobId, dwLevel, (*this) );
	}

	JOB_INFO_2*	Data()										{ return (*this); };

// Operator
public:
	operator JOB_INFO_2*()									{ return reinterpret_cast<JOB_INFO_2*>(GetData()); };
};

typedef CPrintJobData<CDefaultBuffer>		CPrintJobInfo;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTERAPI_H__0C38424D_7B9D_11D4_A469_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrinterAPI.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
