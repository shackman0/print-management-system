///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL5DataType.cpp: implementation of the CPCL5DataType class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL5DataType.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL5DataType::ReadFromBuffer( CPrintJobBuffer* pBuffer )
{
	// Check the assumptions
	_ASSERTE( pBuffer != NULL );

	if ( !m_bContinue )
	{
#ifdef USE_PCL5_DEBUG
		// This is for debugging only
		if ( pBuffer->Peek() != PCL5_ESCAPE )
		{
			DEBUG_TEXT( _T("Found Command with Data!\n") );
			DEBUG_TEXT( _T("Header: %s, Value: %d, Term: %c\n"),
				GetData().nValue,
				GetData().chrTerminator );
			pBuffer->Dump();
		}
#endif
		Clear();

		if ( pBuffer->ReadByte() != PCL5_ESCAPE )
		{
			throw PRINT_PARSER_EXCEPTION( _T("PCL5: Missing Escape Character!") );
		}

		// Read the header
		m_Data.chrCommand = pBuffer->ReadByte();
		if ( IsParameterizedCommand() )
		{
			m_Data.chrGroup = pBuffer->ReadByte();
		}
		else
		{
			_ASSERTE( IsTwoCharCommand() );
			return false;
		}
	}

	// If there is a numeric value
	if ( IsNumeric(pBuffer->Peek()) )
	{
		// Read it
		m_Data.nValue = atol( (LPCSTR)pBuffer->GetCurrentBuffer() );

		// Skip past the numeric part
		while ( IsNumeric(pBuffer->Peek()) )
		{
			pBuffer->SkipAhead( sizeof(char) );
		}
	}
	else
	{
		// If there is no numeric value, a value of 0 is assumed
		// According to the PCL5 specs
		m_Data.nValue	= 0;
	}

	// Read the terminator
	m_Data.chrTerminator = pBuffer->ReadByte();

	// Set whether or not commands are grouped together
	if ( IsParameterValue(m_Data.chrTerminator) )
	{
		m_Data.chrTerminator = GetTerminatorValue(m_Data.chrTerminator);
		m_bContinue = true;
	}
	else
	{
		_ASSERTE( IsTerminatorValue(m_Data.chrTerminator) );
		m_bContinue = false;
	}
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL5DataType.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////