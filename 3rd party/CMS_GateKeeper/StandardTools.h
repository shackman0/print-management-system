///////////////////////////////////////////////////////////////////////////////////////////////////////
// StandardTools.h: interface for the CMS Standard Tools library.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_STANDARDTOOLS_H__0BF47EF5_4902_11D3_A3CD_00105A1C588D__INCLUDED_)
#define AFX_STANDARDTOOLS_H__0BF47EF5_4902_11D3_A3CD_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
namespace CMSLIB
{
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// TCHAR Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	_TLEN(str)										((sizeof(str)-1)/(sizeof(TCHAR)))

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Typedefs
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _LPCBYTE_DEFINED
	#define _LPCBYTE_DEFINED
	typedef const BYTE *LPCBYTE;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Common Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DIRECTORY_SEPERATOR								_T("\\")
#define	DOMAIN_SEPERATOR								DIRECTORY_SEPERATOR
#define LOCAL_DOMAIN									_T(".")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Bit Wise Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	BIT(n)						((1 << ((n)-1)))

inline bool IsFlagSet( const DWORD& dwFlags, const DWORD& dwIsSet )
{
	return ( (dwFlags & dwIsSet) ? true : false );
}

inline void SetFlag( DWORD& dwFlags, const DWORD& dwSetMe )
{
	dwFlags |= dwSetMe;
}

inline void ClearFlag( DWORD& dwFlags, const DWORD& dwClearMe )
{
	dwFlags &= ~dwClearMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Numerical Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

template<class SIGNTYPE>
inline int GetSign( const SIGNTYPE& nValue )
{
	return ( (nValue > 0) ? 1 : -1 );
}

template<class MAXTYPE> inline const MAXTYPE& GetMax( const MAXTYPE& nValue1, const MAXTYPE& nValue2 )
{
	return ( (nValue1 > nValue2) ? nValue1 : nValue2 );
}

template<class MINTYPE> inline const MINTYPE& GetMin( const MINTYPE& nValue1, const MINTYPE& nValue2 )
{
	return ( (nValue1 < nValue2) ? nValue1 : nValue2 );
}

template<class BETWEENTYPE> inline bool IsBetween( const BETWEENTYPE& nValue, const BETWEENTYPE& nMin, const BETWEENTYPE& nMax )
{
	_ASSERTE( nMin <= nMax );
	return ( (nMin <= nValue) && (nValue <= nMax) );
}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Averager Class Definition
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<class AVERAGETYPE>
	class CAverager
	{
	// Construction
	public:
		CAverager();

	// Data Access
	public:
		const DWORD&		GetItemCount() const;
		const AVERAGETYPE&	GetMinValue() const;
		const AVERAGETYPE&	GetMaxValue() const;

	// Operations
	public:
		void			Reset();
		AVERAGETYPE		GetAverage() const;
		void			AddItem( const AVERAGETYPE& nNew );

	// Attributes
	private:
		DWORD			m_dwItems;
		AVERAGETYPE		m_nTotalValue;
		AVERAGETYPE		m_nMinValue;
		AVERAGETYPE		m_nMaxValue;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Average Class Inline Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Construction
		///////////////////////////////////////////////////////////////////////////////////////////////
		template<typename AVERAGETYPE>
		inline CAverager<AVERAGETYPE>::CAverager()
		{
			Reset();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Data Access
		///////////////////////////////////////////////////////////////////////////////////////////////
		template<typename AVERAGETYPE>
		inline const DWORD& CAverager<AVERAGETYPE>::GetItemCount() const
		{
			return m_dwItems;
		}

		template<typename AVERAGETYPE>
		inline const AVERAGETYPE& CAverager<AVERAGETYPE>::GetMinValue() const
		{
			return m_nMinValue;
		}

		template<typename AVERAGETYPE>
		inline const AVERAGETYPE& CAverager<AVERAGETYPE>::GetMaxValue() const
		{
			return m_nMaxValue;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		template<typename AVERAGETYPE>
		inline void CAverager<AVERAGETYPE>::Reset()
		{
			::ZeroMemory( this, sizeof(*this) );
		}

		template<typename AVERAGETYPE>
		inline AVERAGETYPE CAverager<AVERAGETYPE>::GetAverage() const
		{
			return ( m_dwItems ? (m_nTotalValue / static_cast<AVERAGETYPE>(m_dwItems)) : 0 );
		}

		template<typename AVERAGETYPE>
		inline void CAverager<AVERAGETYPE>::AddItem( const AVERAGETYPE& nNew )
		{
			m_nTotalValue += nNew;
			m_dwItems++;
			m_nMinValue = GetMin<AVERAGETYPE>( nNew, m_nMinValue );
			m_nMaxValue = GetMax<AVERAGETYPE>( nNew, m_nMaxValue );
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////

// Macros to use when working with constants
#define ROUND_TO_NEAREST(x,y)							(((x)+((y)-1))&~((y)-1))

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Bit-Swapping Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	///////////////////////////////////////////////////////////////////////////////////////////////////

	#ifdef _X86_
		#define SYSTEM_LITTLE_ENDIAN		// Intel x86's are ALWAYS little endian
	#endif

	#ifdef _ALPHA_
		#define SYSTEM_BIG_ENDIAN			// Dec Alpha's are big endian (also known as network order)
	#endif

	// Put other platforms here


	// Make sure one and only one is defined
	#ifdef SYSTEM_LITTLE_ENDIAN
		#ifdef SYSTEM_BIG_ENDIAN
			#error "Both SYSTEM_LITTLE_ENDIAN and SYSTEM_BIG_ENDIAN are defined!"
		#endif
	#else
		#ifndef SYSTEM_BIG_ENDIAN
			#error "Neither SYSTEM_LITTLE_ENDIAN nor SYSTEM_BIG_ENDIAN are defined."
		#endif
	#endif

	// From this point on, this code assumes that either SYSTEM_BIG_ENDIAN is defined or
	// SYSTEM_LITTLE_ENDIAN is defined, but not both.

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Main Conversion
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline short Swap16( const short& nValue )
	{
		short	nReturnMe;

		((LPBYTE)(&nReturnMe))[0] = ((const LPBYTE)(&nValue))[1];
		((LPBYTE)(&nReturnMe))[1] = ((const LPBYTE)(&nValue))[0];
		return nReturnMe;
	}

	inline long Swap32( const long& nValue )
	{
		long	nReturnMe;

		((LPBYTE)(&nReturnMe))[0] = ((const LPBYTE)(&nValue))[3];
		((LPBYTE)(&nReturnMe))[1] = ((const LPBYTE)(&nValue))[2];
		((LPBYTE)(&nReturnMe))[2] = ((const LPBYTE)(&nValue))[1];
		((LPBYTE)(&nReturnMe))[3] = ((const LPBYTE)(&nValue))[0];
		return nReturnMe;
	}

	inline __int64 Swap64( const __int64& nValue )
	{
		__int64	nReturnMe;

		((LPBYTE)(&nReturnMe))[0] = ((const LPBYTE)(&nValue))[7];
		((LPBYTE)(&nReturnMe))[1] = ((const LPBYTE)(&nValue))[6];
		((LPBYTE)(&nReturnMe))[2] = ((const LPBYTE)(&nValue))[5];
		((LPBYTE)(&nReturnMe))[3] = ((const LPBYTE)(&nValue))[4];
		((LPBYTE)(&nReturnMe))[4] = ((const LPBYTE)(&nValue))[3];
		((LPBYTE)(&nReturnMe))[5] = ((const LPBYTE)(&nValue))[2];
		((LPBYTE)(&nReturnMe))[6] = ((const LPBYTE)(&nValue))[1];
		((LPBYTE)(&nReturnMe))[7] = ((const LPBYTE)(&nValue))[0];
		return nReturnMe;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// In Place Versions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline short Swap16( short* pValue )
	{
		*pValue = Swap16(*pValue);
		return *pValue;
	}

	inline long Swap32( long* pValue )
	{
		*pValue = Swap32(*pValue);
		return *pValue;
	}

	inline __int64 Swap64( __int64* pValue )
	{
		*pValue = Swap64(*pValue);
		return *pValue;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Big Endian Conversion
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline short BigEndianToSystem16( const short& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap16(nValue);
	#endif
	}

	inline short SystemToBigEndian16( const short& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap16(nValue);
	#endif
	}

	inline long BigEndianToSystem32( const long& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap32(nValue);
	#endif
	}

	inline long SystemToBigEndian32( const long& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap32(nValue);
	#endif
	}

	inline __int64 BigEndianToSystem64( const __int64& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap64(nValue);
	#endif
	}

	inline __int64 SystemToBigEndian64( const __int64& nValue )
	{
	#ifdef SYSTEM_BIG_ENDIAN
		return nValue;
	#else
		return ::Swap64(nValue);
	#endif
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Little Endian Conversion
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline short LittleEndianToSystem16( const short& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap16(nValue);
	#endif
	}

	inline short SystemToLittleEndian16( const short& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap16(nValue);
	#endif
	}

	inline long LittleEndianToSystem32( const long& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap32(nValue);
	#endif
	}

	inline long SystemToLittleEndian32( const long& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap32(nValue);
	#endif
	}

	inline __int64 LittleEndianToSystem64( const __int64& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap64(nValue);
	#endif
	}

	inline __int64 SystemToLittleEndian64( const __int64& nValue )
	{
	#ifdef SYSTEM_LITTLE_ENDIAN
		return nValue;
	#else
		return ::Swap64(nValue);
	#endif
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Lookup Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline CString LookupSystemError( const DWORD& dwErrorCode )
{
	CString		strMessage;
	LPTSTR		lpszTemp = 0;
	DWORD		dwTemp;

	// Get the text for the error code
	dwTemp = ::FormatMessage
		(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ARGUMENT_ARRAY,
			0,
			dwErrorCode,
			LANG_NEUTRAL,
			(LPTSTR)&lpszTemp,
			0,
			0
		);

	strMessage = lpszTemp;

	// Clean up
	if ( lpszTemp )
	{
		::LocalFree( HLOCAL(lpszTemp) );
	}

	return strMessage;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Message Display Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define MESSAGE_BOX_STYLE_NORMAL			(MB_SETFOREGROUND | MB_TOPMOST)
#define MESSAGE_BOX_STYLE_CRITICAL			(MB_SYSTEMMODAL | MESSAGE_BOX_STYLE_NORMAL)
#define MESSAGE_BOX_STYLE_QUESTION			(MB_YESNO | MB_ICONQUESTION | MB_DEFBUTTON2	|		\
													MESSAGE_BOX_STYLE_NORMAL )

#define MESSAGE_BOX_STYLE_ERROR				(MB_OK | MB_ICONERROR	|							\
													MESSAGE_BOX_STYLE_NORMAL )

#define MESSAGE_BOX_STYLE_INFO				(MB_OK | MB_ICONINFORMATION	|						\
													MESSAGE_BOX_STYLE_NORMAL )

#define MESSAGE_BOX_STYLE_RETRY				(MB_RETRYCANCEL | MB_ICONQUESTION | MB_DEFBUTTON2 |	\
													MESSAGE_BOX_STYLE_NORMAL )

inline int DisplayMessage( LPCTSTR lpszMessage, LPCTSTR lpszTitle, const DWORD& dwOptions, 
							HWND hWnd = ::GetForegroundWindow() )
{
	return ::MessageBox( hWnd, lpszMessage, lpszTitle, dwOptions );
}

inline int DisplayMessage( const UINT& nMsgResID, LPCTSTR lpszTitle, const DWORD& dwOptions, 
								HWND hWnd = ::GetForegroundWindow() )
{
	CString	strMessage;

	strMessage.LoadString( nMsgResID );
	return ::DisplayMessage( strMessage, lpszTitle, dwOptions, hWnd );
}

inline int DisplayMessage( LPCTSTR lpszMessage, const UINT& nTitleResID, const DWORD& dwOptions, 
								HWND hWnd = ::GetForegroundWindow() )
{
	CString	strTitle;

	strTitle.LoadString( nTitleResID );
	return ::DisplayMessage( lpszMessage, strTitle, dwOptions, hWnd );
}

inline int DisplayMessage( const UINT& nMsgResID, const UINT& nTitleResID,	const DWORD& dwOptions, 
								HWND hWnd = ::GetForegroundWindow() )
{
	CString	strMessage;
	CString	strTitle;

	strMessage.LoadString( nMsgResID );
	strTitle.LoadString( nTitleResID );

	return ::DisplayMessage( strMessage, strTitle, dwOptions, hWnd );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Variant Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __IOleAutomationTypes_INTERFACE_DEFINED__
	inline bool IsVariantBlank( const VARIANT& var )
	{
		return ( ((var.vt == VT_NULL) || (var.vt == VT_EMPTY)) ? true : false );
	}

	inline bool ConvertVariantBool( VARIANT_BOOL bValue )
	{
		return ( (bValue == VARIANT_TRUE) ? true : false );
	}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// COM Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __ATLBASE_H__
	#ifdef DEBUG
		#define THROW_FAILED(x)			if (FAILED(x)) DebugIssueError(x, THIS_FILE, __LINE__)
		#define THROW_FAILED_EX(x,sp)	if (FAILED(x)) DebugIssueErrorEx(x, sp ,__uuidof(sp), THIS_FILE, __LINE__)
		#define THROW_LAST_ON_FAIL(x)	if (!(x)) DebugIssueError(HRESULT_FROM_WIN32(::GetLastError()), THIS_FILE, __LINE__)
	#else
		#define THROW_FAILED(x)			if (FAILED((x))) _com_issue_error((x))
		#define THROW_FAILED_EX(x,sp)	if (FAILED((x))) _com_issue_errorex((x),(sp),__uuidof((sp)))
		#define THROW_LAST_ON_FAIL(x)	if (!(x)) _com_issue_error(HRESULT_FROM_WIN32(::GetLastError()))
	#endif

	#ifdef DEBUG
		inline void DebugIssueError( HRESULT hr, LPCTSTR lpszFile, int nLine )
		{
			DEBUG_TEXT( _T("!*!* THROW COM EXCEPTION from %s, Line #%d\n"), lpszFile, nLine );
			_com_issue_error( hr );
		}

		inline void DebugIssueErrorEx( HRESULT hr, IUnknown* pUnk, REFIID iid, LPCTSTR lpszFile, int nLine )
		{
			DEBUG_TEXT( _T("!*!* THROW COM EXCEPTION from %s, Line #%d\n"), lpszFile, nLine );
			_com_issue_errorex( hr, pUnk, iid );
		}
	#endif

	///////////////////////////////////////////////////////////////////////////////////////////////////////
	// A caller-friendy cocreate replacement
	template <class Q>
	static HRESULT CoCreateInstance2( LPTSTR lpszHost, 
		const CLSID& clsid, Q** ppq, bool bUnAuthenticated = false )
	{
		(*ppq) = 0;
   
		if (lpszHost)
		{
			USES_CONVERSION;

			COAUTHINFO		cai = { RPC_C_AUTHN_NONE, 0, 0, 
									RPC_C_AUTHN_LEVEL_NONE, RPC_C_IMP_LEVEL_IMPERSONATE, 0, 0 };
			COSERVERINFO	csi = { 0, T2W(lpszHost), 0, 0 };

			if ( bUnAuthenticated )
			{
				csi.pAuthInfo = &cai;
			}

			MULTI_QI mqi = { &(__uuidof(Q)), 0, 0 };
		  
			HRESULT hr = ::CoCreateInstanceEx( clsid, 0, CLSCTX_SERVER, &csi, 1, &mqi );

			if (FAILED(hr)) return hr;
		  
			(*ppq) = (Q*)(mqi.pItf);
			return S_OK;
		}

		return ::CoCreateInstance( clsid, 0, CLSCTX_SERVER, __uuidof(Q), (void**)(ppq) );
	}
#endif // #ifdef __ATLBASE_H__

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Recordset Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	GET_RECORDSET_VALUE(spRS, strValue)			\
	((spRS)->GetFields()->GetItem(strValue)->GetValue())

#define GET_RECORDSET_VALUE_BOOL(spRS, strValue)	\
	ConvertVariantBool( VARIANT_BOOL(GET_RECORDSET_VALUE((spRS),(strValue))) )
	
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Win32 API Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _WIN32
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CSystemHandle Class Definition
	///////////////////////////////////////////////////////////////////////////////////////////////////
	class CSystemHandle
	{
	// Construction / Destruction
	public:
		CSystemHandle( HANDLE hHandle = INVALID_HANDLE_VALUE );
		virtual ~CSystemHandle();

	// Data Access
	public:
		HANDLE		GetHandle();
		HANDLE		GetHandle() const;
		bool		IsValid() const;
		void		SetHandle( HANDLE hHandle );

	// Operations
	public:
		void		CloseHandle();

	// Operators
	public:
		LPHANDLE	operator&();
		HANDLE		operator=( HANDLE hHandle );
					operator HANDLE();
					operator HANDLE() const;
					operator LPHANDLE();

	// Attributes
	private:
		HANDLE		m_hSystemHandle;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CSystemHandle Class Inline Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Construction / Destruction
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline CSystemHandle::CSystemHandle( HANDLE hHandle )
		{
			m_hSystemHandle = hHandle;
		}

		inline CSystemHandle::~CSystemHandle()
		{
			CloseHandle();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Data Access
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline HANDLE CSystemHandle::GetHandle()
		{
			return m_hSystemHandle;
		}

		inline HANDLE CSystemHandle::GetHandle() const
		{
			return m_hSystemHandle;
		}

		inline void CSystemHandle::SetHandle( HANDLE hHandle )
		{
			CloseHandle();
			m_hSystemHandle = hHandle;
		}

		inline bool CSystemHandle::IsValid() const
		{
			return (m_hSystemHandle != INVALID_HANDLE_VALUE);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline void CSystemHandle::CloseHandle()
		{
			if ( m_hSystemHandle != INVALID_HANDLE_VALUE )
			{
				DEBUG_FAIL( ::CloseHandle(m_hSystemHandle) );
				m_hSystemHandle = INVALID_HANDLE_VALUE;
			}
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operators
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline LPHANDLE CSystemHandle::operator&()
		{
			return &m_hSystemHandle;
		}

		inline HANDLE CSystemHandle::operator=( HANDLE hHandle )
		{
			SetHandle( hHandle );
			return GetHandle();
		}

		inline CSystemHandle::operator HANDLE()
		{
			return GetHandle();
		}

		inline CSystemHandle::operator HANDLE() const
		{
			return GetHandle();
		}

		inline CSystemHandle::operator LPHANDLE()
		{
			return &m_hSystemHandle;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Compare Operators
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline bool operator==( const CSystemHandle& hSystem1, const CSystemHandle& hSystem2 )
		{
			return ( hSystem1.GetHandle() == hSystem2.GetHandle() );
		}

		inline bool operator==( const CSystemHandle& hSystem1, HANDLE hSystem2 )
		{
			return ( hSystem1.GetHandle() == hSystem2 );
		}

		inline bool operator==( HANDLE hSystem1, const CSystemHandle& hSystem2 )
		{
			return ( hSystem1 == hSystem2.GetHandle() );
		}

		inline bool operator!=( const CSystemHandle& hSystem1, const CSystemHandle& hSystem2 )
		{
			return ( hSystem1.GetHandle() != hSystem2.GetHandle() );
		}

		inline bool operator!=( const CSystemHandle& hSystem1, HANDLE hSystem2 )
		{
			return ( hSystem1.GetHandle() != hSystem2 );
		}

		inline bool operator!=( HANDLE hSystem1, const CSystemHandle& hSystem2 )
		{
			return ( hSystem1 != hSystem2.GetHandle() );
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CSystemTimer Class Definition
	///////////////////////////////////////////////////////////////////////////////////////////////////
	class CSystemTimer
	{
	// Construction / Destruction
	public:
		CSystemTimer();
		~CSystemTimer();

	// Data Access
	public:
		const double&	GetClockTime() const;
		const double&	GetKernelTime() const;
		const double&	GetUserTime() const;
		double			GetTotalTime() const;

	// Operations
	public:
		void			GetTotalTimes( const bool& bThreadTimer = true );
		void			StartTimer( const bool& bThreadTimer = true );
		void			StopTimer();

	private:
		void			Reset();

	// Static Operations
	protected:
		static void		GetTimes( __int64& nKernelTime, __int64& nUserTime, const bool& bThreadTimer );
		static double	ConvertToSeconds( const __int64& nTimeUnits );
		static double	ConvertToSeconds( const DWORD& dwTicks );

	// Attributes
	private:
		__int64			m_nKernelStartTime;
		__int64			m_nUserStartTime;
		DWORD			m_dwStartTickCount;
		double			m_dClockTime;
		double			m_dKernelTime;
		double			m_dUserTime;
		bool			m_bThreadTimer;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Definitions
	///////////////////////////////////////////////////////////////////////////////////////////////////

	#define TIME_UNITS_PER_SECOND					((double)(1e7))		// 100 ns *	1*10^7	= 1s
	#define TICKS_PER_SECOND						((double)(1e3))		// 1 ms *	1*10^3	= 1s

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CSystemTimer Class Inline Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Construction / Destruction
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline CSystemTimer::CSystemTimer()
		{
			// Initialize the values
			Reset();
			m_bThreadTimer = true;
		}

		inline CSystemTimer::~CSystemTimer()
		{
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Data Access
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline const double& CSystemTimer::GetClockTime() const
		{
			return m_dClockTime;
		}

		inline const double& CSystemTimer::GetKernelTime() const
		{
			return m_dKernelTime;
		}

		inline const double& CSystemTimer::GetUserTime() const
		{
			return m_dUserTime;
		}

		inline double CSystemTimer::GetTotalTime() const			
		{
			return GetKernelTime() + GetUserTime();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline void CSystemTimer::StartTimer( const bool& bUseThreadTimes )
		{
			// Reset all the values
			Reset();

			// Record if using Thread or Process Times
			m_bThreadTimer = bUseThreadTimes;

			// Record the start times
			GetTimes( m_nKernelStartTime, m_nUserStartTime, m_bThreadTimer );

			// Record the tick count
			m_dwStartTickCount = ::GetTickCount();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Static Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline double CSystemTimer::ConvertToSeconds( const __int64& nTimeUnits )
		{
			return ( static_cast<double>(nTimeUnits) / TIME_UNITS_PER_SECOND );
		}

		inline double CSystemTimer::ConvertToSeconds( const DWORD& dwTicks )
		{
			return ( static_cast<double>(dwTicks) / 1000 );
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CConsole Class Definition
	///////////////////////////////////////////////////////////////////////////////////////////////////

	#ifdef USE_CMS_CONSOLE

	///////////////////////////////////////////////////////////////////////////////////////////////////
	
	class CConsole
	{
	// Construction / Destruction
	public:
		CConsole();
		~CConsole();

	// Operations
	public:
		static void		AllocConsole();
		static void		FreeConsole();
		static bool		IsErrorConsoleValid();
		static bool		IsInputConsoleValid();
		static bool		IsOutputConsoleValid();
		static void		MakeNewConsole();
		static DWORD	ReadText( LPTSTR lpszText, const DWORD& dwLength );
		static void		SetConsoleTitle( LPCTSTR lpszConsoleTitle );
		static void		SetTextColor( const WORD& wNewColor );
		static bool		WaitForKeyPress( const DWORD& dwTimeOut = INFINITE );
		static void		WriteText( LPCTSTR lpszFormat, ... );
		static void		WriteText( const WORD& wColor, LPCTSTR lpszFormat, ... );
		static void		WriteError( LPCTSTR lpszFormat, ... );
		static void		WriteError( const WORD& wColor, LPCTSTR lpszFormat, ... );

	// Internal Operations
	private:
		static void		AssignHandles();
		static void		DoAllocConsole();
		static void		DoFreeConsole();
		static WORD		GetTextColor( HANDLE hConsole );
		static bool		IsHandleValid( HANDLE hTestMe );
		static void		SetTextColor( HANDLE hConsole, const WORD& wNewColor );
		static void		WriteConsoleText( HANDLE hConsole, LPCTSTR lpszFormat, va_list& args );
		static void		WriteConsoleText( const WORD& wColor, HANDLE hConsole, 
							LPCTSTR lpszFormat, va_list& args );
		static void		WriteConsole( HANDLE hConsole, LPCTSTR lpszText, const DWORD& dwLength );

	// Attributes
	private:
		static DWORD						m_dwConsoleCount;
		static BOOL							m_bCreated;
		static HANDLE						m_hError;
		static HANDLE						m_hInput;
		static HANDLE						m_hOutput;

		DECLARE_STATIC_THREAD_LOCK();
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CConsole Class Inline Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Construction / Destruction
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline CConsole::CConsole()
		{
			AllocConsole();
		}

		inline CConsole::~CConsole()
		{
			FreeConsole();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline void CConsole::AllocConsole()
		{
			// Make it thread safe
			STATIC_LOCK_THREAD();

			// Increment the counter and alloc a console if necessary
			if ( m_dwConsoleCount++ == 0 )
			{
				DoAllocConsole();
			}
		}

		inline void CConsole::FreeConsole()
		{
			// Make it thread safe
			STATIC_LOCK_THREAD();

			// Increment the counter and alloc a console if necessary
			if ( --m_dwConsoleCount == 0 )
			{
				DoFreeConsole();
			}
		}

		inline bool CConsole::IsErrorConsoleValid()
		{
			return IsHandleValid( m_hError );
		}

		inline bool CConsole::IsInputConsoleValid()
		{
			return IsHandleValid( m_hInput );
		}

		inline bool CConsole::IsOutputConsoleValid()
		{
			return IsHandleValid( m_hOutput );
		}

		inline void CConsole::MakeNewConsole()
		{
			// Make it thread safe
			STATIC_LOCK_THREAD();

			// Check assumptions
			_ASSERTE( m_dwConsoleCount > 0 );

			// Free the existing console
			DoFreeConsole();

			// Alloc a new one
			DoAllocConsole();

			// Check more assumptions
			_ASSERTE( m_bCreated != FALSE );				// The new console should be created
			_ASSERTE( m_hError != INVALID_HANDLE_VALUE );	// The error handle should be valid
			_ASSERTE( m_hInput != INVALID_HANDLE_VALUE );	// The input handle should be valid
			_ASSERTE( m_hOutput != INVALID_HANDLE_VALUE );	// The output handle should be valid
		}

		inline DWORD CConsole::ReadText( LPTSTR lpszText, const DWORD& dwLength )
		{
			DWORD	dwCharsRead	= 0;

			// Check assumptions
			_ASSERTE( m_hInput != INVALID_HANDLE_VALUE );

			DEBUG_FAIL( ::ReadConsole(m_hInput, lpszText, dwLength, &dwCharsRead, NULL) );

			return dwCharsRead;
		}

		inline void CConsole::SetConsoleTitle( LPCTSTR lpszConsoleTitle )
		{
			DEBUG_FAIL( ::SetConsoleTitle(lpszConsoleTitle) );
		}

		inline void CConsole::SetTextColor( const WORD& wNewColor )
		{
			// Check assumptions
			_ASSERTE( m_hOutput != INVALID_HANDLE_VALUE );

			SetTextColor( m_hOutput, wNewColor );
		}

		inline void CConsole::WriteText( LPCTSTR lpszFormat, ... )
		{
			va_list args;
			va_start(args, lpszFormat);

			// Check assumptions
			_ASSERTE( m_hOutput != INVALID_HANDLE_VALUE );

			WriteConsoleText( m_hOutput, lpszFormat, args );

			va_end(args);
		}

		inline void CConsole::WriteText( const WORD& wColor, LPCTSTR lpszFormat, ... )
		{
			va_list args;
			va_start(args, lpszFormat);

			// Check assumptions
			_ASSERTE( m_hOutput != INVALID_HANDLE_VALUE );

			WriteConsoleText( wColor, m_hOutput, lpszFormat, args );

			va_end(args);
		}

		inline void CConsole::WriteError( LPCTSTR lpszFormat, ... )
		{
			va_list args;
			va_start(args, lpszFormat);

			// Check assumptions
			_ASSERTE( m_hError != INVALID_HANDLE_VALUE );

			WriteConsoleText( m_hError, lpszFormat, args );

			va_end(args);
		}

		inline void CConsole::WriteError( const WORD& wColor, LPCTSTR lpszFormat, ... )
		{
			va_list args;
			va_start(args, lpszFormat);

			// Check assumptions
			_ASSERTE( m_hError != INVALID_HANDLE_VALUE );

			WriteConsoleText( wColor, m_hError, lpszFormat, args );

			va_end(args);
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Internal Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline void CConsole::AssignHandles()
		{
			// Get and check the Error Handle
			m_hError	= ::GetStdHandle( STD_ERROR_HANDLE );
			DEBUG_FAIL( IsErrorConsoleValid() );

			// Get and check the Input Handle
			m_hInput	= ::GetStdHandle( STD_INPUT_HANDLE );
			DEBUG_FAIL( IsInputConsoleValid() );

			// Get and check the Output Handle
			m_hOutput	= ::GetStdHandle( STD_OUTPUT_HANDLE );
			DEBUG_FAIL( IsOutputConsoleValid() );
		}

		inline void CConsole::DoAllocConsole()
		{
			// Make it thread safe
			STATIC_LOCK_THREAD();

			// Try and create a console
			m_bCreated = ::AllocConsole();
			DEBUG_FAIL( m_bCreated );

			// Assign the handles
			AssignHandles();
		}

		inline void CConsole::DoFreeConsole()
		{
			// Make it thread safe
			STATIC_LOCK_THREAD();

			// Check assumptions
			_ASSERTE( (m_dwConsoleCount == 0) || IsErrorConsoleValid() );
			_ASSERTE( (m_dwConsoleCount == 0) || IsInputConsoleValid() );
			_ASSERTE( (m_dwConsoleCount == 0) || IsOutputConsoleValid() );

			if ( m_bCreated )
			{
				DEBUG_FAIL( ::FreeConsole() );
			}

			// Invalidate all the handles
			m_hError	= INVALID_HANDLE_VALUE;
			m_hInput	= INVALID_HANDLE_VALUE;
			m_hOutput	= INVALID_HANDLE_VALUE;
		}

		inline bool CConsole::IsHandleValid( HANDLE hTestMe )
		{
			return ( (hTestMe != INVALID_HANDLE_VALUE) && (hTestMe != NULL) );
		}

		inline void CConsole::SetTextColor( HANDLE hConsole, const WORD& wNewColor )
		{
			// Check assumptions
			_ASSERTE( hConsole != INVALID_HANDLE_VALUE );

			DEBUG_FAIL( ::SetConsoleTextAttribute(hConsole, wNewColor) );
		}

		inline void CConsole::WriteConsoleText( const WORD& wColor, HANDLE hConsole, 
							LPCTSTR lpszFormat, va_list& args )
		{
			// Check assumptions
			_ASSERTE( hConsole != INVALID_HANDLE_VALUE );

			WORD	wOldColor = GetTextColor(hConsole);

			SetTextColor( hConsole, wColor );
			WriteConsoleText( hConsole, lpszFormat, args );
			SetTextColor( hConsole, wOldColor );
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////

	#endif // #ifdef USE_CMS_CONSOLE

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CCMSBitmap Class Definition
	///////////////////////////////////////////////////////////////////////////////////////////////////
	class CCMSBitmap
	{
	// Construction / Destruction
	public:
		CCMSBitmap();
		virtual ~CCMSBitmap();

	// Data Access
	public:
		HBITMAP		GetBitmap();
		bool		IsValid() const;
		void		SetBitmap( HBITMAP hBitmap );

	// Operations
	public:
		void		CloseBitmap();
		bool		LoadBitmap( HINSTANCE hInstance, const UINT& nResID );
		bool		LoadBitmap( LPCTSTR lpszFilePath );

	// Operators
	public:
					operator HBITMAP();

	// Attributes
	private:
		HBITMAP		m_hBitmap;
	};

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// CSystemHandle Class Inline Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Construction / Destruction
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline CCMSBitmap::CCMSBitmap()
		{
			m_hBitmap = NULL;
		}

		inline CCMSBitmap::~CCMSBitmap()
		{
			CloseBitmap();
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Data Access
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline HBITMAP CCMSBitmap::GetBitmap()
		{
			return m_hBitmap;
		}

		inline bool CCMSBitmap::IsValid() const
		{
			return ( m_hBitmap != NULL );
		}

		inline void CCMSBitmap::SetBitmap( HBITMAP hBitmap )
		{
			// Check assumptions
			_ASSERTE( hBitmap != NULL );

			// Close the old bitmap (if applicable)
			CloseBitmap();

			// Assign the new value
			m_hBitmap = hBitmap;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operations
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline void CCMSBitmap::CloseBitmap()
		{
			if ( IsValid() )
			{
				DEBUG_FAIL( ::DeleteObject(m_hBitmap) );
				m_hBitmap = NULL;
			}
		}

		inline bool CCMSBitmap::LoadBitmap( HINSTANCE hInstance, const UINT& nResID )
		{
			bool	bReturnMe = false;
			HBITMAP hBitmap;
			
			// Load the bitmap from the resource
			hBitmap = ::LoadBitmap( hInstance, MAKEINTRESOURCE(nResID) );
			if ( hBitmap != NULL )
			{
				SetBitmap( hBitmap );
				bReturnMe = true;
			}
			DEBUG_ELSE();

			// Return Success or Failure
			return bReturnMe;
		}

		inline bool CCMSBitmap::LoadBitmap( LPCTSTR lpszFilePath )
		{
			bool	bReturnMe = false;
			HBITMAP	hBitmap;

			// Load the bitmap from the file
			hBitmap = static_cast<HBITMAP>(
						::LoadImage(NULL, lpszFilePath, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE) );
			if ( hBitmap != NULL )
			{
				SetBitmap( hBitmap );
				bReturnMe = true;
			}
			DEBUG_ELSE();

			// Return Success or Failure
			return bReturnMe;
		}

		///////////////////////////////////////////////////////////////////////////////////////////////
		// Operators
		///////////////////////////////////////////////////////////////////////////////////////////////
		inline CCMSBitmap::operator HBITMAP()
		{
			return GetBitmap();
		}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Win32 Utility Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void GetComputerName( CString& strComputerName )
	{
		DWORD dwSize = MAX_COMPUTERNAME_LENGTH + 1;

		DEBUG_FAIL( ::GetComputerName(strComputerName.GetBufferSetLength(dwSize), &dwSize) );
		strComputerName.ReleaseBuffer();
	}

	inline CString GetComputerName()
	{
		CString strReturnMe;

		GetComputerName( strReturnMe );
		return strReturnMe;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
#endif // #ifdef _WIN32
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
}	// End of CMSLIB Namespace
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_STANDARDTOOLS_H__0BF47EF5_4902_11D3_A3CD_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of StandardTools.h
///////////////////////////////////////////////////////////////////////////////////////////////////////