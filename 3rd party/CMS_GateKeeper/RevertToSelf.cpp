///////////////////////////////////////////////////////////////////////////////////////////////////////
// RevertToSelf.cpp: implementation of the CRevertToSelf class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RevertToSelf.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG

#ifdef USE_DEBUG_OUTPUT
	// Comment out this line to not display token information
	//#define SHOW_DEBUG_TOKEN_INFO
#endif

#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	TOKEN_INFO_BUFFERSIZE	512

#ifdef SHOW_DEBUG_TOKEN_INFO
	#define DEBUG_TOKEN				Dump
#else
	#define DEBUG_TOKEN				((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

CRevertToSelf::CRevertToSelf()
{
	// Set the default values
	m_bIsReverted		= FALSE;

	// Get the current security context
	m_stContext.GetCurrentContext();
}

CRevertToSelf::~CRevertToSelf()
{
	// Ensure the security context is restored
	ImpersonateAgain();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CRevertToSelf::Revert()
{
	if ( !m_bIsReverted )
	{
		// If the Security Token is valid
		if ( m_stContext.IsValid() )
		{
			// Revert back to using the original security token
			DEBUG_FAIL( (m_bIsReverted = ::RevertToSelf()) );
		}
	}

	// Show more information while debugging
	DEBUG_TOKEN();

	return m_bIsReverted;
}

BOOL CRevertToSelf::ImpersonateAgain()
{
	if ( m_bIsReverted )
	{
		// Check the assumptions
		_ASSERTE( m_stContext.IsValid() );

		// Impersonate the user based on the cached value
		if ( ::ImpersonateLoggedOnUser(m_stContext) )
		{
			m_bIsReverted = FALSE;
		}
		DEBUG_ELSE();
	}

	// Show more information while debugging
	DEBUG_TOKEN();

	return !m_bIsReverted;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CRevertToSelf::Dump()
{
	DEBUG_TEXT( _T("Original:\t") );
	m_stContext.Dump();

	CSecurityToken	stCurrent;
	stCurrent.GetCurrentContext();

	DEBUG_TEXT( _T("Current:\t") );
	stCurrent.Dump();
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CSecurityToken Static Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline BOOL CSecurityToken::GetCurrentContext( PHANDLE pSecurityToken )
{
	BOOL bReturnMe = FALSE;

	// Check the assumptions
	_ASSERTE( pSecurityToken != NULL );

	// Get the current security token
	if( ::OpenThreadToken(::GetCurrentThread(), 
			TOKEN_QUERY|TOKEN_IMPERSONATE, TRUE, pSecurityToken) == TRUE )
	{
		bReturnMe = TRUE;
	}
	else
	{
		if( ::GetLastError() == ERROR_NO_TOKEN )
		{
			// Attempt to open the process token, since no thread token exists
			if( ::OpenProcessToken(::GetCurrentProcess(), 
					TOKEN_QUERY|TOKEN_IMPERSONATE, pSecurityToken) == TRUE )
			{
				bReturnMe = TRUE;
			}
			DEBUG_ELSE();
		}
		DEBUG_ELSE();
	}

	// Return Success or Failure
	return bReturnMe;
}

BOOL CSecurityToken::GetUserAndDomainNames( HANDLE hSecurityToken, 
										   CString& strUserName, CString& strDomainName )
{
	static BYTE	pDataBuffer[TOKEN_INFO_BUFFERSIZE];
	DWORD	dwDataBufferSize	= sizeof(pDataBuffer);
	BOOL	bSuccess			= FALSE;

	if ( hSecurityToken != INVALID_HANDLE_VALUE )
	{
		DEBUG_FAIL( (bSuccess = ::GetTokenInformation(hSecurityToken, TokenUser, 
						pDataBuffer, dwDataBufferSize, &dwDataBufferSize)) );

		if ( bSuccess )
		{
			SID_NAME_USE	snu;
			DWORD			dwUserNameSize		= MAX_COMPUTERNAME_LENGTH+1;
			DWORD			dwDomainNameSize	= MAX_COMPUTERNAME_LENGTH+1;

			DEBUG_FAIL( (bSuccess = ::LookupAccountSid(NULL, 
				((PTOKEN_USER)pDataBuffer)->User.Sid, 
					strUserName.GetBufferSetLength(dwUserNameSize), &dwUserNameSize, 
					strDomainName.GetBufferSetLength(dwDomainNameSize), &dwDomainNameSize, &snu)) );

			// Release the buffers
			strUserName.ReleaseBuffer( dwUserNameSize );
			strDomainName.ReleaseBuffer( dwDomainNameSize );
		}
	}
	return bSuccess;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _DEBUG
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DUMP_BUFFER_SIZE		1024
void CSecurityToken::Dump()
{
	if ( IsValid() )
	{
		CString strUserName;
		CString	strDomainName;

		if ( GetUserAndDomainNames(strUserName, strDomainName) )
		{
			DEBUG_TEXT( _T("Security Context at 0x%08x: %s\\%s\n"), m_hSecurityToken, 
				strUserName, strDomainName );
		}
		else
		{
			DEBUG_TEXT( _T("Unable to read security context info!\n") );
		}
	}
	else
	{
		DEBUG_TEXT( _T("Invalid Handle\n") );
	}
}
///////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of RevertToSelf.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////