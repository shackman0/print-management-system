///////////////////////////////////////////////////////////////////////////////////////////////////////
// DebugUtils.h: interface for the CMS Debugging Utilities.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEBUGUTILS_H__43511FF7_24D9_11D3_BEEF_00E02931B31A__INCLUDED_)
#define AFX_DEBUGUTILS_H__43511FF7_24D9_11D3_BEEF_00E02931B31A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
	#include <crtdbg.h>
#else
	#ifndef _ASSERTE
		#define _ASSERTE(n)					((void)(0))
	#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Standard Win32 Utils
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef	USE_DEBUG_OUTPUT
	#define DEBUG_DUMP(p,n)					DumpRawData(p,n)
	#define	DEBUG_TEXT						DisplayDebugText
	#define DEBUG_ELSE						else DEBUG_LAST_ERROR
	#define DEBUG_ELSE_TEXT					else DEBUG_TEXT
	#define ELSE_DEBUG_ERROR(err)			else DEBUG_ERROR(err)
	#define DEBUG_FAIL(f)					if (!(f))	{ DEBUG_LAST_ERROR(); }
	#define DEBUG_FAIL_ON_ZERO(f)			if ((f)==0)	{ DEBUG_LAST_ERROR(); }
	#ifdef _DEBUG
		#define DEBUG_ERROR(err)			::DisplayError(err, THIS_FILE, __LINE__)
		#define DEBUG_LAST_ERROR()			::DisplayLastError(THIS_FILE, __LINE__)
	#else
		#define DEBUG_ERROR(err)			::DisplayError(err)
		#define DEBUG_LAST_ERROR()			::DisplayLastError()
	#endif

void		DisplayDebugText( LPCTSTR lpszFormat, ... );
void		DumpRawData( LPCVOID pData, const DWORD& dwSize );
void		DisplayError( DWORD dwError, LPCTSTR lpszFile = NULL, int nLine = 0 );

inline void	DisplayLastError( LPCTSTR lpszFile = NULL, int nLine = 0 )
{
	// Record the last error
	DWORD dwLastError = ::GetLastError();

	// Display the error message
	DisplayError(dwLastError, lpszFile, nLine);
	
	// Set the last error to ensure it hasn't changed
	::SetLastError( dwLastError );
};

#else
	#define DEBUG_DUMP						((void)(0))
	#define	DEBUG_TEXT						((void)(0))
	#define DEBUG_ELSE						((void)(0))
	#define DEBUG_ELSE_TEXT					((void)(0))
	#define ELSE_DEBUG_ERROR(err)			((void)(0))
	#define	DEBUG_FAIL(f)					((void)(f))
	#define	DEBUG_FAIL_ON_ZERO(f)			((void)(f))
	#define DEBUG_ERROR						((void)(0))
	#define DEBUG_LAST_ERROR				((void)(0))
#endif

#define DEBUG_BOOL(b)						((b) ? _T("True") : _T("False"))

///////////////////////////////////////////////////////////////////////////////////////////////////////
// COM Debugging
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __ATLBASE_H__
#include <comdef.h>
	// HRESULT Checking
	#ifdef _DEBUG
		#define CHECK_HR(hr,str)						CheckHR( (hr),(str) )
		inline HRESULT CheckHR(HRESULT hr, LPCTSTR lpszCause)
		{
			if (FAILED(hr))
			{
				TCHAR szMsg[512];

				::FormatMessage( FORMAT_MESSAGE_FROM_SYSTEM, 0, hr, 0, szMsg, 500, 0 );
		#ifdef USE_DISPLAY
				if ( ::MessageBox(0, szMsg, lpszCause, MB_ICONSTOP|MB_OKCANCEL|MB_SETFOREGROUND) 
					== IDCANCEL )
				{
					::DebugBreak();
				}
		#endif
				DEBUG_TEXT( szMsg );
			}
			return hr;
		}
	#else
		#define CHECK_HR(hr,str)						((hr))
	#endif

	// COM Exception / Error Codes
	#ifdef	USE_DEBUG_OUTPUT
		#ifdef _DEBUG
			#define DEBUG_COM_FAIL(hr)			::DisplayComErrorCode(hr, THIS_FILE, __LINE__)
			#define DEBUG_COM_EXCEPTION(e)		::DisplayComException(e, THIS_FILE, __LINE__)
		#else
			#define DEBUG_COM_FAIL(hr)			::DisplayComErrorCode(hr)
			#define DEBUG_COM_EXCEPTION(e)		::DisplayComException(e)
		#endif

	void		DisplayComException( const _com_error&, LPCTSTR lpszFile = NULL, int nLine = 0 );
	inline void	DisplayComErrorCode( HRESULT hr, LPCTSTR lpszFile = NULL, int nLine = 0 )
	{
		if (FAILED(hr)) { DisplayError(hr, lpszFile, nLine); }
	}

	#else
		#define DEBUG_COM_EXCEPTION		((void)(0))
		#define DEBUG_COM_FAIL(hr)		((void)(0))
	#endif

#endif // #ifdef __ATLBASE_H__

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Memory Debugging 
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
	#ifdef DEBUG_MEMORY
		#define BEGIN_MEMORY_DUMP_CHECK()			_CrtMemState s1, s2, s3; _CrtMemCheckpoint(&s1)
		#define END_MEMORY_DUMP_CHECK(t) 	\
			_CrtMemCheckpoint( &s2 );\
			DEBUG_TEXT( _T("Dumping Memory Leaks for ") t _T("\n") ); \
			if ( _CrtMemDifference( &s3, &s1, &s2 ) ) \
				_CrtMemDumpStatistics( &s3 );	\
			_CrtMemDumpAllObjectsSince( &s1 )
	#else
		#define BEGIN_MEMORY_DUMP_CHECK()				((void)(0))
		#define END_MEMORY_DUMP_CHECK(t)				((void)(0))
	#endif
#else
	#define BEGIN_MEMORY_DUMP_CHECK()				((void)(0))
	#define END_MEMORY_DUMP_CHECK(t)				((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_DEBUGUTILS_H__43511FF7_24D9_11D3_BEEF_00E02931B31A__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of DebugUtils.h
///////////////////////////////////////////////////////////////////////////////////////////////////////