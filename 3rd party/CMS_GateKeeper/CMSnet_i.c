/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Thu Jul 26 12:46:14 2001
 */
/* Compiler settings for D:\Data\Projects\CMS\Print Tracking\CMSnet\Server\cmsnet\CMSnet.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IPrintJob = {0xD00A4D7B,0xBD54,0x11d3,{0xA4,0x1C,0x00,0xE0,0x29,0x31,0xB1,0x8E}};


const IID IID_IAddInManager = {0xD00A4D7C,0xBD54,0x11d3,{0xA4,0x1C,0x00,0xE0,0x29,0x31,0xB1,0x8E}};


const IID IID_IAddInManager2 = {0xD50834C8,0x26B5,0x440a,{0x87,0x7B,0x49,0xB7,0x2C,0xB5,0x28,0xE4}};


const IID LIBID_CMSnetLib = {0xD00A4D7E,0xBD54,0x11d3,{0xA4,0x1C,0x00,0xE0,0x29,0x31,0xB1,0x8E}};


const CLSID CLSID_PrintJob = {0xD00A4D85,0xBD54,0x11d3,{0xA4,0x1C,0x00,0xE0,0x29,0x31,0xB1,0x8E}};


const CLSID CLSID_AddInManager = {0xD00A4D8B,0xBD54,0x11d3,{0xA4,0x1C,0x00,0xE0,0x29,0x31,0xB1,0x8E}};


#ifdef __cplusplus
}
#endif

