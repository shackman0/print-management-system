///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobBuffer.h: interface for the CPrintJobBuffer class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTJOBBUFFER_H__94E5AF23_7384_49D5_B456_0E4EB38D7D05__INCLUDED_)
#define AFX_PRINTJOBBUFFER_H__94E5AF23_7384_49D5_B456_0E4EB38D7D05__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobBuffer Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrintJobBuffer  
{
// Enumerated Type
public:
	enum	ByteOrder
	{
		ByteOrderUnknown		= 0,
		ByteOrderBigEndian		= 1,
		ByteOrderLittleEndian	= 2
	};

// Attributes
private:
	LPBYTE			m_pBuffer;
	DWORD			m_dwLength;
	DWORD			m_dwPosition;
	ByteOrder		m_eByteOrder;

// Construction / Destruction
public:
	CPrintJobBuffer();
	virtual ~CPrintJobBuffer();

// Data Access
public:
	// Get Functions
	const LPBYTE		GetBuffer() const						{ return m_pBuffer; };
	const DWORD&		GetBufferLength() const					{ return m_dwLength; };
	const ByteOrder&	GetByteOrder() const					{ return m_eByteOrder; };
	DWORD				GetBytesRemaining() const				{ return m_dwLength - m_dwPosition; };
	LPBYTE				GetCurrentBuffer() const				{ return ( GetBytesRemaining() ? (m_pBuffer + m_dwPosition) : NULL ); };
	// Set Functions
	void				SetByteOrder( const ByteOrder& bo )		{ m_eByteOrder = bo; };
	void				SetBuffer(  const LPBYTE pBuffer, const DWORD& dwLength );
	// Look Ahead
	char				Peek() const							{ return *((LPSTR)GetCurrentBuffer()); };

// Byte Order Operations
public:
#ifdef _X86_
	bool				DoesByteOrderMatch() const				{ return (m_eByteOrder == ByteOrderLittleEndian); };
#else
	bool				DoesByteOrderMatch() const				{ return (m_eByteOrder == ByteOrderBigEndian); };
#endif

// Searching Functions
public:
	static LPBYTE		FindPattern( const LPBYTE pBuffer, const DWORD& dwBufferSize, const LPBYTE pPattern, const DWORD& dwPatternSize );

// Operations
public:
	// Read operators
	BYTE	ReadByte();
	WORD	ReadUInt16();
	UINT	ReadUInt32();
	float	ReadReal32()										{ return (float)(ReadUInt32()); };
	short	ReadSInt16()										{ return (short)(ReadUInt16()); };
	long	ReadSInt32()										{ return (long)(ReadUInt32()); };
	// Move around in the buffer
	void	SkipAhead( const DWORD& dwBytes )
	{
		if ( dwBytes < GetBytesRemaining() )
		{
			m_dwPosition += dwBytes;
		}
		else
		{
			m_dwPosition = m_dwLength;
		}
	}
	void	BackUp( const DWORD& dwBytes )
	{
		if ( dwBytes < m_dwPosition)
		{
			m_dwPosition -= dwBytes;
		}
		else
		{
			m_dwPosition = 0;
		}
	}

// Debugging Operations
#ifdef USE_DEBUG_OUTPUT
	void Dump() const
	{
		DEBUG_DUMP( m_pBuffer + m_dwPosition, ( (GetBytesRemaining() > 32) ? 32 : GetBytesRemaining() ) );
	}
#endif

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobBuffer Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CPrintJobBuffer::SetBuffer( const LPBYTE pBuffer, const DWORD& dwLength )
	{
		m_pBuffer		= pBuffer;
		m_dwLength		= dwLength;
		m_dwPosition	= 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Searching Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline LPBYTE CPrintJobBuffer::FindPattern( const LPBYTE pBuffer, const DWORD& dwBufferSize, const LPBYTE pPattern, const DWORD& dwPatternSize )
	{
		unsigned register	nCounter;
		unsigned register	nCounter2;
		bool				bMatch;

		// Do not allow patterns larger than the buffer
		if ( dwPatternSize > dwBufferSize )
		{
			return NULL;
		}

		for ( nCounter = 0 ; nCounter < (dwBufferSize-dwPatternSize+1) ; nCounter++ )
		{
			// If the first byte matches, then check the rest
			if ( pBuffer[nCounter] == pPattern[0] )
			{ 
				// Assume a match will be found
				bMatch = true;

				// Start at 1 because 0 was already checked
				// Continue until the end of the pattern is reached, or a non-match is found
				for ( nCounter2 = 1 ; ( (nCounter2 < dwPatternSize) && (bMatch == true) ) ; nCounter2++ )
				{
					// Check and mark when a non-match is discovered
					if ( pBuffer[nCounter + nCounter2] != pPattern[nCounter2] )
					{
						bMatch = false;
					}
				}
			
				// If a match was found, return
				if ( bMatch == true )
				{
					return pBuffer + nCounter;
				}
			}
		}

		// Not found
		return NULL;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline BYTE CPrintJobBuffer::ReadByte()
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );
		_ASSERTE( m_dwLength != 0 );
		_ASSERTE( GetBytesRemaining() >= sizeof(BYTE) );

		// Read the next byte in the buffer
		return (m_pBuffer[m_dwPosition++]);
	}

	inline WORD CPrintJobBuffer::ReadUInt16()
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );
		_ASSERTE( m_dwLength != 0 );
		_ASSERTE( GetBytesRemaining() >= sizeof(WORD) );
		_ASSERTE( m_eByteOrder != ByteOrderUnknown );

		// Declare the variables
		WORD	nReturnMe	= 0;

		// On most machines, the processing is done LittleEndian
		if ( m_eByteOrder == ByteOrderLittleEndian )
		{
#ifdef _X86_
			nReturnMe	= *((WORD*)(GetCurrentBuffer()));
			SkipAhead( sizeof(WORD) );
#else
			BYTE* pData	= ( (BYTE*)(&nReturnMe) );

			pData[0]	= ReadByte();
			pData[1]	= ReadByte();
#endif
		}
		else
		{
#ifdef _X86_
			BYTE* pData	= ( (BYTE*)(&nReturnMe) );

			pData[1]	= ReadByte();
			pData[0]	= ReadByte();
#else
			nReturnMe	= *((WORD*)(GetCurrentBuffer()));
			SkipAhead( sizeof(WORD) );
#endif
		}

		// Return the result
		return nReturnMe;
	}

	inline UINT CPrintJobBuffer::ReadUInt32()
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );
		_ASSERTE( m_dwLength != 0 );
		_ASSERTE( GetBytesRemaining() >= sizeof(UINT) );
		_ASSERTE( m_eByteOrder != ByteOrderUnknown );

		// Declare the variables
		UINT	nReturnMe	= 0;
		BYTE*	pData		= ( (BYTE*)(&nReturnMe) );

		// On most machines, the processing is done LittleEndian
		if ( m_eByteOrder == ByteOrderLittleEndian )
		{
#ifdef _X86_
			nReturnMe	= *((UINT*)(GetCurrentBuffer()));
			SkipAhead( sizeof(UINT) );
#else
			BYTE* pData	= ( (BYTE*)(&nReturnMe) );

			pData[0]	= ReadByte();
			pData[1]	= ReadByte();
			pData[2]	= ReadByte();
			pData[3]	= ReadByte();
#endif
		}
		else
		{
#ifdef _X86_
			BYTE* pData	= ( (BYTE*)(&nReturnMe) );

			pData[3]	= ReadByte();
			pData[2]	= ReadByte();
			pData[1]	= ReadByte();
			pData[0]	= ReadByte();
#else
			nReturnMe	= *((UINT*)(GetCurrentBuffer()));
			SkipAhead( sizeof(UINT) );
#endif
		}

		// Return the result
		return nReturnMe;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTJOBBUFFER_H__94E5AF23_7384_49D5_B456_0E4EB38D7D05__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobBuffer.h
///////////////////////////////////////////////////////////////////////////////////////////////////////