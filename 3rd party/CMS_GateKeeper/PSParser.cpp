///////////////////////////////////////////////////////////////////////////////////////////////////////
// PSParser.cpp: implementation of the CPSParser class.
//
// By Tom Gibson (term@xerosoft.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PSParser.h"
#include <stdio.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	PS_DSC_HEADER			"%!PS-Adobe-"
#define	PS_DSC_TAG				"%%"
#define	PS_DSC_ATEND			"(atend)"

#define PS_COLOR				"setcolor"
#define PS_COPIESHACK			"/NumCopies"
#define PS_COPIESHACK2			"%%Requirements: numcopies("

#define PS_DSC_NONPPDFEATURE	PS_DSC_TAG "BeginNonPPDFeature:"
#define PS_DSC_BEGINFEATURE		PS_DSC_TAG "BeginFeature:"
#define PS_DSC_PAGE				PS_DSC_TAG "Page:"
#define PS_DSC_PAGES			PS_DSC_TAG "Pages:"
#define PS_DSC_PAGETRAILER		PS_DSC_TAG "PageTrailer"

#define PS_DSC_NUMCOPIES		"NumCopies"
#define PS_DSC_NUMCOPIES2		"*NumCopies"

#define PS_DSC_DUPLEX_NOTUMBLE	"DuplexNoTumble"
#define PS_DSC_DUPLEX_TUMBLE	"DuplexTumble"

#define PS_DSC_COLLATE			"*Collate"
#define PS_DSC_COLLATE2			"*XCollate"
#define PS_DSC_DUPLEX			"*Duplex"
#define PS_DSC_PAGESIZE			"*PageSize"
#define PS_DSC_PAGEREGION		"*PageRegion"
#define PS_DSC_RESOLUTION		"*Resolution"

#define PS_DSC_PAPER_LETTER		"Letter"
#define PS_DSC_PAPER_LEGAL		"Legal"
#define PS_DSC_PAPER_A4			"A4"

#define	MAX_LENGTH_BUFFER		1024

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Known paper size matchings
///////////////////////////////////////////////////////////////////////////////////////////////////////

const PaperSizeInfo	CPSParser::PSFormNames[] =
{
	{ "Letter",		DMPAPER_LETTER					},
	{ "Legal",		DMPAPER_LEGAL					},
	{ "A4",			DMPAPER_A4						},
	//{ "LEDGER",		DMPAPER_LEDGER					},
	{ "A3",			DMPAPER_A3						},
	{ "B4",			DMPAPER_B4						},
	{ "B5",			DMPAPER_B5						},
	{ "ISOB5",		DMPAPER_B5						},
	{ "Executive",	DMPAPER_EXECUTIVE				},
	{ "Env10",		DMPAPER_ENV_10					},
	{ "EnvMonarch",	DMPAPER_ENV_MONARCH				},
	{ "EnvDl",		DMPAPER_ENV_DL					},
	{ "EnvC5",		DMPAPER_ENV_C5					},
	//{ "JPOST",		DMPAPER_JAPANESE_POSTCARD		},
	{ "A5",			DMPAPER_A5						},
#if(WINVER >= 0x0500)
	//{ "JPOSTD",		DMPAPER_DBL_JAPANESE_POSTCARD	},
#endif
	//{ "JISEXEC",					},
	{ "EnvB5",		DMPAPER_ENV_B5				},
	//{ "",			DMPAPER_CSHEET				},
	//{ "",			DMPAPER_DSHEET				},
	//{ "",			DMPAPER_ESHEET				},
	//{ "",			DMPAPER_LETTERSMALL			},
	//{ "",			DMPAPER_STATEMENT			},
	//{ "",			DMPAPER_A4SMALL				},
	{ "Folio",		DMPAPER_FOLIO				},
	//{ "",			DMPAPER_10X14				},
	//{ "",			DMPAPER_11X17				},
	//{ "",			DMPAPER_NOTE				},
	{ "Env9",		DMPAPER_ENV_9				},
	{ "Env11",		DMPAPER_ENV_11				},
	{ "Env12",		DMPAPER_ENV_12				},
	{ "Env14",		DMPAPER_ENV_14				},
	{ "EnvC65",		DMPAPER_ENV_C65				},
	{ "EnvC6",		DMPAPER_ENV_C6				},
	{ "EnvB4",		DMPAPER_ENV_B4				},
	{ "EnvB6",		DMPAPER_ENV_B6				},
	//{ "",			DMPAPER_ENV_ITALY			},
	{ "Tabloid",	DMPAPER_TABLOID				},
	//{ "",			DMPAPER_ENV_PERSONAL		},
	//{ "",			DMPAPER_FANFOLD_US			},
	//{ "",			DMPAPER_FANFOLD_STD_GERMAN	},
	//{ "",			DMPAPER_FANFOLD_LGL_GERMAN	},
	//{ "",			DMPAPER_STATEMENT			},
	{ NULL,			0								}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

CPSParser::CPSParser()
{
	// Allocate memory for our buffers
	m_szLine	= new char[MAX_LENGTH_BUFFER];
	m_szCommand	= new char[MAX_LENGTH_BUFFER];
	m_szParam1	= new char[MAX_LENGTH_BUFFER];
	m_szParam2	= new char[MAX_LENGTH_BUFFER];
	m_szParam3	= new char[MAX_LENGTH_BUFFER];
	m_szParam4	= new char[MAX_LENGTH_BUFFER];
	m_szParam5	= new char[MAX_LENGTH_BUFFER];

	// Provide default variables for our member functions
	Reset();
}

CPSParser::~CPSParser()
{
	// Free our buffers memory
	delete [] m_szLine;
	delete [] m_szCommand;
	delete [] m_szParam1;
	delete [] m_szParam2;
	delete [] m_szParam3;
	delete [] m_szParam4;
	delete [] m_szParam5;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Member Variable Reset Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Gives our member variables default values
void CPSParser::Reset()
{
	m_nPageTrailerCount	= 0;
	m_nPagePageCount	= 0;
	m_bFoundDSCPages	= false;
	m_bFoundHeader		= false;
	m_bFoundCopies		= false;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Parse Operation
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Parses all the lines out of the buffer and calls the handler functions for them
bool CPSParser::Parse(CPrintParserData* pParserData)
{
	__try
	{
		LPSTR		szBuffer		= NULL;
		DWORD		dwBufferLength	= 0;
		UINT		nLineStart		= 0;
		UINT		nLineEnd		= 0;
		bool		bContinueLoop	= true;
		bool		bFoundHeader	= false;

		// Reset member variables
		Reset();

		// Check assumptions
		_ASSERTE( pParserData != NULL );

		szBuffer		= (LPSTR)pParserData->GetBuffer();
		dwBufferLength	= pParserData->GetBufferLength();

		// Finish checking assumptions
		_ASSERTE( szBuffer != NULL );
		_ASSERTE( dwBufferLength != 0 );

		// Main parse loop
		while ( bContinueLoop )
		{
			// Get the offset to the Start of the next line
			nLineStart = nLineEnd;
			while ( (szBuffer[nLineStart] == '\r' ||
					szBuffer[nLineStart] == '\n') &&
					nLineStart < dwBufferLength )
			{
				nLineStart++;
			}

			// Get the offset to the End of the next line
			nLineEnd = nLineStart;
			while (	(szBuffer[nLineEnd] != '\r' &&
					szBuffer[nLineEnd] != '\n') &&
					nLineEnd < dwBufferLength )
			{
				nLineEnd++;
			}

			// Check if we are at the end of the buffer
			if ( nLineEnd + 1 >= dwBufferLength )
			{
				bContinueLoop = false;
			}

			// Call data handlers if the resulting line will not exceed the
			// MAX_LENGTH_BUFFER and the line with be atleast 1 character in length.
			if ( (nLineEnd - nLineStart < MAX_LENGTH_BUFFER) && (nLineEnd - nLineStart >= 1) )
			{
				::CopyMemory( m_szLine, szBuffer + nLineStart, nLineEnd - nLineStart );
				m_szLine[nLineEnd - nLineStart] = NULL;
				HandleLine( m_szLine, szBuffer, nLineStart, pParserData );
			}
		}

		// If color was not set to enabled by the parser it is assumed to be b/w
		if ( m_bFoundHeader )
		{
			if ( pParserData->IsColorSet() == false )
			{
				pParserData->SetColor( DMCOLOR_MONOCHROME );
			}
		}

		// If there was no explicit page count set, we have to use 1 of our 2 other counting methods
		if ( !m_bFoundDSCPages )
		{
			if ( m_nPageTrailerCount > 0 )
			{
				pParserData->SetPageCount( m_nPageTrailerCount );
			}
			else if ( m_nPagePageCount > 0 )
			{
				pParserData->SetPageCount( m_nPagePageCount );
			}
		}

		// Added: 6/16/00.  Temp fix?
		// If copies is less than or equal to 0, change it to 1
		if ( ( pParserData->IsCopiesSet() ) && ( pParserData->GetCopies() <= 0 ) )
		{
			pParserData->SetCopies( 1 );
		}

		return ( m_bFoundHeader );
	}
	__except ( 1 )
	{
		// if this gets called, we should safely be able to ignore the exception
		// and return false for the Parse member function.  it should still be noted
		// if this ever gets called so it can be fixed.  this will hopefully never get
		// triggered.
		DEBUG_TEXT( _T( "An exception was cought in CPSParser::Parse(). SIGH!\r\n" ) );

		return false;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Parser Data Handlers
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Handler for raw lines
bool CPSParser::HandleLine( LPCSTR szLine, LPCSTR szBuffer, int nCurrentBufferOffset, 
						   CPrintParserData* pParserData )
{
	bool	bReturn		= false;
	int		iParamCount	= 0;
	LPSTR	szParams[]	= {m_szParam1, m_szParam2, m_szParam3, m_szParam4, m_szParam5};

	// Check assumptions
	_ASSERTE( szLine != NULL );
	_ASSERTE( pParserData != NULL );

	// Remove any binary characters at the begining of a line
	while ( ((szLine[0] >= 128) ||
			(szLine[0] > 0 && szLine[0] < 32)) &&
			(szLine[0] != '\r' || szLine[0] != '\n') )
	{
		szLine += 1;
	}

	// ** HACK ** hack for the Xerox DC 220/230/332/340 to get the # of copies
	if ( (strncmp( szLine, PS_COPIESHACK2, (sizeof(PS_COPIESHACK2)-1)) == 0) 
			&& m_bFoundCopies == false )
	{
		// Get the # of copies by going to the point of the string that contains the count
		pParserData->SetCopies( atoi( szLine + (sizeof(PS_COPIESHACK2)-1)) );
		m_bFoundCopies = true;
		return true;
	}
	// END HACK

	// Check if the current line is a DSC one
	if ( strncmp( szLine, PS_DSC_TAG, 2 ) == 0 )
	{
		// Parse the command and params from the current line
		iParamCount = sscanf( m_szLine, "%s%s%s%s%s%s", m_szCommand, m_szParam1, m_szParam2, 
			m_szParam3, m_szParam4, m_szParam5 ) - 1;

		// Check if parse failed.
		if ( iParamCount < 0 )
		{
			bReturn = false;
		}
		else
		{
			// Call DSC line handler
			bReturn = HandleDSC( m_szCommand, szParams, iParamCount, pParserData );
		}
	}
	// It's some other type of line. Not DSC.
	else
	{
		// Check if the current line is a DSC header
		if ( strstr( m_szLine, PS_DSC_HEADER ) != NULL )
		{
			m_bFoundHeader	= true;
			bReturn			= true;
		}
		// Check to see if the line uses setcolor
		else if ( strstr( m_szLine, PS_COLOR ) != NULL )
		{
			pParserData->SetColor( DMCOLOR_COLOR );
		}
		// Check for copies *HACK*
		else if ( (m_bFoundCopies == false) && (strstr( m_szLine, PS_COPIESHACK ) != NULL) )
		{
			// Make a copy of the buf pointer and offset since we are changing them
			LPCSTR szTempBuffer = szBuffer + nCurrentBufferOffset;
			int nTempBufferOffset;
			nTempBufferOffset = nCurrentBufferOffset;
			// keep going back looking for { while making sure we stay in the buffer
			while ( szTempBuffer[0] != '{' )
			{
				// decrement the pointer and the offset pos
				szTempBuffer--;
				nTempBufferOffset--;

				// Stop the loop if at the beggining of the buffer
				if ( nTempBufferOffset < 0 )
				{
					break;
				}
			}
			// if we found the '{' char get the # of copies after it
			if ( szTempBuffer[0] == '{' )
			{
				pParserData->SetCopies( atoi( szTempBuffer + 1 ) );
			}
		}
	}

	return bReturn;
}

// Handler for DSC comment lines
bool CPSParser::HandleDSC(LPCSTR szCommand, LPSTR* szParams, int iParamCount, CPrintParserData* pParserData)
{
	// Check assumptions
	_ASSERTE( szCommand != NULL );
	_ASSERTE( szParams != NULL );

	if ( iParamCount >= 1)
	{
		if ( stricmp( szParams[0], PS_DSC_ATEND ) == 0 )
		{
			// this just means that the file will give us a value later
			return true;
		}
	}

	// this gets: Copies
	if ( stricmp( szCommand, PS_DSC_NONPPDFEATURE ) == 0 )
	{
		if ( iParamCount >= 2 )
		{
// Modified by Darrin W. Cullop on April 3, 2001
// Needed to check for new variation on PS_DSC_NUMCOPIES
// Which is defined as PS_DSC_NUMCOPIES2
			if ( stricmp( szParams[0], PS_DSC_NUMCOPIES ) == 0 ||
					  stricmp( szParams[0], PS_DSC_NUMCOPIES2 ) == 0 )
			{
				_ASSERTE( m_bFoundCopies == false );
				pParserData->SetCopies( atoi( szParams[1] ) );
				m_bFoundCopies = true;
			}
// End Modification
		}
	}
	// this gets: Duplex, Collate, PaperSize
	else if ( stricmp( szCommand, PS_DSC_BEGINFEATURE ) == 0 )
	{
		if ( iParamCount >= 2 )
		{
		//	DuplexNoTumble = DMDUP_VRTICAL		= LONG ENDGE BINDING
		//	DuplexTumble   = DMDUP_HORIZONTAL	= SHORT EDGE BINDING
			if ( stricmp( szParams[0], PS_DSC_DUPLEX ) == 0 )
			{
				if ( stricmp( szParams[1], PS_DSC_DUPLEX_NOTUMBLE ) == 0 )
				{
					pParserData->SetDuplex( DMDUP_VERTICAL );
				}
				else if ( stricmp( szParams[1], PS_DSC_DUPLEX_TUMBLE ) == 0 )
				{
					pParserData->SetDuplex( DMDUP_HORIZONTAL );
				}
				else
				{
					pParserData->SetDuplex( DMDUP_SIMPLEX );
				}
			}
			else if ( stricmp( szParams[0], PS_DSC_COLLATE ) == 0 ||
					  stricmp( szParams[0], PS_DSC_COLLATE2 ) == 0 )
			{
				pParserData->SetCollate( stricmp( szParams[1], "True" ) == 0 ? 
					DMCOLLATE_TRUE : DMCOLLATE_FALSE );
			}
			// feature goes by different names on some drivers...
			else if ( stricmp( szParams[0], PS_DSC_PAGESIZE )   == 0 ||
					  stricmp( szParams[0], PS_DSC_PAGEREGION ) == 0   )
			{
				pParserData->SetPaperSizeString( PSFormNames, szParams[1] );
			}
			else if ( stricmp( szParams[0], PS_DSC_RESOLUTION ) == 0 )
			{
				pParserData->SetPrintQuality( atoi( szParams[1] ) );
			}
// Modified by Darrin W. Cullop on April 3, 2001
// Needed to check for new variation on PS_DSC_NUMCOPIES
// Which is defined as PS_DSC_NUMCOPIES2
			else if ( stricmp( szParams[0], PS_DSC_NUMCOPIES ) == 0 ||
					  stricmp( szParams[0], PS_DSC_NUMCOPIES2 ) == 0 )
			{
				_ASSERTE( m_bFoundCopies == false );
				pParserData->SetCopies( atoi( szParams[1] ) );
				m_bFoundCopies = true;
			}
// End Modification
		}
		DEBUG_ELSE_TEXT( _T("Parameter Count: %d\n"), iParamCount );
	}
	// this gets: Pages
	else if ( stricmp( szCommand, PS_DSC_PAGES ) == 0 )
	{
		if ( iParamCount >= 1 )
		{
			m_bFoundDSCPages = true;
			pParserData->SetPageCount( atoi( szParams[0] ) );
		}
	}
	// increment page count if we havn't got a Pages: command.
	else if ( stricmp( szCommand, PS_DSC_PAGETRAILER ) == 0 )
	{
		m_nPageTrailerCount++;
	}
	else if ( stricmp( szCommand, PS_DSC_PAGE ) == 0 )
	{
		m_nPagePageCount++;
	}
	else
	{
		// return FALSE if we didn't handle the particular command
		// this isn't a big deal. we don't handle most.
		return false;
	}

	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PSParser.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
