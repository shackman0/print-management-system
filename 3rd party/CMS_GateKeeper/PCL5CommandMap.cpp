//////////////////////////////////////////////////////////////////////
// PCL5CommandMap.cpp: implementation of the CPCL5CommandMap class.
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL5CommandMap.h"

//////////////////////////////////////////////////////////////////////
// Debugging Information
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
//#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CPCL5CommandMap::CPCL5CommandMap( const UINT& nHashTableSize )
{
	m_ppHashTable		= NULL;
	m_nEntryCount		= 0;
	m_nHashTableSize	= 0;

	InitHashTable( nHashTableSize );
}

CPCL5CommandMap::~CPCL5CommandMap()
{
	RemoveAll();
}

//////////////////////////////////////////////////////////////////////
// Map Operations
//////////////////////////////////////////////////////////////////////

void CPCL5CommandMap::SetAt( const DWORD& dwKey, const PCL5Operator& fpOperator )
{
	(*this)[dwKey] = fpOperator;
}

void CPCL5CommandMap::RemoveAll()
{
	if ( m_ppHashTable != NULL )
	{
		// For each bit bucket in the table
		for ( unsigned register nCounter = 0 ; nCounter < m_nHashTableSize ; nCounter++ )
		{
			CMapEntry*	pNext	= NULL;
			CMapEntry*	pEntry;

			// Delete the linked list of entries in this bit bucket
			for ( pEntry = m_ppHashTable[nCounter] ; pEntry != NULL ; pEntry = pNext )
			{
				pNext = pEntry->GetNext();
				delete pEntry;
			}
		}

		// Free up the hash table itself
		delete [] m_ppHashTable;
		m_ppHashTable = NULL;
	}

	m_nEntryCount = 0;
}


//////////////////////////////////////////////////////////////////////
// Iteration Operations
//////////////////////////////////////////////////////////////////////

CPCL5CommandMap::CMapEntry*	CPCL5CommandMap::GetStartPosition()
{
	CPCL5CommandMap::CMapEntry*	pReturnMe	= NULL;

	// Loop through each entry in the hash table
	for ( unsigned register nCounter = 0 ; ( (nCounter < m_nHashTableSize) && (pReturnMe == NULL) ) ; nCounter++ )
	{
		// If it's not empty, use that as the starting point
		if ( m_ppHashTable[nCounter] != NULL )
		{
			pReturnMe = m_ppHashTable[nCounter];
		}
	}

	// Return the starting point (or NULL)
	return pReturnMe;
}

void CPCL5CommandMap::GetNextEntry( CMapEntry*& pEntry, DWORD& dwKey, PCL5Operator& fpOperator )
{
	// Proceed if the supplied entry pointer is valid
	if ( pEntry != NULL )
	{
		// Set the values from that entry
		dwKey		= pEntry->GetKey();
		fpOperator	= pEntry->GetValue();

		// Get the next entry
		LookupNextEntry( pEntry );
	}
}

void CPCL5CommandMap::LookupNextEntry( CMapEntry*& pEntry )
{
	CPCL5CommandMap::CMapEntry*	pNextEntry	= NULL;

	// Proceed if the supplied entry pointer is valid
	_ASSERTE( pEntry != NULL );

	// If there is another item in this entry's list, use that for the next
	if ( pEntry->GetNext() != NULL )
	{
		pNextEntry	= pEntry->GetNext();
	}
	else
	{
		// Otherwise, search for the next item starting with the
		// the location in the hash table just after this entry
		unsigned register nCounter;

		for ( nCounter = pEntry->GetHashValue()+1 ; ( (nCounter < m_nHashTableSize) && (pNextEntry == NULL) ) ; nCounter++ )
		{
			// If it's not empty, use that as the next entry
			if ( m_ppHashTable[nCounter] != NULL )
			{
				pNextEntry = m_ppHashTable[nCounter];
			}
		}
	}

	// Set the entry point to be the next entry (or NULL)
	pEntry = pNextEntry;
}

//////////////////////////////////////////////////////////////////////
// Operators
//////////////////////////////////////////////////////////////////////

PCL5Operator&	CPCL5CommandMap::operator[]( const DWORD& dwKey )
{
	UINT		nHash;
	CMapEntry*	pEntry;

	if ( (pEntry = GetEntry(dwKey,nHash)) == NULL )
	{
		if ( m_ppHashTable == NULL )
		{
			InitHashTable( m_nHashTableSize );
		}

		// Add the new entry
		pEntry = new CMapEntry( dwKey, nHash, m_ppHashTable[nHash] );

		// Drop it into the appropriate bit-bucket
		m_ppHashTable[nHash] = pEntry;

		// Increment the entry count
		m_nEntryCount++;
	}
	return pEntry->GetValue();
}

//////////////////////////////////////////////////////////////////////
// Internal Operations
//////////////////////////////////////////////////////////////////////

void CPCL5CommandMap::InitHashTable( const UINT& nHashTableSize )
{
	// Check for valid conditions
	_ASSERTE( m_nEntryCount == 0 );
	_ASSERTE( nHashTableSize > 0 );

	if ( m_ppHashTable != NULL )
	{
		delete [] m_ppHashTable;
	}

	m_ppHashTable = new CMapEntry* [nHashTableSize];
	::ZeroMemory( m_ppHashTable, (sizeof(CMapEntry*) * nHashTableSize) );
	m_nHashTableSize = nHashTableSize;
}

CPCL5CommandMap::CMapEntry* CPCL5CommandMap::GetEntry( const DWORD& dwKey, UINT& nHashValue ) const
{
	CMapEntry*	pReturnMe	= NULL;

	// Find the hash value for this key
	nHashValue = dwKey % m_nHashTableSize;

	if ( m_ppHashTable != NULL )
	{
		CMapEntry* pEntry;

		// Search the bit bucket for the desired entry
		for ( pEntry = m_ppHashTable[nHashValue] ; ( (pEntry != NULL) && (pReturnMe == NULL) ) ; pEntry = pEntry->GetNext() )
		{
			if ( pEntry->GetKey() == dwKey )
			{
				pReturnMe = pEntry;
			}
		}
	}

	return pReturnMe;
}

//////////////////////////////////////////////////////////////////////
// End of PCL5CommandMap.cpp
//////////////////////////////////////////////////////////////////////