;/////////////////////////////////////////////////////////////////
;// Languages
;/////////////////////////////////////////////////////////////////

LanguageNames=(Spanish=0x0c0a:MSG00c0a)

;/////////////////////////////////////////////////////////////////
;// Categories
;/////////////////////////////////////////////////////////////////

MessageIdTypedef = WORD

MessageID=0x0001
SymbolicName=EVT_CAT_GENERAL
Language=English
General
.
Language=Spanish
General
.

MessageID=
SymbolicName=EVT_CAT_INTERNAL_ERROR
Language=English
System
.
Language=Spanish
Sistema
.

;/////////////////////////////////////////////////////////////////
;// Messsages
;/////////////////////////////////////////////////////////////////

MessageIdTypedef = DWORD

;/////////////////////////////////////////////////////////////////
;// General

MessageID=0x0100
SymbolicName=EVT_MSG_OPP_INVALID_PARAM
Language=English
OpenPrintProcessor received an invalid parameter.
.
Language=Spanish
OpenPrintProcessor recibi� un par�metro invalido.
.

MessageId=
SymbolicName=EVT_MSG_OPP_INVALID_DATATYPE
Language=English
OpenPrintProcessor received an invalid datatype.
.
Language=Spanish
OpenPrintProcessor recibi� un tipo de dato invalido.
.

MessageId=
SymbolicName=EVT_MSG_OPP_PRINTER_OPEN_FAIL
Language=English
OpenPrintProcessor could not open the specified printer because of the following error: %1
.
Language=Spanish
OpenPrintProcessor no pudo abrir la impresora especificada por el siguiente error: %1
.

MessageId=
SymbolicName=EVT_MSG_PPP_INVALID_PARAM
Language=English
PrintDocumentOnPrintProcessor received an invalid parameter.
.
Language=Spanish
PrintDocumentOnPrintProcessor recibi� un par�metro invalido.
.

MessageId=
SymbolicName=EVT_MSG_PPP_INVALID_DATATYPE
Language=English
PrintDocumentOnPrintProcessor received an invalid datatype.
.
Language=Spanish
PrintDocumentOnPrintProcessor recibi� un tipo de dato invalido.
.

MessageId=
SymbolicName=EVT_MSG_SERVER_FAIL_UNKNOWN
Language=English
Could not authorize the print job because of the following error: "%1".  The print job will be cancelled.
.
Language=Spanish
No podr�a autorizar el trabajo de impresi�n porque el siguiente error: "%1".  El trabajo de impresi�n ser� cancelado.
.

MessageId=
SymbolicName=EVT_MSG_SERVER_FAIL_COM
Language=English
Could not authorize the print job because of the following error: "%1".  The print job will be cancelled.
.
Language=Spanish
No podr�a autorizar el trabajo de impresi�n porque el siguiente error: "%1".  El trabajo de impresi�n ser� cancelado.
.

MessageId=
SymbolicName=EVT_MSG_SERVER_NAME_MISSING
Language=English
The server has not been specified.  Please make sure that the Print Server software has been properly installed, and that all users have read access to that section of the registry.
.
Language=Spanish
El servidor no se ha especificado. Por favor asegurase que el software del servidor de impresi�n fue instalado correctamente y que todos los usuarios tienen derechos de lectura a esa secci�n de del registro.
.

MessageId=
SymbolicName=EVT_MSG_SERVER_FAIL_OUTOFMEMORY
Language=English
Could not authorize the print job because it is too large for the spooler to handle.  Increase the available resources before printing large jobs.
.
Language=Spanish
No podr�a autorizar el trabajo de impresi�n porque es demasiado grande.  Aumente los recursos disponibles antes de trabajos grandes de impresi�n.
.

MessageId=
SymbolicName=EVT_MSG_SERVER_FAIL_ACCESSDENIED
Language=English
Could not authorize the print job because of the following error: "%1".  This is usually caused by the Print Tracking service not being started.  The print job will be cancelled.
.
Language=Spanish
No podr�a autorizar el trabajo de impresi�n porque el siguiente error: "%1".  Generalmente, esto es causada por el servicio de Print Tracking no es comenzado.  El trabajo de impresi�n ser� cancelado.
.

MessageID=
SymbolicName=EVT_MSG_CAUGHT_EXCEPTION
Language=English
The print processor handled exception %1.  The print job will be cancelled.
.
Language=Spanish
El procesador de la impresi�n manej� la anomal�a %1.  El trabajo de impresi�n ser� cancelado.
.

MessageID=
SymbolicName=EVT_MSG_CAUGHT_CANCELPRINT_EXCEPTION
Language=English
The print processor handled exception %1 while cancelling the print job!  If the print queue is not releasing print jobs, please restart the service.
.
Language=Spanish
El procesador de la impresi�n manej� la anomal�a %1 mientras que cancelaba el trabajo de impresi�n!  Si la coleta de impresi�n no deja para ir los trabajos de impresi�n, recomience el servicio.
.

MessageID=
SymbolicName=EVT_MSG_UPDATE_PRINTJOB_SUCCESS
Language=English
Successfully updated page count.
.
Language=Spanish
Exitosa actualizaci�n de conteo de paginas.
.

MessageID=
SymbolicName=EVT_MSG_UPDATE_PRINTJOB_FAILED
Language=English
Failed to updated page count.
.
Language=Spanish
Fallo en actualizar el conteo de paginas.
.
