///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobRequest.cpp: implementation of the CPrintJobRequest class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PrintJobRequest.h"
#include "RegistrySettings.h"

#include "..\GateKeeper\Cpp\GateKeeper.h"            // fws

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#define	PARSE_DEBUG
#endif

#ifdef USE_PARSE_DEBUG
	#define DEBUG_PARSE_TEXT		DEBUG_OBJ_TEXT
#else
	#define	DEBUG_PARSE_TEXT		((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

CPrintJobRequest::CPrintJobRequest()
{
	HRESULT hr;

	// Initialize COM ( BUT DO NOT THROW AN EXCEPTION ON FAILURE )
	hr = ::CoInitializeEx(NULL,COINIT_MULTITHREADED);
	DEBUG_COM_FAIL(hr);
	hr = ::CoInitializeSecurity(NULL, -1, NULL, NULL, RPC_C_AUTHN_LEVEL_NONE, 
		RPC_C_IMP_LEVEL_IMPERSONATE, NULL, EOAC_NONE, NULL);
	DEBUG_COM_FAIL(hr);

	// Set the default values
	m_nPrintJobID			= 0;
	m_pRawData				= NULL;
	m_bParsed				= false;
	m_bAlreadyTracked		= false;
	m_bAlwaysSendJobData	= false;
	m_bJobAborted			= false;
	m_dwPagesPrinted		= 0;
	m_dwCopiesPrinted		= 0;

	// Create the properties
	CreateProperties();
}

CPrintJobRequest::~CPrintJobRequest()
{
	// Destroy the raw data (if applicable)
	if ( m_pRawData != NULL )
	{
		delete m_pRawData;
	}

	// Destroy the properties
	DestroyProperties();

	// Uninitialize COM
	::CoUninitialize();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations for processing the request
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CPrintJobRequest::IsRequestGranted()
{
	VARIANT_BOOL	bSuccess = VARIANT_FALSE;
	CString			strServerName;
	HRESULT			hr;
	CPrintJob		spPrintJob;

	// Look up the server
	ReadServerName( strServerName );

	// Read other registry settings
	ReadRegistry();

	// Set the string property
	if ( strServerName.IsEmpty() == false )
	{
		SetProperty( PRINTJOB_PROP_PRINTER_PRINTJOBSERVER, strServerName );
	}

	// Set all the values
	SetPrinterProperties( GetPrinterInfo() );
	SetPrintJobProperties( GetPrintJobInfo() );
	SetParserProperties();

	// Create the object
	DEBUG_REQ_TEXT( _T("Creating PrintJob object\n") );
	hr = CoCreateInstance2( (strServerName.IsEmpty() ? NULL : strServerName.GetBuffer()), 
		__uuidof(PrintJob), &spPrintJob, true );
	strServerName.ReleaseBuffer();
	THROW_FAILED_EX( hr, spPrintJob );

	// Submit the request
	DEBUG_REQ_TEXT( _T("Submitting PrintJob Request\n") );
	hr = spPrintJob->SubmitRequestWithProperties( m_dwProperties, 
		m_pProperties, m_pValues, &bSuccess );
	THROW_FAILED_EX( hr, spPrintJob );

	// Make sure the PrintJobID is 0
	m_nPrintJobID = 0;

	// Check the result
	DEBUG_REQ_TEXT( _T("Request is %s!\n"), ((bSuccess == VARIANT_TRUE) ? 
		_T("Granted") : _T("Denied")) );
	if ( bSuccess == VARIANT_TRUE )
	{
		VARIANT	varJobID;

		// Initialize the variant
		::VariantInit( &varJobID );

		hr = spPrintJob->GetSingleProperty( _bstr_t(PRINTJOB_PROP_OTHER_PRINTERJOBID), &varJobID );
		if ( hr == S_OK )
		{
			if ( varJobID.vt == VT_I4 )
			{
				m_nPrintJobID = varJobID.lVal;
				DEBUG_REQ_TEXT( _T("PrintJobID = %d\n"), m_nPrintJobID );
			}
			DEBUG_ELSE_TEXT( _T("PrintJobID is not a LONG value!\n") );
		}
		DEBUG_ELSE_TEXT( _T("Could not get PrinterJobID value!\n") );

		// Uninitialize the variant
		::VariantClear( &varJobID );

		// Throw failure
		THROW_FAILED_EX( hr, spPrintJob );

		// Return Success
		return TRUE;
	}
	else
	{
		// Return Failure
		return FALSE;
	}
}

void CPrintJobRequest::UpdateRequest()
{
	if ( m_bJobAborted )
	{
		DWORD	dwPagesPerSide = ( m_ParserData.IsPagesPerPageSet() ? 
				m_ParserData.GetPagesPerPage() : 1 );

		if ( m_dwCopiesPrinted )
		{
			if ( UpdateRequest(m_dwPagesPrinted, m_dwCopiesPrinted, dwPagesPerSide) == TRUE )
			{
				OnUpdateSuccess();
				DEBUG_TEXT( _T("Updated Request to %d Pages/%d Copies\n"), m_dwPagesPrinted, 
					m_dwCopiesPrinted );
			}
			else
			{
				OnUpdateFail();
				DEBUG_TEXT( _T("Failed to Update Request to %d Pages/%d Copies\n"), 
					m_dwPagesPrinted, m_dwCopiesPrinted );
			}
		}
		DEBUG_ELSE_TEXT( _T("Print Job was Aborted, but there are no new values: ")
							_T("%d Pages, %d Copies\n"), m_dwPagesPrinted, m_dwCopiesPrinted );
	}
}

BOOL CPrintJobRequest::UpdateRequest( const DWORD& dwPages, const DWORD& dwCopies, 
										const DWORD& dwPagesPerSide )
{
	BOOL bReturnMe	= FALSE;

	if ( m_nPrintJobID > 0 )
	{
		try
		{
			VARIANT_BOOL	bSuccess = VARIANT_FALSE;
			CString			strServerName;
			HRESULT			hr;
			CPrintJob		spPrintJob;

			// Look up the server
			ReadServerName( strServerName );

			// Reinitialize the properties
			DestroyProperties();
			CreateProperties();

			// Set the 4 values needed for an update
			SetProperty( PRINTJOB_PROP_OTHER_PRINTERJOBID,		m_nPrintJobID		);
			SetProperty( PRINTJOB_PROP_PRINTJOB_PAGECOUNT,		dwPages				);
			if ( dwPagesPerSide > 0 )
			{
				SetProperty( PRINTJOB_PROP_PRINTJOB_PAGESPERPAGE,	dwPagesPerSide	);
			}
			if ( dwCopies > 0 )
			{
				SetProperty( PRINTJOB_PROP_PRINTJOB_COPIES,			dwCopies		);
			}

			// Create the object
			DEBUG_REQ_TEXT( _T("Creating PrintJob object\n") );
			hr = CoCreateInstance2( (strServerName.IsEmpty() ? NULL : strServerName.GetBuffer()), 
				__uuidof(PrintJob), &spPrintJob, true );
			strServerName.ReleaseBuffer();
			THROW_FAILED_EX( hr, spPrintJob );

/* fws
			// Submit the request
			DEBUG_REQ_TEXT( _T("Submitting PrintJob Request\n") );
			hr = spPrintJob->SubmitRequestWithProperties( m_dwProperties, m_pProperties, 
					m_pValues, &bSuccess );
			THROW_FAILED_EX( hr, spPrintJob );
*/

// start fws changes

 // static STRING_UTF8 dbAddress_utf8      = "208.46.68.102";    // ip addr @cms - should be stored in configuration file
	static STRING_UTF8 dbAddress_utf8      = "208.20.204.154";   // ip addr @4peaks - should be stored in configuration file
	static STRING_UTF8 dbUserID_utf8       = "PT";               // should be stored in configuration file
    static STRING_UTF8 dbPasswd_utf8       = "ptpasswd";         // should be stored in configuration file
    static STRING_UTF8 installationID_utf8 = "120001";           // should be stored in configuration file
    static STRING_UTF8 clientAddress_utf8  = "192.168.1.152";    // how do i get this?

    PROPERTY_UNICODE properties_unicode[MAX_PROPERTY_COUNT];
  
    for ( DWORD nCounter = 0 ; nCounter < m_dwProperties ; nCounter++ )
	{
      properties_unicode[nCounter].key	  = m_pProperties[nCounter];
      properties_unicode[nCounter].value  = (LPWSTR)_bstr_t(m_pValues[nCounter]);
	}


	bool rc = GateKeeper::ReleasePrintJob (
                dbAddress_utf8,
                dbUserID_utf8,
                dbPasswd_utf8,
                installationID_utf8,
                clientAddress_utf8,
                properties_unicode,
  	            m_dwProperties );
		        
    bSuccess = rc;

// end fws changes

			// Get the return value
			bReturnMe = ( (bSuccess == VARIANT_TRUE) ? TRUE : FALSE );
		}
		catch ( ... )
		{
			DEBUG_REQ_TEXT( _T("Caught exception trying to update Print Job Request!\n") );
			bReturnMe = FALSE;
		}
	}
	DEBUG_ELSE_TEXT( _T("Cannot update print job because PrintJobID = 0\n") );

	DEBUG_REQ_TEXT( _T("Updating Print Job Request %s\n"), 
							(bReturnMe ? _T("Succeeded") : _T("Failed")) );
	return bReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Batch Property Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CPrintJobRequest::SetPrintJobProperties( JOB_INFO_2* lpJobInfo )
{
	// Check assumptions
	_ASSERTE( lpJobInfo != NULL );

	// Set the Print Job Properties
	SetProperty( PRINTJOB_PROP_WORKSTATION,				lpJobInfo->pMachineName	);
	SetProperty( PRINTJOB_PROP_PRINTER_DRIVER,			lpJobInfo->pDriverName	);
	SetProperty( PRINTJOB_PROP_PRINTJOB_DOCUMENT,		lpJobInfo->pDocument	);
	SetProperty( PRINTJOB_PROP_PRINTJOB_SUBMITTED,		&(lpJobInfo->Submitted)	);
	SetProperty( PRINTJOB_PROP_PRINTJOB_SIZE,			lpJobInfo->Size			);
}

void CPrintJobRequest::SetPrinterProperties( PRINTER_INFO_2* lpPrinterInfo )
{
	// Check assumptions
	_ASSERTE( lpPrinterInfo != NULL );

	CString	strServerName;

	// If the server name is blank, use the name of the current machine
	strServerName = lpPrinterInfo->pServerName;
	if ( strServerName.IsEmpty() )
	{
		::GetComputerName( strServerName );
	}

	// Set the properties
	SetProperty( PRINTJOB_PROP_PRINTER_NAME,			lpPrinterInfo->pPrinterName	);
	SetProperty( PRINTJOB_PROP_PRINTER_SERVERNAME,		strServerName				);
	SetProperty( PRINTJOB_PROP_PRINTER_SHARENAME,		lpPrinterInfo->pShareName	);
	SetProperty( PRINTJOB_PROP_PRINTER_COMMENT,			lpPrinterInfo->pComment		);
	SetProperty( PRINTJOB_PROP_PRINTER_PORT,			lpPrinterInfo->pPortName	);
}

void CPrintJobRequest::SetParserProperties()
{
	// Look at page counts
	if ( m_ParserData.IsPageCountSet() && (m_ParserData.GetPageCount() > 0) )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Page Count:     %d\n"), m_ParserData.GetPageCount() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_PAGECOUNT, long(m_ParserData.GetPageCount()) );
	}

	// Look at copies
	if ( m_ParserData.IsCopiesSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Copies:         %d\n"), m_ParserData.GetCopies() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_COPIES, m_ParserData.GetCopies() );
	}

	// Look at the Pages Per Page
	if ( m_ParserData.IsPagesPerPageSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("PagesPerPage:   %d\n"), m_ParserData.GetPagesPerPage() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_PAGESPERPAGE, long(m_ParserData.GetPagesPerPage()) );
	}

	// Look at the color settings
	if ( m_ParserData.IsColorSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Color:          %s\n"), ((m_ParserData.GetColor() == DMCOLOR_COLOR) 
							? _T("Color") : _T("Monochrome")) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_COLOR, bool(m_ParserData.GetColor() == DMCOLOR_COLOR) );
	}

	// Look at the orientation
	if ( m_ParserData.IsOrientationSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Orientation:    %s\n"), 
							((m_ParserData.GetOrientation() == DMORIENT_LANDSCAPE)
							? _T("Landscape") : _T("Portrait")) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_ORIENTATION, 
			bool(m_ParserData.GetOrientation() == DMORIENT_LANDSCAPE) );
	}

	// Look at the Collation
	if ( m_ParserData.IsCollateSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Collated:       %s\n"), ((m_ParserData.GetCollate() == DMCOLLATE_TRUE)
							? _T("True") : _T("False")) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_COLLATED, 
			bool(m_ParserData.GetCollate() == DMCOLLATE_TRUE) );
	}

	// Look at the PaperSize
	if ( m_ParserData.IsPaperSizeSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("PaperSize:      #%d\n"), m_ParserData.GetPaperSize()  );
		SetProperty( PRINTJOB_PROP_PAGESIZE_PAPERSIZE, m_ParserData.GetPaperSize() );
	}

	// Look at the PaperLength
	if ( m_ParserData.IsPaperLengthSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("PaperLength:    %d\n"), m_ParserData.GetPaperLength() );
		SetProperty( PRINTJOB_PROP_PAGESIZE_PAPERLENGTH, m_ParserData.GetPaperLength() );
	}

	// Look at the PaperWidth
	if ( m_ParserData.IsPaperWidthSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("PaperWidth:     %d\n"), m_ParserData.GetPaperWidth() );
		SetProperty( PRINTJOB_PROP_PAGESIZE_PAPERWIDTH, m_ParserData.GetPaperWidth() );
	}

	// Look at the Form Name
	if ( m_ParserData.IsFormNameSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("FormName:       %s\n"), m_ParserData.GetFormName() );
		SetProperty( PRINTJOB_PROP_PAGESIZE_FORM, m_ParserData.GetFormName() );
	}

	// Look at the Duplex
	if ( m_ParserData.IsDuplexSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Duplex:         %s\n"), ((m_ParserData.GetDuplex() == DMDUP_SIMPLEX)
							? _T("Simplex") : ((m_ParserData.GetDuplex() == DMDUP_HORIZONTAL)
							? _T("Duplexed Horizontally") : _T("Duplexed Vertically"))) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_DUPLEX, bool(m_ParserData.GetDuplex() != DMDUP_SIMPLEX) );
	}

	// Look at the Quality
	if ( m_ParserData.IsPrintQualitySet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("DPI:            %d\n"), m_ParserData.GetPrintQuality() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_QUALITY, m_ParserData.GetPrintQuality() );
	}

	// Look at the Sort
	if ( m_ParserData.IsSortSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Sort:           %s\n"), DEBUG_BOOL(m_ParserData.GetSort()) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_SORT, bool(m_ParserData.GetSort() != false) );
	}

	// Look at the Staple
	if ( m_ParserData.IsStapleSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("Staple:         %s\n"), DEBUG_BOOL(m_ParserData.GetStaple()) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_STAPLE, bool(m_ParserData.GetStaple() != false) );
	}

	// Look at the HolePunch
	if ( m_ParserData.IsHolePunchSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("HolePunch:      %s\n"), DEBUG_BOOL(m_ParserData.GetHolePunch()) );
		SetProperty( PRINTJOB_PROP_PRINTJOB_HOLEPUNCH, bool(m_ParserData.GetHolePunch() != false) );
	}

	// What is the default source?
	if ( m_ParserData.IsDefaultSourceSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("DefSource:      %d\n"), m_ParserData.GetDefaultSource() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_DEFAULTSOURCE, m_ParserData.GetDefaultSource() );
	}

	// Look at the Data Type
	if ( m_ParserData.IsDataTypeSet() )
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("DataType:       %s\n"), m_ParserData.GetDataType() );
		SetProperty( PRINTJOB_PROP_PRINTJOB_DATATYPE, m_ParserData.GetDataType() );
	}
	
	if ( m_bParsed == true )
	{
		// Flag the page count as being reliable
		SetProperty( PRINTJOB_PROP_STATUS_PAGECOUNT, true );

		// Check to see if the Raw Data is forced
		if ( m_bAlwaysSendJobData )
		{
			if ( m_pRawData != NULL )
			{
				SetProperty( m_pRawData );
			}
			DEBUG_ELSE_TEXT( _T("RAW data is forced, but there is no RAW data to send!\n") );
		}
	}
	else
	{
		// Display some info in debug mode
		DEBUG_PARSE_TEXT( _T("!!!! NOT Parsed\n") );
		if ( m_pRawData != NULL )
		{
			SetProperty( m_pRawData );
		}
		DEBUG_ELSE_TEXT( _T("Not Parsed and No Raw Data!\n") );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CPrintJobRequest::ReadServerName( CString& strServerName )
{
	CRegKey		hKey;
	LONG		lReturn;
	BOOL		bReturnMe	= FALSE;

	// Open the registry key
	lReturn = hKey.Open( HKEY_LOCAL_MACHINE, REG_KEY_BASE_PRINTSERVER, KEY_EXECUTE );
	if ( lReturn == ERROR_SUCCESS )
	{
		DWORD dwSize = MAX_SIZE_REGISTRY_SETTING;

		// Read the value
		lReturn = hKey.QueryValue( strServerName.GetBufferSetLength(dwSize), 
			REG_VALUE_PRINTSERVER_NAME_SERVER, &dwSize );
		strServerName.ReleaseBuffer();
		if ( lReturn == ERROR_SUCCESS )
		{
			// Set return value to be successful
			bReturnMe = TRUE;
		}
		DEBUG_ELSE();
	}
	DEBUG_ELSE();

	// If the server name could not be read, call the pure virtual function
	if ( bReturnMe == FALSE )
	{
		OnServerNameMissing();
	}

	// Return success or failure
	return bReturnMe;
}

void CPrintJobRequest::ReadRegistry()
{
	CRegKey		hKey;
	LONG		lReturn;
	DWORD		dwValue	= 0;

	// Open the registry key
	lReturn = hKey.Open( HKEY_LOCAL_MACHINE, REG_KEY_BASE_PRINTSERVER, KEY_EXECUTE );
	if ( lReturn == ERROR_SUCCESS )
	{
		// Read the value for REG_VALUE_PRINTSERVER_FORCE_JOBDATA
		lReturn = hKey.QueryValue( dwValue, REG_VALUE_PRINTSERVER_FORCE_JOBDATA );
		if ( lReturn == ERROR_SUCCESS )
		{
			if ( dwValue )
			{
				m_bAlwaysSendJobData = true;
			}
		}
		// Other Registry Settings Go Here
	}
	DEBUG_ELSE();
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobRequest.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////