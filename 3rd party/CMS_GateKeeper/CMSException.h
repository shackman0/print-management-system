///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMSException.h: interface for the CCMSException class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMSEXCEPTION_H__3684B6D3_649C_11D4_A461_00105A1C588D__INCLUDED_)
#define AFX_CMSEXCEPTION_H__3684B6D3_649C_11D4_A461_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include <eh.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Options
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
	#define CMS_UNKNOWN_EXCEPTION()				CCMSException(_T("Unknown"),0,THIS_FILE,__LINE__)
	#define CMS_EXCEPTION(type,code)			CCMSException((type),(code),THIS_FILE,__LINE__)
	#define WIN_EXCEPTION()						CWinException(THIS_FILE, __LINE__)
	#define WIN_EXCEPTION_CODE(code)			CWinException(THIS_FILE, __LINE__,(code))
#else
	#define CMS_UNKNOWN_EXCEPTION()				CCMSException(_T("Unknown"),0)
	#define CMS_EXCEPTION(type,code)			CCMSException((type),(code))
	#define WIN_EXCEPTION						CWinException
	#define WIN_EXCEPTION_CODE(code)			CWinException((code))
#endif

#ifdef USE_DEBUG_OUTPUT
	#ifdef _DEBUG
		#define DEBUG_CMS_EXCEPTION(e)			CCMSException::DebugException(e, THIS_FILE, __LINE__)
	#else
		#define DEBUG_CMS_EXCEPTION(e)			CCMSException::DebugException(e)
	#endif
#else
	#define DEBUG_CMS_EXCEPTION					((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define THROW_WIN_FAIL(f)						if ((f)==FALSE) throw WIN_EXCEPTION()
#define ELSE_THROW_WIN_FAIL()					else { throw WIN_EXCEPTION(); }

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSException Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CCMSException  
{
// Construction / Destruction
public:
#ifdef _DEBUG
	CCMSException( LPCTSTR lpszType, const DWORD& dwExceptionCode, LPCTSTR lpszFile, int nLine )
	{
		DEBUG_TEXT(	_T("!*!* CMSException Type: %s, Code: 0x%08x\n")
					_T("Source: %s (Line #%d)\n")
						, lpszType, dwExceptionCode
						, lpszFile, nLine );
#else
	CCMSException( LPCTSTR lpszType, const DWORD& dwExceptionCode )
	{
#endif
		_ASSERTE( lpszType != NULL );
		SetType(lpszType);
		SetCode(dwExceptionCode);
	}

	virtual ~CCMSException()									{ };

// Data Access
public:
	// Get Data
	const CString&		GetType() const								{ return m_strType; };
	const DWORD&		GetCode() const								{ return m_dwExceptionCode; };
	// Set Data
	void				SetType( LPCTSTR lpszType )					{ m_strType = lpszType; };
	void				SetCode( const DWORD& dwExceptionCode )		{ m_dwExceptionCode = dwExceptionCode; };

// Operations
public:
	virtual void		DisplayMessageBox() const
	{
		CString	strMessage;

		GetErrorMessage( strMessage );
		MessageBox( strMessage );
	}

	virtual	void		GetErrorMessage( CString& strMessage ) const
	{
		strMessage.Format( _T("Caught CMS Exception : %s - %d\n"), GetType(), GetCode() );
	}

	virtual CString	GetErrorMessage() const
	{
		CString strReturnMe;

		GetErrorMessage( strReturnMe );
		return strReturnMe;
	}

	void				MessageBox( LPCTSTR lpszMessage ) const
	{
		MessageBox( lpszMessage, GetMessageBoxTitle() );
	}

	static void			MessageBox( LPCTSTR lpszMessage, LPCTSTR lpszTitle )
	{
		DisplayMessage( lpszMessage, lpszTitle, MESSAGE_BOX_STYLE_ERROR );
	}

protected:
	virtual LPCTSTR		GetMessageBoxTitle() const					{ return CMS_PRODUCT_NAME _T(" CMSException"); };

// Debugging Operations
#ifdef USE_DEBUG_OUTPUT
public:
	static void	DebugException( const CCMSException& e, LPCTSTR lpszFile = NULL, int nLine = 0 )
	{
		// Display the error
		DEBUG_TEXT( _T("*** Caught CMS Exception (%s - %d)\n"), 
			e.GetType(), e.GetCode() );
		DEBUG_TEXT( _T("*** Info\t: %s\n"), e.GetErrorMessage() );

		// Display the extra info in the debug versions
		if ( lpszFile )
		{
			DEBUG_TEXT( _T("*** File\t: %s [Line #%d]\n"), lpszFile, nLine );
		}
	}
#endif

// Attributes
protected:
	CString		m_strType;
	DWORD		m_dwExceptionCode;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CWinException Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CWinException : public CCMSException
{
// Construction / Destruction
public:
#ifdef _DEBUG
	CWinException( LPCTSTR lpszFile, int nLine, const DWORD& dwErrorCode = 0 ) :
		CCMSException(_T("WinExcept"), ((dwErrorCode == 0) ? ::GetLastError() : dwErrorCode), lpszFile, nLine)
	{
		// Nothing to do
	}
#else
	CWinException( const DWORD& dwErrorCode = 0 ) :
		CCMSException(_T("WinExcept"), ((dwErrorCode == 0) ? ::GetLastError() : dwErrorCode))
	{
		// Nothing to do
	}
#endif

// Operations
public:
	virtual CString		GetErrorMessage() const								{ return CCMSException::GetErrorMessage(); };
	virtual	void		GetErrorMessage( CString& strMessage ) const
	{
		strMessage = ::LookupSystemError(m_dwExceptionCode);
	}

protected:
	virtual LPCTSTR	GetMessageBoxTitle() const							{ return CMS_PRODUCT_NAME _T(" Windows Error"); };

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CSystemException Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CSystemException : public CCMSException
{
// Attributes
private:
	LPVOID		m_lpAddress;

// Construction / Destruction
public:
	CSystemException( const DWORD& dwExceptionCode, LPVOID lpExceptionAddress ) :
#ifdef _DEBUG
		CCMSException( _T("System"), dwExceptionCode, NULL, 0 )
#else
		CCMSException( _T("System"), dwExceptionCode )
#endif
	{
		m_lpAddress = lpExceptionAddress;
	}

// Operations
public:
	virtual CString		GetErrorMessage() const							{ return CCMSException::GetErrorMessage(); };
	virtual	void		GetErrorMessage( CString& strMessage ) const
	{
		strMessage.Format(	_T("Caught Exception 0x%08x @ 0x%08x"),
							GetCode(),
							m_lpAddress );
	}

protected:
	virtual LPCTSTR	GetMessageBoxTitle() const							{ return CMS_PRODUCT_NAME _T(" System Exception"); };

// Static Functions
public:
	static void SystemExceptionTranslator( unsigned int nCode, EXCEPTION_POINTERS* info )
	{
   		throw CSystemException( (*(info->ExceptionRecord)).ExceptionCode, 
			(*(info->ExceptionRecord)).ExceptionAddress );
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CExceptionTranslator Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CExceptionTranslator
{
// Attributes
private:
	_se_translator_function		m_lpfOriginal;

// Construction / Destruction
public:
	CExceptionTranslator( _se_translator_function lpfNew = CSystemException::SystemExceptionTranslator )
	{
		_ASSERTE( lpfNew != NULL );
		m_lpfOriginal = _set_se_translator( lpfNew );
	}

	~CExceptionTranslator()
	{
		_set_se_translator( m_lpfOriginal );
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_CMSEXCEPTION_H__3684B6D3_649C_11D4_A461_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of CMSException.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
