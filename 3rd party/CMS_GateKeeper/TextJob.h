///////////////////////////////////////////////////////////////////////////////////////////////////////
// TextJob.h: interface for the CTextJob class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXTJOB_H__1B5AFEDB_9A45_11D4_A474_00105A1C588D__INCLUDED_)
#define AFX_TEXTJOB_H__1B5AFEDB_9A45_11D4_A474_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "TextJobException.h"
#include "PrintProcessor.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DATATYPE_TEXTJOB		_T("Text")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CTextJob Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CTextJob
{
// Construction / Destruction
public:
	CTextJob()
	{
		// Initialize values
		m_dwANSICodePage		= 0;
		m_nCharWidth			= 0;
		m_nCharHeight			= 0;
		m_nCharsPerLine			= 0;
		m_nLinesPerPage			= 0;
		m_lpszDocumentName		= NULL;
		m_pPrintProcessorData	= NULL;
		m_nTabSize				= 0;
		m_nCurrentColumn		= 0;
		m_nCurrentLine			= 0;
		m_nPageCount			= 0;
		m_bTranslateCR			= true;
		m_bTranslateLF			= true;
		m_hDC					= NULL;
		m_hOldFont				= NULL;
		m_hFont 				= NULL;
	}

	virtual ~CTextJob()
	{
		RestoreHDC();
	}

// Data Access
public:
	const UINT&		GetTabSize() const										{ return m_nTabSize; };
	const UINT&		GetPageCount() const									{ return m_nPageCount; };

// Operations
public:
	void			PrepareTextJob( LPTSTR lpszDocumentName, LPPRINTPROCESSORDATA pPrintProcessorData, VectorData& vdTextData );
	DWORD			PrintTextJob( VectorData& vdTextData );

private:
	void			RunJob( VectorData& vdTextData, const bool& bReallyPrint );
	bool			IsJobAborted() const;
	void			WaitWhilePaused();
	void			RestoreHDC();
	void			GetTextInfo();
	DWORD			GetLineToPrint( DWORD& dwFlags, LPBYTE& lpCurrentData, const LPBYTE lpEndData );
	void			GetTranslationSettings();
	void			ProcessWhiteSpace( const bool& bReallyPrint, bool& bInPage );

// Attributes
private:
	HDC						m_hDC;
	DWORD					m_dwANSICodePage;
	HFONT					m_hOldFont;
	HFONT					m_hFont;
	UINT					m_nCharWidth;
	UINT					m_nCharHeight;
	UINT					m_nCharsPerLine;
	UINT					m_nLinesPerPage;
	LPTSTR					m_lpszDocumentName;
	LPPRINTPROCESSORDATA	m_pPrintProcessorData;
	UINT					m_nTabSize;				// Maximum number of spaces for a tab
	UINT					m_nCurrentColumn;		// The current column
	UINT					m_nCurrentLine;			// The current line
	UINT					m_nPageCount;			// Number of pages
	bool					m_bTranslateCR;			// Treat CR like CRLF
	bool					m_bTranslateLF;			// Treat LF like CRLF
	CProcBuffer				m_PrintLine;			// Holds one line of text to print
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CTextJob Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline bool CTextJob::IsJobAborted() const
{
	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	return (m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_ABORTED);
}

inline void CTextJob::WaitWhilePaused()
{
	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	// If the print processor is paused, wait for it to be resumed
	if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
	{
		::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
	}
}

inline void CTextJob::RestoreHDC()
{
	if ( m_hDC != NULL )
	{
		if ( m_hOldFont != NULL )
		{
			::SelectObject( m_hDC, m_hOldFont );
			DEBUG_FAIL( ::DeleteObject(m_hFont) );
		}
	}
	m_hDC				= NULL;
	m_hOldFont			= NULL;
	m_hFont				= NULL;
	m_dwANSICodePage	= 0;
	m_nCharWidth		= 0;
	m_nCharHeight		= 0;
	m_nCharsPerLine		= 0;
	m_nLinesPerPage		= 0;
}

inline void CTextJob::ProcessWhiteSpace( const bool& bReallyPrint, bool& bInPage )
{
	// Check to see if it is greater than or equals because m_nCurrentLine is zero indexed
	while ( m_nCurrentLine >= m_nLinesPerPage )
	{
		if ( bReallyPrint )
		{
			// Really Printing

			// If a new page hasn't been started
			if ( bInPage == false )
			{
				// Start a new page and increment the page counter
				THROW_WIN_FAIL( ::StartPage(m_hDC) );
				m_nPageCount++;
			}
			THROW_WIN_FAIL( ::EndPage(m_hDC) );
		}
		else
		{
			// If a new page hasn't be started
			if ( bInPage == false )
			{
				// Increment the page counter
				m_nPageCount++;
			}
		}

		bInPage = false;
		m_nCurrentLine -= m_nLinesPerPage;
	}
	// DO NOT move to the beginning of the line!
	// That only should only be done on a carriage return
	// The Windows 2000 DDK's winprint moves to the beginning of a new line
	// but does so incorrectly.
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_TEXTJOB_H__1B5AFEDB_9A45_11D4_A474_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of TextJob.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
