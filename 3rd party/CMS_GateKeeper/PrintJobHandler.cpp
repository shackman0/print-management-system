///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobHandler.cpp: implementation of the CPrintJobHandler class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PrintProcessor.h"
#include "PrintJobHandler.h"
#include "PrintJobPropertyNames.h"
#include "RegistrySettings.h"
#include "ErrorLog.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	STANDARD_ACCOUNT_NAME		_T("SYSTEM")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

CPrintJobHandler::CPrintJobHandler()
{
	// Set the default values
	m_lpszDocumentName		= NULL;
	m_pPrintProcessorData	= NULL;
}

CPrintJobHandler::~CPrintJobHandler()
{
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Public Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CPrintJobHandler::HandlePrintJob( LPTSTR lpszDocumentName, 
									  LPPRINTPROCESSORDATA pPrintProcessorData )
{
	BOOL	bAllowToPrint	= FALSE;
	BOOL	bReturnMe		= FALSE;

	// Call the base class
	CPrintJobHandlerBase::HandlePrintJob( lpszDocumentName, pPrintProcessorData );

	DEBUG_OBJ_TEXT(	_T("New Print Job:\n")
					_T("    Job:              %s\n")
					_T("    Job ID:           %d\n")
					_T("    Printer:          %s\n")
					_T("    Document:         %s\n")
					_T("    Output:           %s\n")
					_T("    DataType:         %s\n")
					_T("    Copies:           %d\n")
			, lpszDocumentName
			, m_pPrintProcessorData->JobId
			, m_pPrintProcessorData->pPrinterName
			, m_pPrintProcessorData->pDocument
			, m_pPrintProcessorData->pOutputFile
			, m_pPrintProcessorData->pDatatype
			, m_pPrintProcessorData->Copies
		);

	// Revert to the system account
	m_Revert.Revert();

	try
	{
		// Look up all the values
		LoadAllProperties( m_pPrintProcessorData->hPrinter, 
			m_pPrintProcessorData->JobId, m_pPrintProcessorData->pDevMode );

		// Prepare the job to be printed
		PrepareJob();

		// Check the status of the job
		CheckJobStatus();

		// Check to see if the job has been cancelled
		if ( m_bJobAborted == false )
		{
			// Check to see if it is already tracked
			if ( m_bAlreadyTracked == false )
			{
				bAllowToPrint = SendRequest();
			}
			else
			{
				DEBUG_OBJ_TEXT( _T("Job was already tracked!\n") );
				bAllowToPrint = TRUE;
			}
		}
	}
	catch ( const CCMSException& e )
	{
		ReportError( EVT_MSG_SERVER_FAIL_COM, e.GetCode(), e.GetErrorMessage() );
		bAllowToPrint = FALSE;
	}

	DEBUG_OBJ_TEXT( _T("Allow To Print = %s\n"), (bAllowToPrint ? _T("True") : _T("False")) );

	if ( m_bJobAborted == false )
	{
		if ( bAllowToPrint == TRUE )
		{
			bReturnMe = PrintJob();
			DEBUG_OBJ_TEXT( _T("PrintJob = %s\n"), 
				(bReturnMe == TRUE) ? _T("Succeeded") : _T("Failed") );
			UpdateRequest();
		}
		else
		{
			// Not allowed to print, so cancel the job.
			DEBUG_OBJ_TEXT( _T("Cancelling Print Job!\n") );
			CPrinterAPI::SetPrintJobCommand( m_pPrintProcessorData->hPrinter, 
				m_pPrintProcessorData->JobId, JOB_CONTROL_CANCEL );

			// Return true because everything went as it was supposed to
			bReturnMe = TRUE;
		}
	}
	else
	{
		DEBUG_OBJ_TEXT( _T("PrintJob aborted; Returning Success\n") );
		bReturnMe = TRUE;
	}

	// Return Success or Failure
	return bReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations for processing the request
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CPrintJobHandler::SendRequest()
{
	BOOL	bReturnMe		= FALSE;

	// Prepare the request
	PrepareRequest();

	// Set the user/domain values
	SetUserProperties();

	// Correct any values that are needed
	if ( m_ParserData.IsCopiesSet() )
	{
		m_ParserData.SetCopies( GetMax(m_ParserData.GetCopies(), 
			(short)m_pPrintProcessorData->Copies) );
	}
	else
	{
		m_ParserData.SetCopies( (short)m_pPrintProcessorData->Copies );
	}
	if ( m_ParserData.IsPageCountSet() )
	{
		m_ParserData.SetPageCount( GetMax(GetPrintJobInfo()->TotalPages, 
			m_ParserData.GetPageCount()) );
	}
	else
	{
		m_ParserData.SetPageCount( GetPrintJobInfo()->TotalPages );
	}
	if ( !m_ParserData.IsPaperSizeSet() && !m_ParserData.IsFormNameSet() && 
			!m_ParserData.IsPaperLengthSet() )
	{
		DEBUG_OBJ_TEXT( _T("No value for PaperSize... Assuming Letter.\n") );
		m_ParserData.SetPaperSize( DMPAPER_LETTER );
	}

	// Submit the request
	DEBUG_OBJ_TEXT( _T("Submitting Print Job Request\n") );
	bReturnMe = IsRequestGranted();
	// Flag the print job as already been tracked
	if ( bReturnMe == TRUE )
	{
		DEBUG_OBJ_TEXT( _T("Request was granted.  Flagging job as tracked...\n") );
		m_PrintJobData.GetJobData( m_pPrintProcessorData->hPrinter, m_pPrintProcessorData->JobId, 2 );
		JOB_INFO_2*	pJobData = m_PrintJobData.Data();
		_ASSERTE( pJobData != NULL );

		if ( pJobData != NULL )
		{
			pJobData->pDocument = WIN9X_HACK_TITLE_STRING;
			pJobData->Position	= JOB_POSITION_UNSPECIFIED;
			m_PrintJobData.SetJobData( m_pPrintProcessorData->hPrinter, 
				m_pPrintProcessorData->JobId, 2 );
		}
	}

	// Return Success or Failure
	return bReturnMe;
}

void CPrintJobHandler::SetUserProperties()
{
	_ASSERTE( GetPrintJobInfo() != NULL );

	if ( GetPrintJobInfo() )
	{
		LPCTSTR		lpszUserName		= NULL;
		LPCTSTR		lpszDomainName		= NULL;
		CString		strUserName;
		CString		strDomainName;
		DWORD		dwUserNameSize		= MAX_COMPUTERNAME_LENGTH+1;
		DWORD		dwDomainNameSize	= MAX_COMPUTERNAME_LENGTH+1;
		BOOL		bSuccess;

		// Set the User Information
		bSuccess = m_Revert.GetToken().GetUserAndDomainNames( strUserName, strDomainName ); 

		if ( bSuccess == TRUE )
		{
			if ( (strUserName == STANDARD_ACCOUNT_NAME) && (GetPrintJobInfo()->pUserName[0]) )
			{
				// If the security context comes back with the standard account, use the JOB_INFO_2
				lpszUserName = GetPrintJobInfo()->pUserName;

				// Try to lookup the domain for this user name
				if ( m_Lookup.DoLookup(GetPrintJobInfo()->pUserName) )
				{
					lpszDomainName = m_Lookup.GetDomainName();
				}
			}
			else
			{
				// If we can get the user and domain information from the security context, use that
				lpszUserName	= strUserName;
				lpszDomainName	= strDomainName;
			}
		}
		else
		{
			// Otherwise, use the user name provided in the JOB_INFO_2 structure
			lpszUserName = GetPrintJobInfo()->pUserName;

			// Try to lookup the domain for this user name
			if ( m_Lookup.DoLookup(GetPrintJobInfo()->pUserName) )
			{
				lpszDomainName = m_Lookup.GetDomainName();
			}
		}
		CPrintJobRequest::SetUserProperties( lpszUserName, lpszDomainName );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// GetJobSize
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Reuses the CProcBuffer passed in as a parameter to prevent
// constant reallocation of memory.
inline DWORD CPrintJobHandler::GetJobSize( CProcBuffer& data )
{
	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	DWORD dwReturnMe;

	// Get the job data
	CPrinterAPI::GetPrintJobData( m_pPrintProcessorData->hPrinter, 
		m_pPrintProcessorData->JobId, 2, data );
	JOB_INFO_2* pJobData = reinterpret_cast<JOB_INFO_2*>(data.GetData());

	// Check the size
	if ( pJobData != NULL )
	{
		dwReturnMe = pJobData->Size;
	}
	else
	{
		DEBUG_OBJ_TEXT( _T("Unable to determine size of print job!\n") );
		dwReturnMe = 0;
	}

	// Return the size
	return dwReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// ReadRawJob
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CPrintJobHandler::ReadRawJob()
{
	DWORD					dwVectorUsed	= 0;
	DWORD					dwBytesRead		= 0;
	DWORD					dwJobSize		= 0;
	BYTE*					pBuffer			= NULL;
	bool					bSpooling		= true;
	JOB_INFO_2*				pJobData		= NULL;
	HRESULT					hr;
	CPrinterAPI				apiPrinter;
	CProcBuffer				data;

	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	// Wait for the job to stop spooling
	DEBUG_OBJ_TEXT( _T("Waiting for job to finish spooling\n") );
	while ( bSpooling )
	{
		CPrinterAPI::GetPrintJobData( m_pPrintProcessorData->hPrinter, 
						m_pPrintProcessorData->JobId, 1, data );
		JOB_INFO_1*	pSpoolJobData = reinterpret_cast<JOB_INFO_1*>(data.GetData());

		_ASSERTE( pSpoolJobData != NULL );
		if ( pSpoolJobData->Status & JOB_STATUS_SPOOLING )
		{
			::Sleep( 100 );
			bSpooling = true;
		}
		else
		{
			bSpooling = false;
		}
	}
	DEBUG_OBJ_TEXT( _T("Job Finished Spooling\n") );
		
	// Get the job size
	dwJobSize = GetJobSize(data) + (DWORD)READ_BUFFER_SIZE;

	// Open the printer
	apiPrinter.OpenPrinter( m_lpszDocumentName );

	// Create the data array
	m_pRawData = new Vector( dwJobSize );

	// Verify the array was created
	if ( ((DWORD)(m_pRawData)->Length()) < dwJobSize )
	{
		THROW_FAILED( E_OUTOFMEMORY );
	}

	// Create the access object
	VectorData	vdRawData(*m_pRawData);

	// Check the status of the job
	CheckJobStatus();

	// Point at the beginning of the buffer
	pBuffer	= &(vdRawData[0]);
	DEBUG_OBJ_TEXT( _T("Begin Reading Raw Data\n") );
	while ( apiPrinter.ReadPrinter(pBuffer, (vdRawData.Length() - dwVectorUsed), &dwBytesRead) 
		&& dwBytesRead && (!m_bJobAborted) )
	{
		// Track the number of bytes read
		dwVectorUsed += dwBytesRead;

		// Check to see if the buffer needs to be resized
		if ( (READ_BUFFER_SIZE + dwVectorUsed) > vdRawData.Length() )
		{
			dwJobSize	= vdRawData.Length() + GetMax( GetJobSize(data), (DWORD)READ_BUFFER_SIZE );
			DEBUG_OBJ_TEXT( _T("Reallocating Job Buffer: %d Bytes --> %d Bytes\n"), 
				vdRawData.Length(), dwJobSize );
			hr = vdRawData.Redim( dwJobSize );
			THROW_FAILED( hr );
		}

		// Make sure that the size is appropriate
		_ASSERTE( vdRawData.Length() >= (READ_BUFFER_SIZE + dwVectorUsed) );

		// Point at the next unused part of the buffer
		pBuffer	= (&(vdRawData[0]) + dwVectorUsed);

		// Check the status again
		CheckJobStatus();
	}
	DEBUG_OBJ_TEXT( _T("Finished Reading Raw Data\n") );

	// Set the array to be the actual size
	hr = vdRawData.Redim( dwVectorUsed );
	THROW_FAILED( hr );

	// Close the printer
	apiPrinter.ClosePrinter();

	// Return success if data was read
	if ( vdRawData.Length() > 0 )
	{
		DEBUG_OBJ_TEXT( _T("ReadRawData Read %d Bytes\n"), vdRawData.Length() );
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobHandler.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////