///////////////////////////////////////////////////////////////////////////////////////////////////////
// PSParser.h: interface for the CPSParser class.
//
// By Tom Gibson (term@xerosoft.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PSPARSER_H__C0185FEA_2431_46F4_8302_85915C592C7A__INCLUDED_)
#define AFX_PSPARSER_H__C0185FEA_2431_46F4_8302_85915C592C7A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintParserBase.h"
#include "PrintParserData.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPSParser Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPSParser : public CPrintParserBase  
{
public:
	// Our single public member function for parsing.
	virtual bool	Parse(CPrintParserData* pParserData);

	// Constructor and Destructor
				CPSParser();
	virtual		~CPSParser();

private:
	// Internal private member functions
	void Reset(void);
	bool HandleLine( LPCSTR szLine, LPCSTR szBuffer, int nCurrentBufferOffset, 
			CPrintParserData* pParserData );
	bool HandleDSC( LPCSTR szCommand, LPSTR* szParams, int iParamCount, 
			CPrintParserData *pParserData );

	// Private member variables
	bool	m_bFoundDSCPages;
	bool	m_bFoundHeader;
	bool	m_bFoundCopies;
	int		m_nPageTrailerCount;
	int		m_nPagePageCount;
	LPSTR	m_szLine;
	LPSTR	m_szCommand;
	LPSTR	m_szParam1;
	LPSTR	m_szParam2;
	LPSTR	m_szParam3;
	LPSTR	m_szParam4;
	LPSTR	m_szParam5;

// Static Attributes
private:
	static const PaperSizeInfo PSFormNames[];
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PSPARSER_H__C0185FEA_2431_46F4_8302_85915C592C7A__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PSParser.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
