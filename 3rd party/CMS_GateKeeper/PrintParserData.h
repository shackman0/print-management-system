///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintParserData.h: interface for the CPrintParserData class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTPARSERDATA_H__8AE1406B_D8D7_11D3_A427_00105A1C588D__INCLUDED_)
#define AFX_PRINTPARSERDATA_H__8AE1406B_D8D7_11D3_A427_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "DevMode.h"
#include "ExDevMode.h"
#include "PrintJobBuffer.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_PRINTPARSER_DEBUG
	#define DEBUG_PARSER					DEBUG_TEXT
#else
	#define DEBUG_PARSER					((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintParserData Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrintParserData : public CDevMode, public CExDevMode, public CPrintJobBuffer 
{
// Construction / Destruction
public:
	CPrintParserData();
	virtual ~CPrintParserData();

// Data Access															
public:																	
	// Get Functions
	const CString&		GetErrorInfo() const;
	const CString&		GetDataType() const;

	// Test Value Functions
	bool				IsErrorInfo() const;
	bool				IsDataTypeSet() const;

	// Set Functions													
	void				SetErrorInfo( LPCTSTR lpszErrorInfo );
	void				SetDataType( LPCTSTR lpszDataType );

// Operations
public:
	void				Clear();

// Operators
public:
	operator +=( const CPrintParserData& );
	operator =( const CPrintParserData& );

// Attributes
private:
	CString		m_strErrorInfo;
	CString		m_strDataType;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintParserData Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPrintParserData::CPrintParserData()
	{
		// Nothing to do
	}

	inline CPrintParserData::~CPrintParserData()
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access															
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline const CString& CPrintParserData::GetErrorInfo() const
	{
		return m_strErrorInfo;
	}

	inline const CString& CPrintParserData::GetDataType() const
	{
		return m_strDataType;
	}

	inline bool CPrintParserData::IsErrorInfo() const
	{
		return (m_strErrorInfo.GetLength() > 0);
	}

	inline bool CPrintParserData::IsDataTypeSet() const
	{
		return (m_strDataType.GetLength() > 0);
	}

	inline void CPrintParserData::SetErrorInfo( LPCTSTR lpszErrorInfo )
	{
		m_strErrorInfo += lpszErrorInfo;
		m_strErrorInfo += _T("\n");
	}

	inline void CPrintParserData::SetDataType( LPCTSTR lpszDataType )
	{
		m_strDataType = lpszDataType;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CPrintParserData::Clear()
	{
		// Clear out the local members
		m_strErrorInfo.Empty();
		m_strDataType.Empty();

		// Clear out the base classes
		CDevMode::Clear();
		CExDevMode::Clear();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operators
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPrintParserData::operator =( const CPrintParserData& ppdOther )
	{
		::CopyMemory( this, &ppdOther, sizeof(CPrintParserData) );
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTPARSERDATA_H__8AE1406B_D8D7_11D3_A427_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintParserData.h
///////////////////////////////////////////////////////////////////////////////////////////////////////