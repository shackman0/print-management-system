///////////////////////////////////////////////////////////////////////////////////////////////////////
// DataBuffer.h: interface for the CDataBuffer class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATABUFFER_H__4BE8984F_ECBB_455E_87E7_4B28F4FD68D9__INCLUDED_)
#define AFX_DATABUFFER_H__4BE8984F_ECBB_455E_87E7_4B28F4FD68D9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "MemoryAllocators.h"
#include "CMSException.h"
#include "CMSBase.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define BLOCK_SIZE_BUFFER_STANDARD							256

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Exception Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Debugging Options
#ifdef _DEBUG
	#define DATA_BUFFER_EXCEPTION(code) 			CDataBufferException((code),THIS_FILE,__LINE__)
#else
	#define DATA_BUFFER_EXCEPTION(code) 			CDataBufferException((code))
#endif

class CDataBufferException : public CCMSException  
{
// Exception Codes
public:
	enum
	{
		exDataBuf_Unknown				= 0x00,
		exDataBuf_NoMemory				= 0x01,
		exDataBuf_BufferOverrun 		= 0x02,
		exDataBuf_NotResizable			= 0x03
	};

// Construction / Destruction
public:
#ifdef _DEBUG
	CDataBufferException( const DWORD& dwExceptionCode, LPCTSTR lpszFile, int nLine ) :
		CCMSException(_T("DataBuf"), dwExceptionCode, lpszFile, nLine)
	{
		// Nothing to do
	}
#else
	CDataBufferException( const DWORD& dwExceptionCode ) :
		CCMSException(_T("DataBuf"), dwExceptionCode)
	{
		// Nothing to do
	}
#endif

protected:
	virtual LPCTSTR GetMessageBoxTitle() const
	{
		return CMS_PRODUCT_NAME _T(" Data Buffer Error");
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Helper Commands
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_DEBUG_OUTPUT
	#ifdef _DEBUG
		#define DEBUG_BUFFER(buf)		((buf).DebugDump(THIS_FILE, __LINE__))
	#else
		#define DEBUG_BUFFER(buf)		((buf).DebugDump())
	#endif
#else
	#define DEBUG_BUFFER(buf)			((void)(0))
#endif

#ifdef USE_BUFFER_DEBUG
	#define DEBUG_BUFFER_TEXT			DEBUG_TEXT
#else
	#define DEBUG_BUFFER_TEXT			((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Conversion Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CBuffer Class Definition (Abstract)
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CBuffer
{
// Construction / Destruction
public:
	CBuffer();

// Pure Virtual Member Functions
public:
	virtual LPBYTE			GetData()							= 0;
	virtual const LPBYTE	GetData() const 					= 0;
	virtual const DWORD&	GetSize() const 					= 0;
	virtual void			SetSize( const DWORD& dwSize )		= 0;

// Operations
public:
	void					Append( const CBuffer& bufAppendMe );
	void					Copy( const CBuffer& bufCopyMe );
	bool					Equals( const CBuffer& bufCompareMe ) const;

// Overridable Operations
public:
	virtual void			Zero();
	virtual void			Append( const LPBYTE lpData, const DWORD& dwSize );
	virtual void			Copy( const LPBYTE lpData, const DWORD& dwSize );
	virtual bool			Equals( const LPBYTE lpData, const DWORD& dwSize ) const;

// Operators
public:
	// Comparison
	bool					operator==( const CBuffer& rOther ) const;
	// Assignment
	CBuffer&				operator=( const CBuffer& bufCopyMe );
	// Addition
	CBuffer&				operator+=( const CBuffer& bufSource );
	// Conversion
							operator LPBYTE();
							operator LPVOID();

// Static Operations
protected:
	static bool 			DoCompare( const LPBYTE lpOne, const LPBYTE lpTwo, const DWORD& dwSize );
	static void 			DoCopy( LPBYTE lpTarget, const LPBYTE lpSource, const DWORD& dwSize );
	static void 			DoZero( LPBYTE lpZeroMe, const DWORD& dwSize );
	static DWORD			GetAdjustedSize( const DWORD& dwSize, const DWORD& dwBlockSize );

// Debug Information
public:
#ifdef USE_DEBUG_OUTPUT
	virtual void			DebugDump( LPCTSTR lpszFile = NULL, int nLine = 0 ) const;
#endif
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CDataStream Base Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CDataStream
{
// Construction / Destruction
public:
	CDataStream( CBuffer* pBuffer = NULL );
	CDataStream( CBuffer& rBuffer );

// Data Access
public:
	// Get Data
	DWORD			GetBytesRemaining() const;
	LPVOID			GetCurrentBuffer();
	const LPVOID	GetCurrentBuffer() const;
	const DWORD&	GetPosition() const;
	// Set Data
	void			SetPosition( const DWORD& dwPosition );
	void			SetBuffer( CBuffer* pBuffer );

// Helper Functions
public:
	void			MoveAhead( const DWORD& dwSkip );
	void			MoveBack( const DWORD& dwSkip );

// Write Operations
public:
	void			Write( const BYTE& nValue );
	void			Write( const WORD& nValue );
	void			Write( const DWORD& nValue );
	void			Write( const __int64& nValue );
	void			Write( const float& nValue );
	void			Write( const double& nValue );
	void			Write( const CString& strValue );
	void			Write( LPCTSTR lpszSource );
	void			Write( const CCMSBase& obj );
	void			WriteData( const LPCBYTE lpData, const DWORD& dwSize );

protected:
	virtual void	Write16( const __int16& nValue ) 			= 0;
	virtual void	Write32( const __int32& dwValue )			= 0;
	virtual void	Write64( const __int64& nValue )			= 0;
	virtual void	WriteString( LPCTSTR lpszSource, const DWORD& dwStringLength ) = 0;
	virtual void	Write8( const __int8& nValue );
	virtual void	WriteFloat( const float& nValue );
	virtual void	WriteDouble( const double& nValue );
	virtual void	WriteObj( const CCMSBase& obj );

// Read Operations
public:
	void			Read( BYTE& nValue, const bool& bPeek = false );
	void			Read( WORD& nValue, const bool& bPeek = false );
	void			Read( DWORD& nValue, const bool& bPeek = false );
	void			Read( float& nValue, const bool& bPeek = false );
	void			Read( double& nValue, const bool& bPeek = false );
	void			Read( CString& strValue, const bool& bPeek = false );
	void			Read( CCMSBase& obj );
	void			ReadData( LPBYTE lpData, const DWORD& dwSize );

protected:
	virtual void	Read16( __int16& nValue, const bool& bPeek = false ) 		= 0;
	virtual void	Read32( __int32& nValue, const bool& bPeek = false )		= 0;
	virtual void	Read64( __int64& nValue, const bool& bPeek = false )		= 0;
	virtual void	ReadString( CString& strValue, const bool& bPeek = false )	= 0;
	virtual void	Read8( __int8& nValue, const bool& bPeek = false );
	virtual void	ReadFloat( float& nValue, const bool& bPeek = false );
	virtual void	ReadDouble( double& nValue, const bool& bPeek = false );
	virtual void	ReadObj( CCMSBase& obj );

// Operators
public:
	template <typename DATATYPE>
	CDataStream&	operator<<( const DATATYPE& nValue )
	{
		Write(nValue);
		return (*this);
	}

	template <typename DATATYPE>
	CDataStream&	operator>>( DATATYPE& nValue )
	{
		Read(nValue);
		return (*this);
	}

	operator LPBYTE();
	operator LPVOID();

// Variant Operations
#ifdef _INC_COMUTIL
public:
	void			Write( const VARIANT& varValue );
	void			Read( _variant_t& varValue, const bool& bPeek = false );

protected:
	virtual void	WriteVariant( const VARIANT& varValue );
	virtual void	ReadVariant( _variant_t& varValue, const bool& bPeek );
#endif

// Attributes
private:
	CBuffer*	m_pBuffer;
	DWORD		m_dwPosition;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CEndianDataStream Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename CONVERSION>
class CEndianDataStream : public CDataStream
{
// Construction / Destruction
public:
	CEndianDataStream<CONVERSION>( CBuffer* pBuffer = NULL );
	CEndianDataStream<CONVERSION>( CBuffer& rBuffer );

// Write Operations
protected:
	virtual void	Write16( const __int16& nValue );
	virtual void	Write32( const __int32& dwValue );
	virtual void	Write64( const __int64& dwValue );

// Read Operations
protected:
	virtual void	Read16( __int16& nValue, const bool& bPeek = false );
	virtual void	Read32( __int32& nValue, const bool& bPeek = false );
	virtual void	Read64( __int64& nValue, const bool& bPeek = false );
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSStringDataStream Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename DATA_STREAM>
class CCMSStringDataStream : public DATA_STREAM
{
// Construction / Destruction
public:
	CCMSStringDataStream( CBuffer* pBuffer = NULL );
	CCMSStringDataStream( CBuffer& rBuffer );

// String Operations
protected:
	virtual void	WriteString( LPCTSTR lpszSource, const DWORD& dwStringLength );
	virtual void	ReadString( CString& strValue, const bool& bPeek = false );
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CANSIStringDataStream Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename DATA_STREAM>
class CANSIStringDataStream : public DATA_STREAM
{
// Construction / Destruction
public:
	CANSIStringDataStream( CBuffer* pBuffer = NULL );
	CANSIStringDataStream( CBuffer& rBuffer );

// String Operations
protected:
	virtual void	WriteString( LPCTSTR lpszSource, const DWORD& dwStringLength );
	virtual void	ReadString( CString& strValue, const bool& bPeek = false );
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CBasicBufferTemplate Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <typename ALLOCATOR>
class CBasicBufferTemplate : public CBuffer, protected ALLOCATOR, public CCMSBase
{
// Attributes
protected:
	LPBYTE			m_pData;
	DWORD			m_dwSize;
	DWORD			m_dwActualSize;

// Construction / Destruction
public:
	CBasicBufferTemplate<ALLOCATOR>( const DWORD& dwBufferSize = 0 );
	CBasicBufferTemplate<ALLOCATOR>( const CBuffer& bufCopyMe );
	CBasicBufferTemplate<ALLOCATOR>( const LPBYTE pData, const DWORD& dwBufferSize );
	virtual ~CBasicBufferTemplate<ALLOCATOR>();

// Virtual Member Functions
public:
	virtual LPBYTE			GetData();
	virtual const LPBYTE	GetData() const;
	virtual const DWORD&	GetSize() const;
	virtual void			SetSize( const DWORD& dwSize );
	virtual void			Zero();
	virtual void			WriteObject( CDataStream& ds ) const;
	virtual void			ReadObject( CDataStream& ds );

protected:
	virtual void			Initialize();
	virtual void			Destroy();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CFileBuffer Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CFileBuffer : public CBuffer, public CCMSBase
{
// Construction / Destruction
public:
	CFileBuffer();
	CFileBuffer( LPCTSTR lpszFileName, const DWORD& dwMinSize = 0, const bool bReadOnly = false );
	virtual ~CFileBuffer();

// Virtual Member Functions
public:
	virtual LPBYTE			GetData();
	virtual const LPBYTE	GetData() const;
	virtual const DWORD&	GetSize() const;
	virtual void			SetSize( const DWORD& dwSize );

// Other Operations
public:
	void					SetFileName( LPCTSTR lpszFileName, const DWORD& dwMinSize = 0,
								const bool bReadOnly = false  );
	void					Close();
	void					Flush();

	virtual void			WriteObject( CDataStream& ds ) const;
	virtual void			ReadObject( CDataStream& ds );

// Internal Operations
protected:
	void					OpenMapping( const DWORD& dwNewSize );
	void					CloseMapping();

// Attributes
protected:
	CSystemHandle	m_hFile;
	CSystemHandle	m_hMapping;
	LPVOID			m_pData;
	DWORD			m_dwSize;
	bool			m_bReadOnly;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CBuffer Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CBuffer::CBuffer()
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CBuffer::Append( const CBuffer& bufAppendMe )
	{
		Append( bufAppendMe.GetData(), bufAppendMe.GetSize() );
	}

	inline void CBuffer::Copy( const CBuffer& bufCopyMe )
	{
		Copy( bufCopyMe.GetData(), bufCopyMe.GetSize() );
	}

	inline bool CBuffer::Equals( const CBuffer& bufCompareMe ) const
	{
		return Equals( bufCompareMe.GetData(), bufCompareMe.GetSize() );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Overridable Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CBuffer::Zero()
	{
		DoZero( GetData(), GetSize() );
	}

	inline void CBuffer::Append( const LPBYTE lpData, const DWORD& dwSize )
	{
		if ( dwSize > 0 )
		{
			DWORD dwOldSize = GetSize();

			SetSize( dwOldSize + dwSize );
			_ASSERTE( (dwOldSize + dwSize) <= GetSize() );
			DoCopy( GetData()+dwOldSize, lpData, dwSize );
		}
	}

	inline void CBuffer::Copy( const LPBYTE lpData, const DWORD& dwSize )
	{
		SetSize( dwSize );
		_ASSERTE( dwSize <= GetSize() );
		if ( dwSize > 0 )
		{
			DoCopy( GetData(), lpData, dwSize );
		}
	}

	inline bool CBuffer::Equals( const LPBYTE lpData, const DWORD& dwSize ) const
	{
		if ( GetSize() == dwSize )
		{
			// Check assumptions
			_ASSERTE( lpData != NULL );
			_ASSERTE( GetData() != NULL );

			// Compare the memory
			return DoCompare( GetData(), lpData, GetSize() );
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operators
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CBuffer::operator==( const CBuffer& rOther ) const
	{
		return Equals(rOther);
	}

	inline CBuffer& CBuffer::operator=( const CBuffer& bufCopyMe )
	{
		Copy( bufCopyMe );
		return (*this);
	}

	inline CBuffer& CBuffer::operator+=( const CBuffer& bufSource )
	{
		Append(bufSource);
		return (*this);
	}

	inline CBuffer::operator LPBYTE()
	{
		return GetData();
	}

	inline CBuffer::operator LPVOID()
	{
		return GetData();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CBuffer::DoCompare( const LPBYTE lpOne, const LPBYTE lpTwo, const DWORD& dwSize )
	{
		// Check assumptions
		_ASSERTE( lpOne != NULL );
		_ASSERTE( lpTwo != NULL );
		_ASSERTE( dwSize != 0 );

		return ( ::memcmp(lpOne, lpTwo, dwSize) == 0 );
	}

	inline void CBuffer::DoCopy( LPBYTE lpTarget, const LPBYTE lpSource, const DWORD& dwSize )
	{
		// Check assumptions
		_ASSERTE( lpTarget != NULL );
		_ASSERTE( lpSource != NULL );
		_ASSERTE( dwSize != 0 );

		// Copy the memory
		::CopyMemory( lpTarget, lpSource, dwSize );
	}

	inline void CBuffer::DoZero( LPBYTE lpZeroMe, const DWORD& dwSize )
	{
		// Check assumptions
		_ASSERTE( ((lpZeroMe != NULL) && (dwSize > 0)) || ((lpZeroMe == NULL) && (dwSize == 0)) );

		// Zero the memory
		if ( lpZeroMe != NULL )
		{
			::ZeroMemory( lpZeroMe, dwSize );
		}
	}

	inline DWORD CBuffer::GetAdjustedSize( const DWORD& dwSize, const DWORD& dwBlockSize )
	{
		return ( dwSize + (dwBlockSize - (dwSize % dwBlockSize)) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Debugging Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	#ifdef USE_DEBUG_OUTPUT
	inline void CBuffer::DebugDump( LPCTSTR lpszFile, int nLine ) const
	{
		DEBUG_TEXT( _T("*** DataBuffer Dump ***\n") );

		// Display the extra info in the debug versions
		if ( lpszFile )
		{
			DEBUG_TEXT( _T("*** File\t: %s [Line #%d]\n"), lpszFile, nLine );
		}

		DEBUG_TEXT( _T("*** Size:\t0x%08x (%d)\n"), GetSize(), GetSize() );
		if ( GetSize() != 0 )
		{
			DEBUG_DUMP( GetData(), GetSize() );
		}
	}
	#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CDataStream Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CDataStream::CDataStream( CBuffer* pBuffer )
	{
		SetBuffer( pBuffer );
	}

	inline CDataStream::CDataStream( CBuffer& rBuffer )
	{
		SetBuffer( &rBuffer );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline DWORD CDataStream::GetBytesRemaining() const
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );

		// Return the value
		return (m_pBuffer->GetSize() - m_dwPosition);
	}

	inline LPVOID CDataStream::GetCurrentBuffer()
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );
		_ASSERTE( m_pBuffer->GetData() != NULL );

		// Return the value
		return (m_pBuffer->GetData() + m_dwPosition);
	}

	inline const LPVOID CDataStream::GetCurrentBuffer() const
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );
		_ASSERTE( m_pBuffer->GetData() != NULL );

		// Return the value
		return ( m_pBuffer->GetData() + m_dwPosition );
	}

	inline const DWORD& CDataStream::GetPosition() const
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );

		// Return the value
		return m_dwPosition;
	}

	inline void	CDataStream::SetPosition( const DWORD& dwPosition )
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );

		m_pBuffer->SetSize( GetMax(dwPosition, m_pBuffer->GetSize()) );
		m_dwPosition = dwPosition;
	}

	inline void	CDataStream::SetBuffer( CBuffer* pBuffer )
	{
		m_pBuffer = pBuffer;
		m_dwPosition = 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Helper Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void	CDataStream::MoveAhead( const DWORD& dwSkip )
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );

		SetPosition( GetPosition() + dwSkip );
	}

	inline void	CDataStream::MoveBack( const DWORD& dwSkip )
	{
		// Check assumptions
		_ASSERTE( m_pBuffer != NULL );

		SetPosition( GetPosition() - dwSkip );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Write Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void	CDataStream::Write( const BYTE& nValue )
	{
		Write8(nValue);
	}

	inline void	CDataStream::Write( const WORD& nValue )
	{
		Write16(nValue);
	}

	inline void	CDataStream::Write( const DWORD& nValue )
	{
		Write32(nValue);
	}

	inline void	CDataStream::Write( const __int64& nValue )	
	{
		Write64(nValue);
	}
	inline void	CDataStream::Write( const float& nValue )
	{
		WriteFloat(nValue);
	}

	inline void	CDataStream::Write( const double& nValue )
	{
		WriteDouble(nValue);
	}

	inline void	CDataStream::Write( const CString& strValue )
	{
		WriteString( strValue, strValue.GetLength() );
	}

	inline void	CDataStream::Write( LPCTSTR lpszSource )
	{
		WriteString( lpszSource, lstrlen(lpszSource) );
	}

	inline void	CDataStream::Write( const CCMSBase& obj )
	{
		WriteObj(obj);
	}

	inline void	CDataStream::WriteData( const LPCBYTE lpData, const DWORD& dwSize )
	{
		_ASSERTE( m_pBuffer != NULL ); 

		if ( dwSize > 0 )
		{
			// Check assumptions
			_ASSERTE( lpData != NULL );

			// Copy the data into the stream
			MoveAhead( dwSize );
			::CopyMemory( (reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - dwSize), lpData, dwSize );
		}
	}

	// Protected Functions
	inline void	CDataStream::Write8( const __int8& nValue )
	{
		_ASSERTE( m_pBuffer != NULL ); 
		MoveAhead( sizeof(__int8) );
		*( reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - 1 ) = nValue;
	}

	inline void	CDataStream::WriteFloat( const float& nValue )
	{
		Write32( *(reinterpret_cast<const __int32*>(&nValue)) );
	}

	inline void	CDataStream::WriteDouble( const double& nValue )
	{
		Write64( *(reinterpret_cast<const __int64*>(&nValue)) );
	}

	inline void	CDataStream::WriteObj( const CCMSBase& obj )
	{
		obj.WriteObject( (*this) );
	}


	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Read Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void	CDataStream::Read( BYTE& nValue, const bool& bPeek )
	{
		Read8( *reinterpret_cast<__int8*>(&nValue), bPeek );
	}

	inline void	CDataStream::Read( WORD& nValue, const bool& bPeek )
	{
		Read16( *reinterpret_cast<__int16*>(&nValue), bPeek );
	}

	inline void	CDataStream::Read( DWORD& nValue, const bool& bPeek )
	{
		Read32( *reinterpret_cast<__int32*>(&nValue), bPeek );
	}

	inline void	CDataStream::Read( float& nValue, const bool& bPeek )
	{
		ReadFloat( nValue, bPeek );
	}

	inline void	CDataStream::Read( double& nValue, const bool& bPeek )
	{
		ReadDouble( nValue, bPeek );
	}

	inline void	CDataStream::Read( CString& strValue, const bool& bPeek )
	{
		ReadString( strValue, bPeek );
	}

	inline void	CDataStream::Read( CCMSBase& obj )
	{
		ReadObj(obj);
	}

	inline void	CDataStream::ReadData( LPBYTE lpData, const DWORD& dwSize )
	{
		// Check assumptions
		_ASSERTE( lpData != NULL );
		_ASSERTE( dwSize > 0 );

		// Copy the data if there is enough left
		if ( GetBytesRemaining() >= dwSize )
		{
			::CopyMemory( lpData, GetCurrentBuffer(), dwSize );
			MoveAhead( dwSize );
		}
		else
		{
			// Not enough data in the buffer, so throw an exception
			throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
		}
	}

	// Protected Functions
	inline void CDataStream::Read8( __int8& nValue, const bool& bPeek )
	{
		if ( GetBytesRemaining() >= sizeof(__int8) )
		{
			nValue = *(reinterpret_cast<__int8*>(GetCurrentBuffer()));
			if ( bPeek == false )
			{
				MoveAhead( sizeof(__int8) );
			}
		}
		else
		{
			// Not enough data in the buffer, so throw an exception
			throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
		}
	}

	inline void CDataStream::ReadFloat( float& nValue, const bool& bPeek )
	{
		Read32( *reinterpret_cast<__int32*>(&nValue), bPeek );
	}

	inline void CDataStream::ReadDouble( double& nValue, const bool& bPeek )
	{
		Read64( *reinterpret_cast<__int64*>(&nValue), bPeek );
	}

	inline void CDataStream::ReadObj( CCMSBase& obj )
	{
		obj.ReadObject( (*this) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operators
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CDataStream::operator LPBYTE()
	{
		return reinterpret_cast<LPBYTE>(GetCurrentBuffer());
	}

	inline CDataStream::operator LPVOID()
	{
		return GetCurrentBuffer();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Variant Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _INC_COMUTIL
	// Public
	inline void	CDataStream::Write( const VARIANT& varValue )
	{
		WriteVariant(varValue);
	}

	inline void	CDataStream::Read( _variant_t& varValue, const bool& bPeek )
	{
		ReadVariant(varValue, bPeek);
	}

	// Protected
	inline void	CDataStream::WriteVariant( const VARIANT& varValue )
	{
		// Write the data type
		Write16( V_VT(&varValue) );

		switch( V_VT(&varValue) )
		{
		// Empty values
		case VT_EMPTY:	break;
		case VT_NULL:	break;

		// 1 byte values
		case VT_UI1:	Write8( V_UI1(&varValue) );												break;
		case VT_I1:		Write8( V_I1(&varValue) );												break;

		// 2 byte values
		case VT_I2:		Write16( V_I2(&varValue) );												break;
		case VT_UI2:	Write16( V_UI2(&varValue) );											break;
		case VT_BOOL:	Write16( V_BOOL(&varValue) );											break;

		// 4 byte values
		case VT_I4:		Write32( V_I4(&varValue) );												break;
		case VT_R4:		WriteFloat( V_R4(&varValue) );											break;
		case VT_UI4:	Write32( V_UI4(&varValue) );											break;
		case VT_INT:	Write32( V_INT(&varValue) );											break;

		// 8 byte values
		case VT_R8:		WriteDouble( V_R8(&varValue) );											break;
		case VT_DATE:	Write64( *(reinterpret_cast<const __int64*>(&(V_DATE(&varValue)))) );	break;
//		case VT_I8:		Write64( V_I8(&varValue) );												break;
//		case VT_UI8:	Write64( V_UI8(&varValue) );											break;

		// Other values
		case VT_CY:		Write64( V_CY(&varValue).int64 );										break;
		case VT_BSTR:
			{
				_bstr_t	bstrTemp = varValue;

				WriteString( static_cast<LPCTSTR>(bstrTemp), bstrTemp.length() );
			}
			break;

		// All other cases
		default:
			DEBUG_TEXT( _T("!*!*!*! Trying to stream unsupported Variant Type: %d\n"), 
				V_VT(&varValue) );
			_ASSERTE( false );
			break;
		}
	}

	inline void	CDataStream::ReadVariant( _variant_t& varValue, const bool& bPeek )
	{
		WORD	nVarType;

		Read( nVarType, bPeek );
		V_VT(&varValue) = nVarType;

		switch( nVarType )
		{
		// Empty Values
		case VT_EMPTY:	varValue.Clear();														break;
		case VT_NULL:	varValue.Clear();														break;

		// 1 byte values
		case VT_I1:		Read8( *(reinterpret_cast<__int8*>(&V_I1(&varValue))), bPeek );			break;
		case VT_UI1:	Read8( *(reinterpret_cast<__int8*>(&V_UI1(&varValue))), bPeek );		break;

		// 2 byte values
		case VT_I2:		Read16( *(reinterpret_cast<__int16*>(&V_I2(&varValue))), bPeek );		break;
		case VT_UI2:	Read16( *(reinterpret_cast<__int16*>(&V_UI2(&varValue))), bPeek );		break;
		case VT_BOOL:	Read16( *(reinterpret_cast<__int16*>(&V_BOOL(&varValue))), bPeek );		break;

		// 4 byte values
		case VT_I4:		Read32( *(reinterpret_cast<__int32*>(&V_I4(&varValue))), bPeek );		break;
		case VT_R4:		Read32( *(reinterpret_cast<__int32*>(&V_R4(&varValue))), bPeek );		break;
		case VT_UI4:	Read32( *(reinterpret_cast<__int32*>(&V_UI4(&varValue))), bPeek );		break;
		case VT_INT:	Read32( *(reinterpret_cast<__int32*>(&V_INT(&varValue))), bPeek );		break;

		// 8 byte values
		case VT_R8:		Read64( *(reinterpret_cast<__int64*>(&V_R8(&varValue))), bPeek );		break;
		case VT_DATE:	Read64( *(reinterpret_cast<__int64*>(&V_DATE(&varValue))), bPeek );		break;
		case VT_CY:		Read64( *(reinterpret_cast<__int64*>(&V_CY(&varValue))), bPeek );		break;
//		case VT_I8:		Read64( *(reinterpret_cast<__int64*>(&V_I8(&varValue))), bPeek );		break;
//		case VT_UI8:	Read64( *(reinterpret_cast<__int64*>(&V_UI8(&varValue))), bPeek );		break;

		// Other values
		case VT_BSTR:
			{
				CString strTemp;

				Read( strTemp, bPeek );
				V_VT(&varValue) = VT_EMPTY;
				varValue = strTemp;
			}
			break;

		// All other cases
		default:
			DEBUG_TEXT( _T("!*!*!*! An unsupported Variant Type was streamed: %d\n"), 
				V_VT(&varValue) );
			_ASSERTE( false );
			break;
		}
	}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CEndianDataStream Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename CONVERSION>
	inline CEndianDataStream<CONVERSION>::CEndianDataStream( CBuffer* pBuffer )
		: CDataStream(pBuffer)
	{
		// Nothing to do
	}

	template <typename CONVERSION>
	inline CEndianDataStream<CONVERSION>::CEndianDataStream( CBuffer& rBuffer )
		: CDataStream(rBuffer)
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Write Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Write16( const __int16& nValue )
	{
		MoveAhead( sizeof(__int16) );
		*( reinterpret_cast<__int16*>(GetCurrentBuffer()) - 1 ) = CONVERSION::Convert16(nValue);
	}

	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Write32( const __int32& dwValue )
	{
		MoveAhead( sizeof(__int32) );
		*( reinterpret_cast<__int32*>(GetCurrentBuffer()) - 1 ) = CONVERSION::Convert32(dwValue);
	}

	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Write64( const __int64& dwValue )
	{
		MoveAhead( sizeof(__int64) );
		*( reinterpret_cast<__int64*>(GetCurrentBuffer()) - 1 ) = CONVERSION::Convert64(dwValue);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Read Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Read16( __int16& nValue, const bool& bPeek )
	{
		if ( GetBytesRemaining() >= sizeof(__int16) )
		{
			nValue = CONVERSION::Convert16( *(reinterpret_cast<__int16*>(GetCurrentBuffer())) );
			if ( bPeek == false )
			{
				MoveAhead( sizeof(__int16) );
			}
		}
		else
		{
			// Not enough data in the buffer, so throw an exception
			throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
		}
	}

	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Read32( __int32& nValue, const bool& bPeek )
	{
		if ( GetBytesRemaining() >= sizeof(__int32) )
		{
			nValue = CONVERSION::Convert32( *(reinterpret_cast<__int32*>(GetCurrentBuffer())) );
			if ( bPeek == false )
			{
				MoveAhead( sizeof(__int32) );
			}
		}
		else
		{
			// Not enough data in the buffer, so throw an exception
			throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
		}
	}

	template <typename CONVERSION>
	inline void CEndianDataStream<CONVERSION>::Read64( __int64& nValue, const bool& bPeek )
	{
		if ( GetBytesRemaining() >= sizeof(__int64) )
		{
			nValue = CONVERSION::Convert64( *(reinterpret_cast<__int64*>(GetCurrentBuffer())) );
			if ( bPeek == false )
			{
				MoveAhead( sizeof(__int64) );
			}
		}
		else
		{
			// Not enough data in the buffer, so throw an exception
			throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSStringDataStream Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename DATA_STREAM>
	inline CCMSStringDataStream<DATA_STREAM>::CCMSStringDataStream( CBuffer* pBuffer ) 
		: DATA_STREAM(pBuffer)
	{
		// Nothing to do
	}

	template <typename DATA_STREAM>
	inline CCMSStringDataStream<DATA_STREAM>::CCMSStringDataStream( CBuffer& rBuffer )
		: DATA_STREAM(rBuffer)
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// String Operations
	//		Strings are pre-fixed by the length of the string in bytes and are ALWAYS in UNICODE format
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename DATA_STREAM>
	inline void CCMSStringDataStream<DATA_STREAM>::
		WriteString( LPCTSTR lpszSource, const DWORD& dwStringLength )
	{
		// Check assumptions
		_ASSERTE( (lpszSource != NULL) || (dwStringLength == 0) );

		if ( (lpszSource != NULL) && (dwStringLength > 0) )
		{
			DWORD	dwLength = dwStringLength * sizeof(WCHAR);

			// Write the size of the string
			DATA_STREAM::Write32( dwLength );

			// Increment the position counter
			MoveAhead( dwLength );

			// Copy the data appropriately depending on whether or not it is UNICODE
#ifdef _UNICODE
			::CopyMemory( (reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - dwLength), 
				lpszSource, dwLength );
#else
			::MultiByteToWideChar( CP_ACP, 0, lpszSource, dwStringLength, 
				reinterpret_cast<LPWSTR>((reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - dwLength)), 
				dwStringLength );
#endif
		}
		else
		{
			// Write an empty string by just giving a size of zero
			DATA_STREAM::Write32( (DWORD)(0x00000000) );
		}
	}

	template <typename DATA_STREAM>
	inline void CCMSStringDataStream<DATA_STREAM>::
		ReadString( CString& strValue, const bool& bPeek )
	{
		DWORD	dwStringBytes	= 0;

		// Read the size of the string
		DATA_STREAM::Read( dwStringBytes, bPeek );

		if ( dwStringBytes > 0 )
		{
			DWORD dwStringLength = dwStringBytes / sizeof(WCHAR);

			// If there is enough data left in the buffer
			if ( GetBytesRemaining() >= dwStringBytes )
			{
				// Copy the data appropriately depending on whether or not it is UNICODE
#ifdef _UNICODE
				::CopyMemory( strValue.GetBufferSetLength(dwStringLength), GetCurrentBuffer(), 
					dwStringBytes );
#else
				DEBUG_FAIL_ON_ZERO( ::WideCharToMultiByte( CP_ACP, 0, 
					reinterpret_cast<LPCWSTR>(GetCurrentBuffer()), dwStringLength, 
					strValue.GetBufferSetLength(dwStringLength), (dwStringLength+1), NULL, NULL ) );
				// +1 for the terminator
#endif
				// Release the string's buffer
				strValue.ReleaseBuffer();

				// Increment the source's position
				if ( bPeek == false )
				{
					MoveAhead( dwStringBytes );
				}
			}
			else
			{
				// Not enough data in the buffer, so throw an exception
				throw DATA_BUFFER_EXCEPTION( CDataBufferException::exDataBuf_BufferOverrun );
			}
		}
		else
		{
			strValue.Empty();
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CANSIStringDataStream Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename DATA_STREAM>
	inline CANSIStringDataStream<DATA_STREAM>::CANSIStringDataStream( CBuffer* pBuffer ) 
		: DATA_STREAM(pBuffer)
	{
		// Nothing to do
	}

	template <typename DATA_STREAM>
	inline CANSIStringDataStream<DATA_STREAM>::CANSIStringDataStream( CBuffer& rBuffer )
		: DATA_STREAM(rBuffer)
	{
		// Nothing to do
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// String Operations
	//		ANSI Strings are always 1 byte characters and include the NULL terminator
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename DATA_STREAM>
	inline void CANSIStringDataStream<DATA_STREAM>::
		WriteString( LPCTSTR lpszSource, const DWORD& dwStringLength )
	{
		// Check assumptions
		_ASSERTE( (lpszSource != NULL) || (dwStringLength == 0) );

		if ( (lpszSource != NULL) && (dwStringLength > 0) )
		{
			DWORD dwLength = dwStringLength+1;	// +1 to include the NULL

			// Increment the position counter
			MoveAhead( dwLength );

			// Copy the data appropriately depending on whether or not it is UNICODE
#ifndef _UNICODE
			::CopyMemory( (reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - dwLength), 
				lpszSource, dwLength );
#else
			// Convert from Unicode to ANSI
			DEBUG_FAIL_ON_ZERO( ::WideCharToMultiByte(CP_ACP, 0, lpszSource, dwStringLength, 
				reinterpret_cast<LPSTR>((reinterpret_cast<LPBYTE>(GetCurrentBuffer()) - dwLength)), 
				dwStringLength, NULL, NULL) );
#endif
		}
		else
		{
			// Just write the NULL Terminator
			DATA_STREAM::Write( (BYTE)(0x00) );
		}
	}

	template <typename DATA_STREAM>
	inline void CANSIStringDataStream<DATA_STREAM>::
		ReadString( CString& strValue, const bool& bPeek )
	{
		// Copy the buffer as an ANSI string
#ifndef __AFXWIN_H__
		strValue.Copy( reinterpret_cast<LPCSTR>(GetCurrentBuffer()) );
#else
		// MFC Version
		strValue = CString( reinterpret_cast<LPCSTR>(GetCurrentBuffer()) );
#endif

		// Increment the source's position
		if ( bPeek == false )
		{
			MoveAhead( strValue.GetLength() + 1 );	// +1 for the NULL
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CBasicBufferTemplate Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename ALLOCATOR>
	inline CBasicBufferTemplate<ALLOCATOR>::CBasicBufferTemplate( const DWORD& dwBufferSize )
	{
		Initialize();
		SetSize( dwBufferSize );
	}

	template <typename ALLOCATOR>
	inline CBasicBufferTemplate<ALLOCATOR>::CBasicBufferTemplate( const CBuffer& bufCopyMe )
	{
		Initialize();
		Copy( bufCopyMe );
	}

	template <typename ALLOCATOR>
	inline CBasicBufferTemplate<ALLOCATOR>::CBasicBufferTemplate( 
		const LPBYTE pData, const DWORD& dwBufferSize )
	{
		Initialize();
		Copy( pData, dwBufferSize );
	}

	template <typename ALLOCATOR>
	inline CBasicBufferTemplate<ALLOCATOR>::~CBasicBufferTemplate()
	{
		Destroy();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Virtual Member Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename ALLOCATOR>
	inline LPBYTE CBasicBufferTemplate<ALLOCATOR>::GetData()
	{
		return m_pData;
	}

	template <typename ALLOCATOR>
	inline const LPBYTE CBasicBufferTemplate<ALLOCATOR>::GetData() const
	{
		return m_pData;
	}

	template <typename ALLOCATOR>
	inline const DWORD& CBasicBufferTemplate<ALLOCATOR>::GetSize() const
	{
		return m_dwSize;
	}

	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::SetSize( const DWORD& dwSize )
	{
		if ( m_dwSize != dwSize )
		{
			m_dwSize = dwSize;
			if ( m_pData != NULL )
			{
				if ( dwSize > m_dwActualSize )
				{
					m_dwActualSize = GetAdjustedSize(dwSize, BLOCK_SIZE_BUFFER_STANDARD);
					DEBUG_BUFFER_TEXT( _T("Move Buffer:	%d Bytes @ 0x%08x\n"), m_dwSize, m_pData );
					m_pData = (LPBYTE)Realloc( m_pData, m_dwActualSize );
					DEBUG_BUFFER_TEXT( _T("				%d Bytes @ 0x%08x\n"), 
						m_dwActualSize, m_pData );
				}
			}
			else
			{
				m_dwActualSize = GetAdjustedSize(dwSize, BLOCK_SIZE_BUFFER_STANDARD);
				m_pData = (LPBYTE)Alloc( m_dwActualSize );
				DEBUG_BUFFER_TEXT( _T("Make Buffer:	%d Bytes @ 0x%08x\n"), m_dwActualSize, m_pData );
			}
		}
	}

	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::Zero()
	{
		DoZero( GetData(), m_dwActualSize );
	}

	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::WriteObject( CDataStream& ds ) const
	{
		ds.Write( GetSize() );
		ds.WriteData( GetData(), GetSize() );
	}

	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::ReadObject( CDataStream& ds )
	{
		DWORD dwSize;

		ds.Read( dwSize );
		SetSize( dwSize );
		ds.ReadData( GetData(), dwSize );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::Initialize()
	{
		m_pData 		= NULL;
		m_dwSize		= 0;
		m_dwActualSize	= 0;
	}

	template <typename ALLOCATOR>
	inline void CBasicBufferTemplate<ALLOCATOR>::Destroy()
	{
		if ( m_pData != NULL )
		{
			Free( m_pData );
			m_pData = NULL;
			m_dwSize = 0;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CFileBuffer Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CFileBuffer::CFileBuffer()
	{
		m_pData		= NULL;
		m_dwSize	= 0;
		m_bReadOnly	= true;
	}

	inline CFileBuffer::CFileBuffer( LPCTSTR lpszFileName, const DWORD& dwMinSize, 
		const bool bReadOnly )
	{
		m_pData		= NULL;
		m_dwSize	= 0;
		SetFileName( lpszFileName, dwMinSize, bReadOnly );
	}

	inline CFileBuffer::~CFileBuffer()
	{
		Close();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Virtual Member Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline LPBYTE CFileBuffer::GetData()
	{
		return reinterpret_cast<LPBYTE>(m_pData);
	}

	inline const LPBYTE CFileBuffer::GetData() const
	{
		return reinterpret_cast<LPBYTE>(m_pData);
	}

	inline const DWORD& CFileBuffer::GetSize() const
	{
		return m_dwSize;
	}

	inline void CFileBuffer::SetSize( const DWORD& dwSize )
	{
		if ( m_dwSize != dwSize )
		{
			OpenMapping( dwSize );
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Other Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CFileBuffer::SetFileName( LPCTSTR lpszFileName, const DWORD& dwMinSize, 
		const bool bReadOnly )
	{
		// Check assumptions
		_ASSERTE( lpszFileName != NULL );

		DWORD	dwNewSize = dwMinSize;

		// Close everything
		Close();

		// Assign the read only flag
		m_bReadOnly = bReadOnly;

		// Open the file
		m_hFile = ::CreateFile(
						lpszFileName,
						( (m_bReadOnly == false) ? (GENERIC_WRITE|GENERIC_READ) : GENERIC_READ ),
						( (m_bReadOnly == false) ? 0 : FILE_SHARE_READ ),
						NULL,
						OPEN_ALWAYS,
						FILE_FLAG_SEQUENTIAL_SCAN,
						NULL
						);

		// Check more assumptions
		_ASSERTE( m_hFile.IsValid() );

		// Map the entire file
		if ( m_hFile.IsValid() )
		{
			DWORD dwFileSize = ::GetFileSize( m_hFile, NULL );

			if ( dwFileSize > dwNewSize )
			{
				dwNewSize = dwFileSize;
			}
		}

		// Open the mapping
		if ( dwNewSize > 0 )
		{
			OpenMapping( dwNewSize );
		}
	}

	inline void CFileBuffer::Close()
	{
		CloseMapping();
		m_hFile.CloseHandle();
	}

	inline void CFileBuffer::Flush()
	{
		// Check assumptions
		_ASSERTE( m_dwSize > 0 );
		_ASSERTE( m_pData != NULL );

		// Flush the view (saves it to disk)
		THROW_WIN_FAIL( ::FlushViewOfFile(m_pData, m_dwSize) );
	}

	inline void CFileBuffer::WriteObject( CDataStream& ds ) const
	{
		ds.Write( GetSize() );
		ds.WriteData( GetData(), GetSize() );
	}

	inline void CFileBuffer::ReadObject( CDataStream& ds )
	{
		DWORD dwSize;

		ds.Read( dwSize );
		SetSize( dwSize );
		ds.ReadData( GetData(), dwSize );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CFileBuffer::OpenMapping( const DWORD& dwNewSize )
	{
		// Close the mapping
		CloseMapping();

		if ( dwNewSize > 0 )
		{
			// Create the File Mapping
			m_hMapping = ::CreateFileMapping(
							m_hFile,				// handle to file
							NULL,					// security
							( (m_bReadOnly == false) ? (PAGE_READWRITE) : PAGE_READONLY ),
							0,						// high-order DWORD of size
							dwNewSize,				// low-order DWORD of size
							NULL					// object name
							);
			DEBUG_FAIL( m_hMapping.GetHandle() != NULL );

			// Create the map
			m_pData = ::MapViewOfFile(
							m_hMapping,				// handle to file-mapping object
							( (m_bReadOnly == false) ? (FILE_MAP_ALL_ACCESS) : FILE_MAP_READ ),
							0,						// high-order DWORD of offset
							0,						// low-order DWORD of offset
							dwNewSize				// Number of bytes to map
							);
			DEBUG_FAIL( m_pData != NULL );
		}
		m_dwSize = dwNewSize;
		DEBUG_BUFFER_TEXT( _T("New File Buffer:	%d Bytes @ 0x%08x\n"), m_dwSize, m_pData );
	}

	inline void CFileBuffer::CloseMapping()
	{
		if ( m_hMapping.IsValid() )
		{
			// Check assumptions
			_ASSERTE( m_dwSize > 0 );
			_ASSERTE( m_pData != NULL );

			// Flush the data
			Flush();

			// Remove the mapping
			THROW_WIN_FAIL( ::UnmapViewOfFile(m_pData) );

			// Initialize the variables
			m_pData = NULL;
			m_dwSize = 0;

			// Close the mapping
			m_hMapping.CloseHandle();
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CBigEndianByteOrder Utility Class
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CBigEndianByteOrder
{
public:
	static __int64 	Convert64( const __int64& nValue )
	{
		return ::SystemToBigEndian64( nValue );
	}

	static long 	Convert32( const long& nValue )
	{
		return ::SystemToBigEndian32( nValue );
	}

	static short	Convert16( const short& nValue )
	{
		return ::SystemToBigEndian16( nValue );
	}
};

typedef CEndianDataStream<CBigEndianByteOrder>					CBigEndianDataStream;
typedef CCMSStringDataStream<CBigEndianDataStream>				CCMSStringBEStream;
typedef CANSIStringDataStream<CBigEndianDataStream> 			CANSIStringBEStream;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CLittleEndianByteOrder Utility Class
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CLittleEndianByteOrder
{
public:
	static __int64 	Convert64( const __int64& nValue )
	{
		return ::SystemToLittleEndian64( nValue );
	}

	static long 	Convert32( const long& nValue )
	{
		return ::SystemToLittleEndian32( nValue );
	}

	static short	Convert16( const short& nValue )
	{
		return ::SystemToLittleEndian16( nValue );
	}
};

typedef CEndianDataStream<CLittleEndianByteOrder>				CLittleEndianDataStream;
typedef CCMSStringDataStream<CLittleEndianDataStream>			CCMSStringLEStream;
typedef CANSIStringDataStream<CLittleEndianDataStream>			CANSIStringLEStream;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debug Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_DATABUFFER_H__4BE8984F_ECBB_455E_87E7_4B28F4FD68D9__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of DataBuffer.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
