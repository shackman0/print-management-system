///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL6DataType.cpp: implementation of the CPCL6DataType class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL6DataType.h"
#include "PrintParserBase.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Public Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL6DataType::ReadFromBuffer( CPrintJobBuffer& pjb )
{
	bool	bReturnMe;

	m_nDataType	= pjb.ReadByte();
	switch( GetDataTypeSubType(m_nDataType) )
	{
	case PCL_DATATYPE_SUBTYPE_VALUE:
		bReturnMe = ReadValue( pjb );
		break;

	case PCL_DATATYPE_SUBTYPE_ARRAY:
		bReturnMe = ReadArray( pjb );
		break;

	case PCL_DATATYPE_SUBTYPE_BOX:
		bReturnMe = ReadBox( pjb );
		break;

	case PCL_DATATYPE_SUBTYPE_XY:
		bReturnMe = ReadXY( pjb );
		break;

	case PCL_DATATYPE_SUBTYPE_INVALID:
		// Back up the PrintJobBuffer to put back the byte that was
		// used to check to see if it is a valid data type or not
		pjb.BackUp( sizeof(BYTE) );
		bReturnMe = false;
		break;

	default:
		bReturnMe = false;
		DEBUG_TEXT( _T("Trying to read value when datatype is invalid: 0x%02x\n"), m_nDataType );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Trying to read value when datatype is invalid") );
		break;
	}

	// Return success or failure
	return bReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Read Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL6DataType::ReadValue( CPrintJobBuffer& pjb )
{
	// Check the assumptions
	_ASSERTE( GetDataTypeSubType(m_nDataType) == PCL_DATATYPE_SUBTYPE_VALUE );

	// Handle the reading based on the base type
	switch( GetDataTypeBaseType(m_nDataType) )
	{
	// UByte Base Type
	case PCL_DATATYPE_BASE_UBYTE:
		m_Data.nByte	= pjb.ReadByte();
		break;

	// UInt16 Base Type
	case PCL_DATATYPE_BASE_UINT16:
		m_Data.nUInt16	= pjb.ReadUInt16();
		break;

	// UInt32 Base Type
	case PCL_DATATYPE_BASE_UINT32:
		m_Data.dwUInt32	= pjb.ReadUInt32();
		break;

	// SInt16 Base Type
	case PCL_DATATYPE_BASE_SINT16:
		m_Data.nSInt16	= pjb.ReadSInt16();
		break;

	// SInt32 Base Type
	case PCL_DATATYPE_BASE_SINT32:
		m_Data.nSInt32	= pjb.ReadSInt32();
		break;

	// Real32 Base Type
	case PCL_DATATYPE_BASE_REAL32:
		m_Data.nReal32	= pjb.ReadReal32();
		break;

	// Invalid Base Type
	default:
		DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type") );
		break;
	}

	// Return success
	return true;
}

bool CPCL6DataType::ReadArray( CPrintJobBuffer& pjb )
{
	// Check the assumptions
	_ASSERTE( GetDataTypeSubType(m_nDataType) == PCL_DATATYPE_SUBTYPE_ARRAY );

	// Declare some variables
	BYTE				nArraySizeType = pjb.ReadByte();
	unsigned register	nCounter;

	// Read the size based on the value of type indicator
	switch( nArraySizeType )
	{
	case PCL_DATATYPE_UBYTE:
		m_dwSize	= pjb.ReadByte();
		break;

	case PCL_DATATYPE_UINT16:
		m_dwSize	= pjb.ReadUInt16();
		break;

	// According to the current docs, all of these are invalid types
	case PCL_DATATYPE_UINT32:
	case PCL_DATATYPE_SINT16:
	case PCL_DATATYPE_SINT32:
	case PCL_DATATYPE_REAL32:
		DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type") );
		return false;
		break;

	// Is not a known data type
	default:
		DEBUG_TEXT( _T("Unknown data type: 0x%02x\n"), m_nDataType );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Unknown data type") );
		return false;
		break;
	}

	// Handle the reading based on the base type
	switch( GetDataTypeBaseType(m_nDataType) )
	{
	// UByte Base Type
	case PCL_DATATYPE_BASE_UBYTE:
		m_Data.pByte	= pjb.GetCurrentBuffer();
		pjb.SkipAhead( m_dwSize );
		break;

	// UInt16 Base Type
	case PCL_DATATYPE_BASE_UINT16:
		if ( pjb.DoesByteOrderMatch() )
		{
			m_Data.pUInt16	= (WORD*)pjb.GetCurrentBuffer();
			pjb.SkipAhead( m_dwSize * sizeof(WORD) );
			m_bDestroyArray = false;
		}
		else
		{
			m_Data.pUInt16	= new WORD[m_dwSize];
			for ( nCounter = 0 ; nCounter < m_dwSize ; nCounter++ )
			{
				m_Data.pUInt16[nCounter]	= pjb.ReadUInt16();
			}
			m_bDestroyArray = true;
		}
		break;
	
	// UInt32 Base Type
	case PCL_DATATYPE_BASE_UINT32:
		if ( pjb.DoesByteOrderMatch() )
		{
			m_Data.pUInt32	= (DWORD*)pjb.GetCurrentBuffer();
			pjb.SkipAhead( m_dwSize * sizeof(DWORD) );
			m_bDestroyArray = false;
		}
		else
		{
			m_Data.pUInt32	= new DWORD[m_dwSize];
			for ( nCounter = 0 ; nCounter < m_dwSize ; nCounter++ )
			{
				m_Data.pUInt32[nCounter]	= pjb.ReadUInt32();
			}
			m_bDestroyArray = true;
		}
		break;
	
	// SInt16 Base Type
	case PCL_DATATYPE_BASE_SINT16:
		if ( pjb.DoesByteOrderMatch() )
		{
			m_Data.pSInt16	= (short*)pjb.GetCurrentBuffer();
			pjb.SkipAhead( m_dwSize * sizeof(short) );
			m_bDestroyArray = false;
		}
		else
		{
			m_Data.pSInt16	= new short[m_dwSize];
			for ( nCounter = 0 ; nCounter < m_dwSize ; nCounter++ )
			{
				m_Data.pSInt16[nCounter]	= pjb.ReadSInt16();
			}
			m_bDestroyArray = true;
		}
		break;
	
	// SInt32 Base Type
	case PCL_DATATYPE_BASE_SINT32:
		if ( pjb.DoesByteOrderMatch() )
		{
			m_Data.pSInt32	= (long*)pjb.GetCurrentBuffer();
			pjb.SkipAhead( m_dwSize * sizeof(long) );
			m_bDestroyArray = false;
		}
		else
		{
			m_Data.pSInt32	= new long[m_dwSize];
			for ( nCounter = 0 ; nCounter < m_dwSize ; nCounter++ )
			{
				m_Data.pSInt32[nCounter]	= pjb.ReadSInt32();
			}
			m_bDestroyArray = true;
		}
		break;
	
	// Real32 Base Type
	case PCL_DATATYPE_BASE_REAL32:
		if ( pjb.DoesByteOrderMatch() )
		{
			m_Data.pReal32	= (float*)pjb.GetCurrentBuffer();
			pjb.SkipAhead( m_dwSize * sizeof(float) );
			m_bDestroyArray = false;
		}
		else
		{
			m_Data.pReal32	= new float[m_dwSize];
			for ( nCounter = 0 ; nCounter < m_dwSize ; nCounter++ )
			{
				m_Data.pReal32[nCounter]	= pjb.ReadReal32();
			}
			m_bDestroyArray = true;
		}
		break;
	
	// Invalid Base Type
	default:
		DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
		throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type") );
		break;
	}

	// Return success
	return true;
}

bool CPCL6DataType::ReadBox( CPrintJobBuffer& pjb )
{
	// Check the assumptions
	_ASSERTE( GetDataTypeSubType(m_nDataType) == PCL_DATATYPE_SUBTYPE_BOX );

	if ( pjb.DoesByteOrderMatch() )
	{
		// Uses the current byte order because it matches the architecture
		// of the current machine.

		BYTE*	pData	= NULL;
		DWORD	dwSize	= 0;

		// Handle the reading based on the base type
		switch( GetDataTypeBaseType(m_nDataType) )
		{
		// UByte Base Type
		case PCL_DATATYPE_BASE_UBYTE:
			pData	= (BYTE*)m_Data.nByteBox;
			dwSize	= sizeof(m_Data.nByteBox);
			break;

		// UInt16 Base Type
		case PCL_DATATYPE_BASE_UINT16:
			pData	= (BYTE*)m_Data.nUInt16Box;
			dwSize	= sizeof(m_Data.nUInt16Box);
			break;

		// UInt32 Base Type
		case PCL_DATATYPE_BASE_UINT32:
			pData	= (BYTE*)m_Data.dwUInt32Box;
			dwSize	= sizeof(m_Data.dwUInt32Box);
			break;

		// SInt16 Base Type
		case PCL_DATATYPE_BASE_SINT16:
			pData	= (BYTE*)m_Data.nSInt16Box;
			dwSize	= sizeof(m_Data.nSInt16Box);
			break;

		// SInt32 Base Type
		case PCL_DATATYPE_BASE_SINT32:
			pData	= (BYTE*)m_Data.nSInt32Box;
			dwSize	= sizeof(m_Data.nSInt32Box);
			break;

		// Real32 Base Type
		case PCL_DATATYPE_BASE_REAL32:
			pData	= (BYTE*)m_Data.nReal32Box;
			dwSize	= sizeof(m_Data.nReal32Box);
			break;

		// Invalid Base Type
		default:
			DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
			throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type in ReadBox()") );
			break;
		}

		// Copy the data
		if ( pData != NULL )
		{
			::CopyMemory( pData, pjb.GetCurrentBuffer(), dwSize );
			pjb.SkipAhead( dwSize );
		}
	}
	else
	{
		// Fixes the order because it does not match the architecture
		// of the current machine.

		// Handle the reading based on the base type
		switch( GetDataTypeBaseType(m_nDataType) )
		{
		// UByte Base Type
		case PCL_DATATYPE_BASE_UBYTE:
			m_Data.nByteBox[0]		= pjb.ReadByte();
			m_Data.nByteBox[1]		= pjb.ReadByte();
			m_Data.nByteBox[2]		= pjb.ReadByte();
			m_Data.nByteBox[3]		= pjb.ReadByte();
			break;

		// UInt16 Base Type
		case PCL_DATATYPE_BASE_UINT16:
			m_Data.nUInt16Box[0]	= pjb.ReadUInt16();
			m_Data.nUInt16Box[1]	= pjb.ReadUInt16();
			m_Data.nUInt16Box[2]	= pjb.ReadUInt16();
			m_Data.nUInt16Box[3]	= pjb.ReadUInt16();
			break;

		// UInt32 Base Type
		case PCL_DATATYPE_BASE_UINT32:
			m_Data.dwUInt32Box[0]	= pjb.ReadUInt32();
			m_Data.dwUInt32Box[1]	= pjb.ReadUInt32();
			m_Data.dwUInt32Box[2]	= pjb.ReadUInt32();
			m_Data.dwUInt32Box[3]	= pjb.ReadUInt32();
			break;

		// SInt16 Base Type
		case PCL_DATATYPE_BASE_SINT16:
			m_Data.nSInt16Box[0]	= pjb.ReadSInt16();
			m_Data.nSInt16Box[1]	= pjb.ReadSInt16();
			m_Data.nSInt16Box[2]	= pjb.ReadSInt16();
			m_Data.nSInt16Box[3]	= pjb.ReadSInt16();
			break;

		// SInt32 Base Type
		case PCL_DATATYPE_BASE_SINT32:
			m_Data.nSInt32Box[0]	= pjb.ReadSInt32();
			m_Data.nSInt32Box[1]	= pjb.ReadSInt32();
			m_Data.nSInt32Box[2]	= pjb.ReadSInt32();
			m_Data.nSInt32Box[3]	= pjb.ReadSInt32();
			break;

		// Real32 Base Type
		case PCL_DATATYPE_BASE_REAL32:
			m_Data.nReal32Box[0]	= pjb.ReadReal32();
			m_Data.nReal32Box[1]	= pjb.ReadReal32();
			m_Data.nReal32Box[2]	= pjb.ReadReal32();
			m_Data.nReal32Box[3]	= pjb.ReadReal32();
			break;

		// Invalid Base Type
		default:
			DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
			throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type in ReadBox()") );
			break;
		}
	}

	// Return success
	return true;
}

bool CPCL6DataType::ReadXY( CPrintJobBuffer& pjb )
{
	// Check the assumptions
	_ASSERTE( GetDataTypeSubType(m_nDataType) == PCL_DATATYPE_SUBTYPE_XY );

	if ( pjb.DoesByteOrderMatch() )
	{
		// Uses the current byte order because it matches the architecture
		// of the current machine.
		BYTE*	pData	= NULL;
		DWORD	dwSize	= 0;

		// Handle the reading based on the base type
		switch( GetDataTypeBaseType(m_nDataType) )
		{
		// UByte Base Type
		case PCL_DATATYPE_BASE_UBYTE:
			pData	= (BYTE*)m_Data.nByteXY;
			dwSize	= sizeof(m_Data.nByteXY);
			break;

		// UInt16 Base Type
		case PCL_DATATYPE_BASE_UINT16:
			pData	= (BYTE*)m_Data.nUInt16XY;
			dwSize	= sizeof(m_Data.nUInt16XY);
			break;

		// UInt32 Base Type
		case PCL_DATATYPE_BASE_UINT32:
			pData	= (BYTE*)m_Data.dwUInt32XY;
			dwSize	= sizeof(m_Data.dwUInt32XY);
			break;

		// SInt16 Base Type
		case PCL_DATATYPE_BASE_SINT16:
			pData	= (BYTE*)m_Data.nSInt16XY;
			dwSize	= sizeof(m_Data.nSInt16XY);
			break;

		// SInt32 Base Type
		case PCL_DATATYPE_BASE_SINT32:
			pData	= (BYTE*)m_Data.nSInt32XY;
			dwSize	= sizeof(m_Data.nSInt32XY);
			break;

		// Real32 Base Type
		case PCL_DATATYPE_BASE_REAL32:
			pData	= (BYTE*)m_Data.nReal32XY;
			dwSize	= sizeof(m_Data.nReal32XY);
			break;

		// Invalid Base Type
		default:
			DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
			throw PRINT_PARSER_EXCEPTION( _T("PCL6: Invalid data type in ReadXY()") );
			break;
		}

		// Copy the data
		if ( pData != NULL )
		{
			::CopyMemory( pData, pjb.GetCurrentBuffer(), dwSize );
			pjb.SkipAhead( dwSize );
		}
	}
	else
	{
		// Fixes the order because it does not match the architecture
		// of the current machine.

		// Handle the reading based on the base type
		switch( GetDataTypeBaseType(m_nDataType) )
		{
		// UByte Base Type
		case PCL_DATATYPE_BASE_UBYTE:
			m_Data.nByteXY[0]		= pjb.ReadByte();
			m_Data.nByteXY[1]		= pjb.ReadByte();
			break;

		// UInt16 Base Type
		case PCL_DATATYPE_BASE_UINT16:
			m_Data.nUInt16XY[0]		= pjb.ReadUInt16();
			m_Data.nUInt16XY[1]		= pjb.ReadUInt16();
			break;

		// UInt32 Base Type
		case PCL_DATATYPE_BASE_UINT32:
			m_Data.dwUInt32XY[0]	= pjb.ReadUInt32();
			m_Data.dwUInt32XY[1]	= pjb.ReadUInt32();
			break;

		// SInt16 Base Type
		case PCL_DATATYPE_BASE_SINT16:
			m_Data.nSInt16XY[0]		= pjb.ReadSInt16();
			m_Data.nSInt16XY[1]		= pjb.ReadSInt16();
			break;

		// SInt32 Base Type
		case PCL_DATATYPE_BASE_SINT32:
			m_Data.nSInt32XY[0]		= pjb.ReadSInt32();
			m_Data.nSInt32XY[1]		= pjb.ReadSInt32();
			break;

		// Real32 Base Type
		case PCL_DATATYPE_BASE_REAL32:
			m_Data.nReal32XY[0]		= pjb.ReadReal32();
			m_Data.nReal32XY[1]		= pjb.ReadReal32();
			break;

		// Invalid Base Type
		default:
			DEBUG_TEXT( _T("Invalid data type: 0x%02x\n"), m_nDataType );
			break;
		}
	}

	// Return success
	return true;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Private Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CPCL6DataType::Destroy()
{
	// Is there anything to destroy?
	if ( m_nDataType != PCL_DATATYPE_SUBTYPE_INVALID )
	{
		// If it is an array, destroy the data
		if ( GetDataTypeSubType(m_nDataType) == PCL_DATATYPE_SUBTYPE_ARRAY )
		{
			if ( m_bDestroyArray )
			{
				switch( m_nDataType )
				{
				// Array of UBytes
				case PCL_DATATYPE_UBYTE_ARRAY:
					delete [] m_Data.pByte;
					break;

				// Array of UInt16s
				case PCL_DATATYPE_UINT16_ARRAY:
					delete [] m_Data.pUInt16;
					break;	

				// Array of UInt32s
				case PCL_DATATYPE_UINT32_ARRAY:
					delete [] m_Data.pUInt32;
					break;	

				// Array of SInt16s
				case PCL_DATATYPE_SINT16_ARRAY:
					delete [] m_Data.pSInt16;
					break;	

				// Array of SInt32s
				case PCL_DATATYPE_SINT32_ARRAY:
					delete [] m_Data.pSInt32;
					break;	

				// Array of Real32s
				case PCL_DATATYPE_REAL32_ARRAY:
					delete [] m_Data.pReal32;
					break;	

				// No matching values
				default:
					DEBUG_TEXT( _T("Trying to Destroy Array for invalid Data Type: 0x%02x\n"), m_nDataType );
					throw PRINT_PARSER_EXCEPTION( _T("PCL6: Trying to Destroy Array for invalid Data Type") );
					break;
				}
			}
		}

		// Reset all the values
		m_nDataType	= PCL_DATATYPE_SUBTYPE_INVALID;
		m_dwSize	= 0;
		::ZeroMemory( &m_Data, sizeof(m_Data) );
	}

}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_PCL6_DEBUG
void CPCL6DataType::Dump()
{
	switch( m_nDataType )
	{
	// Regular Values
	case PCL_DATATYPE_UBYTE:
		DEBUG_PCL6_TEXT( _T("nByte        -> 0x%02x\n"), m_Data.nByte );
		break;

	case PCL_DATATYPE_UINT16:
		DEBUG_PCL6_TEXT( _T("nUInt16      -> %d\n"), m_Data.nUInt16 );
		break;

	case PCL_DATATYPE_UINT32:
		DEBUG_PCL6_TEXT( _T("dwUInt32     -> %d\n"), m_Data.dwUInt32 );
		break;

	case PCL_DATATYPE_SINT16:
		DEBUG_PCL6_TEXT( _T("nSInt16      -> %d\n"), m_Data.nSInt16 );
		break;

	case PCL_DATATYPE_SINT32:
		DEBUG_PCL6_TEXT( _T("nSInt32      -> %d\n"), m_Data.nSInt32 );
		break;

	case PCL_DATATYPE_REAL32:
		DEBUG_PCL6_TEXT( _T("nReal32      -> %f\n"), m_Data.nReal32 );
		break;

	// Array Values
	case PCL_DATATYPE_UBYTE_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d nBytes\n"), m_dwSize );
		break;

	case PCL_DATATYPE_UINT16_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d nUInt16s\n"), m_dwSize );
		break;

	case PCL_DATATYPE_UINT32_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d dwUInt32s\n"), m_dwSize );
		break;

	case PCL_DATATYPE_SINT16_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d nSInt16s\n"), m_dwSize );
		break;

	case PCL_DATATYPE_SINT32_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d nSInt32s\n"), m_dwSize );
		break;

	case PCL_DATATYPE_REAL32_ARRAY:
		DEBUG_PCL6_TEXT( _T("Array        -> %d nReal32s\n"), m_dwSize );
		break;

	// Box Values
	case PCL_DATATYPE_UBYTE_BOX:
		DEBUG_PCL6_TEXT( _T("nByte Box    -> 0x%02x 0x%02x 0x%02x 0x%02x\n"), 
			m_Data.nByteBox[0], m_Data.nByteBox[1], m_Data.nByteBox[2], m_Data.nByteBox[3] );
		break;

	case PCL_DATATYPE_UINT16_BOX:
		DEBUG_PCL6_TEXT( _T("nUInt16 Box  -> %d %d %d %d\n"), 
			m_Data.nUInt16Box[0], m_Data.nUInt16Box[1], 
			m_Data.nUInt16Box[2], m_Data.nUInt16Box[3] );
		break;

	case PCL_DATATYPE_UINT32_BOX:
		DEBUG_PCL6_TEXT( _T("dwUInt32 Box -> %d %d %d %d\n"), 
			m_Data.dwUInt32Box[0], m_Data.dwUInt32Box[1], 
			m_Data.dwUInt32Box[2], m_Data.dwUInt32Box[3] );
		break;

	case PCL_DATATYPE_SINT16_BOX:
		DEBUG_PCL6_TEXT( _T("nSInt16 Box  -> %d %d %d %d\n"), 
			m_Data.nSInt16Box[0], m_Data.nSInt16Box[1], 
			m_Data.nSInt16Box[2], m_Data.nSInt16Box[3] );
		break;

	case PCL_DATATYPE_SINT32_BOX:
		DEBUG_PCL6_TEXT( _T("nSInt32 Box  -> %d %d %d %d\n"), 
			m_Data.nSInt32Box[0], m_Data.nSInt32Box[1], 
			m_Data.nSInt32Box[2], m_Data.nSInt32Box[3] );
		break;

	case PCL_DATATYPE_REAL32_BOX:
		DEBUG_PCL6_TEXT( _T("nReal32 Box  -> %f %f %f %f\n"), 
			m_Data.nReal32Box[0], m_Data.nReal32Box[1], 
			m_Data.nReal32Box[2], m_Data.nReal32Box[3] );
		break;

	// XY Values
	case PCL_DATATYPE_UBYTE_XY:
		DEBUG_PCL6_TEXT( _T("nByte XY     -> 0x%02x 0x%02x\n"), 
			m_Data.nByteXY[0], m_Data.nByteXY[1] );
		break;

	case PCL_DATATYPE_UINT16_XY:
		DEBUG_PCL6_TEXT( _T("nUInt16 XY   -> %d %d\n"), 
			m_Data.nUInt16XY[0], m_Data.nUInt16XY[1] );
		break;

	case PCL_DATATYPE_UINT32_XY:
		DEBUG_PCL6_TEXT( _T("dwUInt32 XY  -> %d %d\n"), 
			m_Data.dwUInt32XY[0], m_Data.dwUInt32XY[1] );
		break;

	case PCL_DATATYPE_SINT16_XY:
		DEBUG_PCL6_TEXT( _T("nSInt16 XY   -> %d %d\n"), 
			m_Data.nSInt16XY[0], m_Data.nSInt16XY[1] );
		break;

	case PCL_DATATYPE_SINT32_XY:
		DEBUG_PCL6_TEXT( _T("nSInt32 XY   -> %d %d\n"), 
			m_Data.nSInt32XY[0], m_Data.nSInt32XY[1] );
		break;

	case PCL_DATATYPE_REAL32_XY:
		DEBUG_PCL6_TEXT( _T("nReal32 XY   -> %f %f\n"), 
			m_Data.nReal32XY[0], m_Data.nReal32XY[1] );
		break;

	// No matching values
	default:
		DEBUG_PCL6_TEXT( _T("Invalid      -> 0x%02x\n"), m_nDataType );
		break;
	}
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL6DataType.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
