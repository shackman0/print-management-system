//////////////////////////////////////////////////////////////////////
// PrintProcessor.h : Header file for Print Processor DLL
//////////////////////////////////////////////////////////////////////

#ifndef PRINTPROCESSOR_H
#define PRINTPROCESSOR_H

//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////

#include "SpoolMemory.h"
#include "ComVector.h"
#include "WinPPI.h"

//////////////////////////////////////////////////////////////////////
// Global Variables
//////////////////////////////////////////////////////////////////////

extern HINSTANCE	theInstance;

//////////////////////////////////////////////////////////////////////
// Definitions
//////////////////////////////////////////////////////////////////////

#define PRINTPROCESSORDATA_SIGNATURE		0x4443	// 'DC' is the signature value

// Define flags for fsStatus field
#define PRINTPROCESSOR_ABORTED				0x0001
#define PRINTPROCESSOR_CLOSED				0x0004
#define PRINTPROCESSOR_PAUSED				0x0008

#define PRINTPROCESSOR_RESERVED 			0xFFF8

// Data types we support
#define PRINTPROCESSOR_TYPE_RAW 			0
#define PRINTPROCESSOR_TYPE_RAW_FF			1
#define PRINTPROCESSOR_TYPE_RAW_FF_AUTO		2
#define PRINTPROCESSOR_TYPE_EMF_40 			3
#define PRINTPROCESSOR_TYPE_EMF_50_1		4
#define PRINTPROCESSOR_TYPE_EMF_50_2		5
#define PRINTPROCESSOR_TYPE_EMF_50_3		6
#define PRINTPROCESSOR_TYPE_TEXT			7
#define PRINTPROCESSOR_TYPE_PSCRIPT1		8

#define PRINTPROCESSOR_TYPE_MAX_VALUE		(PRINTPROCESSOR_TYPE_PSCRIPT1+1)

//////////////////////////////////////////////////////////////////////
// Defines
//////////////////////////////////////////////////////////////////////

#define READ_BUFFER_SIZE					4096

//////////////////////////////////////////////////////////////////////
// Type Definitions
//////////////////////////////////////////////////////////////////////

typedef CComVector<BYTE>					Vector;
typedef CComVectorData<BYTE>				VectorData;
typedef CDefaultBuffer						CProcBuffer;

//////////////////////////////////////////////////////////////////////
// Structures Needed
//////////////////////////////////////////////////////////////////////

// PRINTPROCESSORDATA Structure
typedef struct _PRINTPROCESSORDATA
{
	DWORD		signature;
	DWORD		cb;
	struct		_PRINTPROCESSORDATA *pNext;
	DWORD		fsStatus;
	HANDLE		semPaused;
	DWORD		uDatatype;
	HANDLE		hPrinter;
	LPTSTR		pPrinterName;
	LPTSTR		pDocument;
	LPTSTR		pOutputFile;
	LPTSTR		pDatatype;
	LPTSTR		pParameters;
	DWORD		JobId;
	DWORD		Copies; 		// Number of copies to print
	DWORD		TabSize;
	HDC 		hDC;
	LPDEVMODE	pDevMode;
    LPTSTR		lpszPrinterNameFromOpenData;
} PRINTPROCESSORDATA, *LPPRINTPROCESSORDATA;

// PRINTPROCESSOROPENDATA Structure
typedef struct _PRINTPROCESSOROPENDATA
{
	LPDEVMODE	pDevMode;
    LPTSTR		pDatatype;
    LPTSTR		pParameters;
    LPTSTR		pDocumentName;
    DWORD		JobId;
    LPTSTR		pOutputFile;
    LPTSTR		pPrinterName;
} PRINTPROCESSOROPENDATA, *LPPRINTPROCESSOROPENDATA;

//////////////////////////////////////////////////////////////////////
// Export these functions
//////////////////////////////////////////////////////////////////////

BOOL	WINAPI	ClosePrintProcessor( HANDLE hPrintProcessor	);
BOOL	WINAPI	ControlPrintProcessor( HANDLE  hPrintProcessor, DWORD Command ); 
BOOL	WINAPI	EnumPrintProcessorDatatypes( LPTSTR pName, LPTSTR pPrintProcessorName, DWORD Level, LPBYTE pDatatypes, DWORD cbBuf, LPDWORD pcbNeeded, LPDWORD pcReturned );
HANDLE	WINAPI	OpenPrintProcessor(	LPTSTR pPrinterName, LPPRINTPROCESSOROPENDATA LPPRINTPROCESSOROPENDATA ); 
BOOL	WINAPI	PrintDocumentOnPrintProcessor( HANDLE hPrintProcessor, LPTSTR pDocumentName );
DWORD	WINAPI	GetPrintProcessorCapabilities( LPTSTR pValueName, DWORD dwAttributes, LPBYTE pData, DWORD dwSize, LPDWORD pcbNeeded );

//////////////////////////////////////////////////////////////////////
// Inline Functions
//////////////////////////////////////////////////////////////////////

// Validates memory blocks
inline LPPRINTPROCESSORDATA ValidateHandle( HANDLE hProcessorData )
{
	// Pick up the pointer
	LPPRINTPROCESSORDATA pData = (LPPRINTPROCESSORDATA)hProcessorData;

	// See if our signature exists in the suspected data region
	if ( (pData != NULL) && (pData->signature != PRINTPROCESSORDATA_SIGNATURE) )
	{
		// Bad pointer - return failed
		pData = NULL;
		::SetLastError( ERROR_INVALID_HANDLE );
	}
	return pData;
}

//////////////////////////////////////////////////////////////////////
// Debug routines
//////////////////////////////////////////////////////////////////////

#ifdef DISPLAY_STRUCTS

#define DEBUG_PROCESSOR(data)				DebugProcessorData(data)
#define DEBUG_PROCESSOR_OPEN(data)			DebugProcessorOpenData(data)

void DebugProcessorData( LPPRINTPROCESSORDATA pData );
void DebugProcessorOpenData( LPPRINTPROCESSOROPENDATA pData );

#else

#define DEBUG_PROCESSOR(data)				((void)(0))
#define DEBUG_PROCESSOR_OPEN(data)			((void)(0))

#endif // #ifdef DISPLAY_STRUCTS

//////////////////////////////////////////////////////////////////////

#endif // PRINTPROCESSOR_H

//////////////////////////////////////////////////////////////////////
// End of PrintProcessor.h
//////////////////////////////////////////////////////////////////////
