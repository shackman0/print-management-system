///////////////////////////////////////////////////////////////////////////////////////////////////////
// Iterator.h: interfaces for the various iteration classes.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_ITERATOR_H__F82C8770_E0F3_4EC1_A3CA_0E73C45D3F2D__INCLUDED_)
#define AFX_ITERATOR_H__F82C8770_E0F3_4EC1_A3CA_0E73C45D3F2D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CIterator Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename ARG_TYPE>
class CIterator  
{
// Attributes
protected:
	POSITION	m_Position;

// Construction / Destruction
protected:
	CIterator()
	{
		// Initialize the values
		m_Position = NULL;
	}

	virtual ~CIterator()						{	};

// Operations
public:
	virtual	void		MoveToStart()	= 0;
	virtual ARG_TYPE	GetValue()		= 0;
	bool				IsFinished() const		{ return ( m_Position == NULL );	};
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CListIterator Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_LIST

template<typename TYPE, typename ARG_TYPE>
class CListIterator : public CIterator<ARG_TYPE>
{
// TypeDefs
protected:
	typedef CCMSList<TYPE,ARG_TYPE>		CListType;

// Attributes
protected:
	const CListType*	m_pList;

// Construction / Destruction
public:
	CListIterator( const CListType* pList )
	{
		// Check assumptions
		_ASSERTE( pList != NULL );

		// Assign the variables
		m_pList = pList;

		// Move to the start
		MoveToStart();
	}

	virtual ~CListIterator()					{	};

// Operations
public:
	virtual	void		MoveToStart()			{ m_Position = m_pList->GetHeadPosition();	};
	virtual ARG_TYPE	GetValue()				{ return m_pList->GetNext(m_Position);		};
};

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CRevListIterator Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_LIST

template<typename TYPE, typename ARG_TYPE>
class CRevListIterator : public CIterator<ARG_TYPE>
{
// TypeDefs
protected:
	typedef CCMSList<TYPE,ARG_TYPE>		CListType;

// Attributes
protected:
	const CListType*	m_pList;

// Construction / Destruction
public:
	CRevListIterator( const CListType* pList )
	{
		// Check assumptions
		_ASSERTE( pList != NULL );

		// Assign the variables
		m_pList = pList;

		// Move to the start
		MoveToStart();
	}

	virtual ~CRevListIterator()					{	};

// Operations
public:
	virtual	void		MoveToStart()			{ m_Position = m_pList->GetTailPosition();	};
	virtual ARG_TYPE	GetValue()				{ return m_pList->GetPrev(m_Position);		};
};

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMapIterator Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_MAP

template<class KEY, class ARG_KEY, class VALUE, class ARG_VALUE>
class CMapIterator : public CIterator<ARG_VALUE>
{
// TypeDefs
protected:
	typedef CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>		CMapType;

// Attributes
protected:
	const CMapType*	m_pMap;

// Construction / Destruction
public:
	CMapIterator( const CMapType* pMap )
	{
		// Check assumptions
		_ASSERTE( pMap != NULL );

		// Assign the variables
		m_pMap = pMap;

		// Move to the start
		MoveToStart();
	}

	virtual ~CMapIterator()					{	};

// Operations
public:
	virtual	void		MoveToStart()		{ m_Position = m_pMap->GetStartPosition();	};
	virtual ARG_VALUE	GetValue()			{ return m_pMap->GetNextAssoc(m_Position);	};
	void				GetValues( KEY& rKey, VALUE& rValue )
	{ 
		m_pMap->GetNextAssoc( m_Position, rKey, rValue );
	}
};

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_ITERATOR_H__F82C8770_E0F3_4EC1_A3CA_0E73C45D3F2D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of Iterator.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
