///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobPropertyNames.h
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTJOBPROPERTYNAMES_H__18ED04F8_7062_11D3_A3DE_00105A1C588D__INCLUDED_)
#define AFX_PRINTJOBPROPERTYNAMES_H__18ED04F8_7062_11D3_A3DE_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Print Job Property Names
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Document values
#define	PRINTJOB_PROP_PRINTJOB_DOCUMENT					OLESTR("Document")
#define	PRINTJOB_PROP_PRINTJOB_PAGECOUNT				OLESTR("PageCount")
#define PRINTJOB_PROP_PRINTJOB_PAGESPERPAGE				OLESTR("PagesPerPage")
#define	PRINTJOB_PROP_PRINTJOB_COPIES					OLESTR("Copies")
#define	PRINTJOB_PROP_PRINTJOB_ORIENTATION				OLESTR("IsOrientationLandscape")
#define	PRINTJOB_PROP_PRINTJOB_QUALITY					OLESTR("Quality")
#define	PRINTJOB_PROP_PRINTJOB_COLOR					OLESTR("Color")
#define	PRINTJOB_PROP_PRINTJOB_DUPLEX					OLESTR("Duplex")
#define	PRINTJOB_PROP_PRINTJOB_COLLATED					OLESTR("Collated")
#define PRINTJOB_PROP_PRINTJOB_SORT						OLESTR("Sort")
#define PRINTJOB_PROP_PRINTJOB_STAPLE					OLESTR("Staple")
#define PRINTJOB_PROP_PRINTJOB_HOLEPUNCH				OLESTR("HolePunch")
#define PRINTJOB_PROP_PRINTJOB_DEVICENAME				OLESTR("DeviceName")
#define PRINTJOB_PROP_PRINTJOB_SCALE					OLESTR("Scale")
#define PRINTJOB_PROP_PRINTJOB_DEFAULTSOURCE			OLESTR("DefaultSource")
#define PRINTJOB_PROP_PRINTJOB_DATATYPE					OLESTR("DataType")
#define PRINTJOB_PROP_PRINTJOB_SUBMITTED				OLESTR("Submitted")
#define PRINTJOB_PROP_PRINTJOB_SIZE						OLESTR("JobSize")

// User values
#define PRINTJOB_PROP_USER_NAME							OLESTR("UserName")
#define PRINTJOB_PROP_USER_DOMAIN						OLESTR("UserDomain")

// Workstation values
#define PRINTJOB_PROP_WORKSTATION						OLESTR("Workstation")

// Page Size values
#define	PRINTJOB_PROP_PAGESIZE_FORM						OLESTR("Form")
#define PRINTJOB_PROP_PAGESIZE_PAPERSIZE				OLESTR("PaperSize")
#define PRINTJOB_PROP_PAGESIZE_PAPERLENGTH				OLESTR("PaperLength")
#define PRINTJOB_PROP_PAGESIZE_PAPERWIDTH				OLESTR("PaperWidth")

// Printer values
#define	PRINTJOB_PROP_PRINTER_NAME						OLESTR("PrinterName")
#define	PRINTJOB_PROP_PRINTER_SERVERNAME				OLESTR("ServerName")
#define PRINTJOB_PROP_PRINTER_SERVERDOMAIN				OLESTR("ServerDomain")
#define	PRINTJOB_PROP_PRINTER_SHARENAME					OLESTR("ShareName")
#define	PRINTJOB_PROP_PRINTER_COMMENT					OLESTR("Comment")
#define	PRINTJOB_PROP_PRINTER_DRIVER					OLESTR("Driver")
#define PRINTJOB_PROP_PRINTER_PRINTJOBSERVER			OLESTR("PrintTrackingServer")
#define PRINTJOB_PROP_PRINTER_PORT						OLESTR("PrinterPort")

// Status Values
#define PRINTJOB_PROP_STATUS_PAGECOUNT					OLESTR("IsPageCountReliable")

// Release Station Values
#define PRINTJOB_PROP_RS_JOBID							OLESTR("ReleaseStationJobID")
#define PRINTJOB_PROP_RS_TIMESTAMP						OLESTR("ReleaseStationTimeStamp")
#define PRINTJOB_PROP_RS_SIMPLEXPAGES					OLESTR("ReleaseStationSimplexPages")
#define PRINTJOB_PROP_RS_DUPLEXPAGES					OLESTR("ReleaseStationDuplexPages")
#define PRINTJOB_PROP_RS_PASSWORD						OLESTR("ReleaseStationPassword")
#define PRINTJOB_PROP_RS_JOBDESCRIPTION					OLESTR("ReleaseStationJobDescription")

// Other values
#define PRINTJOB_PROP_OTHER_ACCOUNTID					OLESTR("AccountID")
#define PRINTJOB_PROP_OTHER_RAWDATA						OLESTR("RawData")
#define PRINTJOB_PROP_OTHER_RAWDATAFILE					OLESTR("RawDataFile")
#define PRINTJOB_PROP_OTHER_ALLOWPRINT					OLESTR("AllowPrint")
#define PRINTJOB_PROP_OTHER_CLIENTINTERFACE				OLESTR("ClientInterface")
#define PRINTJOB_PROP_OTHER_PRINTERJOBID				OLESTR("PrinterJobID")
#define PRINTJOB_PROP_OTHER_PRICE						OLESTR("JobPrice")
#define PRINTJOB_PROP_OTHER_AMOUNTPAID					OLESTR("AmountPaid")
#define PRINTJOB_PROP_OTHER_POPUP_ADDRESS				OLESTR("PopUpAddress")
#define PRINTJOB_PROP_OTHER_LOGICALPRINTERID			OLESTR("LogicalPrinterID")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Default Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define PRINTJOB_DEFAULT_VALUE_PRINTJOBID				((long)0)
#define PRINTJOB_DEFAULT_VALUE_PAGECOUNT				((long)1)
#define PRINTJOB_DEFAULT_VALUE_PAGESPERPAGE				((long)1)
#define PRINTJOB_DEFAULT_VALUE_COPIES					((long)1)
#define PRINTJOB_DEFAULT_VALUE_QUALITY					((long)0)
#define PRINTJOB_DEFAULT_VALUE_SCALE					((long)100)
#define PRINTJOB_DEFAULT_VALUE_ORIENTATION				VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_COLOR					VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_DUPLEX					VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_COLLATED					VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_STAPLE					VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_SORT						VARIANT_FALSE
#define PRINTJOB_DEFAULT_VALUE_HOLEPUNCH				VARIANT_FALSE

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTJOBPROPERTYNAMES_H__18ED04F8_7062_11D3_A3DE_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobPropertyNames.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
