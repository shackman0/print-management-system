///////////////////////////////////////////////////////////////////////////////////////////////////////
// RevertToSelf.h: interface for the CRevertToSelf class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_REVERTTOSELF_H__02194364_828C_11D3_A3EB_00105A1C588D__INCLUDED_)
#define AFX_REVERTTOSELF_H__02194364_828C_11D3_A3EB_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CSecurityToken Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CSecurityToken
{
// Attributes
private:
	HANDLE	m_hSecurityToken;

// Construction / Destruction
public:
	CSecurityToken()										{ m_hSecurityToken = INVALID_HANDLE_VALUE; };
	~CSecurityToken()
	{
		// If the handle is in use, then close it.
		if ( m_hSecurityToken != INVALID_HANDLE_VALUE )
		{
			//DEBUG_FAIL( ::CloseHandle(m_hSecurityToken) );
			::CloseHandle( m_hSecurityToken );
		}
	}

// Operators
public:
	operator HANDLE()										{ return m_hSecurityToken; };
	operator PHANDLE()										{ return &m_hSecurityToken; };

// Operations
public:
	BOOL IsValid() const									{ return ( (m_hSecurityToken != INVALID_HANDLE_VALUE) ? TRUE : FALSE ); };
	BOOL GetCurrentContext()								{ return GetCurrentContext(*this); };
	BOOL GetUserAndDomainNames( CString& strUserName, CString& strDomainName )
	{
		return GetUserAndDomainNames( *this, strUserName, strDomainName );
	}

// Static Operations
public:
	static BOOL GetCurrentContext( PHANDLE pSecurityToken );
	static BOOL	GetUserAndDomainNames( HANDLE hSecurityToken, CString& strUserName, CString& strDomainName );

// Debug Code
#ifdef _DEBUG
public:
	void Dump();
#endif

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CRevertToSelf Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CRevertToSelf
{
// Attributes
private:
	BOOL				m_bIsReverted;
	CSecurityToken		m_stContext;

// Construction / Destruction
public:
	CRevertToSelf();
	virtual ~CRevertToSelf();

// Data Access
public:
	CSecurityToken&		GetToken()			{ return m_stContext; };

// Operations
public:
	BOOL	Revert();
	BOOL	ImpersonateAgain();

// Debug Code
#ifdef _DEBUG
public:
	void Dump();
#endif

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_REVERTTOSELF_H__02194364_828C_11D3_A3EB_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of RevertToSelf.h
///////////////////////////////////////////////////////////////////////////////////////////////////////