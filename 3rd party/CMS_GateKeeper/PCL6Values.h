//////////////////////////////////////////////////////////////////////
// PCL6Values.h: enumerated values for parsing PCL6.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL6VALUES_H__BAB08E69_EA0D_11D3_A430_00105A1C588D__INCLUDED_)
#define AFX_PCL6VALUES_H__BAB08E69_EA0D_11D3_A430_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// PCL6 Operators
//////////////////////////////////////////////////////////////////////

enum pcl6_Operator
{
	pcl6_opArcPath						= 0x91,
	pcl6_opBeginChar					= 0x52,
	pcl6_opBeginFontHeader				= 0x4f,
	pcl6_opBeginImage					= 0xb0,
	pcl6_opBeginPage					= 0x43,
	pcl6_opBeginRastPattern				= 0xb3,
	pcl6_opBeginScan					= 0xb6,
	pcl6_opBeginSession					= 0x41,
	pcl6_opBeginStream					= 0x5b,
	pcl6_opBezierPath					= 0x93,
	pcl6_opBezierRelPath				= 0x95,
	pcl6_opChord						= 0x96,
	pcl6_opChordPath					= 0x97,
	pcl6_opCloseDataSource				= 0x49,
	pcl6_opCloseSubPath					= 0x84,
	pcl6_opComment						= 0x47,
	pcl6_opEllipse						= 0x98,
	pcl6_opEllipsePath					= 0x99,
	pcl6_opEndChar						= 0x54,
	pcl6_opEndFontHeader				= 0x51,
	pcl6_opEndImage						= 0xb2,
	pcl6_opEndPage						= 0x44,
	pcl6_opEndRastPattern				= 0xb5,
	pcl6_opEndScan						= 0xb8,
	pcl6_opEndSession					= 0x42,
	pcl6_opEndStream					= 0x5d,
	pcl6_opExecStream					= 0x5e,
	pcl6_opLinePath						= 0x9b,
	pcl6_opLineRelPath					= 0x9d,
	pcl6_opNewPath						= 0x85,
	pcl6_opOpenDataSource				= 0x48,
	pcl6_opPaintPath					= 0x86,
	pcl6_opPie							= 0x9e,
	pcl6_opPiePath						= 0x9f,
	pcl6_opPopGS						= 0x60,
	pcl6_opPushGS						= 0x61,
	pcl6_opReadChar						= 0x53,
	pcl6_opReadFontHeader				= 0x50,
	pcl6_opReadImage					= 0xb1,
	pcl6_opReadRastPattern				= 0xb4,
	pcl6_opReadStream					= 0x5c,
	pcl6_opRectangle					= 0xa0,
	pcl6_opRectanglePath				= 0xa1,
	pcl6_opRemoveFont					= 0x55,
	pcl6_opSetCharAttributes			= 0x56,
	pcl6_opRemoveStream					= 0x5f,
	pcl6_opRoundRectangle				= 0xa2,
	pcl6_opRoundRectanglePath			= 0xa3,
	pcl6_opScanLineRel					= 0xb9,
	pcl6_opSetClipReplace				= 0x62,
	pcl6_opSetBrushSource				= 0x63,
	pcl6_opSetCharAngle					= 0x64,
	pcl6_opSetCharBoldValue				= 0x7d,
	pcl6_opSetCharScale					= 0x65,
	pcl6_opSetCharShear					= 0x66,
	pcl6_opSetCharSubMode				= 0x81,
	pcl6_opSetClipIntersect				= 0x67,
	pcl6_opSetClipMode					= 0x7f,
	pcl6_opSetClipRectangle				= 0x68,
	pcl6_opSetClipToPage				= 0x69,
	pcl6_opSetColorSpace				= 0x6a,
	pcl6_opSetCursor					= 0x6b,
	pcl6_opSetCursorRel					= 0x6c,
	pcl6_opSetHalftoneMethod			= 0x6d,
	pcl6_opSetFillMode					= 0x6e,
	pcl6_opSetFont						= 0x6f,
	pcl6_opSetLineCap					= 0x71,
	pcl6_opSetLineDash					= 0x70,
	pcl6_opSetLineJoin					= 0x72,
	pcl6_opSetMiterLimit				= 0x73,
	pcl6_opSetPageDefaultCTM			= 0x74,
	pcl6_opSetPageOrigin				= 0x75,
	pcl6_opSetPageRotation				= 0x76,
	pcl6_opSetPageScale					= 0x77,
	pcl6_opSetPathToClip				= 0x80,
	pcl6_opSetPatternTxMode				= 0x78,
	pcl6_opSetPenSource					= 0x79,
	pcl6_opSetPenWidth					= 0x7a,
	pcl6_opSetROP						= 0x7b,
	pcl6_opSetSourceTxMode				= 0x7c,
	pcl6_opText							= 0xa8,
	pcl6_opTextPath						= 0xa9
};

//////////////////////////////////////////////////////////////////////
// 
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: ColorDepth
//////////////////////////////////////////////////////////////////////

enum pcl6_ColorDepth
{
	pcl6_e1Bit						= 0,
	pcl6_e4Bit						= 1,
	pcl6_e8Bit						= 2
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: ColorMapping
//////////////////////////////////////////////////////////////////////

enum pcl6_ColorMapping
{
	pcl6_eDirectPixel				= 0,
	pcl6_eIndexedPixel				= 1
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: ColorSpace
//////////////////////////////////////////////////////////////////////

enum pcl6_ColorSpace
{
	pcl6_eGray						= 1,
	pcl6_eRGB						= 2,
	pcl6_eSRGB						= 6
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: DuplexPageMode
//////////////////////////////////////////////////////////////////////

enum pcl6_DuplexPageMode
{
	pcl6_eDuplexHorizontalBinding	= 0,
	pcl6_eDuplexVerticalBinding		= 1
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: DuplexPageSide
//////////////////////////////////////////////////////////////////////

enum pcl6_DuplexPageSide
{
	pcl6_eFrontMediaSide			= 0,
	pcl6_eBackMediaSide				= 1
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: SimplexPageMode
//////////////////////////////////////////////////////////////////////

enum pcl6_SimplePageMode
{
	pcl6_eSimplexFrontSide			= 0
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: Measure
//////////////////////////////////////////////////////////////////////

enum pcl6_Measure
{
	pcl6_eInch						= 0,
	pcl6_eMillimeter				= 1,
	pcl6_eTenthsOfAMillimeter		= 2
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: MediaSize					
//////////////////////////////////////////////////////////////////////

enum pcl6_MediaSize					
{									
	pcl6_eLetter					= 0,
	pcl6_eLegal						= 1,
	pcl6_eA4						= 2,
	pcl6_eExec						= 3,
	pcl6_eLedger					= 4,
	pcl6_eA3						= 5,
	pcl6_eCOM10Envelope				= 6,
	pcl6_eMonarchEnvelope			= 7,
	pcl6_eC5Envelope				= 8,
	pcl6_eDLEnvelope				= 9,
	pcl6_eJB4						= 10,
	pcl6_eJB5						= 11,
	pcl6_eB5Envelope				= 12,
	// The PCL-XL 2.0 ref does not list a MediaSize with value 13
	pcl6_eJPostcard					= 14,
	pcl6_eJDoublePostcard			= 15,
	pcl6_eA5						= 16,
	pcl6_eA6						= 17,
	pcl6_eJB6						= 18
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: MediaSource
//////////////////////////////////////////////////////////////////////

enum pcl6_MediaSource
{
	pcl6_eDefaultSource				= 0,
	pcl6_eAutoSelect				= 1,
	pcl6_eManualFeed				= 2,
	pcl6_eMultiPurposeTray			= 3,
	pcl6_eUpperCassette				= 4,
	pcl6_eLowerCassette				= 5,
	pcl6_eEnvelopeTray				= 6,
	pcl6_eThirdCassette				= 7
};

//////////////////////////////////////////////////////////////////////
// PCL6 Enumerated Type: Orientation
//////////////////////////////////////////////////////////////////////

enum pcl6_Orientation
{
	pcl6_ePortraitOrientation		= 0,
	pcl6_eLandscapeOrientation		= 1,
	pcl6_eReversePortrait			= 2,
	pcl6_eReverseLandscape			= 3
};

#endif // !defined(AFX_PCL6VALUES_H__BAB08E69_EA0D_11D3_A430_00105A1C588D__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of PCL6Values.h
//////////////////////////////////////////////////////////////////////