//////////////////////////////////////////////////////////////////////
// DevMode.cpp: implementation of the CDevMode class.
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "DevMode.h"

//////////////////////////////////////////////////////////////////////
// Debug Information
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

//////////////////////////////////////////////////////////////////////
// Defines
//////////////////////////////////////////////////////////////////////

#define	DEVMODE_BLOCK_SIZE				ROUND_TO_NEAREST(sizeof(DEVMODE), 4)
#define DEVMODE_BLOCK_COUNT				8

//////////////////////////////////////////////////////////////////////
// Static Variables
//////////////////////////////////////////////////////////////////////

CReservedBlocks		CDevMode::m_DevModeBlocks( DEVMODE_BLOCK_SIZE, DEVMODE_BLOCK_COUNT );

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

	// Defined Inline

//////////////////////////////////////////////////////////////////////
// Memory Operations
//////////////////////////////////////////////////////////////////////

void CDevMode::AllocBuffer( const WORD& wSize, const WORD& wDriverExtra )
{
	DWORD dwNewSize = (wSize + wDriverExtra);

	// Release the existing buffer (if necessary)
	ReleaseBuffer();

	// Check Assumptions
	_ASSERTE( wSize > 0 );
	_ASSERTE( m_lpDevMode == NULL );

	if ( dwNewSize <= DEVMODE_BLOCK_SIZE )
	{
		// Use the reserved blocks
		m_lpDevMode = reinterpret_cast<LPDEVMODE>(m_DevModeBlocks.Alloc());
	}
	else
	{
		// Allocate a buffer
		m_lpDevMode = reinterpret_cast<LPDEVMODE>(new BYTE[dwNewSize]);
	}

	// Clear out the memory
	::ZeroMemory( m_lpDevMode, dwNewSize );

	// Set the size values
	m_lpDevMode->dmSize			= wSize;
	m_lpDevMode->dmDriverExtra	= wDriverExtra;
}

void CDevMode::ReleaseBuffer()
{
	if ( m_lpDevMode != NULL )
	{
		DWORD dwSize = (m_lpDevMode->dmSize + m_lpDevMode->dmDriverExtra);

		if ( dwSize <= DEVMODE_BLOCK_SIZE )
		{
			// Free the reserved block
			m_DevModeBlocks.Free( m_lpDevMode );
		}
		else
		{
			// Free the buffer
			delete [] (LPBYTE)m_lpDevMode;
		}

		m_lpDevMode = NULL;
	}
}

//////////////////////////////////////////////////////////////////////
// Special Operations
//////////////////////////////////////////////////////////////////////

void CDevMode::SetPaperSizeString( const PaperSizeInfo* pInfo, LPCSTR lpszPaperSize )
{
	// Check the assumptions
	_ASSERTE( pInfo != NULL );
	_ASSERTE( lpszPaperSize != NULL );

	// Loop through each item in the array
	while( pInfo->lpszFormName != NULL )
	{
		// Check to see if the form name matches
		if ( stricmp(pInfo->lpszFormName, lpszPaperSize) == 0 )
		{
			// Set the paper size
			SetPaperSize( pInfo->nDevModeValue );

			// Quit looking
			break;
		}

		// Move to the next record
		pInfo++;
	}

	// Uses ATL Conversion Macros
	USES_CONVERSION;

	// Set the form name
	SetFormName( A2CT(lpszPaperSize) );
}

//////////////////////////////////////////////////////////////////////
// End of DevMode.cpp
//////////////////////////////////////////////////////////////////////