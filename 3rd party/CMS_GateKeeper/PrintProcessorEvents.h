/////////////////////////////////////////////////////////////////
// Languages
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// Categories
/////////////////////////////////////////////////////////////////
//
//  Values are 32 bit values layed out as follows:
//
//   3 3 2 2 2 2 2 2 2 2 2 2 1 1 1 1 1 1 1 1 1 1
//   1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0 9 8 7 6 5 4 3 2 1 0
//  +---+-+-+-----------------------+-------------------------------+
//  |Sev|C|R|     Facility          |               Code            |
//  +---+-+-+-----------------------+-------------------------------+
//
//  where
//
//      Sev - is the severity code
//
//          00 - Success
//          01 - Informational
//          10 - Warning
//          11 - Error
//
//      C - is the Customer code flag
//
//      R - is a reserved bit
//
//      Facility - is the facility code
//
//      Code - is the facility's status code
//
//
// Define the facility codes
//


//
// Define the severity codes
//


//
// MessageId: EVT_CAT_GENERAL
//
// MessageText:
//
//  General
//
#define EVT_CAT_GENERAL                  ((WORD)0x00000001L)

//
// MessageId: EVT_CAT_INTERNAL_ERROR
//
// MessageText:
//
//  System
//
#define EVT_CAT_INTERNAL_ERROR           ((WORD)0x00000002L)

/////////////////////////////////////////////////////////////////
// Messsages
/////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////
// General
//
// MessageId: EVT_MSG_OPP_INVALID_PARAM
//
// MessageText:
//
//  OpenPrintProcessor received an invalid parameter.
//
#define EVT_MSG_OPP_INVALID_PARAM        ((DWORD)0x00000100L)

//
// MessageId: EVT_MSG_OPP_INVALID_DATATYPE
//
// MessageText:
//
//  OpenPrintProcessor received an invalid datatype.
//
#define EVT_MSG_OPP_INVALID_DATATYPE     ((DWORD)0x00000101L)

//
// MessageId: EVT_MSG_OPP_PRINTER_OPEN_FAIL
//
// MessageText:
//
//  OpenPrintProcessor could not open the specified printer because of the following error: %1
//
#define EVT_MSG_OPP_PRINTER_OPEN_FAIL    ((DWORD)0x00000102L)

//
// MessageId: EVT_MSG_PPP_INVALID_PARAM
//
// MessageText:
//
//  PrintDocumentOnPrintProcessor received an invalid parameter.
//
#define EVT_MSG_PPP_INVALID_PARAM        ((DWORD)0x00000103L)

//
// MessageId: EVT_MSG_PPP_INVALID_DATATYPE
//
// MessageText:
//
//  PrintDocumentOnPrintProcessor received an invalid datatype.
//
#define EVT_MSG_PPP_INVALID_DATATYPE     ((DWORD)0x00000104L)

//
// MessageId: EVT_MSG_SERVER_FAIL_UNKNOWN
//
// MessageText:
//
//  Could not authorize the print job because of the following error: "%1".  The print job will be cancelled.
//
#define EVT_MSG_SERVER_FAIL_UNKNOWN      ((DWORD)0x00000105L)

//
// MessageId: EVT_MSG_SERVER_FAIL_COM
//
// MessageText:
//
//  Could not authorize the print job because of the following error: "%1".  The print job will be cancelled.
//
#define EVT_MSG_SERVER_FAIL_COM          ((DWORD)0x00000106L)

//
// MessageId: EVT_MSG_SERVER_NAME_MISSING
//
// MessageText:
//
//  The server has not been specified.  Please make sure that the Print Server software has been properly installed, and that all users have read access to that section of the registry.
//
#define EVT_MSG_SERVER_NAME_MISSING      ((DWORD)0x00000107L)

//
// MessageId: EVT_MSG_SERVER_FAIL_OUTOFMEMORY
//
// MessageText:
//
//  Could not authorize the print job because it is too large for the spooler to handle.  Increase the available resources before printing large jobs.
//
#define EVT_MSG_SERVER_FAIL_OUTOFMEMORY  ((DWORD)0x00000108L)

//
// MessageId: EVT_MSG_SERVER_FAIL_ACCESSDENIED
//
// MessageText:
//
//  Could not authorize the print job because of the following error: "%1".  This is usually caused by the Print Tracking service not being started.  The print job will be cancelled.
//
#define EVT_MSG_SERVER_FAIL_ACCESSDENIED ((DWORD)0x00000109L)

//
// MessageId: EVT_MSG_CAUGHT_EXCEPTION
//
// MessageText:
//
//  The print processor handled exception %1.  The print job will be cancelled.
//
#define EVT_MSG_CAUGHT_EXCEPTION         ((DWORD)0x0000010AL)

//
// MessageId: EVT_MSG_CAUGHT_CANCELPRINT_EXCEPTION
//
// MessageText:
//
//  The print processor handled exception %1 while cancelling the print job!  If the print queue is not releasing print jobs, please restart the service.
//
#define EVT_MSG_CAUGHT_CANCELPRINT_EXCEPTION ((DWORD)0x0000010BL)

//
// MessageId: EVT_MSG_UPDATE_PRINTJOB_SUCCESS
//
// MessageText:
//
//  Successfully updated page count.
//
#define EVT_MSG_UPDATE_PRINTJOB_SUCCESS  ((DWORD)0x0000010CL)

//
// MessageId: EVT_MSG_UPDATE_PRINTJOB_FAILED
//
// MessageText:
//
//  Failed to updated page count.
//
#define EVT_MSG_UPDATE_PRINTJOB_FAILED   ((DWORD)0x0000010DL)

