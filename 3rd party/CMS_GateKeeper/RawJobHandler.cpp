///////////////////////////////////////////////////////////////////////////////////////////////////////
// RawJobHandler.cpp : implementation of the CRawJobHandler.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "RawJobHandler.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Prepare Request Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CRawJobHandler::PrepareRequest()
{
	if ( m_pTextJob != NULL )
	{
		// Set the page count
		m_ParserData.SetPageCount( m_pTextJob->GetPageCount() );

		// Set the data type
		m_ParserData.SetDataType( DATATYPE_TEXTJOB );
		DEBUG_OBJ_TEXT( _T("Text Page Count: %d\n"), m_ParserData.GetPageCount() );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrepareJob Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CRawJobHandler::PrepareJob()
{
	ReadRawJob();
	ParseRawData();

	// If the parser fails, treat it like a text job
	if ( m_bParsed == false )
	{
		// Create the device context (if necessary)
		if ( m_pPrintProcessorData->hDC == NULL )
		{
			DEBUG_FAIL( m_pPrintProcessorData->hDC = ::CreateDC(_T(""), 
				GetPrinterInfo()->pPrinterName, NULL, m_pPrintProcessorData->pDevMode) );
		}

		// Create the access object
		VectorData		vdRawData(*m_pRawData);

		_ASSERTE( vdRawData.Length() > 0 );
		_ASSERTE( m_pTextJob == NULL );

		m_pTextJob = new CTextJob;
		m_pTextJob->PrepareTextJob( m_lpszDocumentName, m_pPrintProcessorData, vdRawData );
		m_bParsed = true;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Print Job Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CRawJobHandler::PrintJob()
{
	BOOL bReturnMe = FALSE;

	_ASSERTE( m_pRawData != NULL );
	if ( (m_pRawData != NULL) )
	{
		VectorData	vdRawData(*m_pRawData);

		if ( vdRawData.Length() > 0 )
		{
			if ( m_pTextJob == NULL )
			{
				bReturnMe = PrintRawJob(vdRawData);
			}
			else
			{
				bReturnMe = PrintTextJob(vdRawData);
			}
		}
		DEBUG_ELSE_TEXT( _T("Raw Data Is Empty!\n") );
	}
	DEBUG_OBJ_TEXT( _T("Finished Printing Job!\n") );

	// Return success or failure
	return bReturnMe;
}


BOOL CRawJobHandler::PrintRawJob( VectorData& vdRawData )
{
	BOOL bReturnMe = FALSE;

	// Document Info
	DOC_INFO_1	DocInfo;
	::ZeroMemory( &DocInfo, sizeof(DocInfo) );
	DocInfo.pDocName	= m_pPrintProcessorData->pDocument;
	DocInfo.pOutputFile = m_pPrintProcessorData->pOutputFile;
	DocInfo.pDatatype	= m_pPrintProcessorData->pDatatype;
	
	// Let the printer know we are starting a new document
	if ( ::StartDocPrinter(m_pPrintProcessorData->hPrinter, 1, (LPBYTE)&DocInfo) )
	{
		bool		bStartPage			= false;

		try
		{
			DWORD		dwCopies			= m_pPrintProcessorData->Copies;
			LPBYTE		lpBuffer;
			DWORD		dwBytesWritten;
			DWORD		dwTotalBytesWritten;
			DWORD		dwBufferSize;

			// Print the data pData->Copies times
			dwCopies = m_pPrintProcessorData->Copies;

			CheckJobStatus();
			while ( (dwCopies--) && (m_bJobAborted == false) )
			{
				dwBufferSize		= GetMin( vdRawData.Length(), (DWORD)READ_BUFFER_SIZE );
				dwBytesWritten		= 0;
				dwTotalBytesWritten	= 0;
				lpBuffer			= &(vdRawData[0]);

				DEBUG_OBJ_TEXT( _T("Printing Copy #%d\n"), dwCopies+1 );

				THROW_WIN_FAIL( ::StartPagePrinter(m_pPrintProcessorData->hPrinter) );
				bStartPage = true;

				// Check the job's status
				CheckJobStatus();

				// Write the data to the printer
				while ( ::WritePrinter(m_pPrintProcessorData->hPrinter, lpBuffer, dwBufferSize, 
					&dwBytesWritten) && dwBytesWritten && (m_bJobAborted == false) )
				{
					dwTotalBytesWritten += dwBytesWritten;
					lpBuffer			+= dwBytesWritten;
					dwBufferSize		= GetMin( vdRawData.Length() - dwTotalBytesWritten, 
													(DWORD)READ_BUFFER_SIZE );
					CheckJobStatus();
				}

				bStartPage = false;
				THROW_WIN_FAIL( ::EndPagePrinter(m_pPrintProcessorData->hPrinter) );

				DEBUG_OBJ_TEXT( _T("Wrote %d out of %d Bytes!\n"), dwTotalBytesWritten, 
									vdRawData.Length() );
			} // While copies to print

			// If the job has been aborted, update the values for pages and copies
			if ( m_bJobAborted )
			{
				// TODO : Make this more accurate
				m_dwCopiesPrinted = dwCopies+1;
				ParseRawData( dwTotalBytesWritten );
				m_dwPagesPrinted = m_ParserData.GetPageCount();
			}

			bReturnMe = TRUE;
		}
		catch ( ... )
		{
			DEBUG_LAST_ERROR();
			DEBUG_OBJ_TEXT( _T("Unknown Exception!\n") );

			// If in the middle of a page, make sure to end the page
			if ( bStartPage == true )
			{
				DEBUG_FAIL( ::EndPagePrinter(m_pPrintProcessorData->hPrinter) );
			}

			// The printer MUST be notified that the printing is done!
			DEBUG_FAIL( ::EndDocPrinter(m_pPrintProcessorData->hPrinter) );

			// Pass the exception on
			throw;
		}

		// Let the printer know that the printing is done
		DEBUG_OBJ_TEXT( _T("Ending Print Job\n") );
		DEBUG_FAIL( ::EndDocPrinter(m_pPrintProcessorData->hPrinter) );
		DEBUG_OBJ_TEXT( _T("Print Job has ended!\n") );
	}
	DEBUG_ELSE();

	DEBUG_OBJ_TEXT( _T("Print Raw Job %s\n"), ( bReturnMe ? _T("Succeeded") : _T("Failed") ) );

	// Return success or failure
	return bReturnMe;
}

BOOL CRawJobHandler::PrintTextJob( VectorData& vdRawData )
{
	// Check assumptions
	_ASSERTE( m_pTextJob != NULL );

	DWORD		dwPagesPerCopy	= m_pTextJob->GetPageCount();				// Parsed number of pages per copy
	DWORD		dwPrinted		= m_pTextJob->PrintTextJob( vdRawData );	// Actual number of pages printed

	// Treat the RAW data as a Text Job
	DEBUG_OBJ_TEXT( _T("Printing RAW Job as Text!  %d Pages, %d Copies\n"), 
		dwPagesPerCopy, m_pPrintProcessorData->Copies );

	// If the pages printed doesn't match what is expected, make sure it gets updated
	if ( (dwPagesPerCopy * m_pPrintProcessorData->Copies) != dwPrinted )
	{
		m_bJobAborted		= true;
		m_dwPagesPrinted	= dwPrinted;
		m_dwCopiesPrinted	= 1;
	}
	DEBUG_OBJ_TEXT( _T("Print Raw Job As Text %s (%d Total Pages Printed)\n"),
		( ((dwPagesPerCopy * m_pPrintProcessorData->Copies) == dwPrinted)
				? _T("Succeeded") : _T("Failed") ), dwPrinted );

	// Return success
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of RawJobHandler.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////