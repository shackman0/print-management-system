///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL6DataType.h: interface for the CPCL6DataType class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL6DATATYPE_H__4151461E_65F8_4287_84BA_563D053C7D3B__INCLUDED_)
#define AFX_PCL6DATATYPE_H__4151461E_65F8_4287_84BA_563D053C7D3B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintJobBuffer.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_PCL6_DEBUG
	#define	DEBUG_PCL6_TEXT			DEBUG_TEXT
	#define DEBUG_PCL6_DUMP(d)		((d).Dump())
#else
	#define DEBUG_PCL6_TEXT			((void)(0))
	#define DEBUG_PCL6_DUMP(d)		((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL6DataType Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma pack(push,PCL6DataType,1)

class CPCL6DataType  
{
// TypeDefs
private:
	typedef union
	{
		// Regular Values
		BYTE	nByte;
		WORD	nUInt16;
		DWORD	dwUInt32;
		short	nSInt16;
		long	nSInt32;
		float	nReal32;
		// Array Values
		BYTE*	pByte;
		WORD*	pUInt16;
		DWORD*	pUInt32;
		short*	pSInt16;
		long*	pSInt32;
		float*	pReal32;
		// Box Values
		BYTE	nByteBox[4];
		WORD	nUInt16Box[4];
		DWORD	dwUInt32Box[4];
		short	nSInt16Box[4];
		long	nSInt32Box[4];
		float	nReal32Box[4];
		// XY Values
		BYTE	nByteXY[2];
		WORD	nUInt16XY[2];
		DWORD	dwUInt32XY[2];
		short	nSInt16XY[2];
		long	nSInt32XY[2];
		float	nReal32XY[2];
	} PCL6Data;

// Construction / Destruction
public:
	CPCL6DataType();
	virtual ~CPCL6DataType();

// Operations
public:
	virtual bool	ReadFromBuffer( CPrintJobBuffer& pjb );

private:
	void			Destroy();

// Data Access
public:
	const BYTE&		GetDataType() const;
	const PCL6Data&	GetData() const;
	const DWORD&	GetSize() const;

// Read Operations
private:
	bool			ReadValue( CPrintJobBuffer& pjb );
	bool			ReadArray( CPrintJobBuffer& pjb );
	bool			ReadBox( CPrintJobBuffer& pjb );
	bool			ReadXY( CPrintJobBuffer& pjb );

// Static Operations
public:
	static BYTE		GetDataTypeBaseType( const BYTE& nValue );
	static bool		IsValidDataType( const BYTE& nValue );
	static BYTE		GetDataTypeSubType( const BYTE& nValue );

// Debugging Operations
#ifdef USE_PCL6_DEBUG
public:
	virtual void	Dump();
#endif

// Attributes
private:
	bool		m_bDestroyArray;
	BYTE		m_nDataType;
	DWORD		m_dwSize;
	PCL6Data	m_Data;

// Enumerated Values
public:
	enum
	{
		// Base Types
		PCL_DATATYPE_BASE_UBYTE			= 0x00,
		PCL_DATATYPE_BASE_UINT16		= 0x01,
		PCL_DATATYPE_BASE_UINT32		= 0x02,
		PCL_DATATYPE_BASE_SINT16		= 0x03,
		PCL_DATATYPE_BASE_SINT32		= 0x04,
		PCL_DATATYPE_BASE_REAL32		= 0x05,

		PCL_DATATYPE_BASE_MASK			= 0x07,	// 00000111 = Masks off the base type

		// Sub Types
		PCL_DATATYPE_SUBTYPE_INVALID	= 0x00,
		PCL_DATATYPE_SUBTYPE_VALUE		= 0xc0,
		PCL_DATATYPE_SUBTYPE_ARRAY		= 0xc8,
		PCL_DATATYPE_SUBTYPE_BOX		= 0xe0,
		PCL_DATATYPE_SUBTYPE_XY			= 0xd0,

		// Regular Values
		PCL_DATATYPE_UBYTE				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_UBYTE  ),
		PCL_DATATYPE_UINT16				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_UINT16 ),
		PCL_DATATYPE_UINT32				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_UINT32 ),
		PCL_DATATYPE_SINT16				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_SINT16 ),
		PCL_DATATYPE_SINT32				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_SINT32 ),
		PCL_DATATYPE_REAL32				= ( PCL_DATATYPE_SUBTYPE_VALUE | PCL_DATATYPE_BASE_REAL32 ),

		// Array Values
		PCL_DATATYPE_UBYTE_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_UBYTE  ),
		PCL_DATATYPE_UINT16_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_UINT16 ),
		PCL_DATATYPE_UINT32_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_UINT32 ),
		PCL_DATATYPE_SINT16_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_SINT16 ),
		PCL_DATATYPE_SINT32_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_SINT32 ),
		PCL_DATATYPE_REAL32_ARRAY		= ( PCL_DATATYPE_SUBTYPE_ARRAY | PCL_DATATYPE_BASE_REAL32 ),

		// Box Values
		PCL_DATATYPE_UBYTE_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_UBYTE	),
		PCL_DATATYPE_UINT16_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_UINT16 ),
		PCL_DATATYPE_UINT32_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_UINT32 ),
		PCL_DATATYPE_SINT16_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_SINT16 ),
		PCL_DATATYPE_SINT32_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_SINT32 ),
		PCL_DATATYPE_REAL32_BOX			= ( PCL_DATATYPE_SUBTYPE_BOX | PCL_DATATYPE_BASE_REAL32 ),

		// XY Values
		PCL_DATATYPE_UBYTE_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_UBYTE  ),
		PCL_DATATYPE_UINT16_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_UINT16 ),
		PCL_DATATYPE_UINT32_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_UINT32 ),
		PCL_DATATYPE_SINT16_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_SINT16 ),
		PCL_DATATYPE_SINT32_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_SINT32 ),
		PCL_DATATYPE_REAL32_XY			= ( PCL_DATATYPE_SUBTYPE_XY | PCL_DATATYPE_BASE_REAL32 ),

		// Indexes into the array
		PCL_XY_X_VALUE					= 0,
		PCL_XY_Y_VALUE					= 1
	};
};

#pragma pack(pop,PCL6DataType)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL6DataType Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPCL6DataType::CPCL6DataType()
	{
		// Initialize the values
		m_nDataType		= PCL_DATATYPE_SUBTYPE_INVALID;
		m_dwSize		= 0;
		m_bDestroyArray	= false;
		::ZeroMemory( &m_Data, sizeof(m_Data) );
	}

	inline CPCL6DataType::~CPCL6DataType()
	{
		// Do all the clean up
		Destroy();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline const BYTE& CPCL6DataType::GetDataType() const
	{
		return m_nDataType;
	}
	
	inline const CPCL6DataType::PCL6Data& CPCL6DataType::GetData() const
	{
		return m_Data;
	}

	inline const DWORD& CPCL6DataType::GetSize() const
	{
		return m_dwSize;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline BYTE CPCL6DataType::GetDataTypeBaseType( const BYTE& nValue )
	{
		return ( nValue & PCL_DATATYPE_BASE_MASK );
	}

	inline bool CPCL6DataType::IsValidDataType( const BYTE& nValue )
	{
		return ( GetDataTypeSubType(nValue) != PCL_DATATYPE_SUBTYPE_INVALID );
	}

	inline BYTE CPCL6DataType::GetDataTypeSubType( const BYTE& nValue )
	{
		BYTE	nSubType	= PCL_DATATYPE_SUBTYPE_INVALID;

		switch( nValue )
		{
		// Regular Values
		case PCL_DATATYPE_UBYTE:
		case PCL_DATATYPE_UINT16:
		case PCL_DATATYPE_UINT32:
		case PCL_DATATYPE_SINT16:
		case PCL_DATATYPE_SINT32:
		case PCL_DATATYPE_REAL32:
			nSubType = PCL_DATATYPE_SUBTYPE_VALUE;
			break;

		// Array Values
		case PCL_DATATYPE_UBYTE_ARRAY:
		case PCL_DATATYPE_UINT16_ARRAY:
		case PCL_DATATYPE_UINT32_ARRAY:
		case PCL_DATATYPE_SINT16_ARRAY:
		case PCL_DATATYPE_SINT32_ARRAY:
		case PCL_DATATYPE_REAL32_ARRAY:
			nSubType = PCL_DATATYPE_SUBTYPE_ARRAY;
			break;	

		// Box Values
		case PCL_DATATYPE_UBYTE_BOX:
		case PCL_DATATYPE_UINT16_BOX:
		case PCL_DATATYPE_UINT32_BOX:
		case PCL_DATATYPE_SINT16_BOX:
		case PCL_DATATYPE_SINT32_BOX:
		case PCL_DATATYPE_REAL32_BOX:
			nSubType = PCL_DATATYPE_SUBTYPE_BOX;
			break;	

		// XY Values
		case PCL_DATATYPE_UBYTE_XY:
		case PCL_DATATYPE_UINT16_XY:
		case PCL_DATATYPE_UINT32_XY:
		case PCL_DATATYPE_SINT16_XY:
		case PCL_DATATYPE_SINT32_XY:
		case PCL_DATATYPE_REAL32_XY:
			nSubType = PCL_DATATYPE_SUBTYPE_XY;
			break;	

		// No matching values
		default:
			DEBUG_PCL6_TEXT( _T("Invalid Data Type: 0x%02x\n"), nValue );
			break;
		}

		return nSubType;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PCL6DATATYPE_H__4151461E_65F8_4287_84BA_563D053C7D3B__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL6DataType.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
