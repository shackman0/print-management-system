///////////////////////////////////////////////////////////////////////////////////////////////////////
// ReservedBlocks.h: interface for the CReservedBlocks class.
//
// By Darrin W. Cullop (DarrinC@cmsdiginet.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_RESERVEDBLOCKS_H__C347CC4C_7CFB_11D4_A469_00105A1C588D__INCLUDED_)
#define AFX_RESERVEDBLOCKS_H__C347CC4C_7CFB_11D4_A469_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "DataChain.h"
#include "ThreadLock.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE								_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_RESBLOCK_DEBUG
	#define DEBUG_RESBLOCK_TEXT					DEBUG_TEXT
#else
	#define DEBUG_RESBLOCK_TEXT					((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_BLOCK_ALLOC_COUNT				16

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CReservedBlocks  
{
// Internal Classes
protected:
	class CReservedBlock
	{
	// Attributes
	public:
		CReservedBlock*		m_pNext;
	};

// Construction / Destruction
public:
	CReservedBlocks( const DWORD& dwBlockSize,
					const DWORD& dwBlockAllocCount = DEFAULT_BLOCK_ALLOC_COUNT );
	~CReservedBlocks();

// Data Access
public:
	const DWORD&	GetBlockSize() const;

// Operations
public:
	LPVOID			Alloc();
	void			Free( LPVOID pFreeMe );
	void			FreeAll();

// Attributes
private:
	DWORD				m_dwBlockSize;			// The size of each blocks
	DWORD				m_dwBlockAllocCount;	// Number of blocks to allocate at a time
	CDataChain*			m_pDataChain;			// Raw data blocks
	CReservedBlock*		m_pFreeBlocks;			// Available blocks
	DECLARE_THREAD_LOCK();						// Makes class thread safe
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CReservedBlocks::CReservedBlocks( const DWORD& dwBlockSize, const DWORD& dwBlockAllocCount )
	{
		// Make it thread safe
		LOCK_THREAD();

		// Check assumptions
		_ASSERTE( dwBlockSize >= sizeof(CReservedBlock) );
		_ASSERTE( dwBlockAllocCount > 0 );

		m_dwBlockSize		= dwBlockSize;
		m_dwBlockAllocCount	= dwBlockAllocCount;
		m_pFreeBlocks		= NULL;
		m_pDataChain		= NULL;

		// Display Debugging Information
		DEBUG_RESBLOCK_TEXT( _T("Created Reserve Blocks: Size = %d, Count = %d\n"),
			m_dwBlockSize, m_dwBlockAllocCount );
	}

	inline CReservedBlocks::~CReservedBlocks()
	{
		FreeAll();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline const DWORD&	CReservedBlocks::GetBlockSize() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Return the Value
		return m_dwBlockSize;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CReservedBlocks::Free( LPVOID pFreeMe )
	{
		// Make it thread safe
		LOCK_THREAD();

		// Add it to the head of the free block list
		CReservedBlock* pBlock = reinterpret_cast<CReservedBlock*>(pFreeMe);

		pBlock->m_pNext = m_pFreeBlocks;
		m_pFreeBlocks = pBlock;
	}

	inline void CReservedBlocks::FreeAll()
	{
		// Make it thread safe
		LOCK_THREAD();

		// Display Debugging Information
		DEBUG_RESBLOCK_TEXT( _T("Freed Reserve Blocks @ 0x%08x: Size = %d, Count = %d\n"),
			m_pDataChain, m_dwBlockSize, m_dwBlockAllocCount );

		CDataChain::RemoveAll( m_pDataChain );
		_ASSERTE( m_pDataChain == NULL );
		m_pFreeBlocks		= NULL;
	}

	inline LPVOID CReservedBlocks::Alloc()
	{
		// Make it thread safe
		LOCK_THREAD();

		LPVOID pReturnMe = NULL;

		// Are there any blocks available?
		if ( m_pFreeBlocks == NULL )
		{
			// Display Debugging Information
			DEBUG_RESBLOCK_TEXT( _T("Allocating More Reserved Blocks %d at %d Bytes each\n"),
				m_dwBlockAllocCount, m_dwBlockSize );

			// There are none available, so make m_dwBlockAllocCount of them
			CDataChain*	pNewBlock = 
				CDataChain::Add( m_pDataChain, (m_dwBlockSize * m_dwBlockAllocCount) );

			// Link them together
			CReservedBlock* pBlock = reinterpret_cast<CReservedBlock*>(pNewBlock->GetData());
			for ( unsigned register nCounter = 0 ; nCounter < m_dwBlockAllocCount ; nCounter++ )
			{
				// Add this block to the front of the list of free blocks
				pBlock->m_pNext	= m_pFreeBlocks;
				m_pFreeBlocks	= pBlock;

				// Skip ahead to the next block
				(LPBYTE&)pBlock += m_dwBlockSize;
			}
		}

		// There *must* be blocks available at this point
		_ASSERTE( m_pFreeBlocks != NULL );

		// Return the first one in the list (after removing it from the list)
		pReturnMe = m_pFreeBlocks;
		m_pFreeBlocks = m_pFreeBlocks->m_pNext;
		return pReturnMe;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Useful Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DECLARE_RESERVED_BLOCKS(class_name)			\
public:												\
	void* operator new(size_t size)					\
	{												\
		_ASSERTE(size == s_alloc.GetBlockSize());	\
		return s_alloc.Alloc();						\
	}												\
	void* operator new(size_t, void* p)				\
	{												\
		return p;									\
	}												\
	void operator delete(void* p)					\
	{												\
		s_alloc.Free(p);							\
	}												\
	void* operator new(size_t size, LPCSTR, int)	\
	{												\
		_ASSERTE(size == s_alloc.GetBlockSize());	\
		return s_alloc.Alloc();						\
	}												\
protected:											\
	static CReservedBlocks s_alloc

#define IMPLEMENT_RESERVED_BLOCKS(class_name, block_size)			\
CReservedBlocks class_name::s_alloc(sizeof(class_name), block_size)	\

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_RESERVEDBLOCKS_H__C347CC4C_7CFB_11D4_A469_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of ReservedBlock.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
