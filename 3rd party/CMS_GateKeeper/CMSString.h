///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMSString.h: interface for the CCMSString class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMSSTRING_H__C347CC4D_7CFB_11D4_A469_00105A1C588D__INCLUDED_)
#define AFX_CMSSTRING_H__C347CC4D_7CFB_11D4_A469_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMSSTRING

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Includes
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ReservedBlocks.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define NO_REF_COUNT			-1

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma pack(push,CMSStringData,1)

class CCMSStringData
{
public:
	DWORD	dwRefCount;				// Number of strings using this data
	DWORD	dwDataLength;			// Size of data used for this string (includes NULL)
	DWORD	dwAllocLength;			// Size of data reserved for this string

	LPTSTR	GetData()				{ return (LPTSTR)(this+1); }
	DWORD	IncrementRefCount()		{ return ::InterlockedIncrement( reinterpret_cast<LONG*>(&dwRefCount) ); };
	DWORD	DecrementRefCount()		{ return ::InterlockedDecrement( reinterpret_cast<LONG*>(&dwRefCount) ); };
};

#pragma pack(pop,CMSStringData,1)

class CCMSString  
{
// Attributes
protected:
	LPTSTR		m_lpszData;

// Static Attributes
private:
	static CReservedBlocks			m_Blocks64;
	static CReservedBlocks			m_Blocks128;
	static CReservedBlocks			m_Blocks256;
	static CReservedBlocks			m_Blocks512;
	static DWORD					m_pEmptyInitData[];
	static CCMSStringData*			m_pEmptyData;
	static LPCTSTR					m_pEmptyString;
	static HINSTANCE				m_hInstance;

// Constructors
public:
	CCMSString()														{ Initialize(); };
	CCMSString( const CCMSString& strSource )							{ Initialize(); Copy(strSource); };
	CCMSString( LPCSTR lpszSource )										{ Initialize(); Copy(lpszSource); };
	CCMSString( LPCWSTR lpszSource )									{ Initialize(); Copy(lpszSource); };
	CCMSString( LPBYTE lpData )											{ Initialize(); Copy( (LPCSTR)lpData ); };

// Destructor
public:
	~CCMSString()														{ Release(); };

// Data Access
private:
	CCMSStringData*		GetData() const									{ _ASSERTE(m_lpszData != NULL); return ((CCMSStringData*)m_lpszData)-1; };
	bool				IsDataShared() const;

public:
	TCHAR				GetAt( const DWORD& dwIndex ) const				{ _ASSERTE( dwIndex < GetLength() ); return m_lpszData[dwIndex]; };
	const DWORD&		GetLength() const								{ return GetData()->dwDataLength; };
	bool				IsEmpty() const									{ return (GetLength() == 0); };

// Internal Operations
protected:
	void				Initialize()									{ m_lpszData = GetEmptyString().m_lpszData; };
	void				Concat( const DWORD& dwSourceLength, LPCTSTR lpszSource );
	void				ConcatCopy( const DWORD& dwLength1, LPCTSTR lpszSource1, const DWORD& dwLength2, LPCTSTR lpszSource2 );
	void				AllocCopy( CCMSString& strTarget, const DWORD& dwCopyLength, const DWORD& dwStartAt, const DWORD& dwExtraLength ) const;
	void				FormatString( LPCTSTR lpszFormat, va_list argList );

// Memory Operations
private:
	void				Release();
	void				AllocBuffer( const DWORD& dwLength );
	void				AllocBeforeWrite( const DWORD& dwLength );
	void				CopyBeforeWrite();

public:
	LPTSTR				GetBuffer( DWORD dwMinLength = 0 );
	void				ReleaseBuffer( const DWORD& dwNewLength );
	LPTSTR				GetBufferSetLength( DWORD dwMinLength );
	void				FreeExtra();
	LPTSTR				LockBuffer();
	void				UnlockBuffer();
	void				ReleaseBuffer()									{ ReleaseBuffer( StringLength(m_lpszData) ); };

// Copy Commands
public:
	void				Copy( const CCMSString& strSource );
	void				Copy( LPCSTR lpszSource );
	void				Copy( LPCWSTR lpszSource );

// Public Operations
public:
	void				Empty();
	int					Compare( LPCTSTR lpszCompareMe ) const			{ return lstrcmp( m_lpszData, lpszCompareMe ); };
	int					CompareNoCase( LPCTSTR lpszCompareMe ) const	{ return lstrcmpi( m_lpszData, lpszCompareMe ); };

	CCMSString			Mid( const DWORD& dwFirst ) const				{ return Mid( dwFirst, GetLength()-dwFirst ); };
	CCMSString			Mid( const DWORD& dwFirst, const DWORD& dwSize ) const;
	CCMSString			Left( const DWORD& dwSize ) const;
	CCMSString			Right( const DWORD& dwSize ) const;

	bool				LoadString( const DWORD& dwResID );
	void				Format( LPCTSTR lpszFormat, ... );
	void				Format( DWORD dwResID, ... );

	LPSTR				GetStringA( LPSTR lpszTarget, const DWORD& dwSize ) const;
	LPWSTR				GetStringW( LPWSTR lpszTarget, const DWORD& dwSize ) const;

	void				MakeUpper();
	void				MakeLower();
	void				MakeReverse();

	CCMSString			SpanIncluding( LPCTSTR lpszCharSet ) const;
	CCMSString			SpanExcluding( LPCTSTR lpszCharSet ) const;

// Find Operations
public:
	int					ReverseFind( TCHAR ch ) const;
	int					Find( LPCTSTR lpszSub, const DWORD& dwStart = 0 ) const;

// Trim Operations
public:
	void				TrimLeft( LPCTSTR lpszTargets );
	void				TrimLeft( TCHAR chTarget );
	void				TrimLeft();
	void				TrimRight( LPCTSTR lpszTargets );
	void				TrimRight( TCHAR chTarget );
	void				TrimRight();
	void				Trim()											{ TrimRight();	TrimLeft();										};

// Fancy Manipulation Operations
public:
	DWORD				Delete( const DWORD& dwIndex, const DWORD& dwCount = 1 );
	DWORD				Insert( DWORD dwIndex, TCHAR ch );
//	int					Insert( int nIndex, LPCTSTR pstr );
//	int					Remove( TCHAR chRemove );
//	int					Replace( LPCTSTR lpszOld, LPCTSTR lpszNew );
//	int					Replace( TCHAR chOld, TCHAR chNew );

// Operators
public:
						operator LPCTSTR() const						{ return m_lpszData; };
	const CCMSString&	operator=( const CCMSString& strSource )		{ Copy(strSource); return (*this); };
	const CCMSString&	operator=( LPCSTR lpszSource )					{ Copy(lpszSource); return (*this); };
	const CCMSString&	operator=( LPCWSTR lpszSource )					{ Copy(lpszSource); return (*this); };
	const CCMSString&	operator+=( const CCMSString& strSource )		{ Concat( strSource.GetLength(), strSource ); return (*this); };
	const CCMSString&	operator+=( LPCTSTR lpszSource )				{ Concat( StringLength(lpszSource), lpszSource ); return (*this); };
	TCHAR				operator[]( const DWORD& dwIndex ) const		{ return GetAt(dwIndex); };

// Static Member Access
protected:
	static
	const CCMSString&	GetEmptyString()								{ return *((CCMSString*)&m_pEmptyString); };
	static HINSTANCE	GetInstance()									{ _ASSERTE(m_hInstance != NULL); return m_hInstance; };

public:
	static void			SetInstance( HINSTANCE hInstance )				{ m_hInstance = hInstance; };

// Static Utility Functions
protected:
	static DWORD		StringLength( LPCSTR lpszString );
	static DWORD		StringLength( LPCWSTR lpszString );
	static int			LoadStringFromResource( const DWORD& dwResID, LPTSTR lpszBuffer, const DWORD& dwMaxSize );

// Static Memory Operations
private:
	static void			Release( CCMSStringData* );
	static void			FreeData( CCMSStringData* pData );

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Compare Operators
///////////////////////////////////////////////////////////////////////////////////////////////////////
inline bool operator==( const CCMSString& s1, const CCMSString& s2 )	{ return (s1.Compare(s2) == 0); };
inline bool operator==( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) == 0); };
inline bool operator==( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) == 0); };

inline bool operator!=( const CCMSString& s1, const CCMSString& s2 )	{ return (s1.Compare(s2) != 0); };
inline bool operator!=( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) != 0); };
inline bool operator!=( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) != 0); };

inline bool operator<( const CCMSString& s1, const CCMSString& s2 )		{ return (s1.Compare(s2) < 0); };
inline bool operator<( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) < 0); };
inline bool operator<( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) > 0); };

inline bool operator>( const CCMSString& s1, const CCMSString& s2 )		{ return (s1.Compare(s2) > 0); };
inline bool operator>( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) > 0); };
inline bool operator>( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) < 0); };

inline bool operator<=( const CCMSString& s1, const CCMSString& s2 )	{ return (s1.Compare(s2) <= 0); };
inline bool operator<=( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) <= 0); };
inline bool operator<=( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) >= 0); };

inline bool operator>=( const CCMSString& s1, const CCMSString& s2 )	{ return (s1.Compare(s2) >= 0); };
inline bool operator>=( const CCMSString& s1, LPCTSTR s2 )				{ return (s1.Compare(s2) >= 0); };
inline bool operator>=( LPCTSTR s1, const CCMSString& s2 )				{ return (s2.Compare(s1) <= 0); };

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Concat Operators
///////////////////////////////////////////////////////////////////////////////////////////////////////
inline CCMSString operator+( const CCMSString& s1, const CCMSString& s2 )
{
	CCMSString str = s1;
	str += s2;
	return str;
}

inline CCMSString operator+( const CCMSString& s1, LPCTSTR s2 )
{
	CCMSString str = s1;
	str += s2;
	return str;
}

inline CCMSString operator+( LPCTSTR s1, const CCMSString& s2 )
{
	CCMSString str = s1;
	str += s2;
	return str;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CCMSString::IsDataShared() const
	{
		return ( (GetData()->dwRefCount != NO_REF_COUNT) && (GetData()->dwRefCount > 1) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CCMSString::Empty()
	{
		if ( GetData()->dwDataLength > 0 )
		{
			Release();
		}

		_ASSERTE( GetData()->dwDataLength == 0 );
	}

	inline CCMSString CCMSString::Right( const DWORD& dwSize ) const
	{
		if ( dwSize >= GetLength() )
		{
			return *this;
		}

		CCMSString strReturnMe;

		AllocCopy( strReturnMe, dwSize, GetLength() - dwSize, 0 );
		return strReturnMe;
	}

	inline CCMSString CCMSString::Left( const DWORD& dwSize ) const
	{
		if ( dwSize >= GetLength() )
		{
			return *this;
		}

		CCMSString strReturnMe;

		AllocCopy( strReturnMe, dwSize, 0, 0 );
		return strReturnMe;
	}

	inline CCMSString CCMSString::Mid( const DWORD& dwFirst, const DWORD& dwSize ) const
	{
		// If returning the whole string
		if ( (dwFirst == 0) && (dwSize == GetLength()) )
		{
			return *this;
		}
		else
		{
			CCMSString	strReturnMe;
			DWORD		dwStart = dwFirst;
			DWORD		dwCount	= dwSize;

			if ( dwStart > GetLength() )
			{
				dwCount = 0;
			}
			else if ( (dwStart + dwCount) > GetLength() )
			{
				dwCount = GetLength() - dwStart;
			}

			// The values should fit now
			_ASSERTE( (dwFirst + dwCount) <= GetLength() );

			AllocCopy( strReturnMe, dwCount, dwStart, 0 );
			return strReturnMe;
		}
	}

	inline void CCMSString::MakeUpper()
	{
		CopyBeforeWrite();
		_tcsupr( m_lpszData );
	}

	inline void CCMSString::MakeLower()
	{
		CopyBeforeWrite();
		_tcslwr( m_lpszData );
	}

	inline void CCMSString::MakeReverse()
	{
		CopyBeforeWrite();
		_tcsrev( m_lpszData );
	}

	inline CCMSString CCMSString::SpanIncluding( LPCTSTR lpszCharSet ) const
	{
		_ASSERTE( lpszCharSet != NULL );
		return Left( _tcsspn(m_lpszData, lpszCharSet) );
	}

	inline CCMSString CCMSString::SpanExcluding( LPCTSTR lpszCharSet ) const
	{
		_ASSERTE( lpszCharSet != NULL );
		return Left( _tcscspn(m_lpszData, lpszCharSet) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Find Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline int CCMSString::ReverseFind( TCHAR ch ) const
	{
		// Go to Last Character
		LPTSTR lpsz = _tcsrchr( m_lpszData, (_TUCHAR)ch );

		// Return delta from beginning
		// or -1 if not found
		return (lpsz != NULL) ? (int)(lpsz - m_lpszData) : -1;
	}

	inline int CCMSString::Find( LPCTSTR lpszSub, const DWORD& dwStart /* = 0 */ ) const
	{
		_ASSERTE( lpszSub != NULL );

		if ( dwStart <= GetLength() )
		{
			// Find first matching substring
			LPTSTR lpsz = _tcsstr( m_lpszData + dwStart, lpszSub );

			// Return -1 for not found, distance from beginning otherwise
			return (lpsz != NULL) ? (int)(lpsz - m_lpszData) : -1;
		}
		else
		{
			return -1;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Memory Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CCMSString::Release()
	{
		if ( GetData() != m_pEmptyData )
		{
			Release( GetData() );
			Initialize();
		}
	}

	inline void CCMSString::CopyBeforeWrite()
	{
		if ( IsDataShared() )
		{
			CCMSStringData* pData = GetData();
			Release();
			AllocBuffer( pData->dwDataLength );
			::CopyMemory( m_lpszData, pData->GetData(), ((pData->dwDataLength + 1) * sizeof(TCHAR)) );
		}
	}

	inline void CCMSString::AllocBeforeWrite( const DWORD& dwLength )
	{
		// If there are others sharing this Data, or the data isn't big enough
		// let it go and reallocate a new one
		if ( IsDataShared()	|| (dwLength > GetData()->dwAllocLength) )
		{
			Release();
			AllocBuffer( dwLength );
		}
	}

	inline LPTSTR CCMSString::LockBuffer()
	{
		LPTSTR lpszBuffer = GetBuffer(0);
		GetData()->dwRefCount = NO_REF_COUNT;
		return lpszBuffer;
	}

	inline void CCMSString::UnlockBuffer()
	{
		_ASSERTE( GetData()->dwRefCount == NO_REF_COUNT );

		if ( GetData() != m_pEmptyData )
		{
			GetData()->dwRefCount = 1;
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Copy Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CCMSString::Copy( const CCMSString& strSource )
	{
		Release();
		if ( strSource.GetData()->dwRefCount != NO_REF_COUNT )
		{
			_ASSERTE( strSource.GetData() != m_pEmptyData );
			m_lpszData = strSource.m_lpszData;
			GetData()->IncrementRefCount();
		}
		else
		{
			// Create a new block
			Initialize();
			Copy( strSource.m_lpszData );
		}
	}

	inline void CCMSString::Copy( LPCTSTR lpszSource )
	{
		Release();
		DWORD dwLength = StringLength(lpszSource);
		if ( dwLength > 0 )
		{
			AllocBuffer( dwLength );
			::CopyMemory( m_lpszData, lpszSource, dwLength*sizeof(TCHAR) );
		}
	}

	#ifdef _UNICODE

	inline void CCMSString::Copy( LPCSTR lpszSource )
	{
		Release();
		DWORD dwLength = StringLength(lpszSource);
		if ( dwLength > 0 )
		{
			AllocBuffer(dwLength);
			DWORD dwResult = ::MultiByteToWideChar( CP_ACP, 0, lpszSource, -1, m_lpszData, dwLength+1 );
			_ASSERTE( dwResult <= (dwLength+1) );
			ReleaseBuffer();
		}
	}

	#else

	inline void CCMSString::Copy( LPCWSTR lpszSource )
	{
		Release();
		DWORD dwLength = StringLength(lpszSource);
		if ( dwLength > 0 )
		{
			AllocBuffer(dwLength*2);
			DWORD dwResult = ::WideCharToMultiByte( CP_ACP, 0, lpszSource, -1, m_lpszData, ((dwLength*2)+1), NULL, NULL );
			_ASSERTE( dwResult <= ((dwLength*2)+1) );
			ReleaseBuffer();
		}
	}

	#endif

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Formatting Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CCMSString::Release( CCMSStringData* pData )
	{
		if ( pData != m_pEmptyData )
		{
			_ASSERTE( pData->dwRefCount > 0 );
			if ( (pData->dwRefCount == NO_REF_COUNT) || (pData->DecrementRefCount() == 0) )
			{
				FreeData( pData );
			}
		}
	}

	inline int CCMSString::LoadStringFromResource( const DWORD& dwResID, LPTSTR lpszBuffer, const DWORD& dwMaxSize )
	{
		int nLen = ::LoadString( GetInstance(), dwResID, lpszBuffer, dwMaxSize );

		if ( nLen == 0 )
		{
			lpszBuffer[0] = _T('\0');
		}
		return nLen;
	}

	inline DWORD CCMSString::StringLength( LPCSTR lpszString )
	{
		return ( (lpszString == NULL) ? 0 : lstrlenA(lpszString) );
	}

	inline DWORD CCMSString::StringLength( LPCWSTR lpszString )
	{
		return ( (lpszString == NULL) ? 0 : wcslen(lpszString) );
	}

	inline void CCMSString::FreeData( CCMSStringData* pData )
	{
		// Check assumptions
		_ASSERTE( pData != NULL );
		_ASSERTE( pData != m_pEmptyData );

		DWORD	dwLength = pData->dwAllocLength;
		if ( dwLength == 64 )
		{
			m_Blocks64.Free(pData);
		}
		else if ( dwLength == 128 )
		{
			m_Blocks128.Free(pData);
		}
		else if ( dwLength == 256 )
		{
			m_Blocks256.Free(pData);
		}
		else if ( dwLength == 512 )
		{
			m_Blocks512.Free(pData);
		}
		else
		{
			_ASSERTE( dwLength > 512 );
			delete [] (LPBYTE)pData;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // #ifndef NO_CMSSTRING

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_CMSSTRING_H__C347CC4D_7CFB_11D4_A469_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of CMSString.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
