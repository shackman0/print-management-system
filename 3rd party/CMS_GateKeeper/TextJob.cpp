///////////////////////////////////////////////////////////////////////////////////////////////////////
// TextJob.cpp: implementation of the CTextJob class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "TextJob.h"
#include "PrinterAPI.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Tools
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_RAWTEXT_DEBUG
	#define DEBUG_RAWTEXT					DEBUG_TEXT
#else
	#define DEBUG_RAWTEXT					((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define TEXTJOB_FLAG_TAB								BIT(1)
#define TEXTJOB_FLAG_FF									BIT(2)
#define TEXTJOB_FLAG_LF									BIT(3)
#define TEXTJOB_FLAG_CR									BIT(4)
#define TEXTJOB_FLAG_CRLF								(TEXTJOB_FLAG_LF | TEXTJOB_FLAG_CR)
														
#define ASCII_FF										((char)(0x0c))
#define ASCII_CR										((char)(0x0d))
#define ASCII_LF										((char)(0x0a))
#define ASCII_SPACE										((char)(0x20))
#define ASCII_TAB										((char)('\t'))
														
#define	PRINTER_DATA_NOTRANSLATE_CRLF					_T("Winprint_TextNoTranslation")
#define	PRINTER_DATA_NOTRANSLATE_CR						_T("Winprint_TextNoCRTranslation")
											
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CTextJob::PrepareTextJob( LPTSTR lpszDocumentName, 
							  LPPRINTPROCESSORDATA pPrintProcessorData, VectorData& vdTextData )
{
	// Check assumptions
	_ASSERTE( pPrintProcessorData != NULL );
	_ASSERTE( lpszDocumentName != NULL );
	_ASSERTE( vdTextData.Length() > 0 );
	_ASSERTE( m_pPrintProcessorData == NULL );
	_ASSERTE( m_lpszDocumentName == NULL );

	// Make sure values that cannot be assumed are correct
	if ( pPrintProcessorData->hDC == NULL )
	{
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_MissingHDC );
	}

	// Initialize some values
	RestoreHDC();
	m_pPrintProcessorData	= pPrintProcessorData;
	m_lpszDocumentName		= lpszDocumentName;
	m_hDC					= m_pPrintProcessorData->hDC;
	m_nTabSize				= m_pPrintProcessorData->TabSize ? m_pPrintProcessorData->TabSize : 1;
	GetTextInfo();

	// Check assumptions
	_ASSERTE( m_nCharsPerLine > 0 );

	// Initialize values
	m_PrintLine.SetSize( m_nCharsPerLine + 5 );
	GetTranslationSettings();

	// Run through the job one time to count the pages (but do not print)
	RunJob( vdTextData, false );

	//DEBUG_DUMP( lpTextData, dwSize );
}

DWORD CTextJob::PrintTextJob( VectorData& vdTextData )
{
	DOCINFO 	di;
	DWORD		dwTotalPrinted = 0;
	DWORD		dwCopies;

	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );
	_ASSERTE( m_lpszDocumentName != NULL );

	// Initialize the DOCINFO struct
	::ZeroMemory( &di, sizeof(di) );
	di.lpszDocName	= m_lpszDocumentName;
	di.lpszOutput	= NULL;
	di.lpszDatatype	= NULL;
	di.cbSize		= sizeof(di);

	// Set the number of copies
	dwCopies = m_pPrintProcessorData->Copies;

	// Tell the printer there is a new document being created
	THROW_WIN_FAIL( ::StartDoc(m_hDC, (LPDOCINFO)&di) );

	while ( (dwCopies--) && (IsJobAborted() == false) )
	{
		DEBUG_RAWTEXT( _T("Printing Copy %d!\n"), dwCopies+1 );
		
		// Print the job
		RunJob( vdTextData, true );
		dwTotalPrinted += m_nPageCount;
	}

	// Finished with the document
	THROW_WIN_FAIL( ::EndDoc(m_hDC) );

	// Return the total number of printed pages
	return dwTotalPrinted;
}

void CTextJob::RunJob( VectorData& vdTextData, const bool& bReallyPrint )
{
	bool			bInPage			= false;
	LPBYTE			lpCurrentData	= &(vdTextData[0]);
	const LPBYTE	lpEndData		= lpCurrentData + vdTextData.Length();
	DWORD			dwFlags			= 0;
	DWORD			dwLength		= 0;

	// Check assumptions
	_ASSERTE( m_nLinesPerPage > 0 );
	_ASSERTE( m_nCharsPerLine > 0 );
	_ASSERTE( m_nCharHeight > 0 );
	_ASSERTE( m_nCharWidth > 0 );
	_ASSERTE( vdTextData.Length() > 0 );
	_ASSERTE( m_nTabSize > 0 );

	// Initialize values
	m_nCurrentColumn	= 0;
	m_nCurrentLine		= 0;
	m_nPageCount		= 0;

#ifdef USE_RAWTEXT_DEBUG
	USES_CONVERSION;
#endif

	while ( (lpEndData > lpCurrentData) && (IsJobAborted() == false) )
	{
		// Stop while the printer is paused
		if ( bReallyPrint )
		{
			WaitWhilePaused();
		}

		// Get a line of text to print
		dwLength = GetLineToPrint( dwFlags, lpCurrentData, lpEndData );

		if ( dwLength > 0 )
		{
			// Process any white space
			ProcessWhiteSpace( bReallyPrint, bInPage );

			if ( bInPage == false )
			{
				THROW_WIN_FAIL( ::StartPage(m_hDC) );
				bInPage = true;
				m_nPageCount++;
			}

			if ( bReallyPrint )
			{
				// Display Text
				THROW_WIN_FAIL(
					::TextOutA(m_hDC, m_nCurrentColumn*m_nCharWidth, 
									m_nCurrentLine*m_nCharHeight,
									(LPSTR)m_PrintLine.GetData(), dwLength)
				);
			}

#ifdef USE_RAWTEXT_DEBUG
			DEBUG_RAWTEXT( _T("%d-%d-%d (%d): %s\n"), m_nPageCount, m_nCurrentLine, 
				m_nCurrentColumn, dwLength, A2CT((LPSTR)m_PrintLine.GetData()) );
#endif
			m_nCurrentColumn += dwLength;
		}

#ifdef USE_RAWTEXT_DEBUG
		if ( IsFlagSet(dwFlags,(TEXTJOB_FLAG_CRLF | TEXTJOB_FLAG_FF)) )
		{
			DEBUG_RAWTEXT( _T("%s%s%s\n"),
				( IsFlagSet(dwFlags,TEXTJOB_FLAG_CR) ? _T("CR ") : _T("") ),
				( IsFlagSet(dwFlags,TEXTJOB_FLAG_LF) ? _T("LF ") : _T("") ),
				( IsFlagSet(dwFlags,TEXTJOB_FLAG_FF) ? _T("FF") : _T("") )
			);
		}
#endif

		if ( IsFlagSet(dwFlags, TEXTJOB_FLAG_CR) )
		{
			// Move to the beginning of the line
			m_nCurrentColumn	= 0;
		}

		if ( IsFlagSet(dwFlags, TEXTJOB_FLAG_LF) )
		{
			// Move to the next line
			m_nCurrentLine++;
		}

		// Check for a Form Feed
		if ( IsFlagSet(dwFlags, TEXTJOB_FLAG_FF) )
		{
			DEBUG_RAWTEXT( _T("Got Form Feed at = P: %d, L: %d, C: %d, InPage = %s\n"),
				m_nPageCount, m_nCurrentLine, m_nCurrentColumn, DEBUG_BOOL(bInPage) );
			
			// Skip enough lines to end at the end of the current page
			m_nCurrentLine += (m_nLinesPerPage - (m_nCurrentLine % m_nLinesPerPage));

			// Process any white space
			ProcessWhiteSpace( bReallyPrint, bInPage );

			// Check assumptions (should be at the first line of the page)
			_ASSERTE( m_nCurrentLine == 0 );

			DEBUG_RAWTEXT( _T("After Form Feed... P: %d, L: %d, C: %d, InPage = %s\n"),
				m_nPageCount, m_nCurrentLine, m_nCurrentColumn, DEBUG_BOOL(bInPage) );
			
		}
	}

	if ( bReallyPrint && bInPage )
	{
		// End the last page
		THROW_WIN_FAIL( ::EndPage(m_hDC) );
	}
}

DWORD CTextJob::GetLineToPrint( DWORD& dwFlags, LPBYTE& lpCurrentData, const LPBYTE lpEndData )
{
	unsigned register	nCounter		= 0;
	DWORD				dwWritten		= 0;
	DWORD				dwMaxLength		= ( m_nCharsPerLine - m_nCurrentColumn );
	DWORD				dwLineLength	= 0;
	LPBYTE				lpBreakChar		= NULL;
	DWORD				dwBreakPos		= 0;
	DWORD				dwBreakLength	= 0;

	// Start with a fresh buffer
	m_PrintLine.Zero();

	// Reset the flags
	ClearFlag( dwFlags, (TEXTJOB_FLAG_CRLF | TEXTJOB_FLAG_FF) );

	// Did the last line end on a tab?
	if ( IsFlagSet(dwFlags, TEXTJOB_FLAG_TAB) )
	{
		// Remove the flag
		ClearFlag( dwFlags, TEXTJOB_FLAG_TAB );

		nCounter = m_nTabSize - ( m_nCurrentColumn % m_nTabSize );
		if ( nCounter > dwMaxLength )
		{
			// Cut the spacing off at the maximum level
			nCounter = dwMaxLength;

			// Tab won't properly expand, so this is the end of the line
			SetFlag( dwFlags, TEXTJOB_FLAG_CRLF );
		}

		// Expand the appropriate number of spaces
		while ( nCounter-- )
		{
			m_PrintLine[dwWritten++] = ASCII_SPACE;
			dwLineLength++;
		}
	}

	// Loop as long as the follow conditions are true
	//	1) The end of the data has not been reached
	//	2) The string is less than the maximum
	//	3) There has not been a CR, LF, or FF
	while ( (lpEndData > lpCurrentData) && (dwMaxLength > dwLineLength) 
				&& !IsFlagSet(dwFlags,(TEXTJOB_FLAG_CRLF | TEXTJOB_FLAG_FF)) )
	{
		switch( *lpCurrentData )
		{
		// TAB
		case ASCII_TAB:
			nCounter = m_nTabSize - ( (m_nCurrentColumn + dwLineLength) % m_nTabSize );
			if ( dwLineLength+nCounter > dwMaxLength )
			{
				// Cut the spacing off at the maximum level
				nCounter = dwMaxLength - dwLineLength;
				// Set the flag so that the tab can go on the next line
				SetFlag( dwFlags, (TEXTJOB_FLAG_CRLF | TEXTJOB_FLAG_TAB) );
			}

			// Expand the appropriate number of spaces
			while ( nCounter-- )
			{
				m_PrintLine[dwWritten++] = ASCII_SPACE;
				dwLineLength++;
			}
			break;

		// Form Feed
		case ASCII_FF:
			SetFlag( dwFlags, TEXTJOB_FLAG_FF );
			break;

		// Line Feed
		case ASCII_LF:
			// If LF is being translated into CRLF
			if ( m_bTranslateLF )
			{
				// Set both the CR and LF flags
				SetFlag( dwFlags, TEXTJOB_FLAG_CRLF );

				// If the CR was there anyway, make sure it is skipped over
				if ( *(lpCurrentData+1) == ASCII_CR )
				{
					lpCurrentData++;
				}
			}
			else
			{
				// Set only the LF flag
				SetFlag( dwFlags, TEXTJOB_FLAG_LF );
			}
			break;

		// Carriage Return
		case ASCII_CR:
			// If CR is being translated into CRLF
			if ( m_bTranslateCR )
			{
				// Set both the CR and LF flags
				SetFlag( dwFlags, TEXTJOB_FLAG_CRLF );

				// If the LF was there anyway, make sure it is skipped over
				if ( *(lpCurrentData+1) == ASCII_LF )
				{
					lpCurrentData++;
				}
			}
			else
			{
				// Set only the CR flag
				SetFlag( dwFlags, TEXTJOB_FLAG_CR );
			}
			break;

		// Invalid Byte Values
		case 0:
			DEBUG_RAWTEXT( _T("Got a ZERO BYTE!\n") );
			break;

		// Record the last good spot to break
		case ASCII_SPACE:
			lpBreakChar		= lpCurrentData;
			dwBreakPos		= dwWritten;
			dwBreakLength	= dwLineLength;
			// Allow to fall through!

		// Everything else
		default:
			// Write out the byte
			m_PrintLine[dwWritten++] = *lpCurrentData;
			dwLineLength++;

			// Check to see if this byte is part of a two byte set, and if both bytes are present
			if ( ::IsDBCSLeadByteEx(m_dwANSICodePage, *lpCurrentData) && (lpCurrentData < lpEndData) )
			{
				// Write the other byte
				lpCurrentData++;
				m_PrintLine[dwWritten++] = *lpCurrentData;
				// DO NOT increment dwLineLength because it is still just one printed character
			}
			break;
		}

		// Advance the pointer
		lpCurrentData++;
	}

	// Check to see if a CRLF is needed
	if ( dwLineLength == dwMaxLength )
	{
		// This is the end of the line
		SetFlag( dwFlags, TEXTJOB_FLAG_CRLF );

		// Check to see if there is a good spot to break
		if ( lpBreakChar != NULL )
		{
			// Check assumptions
			_ASSERTE( dwBreakPos != 0 );

			// Set the current spot to one past the break point
			lpCurrentData = lpBreakChar+1;

			// Zero out the break point
			m_PrintLine[dwBreakPos] = 0;

			// Get the new length
			dwLineLength = dwBreakLength;
		}
	}

	// Return the length of the line
	return dwLineLength;
}

void CTextJob::GetTextInfo()
{
	CHARSETINFO		csi;
	TEXTMETRIC		tm;
	int				nPageHeight;
	int				nPageWidth;

	// Check assumptions
	_ASSERTE( m_hDC != NULL );
	_ASSERTE( m_hFont == NULL );
	_ASSERTE( m_hOldFont == NULL );

	// Get the ANSI Code Page
	m_dwANSICodePage = ::GetACP();

	// Create FIXED PITCH font and select it into the HDC
	::ZeroMemory( &csi, sizeof(csi) );
	if ( ::TranslateCharsetInfo((LPDWORD)m_dwANSICodePage, &csi, TCI_SRCCODEPAGE) )
	{
		LOGFONT	lf;

		// Initialize the struct
		::ZeroMemory( &lf, sizeof(lf) );
		lf.lfWeight			= 400;
		lf.lfCharSet		= (BYTE)csi.ciCharset;
		lf.lfPitchAndFamily	= FIXED_PITCH;
		//_tcscpy( lf.lfFaceName, _T("Wingdings") );

		m_hFont	= ::CreateFontIndirect( &lf );
		if ( m_hFont == NULL )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("CreateFontIndirect Failed in CTextJobHandler::GetTextInfo\n") );
			throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_FailCreateFontIndirect );
		}
		m_hOldFont	= (HFONT)::SelectObject( m_hDC, m_hFont );
	}

	// Retrieve the text metrics
	if ( ::GetTextMetrics(m_hDC, &tm) == FALSE )
	{
		// Essential text processing computation failed
		DEBUG_LAST_ERROR();
		DEBUG_TEXT( _T("GetTextMetrics Failed in CTextJobHandler::GetTextInfo\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_FailGetTextMetrics );
	}

	// Get the character height
	m_nCharHeight = tm.tmHeight + tm.tmExternalLeading;
	if ( m_nCharHeight == 0 )
	{
		DEBUG_TEXT( _T("Invalid CharHeight!\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_InvalidCharHeight );
	}

	// Get the character width
	m_nCharWidth = tm.tmAveCharWidth;
	if ( m_nCharWidth == 0 )
	{
		DEBUG_TEXT( _T("Invalid CharWidth!\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_InvalidCharWidth );
	}

	// Get the page dimensions
	nPageHeight	= ::GetDeviceCaps( m_hDC, DESKTOPVERTRES );
	nPageWidth	= ::GetDeviceCaps( m_hDC, DESKTOPHORZRES );

	// Compute the lines per page
	m_nLinesPerPage = nPageHeight / m_nCharHeight;
	if ( m_nLinesPerPage == 0 )
	{
		DEBUG_TEXT( _T("Invalid Lines Per Page!\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_InvalidLinesPerPage );
	}

	// Compute the chars per line
	m_nCharsPerLine = nPageWidth / m_nCharWidth;
	if ( m_nCharsPerLine == 0 )
	{
		DEBUG_TEXT( _T("Invalid Chars Per Line!\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_InvalidCharsPerLine );
	}

	// Display the results as debug information
	DEBUG_RAWTEXT( _T("TextInfo: PageWidth = %d, PageHeight = %d\n          ")
					_T("CharHeight = %d, CharWidth = %d\n          ")
					_T("CharsPerLine = %d, LinesPerPage = %d\n"), 
		nPageWidth, nPageHeight, m_nCharHeight, m_nCharWidth, m_nCharsPerLine, m_nLinesPerPage );
}

void CTextJob::GetTranslationSettings()
{
	CPrinterAPI		hPrinter;
	DWORD			dwResult;
	DWORD			dwNeeded;

	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );
	_ASSERTE( m_lpszDocumentName != NULL );

	DEBUG_RAWTEXT( _T("Checking Translation Settings for %s\n"), m_lpszDocumentName );
	hPrinter.OpenPrinter( m_lpszDocumentName );

	if ( GetPrinterData(hPrinter, PRINTER_DATA_NOTRANSLATE_CRLF, NULL, (LPBYTE)&dwResult, 
			sizeof(dwResult), &dwNeeded) == ERROR_SUCCESS )
	{
		if ( dwResult )
		{
			// Do not translate either CRs or LFs into CRLFs
			m_bTranslateCR	= false;
			m_bTranslateLF	= false;
		}
	}

	if ( GetPrinterData(hPrinter, PRINTER_DATA_NOTRANSLATE_CR, NULL, (LPBYTE)&dwResult, 
			sizeof(dwResult), &dwNeeded) == ERROR_SUCCESS )
	{
		if ( dwResult )
		{
			// Do not translate CRs into CRLFs
			m_bTranslateCR	= false;
		}
	}

	DEBUG_RAWTEXT( _T("Translate CR: %s, Translate LF: %s\n"), DEBUG_BOOL(m_bTranslateCR), 
		DEBUG_BOOL(m_bTranslateLF) );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of TextJob.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
