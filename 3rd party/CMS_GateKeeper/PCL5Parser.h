///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL5Parser.h: interface for the CPCL5Parser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL5PARSER_H__FFD1B45B_EC98_11D3_A431_00105A1C588D__INCLUDED_)
#define AFX_PCL5PARSER_H__FFD1B45B_EC98_11D3_A431_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintParserBase.h"
#include "PCL5DataType.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_PCL5_DEBUG
	#define DEBUG_PCL5_TEXT						DEBUG_TEXT
#else
	#define DEBUG_PCL5_TEXT						((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define FORM_FEED									0x0c
#define LINE_FEED									'\r'

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL5Parser Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPCL5Parser : public CPrintParserBase  
{
// TypeDefs
private:
	typedef void (CPCL5Parser::*PCL5Operator)( const CPCL5DataType* );

	typedef struct
	{
		DWORD			dwHashValue;
		PCL5Operator	fpOperator;
	} PCL5Command;

	typedef CCMSmap<DWORD, const DWORD&, PCL5Operator, PCL5Operator>	CPCL5CommandMap;

// Attributes
private:
	bool						m_bMismatchedCopies;
	bool						m_bNewPage;
	CPrintParserData*			m_pParserData;
	// Parsed Values
	DWORD						m_dwPageCount;
	WORD						m_nCopies;

// Static Attributes
private:
	static const PCL5Command	m_pCommandList[];
	static CPCL5CommandMap		m_mapCommands;
	static bool					m_bMapInitialized;

// Construction
public:
	CPCL5Parser();

// Operations
public:
	virtual bool	Parse( CPrintParserData* pParserData );

// Main Processing Functions
private:
	bool			VerifyPCL5();
	void			SkipText();
	void			TrackBeginPage();
	void			TrackEndPage();

private:
	void			ProcessColor( const CPCL5DataType* pData );
	void			ProcessColorPalette( const CPCL5DataType* pData );
	void			ProcessCopies( const CPCL5DataType* pData );
	void			ProcessDuplexing( const CPCL5DataType* pData );
	void			ProcessMoveCursor( const CPCL5DataType* pData );
	void			ProcessOrientation( const CPCL5DataType* pData );
	void			ProcessPageSize( const CPCL5DataType* pData );
	void			ProcessReset( const CPCL5DataType* pData );
	void			ProcessResolution( const CPCL5DataType* pData );
	void			ProcessSkipData( const CPCL5DataType* pData );
//	void			ProcessPaperSource( const CPCL5DataType* pData );
//	void			ProcessSkipText( const CPCL5DataType* pData );
//	void			Process( const CPCL5DataType* pData );

// Static Operations
private:
	static void		InitializeCommandMap();

// Enumerated Types
private:
	enum
	{
		// Duplex Settings
		PCL5_DUPLEX_SIMPLEX							= 0,
		PCL5_DUPLEX_DUPLEX_LONG						= 1,
		PCL5_DUPLEX_DUPLEX_SHORT					= 2,

		// Paper Size Settings
		PCL5_PAPERSIZE_EXECUTIVE					= 1,
		PCL5_PAPERSIZE_LETTER						= 2,
		PCL5_PAPERSIZE_LEGAL						= 3,
		PCL5_PAPERSIZE_LEDGER						= 6,
		PCL5_PAPERSIZE_A3							= 27,
		PCL5_PAPERSIZE_A4							= 26,
		PCL5_PAPERSIZE_A5							= 25,
		PCL5_PAPERSIZE_A6							= 24,
		PCL5_PAPERSIZE_JIS_B4						= 46,
		PCL5_PAPERSIZE_JIS_B5						= 45,
		PCL5_PAPERSIZE_JIS_B6						= 44,
		PCL5_PAPERSIZE_HAGAKI_POSTCARD				= 71,		
		PCL5_PAPERSIZE_OUFUKU_HAGAKI				= 72,		
		PCL5_PAPERSIZE_MONARCH						= 80,
		PCL5_PAPERSIZE_COM10						= 81,
		PCL5_PAPERSIZE_DL							= 90,
		PCL5_PAPERSIZE_C5							= 91,
		PCL5_PAPERSIZE_B5							= 100,
		PCL5_PAPERSIZE_CUSTOM						= 101,

		// Orientation Settings
		PCL5_ORIENTATION_PORTRAIT					= 0,
		PCL5_ORIENTATION_LANDSCAPE					= 1,
		PCL5_ORIENTATION_REVERSE_PORTRAIT			= 2,
		PCL5_ORIENTATION_REVERSE_LANDSCAPE			= 3,

		// Color Settings
		PCL5_COLOR_SIMPLE_CMY						= -3,
		PCL5_COLOR_SIMPLE_MONOCHROME				= 1,
		PCL5_COLOR_SIMPLE_RGB						= 3
	};
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL5Parser Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPCL5Parser::CPCL5Parser()
	{
		m_bMismatchedCopies	= false;
		m_bNewPage			= false;
		m_pParserData		= NULL;
		m_dwPageCount		= 0;
		m_nCopies			= 0;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Main Process Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline bool CPCL5Parser::VerifyPCL5()
	{
		// Verifies whether or not the data is actually PCL5
		return ( m_pParserData->Peek() == CPCL5DataType::PCL5_ESCAPE );
	}

	inline void CPCL5Parser::SkipText()
	{
		while ( m_pParserData->GetBytesRemaining() && 
				(m_pParserData->Peek() != CPCL5DataType::PCL5_ESCAPE) )
		{
			if ( m_pParserData->Peek() == FORM_FEED )
			{
				DEBUG_PCL5_TEXT( _T("Got form feed!\n") );
				TrackEndPage();
			}
			m_pParserData->SkipAhead( sizeof(char) );
		}
	}

	inline void	CPCL5Parser::TrackBeginPage()
	{
		m_bNewPage = true;
	}

	inline void CPCL5Parser::TrackEndPage()
	{
		m_dwPageCount++;
		m_bNewPage = false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Static Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CPCL5Parser::InitializeCommandMap()
	{
		if ( m_bMapInitialized == false )
		{
			for ( const PCL5Command* pCommand = m_pCommandList ; pCommand->dwHashValue ; pCommand++ )
			{
				m_mapCommands.SetAt( pCommand->dwHashValue, pCommand->fpOperator );
			}
			DEBUG_PCL5_TEXT( _T("PCL5 Command Map Initialized!\n") );
			m_bMapInitialized = true;
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PCL5PARSER_H__FFD1B45B_EC98_11D3_A431_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL5Parser.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
