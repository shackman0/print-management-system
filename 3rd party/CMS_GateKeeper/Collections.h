///////////////////////////////////////////////////////////////////////////////////////////////////////
// Collections.h: Utility Functions for the Collection Classes
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_COLLECTIONS_H__4F9DD122_F88B_40C9_9203_812C4A0964FF__INCLUDED_)
#define AFX_COLLECTIONS_H__4F9DD122_F88B_40C9_9203_812C4A0964FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef _INC_NEW
	#include <new.h>
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	MAX_LENGTH_RANDOM_STRING			20
#define	MIN_LENGTH_RANDOM_STRING			5
#define MIN_PRINTABLE_CHAR					(BYTE)('A')
#define MAX_PRINTABLE_CHAR					(BYTE)('Z')

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Iteration Helper Types
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef __AFX_H__
	// Abstract iteration position (if needed)
	struct __POSITION { };
	typedef __POSITION* POSITION;
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CreateElements
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Template Function
template <typename TYPE>
void CreateElements( TYPE* pElements, DWORD dwCount = 1 )
{
	// Zero out the memory
	::ZeroMemory( pElements, dwCount*sizeof(TYPE) );

	// Call the constructors
	for ( ; dwCount-- ; pElements++ )
	{
		::new( (LPVOID)pElements ) TYPE;
	}
}

// Special Case

///////////////////////////////////////////////////////////////////////////////////////////////////////
// DestroyElements
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Template Function
template <typename TYPE>
void DestroyElements( TYPE* pElements, DWORD dwCount = 1 )
{
	// Call the destructors
	for ( ; dwCount-- ; pElements++ )
	{
		pElements->~TYPE();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// ElementString
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Template Function
template <typename TYPE>
CString ElementString( const TYPE& Element )
{
	CString strReturnMe;

	strReturnMe = Element;
	return strReturnMe;
}

// Special Case for WORD
template <>
inline CString ElementString<WORD>( const WORD& wElement )
{
	CString strReturnMe;

	strReturnMe.Format( _T("%u"), wElement );
	return strReturnMe;
}

// Special Case for DWORD
template <>
inline CString ElementString<DWORD>( const DWORD& dwElement )
{
	CString strReturnMe;

	strReturnMe.Format( _T("%u"), dwElement );
	return strReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// RandomizeElement
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef RANDOMIZE_ELEMENTS

#include "RandomNumber.h"

// Template Function
template <typename TYPE>
void RandomizeElement( TYPE& Element )
{
	CRandomNumber rnd;

	Element = rnd;
}

// Special Case for CString
template <>
inline void RandomizeElement<CString>( CString& strElement )
{
	CRandomNumber		rnd;
	DWORD				dwLength;
	LPTSTR				lpszTemp;
	unsigned register	nCounter;

	// Empty out the string
	strElement.Empty();

	// Get a random length
	dwLength = rnd.GetRangedValue( MAX_LENGTH_RANDOM_STRING, MIN_LENGTH_RANDOM_STRING );

	// Get the buffer
	lpszTemp = strElement.GetBufferSetLength( dwLength );

	// Randomize the letters
	for ( nCounter = 0 ; nCounter < dwLength ; nCounter++, lpszTemp++ )
	{
		*lpszTemp = static_cast<TCHAR>( rnd.GetRangedValue(MAX_PRINTABLE_CHAR, MIN_PRINTABLE_CHAR) );
	}

	// Release the buffer
	strElement.ReleaseBuffer();
}

#endif // #ifdef RANDOMIZE_ELEMENTS

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_COLLECTIONS_H__4F9DD122_F88B_40C9_9203_812C4A0964FF__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of Collections.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
