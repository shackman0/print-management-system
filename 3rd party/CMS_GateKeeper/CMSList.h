///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMSList.h: interface for the CCMSList typename.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMSLIST_H__8D1C9FEE_6E10_11D4_A462_00105A1C588D__INCLUDED_)
#define AFX_CMSLIST_H__8D1C9FEE_6E10_11D4_A462_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ThreadLock.h"
#include "DataChain.h"
#include "Collections.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_LIST_DEBUG
	#define DEBUG_LIST_TEXT						DEBUG_TEXT
#else
	#define DEBUG_LIST_TEXT						((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_SIZE_LIST_BLOCK_COUNT			16

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
using namespace CMSLIB;

namespace CMSList
{
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSList Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename TYPE, typename ARG_TYPE>
class CCMSList  
{
protected:
	class CNode
	{
	// Attributes
	private:
		CNode*	m_pNext;
		CNode*	m_pPrev;
		TYPE	m_Data;

	// Data Access
	private:
		CNode*		GetNext()					{ return m_pNext;	};
		CNode*		GetPrev()					{ return m_pPrev;	};
		ARG_TYPE	GetData()					{ return m_Data;	};
			
		void		SetNext( CNode* pNext )
		{
			m_pNext = pNext;
			if ( m_pNext != NULL )
			{
				m_pNext->m_pPrev = this;
			}
		}

		void		SetPrev( CNode* pPrev )
		{
			m_pPrev = pPrev;
			if ( m_pPrev != NULL )
			{
				m_pPrev->m_pNext = this;
			}
		}
		void		SetData( ARG_TYPE Data )	{ m_Data = Data;	};

	// Operations
	private:
		void		Initialize()
		{
			CreateElements<TYPE>( &m_Data, 1 );
			m_pNext	= NULL;
			m_pPrev	= NULL;
		}

		void		CleanUp()
		{
			DestroyElements<TYPE>( &m_Data, 1 );
			if ( m_pNext != NULL )
			{
				m_pNext->SetPrev( GetPrev() );
			}
			else if ( m_pPrev != NULL )
			{
				m_pPrev->SetNext( GetNext() );
			}
		}

	// Friend Declarations
		friend typename CCMSList<TYPE, ARG_TYPE>;
	};

// Construction / Destruction
public:
	CCMSList( const DWORD& dwBlockCount = DEFAULT_SIZE_LIST_BLOCK_COUNT );
	virtual ~CCMSList();

// Data Access
public:
	const DWORD&	GetCount() const;
	bool			IsEmpty() const;

// Head / Tail Functions
public:
	POSITION		GetHeadPosition() const;
	POSITION		GetTailPosition() const;
	ARG_TYPE		GetHead() const;
	ARG_TYPE		GetTail() const;

// Get Functions
public:
	ARG_TYPE		GetNext( POSITION& rPosition ) const;
	ARG_TYPE		GetPrev( POSITION& rPosition ) const;
	ARG_TYPE		GetAt( POSITION position ) const;
	ARG_TYPE		GetAtIndex( const DWORD& dwIndex ) const;

// Set Functions
public:
	void			SetAt( POSITION position, ARG_TYPE newValue );

// Find Operations
public:
	POSITION		Find( ARG_TYPE searchValue, POSITION startAfter = NULL ) const;
	POSITION		FindIndex( const DWORD& dwIndex ) const;

// Basic Add Operations
protected:
	void			AddHead( CNode* pNew );
	void			AddTail( CNode* pNew );

// Add before head
public:
	POSITION		AddHead( ARG_TYPE NewData );
	ARG_TYPE		AddHead();

// Add after Tail
public:
	POSITION		AddTail( ARG_TYPE NewData );
	ARG_TYPE		AddTail();

// Append a list of elements
public:
	void			AddHead( CCMSList<TYPE, ARG_TYPE>& lstOtherList );
	void			AddTail( CCMSList<TYPE, ARG_TYPE>& lstOtherList );

// Insert After/Before Operations
public:
	POSITION		InsertBefore( POSITION position, ARG_TYPE newElement );
	POSITION		InsertAfter( POSITION position, ARG_TYPE newElement );

// Remove Operations
public:
	ARG_TYPE		RemoveHead();							// Get the head node's data (and remove it)
	ARG_TYPE		RemoveTail();							// Get the tail node's data (and remove it)
	ARG_TYPE		RemoveAt( POSITION position );			// Remove at a given position
	ARG_TYPE		RemoveAtIndex( const DWORD& dwIndex );	// Remove at a given index
	virtual void	RemoveAll();							// Remove all elements

// Stack Operations
public:
	ARG_TYPE		GetTop() const;							// Returns the top of the stack
	void			Push( ARG_TYPE NewData );				// Adds data to the top of the stack
	ARG_TYPE		Pop();									// Removes the data on the top of the stack

// Create / Destroy
private:
	CNode*			CreateNode( CNode* pNext, CNode* pPrev );
	void			DestroyNode( CNode* pDestroyMe );

// Attributes
protected:
	CNode*			m_pNodeHead;
	CNode*			m_pNodeTail;
	DWORD			m_dwNodeCount;
	DWORD			m_dwBlockCount;
	CDataChain*		m_pDataChain;
	CNode*			m_pFreeList;

// Make Thread Safe
protected:
	DECLARE_THREAD_LOCK();

public:
	DECLARE_THREAD_LOCK_FUNCTIONS();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSList Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline CCMSList<TYPE,ARG_TYPE>::CCMSList( const DWORD& dwBlockCount )
	{
		// Make it thread safe
		LOCK_THREAD();

		m_pNodeHead		= NULL;
		m_pNodeTail		= NULL;
		m_dwNodeCount	= 0;
		m_dwBlockCount	= dwBlockCount;
		m_pDataChain	= NULL;
		m_pFreeList		= NULL;
	}

	template<typename TYPE, typename ARG_TYPE>
	inline CCMSList<TYPE,ARG_TYPE>::~CCMSList()
	{
		RemoveAll();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline const DWORD& CCMSList<TYPE,ARG_TYPE>::GetCount() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Return the value
		return m_dwNodeCount;
	}

	template<typename TYPE, typename ARG_TYPE>
	inline bool CCMSList<TYPE,ARG_TYPE>::IsEmpty() const
	{
		// Return the value
		return ( GetCount() == 0 );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Head/Tail Access Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetHead() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( m_pNodeHead != NULL );

		// Return Data of the Head Node
		return m_pNodeHead->GetData();
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetTail() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( m_pNodeTail != NULL );

		// Return Data of the Tail Node
		return m_pNodeTail->GetData();
	}

	template<typename TYPE, typename ARG_TYPE>
	inline POSITION CCMSList<TYPE,ARG_TYPE>::GetHeadPosition() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Return the value
		return reinterpret_cast<POSITION>(m_pNodeHead);
	}

	template<typename TYPE, typename ARG_TYPE>
	inline POSITION CCMSList<TYPE,ARG_TYPE>::GetTailPosition() const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Return the value
		return reinterpret_cast<POSITION>(m_pNodeTail);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Get Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetNext( POSITION& rPosition ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( rPosition != NULL );

		// Get the next value
		CNode* pNode	= reinterpret_cast<CNode*>(rPosition);
		rPosition		= reinterpret_cast<POSITION>(pNode->GetNext());

		// Return the data
		return pNode->GetData(); 
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetPrev( POSITION& rPosition ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( rPosition != NULL );

		// Get the next value
		CNode* pNode	= reinterpret_cast<CNode*>(rPosition);
		rPosition		= reinterpret_cast<POSITION>(pNode->GetPrev());

		// Return the data
		return pNode->GetData(); 
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetAt( POSITION position ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( position != NULL );

		CNode* pNode = reinterpret_cast<CNode*>(position);
		return pNode->GetData();
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetAtIndex( const DWORD& dwIndex ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Find the node indicated by the index and return its value
		return GetAt( FindIndex(dwIndex) );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Set Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline void CCMSList<TYPE,ARG_TYPE>::SetAt( POSITION position, ARG_TYPE newValue )
	{
		// Make it thread safe
		LOCK_THREAD();

		// Check assumptions
		_ASSERTE( position != NULL );
	
		if ( position != NULL )
		{
			CNode* pNode = reinterpret_cast<CNode*>(position);
			pNode->SetData(newValue);
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Find Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline POSITION CCMSList<TYPE,ARG_TYPE>::FindIndex( const DWORD& dwIndex ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Check assumptions
		_ASSERTE( dwIndex < m_dwNodeCount );

		CNode*				pNode		= m_pNodeHead;
		unsigned register	nCounter	= 0;

		// Find the node's position
		while ( (pNode != NULL) && (nCounter < dwIndex) )
		{
			pNode = pNode->GetNext();
			nCounter++;
		}

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Found Index %d of %d Nodes @ 0x%08x\n"), dwIndex, m_dwNodeCount, pNode );

		// Return the position
		return reinterpret_cast<POSITION>(pNode);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Basic Add Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline void CCMSList<TYPE,ARG_TYPE>::AddHead( CNode* pNew )
	{
		m_pNodeHead = pNew;
		if ( m_pNodeTail == NULL )
		{
			m_pNodeTail = pNew;
		}

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Inserted new node at the head (%d Node(s) Total)\n"), m_dwNodeCount );
	}

	template<typename TYPE, typename ARG_TYPE>
	inline void CCMSList<TYPE,ARG_TYPE>::AddTail( CNode* pNew )
	{
		m_pNodeTail = pNew;
		if ( m_pNodeHead == NULL )
		{
			m_pNodeHead = pNew;
		}

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Inserted new node at the tail (%d Node(s) Total)\n"), m_dwNodeCount );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Add Before Head
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline POSITION CCMSList<TYPE,ARG_TYPE>::AddHead( ARG_TYPE NewData )
	{
		// Make it thread safe
		LOCK_THREAD();

		CNode*	pNewNode = CreateNode( m_pNodeHead, NULL );
		AddHead( pNewNode );

		// Assign the data
		pNewNode->SetData( NewData );

		return reinterpret_cast<POSITION>(pNewNode);
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::AddHead()
	{
		// Make it thread safe
		LOCK_THREAD();

		CNode*	pNewNode = CreateNode( m_pNodeHead, NULL );
		AddHead( pNewNode );

		return pNewNode->GetData();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Add After Tail
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline POSITION CCMSList<TYPE,ARG_TYPE>::AddTail( ARG_TYPE NewData )
	{
		// Make it thread safe
		LOCK_THREAD();

		CNode*	pNewNode = CreateNode( NULL, m_pNodeTail );
		AddTail( pNewNode );

		// Assign the data
		pNewNode->SetData( NewData );

		return reinterpret_cast<POSITION>(pNewNode);
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::AddTail()
	{
		// Make it thread safe
		LOCK_THREAD();

		CNode*	pNewNode = CreateNode( NULL, m_pNodeTail );
		AddTail( pNewNode );

		return pNewNode->GetData();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Remove Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::RemoveHead()
	{
		// Make it thread safe
		LOCK_THREAD();

		// Check Assumptions
		_ASSERTE( m_pNodeHead != NULL );

		CNode*		pOldNode	= m_pNodeHead;
		ARG_TYPE	nReturnMe	= pOldNode->GetData();

		// Delete the head node
		m_pNodeHead = pOldNode->GetNext();
		if ( m_pNodeHead == NULL )
		{
			m_pNodeTail = NULL;
		}

		// Delete the node
		DestroyNode( pOldNode );

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Head Node Removed (%d Node(s) Remain)\n"), m_dwNodeCount );

		// Return the value
		return nReturnMe;
	}

	// Get the tail node's data (and remove it)
	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::RemoveTail()
	{
		// Make it thread safe
		LOCK_THREAD();

		// Check Assumptions
		_ASSERTE( m_pNodeTail != NULL );

		CNode*		pOldNode	= m_pNodeTail;
		ARG_TYPE	nReturnMe	= pOldNode->GetData();

		// Delete the tail node
		m_pNodeTail = pOldNode->GetPrev();
		if ( m_pNodeTail == NULL )
		{
			m_pNodeHead = NULL;
		}

		// Delete the node
		DestroyNode( pOldNode );

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Tail Node Removed (%d Node(s) Remain)\n"), m_dwNodeCount );

		// Return the value
		return nReturnMe;
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::RemoveAt( POSITION position )
	{
		// Make it thread safe
		LOCK_THREAD();

		// Check assumptions
		_ASSERTE( position != NULL );

		CNode*		pOldNode	= reinterpret_cast<CNode*>(position);
		ARG_TYPE	nReturnMe	= pOldNode->GetData();

		// Adjust the head pointer (if necessary)
		if ( pOldNode == m_pNodeHead )
		{
			m_pNodeHead = pOldNode->GetNext();
		}

		// Adjust the tail pointer (if necessary)
		if ( pOldNode == m_pNodeTail )
		{
			m_pNodeTail = pOldNode->GetPrev();
		}

		// Delete the node
		DestroyNode( pOldNode );

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Removed Node @ 0x%08x (%d Node(s) Remain)\n"), position, m_dwNodeCount );

		// Return the value
		return nReturnMe;
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::RemoveAtIndex( const DWORD& dwIndex )
	{
		// Display debugging information
		DEBUG_LIST_TEXT( _T("Removing Node with Index #%d\n"), dwIndex );

		return RemoveAt( FindIndex(dwIndex) );
	}

	// Remove all elements
	template<typename TYPE, typename ARG_TYPE>
	inline void CCMSList<TYPE,ARG_TYPE>::RemoveAll()
	{
		// Make it thread safe
		LOCK_THREAD();

		// Display debugging information
		DEBUG_LIST_TEXT( _T("Removing all %d Node(s)!\n"), m_dwNodeCount );

		// Remove the head until all elements are gone
		while ( GetCount() )
		{
			RemoveHead();
		}

		// Clean up the Data Chain
		m_pFreeList = NULL;
		m_pDataChain->RemoveAll( m_pDataChain );
		m_pDataChain = NULL;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Stack Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::GetTop() const
	{
		return GetHead();
	}

	template<typename TYPE, typename ARG_TYPE>
	inline void CCMSList<TYPE,ARG_TYPE>::Push( ARG_TYPE NewData )
	{
		AddHead( NewData );
	}

	template<typename TYPE, typename ARG_TYPE>
	inline ARG_TYPE CCMSList<TYPE,ARG_TYPE>::Pop()
	{
		return RemoveHead();
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Regular Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Find Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	POSITION CCMSList<TYPE,ARG_TYPE>::Find( ARG_TYPE searchValue, POSITION startAfter ) const
	{
		// Make it thread safe
		LOCK_THREAD_CONST();

		// Note: O(n) speed
		CNode* pNode = reinterpret_cast<CNode*>(startAfter);
		if (pNode == NULL)
		{
			// Start at the beginning
			pNode = m_pNodeHead;
		}
		else
		{
			// Start after the one specified
			pNode = pNode->GetNext();
		}

		for ( ; pNode != NULL ; pNode = pNode->GetNext() )
		{
			if ( pNode->GetData() == searchValue )
			{
				return reinterpret_cast<POSITION>(pNode);
			}
		}
		return NULL;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Append Other Lists
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	void CCMSList<TYPE,ARG_TYPE>::AddHead( CCMSList<TYPE, ARG_TYPE>& lstOtherList )
	{
		// Make it thread safe
		LOCK_THREAD();
		TEMP_LOCK_THREAD( lstOtherList.STD_LOCK_NAME );

		CNode* pOtherTailNode = lstOtherList.m_pNodeTail;
	
		// Hook up the end of the other list to the start of the current one
		pOtherTailNode->SetNext( m_pNodeHead );
		m_pNodeHead->SetPrev( pOtherTailNode );
		
		// Set the head of this list to be the head of the other list
		m_pNodeHead = lstOtherList.m_pNodeHead;

		// Increment the node count
		m_dwNodeCount += lstOtherList.m_dwNodeCount;

		// Append the data chain
		m_pDataChain->Append( lstOtherList.m_pDataChain );

		// Append the free list
		if ( lstOtherList.m_pFreeList != NULL )
		{
			if ( m_pFreeList != NULL )
			{
				CNode* pFree = m_pFreeList;

				while ( pFree->m_pNext != NULL )
				{
					pFree = pFree->m_pNext;
				}

				pFree->m_pNext = lstOtherList.m_pFreeList;
			}
			else
			{
				m_pFreeList = lstOtherList.m_pFreeList;
			}
		}
		
		// Clear out the other list's items
		lstOtherList.m_dwNodeCount	= 0;
		lstOtherList.m_pNodeHead	= NULL;
		lstOtherList.m_pNodeTail	= NULL;
		lstOtherList.m_pDataChain	= NULL;
		lstOtherList.m_pFreeList	= NULL;
	}

	template<typename TYPE, typename ARG_TYPE>
	void CCMSList<TYPE,ARG_TYPE>::AddTail( CCMSList<TYPE, ARG_TYPE>& lstOtherList )
	{
		// Make it thread safe
		LOCK_THREAD();
		TEMP_LOCK_THREAD( lstOtherList.STD_LOCK_NAME );

		CNode* pOtherHeadNode = lstOtherList.m_pNodeHead;
	
		// Hook up the end of the the current list to the start of the other one
		pOtherHeadNode->SetPrev( m_pNodeTail );
		m_pNodeTail->SetNext( pOtherHeadNode );

		// Set the tail of this list to be the tail of the other list
		m_pNodeTail = lstOtherList.m_pNodeTail;

		// Increment the node count
		m_dwNodeCount += lstOtherList.m_dwNodeCount;

		// Append the data chain
		m_pDataChain->Append( lstOtherList.m_pDataChain );

		// Append the free list
		if ( lstOtherList.m_pFreeList != NULL )
		{
			if ( m_pFreeList != NULL )
			{
				CNode* pFree = m_pFreeList;

				while ( pFree->m_pNext != NULL )
				{
					pFree = pFree->m_pNext;
				}

				pFree->m_pNext = lstOtherList.m_pFreeList;
			}
			else
			{
				m_pFreeList = lstOtherList.m_pFreeList;
			}
		}
		
		// Clear out the other list's items
		lstOtherList.m_dwNodeCount	= 0;
		lstOtherList.m_pNodeHead	= NULL;
		lstOtherList.m_pNodeTail	= NULL;
		lstOtherList.m_pDataChain	= NULL;
		lstOtherList.m_pFreeList	= NULL;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Insert After/Before Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	POSITION CCMSList<TYPE,ARG_TYPE>::InsertBefore( POSITION position, ARG_TYPE newElement )
	{
		// Make it thread safe
		LOCK_THREAD();

		if ( m_dwNodeCount > 0 )
		{
			CNode* pNodeBefore = reinterpret_cast<CNode*>(position);
			if ( pNodeBefore == NULL )
			{
				// Start at the beginning
				pNodeBefore = m_pNodeHead;
			}

			CNode* pNewNode = CreateNode( pNodeBefore, pNodeBefore->GetPrev() );
			if ( pNodeBefore == m_pNodeHead )
			{
				m_pNodeHead = pNewNode;
			}

			// Assign the data
			pNewNode->SetData( newElement );

			// Display debugging information
			DEBUG_LIST_TEXT( _T("Inserted new node before 0x%08x (%d Node(s) Total)\n"),
				position, m_dwNodeCount );

			// Return the result
			return reinterpret_cast<POSITION>(pNewNode);
		}
		else
		{
			return AddHead( newElement );
		}
	}

	template<typename TYPE, typename ARG_TYPE>
	POSITION CCMSList<TYPE,ARG_TYPE>::InsertAfter( POSITION position, ARG_TYPE newElement )
	{
		// Make it thread safe
		LOCK_THREAD();

		if ( m_dwNodeCount > 0 )
		{
			CNode* pNodeAfter = reinterpret_cast<CNode*>(position);
			if ( pNodeAfter == NULL )
			{
				// Start at the end
				pNodeAfter = m_pNodeTail;
			}

			CNode* pNewNode = CreateNode( pNodeAfter->GetNext(), pNodeAfter );
			if ( pNodeAfter == m_pNodeTail )
			{
				m_pNodeTail = pNewNode;
			}

			// Assign the data
			pNewNode->SetData( newElement );

			// Display debugging information
			DEBUG_LIST_TEXT( _T("Inserted new node after 0x%08x (%d Node(s) Total)\n"),
				position, m_dwNodeCount );

			// Return the result
			return reinterpret_cast<POSITION>(pNewNode);
		}
		else
		{
			return AddTail( newElement );
		}
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Create / Destroy Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename TYPE, typename ARG_TYPE>
	CCMSList<TYPE,ARG_TYPE>::CNode*	CCMSList<TYPE,ARG_TYPE>::CreateNode( CNode* pNext, CNode* pPrev )
	{
		// Check to see if any free nodes are available
		if ( m_pFreeList == NULL )
		{
			DEBUG_LIST_TEXT( _T("CMSList: Allocating %d blocks, Size = %d bytes\n"), 
				m_dwBlockCount, sizeof(CCMSList::CNode) );

			CDataChain* pNewBlock = CDataChain::Add( m_pDataChain, 
				(m_dwBlockCount * sizeof(CCMSList::CNode)) );

			// Link them together
			CNode* pNode = reinterpret_cast<CNode*>(pNewBlock->GetData());
			for ( unsigned register nCounter = 0 ; nCounter < m_dwBlockCount ; nCounter++ )
			{
				pNode->m_pNext	= m_pFreeList;
				m_pFreeList		= pNode;
				pNode++;
			}
		}

		// There must be free blocks now
		_ASSERTE( m_pFreeList != NULL );

		// Now create the new node
		CNode* pNewNode = m_pFreeList;
		m_pFreeList = m_pFreeList->m_pNext;

		pNewNode->Initialize();
		pNewNode->SetNext( pNext );
		pNewNode->SetPrev( pPrev );

		// Increment the counter
		m_dwNodeCount++;

		// Make sure there is no overflow
		_ASSERTE( m_dwNodeCount > 0 );

		// Return the result
		return pNewNode;
	}

	template<typename TYPE, typename ARG_TYPE>
	void CCMSList<TYPE,ARG_TYPE>::DestroyNode( CNode* pDestroyMe )
	{
		// DESTROY THE ELEMENTS
		pDestroyMe->CleanUp();

		// Add it to the free list
		pDestroyMe->m_pNext = m_pFreeList;
		m_pFreeList = pDestroyMe;

		// Decrement the entry count
		--m_dwNodeCount;
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
}	// End of CMSList Namespace
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_CMSLIST_H__8D1C9FEE_6E10_11D4_A462_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of CMSList.h
///////////////////////////////////////////////////////////////////////////////////////////////////////