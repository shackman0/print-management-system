///////////////////////////////////////////////////////////////////////////////////////////////////////
// TextJobHandler.cpp: implementation of the CTextJobHandler class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "SpoolMemory.h"
#include "TextJobHandler.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debug Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrepareJob Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CTextJobHandler::PrepareJob()
{
	if ( ReadRawJob() )
	{
		// Create the access object
		VectorData		vdRawData(*m_pRawData);

		// Check assumptions
		_ASSERTE( vdRawData.Length() > 0 );

		// Prepare the text job
		PrepareTextJob( CPrintJobHandlerBase::m_lpszDocumentName, 
			CPrintJobHandlerBase::m_pPrintProcessorData, vdRawData );
	}
	else
	{
		DEBUG_OBJ_TEXT( _T("ReadRawData Failed!\n") );
		throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_NoData );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrepareRequest Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CTextJobHandler::PrepareRequest()
{
	// Count the pages
	m_ParserData.SetPageCount( GetPageCount() );
	// Set the data type
	m_ParserData.SetDataType( DATATYPE_TEXTJOB );
	DEBUG_OBJ_TEXT( _T("Text Page Count: %d\n"), m_ParserData.GetPageCount() );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJob Function
///////////////////////////////////////////////////////////////////////////////////////////////////////

BOOL CTextJobHandler::PrintJob()
{
	// Check assumptions
	_ASSERTE( CPrintJobHandlerBase::m_pPrintProcessorData != NULL );
	_ASSERTE( CPrintJobHandlerBase::m_lpszDocumentName != NULL );

	if ( m_pRawData == NULL )
	{
		if ( ReadRawJob() == FALSE )
		{
			DEBUG_TEXT( _T("ReadRawData Failed!\n") );
			throw TEXTJOB_EXCEPTION( CTextJobException::exTextJob_NoData );
		}
	}

	// Create the access object
	VectorData	vdRawData(*m_pRawData);							// Data Access object
	DWORD		dwPagesPerCopy	= GetPageCount();				// Parsed number of pages per copy
	DWORD		dwPrinted		= PrintTextJob( vdRawData );	// Actual number of pages printed

	// If the pages printed doesn't match what is expected, make sure it gets updated
	if ( (dwPagesPerCopy * CPrintJobHandlerBase::m_pPrintProcessorData->Copies) != dwPrinted )
	{
		m_bJobAborted		= true;
		m_dwPagesPrinted	= dwPrinted;
		m_dwCopiesPrinted	= 1;
	}
	DEBUG_OBJ_TEXT( _T("Print Text %s (%d Total Pages Printed)\n"),
		( ((dwPagesPerCopy * CPrintJobHandlerBase::m_pPrintProcessorData->Copies) == dwPrinted)
		? _T("Succeeded") : _T("Failed") ), dwPrinted );

	// Return success
	return TRUE;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of TextJobHandler.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
