/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Thu Jul 26 12:46:14 2001
 */
/* Compiler settings for D:\Data\Projects\CMS\Print Tracking\CMSnet\Server\cmsnet\CMSnet.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __CMSnet_h__
#define __CMSnet_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IPrintJob_FWD_DEFINED__
#define __IPrintJob_FWD_DEFINED__
typedef interface IPrintJob IPrintJob;
#endif 	/* __IPrintJob_FWD_DEFINED__ */


#ifndef __IAddInManager_FWD_DEFINED__
#define __IAddInManager_FWD_DEFINED__
typedef interface IAddInManager IAddInManager;
#endif 	/* __IAddInManager_FWD_DEFINED__ */


#ifndef __IAddInManager2_FWD_DEFINED__
#define __IAddInManager2_FWD_DEFINED__
typedef interface IAddInManager2 IAddInManager2;
#endif 	/* __IAddInManager2_FWD_DEFINED__ */


#ifndef __PrintJob_FWD_DEFINED__
#define __PrintJob_FWD_DEFINED__

#ifdef __cplusplus
typedef class PrintJob PrintJob;
#else
typedef struct PrintJob PrintJob;
#endif /* __cplusplus */

#endif 	/* __PrintJob_FWD_DEFINED__ */


#ifndef __AddInManager_FWD_DEFINED__
#define __AddInManager_FWD_DEFINED__

#ifdef __cplusplus
typedef class AddInManager AddInManager;
#else
typedef struct AddInManager AddInManager;
#endif /* __cplusplus */

#endif 	/* __AddInManager_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"
//#include "ProcessPrintJob.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

/* interface __MIDL_itf_CMSnet_0000 */
/* [local] */ 

typedef /* [helpstring][uuid] */ 
enum _error_type
    {	ErrorInformation	= 0,
	ErrorWarning	= ErrorInformation + 1,
	ErrorError	= ErrorWarning + 1
    }	ErrorType;



extern RPC_IF_HANDLE __MIDL_itf_CMSnet_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_CMSnet_0000_v0_0_s_ifspec;

#ifndef __IPrintJob_INTERFACE_DEFINED__
#define __IPrintJob_INTERFACE_DEFINED__

/* interface IPrintJob */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IPrintJob;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D00A4D7B-BD54-11d3-A41C-00E02931B18E")
    IPrintJob : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetProperties( 
            /* [out] */ DWORD __RPC_FAR *pdwCount,
            /* [size_is][size_is][out] */ BSTR __RPC_FAR *__RPC_FAR *pProperties,
            /* [size_is][size_is][out] */ VARIANT __RPC_FAR *__RPC_FAR *pValues) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetProperties( 
            /* [in] */ DWORD dwCount,
            /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
            /* [size_is][in] */ VARIANT __RPC_FAR *pValues) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetSingleProperty( 
            /* [in] */ BSTR bstrProperty,
            /* [out] */ VARIANT __RPC_FAR *pValue) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetSingleProperty( 
            /* [in] */ BSTR bstrProperty,
            /* [in] */ VARIANT varValue) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ClearProperties( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SubmitRequest( 
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SubmitRequestWithProperties( 
            /* [in] */ DWORD dwCount,
            /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
            /* [size_is][in] */ VARIANT __RPC_FAR *pValues,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IPrintJobVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IPrintJob __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IPrintJob __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IPrintJob __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetProperties )( 
            IPrintJob __RPC_FAR * This,
            /* [out] */ DWORD __RPC_FAR *pdwCount,
            /* [size_is][size_is][out] */ BSTR __RPC_FAR *__RPC_FAR *pProperties,
            /* [size_is][size_is][out] */ VARIANT __RPC_FAR *__RPC_FAR *pValues);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetProperties )( 
            IPrintJob __RPC_FAR * This,
            /* [in] */ DWORD dwCount,
            /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
            /* [size_is][in] */ VARIANT __RPC_FAR *pValues);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetSingleProperty )( 
            IPrintJob __RPC_FAR * This,
            /* [in] */ BSTR bstrProperty,
            /* [out] */ VARIANT __RPC_FAR *pValue);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetSingleProperty )( 
            IPrintJob __RPC_FAR * This,
            /* [in] */ BSTR bstrProperty,
            /* [in] */ VARIANT varValue);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ClearProperties )( 
            IPrintJob __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SubmitRequest )( 
            IPrintJob __RPC_FAR * This,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SubmitRequestWithProperties )( 
            IPrintJob __RPC_FAR * This,
            /* [in] */ DWORD dwCount,
            /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
            /* [size_is][in] */ VARIANT __RPC_FAR *pValues,
            /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted);
        
        END_INTERFACE
    } IPrintJobVtbl;

    interface IPrintJob
    {
        CONST_VTBL struct IPrintJobVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IPrintJob_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IPrintJob_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IPrintJob_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IPrintJob_GetProperties(This,pdwCount,pProperties,pValues)	\
    (This)->lpVtbl -> GetProperties(This,pdwCount,pProperties,pValues)

#define IPrintJob_SetProperties(This,dwCount,pProperties,pValues)	\
    (This)->lpVtbl -> SetProperties(This,dwCount,pProperties,pValues)

#define IPrintJob_GetSingleProperty(This,bstrProperty,pValue)	\
    (This)->lpVtbl -> GetSingleProperty(This,bstrProperty,pValue)

#define IPrintJob_SetSingleProperty(This,bstrProperty,varValue)	\
    (This)->lpVtbl -> SetSingleProperty(This,bstrProperty,varValue)

#define IPrintJob_ClearProperties(This)	\
    (This)->lpVtbl -> ClearProperties(This)

#define IPrintJob_SubmitRequest(This,pRequestGranted)	\
    (This)->lpVtbl -> SubmitRequest(This,pRequestGranted)

#define IPrintJob_SubmitRequestWithProperties(This,dwCount,pProperties,pValues,pRequestGranted)	\
    (This)->lpVtbl -> SubmitRequestWithProperties(This,dwCount,pProperties,pValues,pRequestGranted)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_GetProperties_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [out] */ DWORD __RPC_FAR *pdwCount,
    /* [size_is][size_is][out] */ BSTR __RPC_FAR *__RPC_FAR *pProperties,
    /* [size_is][size_is][out] */ VARIANT __RPC_FAR *__RPC_FAR *pValues);


void __RPC_STUB IPrintJob_GetProperties_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_SetProperties_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [in] */ DWORD dwCount,
    /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
    /* [size_is][in] */ VARIANT __RPC_FAR *pValues);


void __RPC_STUB IPrintJob_SetProperties_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_GetSingleProperty_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [in] */ BSTR bstrProperty,
    /* [out] */ VARIANT __RPC_FAR *pValue);


void __RPC_STUB IPrintJob_GetSingleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_SetSingleProperty_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [in] */ BSTR bstrProperty,
    /* [in] */ VARIANT varValue);


void __RPC_STUB IPrintJob_SetSingleProperty_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_ClearProperties_Proxy( 
    IPrintJob __RPC_FAR * This);


void __RPC_STUB IPrintJob_ClearProperties_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_SubmitRequest_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted);


void __RPC_STUB IPrintJob_SubmitRequest_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IPrintJob_SubmitRequestWithProperties_Proxy( 
    IPrintJob __RPC_FAR * This,
    /* [in] */ DWORD dwCount,
    /* [size_is][in] */ BSTR __RPC_FAR *pProperties,
    /* [size_is][in] */ VARIANT __RPC_FAR *pValues,
    /* [retval][out] */ VARIANT_BOOL __RPC_FAR *pRequestGranted);


void __RPC_STUB IPrintJob_SubmitRequestWithProperties_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IPrintJob_INTERFACE_DEFINED__ */


#ifndef __IAddInManager_INTERFACE_DEFINED__
#define __IAddInManager_INTERFACE_DEFINED__

/* interface IAddInManager */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAddInManager;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D00A4D7C-BD54-11d3-A41C-00E02931B18E")
    IAddInManager : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Add( 
            /* [in] */ BSTR bstrModuleInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Remove( 
            /* [in] */ BSTR bstrModuleInfo) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE ReportError( 
            /* [in] */ BSTR bstrModuleInfo,
            /* [in] */ BSTR bstrErrorInfo,
            /* [in] */ ErrorType Error) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAddInManagerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IAddInManager __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IAddInManager __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IAddInManager __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Add )( 
            IAddInManager __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Remove )( 
            IAddInManager __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ReportError )( 
            IAddInManager __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo,
            /* [in] */ BSTR bstrErrorInfo,
            /* [in] */ ErrorType Error);
        
        END_INTERFACE
    } IAddInManagerVtbl;

    interface IAddInManager
    {
        CONST_VTBL struct IAddInManagerVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAddInManager_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAddInManager_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAddInManager_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAddInManager_Add(This,bstrModuleInfo)	\
    (This)->lpVtbl -> Add(This,bstrModuleInfo)

#define IAddInManager_Remove(This,bstrModuleInfo)	\
    (This)->lpVtbl -> Remove(This,bstrModuleInfo)

#define IAddInManager_ReportError(This,bstrModuleInfo,bstrErrorInfo,Error)	\
    (This)->lpVtbl -> ReportError(This,bstrModuleInfo,bstrErrorInfo,Error)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager_Add_Proxy( 
    IAddInManager __RPC_FAR * This,
    /* [in] */ BSTR bstrModuleInfo);


void __RPC_STUB IAddInManager_Add_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager_Remove_Proxy( 
    IAddInManager __RPC_FAR * This,
    /* [in] */ BSTR bstrModuleInfo);


void __RPC_STUB IAddInManager_Remove_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager_ReportError_Proxy( 
    IAddInManager __RPC_FAR * This,
    /* [in] */ BSTR bstrModuleInfo,
    /* [in] */ BSTR bstrErrorInfo,
    /* [in] */ ErrorType Error);


void __RPC_STUB IAddInManager_ReportError_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAddInManager_INTERFACE_DEFINED__ */


#ifndef __IAddInManager2_INTERFACE_DEFINED__
#define __IAddInManager2_INTERFACE_DEFINED__

/* interface IAddInManager2 */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAddInManager2;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("D50834C8-26B5-440a-877B-49B72CB528E4")
    IAddInManager2 : public IAddInManager
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Install( 
            /* [in] */ REFCLSID rclsid,
            /* [in] */ LONG nSequenceNumber) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Uninstall( 
            /* [in] */ REFCLSID rclsid) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE QuerySettingString( 
            /* [in] */ BSTR bstrSettingName,
            /* [out] */ BSTR __RPC_FAR *pValue) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE QuerySettingLong( 
            /* [in] */ BSTR bstrSettingName,
            /* [out] */ LONG __RPC_FAR *pValue) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE QueryCustomerName( 
            /* [out] */ BSTR __RPC_FAR *pValue) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAddInManager2Vtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IAddInManager2 __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IAddInManager2 __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Add )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Remove )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *ReportError )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ BSTR bstrModuleInfo,
            /* [in] */ BSTR bstrErrorInfo,
            /* [in] */ ErrorType Error);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Install )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ REFCLSID rclsid,
            /* [in] */ LONG nSequenceNumber);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Uninstall )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ REFCLSID rclsid);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QuerySettingString )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ BSTR bstrSettingName,
            /* [out] */ BSTR __RPC_FAR *pValue);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QuerySettingLong )( 
            IAddInManager2 __RPC_FAR * This,
            /* [in] */ BSTR bstrSettingName,
            /* [out] */ LONG __RPC_FAR *pValue);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryCustomerName )( 
            IAddInManager2 __RPC_FAR * This,
            /* [out] */ BSTR __RPC_FAR *pValue);
        
        END_INTERFACE
    } IAddInManager2Vtbl;

    interface IAddInManager2
    {
        CONST_VTBL struct IAddInManager2Vtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAddInManager2_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IAddInManager2_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IAddInManager2_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IAddInManager2_Add(This,bstrModuleInfo)	\
    (This)->lpVtbl -> Add(This,bstrModuleInfo)

#define IAddInManager2_Remove(This,bstrModuleInfo)	\
    (This)->lpVtbl -> Remove(This,bstrModuleInfo)

#define IAddInManager2_ReportError(This,bstrModuleInfo,bstrErrorInfo,Error)	\
    (This)->lpVtbl -> ReportError(This,bstrModuleInfo,bstrErrorInfo,Error)


#define IAddInManager2_Install(This,rclsid,nSequenceNumber)	\
    (This)->lpVtbl -> Install(This,rclsid,nSequenceNumber)

#define IAddInManager2_Uninstall(This,rclsid)	\
    (This)->lpVtbl -> Uninstall(This,rclsid)

#define IAddInManager2_QuerySettingString(This,bstrSettingName,pValue)	\
    (This)->lpVtbl -> QuerySettingString(This,bstrSettingName,pValue)

#define IAddInManager2_QuerySettingLong(This,bstrSettingName,pValue)	\
    (This)->lpVtbl -> QuerySettingLong(This,bstrSettingName,pValue)

#define IAddInManager2_QueryCustomerName(This,pValue)	\
    (This)->lpVtbl -> QueryCustomerName(This,pValue)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager2_Install_Proxy( 
    IAddInManager2 __RPC_FAR * This,
    /* [in] */ REFCLSID rclsid,
    /* [in] */ LONG nSequenceNumber);


void __RPC_STUB IAddInManager2_Install_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager2_Uninstall_Proxy( 
    IAddInManager2 __RPC_FAR * This,
    /* [in] */ REFCLSID rclsid);


void __RPC_STUB IAddInManager2_Uninstall_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager2_QuerySettingString_Proxy( 
    IAddInManager2 __RPC_FAR * This,
    /* [in] */ BSTR bstrSettingName,
    /* [out] */ BSTR __RPC_FAR *pValue);


void __RPC_STUB IAddInManager2_QuerySettingString_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager2_QuerySettingLong_Proxy( 
    IAddInManager2 __RPC_FAR * This,
    /* [in] */ BSTR bstrSettingName,
    /* [out] */ LONG __RPC_FAR *pValue);


void __RPC_STUB IAddInManager2_QuerySettingLong_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IAddInManager2_QueryCustomerName_Proxy( 
    IAddInManager2 __RPC_FAR * This,
    /* [out] */ BSTR __RPC_FAR *pValue);


void __RPC_STUB IAddInManager2_QueryCustomerName_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IAddInManager2_INTERFACE_DEFINED__ */



#ifndef __CMSnetLib_LIBRARY_DEFINED__
#define __CMSnetLib_LIBRARY_DEFINED__

/* library CMSnetLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_CMSnetLib;

EXTERN_C const CLSID CLSID_PrintJob;

#ifdef __cplusplus

class DECLSPEC_UUID("D00A4D85-BD54-11d3-A41C-00E02931B18E")
PrintJob;
#endif

EXTERN_C const CLSID CLSID_AddInManager;

#ifdef __cplusplus

class DECLSPEC_UUID("D00A4D8B-BD54-11d3-A41C-00E02931B18E")
AddInManager;
#endif
#endif /* __CMSnetLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
