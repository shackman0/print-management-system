//////////////////////////////////////////////////////////////////////
// WinPPI.h: Procedure declarations, constant definitions and macros for the GDI component.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WINPPI_H__A95038C4_8A72_11D4_A46C_00105A1C588D__INCLUDED_)
#define AFX_WINPPI_H__A95038C4_8A72_11D4_A46C_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// Use "C" Style Definitions
//////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
extern "C"
{
#endif  // #ifdef __cplusplus

//////////////////////////////////////////////////////////////////////
// Definitions
//////////////////////////////////////////////////////////////////////

#define EMF_DUP_NONE			0
#define EMF_DUP_VERT			1
#define EMF_DUP_HORZ			2

#define EMF_DEGREE_90			1
#define EMF_DEGREE_270			2

//////////////////////////////////////////////////////////////////////
// Definitions from the Windows 2000 DDK (winddiui.h)
//////////////////////////////////////////////////////////////////////

// Print processor capabilities for the driver.
#define BORDER_PRINT                   0x00000000        // default
#define NO_BORDER_PRINT                0x00000001

#define NORMAL_PRINT                   0x00000000        // default
#define REVERSE_PRINT                  0x00000001
#define BOOKLET_PRINT                  0x00000002

#define NO_COLOR_OPTIMIZATION          0x00000000        // default
#define COLOR_OPTIMIZATION             0x00000001

typedef struct _ATTRIBUTE_INFO_1 {
    DWORD    dwJobNumberOfPagesPerSide;
    DWORD    dwDrvNumberOfPagesPerSide;
    DWORD    dwNupBorderFlags;
    DWORD    dwJobPageOrderFlags;
    DWORD    dwDrvPageOrderFlags;
    DWORD    dwJobNumberOfCopies;
    DWORD    dwDrvNumberOfCopies;
} ATTRIBUTE_INFO_1, *PATTRIBUTE_INFO_1;

typedef struct _ATTRIBUTE_INFO_2 {
    DWORD    dwJobNumberOfPagesPerSide;
    DWORD    dwDrvNumberOfPagesPerSide;
    DWORD    dwNupBorderFlags;
    DWORD    dwJobPageOrderFlags;
    DWORD    dwDrvPageOrderFlags;
    DWORD    dwJobNumberOfCopies;
    DWORD    dwDrvNumberOfCopies;
    DWORD    dwColorOptimization;           // Added for monochrome optimization
} ATTRIBUTE_INFO_2, *PATTRIBUTE_INFO_2;

typedef struct _ATTRIBUTE_INFO_3 {
    DWORD    dwJobNumberOfPagesPerSide;
    DWORD    dwDrvNumberOfPagesPerSide;
    DWORD    dwNupBorderFlags;
    DWORD    dwJobPageOrderFlags;
    DWORD    dwDrvPageOrderFlags;
    DWORD    dwJobNumberOfCopies;
    DWORD    dwDrvNumberOfCopies;
    DWORD    dwColorOptimization;           // Added for monochrome optimization
    short    dmPrintQuality;                // Added for monochrome optimization
    short    dmYResolution;                 // Added for monochrome optimization
} ATTRIBUTE_INFO_3, *PATTRIBUTE_INFO_3;
/*
#ifndef STRUCT_PRINTPROCESSOR_CAPS_1
#define STRUCT_PRINTPROCESSOR_CAPS_1

typedef struct _PRINTPROCESSOR_CAPS_1 {
    DWORD     dwLevel;
    DWORD     dwNupOptions;
    DWORD     dwPageOrderFlags;
    DWORD     dwNumberOfCopies;
} PRINTPROCESSOR_CAPS_1, *PPRINTPROCESSOR_CAPS_1;

#endif
*/
//////////////////////////////////////////////////////////////////////
// Structure Definition
//////////////////////////////////////////////////////////////////////

typedef struct _pfnWinSpoolDrv
{
    BOOL    (*pfnOpenPrinter)(LPTSTR, LPHANDLE, LPPRINTER_DEFAULTS);
    BOOL    (*pfnClosePrinter)(HANDLE);
    BOOL    (*pfnDevQueryPrint)(HANDLE, LPDEVMODE, DWORD *, LPWSTR, DWORD);
    BOOL    (*pfnPrinterEvent)(LPWSTR, INT, DWORD, LPARAM);
    LONG    (*pfnDocumentProperties)(HWND, HANDLE, LPWSTR, PDEVMODE, PDEVMODE, DWORD);
    HANDLE  (*pfnLoadPrinterDriver)(HANDLE);
    BOOL    (*pfnSetDefaultPrinter)(LPCWSTR);
    BOOL    (*pfnGetDefaultPrinter)(LPWSTR, LPDWORD);
    HANDLE  (*pfnRefCntLoadDriver)(LPWSTR, DWORD, DWORD, BOOL);
    BOOL    (*pfnRefCntUnloadDriver)(HANDLE, BOOL);
    BOOL    (*pfnForceUnloadDriver)(LPWSTR);
} fnWinSpoolDrv, *pfnWinSpoolDrv;

//////////////////////////////////////////////////////////////////////
// Imported Prototypes
//////////////////////////////////////////////////////////////////////

typedef int (CALLBACK* EMFPLAYPROC)( HDC hdc, INT iFunction, HANDLE hPageQuery );

// GDI Prototypes
BOOL WINAPI GdiPlayEMF
(
	LPWSTR		pwszPrinterName,
	LPDEVMODEW	pDevmode,
	LPWSTR		pwszDocName,
	EMFPLAYPROC pfnEMFPlayFn,
	HANDLE		hPageQuery
);

HANDLE WINAPI GdiGetSpoolFileHandle
(
	LPWSTR	   pwszPrinterName,
	LPDEVMODEW pDevmode,
	LPWSTR	   pwszDocName
);

BOOL WINAPI GdiDeleteSpoolFileHandle
(
	HANDLE	   SpoolFileHandle
);

DWORD WINAPI GdiGetPageCount
(
	HANDLE	   SpoolFileHandle
);

HDC WINAPI GdiGetDC
(
	HANDLE	   SpoolFileHandle
);

HANDLE WINAPI GdiGetPageHandle
(
	HANDLE	   SpoolFileHandle,
	DWORD	   Page,
	LPDWORD    pdwPageType
);

BOOL WINAPI GdiStartDocEMF
(
	HANDLE	   SpoolFileHandle,
	DOCINFOW   *pDocInfo
);

BOOL WINAPI GdiStartPageEMF
(
	HANDLE	   SpoolFileHandle
);

BOOL WINAPI GdiPlayPageEMF
(
	HANDLE	   SpoolFileHandle,
	HANDLE	   hemf,
	RECT	   *prectDocument,
	RECT	   *prectBorder,
	RECT	   *prectClip
);

BOOL WINAPI GdiEndPageEMF
(
	HANDLE	   SpoolFileHandle,
	DWORD	   dwOptimization
);

BOOL WINAPI GdiEndDocEMF
(
	HANDLE	   SpoolFileHandle
);

BOOL WINAPI GdiGetDevmodeForPage
(
	HANDLE	   SpoolFileHandle,
	DWORD	   dwPageNumber,
	PDEVMODEW  *pCurrDM,
	PDEVMODEW  *pLastDM
);

BOOL WINAPI GdiResetDCEMF
(
	HANDLE	   SpoolFileHandle,
	PDEVMODEW  pCurrDM
);

// SpoolSS Prototypes
//
// NOTE:
//		To use these functions, you must add "spoolss.lib" to the project file.
//		That file can be found in the Windows 2000 DDK.
//
BOOL WINAPI GetJobAttributes
(
	LPTSTR pPrinterName,
	LPDEVMODE pDevmode,
	PATTRIBUTE_INFO_3 pAttributeInfo
);

BOOL WINAPI SplInitializeWinSpoolDrv
(
	pfnWinSpoolDrv pfnList
);

//////////////////////////////////////////////////////////////////////
// End of "C" Style
//////////////////////////////////////////////////////////////////////

#ifdef __cplusplus
}
#endif  // #ifdef __cplusplus

//////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_WINPPI_H__A95038C4_8A72_11D4_A46C_00105A1C588D__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of WinPPI.h
//////////////////////////////////////////////////////////////////////
