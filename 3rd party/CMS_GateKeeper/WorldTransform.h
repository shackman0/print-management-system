//////////////////////////////////////////////////////////////////////
// WorldTransform.h: interface for the CWorldTransform class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_WORLDTRANSFORM_H__A7999BFA_8E7E_11D4_A46C_00105A1C588D__INCLUDED_)
#define AFX_WORLDTRANSFORM_H__A7999BFA_8E7E_11D4_A46C_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// Debug Information
//////////////////////////////////////////////////////////////////////

#define THIS_FILE				_T("WorldTransform.h")

//////////////////////////////////////////////////////////////////////
// Class Definition
//////////////////////////////////////////////////////////////////////

class CWorldTransform  
{
// Attributes
private:
	HDC			m_hDeviceContext;
	bool		m_bPreserved;
	XFORM		m_xfPreserved;

// Construction / Destruction
public:
	CWorldTransform()
	{
		// Initialize Values
		m_hDeviceContext	= NULL;
		m_bPreserved		= false;
		::ZeroMemory( &m_xfPreserved, sizeof(m_xfPreserved) );
	}
	~CWorldTransform()
	{
		if ( m_bPreserved == true )
		{
			Restore();
		}
	}

// Data Access
public:
	void	SetDC( HDC hDC )
	{
		// Restore in case there was a value already saved for the old DC
		if ( m_hDeviceContext != NULL )
		{
			Restore();
		}

		m_hDeviceContext = hDC;

		// Automatically preserve the XFORM
		if ( m_hDeviceContext != NULL )
		{
			// Set the graphics mode to advanced
			SetGraphicsMode( GM_ADVANCED );
			Preserve();
		}
	}

// Operations
public:
	void	SetTransform( const LPXFORM lpXform )
	{
		// Check assumptions
		_ASSERTE( m_hDeviceContext != NULL );
		_ASSERTE( lpXform != NULL );
		THROW_WIN_FAIL( ::SetWorldTransform(m_hDeviceContext, lpXform) );
	}

	void	ModifyTransform( const LPXFORM lpXform, const DWORD& dwMode )
	{
		// Check assumptions
		_ASSERTE( m_hDeviceContext != NULL );
		_ASSERTE( (lpXform != NULL) || (dwMode == MWT_IDENTITY) );
		THROW_WIN_FAIL( ::ModifyWorldTransform(m_hDeviceContext, lpXform, dwMode) );
	}

	void	SetMatrix( const LPXFORM lpRotateXform, const LPXFORM lpTransXform, const DWORD& dwMode )
	{
		SetTransform( lpRotateXform );
		ModifyTransform( lpTransXform, dwMode );
	}

	void	Preserve()
	{
		// Check assumptions
		_ASSERTE( m_hDeviceContext != NULL );
		_ASSERTE( m_bPreserved == false );

		THROW_WIN_FAIL( ::GetWorldTransform(m_hDeviceContext, &m_xfPreserved) );
		m_bPreserved = true;
	}

	void	Restore()
	{
		if ( m_bPreserved == true )
		{
			// Check assumptions
			_ASSERTE( m_hDeviceContext != NULL );

			m_bPreserved = false;
			SetTransform( &m_xfPreserved );
			::ZeroMemory( &m_xfPreserved, sizeof(m_xfPreserved) );
		}
	}

	void	SetGraphicsMode( int nMode )
	{
		// Check assumptions
		_ASSERTE( m_hDeviceContext != NULL );

		THROW_WIN_FAIL( ::SetGraphicsMode(m_hDeviceContext, nMode) );
	}
};

//////////////////////////////////////////////////////////////////////
// Remove Debug Information
//////////////////////////////////////////////////////////////////////

#undef THIS_FILE

//////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_WORLDTRANSFORM_H__A7999BFA_8E7E_11D4_A46C_00105A1C588D__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of WorldTransform.h
//////////////////////////////////////////////////////////////////////
