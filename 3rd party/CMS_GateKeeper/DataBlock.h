///////////////////////////////////////////////////////////////////////////////////////////////////////
// DataBlock.h: interface for the CDataBlock class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DATABLOCK_H__0C38424C_7B9D_11D4_A469_00105A1C588D__INCLUDED_)
#define AFX_DATABLOCK_H__0C38424C_7B9D_11D4_A469_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CDataBlock Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CDataBlock
{
// Attributes
protected:
	LPBYTE		m_pData;
	DWORD		m_dwSize;

// Construction / Destruction
public:
	CDataBlock()
	{
		m_dwSize	= 0;
		m_pData		= NULL;
	}
	virtual ~CDataBlock()							{ };

// Data Access
public:
	LPBYTE			GetData()						{ return m_pData; };
	const LPBYTE	GetData() const					{ return m_pData; };
	const DWORD&	GetSize() const					{ return m_dwSize; };

// Pure Virtual Functions
public:
	virtual bool	SetSize( const DWORD& dwSize )	= 0;
	virtual void	Clear()							= 0;

// Operations
public:
	void Zero()
	{
		if ( m_pData != NULL )
		{
			_ASSERTE( m_dwSize > 0 );
			::ZeroMemory( m_pData, m_dwSize );
		}
	}

// Operators
public:
	operator LPBYTE()								{ return GetData(); };
	operator LPVOID()								{ return GetData(); };
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Template Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template <class ALLOCATOR>
class CDataBlockTemplate : public CDataBlock, protected ALLOCATOR
{
// Destruction
public:
	virtual ~CDataBlockTemplate()							{ Clear(); };

// Pure Virtual Functions
protected:
	virtual LPVOID	Alloc( const DWORD& dwSize )			{ return ALLOCATOR::Alloc(dwSize);	};
	virtual void	Free( LPVOID lpData )					{ ALLOCATOR::Free(lpData);			};

// Operations
public:
	bool	SetSize( const DWORD& dwSize )
	{
		bool bReturnMe = false;

		// Only allocate if the new size is bigger
		// than the old size to avoid fragmentation
		if ( GetSize() < dwSize )
		{
			//DEBUG_TEXT( _T("Reallocating %d -> %d\n"), m_dwSize, dwSize );
			Clear();
			m_pData = reinterpret_cast<LPBYTE>(Alloc(dwSize));
			if ( m_pData != NULL )
			{
				bReturnMe = true;
				m_dwSize = dwSize;
			}
		}
		else
		{
			bReturnMe = true;
		}

		return bReturnMe;
	}

	void	Clear()
	{
		if ( m_pData != NULL )
		{
			_ASSERTE( m_dwSize > 0 );
			Free(m_pData);
			m_pData	= NULL;
			m_dwSize = 0;
		}
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////

typedef CDataBlockTemplate<CStandardAllocation>		CStdDataBlock;

// Include if using COM in this app
#ifdef __ATLBASE_H__
	typedef CDataBlockTemplate<CCOMAllocation>		CComDataBlock;
#endif // #ifdef __ATLBASE_H__

///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_DATABLOCK_H__0C38424C_7B9D_11D4_A469_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of DataBlock.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
