//////////////////////////////////////////////////////////////////////
// PrintProcessor.cpp : Defines the entry point for the DLL application.
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PrintProcessor.h"
#include "Win9xHack.h"
#include "Utils.h"
#include "RawJobHandler.h"
#include "EMFJobHandler.h"
#include "TextJobHandler.h"
#include "PrintProcessorEvents.h"
#include "PrinterAPI.h"

#include <ostream.h>                                 // fws
#include "..\GateKeeper\Cpp\GateKeeper.h"            // fws

//////////////////////////////////////////////////////////////////////
// Debug Information
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

//////////////////////////////////////////////////////////////////////
// Global Values
//////////////////////////////////////////////////////////////////////

LPTSTR	Datatypes[]=
{
	_T("RAW"),
	_T("RAW [FF appended]"),
	_T("RAW [FF auto]"),
	_T("NT EMF 1.003"),
    _T("NT EMF 1.006"),
    _T("NT EMF 1.007"),
    _T("NT EMF 1.008"),
	_T("TEXT"),
	_T("PSCRIPT1"),
	0
};

//////////////////////////////////////////////////////////////////////
// Definitions
//////////////////////////////////////////////////////////////////////

#define BASE_TAB_SIZE				8
#define PARAM_KEY_TABS				_T("TABS")
#define PARAM_KEY_COPIES			_T("COPIES")

#define NUMBER_COPIES_MAX			0xFFFFFFFF												// Largest 4 byte value
#define NUMBER_COPIES_OTHER			0x00000001
#define NUP_OPTIONS_EMF				(BIT(1) | BIT(2) | BIT(4) | BIT(6) | BIT(9) | BIT(16))	// For 1,2,4,6,9,16 up options
#define NUP_OPTIONS_OTHER			(BIT(1))

//////////////////////////////////////////////////////////////////////
// Entry point for the DLL
//////////////////////////////////////////////////////////////////////

BOOL APIENTRY DllMain( HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved )
{
	bool rc;                                     // fws
    STRING_UTF8 jvmDLL_utf8;                     // fws

	switch (ul_reason_for_call)
	{
		case DLL_PROCESS_ATTACH:
			//DEBUG_TEXT( _T("DLL_PROCESS_ATTACH\n") );
// begin fws changes
			jvmDLL_utf8 = "jvm.dll";         // should be read in from the registry
            rc = GateKeeper::Init ( jvmDLL_utf8 );
            if (rc == false)
			{
              cerr << "Failed to initialize JVM \"" || jvmDLL_utf8 || "\" properly.\n";
              exit(1);
			}
// end fws changes  
			break;
		case DLL_THREAD_ATTACH:
			//DEBUG_TEXT( _T("DLL_THREAD_ATTACH\n") );
			break;
		case DLL_THREAD_DETACH:
			//DEBUG_TEXT( _T("DLL_THREAD_DETACH\n") );
			break;
		case DLL_PROCESS_DETACH:
			//DEBUG_TEXT( _T("DLL_PROCESS_DETACH\n") );
              GateKeeper::Done();                  // fws
			break;
	}
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// ClosePrintProcessor
//////////////////////////////////////////////////////////////////////

BOOL WINAPI ClosePrintProcessor( HANDLE hPrintProcessor )
{
	LPPRINTPROCESSORDATA pData;

	// Make sure the handle is valid and pick up the Print Processors data area.
	if ( (pData = ValidateHandle(hPrintProcessor)) == NULL )
	{
		// Set the error code
		::SetLastError( ERROR_INVALID_PARAMETER );
		DEBUG_TEXT( _T("Invalid Handle on Closing Print Processor!\n") );
		return FALSE;
	}

	// Clear the signature so the block can no longer be used
	pData->signature = 0;

	// Close the printer (if open)
	if ( pData->hPrinter != INVALID_HANDLE_VALUE )
	{
		DEBUG_FAIL( ::ClosePrinter(pData->hPrinter) );
	}

	// Destroy the Device Context (if applicable)
	if ( pData->hDC != NULL )
	{
		DEBUG_FAIL( ::DeleteDC(pData->hDC) );
	}

	// Close the event for pausing
	_ASSERTE( pData->semPaused != INVALID_HANDLE_VALUE );
	DEBUG_FAIL( ::CloseHandle(pData->semPaused) );

	// Free all the memory pointed to by the members of pData
	CSpoolMemory::Free(pData->pDevMode);
	CSpoolMemory::FreeString(pData->pPrinterName);
	CSpoolMemory::FreeString(pData->pDatatype);
	CSpoolMemory::FreeString(pData->pDocument);
	CSpoolMemory::FreeString(pData->pOutputFile);
	CSpoolMemory::FreeString(pData->pParameters);

	// Free pData
	CSpoolMemory::Free(pData);

	// Return Success
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// ControlPrintProcessor
//////////////////////////////////////////////////////////////////////

BOOL WINAPI ControlPrintProcessor( HANDLE hPrintProcessor, DWORD Command )
{
	LPPRINTPROCESSORDATA	pData;
	BOOL					bReturnMe = FALSE;

	// Make sure the handle is valid and pick up
	// the Print Processors data area.
	if ( pData = ValidateHandle(hPrintProcessor) )
	{
		switch( Command )
		{
		case JOB_CONTROL_PAUSE:
			DEBUG_TEXT( _T("Pausing Print Job...\n") );
			::ResetEvent( pData->semPaused );
			pData->fsStatus |= PRINTPROCESSOR_PAUSED;
			bReturnMe = TRUE;
			break;

		case JOB_CONTROL_CANCEL:
			pData->fsStatus |= PRINTPROCESSOR_ABORTED;

			// Cancel the hDC if applicable
			if ( pData->hDC != NULL )
			{
				DEBUG_FAIL( ::CancelDC(pData->hDC) );
			}

			// fall through to release job if paused
		case JOB_CONTROL_RESUME:
			if ( pData->fsStatus & PRINTPROCESSOR_PAUSED )
			{
				::SetEvent( pData->semPaused );
				pData->fsStatus &= ~PRINTPROCESSOR_PAUSED;
			}
			bReturnMe = TRUE;
			break;

		default:
			DEBUG_TEXT( _T("Unknown Command to ControlPrintProcessor: 0x%08x!\n"), Command );
			break;
		}
	}
	else
	{
		DEBUG_TEXT( _T("Invalid Data Handle to ControlPrintProcessor!\n") );
		// Set the error code
		::SetLastError( ERROR_INVALID_PARAMETER );
	}

	return bReturnMe;
}
  
//////////////////////////////////////////////////////////////////////
// EnumPrintProcessorDatatypes
//////////////////////////////////////////////////////////////////////

BOOL WINAPI EnumPrintProcessorDatatypes( LPTSTR pName, LPTSTR pPrintProcessorName, DWORD Level, LPBYTE pDatatypes, DWORD cbBuf, LPDWORD pcbNeeded, LPDWORD pcReturned )
{
	DATATYPES_INFO_1*	pInfo1			= (DATATYPES_INFO_1*)pDatatypes;
	LPTSTR*				pMyDatatypes	= Datatypes;
	DWORD				cbTotal			= 0;
	LPBYTE				pEnd;

	// Start assuming failed / no entries returned
	*pcReturned = 0;

	// Pick up pointer to end of buffer given
	pEnd = (LPBYTE)pInfo1 + cbBuf;

	// Add up the minimum buffer required
	while (*pMyDatatypes)
	{
		cbTotal += _tcsclen(*pMyDatatypes) * sizeof(TCHAR) + sizeof(TCHAR) + sizeof(DATATYPES_INFO_1);
		pMyDatatypes++;
	}

	// Set the buffer length returned/required
	*pcbNeeded = cbTotal;

	// Fill in the array only if there is sufficient space to
	if ( cbTotal <= cbBuf )
	{
		// Pick up our list of supported data types
		pMyDatatypes = Datatypes;

		// Fill in the given buffer.  We put the data names at the end of
		// the buffer, working towards the front. The structures are put
		// at the front, working towards the end.
		while (*pMyDatatypes)
		{
			pEnd -= _tcsclen(*pMyDatatypes)*sizeof(TCHAR) + sizeof(TCHAR);
			_tcscpy((LPTSTR)pEnd, *pMyDatatypes);
			pInfo1->pName = (LPTSTR)pEnd;
			pInfo1++;
			(*pcReturned)++;
			pMyDatatypes++;
		}
	}
	else
	{
		// Caller didn't have large enough buffer, set error and return
		DEBUG_TEXT( _T("Insufficient buffer calling EnumPrintProcessorDatatypes.  Had %d, Needed %d\n"), cbBuf, cbTotal );
		::SetLastError( ERROR_INSUFFICIENT_BUFFER );
		return FALSE;
	}

	// Return success
	return TRUE;
}

//////////////////////////////////////////////////////////////////////
// OpenPrintProcessor
//////////////////////////////////////////////////////////////////////

HANDLE WINAPI OpenPrintProcessor( LPTSTR pPrinterName, LPPRINTPROCESSOROPENDATA pPrintProcessorOpenData )
{
	LPPRINTPROCESSORDATA	pData			= NULL;
	LPTSTR*					pMyDatatypes	= Datatypes;
	DWORD					uDatatype		= 0;
	HDC 					hDC				= NULL;
	HANDLE					hPrinter		= INVALID_HANDLE_VALUE;
	LPDEVMODE				pDevMode		= NULL;

	// If the caller passed a NULL for the open data, fail the call
	if ( !pPrintProcessorOpenData || !pPrintProcessorOpenData->pDatatype || !*pPrintProcessorOpenData->pDatatype )
	{
		DEBUG_TEXT( _T("Invalid OpenPrintProcessor Parameters!\n") );
		ReportError( EVT_MSG_OPP_INVALID_PARAM );
		::SetLastError( ERROR_INVALID_PARAMETER );
		return NULL;
	}

	// Display information in debug mode
	DEBUG_PROCESSOR_OPEN( pPrintProcessorOpenData );

	// Search for the data type index we are opening for
	while ( (*pMyDatatypes) && (uDatatype < PRINTPROCESSOR_TYPE_MAX_VALUE) )
	{
		if ( !_tcsicmp(*pMyDatatypes, pPrintProcessorOpenData->pDatatype) )
		{
			break;
		}
		pMyDatatypes++;
		uDatatype++;
	}
	
	// Check to see if the datatype is valid
	if ( uDatatype >= PRINTPROCESSOR_TYPE_MAX_VALUE )
	{
		DEBUG_TEXT( _T("Invalid Data Type: %s\n"), pPrintProcessorOpenData->pDatatype );
		ReportError( EVT_MSG_OPP_INVALID_DATATYPE );
		::SetLastError( ERROR_INVALID_DATATYPE );
		return NULL;
	}

	// Create the DC
	if	( (uDatatype == PRINTPROCESSOR_TYPE_TEXT) || (uDatatype == PRINTPROCESSOR_TYPE_RAW) )
	{
		hDC = ::CreateDC( _T(""), pPrinterName, _T(""), pPrintProcessorOpenData->pDevMode );
		if ( hDC == NULL )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("Failed to create DC!\n") );
			return NULL;
		}
	}

	// Copy the DevMode stuff when present
	if ( pPrintProcessorOpenData->pDevMode != NULL )
	{
		DWORD dwSize;
		
		dwSize = pPrintProcessorOpenData->pDevMode->dmSize + pPrintProcessorOpenData->pDevMode->dmDriverExtra;
		pDevMode = (LPDEVMODE)CSpoolMemory::Alloc(dwSize);

		// Memory allocation failed, so return NULL
		if ( pDevMode == NULL )
		{
			DEBUG_TEXT( _T("Failed to allocate memory for DEVMODE structure in OpenPrintProcessor\n") );
			return NULL;
		}

		// Do the copy
		::CopyMemory( pDevMode, pPrintProcessorOpenData->pDevMode, dwSize );
	}

	// Open the target printer
	if ( ::OpenPrinter(pPrinterName, &hPrinter, NULL) == FALSE )
	{
		// Display an error in the debug version
		DEBUG_LAST_ERROR();

		// Report the Error
		ReportError( EVT_MSG_OPP_PRINTER_OPEN_FAIL, ::GetLastError() );
		DEBUG_TEXT( _T("Couldn't open printer in OpenPrintProcessor!\n") );

		// If we allocated memory for pDevMode, it needs to be freed
		CSpoolMemory::Free(pDevMode);

		return NULL;
	}

	// Allocate a buffer for the print processor data to return
	pData = static_cast<LPPRINTPROCESSORDATA>(CSpoolMemory::Alloc( sizeof(PRINTPROCESSORDATA) ));

	// Was the allocation successful?
	if ( pData != NULL )
	{
		// Initialize the memory
		::ZeroMemory( pData, sizeof(PRINTPROCESSORDATA) );
	}
	else
	{
		// Memory allocation failed, so clean up and return NULL
		// If we allocated memory for pDevMode, it needs to be freed
		CSpoolMemory::Free(pDevMode);

		// Close the printer
		DEBUG_FAIL( ::ClosePrinter(hPrinter) );

		return NULL;
	}

	// Fill in the print processor information
	pData->signature	= PRINTPROCESSORDATA_SIGNATURE;
	pData->cb			= sizeof(PRINTPROCESSORDATA);
	pData->pNext		= NULL;
	pData->fsStatus		= 0;
	pData->semPaused	= ::CreateEvent(NULL, FALSE, TRUE, NULL);
	pData->uDatatype	= uDatatype;
	pData->hPrinter		= hPrinter;

	// Allocate and fill in the processors strings
	pData->pPrinterName	= CSpoolMemory::AllocString( pPrinterName );
	pData->pDocument	= CSpoolMemory::AllocString( pPrintProcessorOpenData->pDocumentName );
	pData->pOutputFile	= CSpoolMemory::AllocString( pPrintProcessorOpenData->pOutputFile );
	pData->pDatatype	= CSpoolMemory::AllocString( pPrintProcessorOpenData->pDatatype );
	pData->pParameters	= CSpoolMemory::AllocString( pPrintProcessorOpenData->pParameters );
	pData->lpszPrinterNameFromOpenData = CSpoolMemory::AllocString(pPrintProcessorOpenData->pPrinterName);

	// Fill in the rest of the print processor information
	pData->JobId		= pPrintProcessorOpenData->JobId;
	pData->hDC			= hDC;
	pData->Copies		= 1;
	pData->TabSize		= BASE_TAB_SIZE;
	pData->pDevMode		= pDevMode;

	// Display the parameter string
	DEBUG_TEXT( _T("Print Job Params: %s\n"), pData->pParameters );

	// Parse the parameters string
	if (pData->pParameters)
	{
		ULONG	nValue;
		USHORT	nLength = sizeof(ULONG);

		// 	Look to see if there is a COPIES=n key/value in the
		//	Parameters field of this job.  This tells us the number
		//	of times to play the data.
		GetKeyValue(pData->pParameters,	PARAM_KEY_COPIES, VALUE_ULONG, &nLength, &nValue);
		if (nLength == sizeof(ULONG))
		{
			DEBUG_TEXT( _T("Set Copies from parameters: %d\n"), nValue );
			pData->Copies = nValue;
		}

		// Make sure the value for nLength is correct
		nLength = sizeof(ULONG);

		GetKeyValue(pData->pParameters,	PARAM_KEY_TABS, VALUE_ULONG, &nLength, &nValue);
		if ((nLength == sizeof(ULONG)) && nValue)
		{
			pData->TabSize = nValue;
		}
	}

	//	If we are doing copies, we need to check to see if
	//	this is a direct or spooled job.  If it is direct, then
	//	we can't do copies because we can't rewind the data stream.
	if ( pData->Copies > 1 )
	{
		CProcBuffer	data;

		// Load the values
		CPrinterAPI::GetPrinterData( hPrinter, 2, data );
		PPRINTER_INFO_2 pPrinterInfo2 = reinterpret_cast<PPRINTER_INFO_2>(data.GetData());

		if ( pPrinterInfo2 != NULL )
		{
			// If printing directly to the printer, we can't print copies
			if ( pPrinterInfo2->Attributes & PRINTER_ATTRIBUTE_DIRECT )
			{
				DEBUG_TEXT( _T("Printing directly to the printer, we can't print copies - falling back to 1 copy\n") );
				pData->Copies = 1;
			}
		}
		else
		{
			// If we couldn't get the info, be safe and don't do copies
			DEBUG_TEXT( _T("GetPrinter failed - falling back to 1 copy\n") );
			pData->Copies = 1;
		}
	}

	return (HANDLE)pData;
}
 
//////////////////////////////////////////////////////////////////////
// PrintDocumentOnPrintProcessor
//////////////////////////////////////////////////////////////////////

BOOL DoPrinting( HANDLE hPrintProcessor, LPTSTR pDocumentName )
{
	BOOL					bReturnMe = TRUE;
	LPPRINTPROCESSORDATA	pData;

	DEBUG_TEXT( _T("Inside of DoPrinting with Data @ 0x%08x\n"), hPrintProcessor );
	// Make sure the handle is valid and pick up the Print Processors data area.
	if ( (pData = ValidateHandle(hPrintProcessor)) != NULL )
	{
		DEBUG_PROCESSOR( pData );

		try
		{
			// Print the job based on its data type.
			switch (pData->uDatatype)
			{
			case PRINTPROCESSOR_TYPE_RAW:
			case PRINTPROCESSOR_TYPE_RAW_FF:
			case PRINTPROCESSOR_TYPE_RAW_FF_AUTO:
			case PRINTPROCESSOR_TYPE_PSCRIPT1:
				{
					CRawJobHandler	jobHandler;

					DEBUG_TEXT( _T("Using RawJobHandler...\n") );
					bReturnMe = jobHandler.HandlePrintJob( pDocumentName, pData );
				}
				break;

			case PRINTPROCESSOR_TYPE_EMF_40:
			case PRINTPROCESSOR_TYPE_EMF_50_1:
			case PRINTPROCESSOR_TYPE_EMF_50_2:
			case PRINTPROCESSOR_TYPE_EMF_50_3:
				{
					CEMFJobHandler	jobHandler;

					DEBUG_TEXT( _T("Using EMFJobHandler...\n") );
					bReturnMe = jobHandler.HandlePrintJob( pDocumentName, pData );
				}
				break;

			case PRINTPROCESSOR_TYPE_TEXT:
				{
					CTextJobHandler	jobHandler;

					DEBUG_TEXT( _T("Using TextJobHandler...\n") );
					bReturnMe = jobHandler.HandlePrintJob( pDocumentName, pData );
				}
				break;

			default:
				ReportError( EVT_MSG_PPP_INVALID_DATATYPE );
				DEBUG_TEXT( _T("Invalid Data Type\n") );
				::SetLastError( ERROR_INVALID_DATATYPE );
				break;
			}
		}
		catch ( const _com_error& e )
		{
			// Report an error based on the error code
			switch ( e.Error() )
			{
			// Out of memory error
			case E_OUTOFMEMORY:
				ReportError( EVT_MSG_SERVER_FAIL_OUTOFMEMORY );
				break;

			// Access Denied error
			case E_ACCESSDENIED:
				ReportError( EVT_MSG_SERVER_FAIL_ACCESSDENIED, e.Error(), e.ErrorMessage() );
				break;

			// If no special error message has been defined, use a generic one.
			default:
				ReportError( EVT_MSG_SERVER_FAIL_COM, e.Error(), e.ErrorMessage() );
				break;
			}

			// Display debugging information
			DEBUG_COM_EXCEPTION(e);
		}
	}
	else
	{
		DEBUG_TEXT( _T("ValidateHandle Failed!\n") );
		ReportError( EVT_MSG_PPP_INVALID_PARAM );
		// Set the error code
		::SetLastError( ERROR_INVALID_PARAMETER );
		bReturnMe = FALSE;
	}
	return bReturnMe;
}

BOOL WINAPI PrintDocumentOnPrintProcessor( HANDLE hPrintProcessor, LPTSTR pDocumentName )
{
	DWORD	dwOldErrorMode	= ::SetErrorMode( SEM_FAILCRITICALERRORS | SEM_NOGPFAULTERRORBOX | SEM_NOOPENFILEERRORBOX ); 
	BOOL	bReturnMe		= TRUE;

	__try
	{
		bReturnMe = DoPrinting(hPrintProcessor, pDocumentName);
	}
	__except ( EXCEPTION_EXECUTE_HANDLER )
	{
		ReportException( EVT_MSG_CAUGHT_EXCEPTION, ::GetExceptionCode() );
		DEBUG_TEXT( _T("*** Handled Exception: 0x%08x\n"), ::GetExceptionCode() );
		bReturnMe = TRUE;

		__try
		{
			LPPRINTPROCESSORDATA	pData;

			// Make sure the handle is valid and pick up the Print Processors data area.
			if ( (pData = ValidateHandle(hPrintProcessor)) != NULL )
			{
				// Printing failed, so delete the print job from the queue
				DEBUG_FAIL( ::SetJob(pData->hPrinter, pData->JobId, 0, NULL, JOB_CONTROL_CANCEL) );
			}
			else
			{
				ReportError( EVT_MSG_PPP_INVALID_PARAM );
				// Set the error code
				::SetLastError( ERROR_INVALID_PARAMETER );
				bReturnMe = FALSE;
			}
		}
		__except ( EXCEPTION_EXECUTE_HANDLER )
		{
			ReportException( EVT_MSG_CAUGHT_CANCELPRINT_EXCEPTION, ::GetExceptionCode() );
			DEBUG_TEXT( _T("*** Handled exception while cancelling job: 0x%08x\n"), ::GetExceptionCode() );
			bReturnMe = TRUE;
		}
	}

	// Restore the original error mode
	::SetErrorMode( dwOldErrorMode );

	DEBUG_TEXT( _T("Returning %s\n"), bReturnMe == TRUE ? _T("True") : _T("False") );

	// Return success or failure
	return bReturnMe;
}

//////////////////////////////////////////////////////////////////////
// GetPrintProcessorCapabilities
//////////////////////////////////////////////////////////////////////

DWORD WINAPI GetPrintProcessorCapabilities( LPTSTR pValueName, DWORD dwAttributes, LPBYTE pData, DWORD dwSize, LPDWORD pcbNeeded )
// Function Description:
//		GetPrintProcessorCapabilities returns information about the
//		options supported by the print processor for the given datatype
//		in a PRINTPROCESSOR_CAPS_1 struct.
//
// Parameters:
//		pValueName	 -- datatype like RAW|NT EMF 1.006|TEXT|...
//		dwAttributes -- printer attributes
//		pData		 -- pointer to the buffer
//		nSize		 -- size of the buffer
//		pcbNeeded	 -- pointer to the variable to store the required buffer size
//
// Return Values:
//		Error Codes.
{
	LPTSTR*					pDatatypes	= Datatypes;
	DWORD					dwDatatype	= 0;
	DWORD					dwReturnMe;
	PPRINTPROCESSOR_CAPS_1	ppcInfo;

	*pcbNeeded = sizeof(PRINTPROCESSOR_CAPS_1);

	// Check for valid parameters.
	if ( (pValueName != NULL) && (pData != NULL) )
	{
		// Check for sufficient buffer.
		if ( *pcbNeeded >= dwSize  )
		{
			// Loop to find the index of the datatype.
			while( (*pDatatypes) && ( _tcsicmp(*pDatatypes, pValueName) != 0) )
			{
			   pDatatypes++;
			   dwDatatype++;
			}

			ppcInfo = (PPRINTPROCESSOR_CAPS_1) pData;

			// Level is 1 for PRINTPROCESSOR_CAPS_1.
			ppcInfo->dwLevel = 1;

			// If we get this far, assume success
			dwReturnMe = ERROR_SUCCESS;

			switch (dwDatatype)
			{
			case PRINTPROCESSOR_TYPE_RAW:
			case PRINTPROCESSOR_TYPE_RAW_FF:
			case PRINTPROCESSOR_TYPE_RAW_FF_AUTO:
			case PRINTPROCESSOR_TYPE_EMF_40:
			case PRINTPROCESSOR_TYPE_TEXT:
			case PRINTPROCESSOR_TYPE_PSCRIPT1:
				ppcInfo->dwNupOptions		= NUP_OPTIONS_OTHER;
				ppcInfo->dwNumberOfCopies	= NUMBER_COPIES_MAX;
				ppcInfo->dwPageOrderFlags	= NORMAL_PRINT;
				break;

			case PRINTPROCESSOR_TYPE_EMF_50_1:
			case PRINTPROCESSOR_TYPE_EMF_50_2:
			case PRINTPROCESSOR_TYPE_EMF_50_3:
				// For direct printing, masq. printers and print RAW only,
				// EMF is not spooled. Dont expose EMF features in the UI.
				if ( (dwAttributes & PRINTER_ATTRIBUTE_DIRECT) ||
				  (dwAttributes & PRINTER_ATTRIBUTE_RAW_ONLY) ||
				  ((dwAttributes & PRINTER_ATTRIBUTE_LOCAL) && (dwAttributes & PRINTER_ATTRIBUTE_NETWORK)) )
				{
					ppcInfo->dwNupOptions		= NUP_OPTIONS_OTHER;
					ppcInfo->dwNumberOfCopies	= NUMBER_COPIES_OTHER;
					ppcInfo->dwPageOrderFlags	= NORMAL_PRINT;
				}
				else
				{
					ppcInfo->dwNupOptions		= NUP_OPTIONS_EMF;
					ppcInfo->dwNumberOfCopies	= NUMBER_COPIES_MAX;
					ppcInfo->dwPageOrderFlags	= REVERSE_PRINT | BOOKLET_PRINT;
				}
				break;

			default:
				// Should not happen since the spooler must check if the datatype is
				// supported before calling this print processor.
				DEBUG_TEXT( _T("Invalid Datatype inside of GetPrintProcessorCapabilities, after it was already checked!\n") );
				dwReturnMe = ERROR_INVALID_DATATYPE;
				break;
			}
		}
		else
		{
			DEBUG_TEXT( _T("Buffer not big enough to hold PRINTPROCESSOR_CAPS_1\n") );
			dwReturnMe = ERROR_MORE_DATA;
		}
	}
	else
	{
		DEBUG_TEXT( _T("Invalid Datatype inside of GetPrintProcessorCapabilities\n") );
		dwReturnMe = ERROR_INVALID_PARAMETER;
	}

	return dwReturnMe;
}

//////////////////////////////////////////////////////////////////////
// Debugging Code
//////////////////////////////////////////////////////////////////////

#ifdef DISPLAY_STRUCTS

void DebugProcessorData( LPPRINTPROCESSORDATA pData )
{
	DEBUG_TEXT(	_T("Print Processor Data...\n")
					_T("    Signature:        %d\n")
					_T("    CB:               %d\n")
					_T("    Status:           %d\n")
					_T("    DataType:         %d\n")
					_T("    Printer:          %s\n")
					_T("    Document:         %s\n")
					_T("    Output:           %s\n")
					_T("    DataType:         %s\n")
					_T("    Parameters:       %s\n")
					_T("    Job ID:           %d\n")
					_T("    Copies:           %d\n")
					_T("    Tab Size:         %d\n")
					_T("    DevMode:          %sPresent\n")
					_T("    Printer Name:     %s\n")
			, pData->signature
			, pData->cb
			, pData->fsStatus
			, pData->uDatatype
			, pData->pPrinterName
			, pData->pDocument
			, pData->pOutputFile
			, pData->pDatatype
			, pData->pParameters
			, pData->JobId
			, pData->Copies
			, pData->TabSize
			, pData->pDevMode ? _T("") : _T("NOT ")
			, pData->lpszPrinterNameFromOpenData
		);

	if ( pData->pDevMode )
	{
		DEBUG_DEVMODE( _T("ProcessorData"), pData->pDevMode );
	}
}

void DebugProcessorOpenData( LPPRINTPROCESSOROPENDATA pData )
{
	DEBUG_TEXT( _T("Print Processor Open Data...\n")
				_T("    Job ID:               %d\n")
				_T("    DataType:             %s\n")
				_T("    Document:             %s\n")
				_T("    Output:               %s\n")
				_T("    Parameters:           %s\n")
				_T("    Printer:              %s\n")
				_T("    DevMode:              %sPresent\n")
			, pData->JobId
			, pData->pDatatype
			, pData->pDocumentName
			, pData->pOutputFile
			, pData->pParameters
			, pData->pPrinterName
			, pData->pDevMode ? _T("") : _T("NOT ")
		);

	if ( pData->pDevMode )
	{
		DEBUG_DEVMODE( _T("ProcessorOpenData"), pData->pDevMode );
	}
}

#endif  // #ifdef DISPLAY_STRUCTS

//////////////////////////////////////////////////////////////////////
// End of PrintProcessor.cpp
//////////////////////////////////////////////////////////////////////