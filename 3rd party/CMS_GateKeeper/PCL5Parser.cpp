///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL5Parser.cpp: implementation of the CPCL5Parser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL5Parser.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Static Variables
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool							CPCL5Parser::m_bMapInitialized	= false;
CPCL5Parser::CPCL5CommandMap	CPCL5Parser::m_mapCommands;
const CPCL5Parser::PCL5Command	CPCL5Parser::m_pCommandList[]	=
{
	// <ESC>&l#X -> # = Number of copies
	{ HASH_PCL5('&', 'l', 'X'), &CPCL5Parser::ProcessCopies			},

	// <ESC>&l#S -> # = A constant that tells about duplexing
	{ HASH_PCL5('&', 'l', 'S'), &CPCL5Parser::ProcessDuplexing		},

	// <ESC>&l#A -> # = A constant that tells about the page size
	{ HASH_PCL5('&', 'l', 'A'), &CPCL5Parser::ProcessPageSize		},

	// <ESC>&l#O -> # = A constant that tells about the orientation
	{ HASH_PCL5('&', 'l', 'O'), &CPCL5Parser::ProcessOrientation	},

	// <ESC>*t#R -> # = Resolution (in DPI)
	{ HASH_PCL5('*', 't', 'R'), &CPCL5Parser::ProcessResolution		},

	// Move cursor commands
	{ HASH_PCL5('*', 'p', 'X'), &CPCL5Parser::ProcessMoveCursor		},
	{ HASH_PCL5('*', 'p', 'Y'), &CPCL5Parser::ProcessMoveCursor		},

	// Color Commands
	{ HASH_PCL5('*', 'r', 'U'), &CPCL5Parser::ProcessColor			},
	{ HASH_PCL5('*', 'v', 'W'), &CPCL5Parser::ProcessColorPalette	},

	// Printer Reset
	{ HASH_PCL5('E', 000, 000), &CPCL5Parser::ProcessReset			},

	// All of these are in the form of:
	//	<ESC><header>#<terminator>[Data] where # is the size of the data
	{ HASH_PCL5('&', 'p', 'X'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('(', 'f', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5(')', 's', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('(', 's', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('*', 'b', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('*', 'b', 'V'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('*', 'c', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('*', 'l', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('&', 'b', 'W'), &CPCL5Parser::ProcessSkipData		},
	{ HASH_PCL5('&', 'n', 'W'), &CPCL5Parser::ProcessSkipData		},

	// Terminate the list
	{ HASH_PCL5(000, 000, 000), NULL }
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Parse Operation
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL5Parser::Parse( CPrintParserData* pParserData )
{
	bool	bParsed		= false;	// Assume failure

	// Check Assumptions
	_ASSERTE( pParserData != NULL );
	_ASSERTE( m_pParserData == NULL );

	// Set the values
	m_bMismatchedCopies	= false;
	m_bNewPage			= false;
	m_pParserData		= pParserData;
	m_dwPageCount		= m_pParserData->GetPageCount();
	m_nCopies			= m_pParserData->GetCopies();

	// Verify the job is actually PCL5
	if ( VerifyPCL5() )
	{
		// Mark as parsed
		bParsed	= true;

		// Initialize the command map
		InitializeCommandMap();

		// Do the parsing
		try
		{
			CPCL5DataType		pcl5Data;
			PCL5Operator		fpOperator;

			// Until the end of the buffer is reached
			while ( m_pParserData->GetBytesRemaining() )
			{
				pcl5Data.ReadFromBuffer( m_pParserData );
				if ( m_mapCommands.Lookup(pcl5Data.GetHashValue(), fpOperator) )
				{
					(this->*(fpOperator))(&pcl5Data);
				}

				// Take care of any extra data
				if ( !pcl5Data.DoContinue() )
				{
					SkipText();
				}
			}
		}
		catch( CPrintParserException& e )
		{
#ifdef USE_DEBUG_OUTPUT
			DEBUG_TEXT( _T("PCL5 Parser Caught Exception: %s\n"), e.GetErrorMessage() );
#endif
			m_pParserData->SetErrorInfo( e.GetErrorMessage() );
		}

		// See if the last page was ejected
		if ( m_bNewPage )
		{
			TrackEndPage();
		}

		// Set the values
		if ( m_nCopies )
		{
			// If the copies are mismatched, say there is only one copy
			m_pParserData->SetCopies( (m_bMismatchedCopies ? 1 : m_nCopies) );
		}
		if ( m_dwPageCount )
		{
			m_pParserData->SetPageCount( m_dwPageCount );
		}
	}

	// Clear the values
	m_pParserData	= NULL;
	m_dwPageCount	= 0;
	m_nCopies		= 0;

	// Return Success or Failure
	return bParsed;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL5 Command Processors
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CPCL5Parser::ProcessColor( const CPCL5DataType* pData )
{
	switch ( pData->GetValue() )
	{
	case PCL5_COLOR_SIMPLE_CMY:
	case PCL5_COLOR_SIMPLE_RGB:
		DEBUG_PCL5_TEXT( _T("PCL5: Using color\n") );
		m_pParserData->SetColor( DMCOLOR_COLOR );
		break;
	case PCL5_COLOR_SIMPLE_MONOCHROME:
		DEBUG_PCL5_TEXT( _T("PCL5: Using monochrome\n") );
		m_pParserData->SetDuplex( DMCOLOR_MONOCHROME );
		break;
	default:
		DEBUG_TEXT( _T("Unknown PCL5 color value: %d\n"), pData->GetValue() );
		// Do not throw an exception because PCL5 Definitions unknown values to be ignored
		break;
	}
}

void CPCL5Parser::ProcessColorPalette( const CPCL5DataType* pData )
{
	// If palette information is provided, assume that it is a color print job
	m_pParserData->SetColor( DMCOLOR_COLOR );

	// Skip past the palette information
	ProcessSkipData( pData );
}

void CPCL5Parser::ProcessCopies( const CPCL5DataType* pData )
{
	WORD nCopies	= (WORD)pData->GetValue();

	// Check Assumptions
	_ASSERTE( nCopies != 0 );

	DEBUG_PCL5_TEXT( _T("Number of copies: %d\n"), pData->GetValue() );

	if ( m_bMismatchedCopies == false)
	{
		// If the copies have not yet been set
		if ( m_nCopies != 0 )
		{
			// Copies have been set, and do match the current value
			if ( m_nCopies != nCopies )
			{
				// The copies are now mismatched
				m_bMismatchedCopies = true;

				// Count each copy of a previous page as a seperate page
				m_dwPageCount *= m_nCopies;

				// Display some debug text
				DEBUG_TEXT( _T("Found Mismatched Copy Count!\n") );
			}
		}
	}

	// From now on, use the new number of copies
	m_nCopies = nCopies;
}

void CPCL5Parser::ProcessDuplexing( const CPCL5DataType* pData )
{
	switch ( pData->GetValue() )
	{
	case PCL5_DUPLEX_SIMPLEX:
		m_pParserData->SetDuplex( DMDUP_SIMPLEX );
		break;
	case PCL5_DUPLEX_DUPLEX_LONG:
		m_pParserData->SetDuplex( DMDUP_VERTICAL );
		break;
	case PCL5_DUPLEX_DUPLEX_SHORT:
		m_pParserData->SetDuplex( DMDUP_HORIZONTAL );
		break;
	default:
		DEBUG_TEXT( _T("Unknown PCL5 duplex value: %d\n"), pData->GetValue() );
		// Do not throw an exception because PCL5 Definitions unknown values to be ignored
		break;
	}
}

void CPCL5Parser::ProcessMoveCursor( const CPCL5DataType* pData )
{
	// If the printer is given a cursor command, where the position 
	// is not 0, mark that a page has been started (if it has not already)
	if ( pData->GetValue() != 0 )
	{
		TrackBeginPage();
	}
}

void CPCL5Parser::ProcessOrientation( const CPCL5DataType* pData )
{
	switch ( pData->GetValue() )
	{
	case PCL5_ORIENTATION_PORTRAIT:
	case PCL5_ORIENTATION_REVERSE_PORTRAIT:
		m_pParserData->SetOrientation( DMORIENT_PORTRAIT );
		break;

	case PCL5_ORIENTATION_LANDSCAPE:
	case PCL5_ORIENTATION_REVERSE_LANDSCAPE:
		m_pParserData->SetOrientation( DMORIENT_LANDSCAPE );
		break;
	default:
		DEBUG_TEXT( _T("Unknown PCL5 orientation value: %d\n"), pData->GetValue() );
		// Do not throw an exception because PCL5 Definitions unknown values to be ignored
		break;
	}
}

void CPCL5Parser::ProcessPageSize( const CPCL5DataType* pData )
{
	switch ( pData->GetValue() )
	{
	case PCL5_PAPERSIZE_EXECUTIVE:			m_pParserData->SetPaperSize( DMPAPER_EXECUTIVE );
											break;
	case PCL5_PAPERSIZE_LETTER:				m_pParserData->SetPaperSize( DMPAPER_LETTER );
											break;
	case PCL5_PAPERSIZE_LEGAL:				m_pParserData->SetPaperSize( DMPAPER_LEGAL );
											break;
	case PCL5_PAPERSIZE_LEDGER:				m_pParserData->SetPaperSize( DMPAPER_LEDGER );
											break;
	case PCL5_PAPERSIZE_A3:					m_pParserData->SetPaperSize( DMPAPER_A3 );
											break;
	case PCL5_PAPERSIZE_A4:					m_pParserData->SetPaperSize( DMPAPER_A4 );
											break;
	case PCL5_PAPERSIZE_A5:					m_pParserData->SetPaperSize( DMPAPER_A5 );
											break;
	case PCL5_PAPERSIZE_JIS_B4:				m_pParserData->SetPaperSize( DMPAPER_B4 );
											break;
	case PCL5_PAPERSIZE_JIS_B5:				m_pParserData->SetPaperSize( DMPAPER_B5 );
											break;
	case PCL5_PAPERSIZE_HAGAKI_POSTCARD:	m_pParserData->SetPaperSize( DMPAPER_JAPANESE_POSTCARD );
											break;
	case PCL5_PAPERSIZE_MONARCH:			m_pParserData->SetPaperSize( DMPAPER_ENV_MONARCH );
											break;
	case PCL5_PAPERSIZE_COM10:				m_pParserData->SetPaperSize( DMPAPER_ENV_10 );
											break;
	case PCL5_PAPERSIZE_DL:					m_pParserData->SetPaperSize( DMPAPER_ENV_DL );
											break;
	case PCL5_PAPERSIZE_C5:					m_pParserData->SetPaperSize( DMPAPER_ENV_C5 );
											break;
	case PCL5_PAPERSIZE_B5:					m_pParserData->SetPaperSize( DMPAPER_ENV_B5 );
											break;
	case PCL5_PAPERSIZE_CUSTOM:				DEBUG_TEXT( _T("Using Custom Paper Size!\n") );
		break;
#if(WINVER >= 0x0500)
	case PCL5_PAPERSIZE_A6:					m_pParserData->SetPaperSize( DMPAPER_A6 );
											break;
	case PCL5_PAPERSIZE_JIS_B6:				m_pParserData->SetPaperSize( DMPAPER_B6_JIS );
											break;
	case PCL5_PAPERSIZE_OUFUKU_HAGAKI:		m_pParserData->SetPaperSize( DMPAPER_DBL_JAPANESE_POSTCARD );
											break;
#else
	case PCL5_PAPERSIZE_A6:
	case PCL5_PAPERSIZE_JIS_B6:
	case PCL5_PAPERSIZE_OUFUKU_HAGAKI:
#endif
	default:
		DEBUG_TEXT( _T("Invalid PCL5 Paper Size: %d\n"), pData->GetValue() );
		// Do not throw an exception because PCL5 Definitions unknown values to be ignored
		break;
	}
}

void CPCL5Parser::ProcessReset( const CPCL5DataType* pData )
{
	m_bNewPage = false;
}

void CPCL5Parser::ProcessResolution( const CPCL5DataType* pData )
{
	m_pParserData->SetPrintQuality( (short)(pData->GetValue()) );
}

void CPCL5Parser::ProcessSkipData( const CPCL5DataType* pData )
{
	m_pParserData->SkipAhead( pData->GetValue() );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL5Parser.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
