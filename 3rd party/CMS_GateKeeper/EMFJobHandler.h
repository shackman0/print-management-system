//////////////////////////////////////////////////////////////////////
// EMFJobHandler.h: interface for the CEMFJobHandler class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EMF_H__273BA48D_1D23_11D3_BEEF_00E02931B31A__INCLUDED_)
#define AFX_EMF_H__273BA48D_1D23_11D3_BEEF_00E02931B31A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////

#include "PrintJobHandler.h"
#include "EMFException.h"
#include "CMSList.h"
#include "WorldTransform.h"

//////////////////////////////////////////////////////////////////////
// Debug Information
//////////////////////////////////////////////////////////////////////

#define THIS_FILE				_T("EMFJobHandler.h")

//////////////////////////////////////////////////////////////////////
// Typedefs / Structures Needed
//////////////////////////////////////////////////////////////////////

typedef struct _UpdateRect
{
	double					top;
	double					bottom;
	double					left;
	double					right;
} UpdateRect;

typedef CCMSList<DWORD, DWORD&>		CPageNumberList;

//////////////////////////////////////////////////////////////////////
// Class Definition
//////////////////////////////////////////////////////////////////////

class CEMFJobHandler : public CPrintJobHandler
{
// Attributes
private:
	CWorldTransform				m_Xform;
	HANDLE						m_hSpoolFile;
	HDC							m_hPrinterDC;

	bool						m_bReverseOrderPrinting;
	bool						m_bCollate;
	bool						m_bDuplex;
	bool						m_bBookletPrint;

	DWORD						m_dwNumberOfPagesPerSide;
	DWORD						m_dwTotalNumberOfPages;
	DWORD						m_dwNupBorderFlags;
	DWORD						m_dwJobNumberOfPagesPerSide;
	DWORD						m_dwDrvNumberOfPagesPerSide;
	DWORD						m_dwDuplexMode;
	DWORD						m_dwJobNumberOfCopies;
	DWORD						m_dwDrvNumberOfCopies;
	DWORD						m_dwOptimization;

	DWORD						m_dwPagesPrinted;

	LPDEVMODE					m_pDevMode;
	LPDEVMODE					m_pFirstDM;
	LPDEVMODE					m_pCopyDM;

// Static Members
private:
	static UpdateRect			m_URect21[];
	static UpdateRect			m_URect21R[];
	static UpdateRect			m_URect22[];
	static UpdateRect			m_URect4[];
	static UpdateRect			m_URect61[];
	static UpdateRect			m_URect61R[];
	static UpdateRect			m_URect62[];
	static UpdateRect			m_URect62R[];
	static UpdateRect			m_URect9[];
	static UpdateRect			m_URect16[];

// Construction / Destruction
public:
	CEMFJobHandler( )
	{
		m_hSpoolFile				= INVALID_HANDLE_VALUE;
		m_hPrinterDC				= NULL;

		m_bReverseOrderPrinting		= false;
		m_bCollate					= false;
		m_bDuplex					= false;
		m_bBookletPrint				= false;

		m_dwNumberOfPagesPerSide	= 0;
		m_dwTotalNumberOfPages		= 0;
		m_dwNupBorderFlags			= 0;
		m_dwJobNumberOfPagesPerSide	= 0;
		m_dwDrvNumberOfPagesPerSide	= 0;
		m_dwDuplexMode				= 0;
		m_dwJobNumberOfCopies		= 0;
		m_dwDrvNumberOfCopies		= 0;
		m_dwOptimization			= 0;

		m_dwPagesPrinted			= 0;		
		
		m_pDevMode					= NULL;
		m_pFirstDM					= NULL;
		m_pCopyDM					= NULL;
	}
	virtual ~CEMFJobHandler()
	{
		CSpoolMemory::Free( m_pDevMode );

		if ( m_hSpoolFile != INVALID_HANDLE_VALUE )
		{
		   DEBUG_FAIL( GdiDeleteSpoolFileHandle(m_hSpoolFile) );
		}

		// TODO : Do any clean up work here.
	}

// Operations
public:
//	virtual BOOL	HandlePrintJob( LPTSTR lpszDocumentName, LPPRINTPROCESSORDATA pPrintProcessorData );

protected:
	virtual	BOOL	PrintJob();
	virtual void	PrepareJob();
	virtual void	PrepareRequest();

// Print Functions
private:
	// Level 1
	void			PrintEMFSingleCopy( CPageNumberList& lstPages, bool& bOdd );
	// Level 2
	void			PrintForwardEMF();
	void			PrintReverseForDriverEMF( CPageNumberList& lstPages, bool& bOdd );
	void			PrintReverseEMF( CPageNumberList& lstPages, bool& bOdd );
	void			PrintBookletEMF();
	// Level 3
	DWORD			PrintOneSideForwardEMF( DWORD dwPageNumber );
	void			PrintOneSideReverseForDriverEMF( DWORD dwPageNumber );
	void			PrintOneSideReverseEMF( const DWORD& dwStartPage1, const DWORD& dwEndPage1, const DWORD& dwStartPage2, const DWORD& dwEndPage2 );
	void			PrintOneSideBookletEMF( const DWORD& dwTotalPrintPages, const DWORD& dwStartPage );
	// Level 4
	void			PlayEMFPage( HANDLE hEMF, const DWORD& dwPageNumber, const DWORD& dwPageIndex, const DWORD& dwAngle );

// Helper Functions
private:
	void			CheckNUpValue()							{ CheckNUpValue(m_dwNumberOfPagesPerSide); };
	void			GetPageCoordinatesForNUp( RECT* pRectDocument, RECT* pRectBorder, UINT nCurrentPageNumber, bool& bRotate );
	void			SetDriverCopies( const DWORD& dwNumberOfCopies );
	void			ResetDCForNewDevMode( const DWORD& dwPageNumber, bool bInsidePage, bool& bNewDevMode );
	void			GetStartPageList( CPageNumberList& lstPages, bool& bOdd );
	bool			DifferentDevModes( LPDEVMODE pDevMode1, LPDEVMODE pDevMode2 );
	bool			CopyDevMode();

// Static Helper Functions
private:
	static bool		CompareWithFlags( const DWORD& dwValue1, const DWORD& dwValue2, const DWORD& dwFlags )
	{
		return bool( (dwValue1 & dwFlags) != (dwValue2 & dwFlags) );
	}

	static void		CheckNUpValue( const DWORD& dwPages )
	{
		if ( (dwPages != 1) && (dwPages != 2) && (dwPages != 4) && (dwPages != 6) && (dwPages != 9) && (dwPages != 16) )
		{
			throw EMF_EXCEPTION( CEMFException::exEMF_InvalidNUpValue );
		}
	}
};

//////////////////////////////////////////////////////////////////////
// Remove Debug Information
//////////////////////////////////////////////////////////////////////

#undef THIS_FILE

//////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_EMF_H__273BA48D_1D23_11D3_BEEF_00E02931B31A__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of EMFJobHandler.h
//////////////////////////////////////////////////////////////////////