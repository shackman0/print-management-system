//////////////////////////////////////////////////////////////////////
// PCL5CommandMap.h: interface for the CPCL5CommandMap class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL5COMMANDMAP_H__C7B48480_B91C_4D6D_B850_8527F3C48796__INCLUDED_)
#define AFX_PCL5COMMANDMAP_H__C7B48480_B91C_4D6D_B850_8527F3C48796__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////

#include "PCL5DataType.h"

//////////////////////////////////////////////////////////////////////
// Defines
//////////////////////////////////////////////////////////////////////

#define	DEFAULT_SIZE_HASH_TABLE							37

//////////////////////////////////////////////////////////////////////
// Type Definitions
//////////////////////////////////////////////////////////////////////

class CPCL5Parser;

typedef void (CPCL5Parser::*PCL5Operator)( const CPCL5DataType* );

//////////////////////////////////////////////////////////////////////
// Hash Function for the PCL5 commands
//////////////////////////////////////////////////////////////////////

#define HASH_PCL5(x,y,z)	( ((x) << 16) + ((y) << 8) + (z) )

//////////////////////////////////////////////////////////////////////
// Class Definition
//////////////////////////////////////////////////////////////////////

class CPCL5CommandMap  
{
// Map Entry Class
public:
	class CMapEntry
	{
	// Attributes
	private:
		CMapEntry*			m_pNext;
		UINT				m_nHashValue;
		DWORD				m_dwKey;
		PCL5Operator		m_fpOperator;

	// Construction / Destruction
	private:
		CMapEntry()
		{
			m_pNext			= NULL;
			m_nHashValue	= 0;
		}

		CMapEntry( const DWORD& dwKey, const UINT& nHash, CMapEntry* pNext = NULL )
		{
			m_dwKey			= dwKey;
			m_nHashValue	= nHash;
			m_pNext			= pNext;
		}

		~CMapEntry()
		{
		}

	// Data Access
	private:
		// Get Functions
		CMapEntry*				GetNext() const								{ return m_pNext; };
		const UINT&				GetHashValue() const						{ return m_nHashValue; };
		const DWORD&			GetKey() const								{ return m_dwKey; };
		const PCL5Operator&		GetValue() const							{ return m_fpOperator; };
		PCL5Operator&			GetValue()									{ return m_fpOperator; };
		// Set Functions												
		void					SetNext( CMapEntry* pNext )					{ m_pNext		= pNext; };
		void					SetHashValue( const UINT& nHash )			{ m_nHashValue	= nHash; };
		void					SetKey( const DWORD& dwKey )				{ m_dwKey		= dwKey; };
		void					SetValue( const PCL5Operator& fpOperator )	{ m_fpOperator	= fpOperator; };

		friend class CPCL5CommandMap;
	};

// Attributes:
private:
	CMapEntry**			m_ppHashTable;
	UINT				m_nHashTableSize;
	UINT				m_nEntryCount;

// Construction / Destruction
public:
	CPCL5CommandMap( const UINT& nHashTableSize = DEFAULT_SIZE_HASH_TABLE );
	virtual ~CPCL5CommandMap();

// Data Access
public:
	const UINT&		GetEntryCount() const		{ return m_nEntryCount; };
	const UINT&		GetHashTableSize() const	{ return m_nHashTableSize; };
	BOOL			IsEmpty() const				{ return (m_nEntryCount==0); };

// Map Operations
public:
	BOOL			Lookup( const DWORD& dwKey, PCL5Operator& fpOperator ) const
	{
		UINT		nHash;
		CMapEntry*	pEntry;
		BOOL		bReturnMe = FALSE;

		pEntry = GetEntry( dwKey, nHash );
		if ( pEntry != NULL )
		{
			fpOperator	= pEntry->GetValue();
			bReturnMe	= TRUE;
		}
		return bReturnMe;
	}

	BOOL			Lookup( const CPCL5DataType& pcl5Data, PCL5Operator& fpOperator ) const
	{
		return Lookup( HashKey(pcl5Data), fpOperator );
	}

	void			SetAt( const DWORD& dwKey, const PCL5Operator& fpOperator );
	//BOOL			RemoveKey( const DWORD& dwKey );
	void			RemoveAll();

// Iteration Operations
private:
	void			LookupNextEntry( CMapEntry*& pEntry );

public:
	CMapEntry*		GetStartPosition();
	void			GetNextEntry( CMapEntry*& pEntry, DWORD& dwKey, PCL5Operator& fpOperator );

// Operators
public:
	PCL5Operator&	operator[]( const DWORD& dwKey );

// Internal Operations
protected:
	void			InitHashTable( const UINT& nHashTableSize );
	CMapEntry*		GetEntry( const DWORD& dwKey, UINT& nHashValue ) const;

// Static Operations
protected:
	static DWORD	HashKey( const CPCL5DataType& pcl5Data )
	{
		return HASH_PCL5(pcl5Data.GetData().chrCommand, pcl5Data.GetData().chrGroup, pcl5Data.GetData().chrTerminator);
	}


};

#endif // !defined(AFX_PCL5COMMANDMAP_H__C7B48480_B91C_4D6D_B850_8527F3C48796__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of PCL5CommandMap.h
//////////////////////////////////////////////////////////////////////