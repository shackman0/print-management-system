///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL6AttributeList.cpp: implementation of the CPCL6AttributeList class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "PCL6AttributeList.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

bool CPCL6AttributeList::AddAttribute( CPCL6Attribute*& pAddMe )
{
	if ( GetCount() > 0 )
	{
		POSITION			pos = GetHeadPosition();
		CPCL6Attribute*		pCurrent;

		// Find the first node with an AttributeID that is higher than the current one
		// and insert just before that node
		while ( pos != NULL )
		{
			pCurrent = GetAt(pos);
			if ( pCurrent->GetAttributeID() > pAddMe->GetAttributeID() )
			{
				InsertBefore( pos, pAddMe );
				pos = NULL;
			}
			else
			{
				GetNext(pos);
				if ( pos == NULL )
				{
					// Gone through the entire list and none are higher than the current
					// so the current one must go on the end
					AddTail( pAddMe );
				}
			}
		}
	}
	else
	{
		// Just add it to the beginning
		AddHead( pAddMe );
	}

	// Display Debugging Information
	DEBUG_PCL6_TEXT( _T("Added PCL6 Attribute: 0x%08x (%d Bytes)\n"), pAddMe, sizeof(CPCL6Attribute) );

	// Clear out the pointer passed in, so it is no longer referenced
	pAddMe = NULL;

	// Return success
	return true;
}

CPCL6Attribute*	CPCL6AttributeList::FindAttribute( const WORD& nAttributeID )
{
	POSITION			pos			= GetHeadPosition();
	CPCL6Attribute*		pCurrent	= NULL;
	CPCL6Attribute*		pReturnMe	= NULL;

	// Loop through the list until the value is found, or the end is reached
	while( (pos != NULL) && (pReturnMe == NULL) )
	{
		pCurrent = GetNext(pos);

		// Check to see if there is a match
		if ( pCurrent->GetAttributeID() == nAttributeID )
		{
			pReturnMe = pCurrent;
		}
		else
		{
			// Check to see if it would have been found by now
			if ( pCurrent->GetAttributeID() > nAttributeID )
			{
				// Skip to the end
				pos = NULL;
			}
		}
	}

	// Return the value
	return pReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_PCL6_DEBUG
void CPCL6AttributeList::Dump()
{
	POSITION			pos			= GetHeadPosition();
	CPCL6Attribute*		pAttrib		= NULL;
	unsigned register	nCounter	= 0;

	while ( pos != NULL )
	{
		pAttrib = GetNext(pos);
		DEBUG_TEXT( _T("\tNode %02d:\n"), nCounter++ );
		pAttrib->Dump();
	}
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL6AttributeList.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////