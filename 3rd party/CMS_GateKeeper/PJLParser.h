///////////////////////////////////////////////////////////////////////////////////////////////////////
// PJLParser.h: interface for the CPJLParser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PJLPARSER_H__7BC963D9_EAD3_11D3_A430_00105A1C588D__INCLUDED_)
#define AFX_PJLPARSER_H__7BC963D9_EAD3_11D3_A430_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintParserBase.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define PJL_UEL									"\x1b%-12345X"	// <ESC>%-12345X
#define PJL_TAG									"@PJL"
#define PJL_TERMINATOR							"\n"
#define PJL_TERMINATOR_2						'\r'
#define PJL_ASSIGNMENT							"="
												
#define PJL_COMMANDTEXT_ENTER					"ENTER"
#define PJL_COMMANDTEXT_SET						"SET"
												
#define PJL_COMMAND_ENTER						PJL_TAG " " PJL_COMMANDTEXT_ENTER
#define PJL_COMMAND_SET							PJL_TAG " " PJL_COMMANDTEXT_SET
												
#define PJL_PROPERTY_COPIES						"COPIES"
#define PJL_PROPERTY_DUPLEX						"DUPLEX"
#define PJL_PROPERTY_ORIENTATION				"ORIENTATION"
#define PJL_PROPERTY_PAPER						"PAPER"
#define PJL_PROPERTY_PAPERLENGTH				"PAPERLENGTH"
#define PJL_PROPERTY_PAPERWIDTH					"PAPERWIDTH"
#define PJL_PROPERTY_QUANTITY					"QTY"
#define PJL_PROPERTY_RENDERMODE					"RENDERMODE"
#define PJL_PROPERTY_RESOLUTION					"RESOLUTION"
#define PJL_PROPERTY_FINISH						"FINISH"
												
#define PJL_VALUE_TRUE							"ON"
#define PJL_VALUE_FALSE							"OFF"
												
#define PJL_VALUE_ORIENTATION_PORTRAIT			"PORTRAIT"
#define PJL_VALUE_ORIENTATION_LANDSCAPE			"LANDSCAPE"

#define PJL_VALUE_RENDERMODE_COLOR				"COLOR"
#define PJL_VALUE_RENDERMODE_GRAYSCALE			"GRAYSCALE"

#define PJL_VALUE_FINISH_STAPLE					"STAPLE"
#define PJL_VALUE_FINISH_NONE					"NONE"
												
#define CONST_LENGTH(str)						(sizeof(str) - 1)

#define	CHECK_STRING(buf,string)				CheckString(buf, string, CONST_LENGTH(string))

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPJLParser Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPJLParser : public CPrintParserBase  
{
// Construction / Destruction
public:
	CPJLParser();
	virtual ~CPJLParser();

// Operations
public:
	virtual bool	Parse( CPrintParserData* pParserData );

// Command Functions
private:
	void			ProcessSetCommand( LPCSTR lpCommand );

// Value Handling Functions
private:
	void			HandleFinish( LPCSTR lpString );
	void			HandleOrientation( LPCSTR lpString );
	void			HandlePaper( LPCSTR lpString );
	void			HandleRenderMode( LPCSTR lpString );

// Utility Functions
private:
	static void		SkipToValue( LPCSTR& lpString );
	static bool		ReadBooleanValue( LPCSTR lpString );
	static long		ReadLongValue( LPCSTR lpString );
	static bool		CheckString( LPCSTR lpBuffer, LPCSTR lpString, DWORD dwSize );

// Attributes
private:
	CPrintParserData*			m_pParserData;

// Static Attributes
private:
	static const PaperSizeInfo	PJLFormNames[];
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPJLParser Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPJLParser::CPJLParser()
	{
		m_pParserData = NULL;
	}

	inline CPJLParser::~CPJLParser()
	{
		// Nothing to do.
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Utility Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline void CPJLParser::SkipToValue( LPCSTR& lpString )
	{
		// Find the assignment operator
		lpString = strstr( lpString, PJL_ASSIGNMENT );
		
		// If it isn't found, the parsing should fail
		if ( lpString == NULL )
		{
			throw PRINT_PARSER_EXCEPTION( _T("PJL: Missing assignment operator") );
		}
		
		// Skip past the assignment operator
		lpString += CONST_LENGTH(PJL_ASSIGNMENT);

		// Skip past any white space
		while ( isspace(*lpString) )
		{
			lpString++;
		}
	}

	inline long CPJLParser::ReadLongValue( LPCSTR lpString )
	{
		// Skip to the value
		SkipToValue( lpString );

		// Convert the buffer to a numeric value
		return atol( lpString );
	}

	inline bool CPJLParser::ReadBooleanValue( LPCSTR lpString )
	{
		// Skip to the value
		SkipToValue( lpString );

		// Try every expected value, until a match is found.
		// Then, process the match, and exit.
		// If no match is found, then throw an exception.

		// Try false
		if ( CHECK_STRING(lpString, PJL_VALUE_FALSE) )
		{
			// Return false
			return false;
		}

		// Try true
		if ( CHECK_STRING(lpString, PJL_VALUE_TRUE) )
		{
			// Return true
			return true;
		}

		throw PRINT_PARSER_EXCEPTION( _T("PJL: Invalid Boolean value") );
	}

	inline bool CPJLParser::CheckString( LPCSTR lpBuffer, LPCSTR lpString, DWORD dwSize )
	{
		return ( _strnicmp(lpBuffer, lpString, dwSize) == 0 );
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PJLPARSER_H__7BC963D9_EAD3_11D3_A430_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PJLParser.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
