///////////////////////////////////////////////////////////////////////////////////////////////////////
// RegistrySettings.h : Contains all of the registry information and key names.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 1999-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_REGISTRYSETTINGS_H__50B6F205_4ABD_11D3_A3CE_00105A1C588D__INCLUDED_)
#define AFX_REGISTRYSETTINGS_H__50B6F205_4ABD_11D3_A3CE_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ProductInfo.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Error Checking
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CMS_PRODUCT_NAME
	#error "Value for CMS_PRODUCT_NAME must be defined before including this file (see ProductInfo.h)"
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMS Core Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define MAX_SIZE_REGISTRY_SETTING							256
#define REG_DIV												_T("\\")

#define REG_SECTION_SOFTWARE								_T("SOFTWARE")
#define REG_SECTION_CMS										_T("Card Meter Systems")
#define REG_SECTION_PRINTTRACKING							_T("PrintTracking")
#define REG_SECTION_PRINTSERVER								_T("PrintServer")
#define REG_SECTION_CARDSECURITY							_T("CardSecurity")
#define REG_SECTION_RELEASESTATION							_T("ReleaseStation")
#define REG_SECTION_IPRINT									_T("InternetPrinting")
#define REG_SECTION_PAYMENTS								_T("Payments")
#define REG_SECTION_ADDINS									_T("AddIns")

#define REG_KEY_SOFTWARE									REG_SECTION_SOFTWARE

#define REG_KEY_CMS											REG_KEY_SOFTWARE				\
																REG_DIV						\
																REG_SECTION_CMS

#define REG_KEY_BASE										REG_KEY_CMS						\
																REG_DIV						\
																CMS_PRODUCT_NAME

#define REG_KEY_BASE_PRINTSERVER							REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_PRINTSERVER

#define REG_KEY_BASE_PRINTTRACKING							REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_PRINTTRACKING

#define REG_KEY_BASE_PRINTTRACKING_ADDINS					REG_KEY_BASE_PRINTTRACKING		\
																REG_DIV						\
																REG_SECTION_ADDINS

#define REG_KEY_BASE_CARDSECURITY							REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_CARDSECURITY								
								
#define REG_KEY_BASE_RELEASESTATION							REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_RELEASESTATION
																
#define REG_KEY_BASE_IPRINT									REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_IPRINT
									
#define REG_KEY_BASE_PAYMENTS								REG_KEY_BASE					\
																REG_DIV						\
																REG_SECTION_PAYMENTS		\
																REG_DIV
									
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Common Value Names
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_COMMON_CUSTOMERNAME						_T("CustomerName")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Popular Value Names
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_LICENSE									_T("LicenseKey")

#define REG_VALUE_FORCE_JOBDATA								_T("AlwaysSendJobData")

#define REG_VALUE_LOGOPATH									_T("LogoPath")
	
#define REG_VALUE_REFRESH_PRINTERS_SECONDS					_T("RefreshPrintersSeconds")
#define REG_VALUE_REFRESH_PRINTERS_MINUTES					_T("RefreshPrintersMinutes")
#define REG_VALUE_REFRESH_PRINTERS_HOURS					_T("RefreshPrintersHours")
#define REG_VALUE_REFRESH_PRINTERS_DAYS						_T("RefreshPrintersDays")

#define REG_VALUE_PORT_LISTEN								_T("ListenPort")							

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Print Server Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Print Server Setting : Name of the Print Tracking Server
#define REG_VALUE_PRINTSERVER_NAME_SERVER					CMS_PRODUCT_NAME _T("Server")
// Print Server Setting : Whether or not to force the raw data to be sent
#define REG_VALUE_PRINTSERVER_FORCE_JOBDATA					REG_VALUE_FORCE_JOBDATA

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Print Tracking Server Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Normal Values
#define REG_VALUE_PRINTTRACKING_LICENSE						REG_VALUE_LICENSE
#define REG_VALUE_PRINTTRACKING_TRACKINGONLY				_T("TrackingOnly")
#define REG_VALUE_PRINTTRACKING_CANCELUNTRACKED				_T("CancelUntracked")
// Dialog Box Values
#define REG_VALUE_PRINTTRACKING_LOGOPATH					REG_VALUE_LOGOPATH
#define REG_VALUE_PRINTTRACKING_DIALOGTIMEOUT				_T("DialogTimeout")
#define REG_VALUE_PRINTTRACKING_DIALOGATTEMPTS				_T("DialogAttempts")
#define REG_VALUE_PRINTTRACKING_DIALOGLISTENPORT			_T("Dialog") REG_VALUE_PORT_LISTEN
// Payment Types
#define REG_VALUE_PRINTTRACKING_TAKECASHXCP					_T("TakeCashXCP")
#define REG_VALUE_PRINTTRACKING_TAKECASHMICROCOIN			_T("TakeCashMicroCoin")
#define REG_VALUE_PRINTTRACKING_TAKEMAGCARDCMS				_T("TakeMagCardCMS")
#define REG_VALUE_PRINTTRACKING_TAKEMAGCARDDANYL			_T("TakeMagCardDanyl")
#define REG_VALUE_PRINTTRACKING_TAKESMARTCARDCMS			_T("TakeSmartCardCMS")
#define REG_VALUE_PRINTTRACKING_SHOWPMCTRLFIELDS			_T("ShowPMCtrlFields")
// Payment Values
#define REG_VALUE_PRINTTRACKING_CARDSECSERVER				_T("CardSecurityServer")
#define REG_VALUE_PRINTTRACKING_LOWBALANCEWARNING_MESSAGE	_T("LowBalanceWarningMessage")
#define REG_VALUE_PRINTTRACKING_LOWBALANCEWARNING			_T("LowBalanceWarning")
#define REG_VALUE_PRINTTRACKING_ACCEPTDEBIT					_T("AcceptDebit")
#define REG_VALUE_PRINTTRACKING_ACCEPTCREDIT				_T("AcceptCredit")
#define REG_VALUE_PRINTTRACKING_ACCEPTSERVICE				_T("AcceptService")

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Print Tracking Server Add-In Values
	///////////////////////////////////////////////////////////////////////////////////////////////////
	#define REG_VALUE_PRINTTRACKING_ADDIN_SEQNUM			_T("SequenceNumber")
	#define REG_VALUE_PRINTTRACKING_ADDIN_ENABLE			_T("Enable")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Database Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_DATABASE_SERVER							_T("Server")
#define REG_VALUE_DATABASE_NAME								_T("Database")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Card Security Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_CARDSEC_LICENSE							REG_VALUE_LICENSE
#define REG_VALUE_CARDSEC_PORT_LISTEN						REG_VALUE_PORT_LISTEN
#define REG_VALUE_CARDSEC_RANGE_SERIAL_MIN					_T("SerialNumberMin")
#define REG_VALUE_CARDSEC_RANGE_SERIAL_MAX					_T("SerialNumberMax")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Release Station Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_RS_SERVER									_T("Server")
#define REG_VALUE_RS_KEEPJOB_SECONDS						_T("KeepJobSeconds")
#define REG_VALUE_RS_KEEPJOB_MINUTES						_T("KeepJobMinutes")
#define REG_VALUE_RS_KEEPJOB_HOURS							_T("KeepJobHours")
#define REG_VALUE_RS_KEEPJOB_DAYS							_T("KeepJobDays")
#define REG_VALUE_RS_REFRESH_PRINTERS_SECONDS				REG_VALUE_REFRESH_PRINTERS_SECONDS
#define REG_VALUE_RS_REFRESH_PRINTERS_MINUTES				REG_VALUE_REFRESH_PRINTERS_MINUTES
#define REG_VALUE_RS_REFRESH_PRINTERS_HOURS					REG_VALUE_REFRESH_PRINTERS_HOURS
#define REG_VALUE_RS_REFRESH_PRINTERS_DAYS					REG_VALUE_REFRESH_PRINTERS_DAYS
#define REG_VALUE_RS_SHOWPRINTSERVER						_T("ShowPrintServer")
#define REG_VALUE_RS_PATHJOBINFO							_T("PathJobInfo")
#define REG_VALUE_RS_PATHJOBDATA							_T("PathJobData")
#define REG_VALUE_RS_LICENSE								REG_VALUE_LICENSE
#define REG_VALUE_RS_LOGOPATH								REG_VALUE_LOGOPATH
#define REG_VALUE_RS_PORT_LISTEN							REG_VALUE_PORT_LISTEN

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Line Printer Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Line Printer Setting : Name of the license key
#define REG_VALUE_LP_LICENSE								REG_VALUE_LICENSE
// Line Printer Setting : Name of the Print Tracking Server
#define REG_VALUE_LP_SERVER									REG_VALUE_PRINTSERVER_NAME_SERVER
// Line Printer Setting : Name of the LPD Print Spool Directory
#define REG_VALUE_LP_SPOOL_DIRECTORY						_T("SpoolDirectory")
// Line Printer Setting : Whether or not to force the raw data to be sent
#define REG_VALUE_LP_FORCE_JOBDATA							REG_VALUE_FORCE_JOBDATA
// Line Printer Setting : Whether or not to remove print job raw data from print spool directory
#define REG_VALUE_LP_REMOVE_JOBDATA							_T("AlwaysRemoveJobData")
// Line Printer Setting : How often to read the printer list from the database
#define REG_VALUE_LP_REFRESH_PRINTERS_SECONDS				REG_VALUE_REFRESH_PRINTERS_SECONDS
#define REG_VALUE_LP_REFRESH_PRINTERS_MINUTES				REG_VALUE_REFRESH_PRINTERS_MINUTES
#define REG_VALUE_LP_REFRESH_PRINTERS_HOURS					REG_VALUE_REFRESH_PRINTERS_HOURS
#define REG_VALUE_LP_REFRESH_PRINTERS_DAYS					REG_VALUE_REFRESH_PRINTERS_DAYS
// Line Printer Queue Name
#define REG_VALUE_LP_QUEUE_NAME								_T("QueueName")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Payment Method Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_VALUE_PAYMENT_PORT								_T("Port")
#define REG_VALUE_PAYMENT_PAYMAG_CMSREADER_PORT				REG_VALUE_PAYMENT_PORT
#define REG_VALUE_PAYMENT_PAYMAG_DANYLREADER_PORT			REG_VALUE_PAYMENT_PORT
#define REG_VALUE_PAYMENT_PAYCASH_XCPSTATION_PORT			REG_VALUE_PAYMENT_PORT
#define REG_VALUE_PAYMENT_PAYCASH_CASHACCEPTOR_COIN_PORT	REG_VALUE_PAYMENT_PORT _T("CoinPort")
#define REG_VALUE_PAYMENT_PAYCASH_CASHACCEPTOR_BILL_PORT	REG_VALUE_PAYMENT_PORT _T("BillPort")			
#define REG_VALUE_PAYMENT_STATE								_T("State")

///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////
// Registry Settings Not Specific to any CMS Product
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Main Windows Registry
///////////////////////////////////////////////////////////////////////////////////////////////////////

// Keys
#define	REG_KEY_WINDOWS_LOGONINFO					_T("Software\\Microsoft\\Windows NT\\")			\
															_T("CurrentVersion\\Winlogon")
#define REG_KEY_WINDOWS_LOGONINFO_95				_T("System\\CurrentControlSet\\Services\\")		\
															_T("MSNP32\\NetworkProvider")

// Values
#define REG_VALUE_WINDOWS_DOMAINNAME				_T("CachePrimaryDomain")
#define REG_VALUE_WINDOWS_DOMAINNAME_95				_T("AuthenticatingAgent")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// COM Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_KEY_COM_APP_ID							_T("AppID")
#define REG_VALUE_COM_LOCALSERVICE					_T("LocalService")
#define REG_VALUE_COM_SERVICEPARAMS					_T("ServiceParameters")

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Event Log Values
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define REG_KEY_EVENTLOG_BASE						_T("SYSTEM\\CurrentControlSet\\Services\\")		\
															_T("EventLog\\Application\\")
#define REG_VALUE_EVENTLOG_MESSAGEFILE				_T("EventMessageFile")
#define REG_VALUE_EVENTLOG_TYPES					_T("TypesSupported")
#define REG_VALUE_EVENTLOG_CATEGORYFILE				_T("CategoryMessageFile")
#define REG_VALUE_EVENTLOG_CATEGORYCOUNT			_T("CategoryCount")

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_REGISTRYSETTINGS_H__50B6F205_4ABD_11D3_A3CE_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of RegistrySettings.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
