///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobHandler.h: interface for the CPrintJobHandler class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTJOBHANDLER_H__273BA491_1D23_11D3_BEEF_00E02931B31A__INCLUDED_)
#define AFX_PRINTJOBHANDLER_H__273BA491_1D23_11D3_BEEF_00E02931B31A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "RevertToSelf.h"
#include "AccountLookup.h"
#include "PrintJobRequest.h"
#include "ErrorLog.h"
#include "Utils.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobHandlerBase Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrintJobHandlerBase
{
// Attributes
protected:
	LPTSTR					m_lpszDocumentName;
	LPPRINTPROCESSORDATA	m_pPrintProcessorData;
	CRevertToSelf			m_Revert;
	CAccountLookup			m_Lookup;

// Construction / Destruction
public:
	CPrintJobHandlerBase()
	{
		// Set the default values
		m_pPrintProcessorData	= NULL;
		m_lpszDocumentName		= NULL;
	}

	virtual ~CPrintJobHandlerBase()					{  };

// Operations
public:
	virtual BOOL HandlePrintJob( LPTSTR lpszDocumentName, LPPRINTPROCESSORDATA pPrintProcessorData )
	{
		// Initialize the pointers
		m_pPrintProcessorData	= pPrintProcessorData;
		m_lpszDocumentName		= lpszDocumentName;

		return TRUE;
	}
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobHandler Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrintJobHandler : public CPrintJobRequest, public CPrintJobHandlerBase
{
// Construction / Destruction
public:
	CPrintJobHandler( );
	virtual ~CPrintJobHandler();

// Operations
public:
	virtual BOOL	HandlePrintJob( LPTSTR lpszDocumentName, LPPRINTPROCESSORDATA pPrintProcessorData );

protected:
	void			SetUserProperties();
	BOOL			ReadRawJob();
	DWORD			GetJobSize( CProcBuffer& data );
	void			CheckJobStatus();

private:
	BOOL			SendRequest();

// Overriden Functions
protected:
	virtual void	OnServerNameMissing();
	virtual void	OnUpdateSuccess();
	virtual void	OnUpdateFail();

// Override Functions
protected:
	virtual	BOOL	PrintJob()	= 0;
	virtual void	PrepareJob()				{ ReadRawJob(); };
	virtual void	PrepareRequest()			{ ParseRawData(); };
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobHandler Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	inline void	CPrintJobHandler::OnServerNameMissing()
	{
		CErrorLog	errs;

		// Log the event
		errs.LogEvent( EVT_MSG_SERVER_NAME_MISSING );
		DEBUG_TEXT( _T("*** Registry setting for the server has not been set!\n") );
	}

	inline void	CPrintJobHandler::OnUpdateSuccess()
	{
		CErrorLog	errs;

		// Log the event
		errs.LogEvent( EVT_MSG_UPDATE_PRINTJOB_SUCCESS );
		DEBUG_OBJ_TEXT( _T("*** Successfully updated the page count!\n") );
	}

	inline void	CPrintJobHandler::OnUpdateFail()
	{
		CErrorLog	errs;

		// Log the event
		errs.LogEvent( EVT_MSG_UPDATE_PRINTJOB_FAILED );
		DEBUG_OBJ_TEXT( _T("*** Failed to update the page count!\n") );
	}

	inline void	CPrintJobHandler::CheckJobStatus()
	{
		// Check assumptions
		_ASSERTE( m_pPrintProcessorData != NULL );

		// Don't do anything if the job has already been aborted
		if ( m_bJobAborted == false )
		{
			// If the print processor is paused, wait for it to be resumed
			if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
			{
				DEBUG_OBJ_TEXT( _T("Print Job Paused\n") );
				::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
				DEBUG_OBJ_TEXT( _T("Print Job Resumed\n") );
			}

			// If the job has been aborted, set the member variable to indicate it
			if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_ABORTED )
			{
				DEBUG_OBJ_TEXT( _T("Print Job Aborted!\n") );
				m_bJobAborted = true;
			}
		}
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTJOBHANDLER_H__273BA491_1D23_11D3_BEEF_00E02931B31A__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobHandler.h
///////////////////////////////////////////////////////////////////////////////////////////////////////