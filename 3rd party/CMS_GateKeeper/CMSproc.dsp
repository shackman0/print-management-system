# Microsoft Developer Studio Project File - Name="CMSprocW2K" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=CMSprocW2K - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "CMSproc.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "CMSproc.mak" CFG="CMSprocW2K - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "CMSprocW2K - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "CMSprocW2K - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/CMSnet/Win2k/Print Server/CMSproc", JLCAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "CMSprocW2K - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CMSPROC_EXPORTS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "_USRDLL" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /machine:I386
# ADD LINK32 spoolss.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib netapi32.lib winppi.lib /nologo /dll /machine:I386
# Begin Special Build Tool
TargetPath=.\Release\CMSproc.dll
SOURCE="$(InputPath)"
PostBuild_Desc=Copy to Current Build Directory
PostBuild_Cmds=copy "$(TargetPath)" "\\cms-main\dev\Builds\PrintTracking\Current\CMSnet\Windows 2000"
# End Special Build Tool

!ELSEIF  "$(CFG)" == "CMSprocW2K - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 1
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_MBCS" /D "_USRDLL" /D "CMSPROC_EXPORTS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_UNICODE" /D "_USRDLL" /FR /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 ..\GateKeeper\Debug\Gatekeeper.obj spoolss.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib netapi32.lib winppi.lib jvm.lib /nologo /dll /map /debug /machine:I386 /nodefaultlib:"libc.lib" /nodefaultlib:"libcd.lib" /pdbtype:sept /libpath:"c:\j2sdk1.3\lib"
# SUBTRACT LINK32 /pdb:none

!ENDIF 

# Begin Target

# Name "CMSprocW2K - Win32 Release"
# Name "CMSprocW2K - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\AccountLookup.cpp
# End Source File
# Begin Source File

SOURCE=.\CMSnet_i.c
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\CMSproc.def
# End Source File
# Begin Source File

SOURCE=.\CMSString.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugObject.cpp
# End Source File
# Begin Source File

SOURCE=.\DebugUtils.cpp
# End Source File
# Begin Source File

SOURCE=.\DevMode.cpp
# End Source File
# Begin Source File

SOURCE=.\EMFJobHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\EMFParser.cpp
# End Source File
# Begin Source File

SOURCE=.\ErrorLog.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL5DataType.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL5Parser.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL6Attribute.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL6AttributeList.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL6DataType.cpp
# End Source File
# Begin Source File

SOURCE=.\PCL6Parser.cpp
# End Source File
# Begin Source File

SOURCE=.\PJLParser.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintJobBuffer.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintJobHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintJobRequest.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintParser.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintParserData.cpp
# End Source File
# Begin Source File

SOURCE=.\PrintProcessor.cpp
# End Source File
# Begin Source File

SOURCE=.\PSParser.cpp
# End Source File
# Begin Source File

SOURCE=.\RawJobHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\RevertToSelf.cpp
# End Source File
# Begin Source File

SOURCE=.\SpoolMemory.cpp
# End Source File
# Begin Source File

SOURCE=.\StandardTools.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\TextJob.cpp
# End Source File
# Begin Source File

SOURCE=.\TextJobHandler.cpp
# End Source File
# Begin Source File

SOURCE=.\Utils.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\AccountLookup.h
# End Source File
# Begin Source File

SOURCE=.\CMSException.h
# End Source File
# Begin Source File

SOURCE=.\CMSList.h
# End Source File
# Begin Source File

SOURCE=.\CMSnet.h
# End Source File
# Begin Source File

SOURCE=.\CMSString.h
# End Source File
# Begin Source File

SOURCE=.\ComVector.h
# End Source File
# Begin Source File

SOURCE=.\DataBlock.h
# End Source File
# Begin Source File

SOURCE=.\DataChain.h
# End Source File
# Begin Source File

SOURCE=.\DebugObject.h
# End Source File
# Begin Source File

SOURCE=.\DebugUtils.h
# End Source File
# Begin Source File

SOURCE=.\DevMode.h
# End Source File
# Begin Source File

SOURCE=.\EMFException.h
# End Source File
# Begin Source File

SOURCE=.\EMFJobHandler.h
# End Source File
# Begin Source File

SOURCE=.\EMFParser.h
# End Source File
# Begin Source File

SOURCE=.\ErrorLog.h
# End Source File
# Begin Source File

SOURCE=.\ExDevMode.h
# End Source File
# Begin Source File

SOURCE=.\PCL5DataType.h
# End Source File
# Begin Source File

SOURCE=.\PCL5Parser.h
# End Source File
# Begin Source File

SOURCE=.\PCL6Attribute.h
# End Source File
# Begin Source File

SOURCE=.\PCL6AttributeList.h
# End Source File
# Begin Source File

SOURCE=.\PCL6DataType.h
# End Source File
# Begin Source File

SOURCE=.\PCL6Parser.h
# End Source File
# Begin Source File

SOURCE=.\PJLParser.h
# End Source File
# Begin Source File

SOURCE=.\PrinterAPI.h
# End Source File
# Begin Source File

SOURCE=.\PrintJob.h
# End Source File
# Begin Source File

SOURCE=.\PrintJobBuffer.h
# End Source File
# Begin Source File

SOURCE=.\PrintJobHandler.h
# End Source File
# Begin Source File

SOURCE=.\PrintJobPropertyNames.h
# End Source File
# Begin Source File

SOURCE=.\PrintJobRequest.h
# End Source File
# Begin Source File

SOURCE=.\PrintParser.h
# End Source File
# Begin Source File

SOURCE=.\PrintParserBase.h
# End Source File
# Begin Source File

SOURCE=.\PrintParserData.h
# End Source File
# Begin Source File

SOURCE=.\PrintProcessor.h
# End Source File
# Begin Source File

SOURCE=.\PrintProcessorEvents.h
# End Source File
# Begin Source File

SOURCE=.\ProductInfo.h
# End Source File
# Begin Source File

SOURCE=.\PSParser.h
# End Source File
# Begin Source File

SOURCE=.\RawJobHandler.h
# End Source File
# Begin Source File

SOURCE=.\RegistrySettings.h
# End Source File
# Begin Source File

SOURCE=.\ReservedBlocks.h
# End Source File
# Begin Source File

SOURCE=.\resource.h
# End Source File
# Begin Source File

SOURCE=.\RevertToSelf.h
# End Source File
# Begin Source File

SOURCE=.\SpoolMemory.h
# End Source File
# Begin Source File

SOURCE=.\StandardTools.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\TextJob.h
# End Source File
# Begin Source File

SOURCE=.\TextJobException.h
# End Source File
# Begin Source File

SOURCE=.\TextJobHandler.h
# End Source File
# Begin Source File

SOURCE=.\ThreadLock.h
# End Source File
# Begin Source File

SOURCE=.\Utils.h
# End Source File
# Begin Source File

SOURCE=.\Win9xHack.h
# End Source File
# Begin Source File

SOURCE=.\WinPPI.h
# End Source File
# Begin Source File

SOURCE=.\WorldTransform.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\CMSproc.rc
# End Source File
# End Group
# Begin Source File

SOURCE=.\PrintProcessorEvents.mc
# End Source File
# End Target
# End Project
