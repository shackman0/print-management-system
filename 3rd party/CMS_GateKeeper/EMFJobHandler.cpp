//////////////////////////////////////////////////////////////////////
// EMFJobHandler.cpp: implementation of the EMF functions
//////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "EMFJobHandler.h"

//////////////////////////////////////////////////////////////////////
// Debug Information
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

//////////////////////////////////////////////////////////////////////
// Debug Tools
//////////////////////////////////////////////////////////////////////

#ifdef USE_EMF_DEBUG
	#define DEBUG_EMF				DEBUG_TEXT
#else
	#define DEBUG_EMF				((void)(0))
#endif

//////////////////////////////////////////////////////////////////////
// Print Job Function
//////////////////////////////////////////////////////////////////////

BOOL CEMFJobHandler::PrintJob()
{
	BOOL				bReturnMe		= FALSE;
	bool				bOdd			= false;
	bool				bStartDoc		= false;
	CPageNumberList		lstPages;
	DOCINFO				di;

	try
	{
		// Check for Valid Option for n-up printing
		CheckNUpValue();

		// NOTE: It may be needed to check m_dwJobNumberOfCopies and make sure it is > 0 before proceeding
		_ASSERTE( m_dwJobNumberOfCopies > 0 );

		// Save the old transformation on m_hPrinterDC
		m_Xform.SetDC( m_hPrinterDC );

		if ( m_bReverseOrderPrinting || m_bBookletPrint )
		{
			// Get start page list for reverse printing
			GetStartPageList( lstPages, bOdd );
		}

		// m_pCopyDM will be used for changing the copy count
		m_pCopyDM = m_pFirstDM ? m_pFirstDM : m_pDevMode;
		m_pCopyDM->dmPrintQuality = m_pDevMode->dmPrintQuality;
		m_pCopyDM->dmYResolution = m_pDevMode->dmYResolution;

		// Initialize the struct
		::ZeroMemory( &di, sizeof(di) );
		di.cbSize		= sizeof(di);
		di.lpszDocName	= m_lpszDocumentName;
		di.lpszOutput	= m_pPrintProcessorData->pOutputFile;
		di.lpszDatatype	= NULL;

		if ( GdiStartDocEMF(m_hSpoolFile, &di) )
		{
			bStartDoc = true;
		}
		else
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GdiStartDocEMF failed in PrintJob\n") );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartDocEMF );
		}

		if ( m_bCollate )
		{
			DWORD dwRemainingCopies = m_dwJobNumberOfCopies & 0x0000FFFF;
				
			while ( dwRemainingCopies > 0 )
			{
				DEBUG_EMF( _T("PrintJob() RemainingCopies = %d, JobCopies = %d, DrvCopies = %d\n"), dwRemainingCopies, m_dwJobNumberOfCopies, m_dwDrvNumberOfCopies );
				if ( dwRemainingCopies <= m_dwDrvNumberOfCopies )
				{
					SetDriverCopies( dwRemainingCopies );
					dwRemainingCopies = 0;
				}
				else
				{
					SetDriverCopies( m_dwDrvNumberOfCopies );
					dwRemainingCopies -= m_dwDrvNumberOfCopies;
				}

				PrintEMFSingleCopy( lstPages, bOdd );
			}
		}
		else
		{
			PrintEMFSingleCopy( lstPages, bOdd );
		}

		bReturnMe = TRUE;
	}
	catch ( ... )
	{
		if ( m_dwTotalNumberOfPages > m_dwPagesPrinted )
		{
			if ( UpdateRequest(m_dwPagesPrinted, m_dwJobNumberOfCopies, m_dwNumberOfPagesPerSide) == TRUE )
			{
				// TODO: Put an info message in the event log
				DEBUG_TEXT( _T("Updated Request from %d to %d Pages\n"), m_dwTotalNumberOfPages, m_dwPagesPrinted );
				bReturnMe = TRUE;
			}
			else
			{
				// TODO: Put an error or warning message in the event log
				DEBUG_TEXT( _T("Failed to Update Request from %d to %d Pages\n"), m_dwTotalNumberOfPages, m_dwPagesPrinted );
				bReturnMe = FALSE;
			}
		}
		else
		{
			DEBUG_TEXT( _T("Caught Exception but all pages have printed!\n") );
			bReturnMe = TRUE;
		}
	}

	if ( bStartDoc == true )
	{
		// End the Doc
		DEBUG_FAIL( GdiEndDocEMF(m_hSpoolFile) );
	}

	DEBUG_EMF( _T("Printed %d out of %d Pages!\n"), m_dwPagesPrinted, m_dwTotalNumberOfPages );

	// Clear out the DC for the WorldTransform
	m_Xform.SetDC( NULL );

	return bReturnMe;
}

//////////////////////////////////////////////////////////////////////
// Main Work Functions
//////////////////////////////////////////////////////////////////////

void CEMFJobHandler::PrepareJob()
{
	ATTRIBUTE_INFO_3	AttributeInfo;

	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	// Copy the DEVMODE into m_pDevMode
	if ( CopyDevMode() )
	{
		// Update resolution before CreateDC for monochrome optimization
		if ( GetJobAttributes(m_pPrintProcessorData->pPrinterName, m_pDevMode, &AttributeInfo) == FALSE )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GetJobAttributes failed on Printer %s\n"), m_pPrintProcessorData->pPrinterName );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGetJobAttributes );
		}

		if ( AttributeInfo.dwColorOptimization )
		{
			m_pDevMode->dmPrintQuality	= AttributeInfo.dmPrintQuality;
			m_pDevMode->dmYResolution	= AttributeInfo.dmYResolution;
		}

		// Get the spool file and the Device Context
		m_hSpoolFile = GdiGetSpoolFileHandle( m_pPrintProcessorData->pPrinterName, m_pDevMode, m_lpszDocumentName );
		if ( m_hSpoolFile != NULL )
		{
			m_hPrinterDC = GdiGetDC( m_hSpoolFile );
			// Is the DC valid?
			if ( m_hPrinterDC == NULL )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiGetDC Failed in PrepareJob()\n") );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetDC );
			}
		}
		else
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GdiGetSpoolFileHandle Failed in PrepareJob()!  Printer: %s, Document: %s\n"), m_pPrintProcessorData->pPrinterName, m_lpszDocumentName );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetSpoolFileHandle );
		}

		// Use the first devmode in the spool file to update the copy count
		// and the collate setting
		if ( (GdiGetDevmodeForPage(m_hSpoolFile, 1, &m_pFirstDM, NULL) == FALSE) || (m_pFirstDM == NULL) )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GdiGetDevmodeForPage Failed in PrepareJob!  Printer: %s, Document: %s\n"), m_pPrintProcessorData->pPrinterName, m_lpszDocumentName );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetDevmodeForPage );
		}
		if ( m_pFirstDM->dmFields & DM_COPIES )
		{
			m_pDevMode->dmFields |= DM_COPIES;
			m_pDevMode->dmCopies = m_pFirstDM->dmCopies;
		}
		if ( m_pFirstDM->dmFields & DM_COLLATE )
		{
			m_pDevMode->dmFields |= DM_COLLATE;
			m_pDevMode->dmCollate = m_pFirstDM->dmCollate;
		}

		// The number of copies of the print job is the product of the number of copies set
		// from the driver UI (present in the devmode) and the number of copies in m_pPrintProcessorData struct
		m_dwJobNumberOfCopies = (m_pDevMode->dmFields & DM_COPIES) ? m_pPrintProcessorData->Copies*m_pDevMode->dmCopies
															   : m_pPrintProcessorData->Copies;
		m_pDevMode->dmCopies = (short) m_dwJobNumberOfCopies;
		m_pDevMode->dmFields |=  DM_COPIES;

		// Update the Parser's value for number of copies
		m_ParserData.SetCopies( (short)m_dwJobNumberOfCopies );

		// Call GetJobAttributes again using the updated m_pDevMode
		if ( GetJobAttributes(m_pPrintProcessorData->pPrinterName, m_pDevMode, &AttributeInfo) == FALSE )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GetJobAttributes failed on Printer %s in PrepareJob()\n"), m_pPrintProcessorData->pPrinterName );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGetJobAttributes );
		}

		// Initialize bReverseOrderPrinting, dwJobNumberOfPagesPerSide,
		// dwDrvNumberOfPagesPerSide, dwNupBorderFlags, dwJobNumberOfCopies,
		// dwDrvNumberOfCopies and bCollate
		m_dwJobNumberOfPagesPerSide	= AttributeInfo.dwJobNumberOfPagesPerSide;
		m_dwDrvNumberOfPagesPerSide	= AttributeInfo.dwDrvNumberOfPagesPerSide;
		m_dwNupBorderFlags			= AttributeInfo.dwNupBorderFlags;
		m_dwJobNumberOfCopies 		= AttributeInfo.dwJobNumberOfCopies;
		m_dwDrvNumberOfCopies 		= AttributeInfo.dwDrvNumberOfCopies;

		m_bReverseOrderPrinting		= CompareWithFlags( AttributeInfo.dwJobPageOrderFlags, AttributeInfo.dwDrvPageOrderFlags, (NORMAL_PRINT|REVERSE_PRINT) );
		m_bBookletPrint				= CompareWithFlags( AttributeInfo.dwJobPageOrderFlags, AttributeInfo.dwDrvPageOrderFlags, BOOKLET_PRINT );
		m_bCollate					= bool( (m_pDevMode->dmFields & DM_COLLATE) && (m_pDevMode->dmCollate == DMCOLLATE_TRUE) );
		m_bDuplex 					= bool( (m_pDevMode->dmFields & DM_DUPLEX) && (m_pDevMode->dmDuplex != DMDUP_SIMPLEX) );

		// Color optimization may cause wrong output with duplex
		m_dwOptimization			= ( (AttributeInfo.dwColorOptimization == COLOR_OPTIMIZATION) && (m_bDuplex==false) )
										? COLOR_OPTIMIZATION : NO_COLOR_OPTIMIZATION;
		m_dwDuplexMode				= (m_bDuplex == false) ? EMF_DUP_NONE :
										((m_pDevMode->dmDuplex == DMDUP_HORIZONTAL) ? EMF_DUP_HORZ : EMF_DUP_VERT);

		if ( m_bBookletPrint == true )
		{
			if ( m_bDuplex == false )
			{
				// Not supported w/o duplex printing. Use default settings.
				m_bBookletPrint				= false;
				m_dwDrvNumberOfPagesPerSide = 1;
				m_dwJobNumberOfPagesPerSide = 1;
			}
			else
			{
				// Fixed settings for pages per side.
				m_dwDrvNumberOfPagesPerSide	= 1;
				m_dwJobNumberOfPagesPerSide = 2;
			}
		}

		// Number of pages per side that the print processor has to play
		m_dwNumberOfPagesPerSide = (m_dwDrvNumberOfPagesPerSide == 1) ? m_dwJobNumberOfPagesPerSide : 1;

		if ( m_dwNumberOfPagesPerSide == 1 )
		{
			// If the print processor is not doing nup, don't draw borders
			m_dwNupBorderFlags = NO_BORDER_PRINT;
		}

		// Get the number of pages in the job. This call waits till the last page is spooled.
		m_dwTotalNumberOfPages = GdiGetPageCount(m_hSpoolFile);
		if ( m_dwTotalNumberOfPages > 0 )
		{
			// Getting the page count from the API should be as reliable as being parsed
			m_bParsed = true;
		}
		else
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GdiGetPageCount failed on Printer %s in PrepareJob()\n"), m_pPrintProcessorData->pPrinterName );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetPageCount );
		}
	}
}

void CEMFJobHandler::PrepareRequest()
{
	// Display some debug information
	DEBUG_EMF(	_T("EMFJobHandler Properties...\n")
				_T("    Number of Pages Per Side:         %d\n")
				_T("    Total Number of Pages:            %d\n")
				_T("    Nup Border Flags:                 %d\n")
				_T("    Job Number of Pages Per Side:     %d\n")
				_T("    Driver Number of Pages Per Side:  %d\n")
				_T("    Duplex Mode:                      %d\n")
				_T("    Job Number of Copies:             %d\n")
				_T("    Driver Number of Copies:          %d\n")
				_T("    Optimization:                     %d\n")
				_T("    Reverse Order Printing:           %s\n")
				_T("    Collate:                          %s\n")
				_T("    Duplex:                           %s\n")
				_T("    Booklet Print:                    %s\n")
			, m_dwNumberOfPagesPerSide
			, m_dwTotalNumberOfPages
			, m_dwNupBorderFlags
			, m_dwJobNumberOfPagesPerSide
			, m_dwDrvNumberOfPagesPerSide
			, m_dwDuplexMode
			, m_dwJobNumberOfCopies
			, m_dwDrvNumberOfCopies
			, m_dwOptimization
			, DEBUG_BOOL(m_bReverseOrderPrinting)
			, DEBUG_BOOL(m_bCollate)
			, DEBUG_BOOL(m_bDuplex)
			, DEBUG_BOOL(m_bBookletPrint)
		);

	m_ParserData.SetPageCount( m_dwTotalNumberOfPages );
	m_ParserData.SetPagesPerPage( m_dwNumberOfPagesPerSide );
	m_ParserData.SetDataType( m_pPrintProcessorData->pDatatype );

	if ( m_bCollate )
	{
		m_ParserData.SetCollate( DMCOLLATE_TRUE );
	}

	switch( m_dwDuplexMode )
	{
	case EMF_DUP_NONE:
		m_ParserData.SetDuplex( DMDUP_SIMPLEX );
		break;

	case EMF_DUP_VERT:
		m_ParserData.SetDuplex( DMDUP_VERTICAL );
		break;

	case EMF_DUP_HORZ:
		m_ParserData.SetDuplex( DMDUP_HORIZONTAL );
		break;

	default:
		DEBUG_TEXT( _T("Unknown Duplex Mode Value: %d\n"), m_dwDuplexMode );
		_ASSERTE( false );
		break;
	}
}

//////////////////////////////////////////////////////////////////////
// Print Functions
//////////////////////////////////////////////////////////////////////

// Level 1 Print Functions
void CEMFJobHandler::PrintEMFSingleCopy( CPageNumberList& lstPages, bool& bOdd )
{
	if ( m_bBookletPrint == false )
	{
		if ( m_bReverseOrderPrinting == false )
		{
			DEBUG_EMF( _T("Using PrintForwardEMF()\n") );
			// Normal printing
			PrintForwardEMF();
		}
		else
		{
			if ( (m_dwDrvNumberOfPagesPerSide != 1) || (m_dwNumberOfPagesPerSide == 1) )
			{
				DEBUG_EMF( _T("Using PrintReverseForDriverEMF()\n") );
				PrintReverseForDriverEMF( lstPages, bOdd );
			}
			else
			{
				DEBUG_EMF( _T("Using PrintReverseEMF()\n") );
				// Reverse printing and nup
				PrintReverseEMF( lstPages, bOdd );
			}
		}
	}
	else
	{
		DEBUG_EMF( _T("Using PrintBookletEMF()\n") );
		// Booklet Printing
		PrintBookletEMF();
	}
}

// Level 2 Print Functions
void CEMFJobHandler::PrintForwardEMF()
// Function Description:
//		PrintForwardEMF plays the EMF files in the order in which they were spooled.
//
// Parameters:
//
// Return Values:
{
	DWORD	dwLastPageNumber	= 1;
	DWORD	dwPageNumber;
	DWORD	dwRemainingCopies;

	DEBUG_EMF( _T("Begin PrintForwardEMF()\n") );
	// Keep printing as long as the spool file contains EMF handles.
	while ( dwLastPageNumber )
	{
		DEBUG_EMF( _T("LastPageNumber = %d\n"), dwLastPageNumber );

		// If the print processor is paused, wait for it to be resumed 
		if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
		{
			::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
		}

		dwPageNumber = dwLastPageNumber;
		if ( m_bCollate )
		{
		   dwLastPageNumber = PrintOneSideForwardEMF( dwPageNumber );
		}
		else
		{
			dwRemainingCopies = m_dwJobNumberOfCopies;
			while ( dwRemainingCopies > 0 )
			{
				DEBUG_EMF( _T("PrintForwardEMF() RemainingCopies = %d, JobCopies = %d, DrvCopies = %d\n"), dwRemainingCopies, m_dwJobNumberOfCopies, m_dwDrvNumberOfCopies );
				if ( dwRemainingCopies <= m_dwDrvNumberOfCopies )
				{
					SetDriverCopies( dwRemainingCopies );
					dwRemainingCopies = 0;
				}
				else
				{
					SetDriverCopies( m_dwDrvNumberOfCopies );
					dwRemainingCopies -= m_dwDrvNumberOfCopies;
				}

				// If the print processor is paused, wait for it to be resumed 
				if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
				{
					::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
				}

				dwLastPageNumber = PrintOneSideForwardEMF( dwPageNumber );
			}
		}
	}

	DEBUG_EMF( _T("End PrintForwardEMF()\n") );
}

void CEMFJobHandler::PrintReverseForDriverEMF( CPageNumberList& lstPages, bool& bOdd )
// Function Description:
//		PrintReverseForDriverEMF plays the EMF pages in the reverse order
//		for the driver which does the Nup transformations.
//
// Parameters:
//		lstPages					-> Reference to a linked list of pages
//		bOdd						-> Whether or not to print Odd pages
//
{
	DWORD		dwPageNumber;
	DWORD		dwRemainingCopies;
	POSITION	pos					= lstPages.GetHeadPosition();

	DEBUG_EMF( _T("Begin PrintReverseForDriverEMF()\n") );
	// Select the correct page for duplex printing
	if ( m_bDuplex && !bOdd )
	{
		if ( pos != NULL )
		{
			// Skip to the next page
			lstPages.GetNext(pos);
		}
	}

	// Play the sides in reverse order
	while ( pos != NULL )
	{
		// If the print processor is paused, wait for it to be resumed
		if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
		{
			::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
		}
		
		// Set the page number
		dwPageNumber = lstPages.GetNext(pos);

		if ( m_bCollate )
		{
			PrintOneSideReverseForDriverEMF( dwPageNumber );
		}
		else
		{
			dwRemainingCopies = m_dwJobNumberOfCopies;

			while( dwRemainingCopies > 0 )
			{
				DEBUG_EMF( _T("PrintReverseForDriverEMF() RemainingCopies = %d, JobCopies = %d, DrvCopies = %d\n"), dwRemainingCopies, m_dwJobNumberOfCopies, m_dwDrvNumberOfCopies );
				if ( dwRemainingCopies <= m_dwDrvNumberOfCopies )
				{
					SetDriverCopies( dwRemainingCopies );
					dwRemainingCopies = 0;
				}
				else
				{
					SetDriverCopies( m_dwDrvNumberOfCopies );
					dwRemainingCopies -= m_dwDrvNumberOfCopies;
				}

				PrintOneSideReverseForDriverEMF( dwPageNumber );
			}
		}

		// Go to the next page for duplex printing
		if ( m_bDuplex && (pos != NULL) )
		{
			lstPages.GetNext( pos );
		}
	}

	DEBUG_EMF( _T("End PrintReverseForDriverEMF()\n") );
}

void CEMFJobHandler::PrintReverseEMF( CPageNumberList& lstPages, bool& bOdd )
// Function Description:
//		PrintReverseEMF plays the EMF pages in the reverse order and also
//		performs nup transformations.
//
// Parameters:
//		lstPages					-> Reference to a linked list of pages
//		bOdd						-> Whether or not to print Odd pages
//
{
	DWORD		dwRemainingCopies;
	DWORD		dwStartPage1;
	DWORD		dwStartPage2;
	DWORD		dwEndPage1;
	DWORD		dwEndPage2;
	POSITION	pos					= lstPages.GetHeadPosition();
	bool		bReturnMe			= false;

	DEBUG_EMF( _T("Begin PrintReverseEMF()\n") );
	if ( pos != NULL )
	{
		// Set the start and end page numbers for duplex and regular printing
		if ( m_bDuplex )
		{
			if ( bOdd )
			{
				dwStartPage1	= lstPages.GetAt(pos);
				dwEndPage1		= m_dwTotalNumberOfPages;
				dwStartPage2	= m_dwTotalNumberOfPages+1;
				dwEndPage2		= 0;
			}
			else
			{
				dwStartPage2	= lstPages.GetAt(pos);
				dwEndPage2		= m_dwTotalNumberOfPages;

				lstPages.GetNext(pos);
				if ( pos != NULL )
				{
					dwStartPage1	= lstPages.GetAt(pos);
					dwEndPage1		= dwStartPage2 - 1;
				}
			}
		}
		else
		{
			dwStartPage1	= lstPages.GetAt(pos);
			dwEndPage1		= m_dwTotalNumberOfPages;
			dwStartPage2	= 0;
			dwEndPage2		= 0;
		}

		while( pos != NULL )
		{
			// If the print processor is paused, wait for it to be resumed 
			if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
			{
				::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
			}
			 
			if ( m_bCollate )
			{
				PrintOneSideReverseEMF( dwStartPage1, dwEndPage1, dwStartPage2, dwEndPage2 );
			}
			else
			{
				dwRemainingCopies = m_dwJobNumberOfCopies;
				while( dwRemainingCopies > 0 )
				{
					DEBUG_EMF( _T("PrintReverseEMF() RemainingCopies = %d, JobCopies = %d, DrvCopies = %d\n"), dwRemainingCopies, m_dwJobNumberOfCopies, m_dwDrvNumberOfCopies );
					if ( dwRemainingCopies <= m_dwDrvNumberOfCopies )
					{
						SetDriverCopies( dwRemainingCopies );
						dwRemainingCopies = 0;
					}
					else
					{
						SetDriverCopies( m_dwDrvNumberOfCopies );
						dwRemainingCopies -= m_dwDrvNumberOfCopies;
					}

					PrintOneSideReverseEMF( dwStartPage1, dwEndPage1, dwStartPage2, dwEndPage2 );
				}
			}

			if ( m_bDuplex )
			{
				POSITION posNext		= pos;
				POSITION posNextNext	= NULL;

				lstPages.GetNext(posNext);
				if ( posNext != NULL )
				{
					posNextNext = posNext;
					lstPages.GetNext(posNextNext);
				}
				if ( (posNext != NULL) && (posNextNext != NULL) )
				{
					dwEndPage2		= lstPages.GetAt(pos) - 1;
					lstPages.GetNext(pos);
					dwStartPage2	= lstPages.GetAt(pos);
					dwEndPage1		= dwStartPage2 - 1;
					lstPages.GetNext(pos);
					dwStartPage1	= lstPages.GetAt(pos);
				}
				else
				{
					break;
				}
			}
			else
			{
				lstPages.GetNext(pos);
				if ( pos != NULL)
				{
					dwEndPage1		= dwStartPage1 - 1;
					dwStartPage1	= lstPages.GetAt(pos);
				}
			}
		}
	}
	DEBUG_ELSE_TEXT( _T("PageList is empty!\n") );

	DEBUG_EMF( _T("End PrintReverseEMF()\n") );
}

void CEMFJobHandler::PrintBookletEMF()
// Function Description:
//		PrintBookletEMF prints the job in 2-up in booklet form.
//
{
	DWORD	dwTotalPrintPages;
	DWORD	dwNumberOfPhyPages;
	DWORD	dwRemainingCopies;
	DWORD	dwIndex;

	DEBUG_EMF( _T("Begin PrintBookletEMF()\n") );
	// Get closest multiple of 4 greater than dwTotalNumberOfPages
	dwTotalPrintPages = m_dwTotalNumberOfPages - (m_dwTotalNumberOfPages % 4);
	if ( dwTotalPrintPages != m_dwTotalNumberOfPages )
	{
		dwTotalPrintPages += 4;
	}
	dwNumberOfPhyPages = (DWORD) dwTotalPrintPages / 4;

	for ( dwIndex = 0; dwIndex < dwNumberOfPhyPages; ++dwIndex )
	{
		// If the print processor is paused, wait for it to be resumed 
		if ( m_pPrintProcessorData->fsStatus & PRINTPROCESSOR_PAUSED )
		{
			::WaitForSingleObject( m_pPrintProcessorData->semPaused, INFINITE );
		}

		if ( m_bCollate )
		{
			PrintOneSideBookletEMF(	dwTotalPrintPages, dwIndex * 2 + 1 );
		}
		else
		{
			dwRemainingCopies = m_dwJobNumberOfCopies;
			while ( dwRemainingCopies > 0 )
			{
				DEBUG_EMF( _T("PrintBookletEMF() RemainingCopies = %d, JobCopies = %d, DrvCopies = %d\n"), dwRemainingCopies, m_dwJobNumberOfCopies, m_dwDrvNumberOfCopies );
				if ( dwRemainingCopies <= m_dwDrvNumberOfCopies )
				{
					SetDriverCopies( dwRemainingCopies );
					dwRemainingCopies = 0;
				}
				else
				{
					SetDriverCopies( m_dwDrvNumberOfCopies );
					dwRemainingCopies -= m_dwDrvNumberOfCopies;
				}

				PrintOneSideBookletEMF(	dwTotalPrintPages, dwIndex * 2 + 1 );
			}
		}
	}

	DEBUG_EMF( _T("End PrintBookletEMF()\n") );
}

// Level 3 Print Functions
DWORD CEMFJobHandler::PrintOneSideForwardEMF( DWORD dwPageNumber )
// Function Description:
//		PrintOneSideForwardEMF plays the next physical page in the same order
//		as the spool file.
//
// Parameters:
//		dwPageNumber			-> Starting Page Number
//		pDevMode				-> Devmode with resolution settings
// 
// Return Values:
//		Last Page Number if successful
//		0 on Job completion
{
	bool				bComplete	= false;
	DWORD				dwReturnMe	= 0;
	LPDEVMODE			pLastDM		= NULL;
	LPDEVMODE			pCurrentDM	= NULL;
	HANDLE				hEMF		= NULL;
	DWORD				dwPageIndex;
	DWORD				dwPageType;
	DWORD				dwSides;
	bool				bNewDevMode;
	DWORD				cPagesToPlay;

	// Set the number of sides on this page;
	dwSides = m_bDuplex ? 2 : 1;

	DEBUG_EMF( _T("Begin PrintOneSideForwardEMF()  PageNumber = %d\n"), dwPageNumber );
	while ( (dwSides > 0) && (bComplete == false) )
	{
		// Loop for a single side
		for ( dwPageIndex = 1 ; dwPageIndex <= m_dwNumberOfPagesPerSide ; ++dwPageIndex, ++dwPageNumber )
		{
			hEMF = GdiGetPageHandle( m_hSpoolFile, dwPageNumber, &dwPageType );
			if ( hEMF == NULL )
			{
				if ( ::GetLastError() == ERROR_NO_MORE_ITEMS )
				{
					// End of the print job
					bComplete = true;
					break;
				}

				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiGetPageHandle failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetPageHandle );
			}

			// Process new devmodes in the spool file that appear before this page
			ResetDCForNewDevMode( dwPageNumber, (dwPageIndex != 1), bNewDevMode );

			// Reset page index if new page was started
			if ( bNewDevMode )
			{
				dwPageIndex = 1;
			}

			// Call StartPage for each new page
			if ( (dwPageIndex == 1) && (GdiStartPageEMF(m_hSpoolFile) == FALSE) )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
			}

			PlayEMFPage( hEMF, dwPageNumber, dwPageIndex, EMF_DEGREE_90 );
		}

		// Explaination of the scenario set for the conditions on 
		// dwPageIndex1 , pbComplete and bDuplex.
		// N.B. we are naming them cond.1 and cond.2
		//	  dwPageIndex!=1	pbComplete	 bDuplex	Condition
		//			0				0			0		None	
		//			0				0			1		None
		//			0				1			0		None
		//			0				1			1		Cond2 on Second Side i.e. dwsides==1
		//			1				0			0		Cond1
		//			1				0			1		Cond1
		//			1				1			0		Cond1
		//			1				1			1		Cond1 & Cond2 on First Side i.e. dwsides==2
		//

		// cond.1
		if ( dwPageIndex != 1 )
		{
			// Call EndPage if we played any pages
			if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideForwardEMF\n") );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
			}
		}

		// cond.2
		// play empty page on the back of duplex
		if ( (bComplete == true) && m_bDuplex && (m_dwDrvNumberOfPagesPerSide == 1) )
		{
			DEBUG_TEXT( _T("PCL or PS with no N-up\n") );

			// Checking dwsides against 2 or 1. 
			// depends on whether it is n-up or not.
			if ( (dwPageIndex != 1) ? (dwSides == 2) : (dwSides == 1) )
			{
				if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
				}

				if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideForwardEMF\n") );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
				}
			}
		}

		// Do the next side
		dwSides--;
	}


	if ( (bComplete == true) && (m_dwNumberOfPagesPerSide == 1) && (m_dwDrvNumberOfPagesPerSide != 1) && m_bDuplex && 
		((dwPageNumber-1)%(2*m_dwDrvNumberOfPagesPerSide)) )
	{
		// Number of pages played on last physical page
		cPagesToPlay = 2*m_dwDrvNumberOfPagesPerSide - (dwPageNumber-1)%(2*m_dwDrvNumberOfPagesPerSide);

		DEBUG_TEXT( _T("PS with N-up!  Must fill in %u pages\n"), cPagesToPlay );
		for ( ; cPagesToPlay ; cPagesToPlay-- ) 
		{
			if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
			}

			if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideForwardEMF\n") );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
			}
		}
	}

	if ( !bComplete )
	{
		dwReturnMe = dwPageNumber;
	}
	 
	DEBUG_EMF( _T("End PrintOneSideForwardEMF()  ReturnValue = %d\n"), dwReturnMe );
	return dwReturnMe;
}

void CEMFJobHandler::PrintOneSideReverseForDriverEMF( DWORD dwPageNumber )
// Function Description:
//		PrintOneSideReverseForDriverEMF plays the EMF pages on the next
//		physical page, in the reverse order for the driver which does the
//		Nup transformations.
//
// Parameters:
//		dwPageNumber				-> Page number to start the side
//
{
	DWORD		dwPageIndex;
	DWORD		dwPageType;
	DWORD		dwSides;
	DWORD		dwLimit;
	bool		bNewDevMode	= false;
	bool		bSmart		= false;
	LPDEVMODE	pLastDM		= NULL;
	LPDEVMODE	pCurrentDM	= NULL;
	HANDLE		hEMF		= NULL;

	DEBUG_EMF( _T("Begin PrintOneSideReverseForDriverEMF()\n") );

	dwSides = m_bDuplex ? 2 : 1;

	// If the document will fit on one physical page, then this variable will prevent 
	// the printer from playing extra pages just to fill in one phisical page 
	// The exception is when the pages fit on a single phisical page, but they must
	// be collated. Then because of design, the printer will also draw borders for the
	// empty pages which are played so that the page gets ejected.
	bSmart =  ( (m_dwTotalNumberOfPages <= m_dwDrvNumberOfPagesPerSide) && (m_pDevMode->dmCollate != DMCOLLATE_TRUE) );
		 
	for (; dwSides; --dwSides)
	{
		// This loop may play some empty pages in the last side, since the
		// driver is doing nup and it does not keep count of the page numbers
		dwPageIndex	= bSmart ? dwPageNumber : 1;
		dwLimit		= bSmart ? m_dwTotalNumberOfPages : m_dwDrvNumberOfPagesPerSide;

		for ( ; dwPageIndex <= dwLimit;  ++dwPageIndex, ++dwPageNumber )
		{
			if ( bSmart || (dwPageNumber <= m_dwTotalNumberOfPages) )
			{
				hEMF = GdiGetPageHandle( m_hSpoolFile, dwPageNumber, &dwPageType );
				if ( hEMF == NULL )
				{
					DEBUG_TEXT( _T("GdiGetPageHandle failed on Printer %s\n"), m_pDevMode->dmDeviceName );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetPageHandle );
				}

				// Process new devmodes in the spoolfile
				ResetDCForNewDevMode( dwPageNumber, false, bNewDevMode );
			}

			// Call StartPage for each new page
			if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
			}

			if ( bSmart || (dwPageNumber <= m_dwTotalNumberOfPages) )
			{
				PlayEMFPage( hEMF, dwPageNumber, 1, EMF_DEGREE_90 );
			}

			if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideForwardEMF\n") );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
			}
		}
	}

	DEBUG_EMF( _T("End PrintOneSideReverseForDriverEMF()\n") );
}

void CEMFJobHandler::PrintOneSideReverseEMF( const DWORD& dwStartPage1, const DWORD& dwEndPage1, const DWORD& dwStartPage2, const DWORD& dwEndPage2 )
// Function Description:
//		PrintOneSideReverseEMF plays the EMF pages for the next physical page.
//
// Parameters:
//		dwStartPage1				-> Page number of the first EMF page on 1st side
//		dwEndPage1					-> Page number of the last EMF page on 1st side
//		dwStartPage2				-> Page number of the first EMF page on 2nd side
//		dwEndPage2					-> Page number of the last EMF page on 2nd side
//
{
	DWORD		dwPageNumber;
	DWORD		dwPageIndex;
	DWORD		dwPageType;
	DWORD		dwEndPage;
	DWORD		dwStartPage;
	DWORD		dwSides;
	bool		bNewDevMode	= false;
	LPDEVMODE	pLastDM		= NULL;
	LPDEVMODE	pCurrentDM	= NULL;
	HANDLE		hEMF		= NULL;

	DEBUG_EMF( _T("Begin PrintOneSideReverseEMF()\n") );

	dwSides = m_bDuplex ? 2 : 1;
	for (; (dwSides > 0); --dwSides)
	{
		if ( m_bDuplex && (dwSides == 1) )
		{
			dwStartPage	= dwStartPage2;
			dwEndPage	= dwEndPage2;
		}
		else
		{
			dwStartPage = dwStartPage1;
			dwEndPage = dwEndPage1;
		}

		for ( dwPageNumber = dwStartPage, dwPageIndex = 1; dwPageNumber <= dwEndPage; ++dwPageNumber, ++dwPageIndex)
		{
			hEMF = GdiGetPageHandle( m_hSpoolFile, dwPageNumber, &dwPageType );
			if ( hEMF == NULL )
			{
				DEBUG_TEXT( _T("GdiGetPageHandle failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetPageHandle );
			}

			if ( dwPageIndex == 1 )
			{
				ResetDCForNewDevMode( dwPageNumber, false, bNewDevMode );

				// Call StartPage for each new page
				if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
				}
			}

			PlayEMFPage( hEMF, dwPageNumber, dwPageIndex, EMF_DEGREE_90 );
		}

		if ( dwPageIndex == 1 )
		{
			if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
			}
		}

		if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideReverseEMF\n") );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
		}
	}

	DEBUG_EMF( _T("End PrintOneSideReverseEMF()\n") );
}

void CEMFJobHandler::PrintOneSideBookletEMF( const DWORD& dwTotalPrintPages, const DWORD& dwStartPage )
// Function Description:
//		PrintOneSideBookletEMF prints one page of the booklet job.
//
// Parameters:
//		dwTotalPrintPages			-> Number of pages to printed (multiple of 4)
//		dwStartPage 				-> Number of the starting page for the side
//
{
	DWORD		dwPageArray[4];
	DWORD		dwPageIndex;
	DWORD		dwAngle;
	DWORD		dwPageType;
	DWORD		dwLastPage;
	DWORD		dwPagesPrinted	= 0;
	HANDLE		hEMF			= NULL;
	LPDEVMODE	pLastDM			= NULL;
	LPDEVMODE	pCurrentDM		= NULL;
	bool		bNewDevMode		= false;

	DEBUG_EMF( _T("Begin PrintOneSideBookletEMF()\n") );

	// Set the order of the pages
	if ( m_bReverseOrderPrinting )
	{
		dwPageArray[0] = dwStartPage + 1;
		dwPageArray[1] = dwTotalPrintPages - dwStartPage;
		if ( m_dwDuplexMode == EMF_DUP_VERT )
		{
			dwPageArray[2] = dwStartPage;
			dwPageArray[3] = dwPageArray[1] + 1;
		}
		else
		{
			// EMF_DUP_HORZ
			dwPageArray[3] = dwStartPage;
			dwPageArray[2] = dwPageArray[1] + 1;
		}
	}
	else
	{
		dwPageArray[1] = dwStartPage;
		dwPageArray[0] = dwTotalPrintPages - dwStartPage + 1;
		if ( m_dwDuplexMode == EMF_DUP_VERT )
		{
			dwPageArray[2] = dwPageArray[0] - 1;
			dwPageArray[3] = dwPageArray[1] + 1;
		}
		else
		{
			// EMF_DUP_HORZ
			dwPageArray[2] = dwPageArray[1] + 1;
			dwPageArray[3] = dwPageArray[0] - 1;
		}
	}

	// Set page number for ResetDC
	dwLastPage = (m_dwTotalNumberOfPages < dwPageArray[0]) ? m_dwTotalNumberOfPages : dwPageArray[0];

	// Process devmodes in the spool file
	ResetDCForNewDevMode( dwLastPage, false, bNewDevMode );

	while (dwPagesPrinted < 4)
	{
		for (dwPageIndex = 1 ; dwPageIndex <= m_dwNumberOfPagesPerSide ; ++dwPageIndex, ++dwPagesPrinted)
		{
			if ( dwPageArray[dwPagesPrinted] <= m_dwTotalNumberOfPages )
			{
				hEMF = GdiGetPageHandle( m_hSpoolFile, dwPageArray[dwPagesPrinted], &dwPageType );
				if ( hEMF == NULL )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("GdiGetPageHandle failed on Printer %s\n"), m_pDevMode->dmDeviceName );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetPageHandle );
				}
			}

			if (dwPageIndex == 1)
			{
				if ( GdiStartPageEMF(m_hSpoolFile) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("StartPage failed on Printer %s\n"), m_pDevMode->dmDeviceName );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiStartPageEMF );
				}
			}

			if (dwPageArray[dwPagesPrinted] <= m_dwTotalNumberOfPages)
			{
				if ( (m_dwDuplexMode == EMF_DUP_VERT) && (dwPagesPrinted > 1) )
				{
					dwAngle = EMF_DEGREE_270;
				}
				else
				{
					// EMF_DUP_HORZ or 1st side
					dwAngle = EMF_DEGREE_90;
				}
  
				PlayEMFPage( hEMF, dwPageArray[dwPagesPrinted], dwPageIndex, dwAngle );
			}

			if (dwPageIndex == m_dwNumberOfPagesPerSide)
			{
				if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("GdiEndPageEMF failed inside of PrintOneSideReverseEMF\n") );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
				}
			}
		}
	}

	DEBUG_EMF( _T("End PrintOneSideBookletEMF()\n") );
}

//////////////////////////////////////////////////////////////////////
// Helper Functions
//////////////////////////////////////////////////////////////////////

void CEMFJobHandler::SetDriverCopies( const DWORD& dwNumberOfCopies )
// Function Description:
//		SetDriverCopies sets the dmCopies field in pDevMode and resets
//		m_hPrinterDC with this devmode
//
// Parameters:
//		dwNumberOfCopies		-> Value for dmCopies
//
{
	if ( !(m_pCopyDM->dmFields & DM_COPIES) || (m_pCopyDM->dmCopies != (short)dwNumberOfCopies) )
	{
		DWORD	dmFields;

		// Save the old fields structure
		dmFields = m_pCopyDM->dmFields;
		m_pCopyDM->dmFields |= DM_COPIES;
		m_pCopyDM->dmCopies = (short)dwNumberOfCopies;

		THROW_WIN_FAIL( ::ResetDC(m_hPrinterDC, m_pCopyDM) );

		// Restore the fields structure
		m_pCopyDM->dmFields = dmFields;

		// Make sure the graphics mode is still on advanced
		m_Xform.SetGraphicsMode( GM_ADVANCED );

		// Display Debug Information
		DEBUG_EMF( _T("Set Driver Copies to %d\n"), dwNumberOfCopies );
	}
	DEBUG_ELSE_TEXT( _T("Copies already set!\n") );
}

void CEMFJobHandler::GetStartPageList( CPageNumberList& lstPages, bool& bOdd )
// Function Description:
//		GetStartPageList generates a list of the page numbers which
//		should appear on the start of each side of the job. This takes
//		into consideration the ResetDC calls that may appear before the
//		end of the page. The list generated by GetStartPageList is used
//		to play the job in reverse order.
//
// Parameters:
//		lstPages				-> Reference to a linked list of pages
//		bOdd					-> Whether or not to print Odd pages
//
{
	bool		bCheckDevMode	= bool(m_dwNumberOfPagesPerSide != 1);
	DWORD		dwPageIndex		= 0;
	DWORD		dwPageNumber	= 1;
	LPDEVMODE	pCurrentDM		= NULL;
	LPDEVMODE	pLastDM			= NULL;

	DEBUG_EMF( _T("Begin GetStartPageList()\n") );
	while ( dwPageNumber <= m_dwTotalNumberOfPages )
	{
		for (dwPageIndex = 1;
			( (dwPageIndex <= m_dwNumberOfPagesPerSide) && (dwPageNumber <= m_dwTotalNumberOfPages) );
			++dwPageIndex, ++dwPageNumber)
		{
			if ( bCheckDevMode )
			{
				// Check if the devmode has changed requiring a new page
				if ( GdiGetDevmodeForPage(m_hSpoolFile, dwPageNumber, &pCurrentDM, NULL) == FALSE )
				{
					DEBUG_LAST_ERROR();
					DEBUG_TEXT( _T("GdiGetDevmodeForPage failed in GetStartPageList\n") );
					throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetDevmodeForPage );
				}

				if (dwPageIndex == 1)
				{
					// Save the DevMode for the first page on a side
					pLastDM = pCurrentDM;
				}
				else
				{
					// If the DevMode changes in a side, start a new page
					if ( DifferentDevModes(pCurrentDM, pLastDM) )
					{
						dwPageIndex = 1;
						pLastDM = pCurrentDM;
					}
				}
			}

			// Create a node for the start of a side
			if ( dwPageIndex == 1 )
			{
				lstPages.AddHead( dwPageNumber );

				// Flip the bOdd flag
				bOdd = !bOdd;
			}
		}
	}

	DEBUG_EMF( _T("End GetStartPageList()\n") );
}

void CEMFJobHandler::GetPageCoordinatesForNUp( RECT* pRectDocument, RECT* pRectBorder, UINT nCurrentPageNumber, bool& bRotate )
// Function Description:
//		GetPageCoordinatesForNUp computes the rectangle on the Page where the
//		EMF file is to be played. It also determines if the picture is to
//		rotated.
//
// Parameters:
//		*rectDocument				-> Pointer to RECT where the coordinates to play the page will be returned.
//		*rectBorder					-> Pointer to RECT where the page borders are to drawn.
//		nCurrentPageNumber			-> 1 based page number on the side.
//		bRotate						-> Reference to bool which indicates if the picture must be	rotated.
{

	UpdateRect*	URect;
	LONG		lXPrintPage,lYPrintPage,lXPhyPage,lYPhyPage,lXFrame,lYFrame,ltemp,ldX,ldY;
	LONG		lXNewPhyPage,lYNewPhyPage,lXOffset,lYOffset,lNumRowCol,lRowIndex,lColIndex;
	double		dXleft,dXright,dYtop,dYbottom;
	LONG		xResolution = GetDeviceCaps(m_hPrinterDC, LOGPIXELSX);
	LONG		yResolution = GetDeviceCaps(m_hPrinterDC, LOGPIXELSY);
	
	//DEBUG_EMF( _T("Begin GetPageCoordinatesForNUp()\n") );

	// Get the 0-based array index for the current page
	nCurrentPageNumber--;

	if ( (m_dwNumberOfPagesPerSide == 1) || (xResolution == yResolution) ) 
	{
		xResolution = yResolution = 1;
	}

	pRectDocument->top	= pRectDocument->bottom	= lYPrintPage = (GetDeviceCaps(m_hPrinterDC, DESKTOPVERTRES)-1) * xResolution;
	pRectDocument->left	= pRectDocument->right	= lXPrintPage = (GetDeviceCaps(m_hPrinterDC, DESKTOPHORZRES)-1) * yResolution;

	lXPhyPage = GetDeviceCaps(m_hPrinterDC, PHYSICALWIDTH)  * yResolution;
	lYPhyPage = GetDeviceCaps(m_hPrinterDC, PHYSICALHEIGHT) * xResolution;

	bRotate = false;

	// Select the array containing the update factors
	switch ( m_dwNumberOfPagesPerSide )
	{
	case 1:
		pRectDocument->top		= 0;
		pRectDocument->left		= 0;
		pRectDocument->right	+= 1;
		pRectDocument->bottom	+= 1;
		return;
		break;

	case 2:
		if (lXPrintPage > lYPrintPage)
		{
			// Cut vertically
			URect = m_URect22;
			lXFrame = (LONG) (lXPrintPage / 2.0);
			lYFrame = lYPrintPage;
		}
		else
		{
			// Cut horizontally
			URect = m_URect21;
			lYFrame = (LONG) (lYPrintPage / 2.0);
			lXFrame = lXPrintPage;
		}
		break;


	case 4:
		URect = m_URect4;
		lXFrame = (LONG) (lXPrintPage / 2.0);
		lYFrame = (LONG) (lYPrintPage / 2.0);
		break;

	case 6:
		if ( lXPrintPage > lYPrintPage )
		{
			// Cut vertically twice
			URect = m_URect62;
			lXFrame = (LONG) (lXPrintPage / 3.0);
			lYFrame = (LONG) (lYPrintPage / 2.0);
		}
		else
		{
			// Cut horizontally twice
			URect = m_URect61;
			lYFrame = (LONG) (lYPrintPage / 3.0);
			lXFrame = (LONG) (lXPrintPage / 2.0);
		}
		break;

	case 9:
		URect = m_URect9;
		lXFrame = (LONG) (lXPrintPage / 3.0);
		lYFrame = (LONG) (lYPrintPage / 3.0);
		break;

	case 16:
		URect = m_URect16;
		lXFrame = (LONG) (lXPrintPage / 4.0);
		lYFrame = (LONG) (lYPrintPage / 4.0);
		break;

	default:
		// Should Not Occur.
		DEBUG_TEXT( _T("Invalid NumberOfPagesPerSide in GetPageCoordinatesForNUp\n") );
		throw EMF_EXCEPTION( CEMFException::exEMF_InvalidNUpValue );
		return;
	}

	// Set the flag if the picture has to be rotated
	bRotate = !((lXPhyPage >= lYPhyPage) && (lXFrame >= lYFrame)) && !((lXPhyPage < lYPhyPage) && (lXFrame < lYFrame));
	
	// If the picture is to be rotated, modify the rectangle selected.
	if ( (m_dwNumberOfPagesPerSide == 2) || (m_dwNumberOfPagesPerSide == 6) )
	{
		if ( bRotate )
		{
			switch ( m_dwNumberOfPagesPerSide )
			{
			case 2:
				if (lXPrintPage <= lYPrintPage)
				{
					URect = m_URect21R;
				} // URect22 = URect22R
				break;

			case 6:
				if (lXPrintPage <= lYPrintPage)
				{
					URect = m_URect61R;
				}
				else
				{
					URect = m_URect62R;
				}
				break;
			}
		}
	}
	else
	{
		if ( bRotate )
		{
			// Get the number of rows/columns. switch is faster than sqrt.
			switch( m_dwNumberOfPagesPerSide )
			{
			case 4:			lNumRowCol = 2;				break;
			case 9:			lNumRowCol = 3;				break;
			case 16:		lNumRowCol = 4;				break;
			}

			lRowIndex  = (LONG) (nCurrentPageNumber / lNumRowCol);
			lColIndex  = (LONG) (nCurrentPageNumber % lNumRowCol);

			nCurrentPageNumber = (lNumRowCol - 1 - lColIndex) * lNumRowCol + lRowIndex;
		}
	}

	// Update the Page Coordinates.
	pRectDocument->top		= (LONG) (pRectDocument->top	* URect[nCurrentPageNumber].top);
	pRectDocument->bottom	= (LONG) (pRectDocument->bottom * URect[nCurrentPageNumber].bottom);
	pRectDocument->left		= (LONG) (pRectDocument->left	* URect[nCurrentPageNumber].left);
	pRectDocument->right	= (LONG) (pRectDocument->right	* URect[nCurrentPageNumber].right);

	// If the page border has to drawn, return the corresponding coordinates in rectBorder.
	if ( m_dwNupBorderFlags == BORDER_PRINT)
	{
		pRectBorder->top	= pRectDocument->top/xResolution;
		pRectBorder->bottom	= pRectDocument->bottom/xResolution - 1;
		pRectBorder->left	= pRectDocument->left/yResolution;
		pRectBorder->right	= pRectDocument->right/yResolution - 1;
	}

	if ( bRotate )
	{
		ltemp = lXFrame; lXFrame = lYFrame; lYFrame = ltemp;
	}

	// Get the new size of the rectangle to keep the X/Y ratio constant.
	if ( ((LONG) (lYFrame*((lXPhyPage*1.0)/lYPhyPage))) >= lXFrame)
	{
		ldX = 0;
		ldY = lYFrame - ((LONG) (lXFrame*((lYPhyPage*1.0)/lXPhyPage)));
	}
	else
	{
		ldY = 0;
		ldX = lXFrame - ((LONG) (lYFrame*((lXPhyPage*1.0)/lYPhyPage)));
	}

	// Adjust the position of the rectangle.
	if ( bRotate )
	{
		if (ldX)
		{
			pRectDocument->bottom	-= (LONG) (ldX / 2.0);
			pRectDocument->top		+= (LONG) (ldX / 2.0);
		}
		else
		{
			pRectDocument->right	-= (LONG) (ldY / 2.0);
			pRectDocument->left		+= (LONG) (ldY / 2.0);
		}
	}
	else
	{
		if (ldX)
		{
			pRectDocument->left		+= (LONG) (ldX / 2.0);
			pRectDocument->right	-= (LONG) (ldX / 2.0);
		}
		else
		{
			pRectDocument->top		+= (LONG) (ldY / 2.0);
			pRectDocument->bottom	-= (LONG) (ldY / 2.0);
		}
	}

	// Adjust to get the Printable Area on the rectangle
	lXOffset = GetDeviceCaps(m_hPrinterDC, PHYSICALOFFSETX) * yResolution;
	lYOffset = GetDeviceCaps(m_hPrinterDC, PHYSICALOFFSETY) * xResolution;

	dXleft = ( lXOffset * 1.0) / lXPhyPage;
	dYtop  = ( lYOffset * 1.0) / lYPhyPage;
	dXright =  ((lXPhyPage - (lXOffset + lXPrintPage)) * 1.0) / lXPhyPage;
	dYbottom = ((lYPhyPage - (lYOffset + lYPrintPage)) * 1.0) / lYPhyPage;

	lXNewPhyPage = pRectDocument->right	- pRectDocument->left;
	lYNewPhyPage = pRectDocument->bottom - pRectDocument->top;

	if ( bRotate )
	{
		ltemp = lXNewPhyPage; lXNewPhyPage = lYNewPhyPage; lYNewPhyPage = ltemp;

		pRectDocument->left		+= (LONG) (dYtop	* lYNewPhyPage);
		pRectDocument->right	-= (LONG) (dYbottom * lYNewPhyPage);
		pRectDocument->top		+= (LONG) (dXright	* lXNewPhyPage);
		pRectDocument->bottom	-= (LONG) (dXleft	* lXNewPhyPage);
	}
	else
	{
		pRectDocument->left		+= (LONG) (dXleft	* lXNewPhyPage);
		pRectDocument->right	-= (LONG) (dXright	* lXNewPhyPage);
		pRectDocument->top		+= (LONG) (dYtop	* lYNewPhyPage);
		pRectDocument->bottom	-= (LONG) (dYbottom * lYNewPhyPage);
	}

	if ( xResolution != yResolution )
	{
		pRectDocument->left		= pRectDocument->left	/ yResolution;
		pRectDocument->right	= pRectDocument->right	/ yResolution; 
		pRectDocument->top		= pRectDocument->top	/ xResolution;
		pRectDocument->bottom	= pRectDocument->bottom	/ xResolution; 
	}
	
	//DEBUG_EMF( _T("End GetPageCoordinatesForNUp()\n") );
}

void CEMFJobHandler::PlayEMFPage( HANDLE hEMF, const DWORD& dwPageNumber, const DWORD& dwPageIndex, const DWORD& dwAngle )
// Function Description:
//		PlayEMFPage plays the EMF in the appropriate rectangle. It performs
//		the required scaling, rotation and translation.
//
// Parameters:
//		hEMF						-> Handle to the contents of the page in the spool file
//		dwPageNumber				-> Page number in the document
//		dwPageIndex					-> Page number in the side. (1 based)
//		dwAngle						-> Angle for rotation (if neccesary)
{
	RECT		rectBorder		= { -1, -1, -1, -1 };
	XFORM		TransXForm		= { 1, 0, 0, 1, 0, 0 };
	XFORM		RotateXForm		= { 0, -1, 1, 0, 0, 0 };
	RECT* 		prectClip		= NULL;
	bool 		bRotate;
	RECT 		rectDocument;
	RECT		rectPrinter;

	//DEBUG_EMF( _T("Begin PlayEMFPage()\n") );

	// Compute the rectangle for one page.
	GetPageCoordinatesForNUp( &rectDocument, &rectBorder, dwPageIndex, bRotate );

	if ( dwAngle == EMF_DEGREE_270 )
	{
		RotateXForm.eM12 = 1;
		RotateXForm.eM21 = -1;
	} // EMF_DEGREE_90 case is the initialization

	if ( bRotate )
	{
		rectPrinter.top		= 0;
		rectPrinter.bottom	= rectDocument.right - rectDocument.left;
		rectPrinter.left	= 0;
		rectPrinter.right	= rectDocument.bottom - rectDocument.top;

		// Set the translation matrix
		if ( dwAngle == EMF_DEGREE_270 )
		{
			TransXForm.eDx = (float) rectDocument.right;
			TransXForm.eDy = (float) rectDocument.top;
		}
		else
		{
			// EMF_DEGREE_90
			TransXForm.eDx = (float) rectDocument.left;
			TransXForm.eDy = (float) rectDocument.bottom;
		}

		// Set the transformation matrix
		m_Xform.SetMatrix( &RotateXForm, &TransXForm, MWT_RIGHTMULTIPLY );
	}

	// Add clipping for Nup
	if ( m_dwNumberOfPagesPerSide != 1 )
	{
	   prectClip = &rectDocument;
	}

	// Print the page.
	if ( GdiPlayPageEMF(m_hSpoolFile, hEMF, (bRotate ? &rectPrinter : &rectDocument), &rectBorder, prectClip) == TRUE )
	{
		m_dwPagesPrinted++;
	}
	else
	{
		DEBUG_LAST_ERROR();
		DEBUG_TEXT( _T("GdiPlayPageEMF failed in PlayEMFPage\n") );
		throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiPlayPageEMF );
	}

	// Reset the transformation
	m_Xform.ModifyTransform( NULL, MWT_IDENTITY );

	DEBUG_EMF( _T("PlayEMFPage() : %d Pages Printed\n"), m_dwPagesPrinted );
}

bool CEMFJobHandler::DifferentDevModes( LPDEVMODE pDevMode1, LPDEVMODE pDevMode2 )
// Function Description:
//		Compares the devmodes for differences other than dmTTOption
//
// Parameters:
//		pDevMode1				-> Pointer to 1st DEVMODE to be compared
//		pDevMode2				-> Pointer to 2nd DEVMODE to be compared
//
// Return Values:
//		TRUE					-> DEVMODES are different
//		FALSE					-> DEVMODES are the same
{
	DWORD	dwSize1;
	DWORD	dwSize2;
	DWORD	dwTTOffset;
	DWORD	dwSpecOffset;
	DWORD	dwLogOffset;

	// Same pointers are the same devmode
	if ( pDevMode1 == pDevMode2 )
	{
		return false;
	}

	// Check for Null devmodes
	if ( (pDevMode1 == NULL) || (pDevMode2 == NULL) )
	{
		return true;
	}

	dwSize1 = pDevMode1->dmSize + pDevMode1->dmDriverExtra;
	dwSize2 = pDevMode2->dmSize + pDevMode2->dmDriverExtra;

	// Compare devmode sizes
	if ( dwSize1 != dwSize2 )
	{
		return true;
	}

	dwTTOffset		= FIELD_OFFSET(DEVMODE, dmTTOption);
	dwSpecOffset	= FIELD_OFFSET(DEVMODE, dmSpecVersion);
	dwLogOffset		= FIELD_OFFSET(DEVMODE, dmLogPixels);

	if ( _tcscmp(pDevMode1->dmDeviceName, pDevMode2->dmDeviceName) )
	{
		// Device names are different
		return true;
	}

	if ( (dwTTOffset < dwSpecOffset) ||	(dwSize1 < dwLogOffset) )
	{
		// Incorrent devmode offsets
		return true;
	}

	if ( memcmp((LPBYTE)pDevMode1 + dwSpecOffset, (LPBYTE)pDevMode2 + dwSpecOffset, dwTTOffset - dwSpecOffset) )
	{
		// Front half is different
		return true;
	}

	// Ignore the dmTTOption setting.
	if ( (pDevMode1->dmCollate != pDevMode2->dmCollate) || _tcscmp(pDevMode1->dmFormName, pDevMode2->dmFormName) )
	{
		// Form name or collate option is different
		return true;
	}

	if ( memcmp((LPBYTE) pDevMode1 + dwLogOffset, (LPBYTE) pDevMode2 + dwLogOffset, dwSize1 - dwLogOffset) )
	{
		// Back half is different
		return true;
	}

	// Return Failure
	return false;
}

void CEMFJobHandler::ResetDCForNewDevMode( const DWORD& dwPageNumber, bool bInsidePage, bool& bNewDevMode )
// Function Description:
//		Determines if the devmode for the page is different from the 
//		current devmode for the printer dc and resets the dc if necessary.
//		The parameters allow dmTTOption to be ignored in devmode comparison.
//
// Parameters:
//			dwPageNumber			-> Page number before which we search for the devmode
//			bInsidePage 			-> Flag to ignore changes in TT options and call EndPage before ResetDC
//			bNewDevMode				-> Reference to flag to indicate if ResetDC was called
{
	LPDEVMODE		pLastDM		= NULL;
	LPDEVMODE		pCurrentDM	= NULL;

	// Initialize OUT parameters
	bNewDevMode = false;

	// Get the DevMode just before the page
	if ( GdiGetDevmodeForPage(m_hSpoolFile, dwPageNumber, &pCurrentDM, &pLastDM) == FALSE )
	{
		DEBUG_LAST_ERROR();
		DEBUG_TEXT( _T("GdiGetDevmodeForPage failed inside of ResetDCForNewDevMode\n") );
		throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiGetDevmodeForPage );
	}

	// Check if the devmodes are different
	if ( pLastDM != pCurrentDM )
	{
		// If the pointers are different the devmodes are always different
		if ( !bInsidePage || DifferentDevModes(pLastDM, pCurrentDM) )
		{
			bNewDevMode = true;
		}
	}

	// Call ResetDC on the m_hPrinterDC if necessary
	if ( bNewDevMode )
	{
		if ( bInsidePage )
		{
			if ( GdiEndPageEMF(m_hSpoolFile, m_dwOptimization) == FALSE )
			{
				DEBUG_LAST_ERROR();
				DEBUG_TEXT( _T("GdiEndPageEMF failed inside of ResetDCForNewDevMode\n") );
				throw EMF_EXCEPTION( CEMFException::exEMF_FailGdiEndPageEMF );
			}
		}

		if ( pCurrentDM != NULL )
		{
			pCurrentDM->dmPrintQuality	= m_pCopyDM->dmPrintQuality;
			pCurrentDM->dmYResolution	= m_pCopyDM->dmYResolution;
		}

		DEBUG_EMF( _T("DC has been reset for new DEVMODE!\n") );

		// Do not throw an exception if these fail
		DEBUG_FAIL( GdiResetDCEMF(m_hSpoolFile, pCurrentDM) );
		DEBUG_FAIL( ::SetGraphicsMode(m_hPrinterDC, GM_ADVANCED) );
	}
}

bool CEMFJobHandler::CopyDevMode()
// Function Description:
//		Copies the DEVMODE in m_pPrintProcessorData or the default DEVMODE into m_pDevMode
//
// Parameters:
//		m_pDevMode					-> Pointer to LPDEVMODE
//
// Return Value:
//		TRUE						-> Successful
//		FALSE						-> Failed
{
	bool			 bReturnMe		= false;
	LONG			 lNeeded;

	// Check assumptions
	_ASSERTE( m_pPrintProcessorData != NULL );

	DEBUG_EMF( _T("Begin CopyDevMode()\n") );

	if ( m_pPrintProcessorData->pDevMode != NULL )
	{
		lNeeded = m_pPrintProcessorData->pDevMode->dmSize +  m_pPrintProcessorData->pDevMode->dmDriverExtra;

		m_pDevMode = static_cast<LPDEVMODE>(CSpoolMemory::Alloc(lNeeded));
		if ( m_pDevMode != NULL )
		{
			::CopyMemory( m_pDevMode, m_pPrintProcessorData->pDevMode, lNeeded );
			bReturnMe = true;
		}
	}
	else
	{
		fnWinSpoolDrv	 fnList;
		HANDLE			 hDrvPrinter	= NULL;

		DEBUG_EMF( _T("m_pPrintProcessorData->pDevMode == NULL inside of CopyDevMode\n") );
		// Get the pointer to the client side functions from the router
		if ( SplInitializeWinSpoolDrv(&fnList) == FALSE )
		{
			DEBUG_LAST_ERROR();
			DEBUG_TEXT( _T("SplInitializeWinSpoolDrv failed inside of CopyDevMode\n") );
			throw EMF_EXCEPTION( CEMFException::exEMF_FailSplInitializeWinSpoolDrv );	
		}

		// Get a client side printer handle to pass to the driver
		if ( (* (fnList.pfnOpenPrinter))(m_pPrintProcessorData->pPrinterName, &hDrvPrinter, NULL) == TRUE )
		{
			lNeeded = (* (fnList.pfnDocumentProperties))(NULL,
														 hDrvPrinter,
														 m_pPrintProcessorData->pPrinterName,
														 NULL,
														 NULL,
														 0);

			if ( lNeeded > 0 )
			{
				m_pDevMode = static_cast<LPDEVMODE>(CSpoolMemory::Alloc(lNeeded));
				if ( m_pDevMode != NULL )
				{
					if ( (*(fnList.pfnDocumentProperties))(NULL,
												   hDrvPrinter,
												   m_pPrintProcessorData->pPrinterName,
												   m_pDevMode,
												   NULL,
												   DM_OUT_BUFFER) >= 0 )
					{
						bReturnMe = true;
						// Copy the data for the parser
						if ( !m_ParserData.AreAnyFieldsSet() )
						{
							DEBUG_EMF( _T("Copying Driver defaults to ParserData\n") );
							m_ParserData.Copy( m_pDevMode );
						}
					}
					else
					{
						DEBUG_TEXT( _T("Failed to get default DEVMODE for the printer \"%s\"\n"), m_pPrintProcessorData->pPrinterName );
						CSpoolMemory::Free( m_pDevMode );
						m_pDevMode = NULL;
					}
				}
				DEBUG_ELSE_TEXT( _T("Failed to allocate memory while trying to read default DEVMODE!\n") );
			}
			DEBUG_ELSE_TEXT( _T("DocumentProperties says that DEVMODE is 0 bytes while trying to read default DEVMODE!\n") );

			// Close the printer
			(* (fnList.pfnClosePrinter))(hDrvPrinter);
		}
		DEBUG_ELSE_TEXT( _T("Open Printer Failed while trying to read default DEVMODE!\n") );
	}

	DEBUG_EMF( _T("End CopyDevMode(): %s\n"), DEBUG_BOOL(bReturnMe) );
	return bReturnMe;
}

//////////////////////////////////////////////////////////////////////
// Static UpdateRect Structures
//////////////////////////////////////////////////////////////////////

UpdateRect CEMFJobHandler::m_URect21[]	=	{
												{ 0, 0.5, 0, 1 },
												{ 0.5, 1, 0, 1 }
											};

UpdateRect CEMFJobHandler::m_URect21R[]	=	{
												{ 0.5, 1, 0, 1 },
												{ 0, 0.5, 0, 1 }
											};

UpdateRect CEMFJobHandler::m_URect22[]	=	{
												{ 0, 1, 0, 0.5 },
												{ 0, 1, 0.5, 1 }
											};

UpdateRect CEMFJobHandler::m_URect4[]	=	{
												{ 0, 0.5, 0, 0.5 },
												{ 0, 0.5, 0.5, 1 },
												{ 0.5, 1, 0, 0.5 },
												{ 0.5, 1, 0.5, 1 }
											};

UpdateRect CEMFJobHandler::m_URect61[]	=	{
												{ 0, 1.0/3.0, 0, 0.5 },
												{ 0, 1.0/3.0, 0.5, 1 },
												{ 1.0/3.0, 2.0/3.0, 0, 0.5 },
												{ 1.0/3.0, 2.0/3.0, 0.5, 1 },
												{ 2.0/3.0, 1, 0, 0.5 },
												{ 2.0/3.0, 1, 0.5, 1 }
											};

UpdateRect CEMFJobHandler::m_URect61R[]	=	{
												{ 2.0/3.0, 1, 0, 0.5 },
												{ 1.0/3.0, 2.0/3.0, 0, 0.5 },
												{ 0, 1.0/3.0, 0, 0.5 },
												{ 2.0/3.0, 1, 0.5, 1 },
												{ 1.0/3.0, 2.0/3.0, 0.5, 1 },
												{ 0, 1.0/3.0, 0.5, 1 }
											};

UpdateRect CEMFJobHandler::m_URect62[]	=	{
												{ 0, 0.5, 0, 1.0/3.0 },
												{ 0, 0.5, 1.0/3.0, 2.0/3.0 },
												{ 0, 0.5, 2.0/3.0, 1 },
												{ 0.5, 1, 0, 1.0/3.0 },
												{ 0.5, 1, 1.0/3.0, 2.0/3.0 },
												{ 0.5, 1, 2.0/3.0, 1 }
											};

UpdateRect CEMFJobHandler::m_URect62R[]	=	{
												{ 0.5, 1, 0, 1.0/3.0 },
												{ 0, 0.5, 0, 1.0/3.0 },
												{ 0.5, 1, 1.0/3.0, 2.0/3.0 },
												{ 0, 0.5, 1.0/3.0, 2.0/3.0 },
												{ 0.5, 1, 2.0/3.0, 1 },
												{ 0, 0.5, 2.0/3.0, 1 }
											};

UpdateRect CEMFJobHandler::m_URect9[]	=	{
												{ 0, 1.0/3.0, 0, 1.0/3.0 },
												{ 0, 1.0/3.0, 1.0/3.0, 2.0/3.0 },
												{ 0, 1.0/3.0, 2.0/3.0, 1 },
												{ 1.0/3.0, 2.0/3.0, 0, 1.0/3.0 },
												{ 1.0/3.0, 2.0/3.0, 1.0/3.0, 2.0/3.0 },
												{ 1.0/3.0, 2.0/3.0, 2.0/3.0, 1 },
												{ 2.0/3.0, 1, 0, 1.0/3.0 },
												{ 2.0/3.0, 1, 1.0/3.0, 2.0/3.0 },
												{ 2.0/3.0, 1, 2.0/3.0, 1 }
											};

UpdateRect CEMFJobHandler::m_URect16[]	=	{
												{ 0, 0.25, 0, 0.25 },
												{ 0, 0.25, 0.25, 0.5 },
												{ 0, 0.25, 0.5, 0.75 },
												{ 0, 0.25, 0.75, 1 },
												{ 0.25, 0.5, 0, 0.25 },
												{ 0.25, 0.5, 0.25, 0.5 },
												{ 0.25, 0.5, 0.5, 0.75 },
												{ 0.25, 0.5, 0.75, 1 },
												{ 0.5, 0.75, 0, 0.25 },
												{ 0.5, 0.75, 0.25, 0.5 },
												{ 0.5, 0.75, 0.5, 0.75 },
												{ 0.5, 0.75, 0.75, 1 },
												{ 0.75, 1, 0, 0.25 },
												{ 0.75, 1, 0.25, 0.5 },
												{ 0.75, 1, 0.5, 0.75 },
												{ 0.75, 1, 0.75, 1 }
											};

//////////////////////////////////////////////////////////////////////
// End of EMFJobHandler.cpp
//////////////////////////////////////////////////////////////////////