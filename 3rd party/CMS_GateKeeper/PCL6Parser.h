///////////////////////////////////////////////////////////////////////////////////////////////////////
// PCL6Parser.h: interface for the CPCL6Parser class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PCL6PARSER_H__AAD61936_C5D2_4DC6_A675_D15FA096EB6B__INCLUDED_)
#define AFX_PCL6PARSER_H__AAD61936_C5D2_4DC6_A675_D15FA096EB6B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintParserBase.h"
#include "PrintJobBuffer.h"
#include "PCL6AttributeList.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Forward Defines
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL6Parser Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPCL6Parser : public CPrintParserBase
{
// Construction / Destruction
public:
	CPCL6Parser();
	virtual ~CPCL6Parser();

// Operations
public:
	virtual bool	Parse( CPrintParserData* pParserData );

// Main Processing Functions
private:
	bool			VerifyPCL6();

// Process Functions
private:
	void			ProcessColorSpace( const CPCL6Attribute* pColorSpace );
	void			ProcessCustomMediaSize( const CPCL6Attribute* pSize, 
											const CPCL6Attribute* pUnits );
	void			ProcessDuplexing( const CPCL6Attribute* pDuplexing );
	void			ProcessMediaSize( const CPCL6Attribute* pMediaSize );
	void			ProcessOrientation(	const CPCL6Attribute* pOrientation );
	void			ProcessResolution( const CPCL6Attribute* pSize, const CPCL6Attribute* pUnits );

	void			ProcessEmbeddedData();

// Operator Functions
private:
	// Executes the appropriate operator
	bool			HandleOperator();
	// PCL6 Operators
	void			OperatorArcPath()							{  };
	void			OperatorBeginChar()							{  };
	void			OperatorBeginFontHeader()					{  };
	void			OperatorBeginImage()						{  };
	void			OperatorBeginPage();//							{  };
	void			OperatorBeginRastPattern()					{  };
	void			OperatorBeginScan()							{  };
	void			OperatorBeginSession();//						{  };
	void			OperatorBeginStream()						{  };
	void			OperatorBeginUserDefinedLineCaps()			{  };
	void			OperatorBezierPath()						{ ProcessEmbeddedData(); };
	void			OperatorBezierRelPath()						{ ProcessEmbeddedData(); };
	void			OperatorChord()								{  };
	void			OperatorChordPath()							{  };
	void			OperatorCloseDataSource()					{  };
	void			OperatorCloseSubPath()						{  };
	void			OperatorComment()							{  };
	void			OperatorEllipse()							{  };
	void			OperatorEllipsePath()						{  };
	void			OperatorEndChar()							{  };
	void			OperatorEndFontHeader()						{  };
	void			OperatorEndImage()							{  };
	void			OperatorEndPage();//							{  };
	void			OperatorEndRastPattern()					{  };
	void			OperatorEndScan()							{  };
	void			OperatorEndSession()						{  };
	void			OperatorEndStream()							{  };
	void			OperatorEndUserDefinedLineCaps()			{  };
	void			OperatorExecStream()						{  };
	void			OperatorLinePath()							{ ProcessEmbeddedData(); };
	void			OperatorLineRelPath()						{ ProcessEmbeddedData(); };
	void			OperatorNewPath()							{  };
	void			OperatorOpenDataSource()					{  };
	void			OperatorPaintPath()							{  };
	void			OperatorPie()								{  };
	void			OperatorPiePath()							{  };
	void			OperatorPopGS()								{  };
	void			OperatorPushGS()							{  };
	void			OperatorReadChar()							{ ProcessEmbeddedData(); };
	void			OperatorReadFontHeader()					{ ProcessEmbeddedData(); };
	void			OperatorReadImage()							{ ProcessEmbeddedData(); };
	void			OperatorReadRastPattern()					{ ProcessEmbeddedData(); };
	void			OperatorReadStream()						{ ProcessEmbeddedData(); };
	void			OperatorRectangle()							{  };
	void			OperatorRectanglePath()						{  };
	void			OperatorRemoveFont()						{  };
	void			OperatorSetCharAttributes()					{  };
	void			OperatorRemoveStream()						{  };
	void			OperatorRoundRectangle()					{  };
	void			OperatorRoundRectanglePath()				{  };
	void			OperatorScanLineRel()						{ ProcessEmbeddedData(); };
	void			OperatorSetClipReplace()					{  };
	void			OperatorSetBrushSource()					{  };
	void			OperatorSetCharAngle()						{  };
	void			OperatorSetCharBoldValue()					{  };
	void			OperatorSetCharScale()						{  };
	void			OperatorSetCharShear()						{  };
	void			OperatorSetCharSubMode()					{  };
	void			OperatorSetClipIntersect()					{  };
	void			OperatorSetClipMode()						{  };
	void			OperatorSetClipRectangle()					{  };
	void			OperatorSetClipToPage()						{  };
	void			OperatorSetColorSpace();//						{  };
	void			OperatorSetColorTreatment()					{  };
	void			OperatorSetCursor()							{  };
	void			OperatorSetCursorRel()						{  };
	void			OperatorSetDefaultGS()						{  };
	void			OperatorSetHalftoneMethod()					{ ProcessEmbeddedData(); };
	void			OperatorSetFillMode()						{  };
	void			OperatorSetFont()							{  };
	void			OperatorSetLineCap()						{  };
	void			OperatorSetLineDash()						{  };
	void			OperatorSetLineJoin()						{  };
	void			OperatorSetMiterLimit()						{  };
	void			OperatorSetPageDefaultCTM()					{  };
	void			OperatorSetPageOrigin()						{  };
	void			OperatorSetPageRotation()					{  };
	void			OperatorSetPageScale()						{  };
	void			OperatorSetPathToClip()						{  };
	void			OperatorSetPatternTxMode()					{  };
	void			OperatorSetPenSource()						{  };
	void			OperatorSetPenWidth()						{  };
	void			OperatorSetROP()							{  };
	void			OperatorSetSourceTxMode()					{  };
	void			OperatorText()								{  };
	void			OperatorTextPath()							{  };

	void			OperatorRFU()								{  };

// Attributes
private:
	CPCL6AttributeList	m_AttributeList;
	CPrintParserData*	m_pParserData;
	// Parsed Values
	DWORD				m_dwPageCount;
	WORD				m_nCopies;

// Static Attributes
private:
	static const PaperSizeInfo	PCL6FormNames[];

// Enumerated Types
private:
	// General Values
	enum
	{
		// These are given in decimal because that is how
		// they are given in the PCL-XL 2.0 reference document
		PCL_ATTRIB_BLOCKBYTELENGTH			= 111,
		PCL_ATTRIB_COLORDEPTH				= 98,
		PCL_ATTRIB_COLORSPACE				= 3,
		PCL_ATTRIB_CUSTOMMEDIASIZE			= 47,
		PCL_ATTRIB_CUSTOMMEDIASIZEUNITS		= 48,
		PCL_ATTRIB_DUPLEXPAGEMODE			= 53,
		PCL_ATTRIB_DUPLEXPAGESIDE			= 54,
		PCL_ATTRIB_MEDIASIZE				= 37,
		PCL_ATTRIB_MEDIASOURCE				= 38,
		PCL_ATTRIB_MEASURE					= 134,
		PCL_ATTRIB_ORIENTATION				= 40,
		PCL_ATTRIB_PALETTEDEPTH				= 2,
		PCL_ATTRIB_PALETTEDATA				= 6,
		PCL_ATTRIB_PAGECOPIES				= 49,
		PCL_ATTRIB_SIMPLEXPAGEMODE			= 52,
		PCL_ATTRIB_UNITSPERMEASURE			= 114,

		// Used to identify a PCL stream
		PCL_HEADER_BIGENDIAN				= 0x28,
		PCL_HEADER_LITTLEENDIAN				= 0x29,
		PCL_HEADER_END						= 0x0a,

		// Used for handling embedded data
		PCL_EMBEDDED_DATA					= 0xfa,
		PCL_EMBEDDED_DATA_BYTE				= 0xfb
	};

	// PCL6 Operators
	enum pcl6_Operator
	{
		pcl6_opArcPath						= 0x91,
		pcl6_opBeginChar					= 0x52,
		pcl6_opBeginFontHeader				= 0x4f,
		pcl6_opBeginImage					= 0xb0,
		pcl6_opBeginPage					= 0x43,
		pcl6_opBeginRastPattern				= 0xb3,
		pcl6_opBeginScan					= 0xb6,
		pcl6_opBeginSession					= 0x41,
		pcl6_opBeginStream					= 0x5b,
		pcl6_opBeginUserDefinedLineCaps		= 0x82,	// Added in 2.1
		pcl6_opBezierPath					= 0x93,
		pcl6_opBezierRelPath				= 0x95,
		pcl6_opChord						= 0x96,
		pcl6_opChordPath					= 0x97,
		pcl6_opCloseDataSource				= 0x49,
		pcl6_opCloseSubPath					= 0x84,
		pcl6_opComment						= 0x47,
		pcl6_opEllipse						= 0x98,
		pcl6_opEllipsePath					= 0x99,
		pcl6_opEndChar						= 0x54,
		pcl6_opEndFontHeader				= 0x51,
		pcl6_opEndImage						= 0xb2,
		pcl6_opEndPage						= 0x44,
		pcl6_opEndRastPattern				= 0xb5,
		pcl6_opEndScan						= 0xb8,
		pcl6_opEndSession					= 0x42,
		pcl6_opEndStream					= 0x5d,
		pcl6_opEndUserDefinedLineCaps		= 0x83,	// Added in 2.1
		pcl6_opExecStream					= 0x5e,
		pcl6_opLinePath						= 0x9b,
		pcl6_opLineRelPath					= 0x9d,
		pcl6_opNewPath						= 0x85,
		pcl6_opOpenDataSource				= 0x48,
		pcl6_opPaintPath					= 0x86,
		pcl6_opPie							= 0x9e,
		pcl6_opPiePath						= 0x9f,
		pcl6_opPopGS						= 0x60,
		pcl6_opPushGS						= 0x61,
		pcl6_opReadChar						= 0x53,
		pcl6_opReadFontHeader				= 0x50,
		pcl6_opReadImage					= 0xb1,
		pcl6_opReadRastPattern				= 0xb4,
		pcl6_opReadStream					= 0x5c,
		pcl6_opRectangle					= 0xa0,
		pcl6_opRectanglePath				= 0xa1,
		pcl6_opRemoveFont					= 0x55,
		pcl6_opRemoveStream					= 0x5f,
		pcl6_opRoundRectangle				= 0xa2,
		pcl6_opRoundRectanglePath			= 0xa3,
		pcl6_opScanLineRel					= 0xb9,
		pcl6_opSetCharAttributes			= 0x56,
		pcl6_opSetClipReplace				= 0x62,
		pcl6_opSetBrushSource				= 0x63,
		pcl6_opSetCharAngle					= 0x64,
		pcl6_opSetCharBoldValue				= 0x7d,
		pcl6_opSetCharScale					= 0x65,
		pcl6_opSetCharShear					= 0x66,
		pcl6_opSetCharSubMode				= 0x81,
		pcl6_opSetClipIntersect				= 0x67,
		pcl6_opSetClipMode					= 0x7f,
		pcl6_opSetClipRectangle				= 0x68,
		pcl6_opSetClipToPage				= 0x69,
		pcl6_opSetColorSpace				= 0x6a,
		pcl6_opSetColorTreatment			= 0x58,	// Added in 2.1
		pcl6_opSetCursor					= 0x6b,
		pcl6_opSetCursorRel					= 0x6c,
		pcl6_opSetDefaultGS					= 0x57,	// Added in 2.1
		pcl6_opSetHalftoneMethod			= 0x6d,
		pcl6_opSetFillMode					= 0x6e,
		pcl6_opSetFont						= 0x6f,
		pcl6_opSetLineCap					= 0x71,
		pcl6_opSetLineDash					= 0x70,
		pcl6_opSetLineJoin					= 0x72,
		pcl6_opSetMiterLimit				= 0x73,
		pcl6_opSetPageDefaultCTM			= 0x74,
		pcl6_opSetPageOrigin				= 0x75,
		pcl6_opSetPageRotation				= 0x76,
		pcl6_opSetPageScale					= 0x77,
		pcl6_opSetPathToClip				= 0x80,
		pcl6_opSetPatternTxMode				= 0x78,
		pcl6_opSetPenSource					= 0x79,
		pcl6_opSetPenWidth					= 0x7a,
		pcl6_opSetROP						= 0x7b,
		pcl6_opSetSourceTxMode				= 0x7c,
		pcl6_opText							= 0xa8,
		pcl6_opTextPath						= 0xa9,

		// RFU Values (Should be recognized as operators)
		pcl6_opRFU1							= 0x45,
		pcl6_opRFU2							= 0x46,
		pcl6_opRFU3							= 0x4a,
		pcl6_opRFU4							= 0x4b,
		pcl6_opRFU5							= 0x4c,
		pcl6_opRFU6							= 0x4d,
		pcl6_opRFU7							= 0x4e,

		pcl6_opRFU22						= 0x90,
		pcl6_opRFU23						= 0x92,
		pcl6_opRFU24						= 0x94,
		pcl6_opRFU25						= 0x9a,
		pcl6_opRFU26						= 0x9c,
		pcl6_opRFU27						= 0xa4,
		pcl6_opRFU28						= 0xa5,
		pcl6_opRFU29						= 0xa6,
		pcl6_opRFU30						= 0xa7,
		pcl6_opRFU31						= 0xaa,
		pcl6_opRFU32						= 0xab,
		pcl6_opRFU33						= 0xac,
		pcl6_opRFU34						= 0xad,
		pcl6_opRFU35						= 0xae,
		pcl6_opRFU36						= 0xaf,
		pcl6_opRFU37						= 0xb7,

		// No longer labeled RFU in 2.1
			//pcl6_opRFU10						= 0x59,	
			//pcl6_opRFU11						= 0x5a,
			//pcl6_opRFU18						= 0x8c,
			//pcl6_opRFU19						= 0x8d,
			//pcl6_opRFU20						= 0x8e,
			//pcl6_opRFU21						= 0x8f,

		// Marked as reserved in 2.1
			//pcl6_opRFU13						= 0x87,	
			//pcl6_opRFU14						= 0x88,
			//pcl6_opRFU15						= 0x89,
			//pcl6_opRFU16						= 0x8a,
			//pcl6_opRFU17						= 0x8b,
	};

	// PCL6 Enumerated Type: ColorDepth
	enum pcl6_ColorDepth
	{
		pcl6_e1Bit						= 0,
		pcl6_e4Bit						= 1,
		pcl6_e8Bit						= 2
	};

	// PCL6 Enumerated Type: ColorMapping
	enum pcl6_ColorMapping
	{
		pcl6_eDirectPixel				= 0,
		pcl6_eIndexedPixel				= 1
	};

	// PCL6 Enumerated Type: ColorSpace
	enum pcl6_ColorSpace
	{
		pcl6_eGray						= 1,
		pcl6_eRGB						= 2,
		pcl6_eSRGB						= 6
	};

	// PCL6 Enumerated Type: ColorTreatment
	// Added in PCL-XL 2.1
	enum pcl6_ColorTreatment
	{
		pcl6_eNoTreatment				= 0,
		pcl6_eScreenMatch				= 1,
		pcl6_eVivid						= 2
	};

	// PCL6 Enumerated Type: DuplexPageMode
	enum pcl6_DuplexPageMode
	{
		pcl6_eDuplexHorizontalBinding	= 0,
		pcl6_eDuplexVerticalBinding		= 1
	};

	// PCL6 Enumerated Type: DuplexPageSide
	enum pcl6_DuplexPageSide
	{
		pcl6_eFrontMediaSide			= 0,
		pcl6_eBackMediaSide				= 1
	};

	// PCL6 Enumerated Type: SimplexPageMode
	enum pcl6_SimplePageMode
	{
		pcl6_eSimplexFrontSide			= 0
	};

	// PCL6 Enumerated Type: Measure
	enum pcl6_Measure
	{
		pcl6_eInch						= 0,
		pcl6_eMillimeter				= 1,
		pcl6_eTenthsOfAMillimeter		= 2
	};

	// PCL6 Enumerated Type: MediaSize					
	enum pcl6_MediaSize					
	{
		pcl6_eDefaultPaperSize			= 96,	// Added in 2.1
		pcl6_eLetter					= 0,
		pcl6_eLegal						= 1,
		pcl6_eA4						= 2,
		pcl6_eExec						= 3,
		pcl6_eLedger					= 4,
		pcl6_eA3						= 5,
		pcl6_eCOM10Envelope				= 6,
		pcl6_eMonarchEnvelope			= 7,
		pcl6_eC5Envelope				= 8,
		pcl6_eDLEnvelope				= 9,
		pcl6_eJB4						= 10,
		pcl6_eJB5						= 11,
		pcl6_eB5Envelope				= 12,
		pcl6_eB5Paper					= 13,	// Added in 2.1
		pcl6_eJPostcard					= 14,
		pcl6_eJDoublePostcard			= 15,
		pcl6_eA5						= 16,
		pcl6_eA6						= 17,
		pcl6_eJB6						= 18,
		pcl6_JIS8K						= 19,	// Added in 2.1
		pcl6_JIS16K						= 20,	// Added in 2.1
		pcl6_JISExec					= 21,	// Added in 2.1
	};

	// PCL6 Enumerated Type: MediaSource
	enum pcl6_MediaSource
	{
		pcl6_eDefaultSource				= 0,
		pcl6_eAutoSelect				= 1,
		pcl6_eManualFeed				= 2,
		pcl6_eMultiPurposeTray			= 3,
		pcl6_eUpperCassette				= 4,
		pcl6_eLowerCassette				= 5,
		pcl6_eEnvelopeTray				= 6,
		pcl6_eThirdCassette				= 7
	};

	// PCL6 Enumerated Type: Orientation
	enum pcl6_Orientation
	{
		pcl6_ePortraitOrientation		= 0,
		pcl6_eLandscapeOrientation		= 1,
		pcl6_eReversePortrait			= 2,
		pcl6_eReverseLandscape			= 3,
		pcl6_eDefaultOrientation		= 4,	// Added in 2.1
	};
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPCL6Parser Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction / Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	inline CPCL6Parser::CPCL6Parser()
	{
		m_pParserData	= NULL;
		m_dwPageCount	= 0;
		m_nCopies		= 0;
	}

	inline CPCL6Parser::~CPCL6Parser()
	{
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PCL6PARSER_H__AAD61936_C5D2_4DC6_A675_D15FA096EB6B__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PCL6Parser.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
