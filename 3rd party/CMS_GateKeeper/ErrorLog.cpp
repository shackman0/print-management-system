///////////////////////////////////////////////////////////////////////////////////////////////////////
// ErrorLog.cpp: implementation of the CErrorLog class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 1999-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "ErrorLog.h"
#include "RegistrySettings.h"
#include "resource.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define EVENTLOG_CATEGORIES		2

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

CErrorLog::CErrorLog()
{
	CRegKey		keyEventLog;
	TCHAR		szFullPath[_MAX_PATH];
	TCHAR		szNewKey[_MAX_PATH];
	LONG		ret;
	DWORD		dwBytes;

	// Look up the path of the print processor directory
	if ( ::GetPrintProcessorDirectory(NULL, NULL, 1, (BYTE*)szFullPath, sizeof(szFullPath), &dwBytes) )
	{
		// Append the backslash and file name to the target path
		_tcscat( szFullPath, _T("\\") );
		_tcscat( szFullPath, CMS_PRINTPROC_NAME );
		_tcscat( szFullPath, _T(".dll") );
	}
	DEBUG_ELSE();

	lstrcpy( szNewKey, REG_KEY_EVENTLOG_BASE );
	lstrcat( szNewKey, CMS_PRINTPROC_NAME );
	m_bValid = true;

	// Create the key
	if ( m_bValid )
	{
		ret = keyEventLog.Create( HKEY_LOCAL_MACHINE, szNewKey );
		DEBUG_FAIL( (m_bValid = (ret == ERROR_SUCCESS)) );
	}

	// Set the path to the file for event messages
	if ( m_bValid )
	{
		ret = keyEventLog.SetValue( szFullPath, REG_VALUE_EVENTLOG_MESSAGEFILE );
		DEBUG_FAIL( (m_bValid = (ret == ERROR_SUCCESS)) );
	}

	// Set the usable types
	if ( m_bValid )
	{
		ret = keyEventLog.SetValue( 
			(EVENTLOG_ERROR_TYPE | EVENTLOG_WARNING_TYPE | EVENTLOG_INFORMATION_TYPE), 
			REG_VALUE_EVENTLOG_TYPES );
		DEBUG_FAIL( (m_bValid = (ret == ERROR_SUCCESS)) );
	}

	// Set the path to the file for event categories
	if ( m_bValid )
	{
		ret = keyEventLog.SetValue( szFullPath, REG_VALUE_EVENTLOG_CATEGORYFILE );
		DEBUG_FAIL( (m_bValid = (ret == ERROR_SUCCESS)) );
	}

	// Set the number of categories
	if ( m_bValid )
	{
		ret = keyEventLog.SetValue( EVENTLOG_CATEGORIES, REG_VALUE_EVENTLOG_CATEGORYCOUNT );
		DEBUG_FAIL( (m_bValid = (ret == ERROR_SUCCESS)) );
	}
}

CErrorLog::~CErrorLog()
{

}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Logging Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CErrorLog::LogErrorMessage( DWORD dwEventID, DWORD dwErrorCode )
{
	CString	strErrorMessage = ::LookupSystemError( dwErrorCode );

	LogErrorMessage( dwEventID, dwErrorCode, strErrorMessage );
}

void CErrorLog::LogErrorMessage( DWORD dwEventID, DWORD dwErrorCode, LPCTSTR strErrorMessage )
{
	TCHAR	lpszErrorCode[_MAX_PATH];
	LPCTSTR	pStrings[2];

	if ( (strErrorMessage != NULL) && (_tcslen(strErrorMessage) > 0) )
	{
		_stprintf( lpszErrorCode, _T("0x%08X : %s"), dwErrorCode, strErrorMessage );
	}
	else
	{
		_stprintf( lpszErrorCode, _T("0x%08X"), dwErrorCode );
	}

	pStrings[0] = lpszErrorCode;
	pStrings[1] = 0;
	LogEvent( dwEventID, EVENTLOG_ERROR_TYPE, EVT_CAT_INTERNAL_ERROR, 1, pStrings );
}

void CErrorLog::LogEvent( DWORD dwEventID, WORD wType, WORD wCategory, WORD wStrings, 
						 LPCTSTR* pStrings, DWORD dwData, LPVOID pData )
{
	if ( m_bValid )
	{
		// Get a handle to use with ReportEvent()
		HANDLE hEventSource = ::RegisterEventSource( NULL, CMS_PRINTPROC_NAME );

		if (hEventSource != NULL)
		{
			// Write to event log
			DEBUG_FAIL( ::ReportEvent( hEventSource, wType, wCategory, dwEventID, 
				NULL, wStrings, dwData, pStrings, pData) );
			// Unregister event source
			DEBUG_FAIL( ::DeregisterEventSource(hEventSource) );
		}
		DEBUG_ELSE();
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of ErrorLog.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////