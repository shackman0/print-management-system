///////////////////////////////////////////////////////////////////////////////////////////////////////
// DebugUtils.cpp: implementation of the CMS Debugging Utilities.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"

#ifdef USE_DEBUG_OUTPUT

#include "DebugUtils.h"
#include <stdio.h>
#include <stdarg.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_CMS_CONSOLE
	#ifndef CONSOLE_ERROR_COLOR
		#define CONSOLE_ERROR_COLOR		(FOREGROUND_RED | FOREGROUND_INTENSITY)
	#endif
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Standard Win32 Stuff
///////////////////////////////////////////////////////////////////////////////////////////////////////

void DisplayDebugText( LPCTSTR lpszFormat, ... )
{
	// Make it thread safe
	static CCSThreadLock	 Lock;
	::EnterCriticalSection( Lock.GetCS() );

	va_list args;
	va_start(args, lpszFormat);

	static TCHAR	lpszBuffer[4096];
	static LPTSTR	lpszCurrent = lpszBuffer;
	static int		nBufferSize = (sizeof(lpszBuffer)/sizeof(TCHAR));
	int				nBuf;

#ifdef DEBUG_PREFIX
	static bool		bInitialized = false;
	if ( bInitialized == false )
	{
		int nPrefixLength = _tcslen(DEBUG_PREFIX);

		::CopyMemory( lpszBuffer, DEBUG_PREFIX, sizeof(DEBUG_PREFIX) );
		lpszCurrent += nPrefixLength;
		nBufferSize -= nPrefixLength;
		bInitialized = true;
	}
#endif

	nBuf = _vsntprintf( lpszCurrent, nBufferSize, lpszFormat, args );
	_ASSERTE( nBuf >= 0 );

// Write out the formatted text
#ifdef USE_STDERR
	#ifdef USE_CMS_CONSOLE
		if ( CConsole::IsErrorConsoleValid() )
		{
			CConsole::WriteError( CONSOLE_ERROR_COLOR, lpszBuffer );
		}
	#else
		_ftprintf( stderr, lpszBuffer );
	#endif
#endif

	::OutputDebugString( lpszBuffer );

	va_end(args);

	// Unlock
	::LeaveCriticalSection( Lock.GetCS() );
}

void DisplayError( DWORD dwError, LPCTSTR lpszFile, int nLine )
{
	LPTSTR	lpszTemp = 0;
	DWORD	dwTemp;

	// Get the text for the error code
	dwTemp = ::FormatMessage
		(
			FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM 
				| FORMAT_MESSAGE_ARGUMENT_ARRAY,
			0,
			dwError,
			LANG_NEUTRAL,
			(LPTSTR)&lpszTemp,
			0,
			0
		);

	// Display the error
	DEBUG_TEXT( _T("*** Error 0x%08X : %s"), dwError, lpszTemp );

	// Newline is usually provided by the error description,
	// but provide one if needed
	if ( lpszTemp == NULL )
	{
		DEBUG_TEXT( _T("\n") );
	}

	// Display the extra info in the debug versions
	if ( lpszFile != NULL )
	{
		DEBUG_TEXT( _T("*** File: %s [Line #%d]\n"), lpszFile, nLine );
	}

	// Clean up
	if ( lpszTemp )
	{
		LocalFree( HLOCAL(lpszTemp) );
	}
}

void DumpRawData( LPCVOID pData, const DWORD& dwSize )
{
// Value comes from the following:
//		Line Number:		"00000: "					= 7
//		Individual Bytes:	" 00"		= 3 * 16		= 48
//		Ending:				"\t\t" + <16 values> + "\n" = 19
// Total = 74 + 1 for NULL = 75

#define BYTES_PER_LINE				16
#define LINE_NUMBER_LENGTH			7	// "00000: "
#define BYTE_VALUE_LENGTH			3	// " 00"
#define PRE_DISPLAY_LENGTH			2	// "/t/t"
#define DISPLAY_VALUE_LENGTH		1	// "."
#define PRE_DISPLAY_OFFSET			(LINE_NUMBER_LENGTH + (BYTES_PER_LINE*BYTE_VALUE_LENGTH))
#define DISPLAY_OFFSET				(PRE_DISPLAY_OFFSET + PRE_DISPLAY_LENGTH)
#define BYTE_VALUE_OFFSET(n)		(LINE_NUMBER_LENGTH + ((n)*BYTE_VALUE_LENGTH))
#define DISPLAY_VALUE_OFFSET(n)		(DISPLAY_OFFSET + ((n)*DISPLAY_VALUE_LENGTH))
#define DUMP_LINE_LENGTH			(DISPLAY_OFFSET + (BYTES_PER_LINE*DISPLAY_VALUE_LENGTH) + 2)

	static const TCHAR HEX_VALUES[]	=
	{
		_T('0'), _T('1'), _T('2'), _T('3'), _T('4'), _T('5'), _T('6'), _T('7'),
		_T('8'), _T('9'), _T('a'), _T('b'), _T('c'), _T('d'), _T('e'), _T('f')
	};

	LPCBYTE				pBuffer	= static_cast<LPCBYTE>(pData);
	unsigned register	nCounter;
	unsigned register	nOffset;
	TCHAR				chrTemp;
	BYTE				nCurrent;
	TCHAR				lpszDisplayLine[DUMP_LINE_LENGTH];

	for ( nCounter = 0 ; nCounter < dwSize ; nCounter++ )
	{
		// Display the prefix for each line
		if ((nCounter % 16) == 0)
		{
			nOffset = 0;
			::ZeroMemory( lpszDisplayLine, sizeof(lpszDisplayLine) );
			lpszDisplayLine[DUMP_LINE_LENGTH-2] = _T('\n');
			_stprintf( lpszDisplayLine + nOffset, _T("%05x: "), nCounter );
			nOffset += LINE_NUMBER_LENGTH;
		}

		// Display each character
		nCurrent  = pBuffer[nCounter];
		lpszDisplayLine[ nOffset++ ] = _T(' ');
		lpszDisplayLine[ nOffset++ ] = HEX_VALUES[ BYTE(nCurrent / 16) ];
		lpszDisplayLine[ nOffset++ ] = HEX_VALUES[ BYTE(nCurrent % 16) ];

		if ( nCurrent <= 32 )
		{
			chrTemp = _T('.');
		}
		else
		{
			chrTemp = TCHAR(nCurrent);
		}
		
		// Get the display value for each character
		lpszDisplayLine[ DISPLAY_VALUE_OFFSET(nCounter%16) ] = chrTemp;

		// If at the end of a line, or at the end of the buffer,
		// we need to display the viewable characters
		if ( ((nCounter%16) == 15) || (nCounter == dwSize-1) )
		{
			// If we're at the end of the buffer,
			// add the appropriate spaces
			while ( nOffset < PRE_DISPLAY_OFFSET )
			{
				lpszDisplayLine[ nOffset++ ] = _T(' ');
			}
			lpszDisplayLine[ nOffset++ ] = _T('\t');
			lpszDisplayLine[ nOffset++ ] = _T('\t');
			nOffset += (nCounter % 16) + 1;
			lpszDisplayLine[ nOffset++ ] = _T('\n');
			::DisplayDebugText( lpszDisplayLine );
		}
	}
}

#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// COM Utilities
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef __ATLBASE_H__
	#ifdef	USE_DEBUG_OUTPUT

void DisplayComException( const _com_error& e, LPCTSTR lpszFile, int nLine )
{
	// Display the error
	DEBUG_TEXT( _T("\n*** Handled Exception (0x%08X)\n*** Text\t: %s\n"), 
		e.Error(), e.ErrorMessage() );

	// Display source information (if available)
	if ( (LPCTSTR)e.Source() )
	{
		DEBUG_TEXT( _T("*** Source\t: %s\n"), (LPCTSTR)e.Source() );
	}

	// Display description information (if available)
	if ( (LPCTSTR)e.Description() )
	{
		DEBUG_TEXT( _T("*** Info\t: %s\n"), (LPCTSTR)e.Description() );
	}

	// Display the extra info in the debug versions
	if ( lpszFile )
	{
		DEBUG_TEXT( _T("*** File\t: %s [Line #%d]\n"), lpszFile, nLine );
	}
}
	
	#endif  // #ifdef USE_DEBUG_OUTPUT
#endif // #ifdef __ATLBASE_H__

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of DebugUtils.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////