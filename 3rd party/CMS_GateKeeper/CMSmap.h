///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMSmap.h: interface for the CCMSmap class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
//
// Description:
//		This class template allows for creating a key/value mapping between two arbitrary data types.
//		For any given key, the value can be quickly retrieved.  The work is accomplished by creating 
//		an array of linked lists of entries.  Each KEY is hashed into a 4 byte value.  This value
//		modulo the size of the array is the index in the array for the linked list to which the entry
//		is added.
//
// Usage:
//		The template requires 4 data type parameters: KEY, ARG_KEY, VALUE, and ARG_VALUE. KEY and VALUE
//		are the data types to be used for the mapping.  Their respective counterparts are how those
//		values are passed as parameters.  Typically, the ARG_KEY and ARG_VALUE parameters are reference
//		types for KEY and VALUE respectively.  However, if it is necessary to use a pass-by-value
//		approach, one can simply use the same arguments for ARG_KEY that was used for KEY and the same
//		argument for ARG_VALUE that was used for VALUE.
//
//		Construction / Destruction
//			The constructor takes a single, optional parameter that is the size of the array.  See the
//			notes section for how to determine what value to use.  The destructor will remove all the
//			elements in the map, but if the VALUE is a pointer, it will not free the memory to which
//			it points.  Therefore, one must free that memory before the object is destroyed in order
//			to avoid memory leaks.
//
//		Element Access
//			The operator[] is declared so that one may treat the mapping class like an array of VALUEs
//			using a KEY as the index.  If the value for a given KEY does not exist, it will be created.
//			
//			To test to see if an element exists without creating it, checkout the function "Lookup".
//
//		Iteration
//			If there is a need to cycle through all elements in the map, look at the functions called
//			"GetStartPosition" and "GetNextAssoc".  However, if that is something that is being done
//			frequently, it might be better to use a linked list instead.
//
// Notes:
//		Selecting an appropriate size for the array is critical to the performance of this class.
//		If the size is too small, then the linked lists become large because there is a lot of
//		overlapping on the hashed values.  Larger linked lists increase the time it takes to
//		look up a given KEY value.  However, if the size is too large, then not all entries in the
//		array are used and memory is wasted.
//
//		A good rule of thumb is to use the smallest prime number that is larger than the maximum
//		number of expected elements.
//
//		Another key to good performance with this class is the hashing function that is used.  The
//		more uniquely the hashing function identifies a given KEY, the more spread out the entries
//		are in the array.  This leads to smaller linked lists and therefore to better performance.
//
// Example Usage:
//		// Use a typedef to make it more readable;
//		typedef CCMSmap<CString, CString&, DWORD, DWORD&>	CMapStringToDWORD;
//
//		void main( void )
//		{
//			CMapStringToDWORD	mapAges;
//
//			mapAges[_T("Joe")]		= 22;
//			mapAges[_T("Frank")]	= 24;
//			mapAges[_T("Mark")]		= 29;
//			mapAges[_T("Jane")]		= 21;
//
//			printf( "Joe is %d years old.\n", mapAges[_T("Joe")] );
//		}
//
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_CMSMAP_H__C062FFE9_6C76_11D4_A461_00105A1C588D__INCLUDED_)
#define AFX_CMSMAP_H__C062FFE9_6C76_11D4_A461_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ThreadLock.h"
#include "DataChain.h"
#include "Collections.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_MAP_DEBUG
	#define DEBUG_MAP_TEXT						DEBUG_TEXT
	#define DUMP_MAP_INFO(map)					((map).DumpMapInfo())
#else
	#define DEBUG_MAP_TEXT						((void)(0))
	#define DUMP_MAP_INFO						((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEFAULT_SIZE_HASH_TABLE					37

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
using namespace CMSLIB;

namespace CMSmap
{
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSmap Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
class CCMSmap  
{
// Nested typename: CMapEntry
private:
	class CMapEntry
	{
	// Data Access
	private:
		// Get Functions
		CMapEntry*			GetNext() const							{ return m_pNext;				};
		const DWORD&		GetHashValue() const					{ return m_dwHashValue;			};
		ARG_KEY				GetKey()								{ return m_Key;					};
		ARG_VALUE			GetValue()								{ return m_Value;				};
		// Set Functions												
		void				SetNext( CMapEntry* pNext )				{ m_pNext		= pNext;		};
		void				SetHashValue( const DWORD& dwHashValue ){ m_dwHashValue	= dwHashValue;	};
		void				SetKey( ARG_KEY key )					{ m_Key			= key;			};
		void				SetValue( ARG_VALUE value )				{ m_Value		= value;		};

	// Operations
	private:
		void				Initialize()
		{
			CreateElements<KEY>( &m_Key, 1 );
			CreateElements<VALUE>( &m_Value, 1 );
			m_pNext			= NULL;
			m_dwHashValue	= 0;
		}

		void				CleanUp()
		{
			DestroyElements<KEY>( &m_Key, 1 );
			DestroyElements<VALUE>( &m_Value, 1 );
		}

	// Friend Declarations
		friend class CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>;

	// Attributes
	private:
		CMapEntry*			m_pNext;
		DWORD				m_dwHashValue;
		KEY					m_Key;
		VALUE				m_Value;
	};

// TypeDefs
protected:
	typedef	CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CMapEntry*		LPMAPENTRY;

// Construction / Destruction
public:
	CCMSmap( const DWORD& dwHashTableSize = DEFAULT_SIZE_HASH_TABLE, 
		const DWORD& dwBlockCount = 0 );
	virtual ~CCMSmap();

// Data Access
public:
	const DWORD&	GetCount() const;
	const DWORD&	GetHashTableSize() const;
	bool			IsEmpty() const;

// Map Operations
public:
	bool			Lookup( ARG_KEY key, VALUE& rValue ) const;
	void			SetAt( ARG_KEY key, ARG_VALUE newValue );
	ARG_VALUE		GetAt( ARG_KEY key );
	bool			RemoveKey( ARG_KEY key );
	void			RemoveAll();
	void			SetHashTableSize( const DWORD& dwHashTableSize );

// Iteration Operations
public:
	POSITION		GetStartPosition() const;
	void			GetNextAssoc( POSITION& pos, KEY& rKey, VALUE& rValue ) const;
	ARG_VALUE		GetNextAssoc( POSITION& pos ) const;

protected:
	POSITION		LookupNextEntry( const CMapEntry*& pEntry ) const;
	ARG_KEY			GetKeyAt( POSITION pos ) const;

// Operators
public:
	ARG_VALUE		operator[]( ARG_KEY key );

// Internal Operations
protected:
	void			InitHashTable( const DWORD& dwHashTableSize );
	LPMAPENTRY		FindOrCreateEntry( ARG_KEY key );
	LPMAPENTRY		GetEntry( ARG_KEY key, DWORD& dwHashValue ) const;

// Create / Destroy Entries
private:
	LPMAPENTRY		CreateMapEntry( ARG_KEY key, const DWORD& dwHashValue, CMapEntry* pNext );
	void			DestroyMapEntry( LPMAPENTRY pDestroyMe );

// Thread Safe Operations
public:
	DECLARE_THREAD_LOCK_FUNCTIONS();

// Debugging Operations
#ifdef USE_MAP_DEBUG
public:
	void			DumpMapInfo() const;
#endif
	
// Attributes
private:
	CMapEntry**		m_ppHashTable;
	DWORD			m_dwHashTableSize;
	DWORD			m_dwEntryCount;
	DWORD			m_dwBlockCount;
	CDataChain*		m_pDataChain;
	CMapEntry*		m_pFreeList;

	DECLARE_THREAD_LOCK();
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSmap Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Construction/Destruction
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CCMSmap( 
		const DWORD& dwHashTableSize, const DWORD& dwBlockCount )
	{
		// Make it thread-safe
		LOCK_THREAD();

		// Initialize member variables
		m_ppHashTable		= NULL;
		m_dwEntryCount		= 0;
		m_dwHashTableSize	= 0;
		m_dwBlockCount		= ( dwBlockCount ? dwBlockCount : dwHashTableSize );
		m_pFreeList			= NULL;
		m_pDataChain		= NULL;

		// Initialize the hash table
		InitHashTable( dwHashTableSize );
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::~CCMSmap()
	{
		RemoveAll();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Data Access
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline const DWORD& CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetCount() const
	{
		// Make it thread-safe
		LOCK_THREAD_CONST();

		// Return the value
		return m_dwEntryCount;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline const DWORD& CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetHashTableSize() const
	{
		// Make it thread-safe
		LOCK_THREAD_CONST();

		// Return the value
		return m_dwHashTableSize;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline bool CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::IsEmpty() const
	{
		// Return the value
		return (GetCount()==0);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Map Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline ARG_VALUE CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetAt( ARG_KEY key )
	{
		return FindOrCreateEntry(key)->GetValue();
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::SetAt( ARG_KEY key, ARG_VALUE newValue )
	{
		FindOrCreateEntry(key)->SetValue( newValue );
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline bool CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::Lookup( ARG_KEY key, VALUE& rValue ) const
	{
		// Make it thread-safe
		LOCK_THREAD_CONST();

		DWORD		dwHashValue;
		CMapEntry*	pEntry;

		pEntry = GetEntry( key, dwHashValue );
		if ( pEntry != NULL )
		{
			rValue = pEntry->GetValue();
			return true;
		}
		return false;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Iteration Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline ARG_VALUE CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetNextAssoc( POSITION& pos ) const
	{
		// Check Assumptions
		_ASSERTE( pos != NULL );

		LPMAPENTRY pEntry = reinterpret_cast<LPMAPENTRY>(pos);

		// Make it thread-safe
		LOCK_THREAD_CONST();

		// Get the next entry
		pos = LookupNextEntry( pEntry );

		// Return the value
		return pEntry->GetValue();
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline ARG_KEY CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetKeyAt( POSITION pos ) const
	{
		// Check Assumptions
		_ASSERTE( pos != NULL );

		LPMAPENTRY pEntry = reinterpret_cast<LPMAPENTRY>(pos);

		// Make it thread-safe
		LOCK_THREAD_CONST();

		// Return the value
		return pEntry->GetKey();
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Operators
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	inline ARG_VALUE CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::operator[]( ARG_KEY key )
	{
		// Return the value
		return GetAt(key);
	}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CCMSmap Non-Inlined Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Map Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	bool CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::RemoveKey( ARG_KEY key )
	{
		// Make it thread-safe
		LOCK_THREAD();

		bool bReturnMe = false;

		// Find the hash value for this key
		DWORD dwHashValue = HashKey<ARG_KEY>(key) % m_dwHashTableSize;

		if ( m_ppHashTable != NULL )
		{
			CMapEntry* pEntry = m_ppHashTable[dwHashValue];

			// Make sure there is actually an entry
			if ( pEntry != NULL )
			{
				// Check to see if it is the first entry in the table
				if ( pEntry->GetKey() == key )
				{
					// Remove it and clean up
					m_ppHashTable[dwHashValue] = pEntry->GetNext();

					// Return Success
					bReturnMe = true;
				}
				else
				{
					// Find it
					CMapEntry* pPrevEntry = pEntry;

					// Search the bit bucket for the desired entry
					pEntry = pEntry->GetNext();
					while ( (pEntry != NULL) && (bReturnMe == false) )
					{
						if ( pEntry->GetKey() == key )
						{
							// Point the previous at the one after the one being deleted
							pPrevEntry->SetNext( pEntry->GetNext() );

							// Return Success
							bReturnMe = true;
						}
						else
						{
							// Move on to the next record (if there is one)
							pEntry = pEntry->GetNext();
						}
					}
				}

				// If the entry was found, remove it
				if ( bReturnMe == true )
				{
					// If an entry is being removed, the count MUST be greater than 0
					_ASSERTE( m_dwEntryCount > 0 );

					// Delete the entry
					DestroyMapEntry( pEntry );

					// If no entries left, clean up completely
					if ( m_dwEntryCount == 0 )
					{
						RemoveAll();
					}
				}
			}
		}
		return bReturnMe;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::RemoveAll()
	{
		// Make it thread-safe
		LOCK_THREAD();

		// Check assumptions
		_ASSERTE( (m_dwEntryCount == 0) || ((m_ppHashTable != NULL) && (m_dwEntryCount > 0)) );

		// If there is any data, remove it
		if ( m_ppHashTable != NULL )
		{
			DEBUG_MAP_TEXT( _T("Destroying the CMSmap at 0x%08x with %d elements.\n"),
				m_ppHashTable, m_dwEntryCount );

			// For each bit bucket in the table
			for ( unsigned register nCounter = 0 ; nCounter < m_dwHashTableSize ; nCounter++ )
			{
				CMapEntry*	pNext	= NULL;
				CMapEntry*	pEntry;

				// Delete the linked list of entries in the bucket
				for ( pEntry = m_ppHashTable[nCounter] ; pEntry != NULL ; pEntry = pNext )
				{
					pNext = pEntry->GetNext();
					DestroyMapEntry( pEntry );
				}
			}

			// Free up the hash table itself
			delete [] m_ppHashTable;
			m_ppHashTable = NULL;
		}

		// Release everything else
		m_pFreeList = NULL;
		m_pDataChain->RemoveAll( m_pDataChain );
		m_pDataChain = NULL;

		// Assign the entry count
		m_dwEntryCount = 0;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::SetHashTableSize( const DWORD& dwHashTableSize )
	{
		// Make it thread-safe
		LOCK_THREAD();

		// Display some debugging information
		DEBUG_MAP_TEXT( _T("Resizing Hash Table. Old Size = %d, New Size = %d\n"),
			m_dwHashTableSize, dwHashTableSize );

		// Don't resize if not necessary
		if ( dwHashTableSize != m_dwHashTableSize )
		{
			// Don't move entries if not necessary
			if ( m_dwEntryCount > 0 )
			{
				CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>		mapNew( dwHashTableSize, m_dwBlockCount );
				KEY				key;
				VALUE			value;
				POSITION		pos;

				// Loop through each entry
				pos = GetStartPosition();
				while ( pos != NULL )
				{
					// Get the entry
					GetNextAssoc( pos, key, value );

					// Add it to the temp map
					mapNew.SetAt( key, value );
				}

				// Remove all the old entries
				RemoveAll();

				// Move the data from the temp map
				m_ppHashTable		= mapNew.m_ppHashTable;
				m_dwHashTableSize	= mapNew.m_dwHashTableSize;
				m_dwEntryCount		= mapNew.m_dwEntryCount;
				m_pDataChain		= mapNew.m_pDataChain;
				m_pFreeList			= mapNew.m_pFreeList;

				// Clear out the temp map
				mapNew.m_ppHashTable		= NULL;
				mapNew.m_dwHashTableSize	= 0;
				mapNew.m_dwEntryCount		= 0;
				mapNew.m_pDataChain			= NULL;
				mapNew.m_pFreeList			= NULL;
			}
			else
			{
				// Just reinitialize the hash table
				InitHashTable( dwHashTableSize );
			}
		}
		DEBUG_ELSE_TEXT( _T("Trying to resize hash table to current size!\n") );

		// Display some debugging information
		DEBUG_MAP_TEXT( _T("Successfully resized the Hash Table!\n") );
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Iteration Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	POSITION CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetStartPosition() const
	{
		POSITION			pReturnMe	= NULL;
		unsigned register	nCounter;

		// Make it thread-safe
		LOCK_THREAD_CONST();

		// If there are any entries
		if ( m_ppHashTable != NULL )
		{
			// Loop through each entry in the hash table
			for ( nCounter = 0 ; 
					( (nCounter < m_dwHashTableSize) && (pReturnMe == NULL) ) ;
					nCounter++ )
			{
				// If it's not empty, use that as the starting point
				if ( m_ppHashTable[nCounter] != NULL )
				{
					pReturnMe = reinterpret_cast<POSITION>(m_ppHashTable[nCounter]);
				}
			}
		}

		// Return the starting point (or NULL)
		return pReturnMe;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetNextAssoc( 
		POSITION& pos, KEY& rKey, VALUE& rValue ) const
	{
		// Make it thread-safe
		LOCK_THREAD_CONST();

		// Proceed if the supplied entry pointer is valid
		if ( pos != NULL )
		{
			LPMAPENTRY pEntry = reinterpret_cast<LPMAPENTRY>(pos);

			// Make it thread-safe
			LOCK_THREAD_CONST();

			// Set the values from that entry
			rKey	= pEntry->GetKey();
			rValue	= pEntry->GetValue();

			// Get the next entry
			pos = LookupNextEntry( pEntry );
		}
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	POSITION CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::LookupNextEntry( const CMapEntry*& pEntry ) const
	{
		LPMAPENTRY	pNextEntry	= NULL;

		// Proceed if the supplied entry pointer is valid
		_ASSERTE( pEntry != NULL );

		// Make it thread-safe
		LOCK_THREAD_CONST();

		// If there is another item in this entry's list, use that for the next
		if ( pEntry->GetNext() != NULL )
		{
			pNextEntry	= pEntry->GetNext();
		}
		else
		{
			// Otherwise, search for the next item starting with the
			// the location in the hash table just after this entry
			unsigned register nCounter;

			for ( nCounter = pEntry->GetHashValue()+1 ;
					( (nCounter < m_dwHashTableSize) && (pNextEntry == NULL) ) ; nCounter++ )
			{
				// If it's not empty, use that as the next entry
				if ( m_ppHashTable[nCounter] != NULL )
				{
					pNextEntry = m_ppHashTable[nCounter];
				}
			}
		}

		// Set the entry point to be the next entry (or NULL)
		return reinterpret_cast<POSITION>(pNextEntry);
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Internal Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::InitHashTable( const DWORD& dwHashTableSize )
	{
		// Clean up the existing hash table (if necessary)
		RemoveAll();

		// Display some debugging information
		DEBUG_MAP_TEXT( _T("Initializing Hash Table.  Size = %d\n"), dwHashTableSize );

		// Check for valid conditions
		_ASSERTE( m_dwEntryCount == 0 );
		_ASSERTE( dwHashTableSize > 0 );
		_ASSERTE( m_ppHashTable == NULL );

		m_ppHashTable = new CMapEntry*[dwHashTableSize];
		::ZeroMemory( m_ppHashTable, (sizeof(CMapEntry*) * dwHashTableSize) );
		m_dwHashTableSize = dwHashTableSize;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CMapEntry*
		CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::FindOrCreateEntry( ARG_KEY key )
	{
		CMapEntry*	pEntry;
		DWORD		dwHashValue;

		// Make it thread-safe
		LOCK_THREAD();

		// Find the existing entry or create one
		if ( (pEntry = GetEntry(key, dwHashValue)) == NULL )
		{
			// Initialize the table (if necessary)
			if ( m_ppHashTable == NULL )
			{
				InitHashTable( m_dwHashTableSize );
			}

			// Display Debugging Information
			DEBUG_MAP_TEXT( _T("Inserting map item at index #%d%s.  Entries = %d, TableSize = %d\n"), 
				dwHashValue, 
				((m_ppHashTable[dwHashValue] == NULL) ? _T(" (EMPTY!)") : _T("")),
				m_dwEntryCount, m_dwHashTableSize );

			// Add the new entry
			pEntry = CreateMapEntry( key, dwHashValue, m_ppHashTable[dwHashValue] );

			// Drop it into the appropriate bit-bucket
			m_ppHashTable[dwHashValue] = pEntry;
		}

		// Return the entry
		return pEntry;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CMapEntry*
		CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::GetEntry( ARG_KEY key, DWORD& dwHashValue ) const
	{
		CMapEntry*	pReturnMe = NULL;

		// Find the hash value for this key (this needs to be done even if table is empty)
		dwHashValue = HashKey<ARG_KEY>(key) % m_dwHashTableSize;

		// Only proceed if there entries in the table
		if ( m_ppHashTable != NULL )
		{
			CMapEntry* pEntry;

			// Search the bit bucket for the desired entry
			for ( pEntry = m_ppHashTable[dwHashValue] ;
				( (pEntry != NULL) && (pReturnMe == NULL) ) ; pEntry = pEntry->GetNext() )
			{
				if ( pEntry->GetKey() == key )
				{
					pReturnMe = pEntry;
				}
			}
		}

		// Return the result (or NULL)
		return pReturnMe;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Create / Destroy Functions
	///////////////////////////////////////////////////////////////////////////////////////////////////
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CMapEntry*
		CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::CreateMapEntry( 
			ARG_KEY key, const DWORD& dwHashValue, CMapEntry* pNext )
	{
		// Check to see if any free nodes are available
		if ( m_pFreeList == NULL )
		{
			DEBUG_MAP_TEXT( _T("CMSmap: Allocating %d blocks, Size = %d bytes\n"), 
				m_dwBlockCount, sizeof(CCMSmap::CMapEntry) );

			CDataChain* pNewBlock = CDataChain::Add( m_pDataChain, 
				(m_dwBlockCount * sizeof(CCMSmap::CMapEntry)) );

			// Link them together
			LPMAPENTRY pEntry = reinterpret_cast<LPMAPENTRY>(pNewBlock->GetData());
			for ( unsigned register nCounter = 0 ; nCounter < m_dwBlockCount ; nCounter++ )
			{
				pEntry->m_pNext = m_pFreeList;
				m_pFreeList		= pEntry;
				pEntry++;
			}
		}

		// There must be free blocks now
		_ASSERTE( m_pFreeList != NULL );

		// Get the entry from the FreeList
		LPMAPENTRY	pNewEntry = m_pFreeList;
		m_pFreeList = m_pFreeList->m_pNext;

		// Initialize the new entry
		pNewEntry->Initialize();
		pNewEntry->SetKey( key );
		pNewEntry->SetHashValue( dwHashValue );
		pNewEntry->SetNext( pNext );

		// Increment the counter
		m_dwEntryCount++;

		// Make sure there is no overflow
		_ASSERTE( m_dwEntryCount > 0 );

		// Return the result
		return pNewEntry;
	}

	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::DestroyMapEntry( LPMAPENTRY pDestroyMe )
	{
		// Clean Up
		pDestroyMe->CleanUp();

		// Add it to the free list
		pDestroyMe->SetNext( m_pFreeList );
		m_pFreeList = pDestroyMe;

		// Decrement the entry count
		m_dwEntryCount--;
	}

	///////////////////////////////////////////////////////////////////////////////////////////////////
	// Debugging Operations
	///////////////////////////////////////////////////////////////////////////////////////////////////
#ifdef USE_MAP_DEBUG
	template<typename KEY, typename ARG_KEY, typename VALUE, typename ARG_VALUE>
	void CCMSmap<KEY, ARG_KEY, VALUE, ARG_VALUE>::DumpMapInfo() const
	{
		// Make it thread-safe
		LOCK_THREAD_CONST();

		DEBUG_MAP_TEXT( _T("CCMSMap @ 0x%08x: %d Entries, HashTable Size = %d\n"), this,
			m_dwEntryCount, m_dwHashTableSize );

		if ( m_ppHashTable != NULL )
		{
			LPMAPENTRY			pEntry;
			DWORD				dwEntryCount;
			CAverager<float>	avgEntries;

			for ( unsigned register nCounter = 0 ; nCounter < m_dwHashTableSize ; nCounter++ )
			{
				dwEntryCount = 0;
				pEntry = m_ppHashTable[nCounter];

				while ( pEntry != NULL )
				{
					dwEntryCount++;
					pEntry = pEntry->GetNext();
				}

				DEBUG_MAP_TEXT( _T("\tIndex #%-3d: %02d items\n"), nCounter, dwEntryCount );
				avgEntries.AddItem( static_cast<float>(dwEntryCount) );
			}

			CString strTemp;

			if ( (avgEntries.GetMaxValue() >= 4) || (m_dwEntryCount >= m_dwHashTableSize) )
			{
				strTemp = _T("Not big enough!");
			}
			else if ( avgEntries.GetAverage() <= 0.5 )
			{
				strTemp = _T("Too big");
			}
			else
			{
				strTemp = _T("Okay");
			}

			DEBUG_MAP_TEXT( _T("Average: %5.2f Entries Per Index\n"), avgEntries.GetAverage() );
			DEBUG_MAP_TEXT( _T("Max: %5.2f Entries Per Index\n"), avgEntries.GetMaxValue() );
			DEBUG_MAP_TEXT( _T("Min: %5.2f Entries Per Index\n"), avgEntries.GetMinValue() );
			DEBUG_MAP_TEXT( _T("Analysis: %s\n"), strTemp );
		}
		else
		{
			DEBUG_MAP_TEXT( _T("\tMap is empty!\n") );
		}
	}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Hashing Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

	template<typename ARG_KEY>
	inline DWORD HashKey( ARG_KEY key )
	{
		return ((DWORD)(void*)(DWORD)key) >> 4;
	}

	template<> inline DWORD HashKey<LPCSTR>( LPCSTR lpszKey )
	{
		DWORD dwHashValue = 0;
		while (*lpszKey)
		{
			dwHashValue = ( (dwHashValue << 5) + dwHashValue + *lpszKey++ );
		}
		return dwHashValue;
	}

	template<> inline DWORD HashKey<LPCWSTR>( LPCWSTR lpszKey )
	{
		DWORD dwHashValue = 0;
		while (*lpszKey)
		{
			dwHashValue = ( (dwHashValue << 5) + dwHashValue + *lpszKey++ );
		}
		return dwHashValue;
	}

	template<> inline DWORD HashKey<const CString&>( const CString& strKey )
	{
		return HashKey( static_cast<LPCTSTR>(strKey) );
	}

	template<> inline DWORD HashKey<CString&>( CString& strKey )
	{
		return HashKey( static_cast<LPCTSTR>(strKey) );
	}

	template<> inline DWORD HashKey<CString>( CString strKey )
	{
		return HashKey( static_cast<LPCTSTR>(strKey) );
	}

#ifdef _INC_COMUTIL
	template<> inline DWORD HashKey<BSTR>( BSTR bstrKey )
	{
		DWORD dwHashValue = 0;
		while (*bstrKey)
		{
			dwHashValue = ( (dwHashValue << 5) + dwHashValue + *bstrKey++ );
		}
		return dwHashValue;
	}

	template<> inline DWORD HashKey<const _bstr_t&>( const _bstr_t& bstrKey )
	{
		return HashKey( static_cast<LPCTSTR>(bstrKey) );
	}

	template<> inline DWORD HashKey<_bstr_t&>( _bstr_t& bstrKey )
	{
		return HashKey( static_cast<LPCTSTR>(bstrKey) );
	}

	template<> inline DWORD HashKey<_bstr_t>( _bstr_t bstrKey )
	{
		return HashKey( static_cast<LPCTSTR>(bstrKey) );
	}
#endif // #ifdef _INC_COMUTIL

#ifdef __ATLBASE_H__
	template<> inline DWORD HashKey<CComBSTR&>( CComBSTR& bstrKey )
	{
		return HashKey( bstrKey.m_str );
	}
#endif // #ifdef __ATLBASE_H__

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Namespace Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef NO_CMS_NAMESPACES
}	// End of CMSmap Namespace
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_CMSMAP_H__C062FFE9_6C76_11D4_A461_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of CMSmap.h
///////////////////////////////////////////////////////////////////////////////////////////////////////
