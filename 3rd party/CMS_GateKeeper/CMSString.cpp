///////////////////////////////////////////////////////////////////////////////////////////////////////
// CMSString.cpp: implementation of the CCMSString class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 by Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "CMSString.h"
#include <stdio.h>
#include <stdarg.h>

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
#undef THIS_FILE
static TCHAR THIS_FILE[]=	_T(__FILE__);
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef ULONG_MAX
	#define ULONG_MAX     0xffffffffUL  // Maximum Unsigned Long Value
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define GET_BLOCK_SIZE(x)	(ROUND_TO_NEAREST(((((x)+1) * sizeof(TCHAR)) + sizeof(CCMSStringData)),4))

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Static Variables
///////////////////////////////////////////////////////////////////////////////////////////////////////

CReservedBlocks		CCMSString::m_Blocks64( GET_BLOCK_SIZE(64) );
CReservedBlocks		CCMSString::m_Blocks128( GET_BLOCK_SIZE(128) );
CReservedBlocks		CCMSString::m_Blocks256( GET_BLOCK_SIZE(256) );
CReservedBlocks		CCMSString::m_Blocks512( GET_BLOCK_SIZE(512) );
DWORD				CCMSString::m_pEmptyInitData[] = { NO_REF_COUNT, 0, 0, NULL };
CCMSStringData*		CCMSString::m_pEmptyData = (CCMSStringData*)&(CCMSString::m_pEmptyInitData);
LPCTSTR				CCMSString::m_pEmptyString = CCMSString::m_pEmptyData->GetData();
HINSTANCE			CCMSString::m_hInstance = NULL;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Construction / Destruction
///////////////////////////////////////////////////////////////////////////////////////////////////////

	// Defined Inline

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Memory Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CCMSString::AllocBuffer( const DWORD& dwLength )
{
	// Check Assumptions
	_ASSERTE( dwLength < ULONG_MAX );

	//DEBUG_TEXT( _T("Allocating String = %d\n"), dwLength );
	if ( dwLength > 0 )
	{
		CCMSStringData*	pData;

		if ( dwLength <= 64 )
		{
			pData = reinterpret_cast<CCMSStringData*>(m_Blocks64.Alloc());
			pData->dwAllocLength = 64;
		}
		else if ( dwLength <= 128 )
		{
			pData = reinterpret_cast<CCMSStringData*>(m_Blocks128.Alloc());
			pData->dwAllocLength = 128;
		}
		else if ( dwLength <= 256 )
		{
			pData = reinterpret_cast<CCMSStringData*>(m_Blocks256.Alloc());
			pData->dwAllocLength = 256;
		}
		else if ( dwLength <= 512 )
		{
			pData = reinterpret_cast<CCMSStringData*>(m_Blocks512.Alloc());
			pData->dwAllocLength = 512;
		}
		else
		{
			pData = reinterpret_cast<CCMSStringData*>(new BYTE[GET_BLOCK_SIZE(dwLength)]);
			pData->dwAllocLength = dwLength;
		}

		_ASSERTE( pData != NULL );
		m_lpszData				= pData->GetData();
		pData->dwRefCount		= 1;
		pData->dwDataLength		= dwLength;
		// Null terminate the string
		m_lpszData[dwLength]	= _T('\0');
	}
	else
	{
		// Point to the empty buffer
		Initialize();
	}
}

LPTSTR CCMSString::GetBuffer( DWORD dwMinLength )
{
	_ASSERTE( dwMinLength >= 0 );

	if ( IsDataShared() || (dwMinLength > GetData()->dwAllocLength) )
	{
		// Grow the buffer
		DWORD			dwOldLength	= GetLength();
		CCMSStringData* pOldData	= GetData();

		dwMinLength	= GetMax( dwMinLength, dwOldLength );
		AllocBuffer( dwMinLength );
		::CopyMemory( m_lpszData, pOldData->GetData(), ((dwOldLength+1)*sizeof(TCHAR)) );
		GetData()->dwDataLength = dwMinLength;
		CCMSString::Release( pOldData );
	}

	// Return the new string buffer
	_ASSERTE( m_lpszData != NULL );
	return m_lpszData;
}

void CCMSString::ReleaseBuffer( const DWORD& dwNewLength )
{
	CopyBeforeWrite();

	_ASSERTE( dwNewLength <= GetData()->dwAllocLength );
	GetData()->dwDataLength = dwNewLength;
	m_lpszData[dwNewLength] = _T('\0');
}

LPTSTR CCMSString::GetBufferSetLength( DWORD dwMinLength )
{
	// Check assumptions
	_ASSERTE( dwMinLength >= 0 );

	GetBuffer( dwMinLength );
	GetData()->dwDataLength = dwMinLength;
	m_lpszData[dwMinLength] = _T('\0');
	return m_lpszData;
}

void CCMSString::FreeExtra()
{
	_ASSERTE( GetLength() <= GetData()->dwAllocLength );
	if ( GetLength() != GetData()->dwAllocLength )
	{
		CCMSStringData* pOldData = GetData();

		AllocBuffer( GetLength() );
		::CopyMemory( m_lpszData, pOldData->GetData(), (pOldData->dwDataLength * sizeof(TCHAR)) );
		CCMSString::Release( pOldData );
	}
	_ASSERTE( m_lpszData[GetLength()] == _T('\0') );
	_ASSERTE( GetData() != NULL );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Concatination Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CCMSString::Concat( const DWORD& dwSourceLength, LPCTSTR lpszSource )
{
	if ( dwSourceLength > 0 )
	{
		if ( IsDataShared() || ((GetLength() + dwSourceLength) > GetData()->dwAllocLength) )
		{
			// Buffer is too small, use the ConcatCopy function
			CCMSStringData* pOldData = GetData();

			ConcatCopy( GetData()->dwDataLength, m_lpszData, dwSourceLength, lpszSource );

			_ASSERTE( pOldData != NULL );
			CCMSString::Release( pOldData );
		}
		else
		{
			// Copy the data
			::CopyMemory( m_lpszData + GetLength(), lpszSource, (dwSourceLength*sizeof(TCHAR)) );
			GetData()->dwDataLength += dwSourceLength;
			_ASSERTE( GetLength() <= GetData()->dwAllocLength );

			m_lpszData[GetLength()] = _T('\0');
		}
	}
}

void CCMSString::ConcatCopy( const DWORD& dwLength1, LPCTSTR lpszSource1, 
									const DWORD& dwLength2, LPCTSTR lpszSource2 )
{
	// Assumes the Releasing of the original string data is handled by the caller
	DWORD	dwNewLength = dwLength1 + dwLength2;

	// Allocate the buffer
	_ASSERTE( dwNewLength > 0 );
	AllocBuffer( dwNewLength );

	// Copy the Values
	::CopyMemory( m_lpszData, lpszSource1, dwLength1*sizeof(TCHAR) );
	::CopyMemory( m_lpszData + dwLength1, lpszSource2, dwLength2*sizeof(TCHAR) );
}

void CCMSString::AllocCopy( CCMSString& strTarget, const DWORD& dwCopyLength, const DWORD& dwStartAt, 
								const DWORD& dwExtraLength ) const
{
	DWORD dwNewLength = dwCopyLength + dwExtraLength;

	strTarget.Release();
	strTarget.AllocBuffer( dwNewLength );
	if ( dwNewLength > 0 )
	{
		::CopyMemory( strTarget.m_lpszData, m_lpszData + dwStartAt, (dwCopyLength*sizeof(TCHAR)) );
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Trim Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

void CCMSString::TrimLeft( LPCTSTR lpszTargets )
{
	CopyBeforeWrite();
	LPCTSTR lpsz = m_lpszData;

	// Check assumptions
	_ASSERTE( lpszTargets != NULL );

	while ( (*lpsz) != _T('\0') )
	{
		if ( _tcschr(lpszTargets, *lpsz) == NULL )
			break;
		lpsz = _tcsinc(lpsz);
	}

	// Correct the Data and Length
	if ( lpsz != m_lpszData )
	{
		DWORD dwDataLength = GetLength() - (lpsz - m_lpszData);
		::memmove( m_lpszData, lpsz, (dwDataLength+1)*sizeof(TCHAR) );
		GetData()->dwDataLength = dwDataLength;
	}
}

void CCMSString::TrimLeft( TCHAR chTarget )
{
	CopyBeforeWrite();
	LPCTSTR lpsz		= m_lpszData;

	while ( chTarget == (*lpsz) )
	{
		lpsz = _tcsinc( lpsz );
	}

	// Correct the Data and Length
	if ( lpsz != m_lpszData )
	{
		DWORD dwDataLength = GetLength() - (lpsz - m_lpszData);
		::memmove( m_lpszData, lpsz, (dwDataLength+1)*sizeof(TCHAR) );
		GetData()->dwDataLength = dwDataLength;
	}
}

void CCMSString::TrimLeft()
{
	CopyBeforeWrite();
	LPCTSTR lpsz		= m_lpszData;

	while ( _istspace(*lpsz) )
	{
		lpsz = _tcsinc(lpsz);
	}

	// Correct the Data and Length
	if ( lpsz != m_lpszData )
	{
		DWORD dwDataLength = GetLength() - (lpsz - m_lpszData);
		::memmove( m_lpszData, lpsz, (dwDataLength+1)*sizeof(TCHAR) );
		GetData()->dwDataLength = dwDataLength;
	}
}

void CCMSString::TrimRight( LPCTSTR lpszTargets )
{
	CopyBeforeWrite();
	LPTSTR lpsz		= m_lpszData;
	LPTSTR lpszLast	= NULL;

	// Check Assumptions
	_ASSERTE( lpszTargets != NULL );

	while ( (*lpsz) != _T('\0') )
	{
		if ( _tcschr(lpszTargets, *lpsz) != NULL )
		{
			if ( lpszLast == NULL )
			{
				lpszLast = lpsz;
			}
		}
		else
		{
			lpszLast = NULL;
		}
		lpsz = _tcsinc( lpsz );
	}

	if ( lpszLast != NULL )
	{
		*lpszLast = _T('\0');
		GetData()->dwDataLength = lpszLast - m_lpszData;
	}
}

void CCMSString::TrimRight( TCHAR chTarget )
{
	CopyBeforeWrite();
	LPTSTR lpsz		= m_lpszData;
	LPTSTR lpszLast	= NULL;

	while ( (*lpsz) != _T('\0') )
	{
		if ( (*lpsz) == chTarget )
		{
			if ( lpszLast == NULL )
			{
				lpszLast = lpsz;
			}
		}
		else
		{
			lpszLast = NULL;
		}
		lpsz = _tcsinc( lpsz );
	}

	if ( lpszLast != NULL )
	{
		*lpszLast = _T('\0');
		GetData()->dwDataLength = lpszLast - m_lpszData;
	}
}

void CCMSString::TrimRight()
{
	CopyBeforeWrite();
	LPTSTR lpsz		= m_lpszData;
	LPTSTR lpszLast	= NULL;

	while ( (*lpsz) != _T('\0') )
	{
		if ( _istspace(*lpsz) )
		{
			if ( lpszLast == NULL )
			{
				lpszLast = lpsz;
			}
		}
		else
		{
			lpszLast = NULL;
		}
		lpsz = _tcsinc( lpsz );
	}

	if ( lpszLast != NULL )
	{
		*lpszLast = _T('\0');
		GetData()->dwDataLength = lpszLast - m_lpszData;
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Fancy Manipulation Operations
///////////////////////////////////////////////////////////////////////////////////////////////////////

DWORD CCMSString::Delete( const DWORD& dwIndex, const DWORD& dwCount )
{
	DWORD dwNewLength = GetData()->dwDataLength;

	if ( (dwCount > 0) && (dwIndex < dwNewLength) )
	{
		CopyBeforeWrite();
		DWORD dwBytesToCopy = dwNewLength - (dwIndex + dwCount) + 1;

		::MoveMemory( m_lpszData + dwIndex, m_lpszData + dwIndex + dwCount, 
						(dwBytesToCopy * sizeof(TCHAR)) );
		GetData()->dwDataLength = dwNewLength - dwCount;
	}
	return dwNewLength;
}

DWORD CCMSString::Insert( DWORD dwIndex, TCHAR ch )
{
	DWORD dwNewLength;

	// Copy the string if it is being used more than once
	CopyBeforeWrite();

	// If the index is too large, use the largest allowed value
	dwNewLength = GetLength();
	if ( dwIndex > dwNewLength )
	{
		dwIndex = dwNewLength;
	}
	dwNewLength++;

	// Check to see if enough memory is allocated
	if ( GetData()->dwAllocLength < dwNewLength )
	{
		// Record the old buffer
		CCMSStringData* pOldData	= GetData();
		LPTSTR			pstr		= m_lpszData;

		// Allocate a buffer that is large enough
		AllocBuffer( dwNewLength );

		// Copy the old string
		::CopyMemory( m_lpszData, pstr, ((pOldData->dwDataLength+1) * sizeof(TCHAR)) );

		// Release the old string
		CCMSString::Release( pOldData );
	}

	// Move the memory for the chars after the insertion point
	::MoveMemory( m_lpszData + dwIndex + 1, m_lpszData + dwIndex, 
						((dwNewLength-dwIndex) * sizeof(TCHAR)) );

	// Insert the new char
	m_lpszData[dwIndex] = ch;

	// Null Terminate
	m_lpszData[dwNewLength] = _T('\0');

	// Record the new length and return it
	GetData()->dwDataLength = dwNewLength;
	return dwNewLength;
}

/*
int	CCMSString::Insert( int nIndex, LPCTSTR pstr )
{
}

int	CCMSString::Remove( TCHAR chRemove )
{
}

int	CCMSString::Replace( LPCTSTR lpszOld, LPCTSTR lpszNew )
{
}

int	CCMSString::Replace( TCHAR chOld, TCHAR chNew )
{
}
*/

///////////////////////////////////////////////////////////////////////////////////////////////////////
// String Loading / Formatting Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef _UNICODE
#define CHAR_FUDGE 1    // one TCHAR unused is good enough
#else
#define CHAR_FUDGE 2    // two BYTES unused for case of DBC last char
#endif

bool CCMSString::LoadString( const DWORD& dwResID )
{
	// Try a buffer of 256, then keep adding 256 until
	// it fits completely.
	int	nBufferSize		= 0;
	int	nStringLength	= 0;
	do
	{
		nBufferSize += 128;
		nStringLength = LoadStringFromResource( dwResID, GetBuffer(nBufferSize-1), nBufferSize );
	}
	while ( nBufferSize - nStringLength <= CHAR_FUDGE );
	ReleaseBuffer();

	return bool(nStringLength > 0);
}

void CCMSString::Format( LPCTSTR lpszFormat, ... )
{
	va_list argList;

	va_start( argList, lpszFormat );

	// Try a buffer of 256, then keep adding 256 until
	// it fits completely.
	int	nBufferSize		= 0;
	int	nStringLength	= 0;
	do
	{
		nBufferSize += 128;
		nStringLength = _vsntprintf( GetBuffer(nBufferSize-1), nBufferSize, lpszFormat, argList );
	}
	while ( nStringLength < 0 );
	ReleaseBuffer( nStringLength );
	va_end( argList );
}

void CCMSString::Format( DWORD dwResID, ... )
{
	CCMSString	strFormat;
	va_list		argList;

	strFormat.LoadString( dwResID );
	va_start( argList, dwResID );

	// Try a buffer of 256, then keep adding 256 until
	// it fits completely.
	int	nBufferSize		= 0;
	int	nStringLength	= 0;
	do
	{
		nBufferSize += 128;
		nStringLength = _vsntprintf( GetBuffer(nBufferSize-1), nBufferSize, strFormat, argList );
	}
	while ( nStringLength < 0 );
	ReleaseBuffer( nStringLength );
	va_end( argList );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Converstion Utilities
///////////////////////////////////////////////////////////////////////////////////////////////////////

LPSTR CCMSString::GetStringA( LPSTR lpszTarget, const DWORD& dwSize ) const
{
	_ASSERTE( lpszTarget != NULL );
	_ASSERTE( dwSize >= GetLength() );

#ifdef _UNICODE
	// Convert from Unicode to ANSI
	DEBUG_FAIL_ON_ZERO( ::WideCharToMultiByte(CP_ACP, 0, m_lpszData, GetLength()+1, lpszTarget, 
								dwSize, NULL, NULL) );
#else
	// Copy the string
	lstrcpyn( lpszTarget, m_lpszData, dwSize );
	// And NULL Terminate it
	lpszTarget[dwSize-1] = '\0';
#endif

	return lpszTarget;
}

LPWSTR CCMSString::GetStringW( LPWSTR lpszTarget, const DWORD& dwSize ) const
{
	_ASSERTE( lpszTarget != NULL );
	_ASSERTE( dwSize >= GetLength() );

#ifdef _UNICODE
	// Copy the string
	lstrcpyn( lpszTarget, m_lpszData, dwSize );
	// And NULL Terminate it
	lpszTarget[dwSize-1] = L'\0';
#else
	// Convert from ANSI to Unicode
	DEBUG_FAIL_ON_ZERO( ::MultiByteToWideChar(CP_ACP, 0, m_lpszData, GetLength()+1, 
								lpszTarget, dwSize) );
#endif

	return lpszTarget;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of CMSString.cpp
///////////////////////////////////////////////////////////////////////////////////////////////////////
