///////////////////////////////////////////////////////////////////////////////////////////////////////
// DevMode.h: interface for the CDevMode class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_DEVMODE_H__8AE14067_D8D7_11D3_A427_00105A1C588D__INCLUDED_)
#define AFX_DEVMODE_H__8AE14067_D8D7_11D3_A427_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "ReservedBlocks.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define DEVMODE_FIELD_FUNCTIONS(name,flag)		\
	const bool		Is##name##Set() const				{ return CheckDMField(flag); }; \
	const short&	Get##name() const					{ return m_lpDevMode->dm##name; }; \
	void			Set##name( const short& nValue )	{ m_lpDevMode->dm##name = nValue; SetDMField(flag); }; \
	void			Clear##name()						{ m_lpDevMode->dm##name = 0; ClearDMField(flag); };

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

// 1 Inch = 25.40 Millimeters
#define CONST_TENTHS_OF_MILLIMETERS_PER_INCH			(254)
#define CONST_INCHES_PER_MILLIMETER						(0.0393701f)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Utility functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline short Round( const float& nRoundMe )
{
	// Adding 0.5f forces it to round, instead of just truncating
	return (short)( nRoundMe + 0.5f );
}

// Converting to Tenths of a Millimeter
inline short InchesToTenthsOfAMillimeter( const short& nInches )
{
	return (nInches * CONST_TENTHS_OF_MILLIMETERS_PER_INCH);
}

inline short InchesToTenthsOfAMillimeter( const float& nInches )
{
	return Round( nInches * ((float)CONST_TENTHS_OF_MILLIMETERS_PER_INCH) );
}

inline short MillimetersToTenthsOfAMillimeter( const float& nMillimeters )
{
	return Round( nMillimeters * 10 );
}

inline short TenthsOfAMillimeterToTenthsOfAMillimeter( const float& nTenthsOfAMillimeter )
{
	return Round(nTenthsOfAMillimeter);
}

// Converting to Inches
inline short InchesToInches( const float& nInches )
{
	return Round(nInches);
}

inline short MillimetersToInches( const short& nMillimeters )
{
	return Round( nMillimeters * CONST_INCHES_PER_MILLIMETER );
}

inline short MillimetersToInches( const float& nMillimeters )
{
	return Round( nMillimeters * CONST_INCHES_PER_MILLIMETER );
}

inline short TenthsOfAMillimeterToInches( const short& nTenthsOfAMillimeter )
{
	return MillimetersToInches( ((float)nTenthsOfAMillimeter) / 10.f );
}

inline short TenthsOfAMillimeterToInches( const float& nTenthsOfAMillimeter )
{
	return MillimetersToInches( nTenthsOfAMillimeter / 10.f );
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// PaperSizeInfo Structure
///////////////////////////////////////////////////////////////////////////////////////////////////////

typedef struct
{
	LPCSTR	lpszFormName;
	short	nDevModeValue;
} PaperSizeInfo;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CDevMode Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CDevMode
{
// Attributes
private:
	LPDEVMODE		m_lpDevMode;

// Static Attributes
private:
	static CReservedBlocks			m_DevModeBlocks;

// Construction / Destruction
public:
	CDevMode()														{ m_lpDevMode = NULL; AllocBuffer(); };
	CDevMode( const CDevMode& rDevMode )							{ m_lpDevMode = NULL; Copy(rDevMode); };
	CDevMode( const DEVMODE& dmCopyMe )								{ m_lpDevMode = NULL; Copy(dmCopyMe); };
	CDevMode( const LPDEVMODE pCopyMe )								{ m_lpDevMode = NULL; Copy(pCopyMe); };
	virtual ~CDevMode()												{ ReleaseBuffer(); };

// Maintains the DevMode field bitmask
private:														
	const bool		CheckDMField( const DWORD& dwField ) const		{ return ((m_lpDevMode->dmFields & dwField) != 0); };
	void			SetDMField( const DWORD& dwField )				{ m_lpDevMode->dmFields |= dwField; };
	void			ClearDMField( const DWORD& dwField )			{ m_lpDevMode->dmFields &= ~dwField; };

// Memory Operations
private:
	void			AllocBuffer( const WORD& wSize = sizeof(DEVMODE), const WORD& wDriverExtra = 0 );
	void			ReleaseBuffer();

// Operations
public:
	void			Copy( const CDevMode& rDevMode )				{ Copy((const LPDEVMODE)(*this)); };
	void			Copy( const DEVMODE& dmCopyMe )					{ Copy((const LPDEVMODE)&dmCopyMe); };
	void			Copy( const LPDEVMODE pCopyMe )
	{
		_ASSERTE( pCopyMe != NULL );

		// Allocate the memory
		AllocBuffer( pCopyMe->dmSize, pCopyMe->dmDriverExtra );

		// Copy the structures
		::CopyMemory( m_lpDevMode, pCopyMe, (pCopyMe->dmSize + pCopyMe->dmDriverExtra) );
	}

	void			Clear()
	{
		if ( m_lpDevMode != NULL )
		{
			WORD	wSize			= m_lpDevMode->dmSize;
			WORD	wDriverExtra	= m_lpDevMode->dmDriverExtra;

			// Clear out the old values
			::ZeroMemory( m_lpDevMode, (wSize + wDriverExtra) );

			// Reset the sizes
			m_lpDevMode->dmSize			= wSize;
			m_lpDevMode->dmDriverExtra	= wDriverExtra;
		}
	}

// Operators
public:
	CDevMode&	operator=( const CDevMode& rDevMode )				{ Copy(rDevMode);	return (*this); };
	CDevMode&	operator=( const DEVMODE& rCopyMe )					{ Copy(rCopyMe);	return (*this); };
	CDevMode&	operator=( const LPDEVMODE pCopyMe )				{ Copy(pCopyMe);	return (*this); };
				operator DEVMODE&()									{ return (*m_lpDevMode); };
				operator const LPDEVMODE() const					{ return m_lpDevMode; };
				operator LPDEVMODE()								{ return m_lpDevMode; };

// Data Access functions to manipulate other members
public:
	const bool		AreAnyFieldsSet() const							{ return bool(m_lpDevMode->dmFields != 0); };
	// FormName
	const bool		IsFormNameSet() const							{ return CheckDMField(DM_FORMNAME); };
	LPCTSTR			GetFormName() const								{ return (LPCTSTR)m_lpDevMode->dmFormName;			};
	void			ClearFormName()									{ ::ZeroMemory(m_lpDevMode->dmFormName,sizeof(m_lpDevMode->dmFormName)); ClearDMField(DM_FORMNAME); };
	void			SetFormName( LPCTSTR lpszFormName )
	{
		// Check to see if it is too long
		if ( _tcslen(lpszFormName) < CCHFORMNAME )
		{
			// It is not too long, so just copy it
			_tcscpy( (LPTSTR)m_lpDevMode->dmFormName, lpszFormName );
		}
		else
		{
			// Copy the string up the maximum length that dmFormName can support
			_tcsncpy( (LPTSTR)m_lpDevMode->dmFormName, lpszFormName, CCHFORMNAME-1 );
			// Make sure to terminate the string
			m_lpDevMode->dmFormName[CCHFORMNAME-1] = 0x00;
		}

		// Set the flag
		SetDMField( DM_FORMNAME );
	}

// Special Operations
public:
	void			SetPaperSizeString( const PaperSizeInfo* pInfo, LPCSTR lpszPaperSize );
	
// DevMode Field Functions
public:
	DEVMODE_FIELD_FUNCTIONS( Orientation,	DM_ORIENTATION		)
	DEVMODE_FIELD_FUNCTIONS( PaperSize,		DM_PAPERSIZE		)
	DEVMODE_FIELD_FUNCTIONS( PaperLength,	DM_PAPERLENGTH		)
	DEVMODE_FIELD_FUNCTIONS( PaperWidth,	DM_PAPERWIDTH		)
	DEVMODE_FIELD_FUNCTIONS( Copies,		DM_COPIES			)
	DEVMODE_FIELD_FUNCTIONS( Color,			DM_COLOR			)
	DEVMODE_FIELD_FUNCTIONS( Duplex,		DM_DUPLEX			)
	DEVMODE_FIELD_FUNCTIONS( YResolution,	DM_YRESOLUTION		)
	DEVMODE_FIELD_FUNCTIONS( Collate,		DM_COLLATE			)
	DEVMODE_FIELD_FUNCTIONS( PrintQuality,	DM_PRINTQUALITY		)
	DEVMODE_FIELD_FUNCTIONS( Scale,			DM_SCALE			)
	DEVMODE_FIELD_FUNCTIONS( DefaultSource, DM_DEFAULTSOURCE	)

// Static Debug Information
#ifdef USE_DEBUG_OUTPUT

// Helper Macro
#define DM_STATUS(x)		( (lpDM->dmFields & (x)) ? _T("") : _T(" (Not Set)") )

public:
	inline static void	DebugDevMode( LPCTSTR lpszTitle, const LPDEVMODE lpDM )
	{
		if ( lpDM != NULL )
		{
			DEBUG_TEXT(	_T("DEVMODE \"%s\" at 0x%08x\n")
						_T("    Device Name:        %s\n")
						_T("    Spec Version:       %d\n")
						_T("    Driver Version:     %d\n")
						_T("    Size:               %d (Defined = %d)\n")
						_T("    Driver Extra:       %d\n")
						_T("    Fields:             0x%08x\n")
						// Status Fields
						_T("    Orientation:        %s%s\n")
						_T("    Paper Size:         #%d%s\n")
						_T("    Paper Length:       %d%s\n")
						_T("    Paper Width:        %d%s\n")
						_T("    Scale:              %d%s\n")
						_T("    Copies:             %d%s\n")
						_T("    Def Source:         %d%s\n")
						_T("    Print Quality:      %d%s\n")
						_T("    Color:              %s%s\n")
						_T("    Duplex:             %s%s\n")
						_T("    Y Resolution:       %d%s\n") 
						_T("    TT Option:          %s%s\n")
						_T("    Collate:            %s%s\n")
						_T("    Form Name:          %s%s\n")
						// End Status Fields
					, lpszTitle, lpDM
					, lpDM->dmDeviceName
					, lpDM->dmSpecVersion
					, lpDM->dmDriverVersion
					, lpDM->dmSize, sizeof(DEVMODE)
					, lpDM->dmDriverExtra
					, lpDM->dmFields
					// Begin checking validity of value by checking dmFields
					, ((lpDM->dmOrientation == DMORIENT_LANDSCAPE) ? _T("Landscape") : _T("Portrait")), DM_STATUS(DM_ORIENTATION)
					, lpDM->dmPaperSize, DM_STATUS(DM_PAPERSIZE)
					, lpDM->dmPaperLength, DM_STATUS(DM_PAPERLENGTH)
					, lpDM->dmPaperWidth, DM_STATUS(DM_PAPERWIDTH)
					, lpDM->dmScale, DM_STATUS(DM_SCALE)
					, lpDM->dmCopies, DM_STATUS(DM_COPIES)
					, lpDM->dmDefaultSource, DM_STATUS(DM_DEFAULTSOURCE)
					, lpDM->dmPrintQuality, DM_STATUS(DM_PRINTQUALITY)
					, ((lpDM->dmColor == DMCOLOR_COLOR) ? _T("Color") : _T("Monochrome")), DM_STATUS(DM_COLOR)
					, ((lpDM->dmDuplex == DMDUP_SIMPLEX) ? _T("Simplex") : ((lpDM->dmDuplex == DMDUP_HORIZONTAL) ? _T("Duplexed Horizontally") : _T("Duplexed Vertically"))), DM_STATUS(DM_DUPLEX)
					, lpDM->dmYResolution, DM_STATUS(DM_YRESOLUTION)
					, ((lpDM->dmTTOption == DMTT_BITMAP) ? _T("Bitmaps") : ((lpDM->dmTTOption == DMTT_DOWNLOAD) ? _T("Download") : _T("Substitute"))), DM_STATUS(DM_TTOPTION)
					, ((lpDM->dmCollate == DMCOLLATE_TRUE) ? _T("True") : _T("False")), DM_STATUS(DM_COLLATE)
					, lpDM->dmFormName, DM_STATUS(DM_FORMNAME)

				);

			//if ( lpDM->dmDriverExtra > 0 ) 	{ DEBUG_DUMP( ((LPBYTE)lpDM) + lpDM->dmSize, lpDM->dmDriverExtra ); }
		}
		else
		{
			DEBUG_TEXT( _T("DEVMODE \"%s\" is Null!\n"), lpszTitle );
		}
	}

#endif // #ifdef USE_DEBUG_OUTPUT

};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Helper Macros
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef USE_DEBUG_OUTPUT
	#define	DEBUG_DEVMODE(text,dm)		CDevMode::DebugDevMode( (text), ((const LPDEVMODE)(dm)) )
#else
	#define DEBUG_DEVMODE(text,dm)		((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_DEVMODE_H__8AE14067_D8D7_11D3_A427_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of DevMode.h
///////////////////////////////////////////////////////////////////////////////////////////////////////