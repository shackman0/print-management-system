///////////////////////////////////////////////////////////////////////////////////////////////////////
// PrintJobRequest.h: interface for the CPrintJobRequest class.
//
// By Darrin W. Cullop (d.cullop@cardmeter.com)
// Copyright � 2000-2001 Card Meter Systems
///////////////////////////////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_PRINTJOBREQUEST_H__C2AAE7AA_7E8B_11D4_A46A_00105A1C588D__INCLUDED_)
#define AFX_PRINTJOBREQUEST_H__C2AAE7AA_7E8B_11D4_A46A_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Include File List
///////////////////////////////////////////////////////////////////////////////////////////////////////

#include "PrintProcessor.h"
#include "PrintJob.h"
#include "PrintJobPropertyNames.h"
#include "CMSString.h"
#include "PrintParser.h"
#include "Win9xHack.h"
#include "PrinterAPI.h"
#include "DebugObject.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef THIS_FILE
#define THIS_FILE				_T(__FILE__)
#define DEFINED_THIS_FILE
#endif

#ifdef USE_REQ_DEBUG
	#define DEBUG_REQ_TEXT				DEBUG_OBJ_TEXT
#else
	#define DEBUG_REQ_TEXT				((void)(0))
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Definitions
///////////////////////////////////////////////////////////////////////////////////////////////////////

#define	MAX_PROPERTY_COUNT		36

///////////////////////////////////////////////////////////////////////////////////////////////////////
// TypeDefs
///////////////////////////////////////////////////////////////////////////////////////////////////////

typedef CDefaultBuffer		CHandlerBuffer;

///////////////////////////////////////////////////////////////////////////////////////////////////////
// CPrintJobRequest Class Definition
///////////////////////////////////////////////////////////////////////////////////////////////////////

class CPrintJobRequest DEBUG_OBJECT_BASE
{
// Construction / Destruction
public:
	CPrintJobRequest();
	virtual ~CPrintJobRequest();

// Print Job functions
protected:
	BOOL		IsRequestGranted();
	void		SetUserProperties( LPCTSTR lpszUserName, LPCTSTR lpszDomainName );
	void		LoadAllProperties( HANDLE hPrinter, DWORD dwJobId, LPDEVMODE pAltDevMode );
	void		ParseRawData( const DWORD& dwMaxLength = 0 );
	BOOL		UpdateRequest( const DWORD& dwPages, const DWORD& dwCopies = 0, 
								const DWORD& dwPagesPerSide = 0 );
	void		UpdateRequest();

private:
	void		SetParserProperties();
	void		SetPrinterProperties( PRINTER_INFO_2* lpPrinterInfo );
	void		SetPrintJobProperties( JOB_INFO_2* lpJobInfo );
	BOOL		ReadServerName( CString& strServerName );
	void		ReadRegistry();
	bool		AlreadyTracked( JOB_INFO_2* pJobData );

// Overridable Functions
protected:
	virtual void	OnServerNameMissing()	= 0;
	virtual void	OnUpdateSuccess()		= 0;
	virtual void	OnUpdateFail()			= 0;

// Data Structure Access Functions
protected:
	PRINTER_INFO_2*		GetPrinterInfo()
	{
		_ASSERTE(m_PrinterData.GetData() != NULL);
		return reinterpret_cast<PRINTER_INFO_2*>(m_PrinterData.GetData());
	}
	JOB_INFO_2*			GetPrintJobInfo()
	{
		_ASSERTE(m_PrintJobData.Data() != NULL);
		return m_PrintJobData.Data();
	}

// Property Init Functions
private:
	void		CreateProperties();
	void		DestroyProperties();

// Set Property Functions
private:
	VARIANT&	GetNewProperty( const LPOLESTR lpszName );

protected:
	void		SetProperty( const LPOLESTR lpszName, LPCTSTR lpszValue );
	void		SetProperty( const LPOLESTR lpszName, BSTR bstrValue );
	void		SetProperty( const LPOLESTR lpszName, bool bValue );
	void		SetProperty( const LPOLESTR lpszName, DWORD dwValue );
	void		SetProperty( const LPOLESTR lpszName, long nValue );
	void		SetProperty( const LPOLESTR lpszName, short nValue );
	void		SetProperty( const LPOLESTR lpszName, const LPSYSTEMTIME pValue );
	void		SetProperty( const LPOLESTR lpszName, const DATE& dtValue );
	void		SetProperty( Vector* pRawData );

// Attributes
private:
	LONG					m_nPrintJobID;
	BSTR					m_pProperties[MAX_PROPERTY_COUNT];
	VARIANT					m_pValues[MAX_PROPERTY_COUNT];
	DWORD					m_dwProperties;
	CProcBuffer				m_PrinterData;
	bool					m_bAlwaysSendJobData;

protected:
	DWORD					m_dwPagesPrinted;
	DWORD					m_dwCopiesPrinted;
	bool					m_bJobAborted;
	CPrintJobInfo			m_PrintJobData;
	CPrintParserData		m_ParserData;
	Vector*					m_pRawData;
	bool					m_bParsed;
	bool					m_bAlreadyTracked;
};

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline void CPrintJobRequest::LoadAllProperties( HANDLE hPrinter, DWORD dwJobId, 
													LPDEVMODE pAltDevMode )
{
	m_PrintJobData.GetJobData( hPrinter, dwJobId, 2 );
	if ( AlreadyTracked(GetPrintJobInfo()) )
	{
		m_bAlreadyTracked = true;
	}
	else
	{
		LPDEVMODE	pInitialValues;

		CPrinterAPI::GetPrinterData( hPrinter, 2, m_PrinterData );

		pInitialValues = ( (pAltDevMode != NULL) ? pAltDevMode :
			( (GetPrintJobInfo()->pDevMode != NULL) ? 
					GetPrintJobInfo()->pDevMode : GetPrinterInfo()->pDevMode ));

		if ( pInitialValues != NULL )
		{
			m_ParserData.Copy(pInitialValues);
		}
	}
}

inline void CPrintJobRequest::ParseRawData( const DWORD& dwMaxLength )
{
	// If data has been provided, parse it
	if ( m_pRawData != NULL )
	{
		VectorData		vdRawData(*m_pRawData);			// Create the access object
		DWORD			dwDataLength;

		dwDataLength = ( (dwMaxLength == 0) ? vdRawData.Length() : dwMaxLength );

		if ( dwDataLength > 0 )
		{
			CPrintParser	theParser;

			DEBUG_REQ_TEXT( _T("Parsing %d Bytes of Data!\n"), dwDataLength );
			m_ParserData.SetBuffer( &(vdRawData[0]), dwDataLength );
			m_bParsed = theParser.Parse(&m_ParserData);
		}
		else
		{
			DEBUG_REQ_TEXT( _T("Cannot parse 0 bytes!\n") );
			m_bJobAborted = true;
		}
	}
	else
	{
		DEBUG_REQ_TEXT( _T("Cannot Parse because Raw Data Is Empty!\n") );
		m_bJobAborted = true;
	}
}

inline void CPrintJobRequest::SetUserProperties( LPCTSTR lpszUserName, LPCTSTR lpszDomainName )
{
	if ( lpszUserName != NULL )
	{
		SetProperty( PRINTJOB_PROP_USER_NAME,	lpszUserName );
	}

	if ( lpszDomainName != NULL )
	{
		SetProperty( PRINTJOB_PROP_USER_DOMAIN,	lpszDomainName );
	}
	DEBUG_REQ_TEXT( _T("PrintJob User: %s\\%s\n"), lpszDomainName, lpszUserName );
}

inline bool CPrintJobRequest::AlreadyTracked( JOB_INFO_2* pJobData )
{
	bool bReturnMe = false;
	
	_ASSERTE( pJobData != NULL );
	if ( pJobData != NULL )
	{
		if ( pJobData->pDocument != NULL )
		{
			DEBUG_REQ_TEXT( _T("Checking Title:\t\"%s\" \"%s\"\n"), pJobData->pDocument, 
									WIN9X_HACK_TITLE_STRING );
			if ( _tcscmp( WIN9X_HACK_TITLE_STRING, pJobData->pDocument ) == 0 )
			{
				DEBUG_REQ_TEXT( _T("Print Job has already been tracked!\n") );
				bReturnMe = true;
			}
		}
	}
	return bReturnMe;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////
// Private Inline Functions
///////////////////////////////////////////////////////////////////////////////////////////////////////

inline void CPrintJobRequest::CreateProperties()
{
	// Clear out the Property Arrays
	for ( unsigned register nCounter = 0 ; nCounter < MAX_PROPERTY_COUNT ; nCounter++ )
	{
		m_pProperties[nCounter]	= NULL;
		::ZeroMemory( &(m_pValues[nCounter]), sizeof(VARIANT) );
		::VariantInit( &(m_pValues[nCounter]) );
	}

	m_dwProperties = 0;
}

inline void CPrintJobRequest::DestroyProperties()
{
	// Free up the values
	for ( unsigned register nCounter = 0 ; nCounter < m_dwProperties ; nCounter++ )
	{
		::SysFreeString( m_pProperties[nCounter] );
		::VariantClear( &m_pValues[nCounter] );
	}
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, bool bValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_BOOL;
	V_BOOL( &rProperty )	= ( bValue ? VARIANT_TRUE : VARIANT_FALSE );
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, BSTR bstrValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_BSTR;
	V_BSTR( &rProperty )	= ::SysAllocString( bstrValue );
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, LPCTSTR lpszValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_BSTR;
	V_BSTR( &rProperty )	= CComBSTR(lpszValue).Detach();
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, DWORD dwValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_UI4;
	V_UI4( &rProperty )		= dwValue;
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, long nValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_I4;
	V_I4( &rProperty )		= nValue;
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, short nValue )
{
	VARIANT&	rProperty = GetNewProperty(lpszName);

	V_VT( &rProperty )		= VT_I2;
	V_I2( &rProperty )		= nValue;
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, const LPSYSTEMTIME pValue )
{
	DATE		dtVariant;
	SYSTEMTIME	dtSystem;
	FILETIME	dtFileUTC;
	FILETIME	dtFileLocal;
	
	// Do all of the necessary conversions
	DEBUG_FAIL( ::SystemTimeToFileTime(pValue, &dtFileUTC)				);
	DEBUG_FAIL( ::FileTimeToLocalFileTime(&dtFileUTC, &dtFileLocal)		);
	DEBUG_FAIL( ::FileTimeToSystemTime(&dtFileLocal, &dtSystem)			);
	::SystemTimeToVariantTime( &dtSystem, &dtVariant );

	// Set the value
	SetProperty( lpszName, dtVariant );
}

inline void CPrintJobRequest::SetProperty( const LPOLESTR lpszName, const DATE& dtValue )
{
	VARIANT&	rProperty = GetNewProperty( lpszName );

	V_VT( &rProperty )		= VT_DATE;
	V_DATE( &rProperty )	= dtValue;
}

inline void CPrintJobRequest::SetProperty( Vector* pRawData )
{
	_ASSERTE( pRawData != NULL );
	VARIANT& rNew = GetNewProperty( PRINTJOB_PROP_OTHER_RAWDATA );

	V_VT(&rNew)	= VT_UI1|VT_ARRAY;
	pRawData->CopyTo( &(rNew.parray) );
}

inline VARIANT& CPrintJobRequest::GetNewProperty( const LPOLESTR lpszName )
{
	_ASSERTE( m_dwProperties < MAX_PROPERTY_COUNT );
	m_pProperties[m_dwProperties] = ::SysAllocString( lpszName );

	// Increment the property count
	return (m_pValues[m_dwProperties++]);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////
// Remove Debugging Information
///////////////////////////////////////////////////////////////////////////////////////////////////////

#ifdef DEFINED_THIS_FILE
#undef THIS_FILE
#endif

///////////////////////////////////////////////////////////////////////////////////////////////////////

#endif // !defined(AFX_PRINTJOBREQUEST_H__C2AAE7AA_7E8B_11D4_A46A_00105A1C588D__INCLUDED_)

///////////////////////////////////////////////////////////////////////////////////////////////////////
// End of PrintJobRequest.h
///////////////////////////////////////////////////////////////////////////////////////////////////////