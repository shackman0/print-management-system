//////////////////////////////////////////////////////////////////////
// EMFException.h: interface for the CEMFException class.
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_EMFEXCEPTION_H__A95038C7_8A72_11D4_A46C_00105A1C588D__INCLUDED_)
#define AFX_EMFEXCEPTION_H__A95038C7_8A72_11D4_A46C_00105A1C588D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//////////////////////////////////////////////////////////////////////
// Includes
//////////////////////////////////////////////////////////////////////

#include "CMSException.h"

//////////////////////////////////////////////////////////////////////
// Debugging Options
//////////////////////////////////////////////////////////////////////

#ifdef _DEBUG
	#define EMF_EXCEPTION(code)					CEMFException((code),THIS_FILE,__LINE__)
#else
	#define EMF_EXCEPTION(code)					CEMFException((code))
#endif

//////////////////////////////////////////////////////////////////////
// Class Definition
//////////////////////////////////////////////////////////////////////

class CEMFException : public CCMSException  
{
// Exception Codes
public:
	enum
	{
		exEMF_Unknown						= 0x00,

		// GDI Failures
		exEMF_FailGdiGetPageHandle			= 0x01,
		exEMF_FailGdiStartPageEMF			= 0x02,
		exEMF_FailGdiEndPageEMF				= 0x03,
		exEMF_FailGdiGetDevmodeForPage		= 0x04,
		exEMF_FailGdiStartDocEMF			= 0x05,
		exEMF_FailGdiPlayPageEMF			= 0x06,
		exEMF_FailGdiGetSpoolFileHandle		= 0x07,
		exEMF_FailGdiGetDC					= 0x08,
		exEMF_FailGdiGetPageCount			= 0x09,
		//exEMF_Fail					= 0x0a,
		//exEMF_Fail					= 0x0b,
		//exEMF_Fail					= 0x0c,

		// SpoolSS Failures
		exEMF_FailGetJobAttributes			= 0x10,
		exEMF_FailSplInitializeWinSpoolDrv	= 0x11,

		// Other Errors
		exEMF_InvalidNUpValue				= 0x20
	};

// Construction / Destruction
public:
#ifdef _DEBUG
	CEMFException( const DWORD& dwExceptionCode, LPCTSTR lpszFile, int nLine ) :
		CCMSException(_T("EMF"), dwExceptionCode, lpszFile, nLine)
	{
		// Nothing to do
	}
#else
	CEMFException( const DWORD& dwExceptionCode ) :
		CCMSException(_T("EMF"), dwExceptionCode)
	{
		// Nothing to do
	}
#endif
	virtual ~CEMFException()																			{ };


protected:
	virtual LPCTSTR	GetMessageBoxTitle() const					{ return CMS_PRODUCT_NAME _T(" EMF Error"); };
};

#endif // !defined(AFX_EMFEXCEPTION_H__A95038C7_8A72_11D4_A46C_00105A1C588D__INCLUDED_)

//////////////////////////////////////////////////////////////////////
// End of EMFException.h
//////////////////////////////////////////////////////////////////////
