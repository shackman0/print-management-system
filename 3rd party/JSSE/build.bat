@echo off

rem This script builds the JSSE.
rem You must pass in the location of JAVA, or set the JAVA variable below.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\J2SDK1.4.1

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

rem check to see if the location of JAVA was passed in

if %1. == . goto 1continue

set JAVA=%1
goto 2continue

:1continue

if not %JAVA%. == . goto 2continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:2continue

echo Cleaning up the target JSSE Class directory ...
if exist .\Class\com\   rd /s /q .\Class\com\
if exist .\Class\javax\ rd /s /q .\Class\javax\
if exist .\Class\META-INF\ rd /s /q .\Class\META-INF\

echo Building JSSE classes now ...

if not exist .\class md .\class
cd .\Class
%JAVA%\bin\jar xf ..\lib\jcert.jar
%JAVA%\bin\jar xf ..\lib\jnet.jar
%JAVA%\bin\jar xf ..\lib\jsse.jar
cd ..

:end

