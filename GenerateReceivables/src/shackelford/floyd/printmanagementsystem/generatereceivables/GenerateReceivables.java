package shackelford.floyd.printmanagementsystem.generatereceivables;


import java.sql.SQLException;
import java.util.Calendar;

import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorTable;


/**
 * <p>Title: GenerateReceivables</p>
 * <p>Description:
 * generates receivables for invoicing the customers for shared revenue arrangements.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GenerateReceivables
{

  private static String           DBADDR_OPTION = "-dbAddr";
  private static String           VERBOSE_OPTION = "-verbose";
  private static String           DBUSER_OPTION = "-dbUser";
  private static String           DBPASSWD_OPTION = "-dbPasswd";
  private static String           START_DATE_OPTION = "-startDate";
  private static String           END_DATE_OPTION = "-endDate";
  private static String           INSTALLATION_ID_OPTION = "-instID";

  public static final int         EXIT_OK = 0;

  public static final int         EXIT_ERROR_COMMAND_LINE_PARMS = -1;
  public static final int         EXIT_ERROR_MISSING_RESDIR = -2;
  public static final int         EXIT_ERROR_MISSING_DBADDR = -3;
  public static final int         EXIT_ERROR_MISSING_DBUSERID = -4;
  public static final int         EXIT_ERROR_MISSING_DBPASSWD = -5;
  public static final int         EXIT_ERROR_DATABASE_CONNECT = -6;
  public static final int         EXIT_ERROR_MISSING_INST_ID = -7;
  public static final int         EXIT_ERROR_MONITOR_ROW_NOT_FOUND = -10;


  public static void main(String[] args)
  {

    _GregorianCalendar startCal = null;
    _GregorianCalendar endCal = null;
    String installationID = null;

    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase(START_DATE_OPTION) == true)
      {
        startCal = new _GregorianCalendar(args[++index]);
        System.out.println("Start date = " + startCal.GetDateString() + " (inclusive)");
      }
      else
      if (args[index].equalsIgnoreCase(END_DATE_OPTION) == true)
      {
        endCal = new _GregorianCalendar(args[++index]);
        System.out.println("End date = " + endCal.GetDateString() + " (inclusive)");
      }
      else
      if (args[index].equalsIgnoreCase(VERBOSE_OPTION) == true)
      {
        GlobalAttributes.__verbose = true;
        System.out.println("Running verbose");
      }
      else
      if (args[index].equalsIgnoreCase(DBADDR_OPTION) == true)
      {
        GlobalAttributes.__dbAddr = args[++index];
        System.out.println("Monitor Database Addr = \"" + GlobalAttributes.__dbAddr + "\"");
      }
      else
      if (args[index].equalsIgnoreCase(DBUSER_OPTION) == true)
      {
        GlobalAttributes.__dbUserID = args[++index];
        System.out.println("Monitor Database User ID = \"" + GlobalAttributes.__dbUserID + "\"");
      }
      else
      if (args[index].equalsIgnoreCase(DBPASSWD_OPTION) == true)
      {
        GlobalAttributes.__dbPasswd = args[++index];
        System.out.println("Monitor Database Password found");
      }
      else
      if (args[index].equalsIgnoreCase(INSTALLATION_ID_OPTION) == true)
      {
        installationID = args[++index];
        System.out.println("Processing only installation " + installationID);
      }
      else
      {
        System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
      }
    }

    if (GlobalAttributes.__dbAddr == null)
    {
      System.out.println(DBADDR_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBADDR);
    }

    if (GlobalAttributes.__dbUserID == null)
    {
      System.out.println(DBUSER_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBUSERID);
    }

    if (GlobalAttributes.__dbPasswd == null)
    {
      System.out.println(DBPASSWD_OPTION + " was not specified");
      Usage();
      System.exit(EXIT_ERROR_MISSING_DBPASSWD);
    }

    if (installationID == null)
    {
      System.out.println("Processing ALL installations.");
    }

    if (startCal == null)
    {
      startCal = new _GregorianCalendar();
      startCal.add(Calendar.MONTH,-1);
      startCal.set(Calendar.DAY_OF_MONTH,1);
      System.out.println(START_DATE_OPTION + " was not specified. Using first day of previous month: " + startCal.GetDateString());
    }

    if (endCal == null)
    {
      endCal = new _GregorianCalendar();
      endCal.set(Calendar.DAY_OF_MONTH,1);
      endCal.add(Calendar.DAY_OF_MONTH,-1);
      System.out.println(END_DATE_OPTION + " was not specified. Using last day of previous month: " + endCal.GetDateString());
    }

    try
    {
      MonitorDatabase.SetDefaultMonitorDatabase (
        new MonitorDatabase (
              GlobalAttributes.__dbAddr,
              GlobalAttributes.__dbUserID,
              GlobalAttributes.__dbPasswd ) );
      MonitorDatabase.GetDefaultMonitorDatabase().ConnectDatabase();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      System.exit(EXIT_ERROR_DATABASE_CONNECT);
    }

    if (MonitorTable.GetCachedMonitorRow() == null)
    {
      System.out.println("ERROR: monitor row not found");
      System.exit(EXIT_ERROR_MONITOR_ROW_NOT_FOUND);
    }

    MonitorDatabase.GetDefaultMonitorDatabase().GetReceivablesTable().GenerateReceivables(startCal,endCal,installationID);

    MonitorDatabase.GetDefaultMonitorDatabase().Done();

    System.exit(EXIT_OK);
  }

  private static void Usage ()
  {
    System.err.println (
      "Usage e.g.: java -Djdbc.drivers=org.postgresql.Driver GenerateReceivables " +
      DBADDR_OPTION + " <monitor database hostname or ip addr> " +
      DBUSER_OPTION + " <monitor database user id> " +
      DBPASSWD_OPTION + " <monitor database passwd> " +
      "[" + START_DATE_OPTION + " \"yyyy MMM dd\" ] " +
      "[" + END_DATE_OPTION + " \"yyyy MMM dd\" ] " +
      "[" + INSTALLATION_ID_OPTION + " <installation id> ] " +
      "[" + VERBOSE_OPTION + "]");
  }

}