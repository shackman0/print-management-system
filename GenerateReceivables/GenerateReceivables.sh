#! /bin/bash

# This script launches the GenerateReceivables java script

# Set the JDKTOP variable to point to where the Java Virtual Machine
# and supporting files are located. Use an absolute path.

INSTALLDIR=/usr/local/GenerateReceivables
JDKTOP=/usr/local/java2/
JDKLIB=${JDKTOP}/lib
CLASSPATH=${INSTALLDIR}/GenerateReceivables.jar:${JDKLIB}:
DBADDR=localhost
DBUSER=CMS
DBPASSWD=CMSPASSWD
LOGFILE=/var/log/GenerateReceivables.log

# if no start date is specified, then defaults to first day of previous month
STARTDATE=
# STARTDATE=-startDate "yyyy MMM dd"

# if no end date is specified, then defaults to last day of previous month
ENDDATE=
# ENDDATE=-endDate "yyyy MMM dd"

# if no installation id is specified, then defaults to all installations
INSTALLATION_ID=
# INSTALLATION_ID=-instID 000000

(${JDKTOP}/bin/java -classpath ${CLASSPATH} -Djdbc.drivers=org.postgresql.Driver GenerateReceivables -dbAddr ${DBADDR} -dbUser ${DBUSER} -dbPasswd ${DBPASSWD} -verbose 1>${LOGFILE} 2>&1)

cat ${LOGFILE} | mail -s "GenerateReceivables report" floyds@4peakstech.com a.ames@cardmeter.com
