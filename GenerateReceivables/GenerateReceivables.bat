@echo off

rem This script launches the GenerateReceivables application.
rem You must modify this script when you install the
rem GenerateReceivables application before this script will work.

rem Set the STARTDATE variable to the start date, inclusive. if
rem you leave it blank, then it will default to the first day
rem of the previous month.

set STARTDATE =
rem set STARTDATE = -startDate "yyyy MMM dd"

rem Set the ENDDATE variable to the end date, inclusive. if you
rem leave it blank, then it will default to the last day of
rem the previous month.

set ENDDATE =
rem set ENDDATE = -endDate "yyyy MMM dd"

rem Set the INSTALLATION_ID variable to a particular installation id
rem if you only want to process one installation. leave it blank if
rem you want to process all installations.

set INSTALLATION_ID =
rem set INSTALLATION_ID = -instID 000000

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\j2sdk1.3.1_01

rem Set the SERVER variable to the hostname or the IP address
rem of the Monitor Server. Put the value in between the double
rem quotes.

set SERVER="pay2print"

rem Set the USERID variable to the user id to use to log on to
rem the Monitor database.

set USERID="CMS"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Monitor database.

set PASSWORD="cmspasswd"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

:99continue

%JAVA%\bin\java -classpath .;%JAVA%\lib;.\GenerateReceivables.jar; -Djdbc.drivers=org.postgresql.Driver GenerateReceivables -dbAddr %SERVER% -dbUser %USERID% -dbPasswd %PASSWORD% %INSTALLATION_ID% %STARTDATE% %ENDDATE%

:end

set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set STARTDATE=
set ENDDATE=
set INSTALLATION_ID=

