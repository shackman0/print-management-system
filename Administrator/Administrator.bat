@echo off

rem This script launches the Administrator application.
rem You must modify this script when you install the
rem Administrators application before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem Set the SERVER variable to the hostname or the IP address
rem of the Installation Server. Put the value in between the double
rem quotes.

set SERVER="pay2print"

rem Set the USERID variable to the user id to use to log on to
rem the Installation database.

set USERID="PTP"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Installation database.

set PASSWORD="PTPPASSWD"

rem Set the RESOURCES variable to the directory in which the
rem language-specific string files are stored

set RESOURCES=".\Resources"

rem Set the INSTALLATION_ID variable to the id of the installation

set INSTALLATION_ID="120001"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not "%JAVA%". == "". goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

if not %RESOURCES%. == . goto 5continue

echo ERROR: The RESOURCES variable is not set.
echo   The RESOURCES variable must be properly set before running this
echo   script.

goto end

:5continue

if not %INSTALLATION_ID%. == . goto 98continue

echo ERROR: The INSTALLATION_ID variable is not set.
echo   The INSTALLATION_ID variable must be properly set before running
echo   this script.

goto end

:98continue

rem clean up any upgrade relics
if exist ..\Administrator.zip del /f /q ..\Administrator.zip >nul

rem enable the new upgrade .jar file if one exists
if not exist .\Administrator.jar goto 99continue
if exist .\AdministratorRun.jar del /f /q .\AdministratorRun.jar >nul
ren .\Administrator.jar AdministratorRun.jar

:99continue

rem launch javaw in the background so we don't see a dos window
start "Administrator" /i /b "%JAVA%\bin\javaw.exe" -classpath .;"%JAVA%\lib";.\AdministratorRun.jar; -Djdbc.drivers=org.postgresql.Driver -Djava.security.policy=Administrator.policy Administrator -dbaddr %SERVER% -dbuser %USERID% -dbpasswd %PASSWORD% -resDir %RESOURCES% -instID %INSTALLATION_ID% %1 %2 %3 %4 %5 %6 %7 %8 %9

:end

set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set RESOURCES=
set INSTALLATION_ID=
