package shackelford.floyd.printmanagementsystem.administrator;

import java.text.MessageFormat;
import java.text.ParseException;

import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AdministratorRefundPrePaymentEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorRefundPrePaymentEntryPanel
  extends AdministratorAddCashPrePaymentEntryPanel
{

  /**
   * 
   */
  private static final long   serialVersionUID            = 1L;

  private static Resources    __resources;

  private static final String INVALID_REFUND_AMOUNT_MSG   = "INVALID_REFUND_AMOUNT_MSG";

  private static final String INVALID_REFUND_AMOUNT_TITLE = "INVALID_REFUND_AMOUNT_TITLE";

  public AdministratorRefundPrePaymentEntryPanel()
  {
    super();
  }

  public AdministratorRefundPrePaymentEntryPanel(ListPanelCursor listPanel, UserAccountTable userAccountTable, UserAccountRow userAccountRow)
  {
    super();
    Initialize(listPanel, userAccountTable, userAccountRow);
  }

  @Override
  protected boolean FieldsValid()
  {
    if (super.FieldsValid() == false)
    {
      return false;
    }

    String amountToRefund = new String(_amountField.getText().trim());

    Number parsedObj = null;
    try
    {
      parsedObj = (Number) __formatter.parseObject(amountToRefund);
    }
    catch (ParseException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_REFUND_AMOUNT_MSG), GetResources().getProperty(INVALID_REFUND_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    double refundAmount = parsedObj.doubleValue();
    Double previousBalance = GetUserAccountRow().Get_pre_payment_balance();

    if (refundAmount > previousBalance.doubleValue())
    {
      MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(INVALID_REFUND_AMOUNT_MSG));
      Object[] substitutionValues = new Object[] { previousBalance };
      String msgString = msgFormat.format(substitutionValues).trim();

      MessageDialog.ShowErrorMessageDialog(this, msgString, GetResources().getProperty(INVALID_REFUND_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignFieldsToRow()
  {
    _previousBalance = GetUserAccountRow().Get_pre_payment_balance();
    _amount = new Double(-_amountField.GetNumberValue().doubleValue());
    GetUserAccountRow().Set_pre_payment_balance(_previousBalance.doubleValue() + _amount.doubleValue());
  }

  @Override
  protected void DisplayCompletionMsgBox(PrePaymentHistoryRow prePaymentHistoryRow)
  {
    AdministratorRefundPrePaymentCompleteMsgBox msgBox = new AdministratorRefundPrePaymentCompleteMsgBox(prePaymentHistoryRow, GetUserAccountRow(), _previousBalance);
    msgBox.setVisible(true);
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorRefundPrePaymentEntryPanel.class.getName());
  }

}
