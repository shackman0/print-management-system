package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.text.DecimalFormat;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry2Panel;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderTable;


/**
 * <p>Title: AdministratorPrintJobEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrintJobEntryPanel
  extends AbstractEntry2Panel
{

  /**
   * 
   */
  private static final long           serialVersionUID         = 1L;

  private final NumericTextField      _printJobNumberTextField = new NumericTextField();

  private final OverwritableTextField _userAccountIDTextField  = new OverwritableTextField();

  private final NumericTextField      _numberOfPagesTextField  = new NumericTextField();

  private final OverwritableTextField _paperSizeTextField      = new OverwritableTextField();

  private final OverwritableTextField _sidesTextField          = new OverwritableTextField();

  private final OverwritableTextField _printerTextField        = new OverwritableTextField();

  private final OverwritableTextField _colorTextField          = new OverwritableTextField();

  private final NumericTextField      _costTextField           = new NumericTextField();

  private static Resources            __resources;

  protected static final String       PRINT_JOB_NUMBER         = "PRINT_JOB_NUMBER";

  protected static final String       PRINT_JOB_NUMBER_TIP     = "PRINT_JOB_NUMBER_TIP";

  protected static final String       USER_ACCOUNT_ID          = "USER_ACCOUNT_ID";

  protected static final String       USER_ACCOUNT_ID_TIP      = "USER_ACCOUNT_ID_TIP";

  protected static final String       NUMBER_OF_PAGES          = "NUMBER_OF_PAGES";

  protected static final String       NUMBER_OF_PAGES_TIP      = "NUMBER_OF_PAGES_TIP";

  protected static final String       PAPER_SIZE               = "PAPER_SIZE";

  protected static final String       PAPER_SIZE_TIP           = "PAPER_SIZE_TIP";

  protected static final String       SIDES                    = "SIDES";

  protected static final String       SIDES_TIP                = "SIDES_TIP";

  protected static final String       PRINTER                  = "PRINTER";

  protected static final String       PRINTER_TIP              = "PRINTER_TIP";

  protected static final String       COLOR                    = "COLOR";

  protected static final String       COLOR_TIP                = "COLOR_TIP";

  protected static final String       COST                     = "COST";

  protected static final String       COST_TIP                 = "COST_TIP";

  protected static final String       PRINT_JOB_PROPERTIES     = "PRINT_JOB_PROPERTIES";

  protected static final String       PRINT_JOB_PROPERTIES_TIP = "PRINT_JOB_PROPERTIES_TIP";

  public AdministratorPrintJobEntryPanel()
  {
    super();
  }

  public AdministratorPrintJobEntryPanel(ListPanelCursor listPanel, PrintJobHeaderTable printJobTable, PrintJobHeaderRow printJobRow)
  {
    super();
    Initialize(listPanel, printJobTable, printJobRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    constraints.gridy = 0;

    // print job number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PRINT_JOB_NUMBER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _printJobNumberTextField.setEditable(false);
    _numberOfPagesTextField.SetFormat(new DecimalFormat("#####0"));
    centerPanel.add(_printJobNumberTextField, constraints);

    // user_account_id
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(USER_ACCOUNT_ID).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDTextField.setEditable(false);
    centerPanel.add(_userAccountIDTextField, constraints);

    // number_of_pages
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NUMBER_OF_PAGES).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _numberOfPagesTextField.setEditable(false);
    _numberOfPagesTextField.SetFormat(new DecimalFormat("#,##0"));
    centerPanel.add(_numberOfPagesTextField, constraints);

    // paper_size
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PAPER_SIZE).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paperSizeTextField.setEditable(false);
    centerPanel.add(_paperSizeTextField, constraints);

    // sides
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(SIDES).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _sidesTextField.setEditable(false);
    centerPanel.add(_sidesTextField, constraints);

    // printer
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PRINTER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _printerTextField.setEditable(false);
    centerPanel.add(_printerTextField, constraints);

    // color
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(COLOR).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _colorTextField.setEditable(false);
    centerPanel.add(_colorTextField, constraints);

    // cost
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(COST).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _costTextField.setEditable(false);
    _costTextField.SetFormat(new DecimalFormat("#,##0.000"));
    centerPanel.add(_costTextField, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _saveButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected void AddActionButtonsToSouthPanel(Container southPanel)
  {
    southPanel.add(new ActionButton(new PrintJobPropertiesAction()));
  }

  @Override
  protected boolean FieldsValid()
  {

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _printJobNumberTextField.SetValue(GetPrintJobHeaderRow().Get_print_job_number());
    _userAccountIDTextField.setText(GetPrintJobHeaderRow().Get_user_account_id());
    _numberOfPagesTextField.SetValue(GetPrintJobHeaderRow().Get_number_of_pages());
    _paperSizeTextField.setText(GetPrintJobHeaderRow().Get_paper_size());
    _sidesTextField.setText(GetPrintJobHeaderRow().Get_sides());
    _printerTextField.setText(GetPrintJobHeaderRow().Get_printer());
    _colorTextField.setText(GetPrintJobHeaderRow().Get_color());
    _costTextField.SetValue(GetPrintJobHeaderRow().Get_cost());
  }

  @Override
  protected void AssignFieldsToRow()
  {
    // do nothing
  }

  //  private PrintJobHeaderTable GetPrintJobHeaderTable()
  //  {
  //    return (PrintJobHeaderTable)_sqlTable;
  //  }

  private PrintJobHeaderRow GetPrintJobHeaderRow()
  {
    return (PrintJobHeaderRow) _sqlRow;
  }

  protected void PrintJobPropertiesAction()
  {
    AdministratorPrintJobPropertiesPanel panel = new AdministratorPrintJobPropertiesPanel(GetPrintJobHeaderRow());
    panel.setVisible(true);
    panel.toFront();
  }

  protected class PrintJobPropertiesAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 4532864899552390823L;

    /**
     * 
     */
    

    public PrintJobPropertiesAction()
    {
      putValue(Action.NAME, GetResources().getProperty(PRINT_JOB_PROPERTIES));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(PRINT_JOB_PROPERTIES_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintJobPropertiesAction();
    }
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrintJobEntryPanel.class.getName());
  }

}
