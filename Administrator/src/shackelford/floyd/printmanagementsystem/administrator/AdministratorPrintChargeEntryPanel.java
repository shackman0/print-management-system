package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry3Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PageChargesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PageChargesTable;


/**
 * <p>Title: AdministratorPrintChargeEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrintChargeEntryPanel
  extends AbstractEntry3Panel
{

  /**
   * 
   */
  private static final long          serialVersionUID       = 1L;

  private final JLabel               _pageChargeNumberLabel = new JLabel();

  private final NumericTextField     _pageChargeTextField   = new NumericTextField();

  private final _JComboBox           _paperSizeComboBox     = new _JComboBox();

  private final _JComboBox           _printerComboBox       = new _JComboBox();

  private final JCheckBox            _simplexCheckBox       = new JCheckBox();

  private final JCheckBox            _duplexCheckBox        = new JCheckBox();

  private final JCheckBox            _monochromeCheckBox    = new JCheckBox();

  private final JCheckBox            _colorCheckBox         = new JCheckBox();

  private String[]                   _printersArray         = null;

  private static Resources           __resources;

  private static final DecimalFormat __decimalFormat        = new DecimalFormat("#,##0.000");

  private static final String        PRINT_CHARGE_ID        = "PRINT_CHARGE_ID";

  private static final String        CHARGE                 = "CHARGE";

  private static final String        CHARGE_TIP             = "CHARGE_TIP";

  private static final String        PAPER_SIZE             = "PAPER_SIZE";

  private static final String        PAPER_SIZE_TIP         = "PAPER_SIZE_TIP";

  private static final String        SIDES                  = "SIDES";

  private static final String        COLOR_LABEL            = "COLOR_LABEL";

  private static final String        PRINTER                = "PRINTER";

  private static final String        PRINTER_TIP            = "PRINTER_TIP";

  private static final String        SIMPLEX                = "SIMPLEX";

  private static final String        SIMPLEX_TIP            = "SIMPLEX_TIP";

  private static final String        DUPLEX                 = "DUPLEX";

  private static final String        DUPLEX_TIP             = "DUPLEX_TIP";

  private static final String        MONOCHROME             = "MONOCHROME";

  private static final String        MONOCHROME_TIP         = "MONOCHROME_TIP";

  private static final String        COLOR                  = "COLOR";

  private static final String        COLOR_TIP              = "COLOR_TIP";

  private static final int           CURRENCY_FIELD_WIDTH   = 6;

  public AdministratorPrintChargeEntryPanel()
  {
    super();
  }

  public AdministratorPrintChargeEntryPanel(ListPanelCursor listPanel, PageChargesTable pageChargesTable, PageChargesRow pageChargesRow)
  {
    super();
    Initialize(listPanel, pageChargesTable, pageChargesRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      GetPageChargesRow().SetNewPageChargeNumber();
    }
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // print charge id
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PRINT_CHARGE_ID).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _pageChargeNumberLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_pageChargeNumberLabel, constraints);

    // print charge
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CHARGE).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _pageChargeTextField.SetFormat(__decimalFormat);
    _pageChargeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _pageChargeTextField.setToolTipText(__resources.getProperty(CHARGE_TIP).toString());

    centerPanel.add(_pageChargeTextField, constraints);

    // paper size
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PAPER_SIZE).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paperSizeComboBox.setEditable(false);
    _paperSizeComboBox.setToolTipText(__resources.getProperty(PAPER_SIZE_TIP).toString());
    centerPanel.add(_paperSizeComboBox, constraints);

    // sides
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(SIDES).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _simplexCheckBox.setText(__resources.getProperty(SIMPLEX));
    _simplexCheckBox.setToolTipText(__resources.getProperty(SIMPLEX_TIP));
    centerPanel.add(_simplexCheckBox, constraints);

    constraints.gridx = 2;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _duplexCheckBox.setText(__resources.getProperty(DUPLEX));
    _duplexCheckBox.setToolTipText(__resources.getProperty(DUPLEX_TIP));
    centerPanel.add(_duplexCheckBox, constraints);

    // color
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(COLOR_LABEL).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _monochromeCheckBox.setText(__resources.getProperty(MONOCHROME));
    _monochromeCheckBox.setToolTipText(__resources.getProperty(MONOCHROME_TIP));
    centerPanel.add(_monochromeCheckBox, constraints);

    constraints.gridx = 2;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _colorCheckBox.setText(__resources.getProperty(COLOR));
    _colorCheckBox.setToolTipText(__resources.getProperty(COLOR_TIP));
    centerPanel.add(_colorCheckBox, constraints);

    // Printer
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PRINTER).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _printerComboBox.setEditable(true);
    _printerComboBox.setToolTipText(__resources.getProperty(PRINTER_TIP).toString());
    centerPanel.add(_printerComboBox, constraints);

    return centerPanel;
  }

  @Override
  protected boolean FieldsValid()
  {
    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _pageChargeNumberLabel.setText(GetPageChargesRow().Get_page_charge_number().toString());
    _pageChargeTextField.SetValue(GetPageChargesRow().Get_per_page_charge());

    String[] sizes = { PageChargesRow.ANY_SIZE_NAME, "8.5\" x 11\"", "8.5\" x 14\"", "A4", "A5" };
    String size = GetPageChargesRow().Get_paper_size();
    _paperSizeComboBox.RecreateComboBox(sizes, size, false);

    _simplexCheckBox.setSelected(GetPageChargesRow().SidesIsSimplex() || GetPageChargesRow().SidesIsAny());
    _duplexCheckBox.setSelected(GetPageChargesRow().SidesIsDuplex() || GetPageChargesRow().SidesIsAny());
    _monochromeCheckBox.setSelected(GetPageChargesRow().ColorIsMonochrome() || GetPageChargesRow().ColorIsAny());
    _colorCheckBox.setSelected(GetPageChargesRow().ColorIsColor() || GetPageChargesRow().ColorIsAny());

    if (_printersArray == null)
    {
      _printersArray = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable().GetPrintersAsStringArray();
    }
    String printer = GetPageChargesRow().Get_printer();
    _printerComboBox.RecreateComboBox(_printersArray, printer, false);
    if (_printerComboBox.FindItemIndex(printer) == -1)
    {
      _printerComboBox.insertItemAt(printer, _printerComboBox.getItemCount());
      _printerComboBox.setSelectedItem(printer);
    }
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetPageChargesRow().Set_per_page_charge(_pageChargeTextField.getText());

    GetPageChargesRow().Set_paper_size(_paperSizeComboBox.getSelectedItem().toString());

    if ((_simplexCheckBox.isSelected() == true) && (_duplexCheckBox.isSelected() == false))
    {
      GetPageChargesRow().Set_sides(PageChargesRow.SIDES_SIMPLEX_NAME);
    }
    else
      if ((_simplexCheckBox.isSelected() == false) && (_duplexCheckBox.isSelected() == true))
      {
        GetPageChargesRow().Set_sides(PageChargesRow.SIDES_DUPLEX_NAME);
      }
      else
      {
        GetPageChargesRow().Set_sides(PageChargesRow.ANY_SIDES_NAME);
      }

    if ((_monochromeCheckBox.isSelected() == true) && (_colorCheckBox.isSelected() == false))
    {
      GetPageChargesRow().Set_color(PageChargesRow.COLOR_MONOCHROME_NAME);
    }
    else
      if ((_monochromeCheckBox.isSelected() == false) && (_colorCheckBox.isSelected() == true))
      {
        GetPageChargesRow().Set_color(PageChargesRow.COLOR_COLOR_NAME);
      }
      else
      {
        GetPageChargesRow().Set_color(PageChargesRow.ANY_COLOR_NAME);
      }

    GetPageChargesRow().Set_printer(_printerComboBox.getSelectedItem().toString());
  }

  //  private PageChargesTable GetPageChargesTable()
  //  {
  //    return (PageChargesTable)_sqlTable;
  //  }

  private PageChargesRow GetPageChargesRow()
  {
    return (PageChargesRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrintChargeEntryPanel.class.getName());
  }

}
