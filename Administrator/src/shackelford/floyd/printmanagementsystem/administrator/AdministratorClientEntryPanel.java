package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientTable;


/**
 * <p>Title: AdministratorClientEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorClientEntryPanel
  extends AdministratorGatekeeperEntryPanel
{

  /**
   * 
   */
  private static final long serialVersionUID = 314786503727395060L;
  /**
   * 
   */
  

  private static Resources  __resources;

  public AdministratorClientEntryPanel()
  {
    super();
  }

  public AdministratorClientEntryPanel(ListPanelCursor listPanel, ClientTable clientTable, ClientRow clientRow)
  {
    super(listPanel, clientTable, clientRow);
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorClientEntryPanel.class.getName());
  }

}
