package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry2Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryTable;


/**
 * <p>Title: AdministratorPrePaymentEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrePaymentEntryPanel
  extends AbstractEntry2Panel
{

  /**
   * 
   */
  private static final long           serialVersionUID        = 1L;

  private final OverwritableTextField _userAccountIDTextField = new OverwritableTextField();

  private final NumericTextField      _amountTextField        = new NumericTextField();

  private final OverwritableTextField _sourceTextField        = new OverwritableTextField();

  private static Resources            __resources;

  protected static final String       USER_ACCOUNT_ID         = "USER_ACCOUNT_ID";

  protected static final String       USER_ACCOUNT_ID_TIP     = "USER_ACCOUNT_ID_TIP";

  protected static final String       AMOUNT                  = "AMOUNT";

  protected static final String       AMOUNT_TIP              = "AMOUNT_TIP";

  protected static final String       SOURCE                  = "SOURCE";

  protected static final String       SOURCE_TIP              = "SOURCE_TIP";

  public AdministratorPrePaymentEntryPanel()
  {
    super();
  }

  public AdministratorPrePaymentEntryPanel(ListPanelCursor listPanel, PrePaymentHistoryTable prePaymentTable, PrePaymentHistoryRow prePaymentRow)
  {
    super();
    Initialize(listPanel, prePaymentTable, prePaymentRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    constraints.gridy = 0;

    // user_account_id
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(USER_ACCOUNT_ID).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDTextField.setEditable(false);
    centerPanel.add(_userAccountIDTextField, constraints);

    // amount
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(AMOUNT).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountTextField.setEditable(false);
    _amountTextField.SetFormat(new DecimalFormat("#,##0.00"));
    centerPanel.add(_amountTextField, constraints);

    // source
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(SOURCE).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _sourceTextField.setEditable(false);
    centerPanel.add(_sourceTextField, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _saveButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _userAccountIDTextField.setText(GetPrePaymentHistoryRow().Get_user_account_id());
    _amountTextField.SetValue(GetPrePaymentHistoryRow().Get_amount());
    _sourceTextField.setText(GetPrePaymentHistoryRow().Get_source());
  }

  @Override
  protected void AssignFieldsToRow()
  {
    // do nothing
  }

  //  private PrePaymentHistoryTable GetPrePaymentHistoryTable()
  //  {
  //    return (PrePaymentHistoryTable)_sqlTable;
  //  }

  private PrePaymentHistoryRow GetPrePaymentHistoryRow()
  {
    return (PrePaymentHistoryRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrePaymentEntryPanel.class.getName());
  }

}
