package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Color;
import java.awt.Component;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;

import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SQLStatement;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AdministratorReconcilliationReportTableModel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorReconcilliationReportTableModel
  extends AbstractTableModel
{

  /**
   * 
   */
  private static final long                       serialVersionUID   = 1L;

  JTable                                          _table;

  private TableRow[]                              _rows;

  @SuppressWarnings("unused")
  private AdministratorReconcilliationReportPanel _panel;

  private static Resources                        __resources;

  private static String[]                         __columnNames;

  static DecimalFormat                            __formatter        = new DecimalFormat("#,##0.00");

  private static final String                     USER_ACCOUNT_ID    = "USER_ACCOUNT_ID";

  private static final String                     NUMBER_OF_PAYMENTS = "NUMBER_OF_PAYMENTS";

  private static final String                     TOTAL_PAYMENTS     = "TOTAL_PAYMENTS";

  private static final String                     NUMBER_OF_REFUNDS  = "NUMBER_OF_REFUNDS";

  private static final String                     TOTAL_REFUNDS      = "TOTAL_REFUNDS";

  //  private static final String NET_PRE_PAYMENTS = "NET_PRE_PAYMENTS";
  private static final String                     NUMBER_OF_JOBS     = "NUMBER_OF_JOBS";

  private static final String                     TOTAL_JOB_COST     = "TOTAL_JOB_COST";

  private static final String                     CURRENT_BALANCE    = "CURRENT_BALANCE";

  public AdministratorReconcilliationReportTableModel(MyJTable table, AdministratorReconcilliationReportPanel panel)
  {
    super();
    Initialize(table, panel);
  }

  private void Initialize(MyJTable table, AdministratorReconcilliationReportPanel panel)
  {
    _table = table;
    _panel = panel;

    _table.setDefaultRenderer(Double.class, new DoubleCellRenderer());
  }

  @Override
  public int getRowCount()
  {
    if (_rows == null)
    {
      return 0;
    }
    return _rows.length;
  }

  @Override
  public Class<? extends Object> getColumnClass(int columnIndex)
  {
    return getValueAt(0, columnIndex).getClass();
  }

  @Override
  public int getColumnCount()
  {
    if (__columnNames == null)
    {
      return 0;
    }
    return __columnNames.length;
  }

  @Override
  public Object getValueAt(int rowNum, int colNum)
  {
    switch (colNum)
    {
      case 0:
        return new String(_rows[rowNum]._userAccountID);
      case 1:
        return new Integer(_rows[rowNum]._numberOfPayments);
      case 2:
        return new Double(_rows[rowNum]._totalPayments);
      case 3:
        return new Integer(_rows[rowNum]._numberOfRefunds);
      case 4:
        return new Double(_rows[rowNum]._totalRefunds);
      case 5:
        return new Double(_rows[rowNum]._netPayments);
      case 6:
        return new Integer(_rows[rowNum]._numberOfJobs);
      case 7:
        return new Double(_rows[rowNum]._totalJobCost);
      case 8:
        return new Double(_rows[rowNum]._currentBalance);
      default:
        return "UNKNOWN column=" + colNum + ", row=" + rowNum;
    }
  }

  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  @Override
  public String getColumnName(int colNum)
  {
    return __columnNames[colNum];
  }

  @SuppressWarnings("resource")
  public void PopulateTable()
  {

    if (getRowCount() > 0)
    {
      fireTableRowsDeleted(0, getRowCount() - 1);
    }

    ArrayList<TableRow> rowsArrayList = new ArrayList<>(10);

    // select all the users
    UserAccountTable userAccountTable = InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable();
    SQLStatement userSQLStmt = userAccountTable.GetRowsAsResultSet(null, null, null, -1);

    ResultSet userResultSet = userSQLStmt.GetResultSet();

    try
    {
      while (userResultSet.next() == true)
      {
        TableRow row = new TableRow();
        rowsArrayList.add(row);

        UserAccountRow userRow = new UserAccountRow(userAccountTable, userResultSet);

        row._userAccountID = userRow.Get_user_account_id();

        // determine payments
        String sqlString = "select " + "count(*), sum(" + PrePaymentHistoryRow.Name_amount() + ") " + "from " + "pre_payment_history_table " + "where " + PrePaymentHistoryRow.Name_user_account_id()
          + " = '" + userRow.Get_user_account_id() + "' and " + PrePaymentHistoryRow.Name_amount() + " >= 0";

        SQLStatement sqlStmt = InstallationDatabase.GetDefaultInstallationDatabase().GetAvailableSQLStatement();
        sqlStmt.ExecuteQuery(sqlString);
        ResultSet resultSet = sqlStmt.GetResultSet();

        resultSet.next();
        row._numberOfPayments = resultSet.getInt(1);
        row._totalPayments = resultSet.getDouble(2);

        sqlStmt.MakeAvailable();

        // determine refunds
        sqlString = "select " + "count(*), sum(" + PrePaymentHistoryRow.Name_amount() + ") " + "from " + "pre_payment_history_table " + "where " + PrePaymentHistoryRow.Name_user_account_id() + " = '"
          + userRow.Get_user_account_id() + "' and " + PrePaymentHistoryRow.Name_amount() + " < 0";

        sqlStmt = InstallationDatabase.GetDefaultInstallationDatabase().GetAvailableSQLStatement();
        sqlStmt.ExecuteQuery(sqlString);
        resultSet = sqlStmt.GetResultSet();

        resultSet.next();
        row._numberOfRefunds = resultSet.getInt(1);
        row._totalRefunds = resultSet.getDouble(2);

        sqlStmt.MakeAvailable();

        // determine print jobs
        sqlString = "select " + "count(*), sum(" + PrintJobHeaderRow.Name_cost() + ") " + "from " + "print_job_header_table " + "where " + PrintJobHeaderRow.Name_user_account_id() + " = '"
          + userRow.Get_user_account_id() + "'";

        sqlStmt = InstallationDatabase.GetDefaultInstallationDatabase().GetAvailableSQLStatement();
        sqlStmt.ExecuteQuery(sqlString);
        resultSet = sqlStmt.GetResultSet();

        resultSet.next();
        row._numberOfJobs = resultSet.getInt(1);
        row._totalJobCost = resultSet.getDouble(2);

        sqlStmt.MakeAvailable();

        // determine balance
        row._currentBalance = row._totalPayments - row._totalRefunds - row._totalJobCost;
      }
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
    }
    userSQLStmt.MakeAvailable();

    rowsArrayList.trimToSize();
    _rows = new TableRow[rowsArrayList.size()];
    rowsArrayList.toArray(_rows);

    //fireTableStructureChanged();
    if (getRowCount() > 0)
    {
      fireTableRowsInserted(0, getRowCount() - 1);
    }
  }

  public class TableRow
  {
    /**
     * 
     */
    public TableRow()
    {
      super();
    }

    String _userAccountID    = null;

    int    _numberOfPayments = 0;

    double _totalPayments    = 0d;

    int    _numberOfRefunds  = 0;

    double _totalRefunds     = 0d;

    double _netPayments      = 0d;

    int    _numberOfJobs     = 0;

    double _totalJobCost     = 0d;

    double _currentBalance   = 0d;
  }

  public class DoubleCellRenderer
    extends JLabel
    implements TableCellRenderer
  {
    /**
     * 
     */
    private static final long serialVersionUID = 5722595631841750528L;

    /**
     * 
     */

    public DoubleCellRenderer()
    {
      super();
      setHorizontalAlignment(RIGHT);
      setFont(_table.getFont());
      setForeground(Color.black);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int viewColumn)
    {
      Double theDouble = (Double) value;
      setText(__formatter.format(theDouble));
      return this;
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorReconcilliationReportTableModel.class.getName());

    __columnNames = new String[] { __resources.getProperty(USER_ACCOUNT_ID), __resources.getProperty(NUMBER_OF_PAYMENTS), __resources.getProperty(TOTAL_PAYMENTS),
    __resources.getProperty(NUMBER_OF_REFUNDS), __resources.getProperty(TOTAL_REFUNDS), __resources.getProperty(TOTAL_REFUNDS), __resources.getProperty(NUMBER_OF_JOBS),
    __resources.getProperty(TOTAL_JOB_COST), __resources.getProperty(CURRENT_BALANCE) };
  }

}
