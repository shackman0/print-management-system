package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractList1Panel;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobPropertiesTable;


/**
 * <p>Title: AdministratorPrintJobPropertiesPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrintJobPropertiesPanel
  extends AbstractList1Panel
{

  /**
   * 
   */
  private static final long serialVersionUID = 2231299633719318717L;

  /**
   * 
   */
  

  private PrintJobHeaderRow       _printJobHeaderRow;

  private static Resources        __resources;


  public AdministratorPrintJobPropertiesPanel(PrintJobHeaderRow printJobHeaderRow)
  {
    super();
    _printJobHeaderRow = printJobHeaderRow;
    Initialize (
      InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobPropertiesTable(),
      null ); //AdministratorPrintJobPropertyEntryPanel.class );
  }

  public AdministratorPrintJobPropertiesPanel (
    PrintJobPropertiesTable                 printJobPropertiesTable,
    Class<? extends AbstractEntry1Panel>    entryPanelClass,
    PrintJobHeaderRow                       printJobHeaderRow )
  {
    super();
    _printJobHeaderRow = printJobHeaderRow;
    Initialize (
      printJobPropertiesTable,
      entryPanelClass );
  }

  @Override
  protected String GetPredicate()
  {
    String predicate =
      " ( " + PrintJobPropertiesRow.Name_print_job_number() + " = '" + _printJobHeaderRow.Get_print_job_number() + "' ) ";

    return predicate;
  }

  @Override
  protected String GetOrderBy()
  {
    return PrintJobHeaderRow.Name_print_job_number();
  }

  @Override
  protected boolean FileNewOK()
  {
    return false;
  }

  @Override
  protected void EnableMenuItems(boolean enabledFlag)
  {
    _fileOpenMenuItem.setEnabled(false);
    _fileDeleteMenuItem.setEnabled(false);
    _fileOpenFloatingMenuItem.setEnabled(false);
    _fileDeleteFloatingMenuItem.setEnabled(false);
  }

  @Override
  protected void BuildTable()
  {
    super.BuildTable();

    _table.removeMouseListener(_openOnDoubleClickAdapter);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrintJobPropertiesPanel.class.getName());
  }
}