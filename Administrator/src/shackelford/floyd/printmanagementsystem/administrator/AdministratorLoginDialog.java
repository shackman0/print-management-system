package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractLoginDialog;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;



/**
 * <p>Title: AdministratorLoginDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorLoginDialog
  extends AbstractLoginDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = -7951898252702675371L;

  /**
   * 
   */
  

  private JPasswordField          _passphraseField = new JPasswordField();

  private static Resources        __resources;

  public static final String INVALID_PASSPHRASE_MSG = "INVALID_PASSPHRASE_MSG";
  public static final String INVALID_PASSPHRASE_TITLE = "INVALID_PASSPHRASE_TITLE";
  public static final String PASSPHRASE = "PASSPHRASE";
  public static final String PASSPHRASE_TIP = "PASSPHRASE_TIP";


  public AdministratorLoginDialog(Frame owner)
  {
    super(owner, true);
    Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // passphrase
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(PASSPHRASE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphraseField.setColumns(FIELD_WIDTH);
    _passphraseField.setToolTipText(GetResources().getProperty(PASSPHRASE_TIP));
    centerPanel.add(_passphraseField,constraints);

    return centerPanel;
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(PASSPHRASE,new String(_passphraseField.getPassword()));
    return props;
  }

  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    String passphrase = new String(_passphraseField.getPassword());
    if ( passphrase.equals(InstallationTable.GetCachedInstallationRow().Get_installation_passphrase()) == false )
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_PASSPHRASE_MSG),
        GetResources().getProperty(INVALID_PASSPHRASE_TITLE) );
      _passphraseField.setText("");
      _passphraseField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorLoginDialog.class.getName());
  }
}
