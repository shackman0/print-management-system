package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;



/**
 * <p>Title: AdministratorRefundPrePaymentCompleteMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorRefundPrePaymentCompleteMsgBox
  extends AdministratorAddCashPrePaymentCompleteMsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = 6193223276655043723L;
  /**
   * 
   */
  
  private static Resources             __resources;


  public AdministratorRefundPrePaymentCompleteMsgBox (
           PrePaymentHistoryRow  prePaymentHistoryRow,
           UserAccountRow        userAccountRow,
           Double            previousBalance )
  {
    super(prePaymentHistoryRow,userAccountRow,previousBalance);
  }

  @Override
  protected void AssignDataToFields()
  {
    _prePaymentHistoryRow.Set_amount(-_prePaymentHistoryRow.Get_amount().doubleValue());
    super.AssignDataToFields();
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorRefundPrePaymentCompleteMsgBox.class.getName());
  }

}