package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry2Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperTable;


/**
 * <p>Title: AdministratorGatekeeperEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorGatekeeperEntryPanel
  extends AbstractEntry2Panel
{

  /**
   * 
   */
  private static final long       serialVersionUID             = 1L;

  protected OverwritableTextField _ipAddressTextField          = new OverwritableTextField();

  private static Resources        __resources;

  protected static final String   IP_ADDRESS                   = "IP_ADDRESS";

  protected static final String   IP_ADDRESS_TIP               = "IP_ADDRESS_TIP";

  protected static final String   IP_ADDR_IS_BLANK_MSG         = "IP_ADDR_IS_BLANK_MSG";

  protected static final String   IP_ADDR_IS_BLANK_TITLE       = "IP_ADDR_IS_BLANK_TITLE";

  protected static final String   IP_ADDR_ALREADY_EXISTS_MSG   = "IP_ADDR_ALREADY_EXISTS_MSG";

  protected static final String   IP_ADDR_ALREADY_EXISTS_TITLE = "IP_ADDR_ALREADY_EXISTS_TITLE";

  protected static final int      IP_ADDRESS_COL_WIDTH         = 15;

  public AdministratorGatekeeperEntryPanel()
  {
    super();
  }

  public AdministratorGatekeeperEntryPanel(ListPanelCursor listPanel, GatekeeperTable gatekeeperTable, GatekeeperRow gatekeeperRow)
  {
    super();
    Initialize(listPanel, gatekeeperTable, gatekeeperRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // ip address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(IP_ADDRESS).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _ipAddressTextField.setColumns(IP_ADDRESS_COL_WIDTH);
    _ipAddressTextField.setToolTipText(__resources.getProperty(IP_ADDRESS_TIP));
    _ipAddressTextField.setEditable(ModeIsNew());
    centerPanel.add(_ipAddressTextField, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();
    _saveButton.setEnabled(ModeIsNew());
    _saveAndNewButton.setEnabled(ModeIsNew());
    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    String ipAddress = _ipAddressTextField.getText().trim();
    if (ipAddress.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(IP_ADDR_IS_BLANK_MSG), GetResources().getProperty(IP_ADDR_IS_BLANK_TITLE));
      _ipAddressTextField.requestFocus();
      return false;
    }

    GatekeeperRow gatekeeperRow = GetGatekeeperTable().GetRowForIPAddress(ipAddress);
    if (gatekeeperRow != null)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(IP_ADDR_ALREADY_EXISTS_MSG), GetResources().getProperty(IP_ADDR_ALREADY_EXISTS_TITLE));
      _ipAddressTextField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();
    _ipAddressTextField.setText(GetGatekeeperRow().Get_ip_address());
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetGatekeeperRow().Set_ip_address(_ipAddressTextField.getText().trim());
  }

  private GatekeeperTable GetGatekeeperTable()
  {
    return (GatekeeperTable) _sqlTable;
  }

  private GatekeeperRow GetGatekeeperRow()
  {
    return (GatekeeperRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorGatekeeperEntryPanel.class.getName());
  }

}
