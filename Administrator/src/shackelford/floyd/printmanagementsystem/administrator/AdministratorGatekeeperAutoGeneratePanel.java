package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.ListPanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: AdministratorGatekeeperAutoGeneratePanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorGatekeeperAutoGeneratePanel
  extends AbstractEntry1Panel
{

  /**
   * 
   */
  private static final long     serialVersionUID           = 1L;

  protected JSpinner            _ipAddr1SpinField          = new JSpinner();

  protected JSpinner            _ipAddr2SpinField          = new JSpinner();

  protected JSpinner            _ipAddr3SpinField          = new JSpinner();

  protected JSpinner            _ipAddr4SpinField          = new JSpinner();

  protected JSpinner            _numberToGenerateSpinField = new JSpinner();

  private static Resources      __resources;

  protected static final String IP_ADDRESS                 = "IP_ADDRESS";

  protected static final String IP_ADDR_1_TIP              = "IP_ADDR_1_TIP";

  protected static final String IP_ADDR_2_TIP              = "IP_ADDR_2_TIP";

  protected static final String IP_ADDR_3_TIP              = "IP_ADDR_3_TIP";

  protected static final String IP_ADDR_4_TIP              = "IP_ADDR_4_TIP";

  protected static final String NUMBER_TO_GENERATE         = "NUMBER_TO_GENERATE";

  protected static final String NUMBER_TO_GENERATE_TIP     = "NUMBER_TO_GENERATE_TIP";

  public AdministratorGatekeeperAutoGeneratePanel()
  {
    super();
  }

  public AdministratorGatekeeperAutoGeneratePanel(ListPanelCursor listPanel, GatekeeperTable gatekeeperTable, GatekeeperRow gatekeeperRow)
  {
    super();
    Initialize(listPanel, gatekeeperTable, gatekeeperRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // ip addr fields
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(IP_ADDRESS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    Box lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
    _ipAddr1SpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(0);
    spinnerNumberModel.setMaximum(255);

    _ipAddr1SpinField.setToolTipText(GetResources().getProperty(IP_ADDR_1_TIP));

    lineBox.add(_ipAddr1SpinField);

    spinnerNumberModel = new SpinnerNumberModel();
    _ipAddr2SpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(0);
    spinnerNumberModel.setMaximum(255);

    _ipAddr2SpinField.setToolTipText(GetResources().getProperty(IP_ADDR_2_TIP));

    lineBox.add(_ipAddr2SpinField);

    spinnerNumberModel = new SpinnerNumberModel();
    _ipAddr3SpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(0);
    spinnerNumberModel.setMaximum(255);

    _ipAddr3SpinField.setToolTipText(GetResources().getProperty(IP_ADDR_3_TIP));

    lineBox.add(_ipAddr3SpinField);

    spinnerNumberModel = new SpinnerNumberModel();
    _ipAddr4SpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(0);
    spinnerNumberModel.setMaximum(255);

    _ipAddr4SpinField.setToolTipText(GetResources().getProperty(IP_ADDR_4_TIP));

    lineBox.add(_ipAddr4SpinField);

    // number to generate
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(NUMBER_TO_GENERATE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    spinnerNumberModel = new SpinnerNumberModel();
    _numberToGenerateSpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(1);
    spinnerNumberModel.setMaximum(256);

    _numberToGenerateSpinField.setToolTipText(GetResources().getProperty(NUMBER_TO_GENERATE_TIP));

    lineBox.add(_numberToGenerateSpinField);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();
    _saveAndNewButton.setVisible(false);
    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    return true;
  }

  @Override
  protected void AssignFieldsToRow()
  {
    // do nothing
  }

  @Override
  protected void AssignRowToFields()
  {
    _ipAddr1SpinField.setValue(0);
    _ipAddr2SpinField.setValue(0);
    _ipAddr3SpinField.setValue(0);
    _ipAddr4SpinField.setValue(0);
    _numberToGenerateSpinField.setValue(1);
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  protected int GetMaxAllowed()
  {
    return InstallationTable.GetCachedInstallationRow().Get_max_number_of_gatekeepers().intValue();
  }

  @Override
  protected boolean SaveAction()
  {
    int currentGatekeepers = GetGatekeeperTable().RowCount(null);
    int maxGatekeepers = GetMaxAllowed();

    if (currentGatekeepers >= maxGatekeepers)
    {
      return false;
    }

    GatekeeperRow newGatekeeperEntry = (GatekeeperRow) (GetGatekeeperTable().CreateRow());
    newGatekeeperEntry.Set_installation_id(InstallationTable.GetCachedInstallationRow().Get_installation_id());

    String topIPAddr = _ipAddr1SpinField.getValue().toString() + "." + _ipAddr2SpinField.getValue().toString() + "." + _ipAddr3SpinField.getValue().toString() + ".";

    int startingIPAddr = (Integer) (_ipAddr4SpinField.getValue());
    int endingIPAddr = startingIPAddr + (Integer) (_numberToGenerateSpinField.getValue());
    if (endingIPAddr > 256)
    {
      endingIPAddr = 256;
    }

    for (int i = startingIPAddr; i < endingIPAddr; i++)
    {
      String ipAddr = topIPAddr + String.valueOf(i);
      GatekeeperRow testExist = GetGatekeeperTable().GetRowForIPAddress(ipAddr);
      if (testExist == null)
      {
        newGatekeeperEntry.Set_ip_address(ipAddr);
        newGatekeeperEntry.Insert();
        currentGatekeepers++;
        if (currentGatekeepers >= maxGatekeepers)
        {
          break;
        }
      }
    }
    NotifyListPanelListeners(false);
    NotifyListeners(SAVE_EVENT, false);
    NotifyListenersPanelStateChange(ListPanelNotifier.ACTION_SAVE);
    return false; // keep the panel up until the user cancels
  }

  protected GatekeeperTable GetGatekeeperTable()
  {
    return (GatekeeperTable) _sqlTable;
  }

  protected GatekeeperRow GetGatekeeperRow()
  {
    return (GatekeeperRow) _sqlRow;
  }

  protected InstallationTable GetInstallationTable()
  {
    return InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable();
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorGatekeeperAutoGeneratePanel.class.getName());
  }

}
