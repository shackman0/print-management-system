package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AdministratorAddCashPrePaymentEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorAddCashPrePaymentEntryPanel
  extends AbstractEntry1Panel
{

  /**
   * 
   */
  private static final long            serialVersionUID         = 1L;

  protected Double                     _previousBalance;

  protected Double                     _amount;

  protected NumericTextField           _amountField             = new NumericTextField();

  protected JLabel                     _userAccountIDLabel      = new JLabel();

  protected JLabel                     _userAccountNameLabel    = new JLabel();

  protected JLabel                     _prePaymentBalanceLabel  = new JLabel();

  protected JLabel                     _currencySymbolLabel     = new JLabel();

  private static Resources             __resources;

  protected static final DecimalFormat __formatter              = new DecimalFormat("#,##0.00");

  protected static final String        ACCOUNT_NAME             = "ACCOUNT_NAME";

  protected static final String        ACCOUNT_NUMBER           = "ACCOUNT_NUMBER";

  protected static final String        PRE_PAYMENT_BALANCE      = "PRE_PAYMENT_BALANCE";

  protected static final String        AMOUNT_TO_ADD            = "AMOUNT_TO_ADD";

  protected static final String        AMOUNT_TO_ADD_TIP        = "AMOUNT_TO_ADD_TIP";

  protected static final String        CASH                     = "CASH";

  protected static final String        MISSING_ADD_AMOUNT_MSG   = "MISSING_ADD_AMOUNT_MSG";

  protected static final String        MISSING_ADD_AMOUNT_TITLE = "MISSING_ADD_AMOUNT_TITLE";

  protected static final String        INVALID_ADD_AMOUNT_MSG   = "INVALID_ADD_AMOUNT_MSG";

  protected static final String        INVALID_ADD_AMOUNT_TITLE = "INVALID_ADD_AMOUNT_TITLE";

  protected static final int           CURRENCY_FIELD_WIDTH     = 6;

  public AdministratorAddCashPrePaymentEntryPanel()
  {
    super();
  }

  public AdministratorAddCashPrePaymentEntryPanel(ListPanelCursor listPanel, UserAccountTable userAccountTable, UserAccountRow userAccountRow)
  {
    super();
    Initialize(listPanel, userAccountTable, userAccountRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account number
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(ACCOUNT_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_userAccountIDLabel, constraints);

    // user account name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(ACCOUNT_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_userAccountNameLabel, constraints);

    // current balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(PRE_PAYMENT_BALANCE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_prePaymentBalanceLabel, constraints);

    // amount to add
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(AMOUNT_TO_ADD), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountField.SetFormat(__formatter);
    _amountField.setColumns(CURRENCY_FIELD_WIDTH);
    _amountField.setToolTipText(GetResources().getProperty(AMOUNT_TO_ADD_TIP));
    _amountField.setEnabled(true);

    centerPanel.add(_amountField, constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currencySymbolLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_currencySymbolLabel, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _nextButton.setVisible(false);
    _previousButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    String amountToAdd = new String(_amountField.getText().trim());

    if (amountToAdd.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(MISSING_ADD_AMOUNT_MSG), GetResources().getProperty(MISSING_ADD_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    Number parsedObj = null;
    try
    {
      parsedObj = (Number) __formatter.parseObject(amountToAdd);
    }
    catch (ParseException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_ADD_AMOUNT_MSG), GetResources().getProperty(INVALID_ADD_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    double addAmount = parsedObj.doubleValue();

    if (addAmount <= 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_ADD_AMOUNT_MSG), GetResources().getProperty(INVALID_ADD_AMOUNT_TITLE));
      _amountField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignFieldsToRow()
  {
    _previousBalance = GetUserAccountRow().Get_pre_payment_balance();
    _amount = new Double(_amountField.GetNumberValue().doubleValue());
    GetUserAccountRow().Set_pre_payment_balance(_previousBalance.doubleValue() + _amount.doubleValue());
  }

  @Override
  protected void AssignRowToFields()
  {
    _userAccountIDLabel.setText(GetUserAccountRow().Get_user_account_id());
    _userAccountNameLabel.setText(GetUserAccountRow().Get_user_name());
    _prePaymentBalanceLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + __formatter.format(GetUserAccountRow().Get_pre_payment_balance()));
    _amountField.setText("0");
    _currencySymbolLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol());
  }

  protected void DisplayCompletionMsgBox(PrePaymentHistoryRow prePaymentHistoryRow)
  {
    AdministratorAddCashPrePaymentCompleteMsgBox msgBox = new AdministratorAddCashPrePaymentCompleteMsgBox(prePaymentHistoryRow, GetUserAccountRow(), _previousBalance);
    msgBox.setVisible(true);
  }

  @Override
  protected boolean SaveAction()
  {
    if (super.SaveAction() == true)
    {
      PrePaymentHistoryRow prePaymentHistoryRow = new PrePaymentHistoryRow(InstallationDatabase.GetDefaultInstallationDatabase().GetPrePaymentHistoryTable(), GetUserAccountRow());
      prePaymentHistoryRow.Set_source(GetResources().getProperty(CASH));
      prePaymentHistoryRow.Set_amount(_amount);
      prePaymentHistoryRow.Insert();

      DisplayCompletionMsgBox(prePaymentHistoryRow);

      return true;
    }
    return false;
  }

  protected UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorAddCashPrePaymentEntryPanel.class.getName());
  }

}
