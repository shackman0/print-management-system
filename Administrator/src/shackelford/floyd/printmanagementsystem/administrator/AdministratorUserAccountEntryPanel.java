package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AdministratorUserAccountEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorUserAccountEntryPanel
  extends AbstractEntry4Panel
{

  /**
   * 
   */
  private static final long           serialVersionUID                  = 1L;

  private final OverwritableTextField _nameTextField                    = new OverwritableTextField();

  private final OverwritableTextField _passphraseTextField              = new OverwritableTextField();

  private final _JComboBox            _languageComboBox                 = new _JComboBox();

  private final JLabel                _prePaymentLabel                  = new JLabel();

  private final OverwritableTextField _userAccountIDTextField           = new OverwritableTextField();

  private final JCheckBox             _requireNewPasswordCheckBox       = new JCheckBox();

  private final OverwritableTextField _eMailAddressTextField            = new OverwritableTextField();

  private final OverwritableTextField _homeAddressTextField             = new OverwritableTextField();

  private final OverwritableTextField _phoneNumberTextField             = new OverwritableTextField();

  private final JTextArea             _additionalInfoTextArea           = new JTextArea();

  private RulesRow                    _flexibleUserAccountIDsRuleRow    = null;

  private ActionButton                _addPrePaymentButton;

  private ActionButton                _prePaymentsButton;

  private ActionButton                _printJobsButton;

  static Resources                    __resources;

  private static final String         ACCOUNT_NAME                      = "ACCOUNT_NAME";

  //  private static final String     ACCOUNT_NAME_TIP = "ACCOUNT_NAME_TIP";
  private static final String         ACCOUNT_PASSWORD                  = "ACCOUNT_PASSWORD";

  private static final String         ACCOUNT_PASSWORD_TIP              = "ACCOUNT_PASSWORD_TIP";

  private static final String         LANGUAGE                          = "LANGUAGE";

  //  private static final String     LANGUAGE_TIP = "LANGUAGE_TIP";
  private static final String         ADD_PRE_PAYMENT                   = "ADD_PRE_PAYMENT";

  private static final String         ADD_PRE_PAYMENT_TIP               = "ADD_PRE_PAYMENT_TIP";

  private static final String         REQUIRE_NEW_PASSWORD              = "REQUIRE_NEW_PASSWORD";

  //  private static final String     REQUIRE_NEW_PASSWORD_TIP = "REQUIRE_NEW_PASSWORD_TIP";
  private static final String         E_MAIL_ADDRESS                    = "E_MAIL_ADDRESS";

  private static final String         E_MAIL_ADDRESS_TIP                = "E_MAIL_ADDRESS_TIP";

  private static final String         HOME_ADDRESS                      = "HOME_ADDRESS";

  private static final String         HOME_ADDRESS_TIP                  = "HOME_ADDRESS_TIP";

  private static final String         PHONE_NUMBER                      = "PHONE_NUMBER";

  private static final String         PHONE_NUMBER_TIP                  = "PHONE_NUMBER_TIP";

  private static final String         ADDITIONAL_INFO                   = "ADDITIONAL_INFO";

  private static final String         ADDITIONAL_INFO_TIP               = "ADDITIONAL_INFO_TIP";

  private static final String         PRINT_JOBS                        = "PRINT_JOBS";

  private static final String         PRINT_JOBS_TIP                    = "PRINT_JOBS_TIP";

  private static final String         PRE_PAYMENTS                      = "PRE_PAYMENTS";

  private static final String         PRE_PAYMENTS_TIP                  = "PRE_PAYMENTS_TIP";

  private static final String         ACCOUNT_NUMBER                    = "ACCOUNT_NUMBER";

  private static final String         PRE_PAYMENT_BALANCE               = "PRE_PAYMENT_BALANCE";

  private static final String         USER_ACCOUNT_ALREADY_EXISTS_MSG   = "USER_ACCOUNT_ALREADY_EXISTS_MSG";

  private static final String         USER_ACCOUNT_ALREADY_EXISTS_TITLE = "USER_ACCOUNT_ALREADY_EXISTS_TITLE";

  private static final String         TAKES_EFFECT_NEXT_LOGIN_MSG       = "TAKES_EFFECT_NEXT_LOGIN_MSG";

  private static final int            ADDITIONAL_INFO_ROWS              = 5;

  private static final int            ADDRESS_COLS                      = 40;

  private static final int            ADDITIONAL_INFO_COLS              = 40;

  public AdministratorUserAccountEntryPanel()
  {
    super();
  }

  public AdministratorUserAccountEntryPanel(ListPanelCursor listPanel, UserAccountTable userAccountTable, UserAccountRow userAccountRow)
  {
    super();
    Initialize(listPanel, userAccountTable, userAccountRow);
  }

  @Override
  public void Initialize(ListPanelCursor listPanel, AbstractSQL1Table userAccountTable, AbstractSQL1Row userAccountRow)
  {
    _flexibleUserAccountIDsRuleRow = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable().GetRowForRuleName(RulesRow.FLEXIBLE_USER_ACCOUNT_IDS);
    super.Initialize(listPanel, userAccountTable, userAccountRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      if ((_flexibleUserAccountIDsRuleRow == null) || (_flexibleUserAccountIDsRuleRow.RuleIsNo() == true))
      {
        GetUserAccountRow().SetNewUserAccountID();
      }
    }
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account number
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx = constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDTextField.setColumns(8);
    _userAccountIDTextField.setEditable(false);

    if (_mode == MODE_NEW)
    {
      if ((_flexibleUserAccountIDsRuleRow != null) && (_flexibleUserAccountIDsRuleRow.RuleIsYes() == true))
      {
        _userAccountIDTextField.setEditable(true);
      }
    }

    centerPanel.add(_userAccountIDTextField, constraints);

    // account name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx = constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _nameTextField.setColumns(16);
    centerPanel.add(_nameTextField, constraints);

    // pre-payment balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PRE_PAYMENT_BALANCE), SwingConstants.RIGHT), constraints);

    constraints.gridx = constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _prePaymentLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_prePaymentLabel, constraints);

    // account password
    if (GlobalAttributes.__mainClass == Administrator.class)
    {
      constraints.gridy += 1;

      constraints.gridx = 0;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.NONE;
      constraints.anchor = GridBagConstraints.EAST;

      centerPanel.add(new JLabel(__resources.getProperty(ACCOUNT_PASSWORD), SwingConstants.RIGHT), constraints);

      constraints.gridx += constraints.gridwidth;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.NONE;
      constraints.anchor = GridBagConstraints.WEST;

      _passphraseTextField.setColumns(16);
      _passphraseTextField.setToolTipText(__resources.getProperty(ACCOUNT_PASSWORD_TIP));
      centerPanel.add(_passphraseTextField, constraints);

      constraints.gridx += constraints.gridwidth;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.NONE;
      constraints.anchor = GridBagConstraints.WEST;

      _requireNewPasswordCheckBox.setText(__resources.getProperty(REQUIRE_NEW_PASSWORD));
      _requireNewPasswordCheckBox.setHorizontalTextPosition(SwingConstants.LEFT);
      centerPanel.add(_requireNewPasswordCheckBox, constraints);
    }

    // e-mail address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(E_MAIL_ADDRESS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _eMailAddressTextField.setColumns(16);
    _eMailAddressTextField.setToolTipText(__resources.getProperty(E_MAIL_ADDRESS_TIP));
    centerPanel.add(_eMailAddressTextField, constraints);

    // home address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(HOME_ADDRESS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _homeAddressTextField.setColumns(ADDRESS_COLS);
    _homeAddressTextField.setToolTipText(__resources.getProperty(HOME_ADDRESS_TIP));
    centerPanel.add(_homeAddressTextField, constraints);

    // phone number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PHONE_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _phoneNumberTextField.setColumns(16);
    _phoneNumberTextField.setToolTipText(__resources.getProperty(PHONE_NUMBER_TIP));
    centerPanel.add(_phoneNumberTextField, constraints);

    // language
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LANGUAGE), SwingConstants.RIGHT), constraints);

    constraints.gridx = constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(_languageComboBox, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(new JLabel(__resources.getProperty(TAKES_EFFECT_NEXT_LOGIN_MSG), SwingConstants.LEFT), constraints);

    // additional info
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(ADDITIONAL_INFO), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _additionalInfoTextArea.setRows(ADDITIONAL_INFO_ROWS);
    _additionalInfoTextArea.setColumns(ADDITIONAL_INFO_COLS);
    _additionalInfoTextArea.setLineWrap(true);
    _additionalInfoTextArea.setWrapStyleWord(true);
    _additionalInfoTextArea.setBorder(_eMailAddressTextField.getBorder());
    _additionalInfoTextArea.setToolTipText(__resources.getProperty(ADDITIONAL_INFO_TIP));

    JScrollPane scrollPane = new JScrollPane(_additionalInfoTextArea);
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    centerPanel.add(scrollPane, constraints);

    return centerPanel;

  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    if (GlobalAttributes.__mainClass != Administrator.class) // == UserAccount.class
    {
      _previousButton.setVisible(false);
      _nextButton.setVisible(false);
      _saveAndNewButton.setVisible(false);
    }

    return container;
  }

  @Override
  protected void AddActionButtonsToSouthPanel(Container southPanel)
  {
    _prePaymentsButton = new ActionButton(new PrePaymentsAction());
    _prePaymentsButton.setEnabled(ModeIsUpdate());
    southPanel.add(_prePaymentsButton);

    _printJobsButton = new ActionButton(new PrintJobsAction());
    _printJobsButton.setEnabled(ModeIsUpdate());
    southPanel.add(_printJobsButton);

    _addPrePaymentButton = new ActionButton(new AddPrePaymentAction());
    if ((GlobalAttributes.__mainClass == Administrator.class) && (_mode == MODE_UPDATE) && (GetUserAccountRow().StatusIsClosed() == false))
    {
      southPanel.add(_addPrePaymentButton);
    }
  }

  @Override
  protected boolean FieldsValid()
  {
    if (_mode == MODE_NEW)
    {
      if ((_flexibleUserAccountIDsRuleRow != null) && (_flexibleUserAccountIDsRuleRow.RuleIsYes() == true))
      {
        if (GetUserAccountTable().GetRowForUserAccountID(_userAccountIDTextField.getText()) != null)
        {
          MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(USER_ACCOUNT_ALREADY_EXISTS_MSG), __resources.getProperty(USER_ACCOUNT_ALREADY_EXISTS_TITLE));
          return false;
        }
      }
    }
    return super.FieldsValid();
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _userAccountIDTextField.setText(GetUserAccountRow().Get_user_account_id());
    _nameTextField.setText(GetUserAccountRow().Get_user_name());

    if (GlobalAttributes.__mainClass == Administrator.class)
    {
      _passphraseTextField.setText(GetUserAccountRow().Get_user_passphrase());
      _requireNewPasswordCheckBox.setSelected(GetUserAccountRow().Get_require_new_password().booleanValue());
    }

    _additionalInfoTextArea.setText(GetUserAccountRow().Get_additional_info());
    _eMailAddressTextField.setText(GetUserAccountRow().Get_e_mail_address());
    _homeAddressTextField.setText(GetUserAccountRow().Get_home_address());
    _phoneNumberTextField.setText(GetUserAccountRow().Get_phone_number());
    _prePaymentLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + GetUserAccountRow().GetFormatted_pre_payment_balance());

    String[] languages = Resources.GetLanguages();
    String language = GetUserAccountRow().Get_language();
    _languageComboBox.RecreateComboBox(languages, language, false);
  }

  @Override
  protected void AssignFieldsToRow()
  {
    super.AssignFieldsToRow();

    GetUserAccountRow().Set_user_account_id(_userAccountIDTextField.getText());
    GetUserAccountRow().Set_user_name(_nameTextField.getText());

    if (GlobalAttributes.__mainClass == Administrator.class)
    {
      GetUserAccountRow().Set_user_passphrase(_passphraseTextField.getText());
      GetUserAccountRow().Set_require_new_password(_requireNewPasswordCheckBox.isSelected());
    }

    GetUserAccountRow().Set_additional_info(_additionalInfoTextArea.getText());
    GetUserAccountRow().Set_language(_languageComboBox.getSelectedItem().toString());
    GetUserAccountRow().Set_e_mail_address(_eMailAddressTextField.getText());
    GetUserAccountRow().Set_home_address(_homeAddressTextField.getText());
    GetUserAccountRow().Set_phone_number(_phoneNumberTextField.getText());
  }

  @Override
  public void EndOfPanel(char action)
  {
    if (action == PanelNotifier.ACTION_SAVE)
    {
      AssignRowToFields();
    }
  }

  protected void PrePaymentsAction()
  {
    AdministratorPrePaymentsPanel panel = new AdministratorPrePaymentsPanel(GetUserAccountRow().Get_user_account_id());
    panel.setVisible(true);
    panel.toFront();
  }

  protected class PrePaymentsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 3831351412941995953L;

    /**
     * 
     */
    

    public PrePaymentsAction()
    {
      putValue(Action.NAME, __resources.getProperty(PRE_PAYMENTS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PRE_PAYMENTS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrePaymentsAction();
    }
  }

  protected void PrintJobsAction()
  {
    AdministratorPrintJobsPanel panel = new AdministratorPrintJobsPanel(GetUserAccountRow().Get_user_account_id());
    panel.setVisible(true);
    panel.toFront();
  }

  protected class PrintJobsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -936511889324562693L;

    /**
     * 
     */
    

    public PrintJobsAction()
    {
      putValue(Action.NAME, __resources.getProperty(PRINT_JOBS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PRINT_JOBS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintJobsAction();
    }
  }

  protected void AddPrePaymentAction()
  {
    AdministratorAddCashPrePaymentEntryPanel panel = new AdministratorAddCashPrePaymentEntryPanel(_listPanel, GetUserAccountTable(), GetUserAccountRow());
    panel.AddPanelListener(this);
    panel.AddListPanelListeners(_listPanelListeners);
    panel.setVisible(true);
    panel.toFront();
  }

  protected class AddPrePaymentAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -7259826497853903243L;

    /**
     * 
     */
    

    public AddPrePaymentAction()
    {
      putValue(Action.NAME, __resources.getProperty(ADD_PRE_PAYMENT));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(ADD_PRE_PAYMENT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      AddPrePaymentAction();
    }
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  public UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable) _sqlTable;
  }

  public UserAccountRow GetUserAccountRow()
  {
    return (UserAccountRow) _sqlRow;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorUserAccountEntryPanel.class.getName());
  }

}
