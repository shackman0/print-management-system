package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.AbstractList1Panel;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;


/**
 * <p>Title: AdministratorPrintChargesPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrintChargesPanel
  extends AbstractList1Panel
{

  /**
   * 
   */
  private static final long serialVersionUID = -80136757249657944L;
  /**
   * 
   */
  
  private static Resources        __resources;

  public AdministratorPrintChargesPanel()
  {
    super();
    Initialize (
      InstallationDatabase.GetDefaultInstallationDatabase().GetPageChargesTable(),
      AdministratorPrintChargeEntryPanel.class );
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrintChargesPanel.class.getName());
  }

}