package shackelford.floyd.printmanagementsystem.administrator;


import shackelford.floyd.printmanagementsystem.common.AbstractList2Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL2Row;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryTable;


/**
 * <p>Title: AdministratorPrePaymentsPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrePaymentsPanel
  extends AbstractList2Panel
{

  /**
   * 
   */
  private static final long serialVersionUID = -2386343461188011570L;

  /**
   * 
   */
  

  private String                  _userAccountID = null;

  private static Resources        __resources;


  public AdministratorPrePaymentsPanel()
  {
    super();
    Initialize (
      InstallationDatabase.GetDefaultInstallationDatabase().GetPrePaymentHistoryTable(),
      AdministratorPrePaymentEntryPanel.class );
  }

  public AdministratorPrePaymentsPanel (
    PrePaymentHistoryTable   prePaymentHistoryTable,
    Class<AdministratorPrePaymentEntryPanel>                    entryPanelClass )
  {
    super();
    Initialize (
      prePaymentHistoryTable,
      entryPanelClass );
  }

  public AdministratorPrePaymentsPanel(String userAccountID)
  {
    super();
    _userAccountID = userAccountID.trim();
    Initialize (
      InstallationDatabase.GetDefaultInstallationDatabase().GetPrePaymentHistoryTable(),
      AdministratorPrePaymentEntryPanel.class );
  }

  @Override
  protected String GetPredicate()
  {
    String predicate = "";
    if ( (_userAccountID != null) &&
         (_userAccountID.length() > 0) )
    {
      predicate = " ( " + PrePaymentHistoryRow.Name_user_account_id() + " = '" + _userAccountID + "' ) ";
    }
    return predicate;
  }

  @Override
  protected String GetOrderBy()
  {
    return AbstractSQL2Row.Name_created_dts();
  }

  @Override
  protected boolean FileNewOK()
  {
    return false;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrePaymentsPanel.class.getName());
  }
}