package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import shackelford.floyd.printmanagementsystem.common.AbstractList2Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderTable;


/**
 * <p>Title: AdministratorPrintJobsPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorPrintJobsPanel
  extends AbstractList2Panel
{

  /**
   * 
   */
  private static final long serialVersionUID = -352859119184392287L;

  /**
   * 
   */
  

  @SuppressWarnings("unused")
  private JMenuItem           _editPrintJobPropertiesMenuItem;

  private String              _userAccountID                = null;

  private static Resources    __resources;

  private static final String EDIT_PRINT_JOB_PROPERTIES     = "EDIT_PRINT_JOB_PROPERTIES";

  private static final String EDIT_PRINT_JOB_PROPERTIES_TIP = "EDIT_PRINT_JOB_PROPERTIES_TIP";

  public AdministratorPrintJobsPanel()
  {
    super();
    Initialize(InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable(), AdministratorPrintJobEntryPanel.class);
  }

  public AdministratorPrintJobsPanel(PrintJobHeaderTable printJobHeaderTable, Class<AdministratorPrintJobEntryPanel> entryPanelClass)
  {
    super();
    Initialize(printJobHeaderTable, entryPanelClass);
  }

  public AdministratorPrintJobsPanel(String userAccountID)
  {
    super();
    _userAccountID = userAccountID.trim();
    Initialize(InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable(), AdministratorPrintJobEntryPanel.class);
  }

  @Override
  protected void AddEditMenuItems(JMenu editMenu)
  {
    super.AddEditMenuItems(editMenu);

    _editPrintJobPropertiesMenuItem = editMenu.add(new ActionMenuItem(new EditPrintJobPropertiesAction()));
    editMenu.addSeparator();
  }

  @Override
  protected void AddFloatingEditMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingEditMenuItems(popupMenu);

    _editPrintJobPropertiesMenuItem = popupMenu.add(new ActionMenuItem(new EditPrintJobPropertiesAction()));
    popupMenu.addSeparator();
  }

  @Override
  protected String GetPredicate()
  {
    String predicate = "";
    if ((_userAccountID != null) && (_userAccountID.length() > 0))
    {
      predicate = " ( " + PrintJobHeaderRow.Name_user_account_id() + " = '" + _userAccountID + "' ) ";
    }
    return predicate;
  }

  @Override
  protected String GetOrderBy()
  {
    return PrintJobHeaderRow.Name_print_job_number();
  }

  @Override
  protected boolean FileNewOK()
  {
    return false;
  }

  @Override
  protected void EnableMenuItems(boolean enabledFlag)
  {
    super.EnableMenuItems(enabledFlag);

    _fileDeleteMenuItem.setEnabled(false);
    _fileDeleteFloatingMenuItem.setEnabled(false);
  }

  protected void EditPrintJobPropertiesAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      for (int i = 0; i < sqlRows.length; ++i)
      {
        PrintJobHeaderRow headerRow = (PrintJobHeaderRow) (sqlRows[i]);
        AdministratorPrintJobPropertiesPanel panel = new AdministratorPrintJobPropertiesPanel(headerRow);
        panel.setVisible(true);
      }
    }
  }

  protected class EditPrintJobPropertiesAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 4535579180463300699L;

    /**
     * 
     */
    

    public EditPrintJobPropertiesAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_PRINT_JOB_PROPERTIES));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_PRINT_JOB_PROPERTIES_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditPrintJobPropertiesAction();
    }
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorPrintJobsPanel.class.getName());
  }
}
