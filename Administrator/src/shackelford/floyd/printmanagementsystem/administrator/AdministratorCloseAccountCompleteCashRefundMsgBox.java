package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.text.DecimalFormat;
import java.text.MessageFormat;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;


/**
 * <p>Title: AdministratorCloseAccountCompleteCashRefundMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorCloseAccountCompleteCashRefundMsgBox
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long          serialVersionUID    = 1L;

  //  private ActionButton                 _pageSetupButton;
  //  private ActionButton                 _printButton;
  @SuppressWarnings("unused")
  private final PrePaymentHistoryRow _prePaymentHistoryRow;

  final UserAccountRow               _userAccountRow;

  private final InstallationRow      _installationRow;

  @SuppressWarnings("unused")
  private final Double               _previousBalance;

  PageFormat                         _pageFormat;

  private String                     _dateTimeStamp;

  static Resources                   __resources;

  private static final String        PRINT               = "PRINT";

  private static final String        PRINT_TIP           = "PRINT_TIP";

  private static final String        MSG_1               = "MSG_1";

  private static final String        MSG_2               = "MSG_2";

  private static final String        MSG_3               = "MSG_3";

  private static final String        MSG_4               = "MSG_4";

  private static final String        PAGE_SETUP          = "PAGE_SETUP";

  private static final String        PAGE_SETUP_TIP      = "PAGE_SETUP_TIP";

  private static final String        SIGNATURE           = "SIGNATURE";

  private static final String        TIME_STAMP          = "TIME_STAMP";

  private static final String        USER_ACCOUNT        = "USER_ACCOUNT";

  private static final String        USER_NAME           = "USER_NAME";

  private static final String        PRE_PAYMENT_BALANCE = "PRE_PAYMENT_BALANCE";

  public AdministratorCloseAccountCompleteCashRefundMsgBox(PrePaymentHistoryRow prePaymentHistoryRow, UserAccountRow userAccountRow, InstallationRow installationRow, Double previousBalance)
  {
    super(null, true);
    _prePaymentHistoryRow = prePaymentHistoryRow;
    _userAccountRow = userAccountRow;
    _installationRow = installationRow;
    _previousBalance = previousBalance;
    super.Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    Box centerPanel = new Box(BoxLayout.Y_AXIS);

    // fields of message text 1.
    //   {0} = user account id

    MessageFormat msg1Format = new MessageFormat(__resources.getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { _userAccountRow.Get_user_account_id() };
    String msg1String = msg1Format.format(substitutionValues).trim();
    int cols = 40;
    int rows = (msg1String.length() / cols);

    JTextArea textArea = new JTextArea(msg1String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(centerPanel.getBackground());
    centerPanel.add(textArea);

    // fields of message text 2
    //   {0} = currency symbol
    //   {1,number,currency} = amount to refund

    MessageFormat msg2Format = new MessageFormat(__resources.getProperty(MSG_2));
    substitutionValues = new Object[] { _installationRow.Get_currency_symbol(), _userAccountRow.Get_pre_payment_balance() };
    String msg2String = msg2Format.format(substitutionValues).trim();
    rows = (msg2String.length() / cols);

    textArea = new JTextArea(msg2String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(centerPanel.getBackground());
    centerPanel.add(textArea);

    _dateTimeStamp = SystemUtilities.GetTimestamp();
    centerPanel.add(new JLabel(_dateTimeStamp, SwingConstants.LEFT));

    return centerPanel;
  }

  @Override
  protected void AddSouthPanelButtons(Box buttonBox)
  {
    buttonBox.add(new ActionButton(new PageSetupAction()));
    buttonBox.add(new ActionButton(new PrintAction()));
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  protected void PageSetupAction()
  {
    PrinterJob printJob = PrinterJob.getPrinterJob();
    if (_pageFormat == null)
    {
      _pageFormat = printJob.defaultPage();
    }
    _pageFormat = printJob.pageDialog(_pageFormat);
  }

  private class PageSetupAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -744035403330572094L;

    /**
     * 
     */
    

    public PageSetupAction()
    {
      putValue(Action.NAME, __resources.getProperty(PAGE_SETUP));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PAGE_SETUP_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PageSetupAction();
    }
  }

  private class PrintAction
    extends AbstractAction
    implements Printable
  {
    /**
     * 
     */
    private static final long serialVersionUID = -690627603848122356L;

    /**
     * 
     */
    

    public PrintAction()
    {
      putValue(Action.NAME, __resources.getProperty(PRINT));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PRINT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrinterJob printJob = PrinterJob.getPrinterJob();
      if (_pageFormat == null)
      {
        _pageFormat = printJob.defaultPage();
      }
      printJob.setPrintable(this, _pageFormat);
      if (printJob.printDialog() == true)
      {
        try
        {
          printJob.print();
        }
        catch (PrinterException excp)
        {
          excp.printStackTrace();
          JOptionPane.showMessageDialog(null, excp, "printJob.print()", JOptionPane.ERROR_MESSAGE);
          return;
        }

      }
    }

    @SuppressWarnings("synthetic-access")
    @Override
    public int print(Graphics g, PageFormat pageFormat, int pageNumber)
    {
      // remember: page numbers are zero (0) relative!
      if (pageNumber > 0)
      {
        return Printable.NO_SUCH_PAGE;
      }

      Graphics2D g2D = (Graphics2D) g;

      g2D.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

      float x1 = 0.0f;
      float x2 = 200.0f;
      float y = 0.0f;
      float incr = 20.0f;

      // installation name
      y += incr;
      g2D.drawString(_installationRow.Get_installation_name(), x1, y);

      // title
      y += incr;
      g2D.drawString(__resources.getProperty(TITLE), x1, y);

      // message 6
      y += incr;
      g2D.drawString(__resources.getProperty(MSG_3), x1, y);

      // acct id
      y += (incr * 2.0f);
      g2D.drawString(__resources.getProperty(USER_ACCOUNT), x1, y);
      g2D.drawString(_userAccountRow.Get_user_account_id(), x2, y);

      // user name
      y += incr;
      g2D.drawString(__resources.getProperty(USER_NAME), x1, y);
      g2D.drawString(_userAccountRow.Get_user_name(), x2, y);

      // pre-payment balance
      y += incr;
      g2D.drawString(__resources.getProperty(PRE_PAYMENT_BALANCE), x1, y);
      DecimalFormat formatter = new DecimalFormat("#,##0.00");
      String balance = formatter.format(_userAccountRow.Get_pre_payment_balance());
      g2D.drawString(_installationRow.Get_currency_symbol() + " " + balance, x2, y);

      // dts
      y += incr;
      g2D.drawString(__resources.getProperty(TIME_STAMP), x1, y);
      g2D.drawString(_dateTimeStamp, x2, y);

      // signature
      y += (incr * 2.0f);
      g2D.drawString(__resources.getProperty(SIGNATURE), x1, y);
      g2D.drawString("_____________________________", x2, y);

      // message 7
      y += incr;
      g2D.drawString(__resources.getProperty(MSG_4), x1, y);

      return Printable.PAGE_EXISTS;
    }
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorCloseAccountCompleteCashRefundMsgBox.class.getName());
  }

}
