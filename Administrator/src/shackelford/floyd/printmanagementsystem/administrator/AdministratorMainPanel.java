package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.HelpMenu;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.printercontrolstation.PrinterControlMainPanel;


/**
 * <p>Title: AdministratorMainPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorMainPanel
  extends AbstractEntry4Panel
{

  /**
   * 
   */
  private static final long     serialVersionUID             = 1L;

  private final JLabel          _installationIDLabel         = new JLabel();

  private final JLabel          _installationNameLabel       = new JLabel();

  private final JLabel          _upgradePreferenceLabel      = new JLabel();

  private final JLabel          _interimExpirationDateLabel  = new JLabel();

  private final JLabel          _licenseExpirationDateLabel  = new JLabel();

  private final JLabel          _currentReleaseNumberLabel   = new JLabel();

  private final JLabel          _languageLabel               = new JLabel();

  private final JLabel          _currencySymbolLabel         = new JLabel();

  private final JLabel          _thousandsSeparatorLabel     = new JLabel();

  private final JLabel          _decimalSeparatorLabel       = new JLabel();

  private final JLabel          _maxNumberOfClientsLabel     = new JLabel();

  private final JLabel          _maxNumberOfGatekeepersLabel = new JLabel();

  static Resources              __resources;

  protected static final String FILE                         = "FILE";

  protected static final String FILE_USER_ACCOUNTS           = "FILE_USER_ACCOUNTS";

  protected static final String FILE_USER_ACCOUNTS_TIP       = "FILE_USER_ACCOUNTS_TIP";

  protected static final String FILE_PRE_PAYMENTS            = "FILE_PRE_PAYMENTS";

  protected static final String FILE_PRE_PAYMENTS_TIP        = "FILE_PRE_PAYMENTS_TIP";

  protected static final String FILE_PRINT_JOBS              = "FILE_PRINT_JOBS";

  protected static final String FILE_PRINT_JOBS_TIP          = "FILE_PRINT_JOBS_TIP";

  protected static final String FILE_PRINTER_CONTROL         = "FILE_PRINTER_CONTROL";

  protected static final String FILE_PRINTER_CONTROL_TIP     = "FILE_PRINTER_CONTROL_TIP";

  protected static final String FILE_PRINT_CHARGES           = "FILE_PRINT_CHARGES";

  protected static final String FILE_PRINT_CHARGES_TIP       = "FILE_PRINT_CHARGES_TIP";

  protected static final String FILE_CLIENTS                 = "FILE_CLIENTS";

  protected static final String FILE_CLIENTS_TIP             = "FILE_CLIENTS_TIP";

  protected static final String FILE_EXIT                    = "FILE_EXIT";

  protected static final String FILE_EXIT_TIP                = "FILE_EXIT_TIP";

  protected static final String FILE_GATEKEEPERS             = "FILE_GATEKEEPERS";

  protected static final String FILE_GATEKEEPERS_TIP         = "FILE_GATEKEEPERS_TIP";

  protected static final String EDIT                         = "EDIT";

  protected static final String EDIT_CHANGE_PASSPHRASE       = "EDIT_CHANGE_PASSPHRASE";

  protected static final String EDIT_CHANGE_PASSPHRASE_TIP   = "EDIT_CHANGE_PASSPHRASE_TIP";

  protected static final String REPORT                       = "REPORT";

  protected static final String REPORT_RECONCILLIATION       = "REPORT_RECONCILLIATION";

  protected static final String REPORT_RECONCILLIATION_TIP   = "REPORT_RECONCILLIATION_TIP";

  protected static final String INSTALLATION_ID              = "INSTALLATION_ID";

  protected static final String INSTALLATION_NAME            = "INSTALLATION_NAME";

  protected static final String INTERIM_EXPIRATION_DATE      = "INTERIM_EXPIRATION_DATE";

  protected static final String CURRENCY_SYMBOL              = "CURRENCY_SYMBOL";

  protected static final String LICENSE_EXPIRATION_DATE      = "LICENSE_EXPIRATION_DATE";

  protected static final String LANGUAGE                     = "LANGUAGE";

  protected static final String DECIMAL_SEPARATOR            = "DECIMAL_SEPARATOR";

  protected static final String THOUSANDS_SEPARATOR          = "THOUSANDS_SEPARATOR";

  protected static final String UPGRADE_PREFERENCE           = "UPGRADE_PREFERENCE";

  protected static final String MAX_CLIENTS                  = "MAX_CLIENTS";

  protected static final String MAX_GATEKEEPERS              = "MAX_GATEKEEPERS";

  protected static final String CURRENT_RELEASE_NUMBER       = "CURRENT_RELEASE_NUMBER";

  public AdministratorMainPanel()
  {
    super();
    Initialize(null, InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable(), InstallationTable.GetCachedInstallationRow());
  }

  @Override
  public void Initialize(ListPanelCursor listPanel, AbstractSQL1Table sqlTable, AbstractSQL1Row sqlRow)
  {
    super.Initialize(listPanel, sqlTable, sqlRow);

    JMenuBar menuBar = new JMenuBar();
    menuBar.add(CreateFileMenu());
    menuBar.add(CreateEditMenu());
    menuBar.add(CreateReportMenu());
    menuBar.add(new HelpMenu(this));
    setJMenuBar(menuBar);

    pack();
    SizePanel();
    PositionInitialPanel();
    validate();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    Font valueFont = centerPanel.getFont();
    valueFont = new Font(valueFont.getName(), Font.BOLD, valueFont.getSize() + 2);

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // Installation ID
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _installationIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _installationIDLabel.setFont(valueFont);
    centerPanel.add(_installationIDLabel, constraints);

    // Installation Name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _installationNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _installationNameLabel.setFont(valueFont);
    centerPanel.add(_installationNameLabel, constraints);

    // upgrade preference
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(UPGRADE_PREFERENCE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _upgradePreferenceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _upgradePreferenceLabel.setFont(valueFont);
    centerPanel.add(_upgradePreferenceLabel, constraints);

    // interimExpirationDate
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INTERIM_EXPIRATION_DATE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _interimExpirationDateLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _interimExpirationDateLabel.setFont(valueFont);
    centerPanel.add(_interimExpirationDateLabel, constraints);

    // licenseExpirationDate
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LICENSE_EXPIRATION_DATE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _licenseExpirationDateLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _licenseExpirationDateLabel.setFont(valueFont);
    centerPanel.add(_licenseExpirationDateLabel, constraints);

    // currentReleaseNumber
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENT_RELEASE_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currentReleaseNumberLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _currentReleaseNumberLabel.setFont(valueFont);
    centerPanel.add(_currentReleaseNumberLabel, constraints);

    // language
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LANGUAGE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _languageLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _languageLabel.setFont(valueFont);
    centerPanel.add(_languageLabel, constraints);

    // currencySymbol
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY_SYMBOL), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currencySymbolLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _currencySymbolLabel.setFont(valueFont);
    centerPanel.add(_currencySymbolLabel, constraints);

    // thousandsSeparator
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(THOUSANDS_SEPARATOR), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _thousandsSeparatorLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _thousandsSeparatorLabel.setFont(valueFont);
    centerPanel.add(_thousandsSeparatorLabel, constraints);

    // decimalSeparator
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(DECIMAL_SEPARATOR), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _decimalSeparatorLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _decimalSeparatorLabel.setFont(valueFont);
    centerPanel.add(_decimalSeparatorLabel, constraints);

    // max clients
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(MAX_CLIENTS), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _maxNumberOfClientsLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _maxNumberOfClientsLabel.setFont(valueFont);
    centerPanel.add(_maxNumberOfClientsLabel, constraints);

    // max gatekeepers
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(MAX_GATEKEEPERS), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _maxNumberOfGatekeepersLabel.setHorizontalAlignment(SwingConstants.LEFT);
    _maxNumberOfGatekeepersLabel.setFont(valueFont);
    centerPanel.add(_maxNumberOfGatekeepersLabel, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    _saveButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  public void AssignRowToFields()
  {
    _installationIDLabel.setText(GetInstallationRow().Get_installation_id().toString());
    _installationNameLabel.setText(GetInstallationRow().Get_installation_name());
    _upgradePreferenceLabel.setText(GetInstallationRow().Get_upgrade_preference());
    _interimExpirationDateLabel.setText(GetInstallationRow().Get_interim_expiration_date().toString());
    _licenseExpirationDateLabel.setText(GetInstallationRow().Get_license_expiration_date().toString());
    _currentReleaseNumberLabel.setText(GetInstallationRow().Get_current_release_number().toString());
    _languageLabel.setText(GetInstallationRow().Get_language());
    _currencySymbolLabel.setText(GetInstallationRow().Get_currency_symbol());
    _thousandsSeparatorLabel.setText(GetInstallationRow().Get_thousands_separator());
    _decimalSeparatorLabel.setText(GetInstallationRow().Get_decimal_separator());
    _maxNumberOfClientsLabel.setText(GetInstallationRow().Get_max_number_of_clients().toString());
    _maxNumberOfGatekeepersLabel.setText(GetInstallationRow().Get_max_number_of_gatekeepers().toString());
  }

  protected JMenu CreateFileMenu()
  {
    JMenu fileMenu = new JMenu(GetResources().getProperty(FILE));

    fileMenu.add(new ActionMenuItem(new FileUserAccountsAction()));
    fileMenu.add(new ActionMenuItem(new FilePrintJobsAction()));
    fileMenu.add(new ActionMenuItem(new FilePrePaymentsAction()));

    ActionMenuItem actionMenuItem = new ActionMenuItem(new FilePrintChargesAction());
    actionMenuItem.setEnabled(Administrator.__printerAdministrator);
    fileMenu.add(actionMenuItem);

    actionMenuItem = new ActionMenuItem(new FileClientsAction());
    actionMenuItem.setEnabled(Administrator.__networkAdministrator);
    fileMenu.add(actionMenuItem);

    actionMenuItem = new ActionMenuItem(new FileGatekeepersAction());
    actionMenuItem.setEnabled(Administrator.__networkAdministrator);
    fileMenu.add(actionMenuItem);

    fileMenu.add(new ActionMenuItem(new FilePrinterControlAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileExitAction()));

    return fileMenu;
  }

  protected JMenu CreateEditMenu()
  {
    JMenu editMenu = new JMenu(__resources.getProperty(EDIT));

    editMenu.add(new ActionMenuItem(new EditChangePassphraseAction()));

    return editMenu;
  }

  protected JMenu CreateReportMenu()
  {
    JMenu reportMenu = new JMenu(__resources.getProperty(REPORT));

    reportMenu.add(new ActionMenuItem(new ReportReconcilliationAction()));

    return reportMenu;
  }

  protected InstallationRow GetInstallationRow()
  {
    return (InstallationRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  protected void FileUserAccountsAction()
  {
    AdministratorUserAccountsPanel panel = new AdministratorUserAccountsPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FileUserAccountsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -4585383991744227197L;

    /**
     * 
     */
    

    public FileUserAccountsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_USER_ACCOUNTS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_USER_ACCOUNTS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileUserAccountsAction();
    }

  }

  protected void FilePrePaymentsAction()
  {
    AdministratorPrePaymentsPanel panel = new AdministratorPrePaymentsPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FilePrePaymentsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 4167147644621612448L;

    /**
     * 
     */
    

    public FilePrePaymentsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRE_PAYMENTS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRE_PAYMENTS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrePaymentsAction();
    }

  }

  protected void FilePrintJobsAction()
  {
    AdministratorPrintJobsPanel panel = new AdministratorPrintJobsPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FilePrintJobsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 5281709651846099079L;

    /**
     * 
     */
    

    public FilePrintJobsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRINT_JOBS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRINT_JOBS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintJobsAction();
    }

  }

  protected void FilePrintChargesAction()
  {
    AdministratorPrintChargesPanel panel = new AdministratorPrintChargesPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FilePrintChargesAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -5787179635111217046L;

    /**
     * 
     */
    

    public FilePrintChargesAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRINT_CHARGES));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRINT_CHARGES_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintChargesAction();
    }

  }

  protected void FileClientsAction()
  {
    AdministratorClientsPanel panel = new AdministratorClientsPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FileClientsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 7502559895401665865L;

    /**
     * 
     */
    

    public FileClientsAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_CLIENTS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_CLIENTS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileClientsAction();
    }

  }

  protected void FileGatekeepersAction()
  {
    AdministratorGatekeepersPanel panel = new AdministratorGatekeepersPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class FileGatekeepersAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 8922031334285328800L;

    public FileGatekeepersAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_GATEKEEPERS));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_GATEKEEPERS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileGatekeepersAction();
    }

  }

  protected void FilePrinterControlAction()
  {
    PrinterControlMainPanel panel = new PrinterControlMainPanel();
    panel.AddPanelListener(this);
    panel.setVisible(true);
    panel.RefreshPrintersList();
    invalidate();
  }

  private class FilePrinterControlAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 31409819186724601L;

    public FilePrinterControlAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_PRINTER_CONTROL));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_PRINTER_CONTROL_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrinterControlAction();
    }

  }

  protected class FileExitAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8317481201219165484L;

    public FileExitAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_EXIT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_EXIT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      WindowClosing();
    }
  }

  protected void EditChangePassphraseAction()
  {
    AdministratorChangePassphrasePanel panel = new AdministratorChangePassphrasePanel(null, InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable(),
      InstallationTable.GetCachedInstallationRow());
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class EditChangePassphraseAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -9107153825800335897L;

    public EditChangePassphraseAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_CHANGE_PASSPHRASE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_CHANGE_PASSPHRASE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditChangePassphraseAction();
    }

  }

  protected void ReportReconcilliationAction()
  {
    AdministratorReconcilliationReportPanel panel = new AdministratorReconcilliationReportPanel();
    panel.setVisible(true);
    panel.toFront();
    invalidate();
  }

  private class ReportReconcilliationAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 835744688702862460L;

    public ReportReconcilliationAction()
    {
      putValue(Action.NAME, __resources.getProperty(REPORT_RECONCILLIATION));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(REPORT_RECONCILLIATION_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      ReportReconcilliationAction();
    }

  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorMainPanel.class.getName());
  }
}
