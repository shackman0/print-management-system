package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.text.DecimalFormat;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.PrintingMsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrePaymentHistoryRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;



/**
 * <p>Title: AdministratorAddCashPrePaymentCompleteMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorAddCashPrePaymentCompleteMsgBox
  extends PrintingMsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = -2207526810713851537L;
  /**
   * 
   */
  
  protected JLabel                     _msgLabel = new JLabel();
  protected JLabel                     _userAccountIDLabel = new JLabel();
  protected JLabel                     _userAccountNameLabel = new JLabel();
  protected JLabel                     _previousBalanceLabel = new JLabel();
  protected JLabel                     _amountAddedLabel = new JLabel();
  protected JLabel                     _newBalanceLabel = new JLabel();
  protected JLabel                     _dtsLabel = new JLabel();
  protected PrePaymentHistoryRow       _prePaymentHistoryRow;
  protected UserAccountRow             _userAccountRow;
  protected Double                     _previousBalance;

  private static Resources             __resources;
  protected static final DecimalFormat __decimalFormat = new DecimalFormat("#,##0.00");

  protected static final String        USER_ACCOUNT_ID = "USER_ACCOUNT_ID";
  protected static final String        USER_ACCOUNT_NAME = "USER_ACCOUNT_NAME";
  protected static final String        PREVIOUS_BALANCE = "PREVIOUS_BALANCE";
  protected static final String        AMOUNT_ADDED = "AMOUNT_ADDED";
  protected static final String        NEW_BALANCE = "NEW_BALANCE";
  protected static final String        MSG_LINE_1 = "MSG_LINE_1";
  protected static final String        MSG_LINE_2 = "MSG_LINE_2";
  protected static final String        SIGNATURE = "SIGNATURE";
  protected static final String        TIME_STAMP = "TIME_STAMP";


  public AdministratorAddCashPrePaymentCompleteMsgBox (
           PrePaymentHistoryRow  prePaymentHistoryRow,
           UserAccountRow        userAccountRow,
           Double            previousBalance )
  {
    super ();

    _prePaymentHistoryRow = prePaymentHistoryRow;
    _userAccountRow = userAccountRow;
    _previousBalance = previousBalance;

    Initialize (
      null,
      GetResources().getProperty(TITLE) );
  }

  @Override
  protected Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

    _msgLabel.setHorizontalAlignment(SwingConstants.CENTER);
    northPanel.add (_msgLabel);

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // user account number
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(USER_ACCOUNT_ID),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _userAccountIDLabel,
      constraints );

    // user account name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(USER_ACCOUNT_NAME),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _userAccountNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _userAccountNameLabel,
      constraints );

    // previous balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(PREVIOUS_BALANCE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _previousBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _previousBalanceLabel,
      constraints );

    // amount added
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(AMOUNT_ADDED),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _amountAddedLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _amountAddedLabel,
      constraints );

    // new balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(NEW_BALANCE),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _newBalanceLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _newBalanceLabel,
      constraints );

    // date-time stamp
    constraints.gridy += 1;

    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(TIME_STAMP),SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _dtsLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add (
      _dtsLabel,
      constraints );

    return centerPanel;
  }

  @Override
  protected void AssignDataToFields()
  {
    _msgLabel.setText(GetResources().getProperty(MSG_LINE_1));
    _userAccountIDLabel.setText(_userAccountRow.Get_user_account_id());
    _userAccountNameLabel.setText(_userAccountRow.Get_user_name());
    _previousBalanceLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + __decimalFormat.format(_previousBalance));
    _amountAddedLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + __decimalFormat.format(_prePaymentHistoryRow.Get_amount()));
    _newBalanceLabel.setText(InstallationTable.GetCachedInstallationRow().Get_currency_symbol() + __decimalFormat.format(_userAccountRow.Get_pre_payment_balance()));
    _dtsLabel.setText(_prePaymentHistoryRow.Get_created_dts());
  }

  @Override
  protected int PrintAction_Print (
    Graphics2D  g2D,
    PageFormat  pageFormat,
    int         pageNumber )
  {
    // remember: page numbers are zero (0) relative!
    if (pageNumber > 0)
    {
      return Printable.NO_SUCH_PAGE;
    }

    g2D.translate(pageFormat.getImageableX(),pageFormat.getImageableY());

    float x1 = 0.0f;
    float	x2 = 200.0f;
    float y = 0.0f;
    float incr = 20.0f;

    // installation name
    y += incr;
    g2D.drawString(InstallationTable.GetCachedInstallationRow().Get_installation_name(),x1,y);

    // title
    y += incr;
    g2D.drawString(GetResources().getProperty(TITLE),x1,y);

    // message 1
    y += incr;
    g2D.drawString(GetResources().getProperty(MSG_LINE_1),x1,y);

    // acct id
    y += (incr * 2.0f);
    g2D.drawString(GetResources().getProperty(USER_ACCOUNT_ID),x1,y);
    g2D.drawString(_userAccountRow.Get_user_account_id(),x2,y);

    // user name
    y += incr;
    g2D.drawString(GetResources().getProperty(USER_ACCOUNT_NAME),x1,y);
    g2D.drawString(_userAccountRow.Get_user_name(),x2,y);

    // previous balance
    y += incr;
    g2D.drawString(GetResources().getProperty(PREVIOUS_BALANCE),x1,y);
    g2D.drawString(__decimalFormat.format(_previousBalance),x2,y);

    // amount added
    y += incr;
    g2D.drawString(GetResources().getProperty(AMOUNT_ADDED),x1,y);
    g2D.drawString(__decimalFormat.format(_prePaymentHistoryRow.Get_amount()),x2,y);

    // new balance
    y += incr;
    g2D.drawString(GetResources().getProperty(NEW_BALANCE),x1,y);
    g2D.drawString(__decimalFormat.format(_userAccountRow.Get_pre_payment_balance()),x2,y);

    // dts
    y += incr;
    g2D.drawString(GetResources().getProperty(TIME_STAMP),x1,y);
    g2D.drawString(_prePaymentHistoryRow.Get_created_dts(),x2,y);

    // signature
    y += (incr * 2.0f);
    g2D.drawString(GetResources().getProperty(SIGNATURE),x1,y);
    g2D.drawString("_____________________________",x2,y);

    // message 2
    y += incr;
    g2D.drawString(GetResources().getProperty(MSG_LINE_2),x1,y);

    return Printable.PAGE_EXISTS;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorAddCashPrePaymentCompleteMsgBox.class.getName());
  }
}