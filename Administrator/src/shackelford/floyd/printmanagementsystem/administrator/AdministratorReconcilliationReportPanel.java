package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.util.Enumeration;

import javax.swing.JTable;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import shackelford.floyd.printmanagementsystem.common.AbstractReportPanel;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SortFilterTableModel;



/**
 * <p>Title: AdministratorReconcilliationReportPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorReconcilliationReportPanel
  extends AbstractReportPanel
{

  /**
   * 
   */
  private static final long serialVersionUID = -52393569552120786L;
  /**
   * 
   */
  
  protected AdministratorReconcilliationReportTableModel    _tableModel;
  protected SortFilterTableModel                           _sorter;

  private static Resources             __staticResources;

  protected static final int           INTRA_FIELD_GAP = 3;
  protected static final int           BORDER_GAP = 10;


  public AdministratorReconcilliationReportPanel()
  {
    super();
    Initialize();
    RefreshList();
  }

  @Override
  protected void SizeInitialPanel()
  {
    Dimension panelSize = getSize();
    panelSize.width *= 2.00f;
    panelSize.height *= 1.00f;
    setSize(panelSize);
  }

  @Override
  public void BuildTable()
  {
    _table = new MyJTable();
    _sorter = new SortFilterTableModel(_table);

    _table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _table.setCellSelectionEnabled(false);
    _table.setColumnSelectionAllowed(false);
    _table.setRowSelectionAllowed(true);
    _table.setShowVerticalLines(true);
    _table.setShowHorizontalLines(true);

    _tableModel = new AdministratorReconcilliationReportTableModel (
                        _table,
                        this );
    _sorter.SetTableModel(_tableModel);
    _table.setModel(_sorter);
    _table.doLayout();
  }

  @Override
  public void RefreshList ()
  {
    _tableModel.PopulateTable ();
    _sorter.TableModelChanged();
    _table.doLayout();
    SetTableColumnWidths();
  }

  protected void SetTableColumnWidths ()
  {
    Graphics g = _table.getGraphics();
    if (g != null)
    {
      FontMetrics fm = _table.getGraphics().getFontMetrics();
      TableColumnModel tableColumnModel = _table.getColumnModel();
      for (Enumeration<?> enumerator = tableColumnModel.getColumns(); enumerator.hasMoreElements();)
      {
        TableColumn tableColumn = (TableColumn)enumerator.nextElement();
        int stringWidth = fm.stringWidth(tableColumn.getHeaderValue().toString());
        int additionalWidth = 10;
        tableColumn.setPreferredWidth(stringWidth + additionalWidth);
      }
    }
  }

  @Override
  protected Resources GetResources()
  {
    return __staticResources;
  }

  static
  {
    __staticResources = Resources.CreateApplicationResources(AdministratorReconcilliationReportPanel.class.getName());
  }

}
