package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: AdministratorClientsPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorClientsPanel
  extends AdministratorGatekeepersPanel
{

  /**
   * 
   */
  private static final long serialVersionUID = -4236484833070366793L;
  /**
   * 
   */
  

  @SuppressWarnings("hiding")
  static Resources          __resources;

  public AdministratorClientsPanel()
  {
    super(InstallationDatabase.GetDefaultInstallationDatabase().GetClientTable(), AdministratorClientEntryPanel.class);
  }

  @Override
  protected boolean FileNewOK()
  {
    return (GetClientTable().RowCount(null) < InstallationTable.GetCachedInstallationRow().Get_max_number_of_clients().intValue());
  }

  private ClientTable GetClientTable()
  {
    return (ClientTable) _sqlTable;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  protected void FileAutoPopulateAction()
  {
    AdministratorClientAutoGeneratePanel panel = new AdministratorClientAutoGeneratePanel(this, GetClientTable(), null);
    panel.AddListPanelListener(this);
    panel.setVisible(true);
    panel.toFront();
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorClientsPanel.class.getName());
  }

}
