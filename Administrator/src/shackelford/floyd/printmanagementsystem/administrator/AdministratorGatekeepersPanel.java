package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JPopupMenu;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractList2Panel;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: AdministratorGatekeepersPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorGatekeepersPanel
  extends AbstractList2Panel
{

  //  private JMenuItem               _fileAutoPopulateMenuItem;

  /**
   * 
   */
  private static final long   serialVersionUID       = 1L;

  static Resources            __resources;

  private static final String FILE_AUTO_POPULATE     = "FILE_AUTO_POPULATE";

  private static final String FILE_AUTO_POPULATE_TIP = "FILE_AUTO_POPULATE_TIP";

  public AdministratorGatekeepersPanel()
  {
    super();
    Initialize(InstallationDatabase.GetDefaultInstallationDatabase().GetGatekeeperTable(), AdministratorGatekeeperEntryPanel.class);
  }

  public AdministratorGatekeepersPanel(GatekeeperTable gatekeeperTable, Class<? extends AbstractEntry1Panel> entryPanelClass)
  {
    super();
    Initialize(gatekeeperTable, entryPanelClass);
  }

  @Override
  protected void AddFileMenuItems(JMenu fileMenu)
  {
    super.AddFileMenuItems(fileMenu);

    fileMenu.add(new ActionMenuItem(new FileAutoPopulateAction()));
    fileMenu.addSeparator();
  }

  @Override
  protected void AddFloatingFileMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingFileMenuItems(popupMenu);

    popupMenu.add(new ActionMenuItem(new FileAutoPopulateAction()));
    popupMenu.addSeparator();
  }

  @Override
  protected String GetOrderBy()
  {
    return GatekeeperRow.Name_ip_address();
  }

  @Override
  protected boolean FileNewOK()
  {
    return (GetGatekeeperTable().RowCount(null) < InstallationTable.GetCachedInstallationRow().Get_max_number_of_gatekeepers().intValue());
  }

  private GatekeeperTable GetGatekeeperTable()
  {
    return (GatekeeperTable) _sqlTable;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  protected void FileAutoPopulateAction()
  {
    AdministratorGatekeeperAutoGeneratePanel panel = new AdministratorGatekeeperAutoGeneratePanel(this, GetGatekeeperTable(), null);
    panel.AddListPanelListener(this);
    panel.setVisible(true);
    panel.toFront();
  }

  protected class FileAutoPopulateAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -1032839750314377637L;

    /**
     * 
     */
    

    public FileAutoPopulateAction()
    {
      putValue(Action.NAME, __resources.getProperty(FILE_AUTO_POPULATE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(FILE_AUTO_POPULATE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileAutoPopulateAction();
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorGatekeepersPanel.class.getName());
  }
}
