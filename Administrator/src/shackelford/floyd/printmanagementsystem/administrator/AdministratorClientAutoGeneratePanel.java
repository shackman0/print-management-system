package shackelford.floyd.printmanagementsystem.administrator;

import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: AdministratorClientAutoGeneratePanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorClientAutoGeneratePanel
  extends AdministratorGatekeeperAutoGeneratePanel
{

  /**
   * 
   */
  private static final long serialVersionUID = -6577744852241789683L;
  /**
   * 
   */
  

  private static Resources  __resources;

  public AdministratorClientAutoGeneratePanel()
  {
    super();
  }

  public AdministratorClientAutoGeneratePanel(ListPanelCursor listPanel, ClientTable clientTable, ClientRow clientRow)
  {
    super(listPanel, clientTable, clientRow);
  }

  @Override
  protected int GetMaxAllowed()
  {
    return InstallationTable.GetCachedInstallationRow().Get_max_number_of_clients().intValue();
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorClientAutoGeneratePanel.class.getName());
  }

}
