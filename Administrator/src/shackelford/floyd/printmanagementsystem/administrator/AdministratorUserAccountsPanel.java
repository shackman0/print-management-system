package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;

import shackelford.floyd.printmanagementsystem.common.AbstractList4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: AdministratorUserAccountsPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorUserAccountsPanel
  extends AbstractList4Panel
{

  /**
   * 
   */
  private static final long   serialVersionUID            = 1L;

  private JMenuItem           _editPrePaymentsMenuItem;

  private JMenuItem           _editPrePaymentsFloatingMenuItem;

  private JMenuItem           _editPrintJobsMenuItem;

  private JMenuItem           _editPrintJobsFloatingMenuItem;

  private JMenuItem           _editCloseMenuItem;

  private JMenuItem           _editCloseFloatingMenuItem;

  private JMenuItem           _editAddPrePaymentMenuItem;

  private JMenuItem           _editAddPrePaymentFloatingMenuItem;

  private JMenuItem           _editRefundPrePaymentMenuItem;

  private JMenuItem           _editRefundPrePaymentFloatingMenuItem;

  static Resources            __resources;

  private static final String EDIT_CLOSE                  = "EDIT_CLOSE";

  private static final String EDIT_CLOSE_TIP              = "EDIT_CLOSE_TIP";

  private static final String EDIT_ADD_PRE_PAYMENT        = "EDIT_ADD_PRE_PAYMENT";

  private static final String EDIT_ADD_PRE_PAYMENT_TIP    = "EDIT_ADD_PRE_PAYMENT_TIP";

  private static final String EDIT_REFUND_PRE_PAYMENT     = "EDIT_REFUND_PRE_PAYMENT";

  private static final String EDIT_REFUND_PRE_PAYMENT_TIP = "EDIT_REFUND_PRE_PAYMENT_TIP";

  private static final String EDIT_PRE_PAYMENTS           = "EDIT_PRE_PAYMENTS";

  private static final String EDIT_PRE_PAYMENTS_TIP       = "EDIT_PRE_PAYMENTS_TIP";

  private static final String EDIT_PRINT_JOBS             = "EDIT_PRINT_JOBS";

  private static final String EDIT_PRINT_JOBS_TIP         = "EDIT_PRINT_JOBS_TIP";

  private static final String CLOSE_CONFIRMATION_MSG      = "CLOSE_CONFIRMATION_MSG";

  private static final String CLOSE_CONFIRMATION_TITLE    = "CLOSE_CONFIRMATION_TITLE";

  public AdministratorUserAccountsPanel()
  {
    super();
    Initialize(InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable(), AdministratorUserAccountEntryPanel.class);
  }

  @Override
  protected void AddEditMenuItems(JMenu editMenu)
  {
    super.AddEditMenuItems(editMenu);

    _editPrePaymentsMenuItem = editMenu.add(new ActionMenuItem(new EditPrePaymentsAction()));
    _editPrintJobsMenuItem = editMenu.add(new ActionMenuItem(new EditPrintJobsAction()));
    _editCloseMenuItem = editMenu.add(new ActionMenuItem(new EditCloseAction()));
    _editAddPrePaymentMenuItem = editMenu.add(new ActionMenuItem(new EditAddPrePaymentAction()));
    _editRefundPrePaymentMenuItem = editMenu.add(new ActionMenuItem(new EditRefundPrePaymentAction()));
  }

  @Override
  protected void AddFloatingEditMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingEditMenuItems(popupMenu);

    _editPrePaymentsFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditPrePaymentsAction()));
    _editPrintJobsFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditPrintJobsAction()));
    _editCloseFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditCloseAction()));
    _editAddPrePaymentFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditAddPrePaymentAction()));
    _editRefundPrePaymentFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditRefundPrePaymentAction()));
    popupMenu.addSeparator();
  }

  @Override
  protected void EnableMenuItems(boolean enabledFlag)
  {
    super.EnableMenuItems(enabledFlag);

    _editPrePaymentsMenuItem.setEnabled(enabledFlag);
    _editPrePaymentsFloatingMenuItem.setEnabled(enabledFlag);

    _editPrintJobsMenuItem.setEnabled(enabledFlag);
    _editPrintJobsFloatingMenuItem.setEnabled(enabledFlag);

    _editCloseMenuItem.setEnabled(enabledFlag);
    _editCloseFloatingMenuItem.setEnabled(enabledFlag);

    _editAddPrePaymentMenuItem.setEnabled(enabledFlag);
    _editAddPrePaymentFloatingMenuItem.setEnabled(enabledFlag);

    _editRefundPrePaymentMenuItem.setEnabled(enabledFlag);
    _editRefundPrePaymentFloatingMenuItem.setEnabled(enabledFlag);
  }

  @Override
  protected String GetOrderBy()
  {
    return UserAccountRow.Name_user_name();
  }

  protected void EditPrePaymentsAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      for (int i = 0; i < sqlRows.length; ++i)
      {
        UserAccountRow userAccountRow = (UserAccountRow) (sqlRows[i]);
        AdministratorPrePaymentsPanel prePaymentsPanel = new AdministratorPrePaymentsPanel(userAccountRow.Get_user_account_id());
        prePaymentsPanel.setVisible(true);
      }
    }
  }

  protected class EditPrePaymentsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 212237881516421275L;

    /**
     * 
     */
    

    public EditPrePaymentsAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_PRE_PAYMENTS).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_PRE_PAYMENTS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditPrePaymentsAction();
    }
  }

  protected void EditPrintJobsAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      for (int i = 0; i < sqlRows.length; ++i)
      {
        UserAccountRow userAccountRow = (UserAccountRow) (sqlRows[i]);
        AdministratorPrintJobsPanel printJobsPanel = new AdministratorPrintJobsPanel(userAccountRow.Get_user_account_id());
        printJobsPanel.setVisible(true);
      }
    }
  }

  protected class EditPrintJobsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -451399986079419085L;

    /**
     * 
     */
    

    public EditPrintJobsAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_PRINT_JOBS).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_PRINT_JOBS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditPrintJobsAction();
    }
  }

  protected void EditCloseAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      int rc = JOptionPane.showConfirmDialog(this, GetResources().getProperty(CLOSE_CONFIRMATION_MSG), GetResources().getProperty(CLOSE_CONFIRMATION_TITLE), JOptionPane.YES_NO_OPTION);
      if (rc == JOptionPane.YES_OPTION)
      {
        for (int i = 0; i < sqlRows.length; ++i)
        {
          AbstractSQL4Row sqlRow = (AbstractSQL4Row) (sqlRows[i]);
          if (sqlRow.OK2Close() == true)
          {
            sqlRow.Set_status(AbstractSQL4Row.STATUS_CLOSED_NAME);
            sqlRow.Update();
            //ClosingRow(sqlRow);
          }
        }
        RefreshList();
      }
    }
  }

  protected class EditCloseAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 3974343939566748825L;

    /**
     * 
     */
    

    public EditCloseAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_CLOSE).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_CLOSE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditCloseAction();
    }
  }

  protected void EditAddPrePaymentAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      if (Administrator.LoginOK(this) == true)
      {
        for (int i = 0; i < sqlRows.length; ++i)
        {
          AdministratorAddCashPrePaymentEntryPanel panel = new AdministratorAddCashPrePaymentEntryPanel(this, GetUserAccountTable(), (UserAccountRow) (sqlRows[i]));
          panel.AddListPanelListener(this);
          panel.setVisible(true);
          panel.toFront();
        }
      }
    }
  }

  protected class EditAddPrePaymentAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 7110446596634275015L;

    /**
     * 
     */
    

    public EditAddPrePaymentAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_ADD_PRE_PAYMENT).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_ADD_PRE_PAYMENT_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditAddPrePaymentAction();
    }
  }

  protected void EditRefundPrePaymentAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      if (Administrator.LoginOK(this) == true)
      {
        for (int i = 0; i < sqlRows.length; ++i)
        {
          AdministratorRefundPrePaymentEntryPanel panel = new AdministratorRefundPrePaymentEntryPanel(this, GetUserAccountTable(), (UserAccountRow) (sqlRows[i]));
          panel.AddListPanelListener(this);
          panel.setVisible(true);
          panel.toFront();
        }
      }
    }
  }

  protected class EditRefundPrePaymentAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 5287768717063354938L;

    /**
     * 
     */
    

    public EditRefundPrePaymentAction()
    {
      putValue(Action.NAME, __resources.getProperty(EDIT_REFUND_PRE_PAYMENT).toString());
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(EDIT_REFUND_PRE_PAYMENT_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditRefundPrePaymentAction();
    }
  }

  protected UserAccountTable GetUserAccountTable()
  {
    return (UserAccountTable) _sqlTable;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorUserAccountsPanel.class.getName());
  }
}
