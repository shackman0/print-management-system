package shackelford.floyd.printmanagementsystem.administrator;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry1Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;


/**
 * <p>Title: AdministratorChangePassphrasePanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AdministratorChangePassphrasePanel
  extends AbstractEntry1Panel
{

  /**
   * 
   */
  private static final long    serialVersionUID                      = 1L;

  private final JPasswordField _currentPassphrase                    = new JPasswordField();

  private final JPasswordField _newInstallationPassphrase            = new JPasswordField();

  private final JPasswordField _newInstallationPassphraseAgain       = new JPasswordField();

  private static Resources     __resources;

  private static final String  CURRENT_INSTALLATION_PASSPHRASE       = "CURRENT_INSTALLATION_PASSPHRASE";

  private static final String  CURRENT_INSTALLATION_PASSPHRASE_TIP   = "CURRENT_INSTALLATION_PASSPHRASE_TIP";

  private static final String  INVALID_PASSPHRASE_MSG                = "INVALID_PASSPHRASE_MSG";

  private static final String  INVALID_PASSPHRASE_TITLE              = "INVALID_PASSPHRASE_TITLE";

  private static final String  NEW_INSTALLATION_PASSPHRASE           = "NEW_INSTALLATION_PASSPHRASE";

  private static final String  NEW_INSTALLATION_PASSPHRASE_TIP       = "NEW_INSTALLATION_PASSPHRASE_TIP";

  private static final String  NEW_INSTALLATION_PASSPHRASE_AGAIN     = "NEW_INSTALLATION_PASSPHRASE_AGAIN";

  private static final String  NEW_INSTALLATION_PASSPHRASE_AGAIN_TIP = "NEW_INSTALLATION_PASSPHRASE_AGAIN_TIP";

  private static final String  NON_MATCHING_PASSPHRASE_MSG           = "NON_MATCHING_PASSPHRASE_MSG";

  private static final String  NON_MATCHING_PASSPHRASE_TITLE         = "NON_MATCHING_PASSPHRASE_TITLE";

  private static final int     PASSPHRASE_COLUMNS                    = 20;

  public AdministratorChangePassphrasePanel()
  {
    super();
  }

  public AdministratorChangePassphrasePanel(ListPanelCursor listPanel, InstallationTable installationTable, InstallationRow installationRow)
  {
    super();
    Initialize(listPanel, installationTable, installationRow);
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // current password
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENT_INSTALLATION_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _currentPassphrase.setColumns(PASSPHRASE_COLUMNS);
    _currentPassphrase.setHorizontalAlignment(SwingConstants.LEFT);
    _currentPassphrase.setToolTipText(__resources.getProperty(CURRENT_INSTALLATION_PASSPHRASE_TIP));
    centerPanel.add(_currentPassphrase, constraints);

    // new password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_INSTALLATION_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newInstallationPassphrase.setColumns(PASSPHRASE_COLUMNS);
    _newInstallationPassphrase.setHorizontalAlignment(SwingConstants.LEFT);
    _newInstallationPassphrase.setToolTipText(__resources.getProperty(NEW_INSTALLATION_PASSPHRASE_TIP));
    centerPanel.add(_newInstallationPassphrase, constraints);

    // new installation passphrase again
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_INSTALLATION_PASSPHRASE_AGAIN), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newInstallationPassphraseAgain.setColumns(PASSPHRASE_COLUMNS);
    _newInstallationPassphraseAgain.setHorizontalAlignment(SwingConstants.LEFT);
    _newInstallationPassphraseAgain.setToolTipText(__resources.getProperty(NEW_INSTALLATION_PASSPHRASE_AGAIN_TIP));
    centerPanel.add(_newInstallationPassphraseAgain, constraints);

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _nextButton.setVisible(false);
    _previousButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    String passphrase = new String(_currentPassphrase.getPassword());
    String newPassphrase = new String(_newInstallationPassphrase.getPassword());
    String newPassphraseAgain = new String(_newInstallationPassphraseAgain.getPassword());

    // check to see if the current passphrase is valid
    if (passphrase.equals(GetInstallationRow().Get_installation_passphrase()) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PASSPHRASE_TITLE), __resources.getProperty(INVALID_PASSPHRASE_MSG));
      _currentPassphrase.setText("");
      _currentPassphrase.requestFocus();
      return false;
    }

    // check to see if the new passphrases match
    if (newPassphrase.equals(newPassphraseAgain) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(NON_MATCHING_PASSPHRASE_TITLE), __resources.getProperty(NON_MATCHING_PASSPHRASE_MSG));
      _newInstallationPassphrase.setText("");
      _newInstallationPassphraseAgain.setText("");
      _newInstallationPassphrase.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    // do nothing
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetInstallationRow().Set_installation_passphrase(new String(_newInstallationPassphrase.getPassword()));
  }

  private InstallationRow GetInstallationRow()
  {
    return (InstallationRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(AdministratorChangePassphrasePanel.class.getName());
  }
}
