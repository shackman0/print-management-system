@echo off

rem This script builds the MaintainInstallations application.
rem You must pass in the location of JAVA, or set the JAVA variable below.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\J2SDK1.4.1

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

rem check to see if the location of JAVA was passed in

if %1. == . goto 1continue

set JAVA=%1
goto 2continue

:1continue

if not %JAVA%. == . goto 2continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:2continue

echo Cleaning up the target directory ...
if exist .\class rd /s /q .\Class
md .\class

echo Building Administrator classes now ...

%JAVA%\bin\javac -classpath .\Class;..\ECommerce\Class;..\UtilityClasses\Class;..\InstallationDatabase\Class;..\JClass\Class;%JAVA%\lib; -deprecation -d .\Class .\Java\*.java

:end

