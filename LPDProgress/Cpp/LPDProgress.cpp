#include <windows.h>
#include <memory.h>
#include <ostream.h>
#include "LPDProgress.h"


#define NUMBER_OF_JVM_OPTIONS 2

static STRING_UTF8 JVM        = "jvm.dll";        // windows
//static STRING_UTF8 JVM        = "libjvm.so";      // linux


JavaVM*           jvm = NULL;
JNIEnv*           env = NULL;

jclass            lpdProgress_class;
jmethodID         lpdProgress_mid;
jmethodID         displayReceivingPrintJobMsgBox_mid;
jmethodID         dismissReceivingPrintJobMsgBox_mid;
jmethodID         done_mid;


bool LPDProgress::JNI_Init ( void )
{

  if (jvm == NULL)
  {

    HANDLE jvmDLLHandle = LoadLibrary(JVM);

    if (jvmDLLHandle == NULL)
    {
      cout << "Unable to Load Library \"";
      cout << JVM;
      cout << "\"\n";
      return false;
    }

    JavaVMInitArgs jvmArgs;
    memset(&jvmArgs, 0, sizeof(jvmArgs));
    jvmArgs.version = JNI_VERSION_1_2;
    jvmArgs.ignoreUnrecognized = TRUE;

    JavaVMOption jvmOptions[NUMBER_OF_JVM_OPTIONS];
    jvmOptions[0].optionString = "-Djava.security.policy=LPDProgress.policy";
    jvmOptions[1].optionString = "-Djava.class.path=./LPDProgress.jar;";

    jvmArgs.nOptions = NUMBER_OF_JVM_OPTIONS;
    jvmArgs.options = jvmOptions;

    long rc = JNI_CreateJavaVM(&jvm, (void**) &env, &jvmArgs);
    if (rc == JNI_ERR)
    {
      jvm = NULL;
      cerr << "Error creating JVM.\n";
      return false;
    }

    lpdProgress_class = env->FindClass("LPDProgress");

    if (lpdProgress_class == 0)
    {
      jvm = NULL;
      cerr << "Can't find class LPDProgress.\n";
      return false;
    }

    lpdProgress_mid =
      env->GetMethodID (
             lpdProgress_class,
             "<init>",
             "(Ljava/lang/String;)"      // String clientAddress
             "V" );                      // return value: void

    if (lpdProgress_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method LPDProgress.<init>().\n";
      return false;
    }

    displayReceivingPrintJobMsgBox_mid =
      env->GetMethodID (
             lpdProgress_class,
             "DisplayReceivingPrintJobMsgBox",
             "()"                        // void
             "V" );                      // return value: void

    if (lpdProgress_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method LPDProgress.DisplayReceivingPrintJobMsgBox().\n";
      return false;
    }

    dismissReceivingPrintJobMsgBox_mid =
      env->GetMethodID (
             lpdProgress_class,
             "DismissReceivingPrintJobMsgBox",
             "()"                        // void
             "V" );                      // return value: void

    if (lpdProgress_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method LPDProgress.DismissReceivingPrintJobMsgBox().\n";
      return false;
    }

    done_mid =
      env->GetMethodID (
             lpdProgress_class,
             "Done",
             "()"                        // void
             "V" );                      // return value: void

    if (lpdProgress_mid == 0)
    {
      jvm = NULL;
      cerr << "Can't find method LPDProgress.Done().\n";
      return false;
    }

  }

  return true;
}

LPDProgress::LPDProgress(STRING_UTF8   clientAddress_utf8)
{

  if (jvm == NULL)
  {
    return;
  }

  // convert C Strings into Java Strings
  _clientAddress_j = env->NewStringUTF (clientAddress_utf8);

  // create a lpdProgress object
  _lpdProgress_j = env->NewObject (
                          lpdProgress_class,
                          lpdProgress_mid,
                          _clientAddress_j );
}

void LPDProgress::Done ( void )
{
  env->CallVoidMethod (
         _lpdProgress_j,
         done_mid );
}

void LPDProgress::DisplayReceivingPrintJobMsgBox ( void )
{
  env->CallVoidMethod (
         _lpdProgress_j,
         displayReceivingPrintJobMsgBox_mid );
}

void LPDProgress::DismissReceivingPrintJobMsgBox ( void )
{
  env->CallVoidMethod (
         _lpdProgress_j,
         dismissReceivingPrintJobMsgBox_mid );
}

void LPDProgress::JNI_Done ( void )
{
  if (jvm != NULL)
  {
    jvm->DestroyJavaVM();
    jvm = NULL;
  }
}
