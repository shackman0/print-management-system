#include <stdlib.h>
#include <jni.h>


/*
to compile this program:

  set JDK=<path to java development kit v1.3>
  cl -I%JDK%\include -I%JDK%\include\win32 GateKeeper.cpp %JDK%\lib\jvm.lib

to use this class:

  1. call JNI_Init() to initialize the JVM environment
  2. call DisplayReceivingPrintJobMsgBox()
  3. call DismissReceivingPrintJobMsgBox()
  4. call JNI_Done() when you are done

You MUST call JNI_Init() before calling anything else to init the JVM.
You MUST call JNI_Done() before you end your program to quiesce the Java Virtual Machine.

*/

typedef char*  STRING_UTF8;
typedef struct PROPERTY_UTF8 { STRING_UTF8 key; STRING_UTF8 value; } PROPERTY_UTF8;

typedef wchar_t*  STRING_UNICODE;
typedef struct    PROPERTY_UNICODE { STRING_UNICODE key; STRING_UNICODE value; } PROPERTY_UNICODE;

class LPDProgress
{

public:

  /*
  a print job properties list must contain at least these properties:
  */
  static STRING_UTF8 PRINT_JOB_NUMBER_UTF8;
  static STRING_UTF8 NUMBER_OF_PAGES_UTF8;
  static STRING_UTF8 SIDES_UTF8;
  static STRING_UTF8 SIZE_UTF8;
  static STRING_UTF8 COLOR_UTF8;
  static STRING_UTF8 PRINTER_UTF8;

  static STRING_UNICODE PRINT_JOB_NUMBER_UNICODE;
  static STRING_UNICODE NUMBER_OF_PAGES_UNICODE;
  static STRING_UNICODE SIDES_UNICODE;
  static STRING_UNICODE SIZE_UNICODE;
  static STRING_UNICODE COLOR_UNICODE;
  static STRING_UNICODE PRINTER_UNICODE;


  /*
  call Init() first to initialize the JVM environment.
  pass in the name of the java dll to be used (jdk1.3's dll is called "jvm.dll")
  */
  static bool JNI_Init ( void );

  LPDProgress(STRING_UTF8 clientAddr);

  void Done ( void );

  void DisplayReceivingPrintJobMsgBox ( void );

  void DismissReceivingPrintJobMsgBox ( void );

  /*
  if you call Init() you must call Done() when you are
  ready to shut down your program to quiesce the Java Virtual Machine.
  */
  static void JNI_Done ( void );

private:

  jobject           _lpdProgress_j;
  jstring           _clientAddress_j;

};

