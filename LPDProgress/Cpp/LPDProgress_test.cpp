#include "LPDProgress.h"
#include <ostream.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>


static STRING_UTF8 CLIENT_IP_ADDR = "-clientAddr";

static STRING_UTF8 clientIPAddr_utf8 = NULL;


#define NUM_OF_PROPS 7

void Usage ( void )
{
  cout << "Usage:\n";
  cout << "LPDProgress_test ";
  cout << CLIENT_IP_ADDR ;
  cout << " <client IP address or host name>\n";
}

void main (int argc, char** argv )
{

  if (argc < 2)
  {
    cout << "Missing arguments\n";
    Usage();
    exit(1);
  }

  for (int i = 1; i < argc; i++)
  {
    if (strcmp(argv[i],CLIENT_IP_ADDR) == 0)
    {
      i++;
      clientIPAddr_utf8 = argv[i];
    }
    else
    {
      cout << "Ignoring unknown parameter: \"";
      cout << argv[i];
      cout << "\"\n";
    }
  }

  if (clientIPAddr_utf8 == NULL)
  {
    cout << "Missing parameter: ";
    cout << CLIENT_IP_ADDR;
    cout << "\n";
    Usage();
    exit(1);
  }


  bool rc = LPDProgress::JNI_Init();

  cout << "LPDProgress::JNI_Init() rc = ";
  cout << (rc == 0 ? "false" : "true");
  cout << "\n";

  if (rc == false)
  {
    cerr << "Failed to initialize the JVM properly.\n";
    exit(1);
  }

  cout << "Creating an instance of the LPDProgress class ...\n";
  LPDProgress lpdProgress(clientIPAddr_utf8);

  cout << "Displaying the msg box ...\n";
  lpdProgress.DisplayReceivingPrintJobMsgBox();

  cout << "Sleeping for 5 seconds ...\n";
  Sleep(5000);

  cout << "Dismissing the msg box ...\n";
  lpdProgress.DismissReceivingPrintJobMsgBox();

  cout << "Telling LPDProgress we're done ...\n";
  lpdProgress.Done();

  // lpdProgress self destructs when this method exits

  cout << "Calling JNI_Done() ...\n";
  LPDProgress::JNI_Done();
  cout << "Done!\n";
}

