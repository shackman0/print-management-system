package shackelford.floyd.printmanagementsystem.lpdprogress;


import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;

import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.guiserver.GUIServer;


/**
 * <p>Title: LPDProgress</p>
 * <p>Description:
 * this is a hook into LPD to display progress messages on the sender's GUI server. 
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class LPDProgress
{

  public LPDProgress (String clientAddress)
  {

    _clientAddress = clientAddress.trim();
    _guiServer = null;

    try
    {
      System.setSecurityManager(new RMISecurityManager());
      String guiServerURL = "rmi://"+ clientAddress.trim() +"/GUIServer";
      _guiServer = ((GUIServer)Naming.lookup(guiServerURL)).NewGUIServer();
    }
    catch(Exception excp)
    {
      excp.printStackTrace();
    }

    _didDone = false;
  }

  /**
    the finalize method is here just in case the creator forgot to call
    the done() method
  */

  @Override
  protected void finalize()
  {
    try
    {
      Done();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }
  }

  /**
  This operation notifies the servers that you no longer need their resources.
  */
  private void Done()
  {
    if ( (_didDone == false) &&
         (_guiServer != null) )
    {
      _guiServer = null;
    }
  }


  public void DisplayReceivingPrintJobMsgBox()
  {
    if (_guiServer != null)
    {
      try
      {
        _guiServer.DisplayReceivingPrintJobMsgBox();
      }
      catch (RemoteException excp)
      {
        excp.printStackTrace();
      }
    }
  }

  public void DismissReceivingPrintJobMsgBox()
  {
    if (_guiServer != null)
    {
      try
      {
        _guiServer.DismissReceivingPrintJobMsgBox();
      }
      catch (RemoteException excp)
      {
        excp.printStackTrace();
      }
    }
  }

  private GUIServer             _guiServer;
  private boolean               _didDone;
  @SuppressWarnings("unused")
  private String                _clientAddress;


  /**
  the action parm can have the following values:
    0 = test (default)
    1 = DisplayReceivingPrintJobMsgBox
    2 = DismissReceivingPrintJobMsgBox
  */
  public static void main(String[] args)
  {

    GlobalAttributes.__applicationName = LPDProgress.class.getName();
    GlobalAttributes.__mainClass = LPDProgress.class;

    String clientAddress = null;
    int action = 0;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-clientAddr") == true)
      {
        clientAddress = args[++index];
        clientAddress = clientAddress.trim();
      }
      else
      if (args[index].equalsIgnoreCase("-action") == true)
      {
        action = new Integer(args[++index]).intValue();
      }
      else
      {
        System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
      }
    }

    if (clientAddress == null)
    {
      System.out.println("ERROR: -clientAddr was not specified");
      Usage();
      System.exit(1);
    }

    LPDProgress lpdProgress = new LPDProgress(clientAddress);

    switch (action)
    {
      case  0:
              System.out.println("Client Address = \"" + clientAddress + "\"");
              System.out.println("action = \"" + action + "\"");
              try
              {
                String[] bindings = Naming.list(clientAddress);
                System.out.println("Servers currently bound at " + clientAddress);
                for (int i = 0; i < bindings.length; i++)
                {
                  System.out.println("  " + bindings[i]);
                }
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
              }
              System.out.println (
                "Testing DisplayReceivingPrintJobMsgBox now.\n" +
                "This is a non-blocking call." );
              lpdProgress.DisplayReceivingPrintJobMsgBox();

/*
              System.out.println (
                "Testing DismissReceivingPrintJobMsgBox now.\n" +
                "This is a non-blocking call." );
              lpdProgress.DismissReceivingPrintJobMsgBox();
*/
              break;
      case  1:
              lpdProgress.DisplayReceivingPrintJobMsgBox();
              break;
      case  2:
              lpdProgress.DismissReceivingPrintJobMsgBox();
              break;
      default:
    }

    lpdProgress.Done();
    lpdProgress = null;

    System.exit(0);
  }

  private static void Usage ()
  {
    System.err.println (
      "Usage e.g.: java LPDProgress " +
      "-clientAddr <client hostname or ip addr> [-action <action number>]");
  }
}