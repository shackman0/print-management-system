@echo off

rem This script launches the GateKeeper_text.exe program to test JNI.
rem You must modify this script before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\j2sdk1.3.1_01

rem Set the CLIENT variable to the hostname or the IP address
rem of the Client computert. Put the value in between the double
rem quotes.

set CLIENT="localhost"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %CLIENT%. == . goto 99continue

echo ERROR: The CLIENT variable is not set.
echo   The CLIENT variable must be properly set before running this
echo   script.

goto end

:99continue

set OLDPATH=%PATH%
set PATH=%PATH%;%JAVA%\BIN;%JAVA%\JRE\BIN\CLASSIC;
set CLASSPATH=%JAVA%\lib;.\LPDProgress.jar;

.\LPDProgress_test.exe -clientAddr %CLIENT%

:end

set PATH=%OLDPATH%
set OLDPATH=
set CLIENT=
set CLASSPATH=
set JAVA=
