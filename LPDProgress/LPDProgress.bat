@echo off

rem This script launches the LPDProgress java test.
rem You must modify this script before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem Set the CLIENT variable to the hostname or the IP address
rem of the Client computer. Put the value in between the double
rem quotes.

set CLIENT="localhost"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not %JAVA%. == . goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %CLIENT%. == . goto 99continue

echo ERROR: The CLIENT variable is not set.
echo   The CLIENT variable must be properly set before running this
echo   script.

goto end

:99continue

%JAVA%\bin\java -classpath .;%JAVA%\lib;.\LPDProgress.jar; -Djava.security.policy=LPDProgress.policy LPDProgress -clientAddr %CLIENT% %1 %2 %3 %4 %5

:end

set JAVA=
set CLIENT=
