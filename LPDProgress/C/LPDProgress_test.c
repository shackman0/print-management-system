#include "LPDProgress.h"
#include <string.h>
#include <stdlib.h>
#include <windows.h>


static STRING_UTF8 CLIENT_IP_ADDR = "-clientAddr";

static STRING_UTF8 clientIPAddr_utf8 = NULL;


#define NUM_OF_PROPS 7

void Usage ( void )
{
  printf("Usage:\n");
  printf("LPDProgress_test ");
  printf(CLIENT_IP_ADDR);
  printf(" <client IP address or host name>\n");
}

void main (int argc, char** argv )
{

  int i;
  int rc;

  if (argc < 2)
  {
    printf("Missing arguments\n");
    Usage();
    exit(1);
  }

  for (i = 1; i < argc; i++)
  {
    if (strcmp(argv[i],CLIENT_IP_ADDR) == 0)
    {
      i++;
      clientIPAddr_utf8 = argv[i];
    }
    else
    {
      printf("Ignoring unknown parameter: \"");
      printf(argv[i]);
      printf("\"\n");
    }
  }

  if (clientIPAddr_utf8 == NULL)
  {
    printf("Missing parameter: ");
    printf(CLIENT_IP_ADDR);
    printf("\n");
    Usage();
    exit(1);
  }


  rc = JNI_Init();

  printf("LPDProgress::JNI_Init() rc = ");
  printf((rc == 0 ? "false" : "true"));
  printf("\n");

  if (rc == 0)
  {
    printf("Failed to initialize the JVM properly.\n");
    exit(1);
  }

  printf("Creating an instance of the LPDProgress class ...\n");
  LPDProgress(clientIPAddr_utf8);

  printf("Displaying the msg box ...\n");
  DisplayReceivingPrintJobMsgBox();

  printf("Sleeping for 3 seconds ...\n");
  Sleep(3000);

  printf("Dismissing the msg box ...\n");
  DismissReceivingPrintJobMsgBox();

  printf("Telling LPDProgress we're done ...\n");
  Done();

  // lpdProgress self destructs when this method exits

  printf("Calling JNI_Done() ...\n");
  JNI_Done();
  printf("Done!\n");
}

