
// choose on or the other

#define WINDOWS 1
//#define LINUX 1

#ifdef WINDOWS
#include <windows.h>
#endif

#include <stdio.h>
#include <memory.h>
#include "LPDProgress.h"


#define NUMBER_OF_JVM_OPTIONS 2



#ifdef LINUX

#ifndef TRUE
#define TRUE  1
#endif

#ifndef FALSE
#define FALSE 0
#endif

static STRING_UTF8 CLASS_PATH =
  "-Djava.class.path=/usr/local/LPDProgress/LPDProgress.jar;";

static STRING_UTF8 SECURITY_POLICY =
  "-Djava.security.policy=/usr/local/LPDProgress/LPDProgress.policy";

#endif LINUX



#ifdef WINDOWS

static STRING_UTF8 CLASS_PATH =
  "-Djava.class.path=/cvs/PayToPrint/LPDProgress/LPDProgress.jar;";

static STRING_UTF8 SECURITY_POLICY =
  "-Djava.security.policy=/cvs/PayToPrint/LPDProgress/LPDProgress.policy";

#endif WINDOWS


static JavaVM*           __jvm = NULL;
static JNIEnv*           __env = NULL;

static jclass            __lpdProgress_class;
static jmethodID         __lpdProgress_mid;
static jmethodID         __displayReceivingPrintJobMsgBox_mid;
static jmethodID         __dismissReceivingPrintJobMsgBox_mid;
static jmethodID         __done_mid;

static jobject           __lpdProgress_j;
static jstring           __clientAddress_j;


int JNI_Init ( void )
{

  JavaVMInitArgs  jvmArgs;
  JavaVMOption    jvmOptions[NUMBER_OF_JVM_OPTIONS];
  int             rc;

  if (__jvm == NULL)
  {
    memset(&jvmArgs, 0, sizeof(jvmArgs));
    jvmArgs.version = JNI_VERSION_1_2;
    jvmArgs.ignoreUnrecognized = TRUE;

    JNI_GetDefaultJavaVMInitArgs(&jvmArgs);

    jvmOptions[0].optionString = SECURITY_POLICY;
    jvmOptions[1].optionString = CLASS_PATH;

    jvmArgs.nOptions = NUMBER_OF_JVM_OPTIONS;
    jvmArgs.options = jvmOptions;

    rc = JNI_CreateJavaVM(&__jvm, (void**) &__env, &jvmArgs);
    if (rc == JNI_ERR)
    {
      __jvm = NULL;
      printf("Error creating JVM.\n");
      return 0;
    }

    __lpdProgress_class = (*__env)->FindClass(__env,"LPDProgress");

    if (__lpdProgress_class == 0)
    {
      __jvm = NULL;
      printf("Can't find class LPDProgress.\n");
      return 0;
    }

    __lpdProgress_mid =
      (*__env)->GetMethodID (
                  __env,
                  __lpdProgress_class,
                  "<init>",
                  "(Ljava/lang/String;)"      // String clientAddress
                  "V" );                      // return value: void

    if (__lpdProgress_mid == 0)
    {
      __jvm = NULL;
      printf("Can't find method LPDProgress.<init>().\n");
      return 0;
    }

    __displayReceivingPrintJobMsgBox_mid =
      (*__env)->GetMethodID (
                  __env,
                  __lpdProgress_class,
                  "DisplayReceivingPrintJobMsgBox",
                  "()"                        // void
                  "V" );                      // return value: void

    if (__lpdProgress_mid == 0)
    {
      __jvm = NULL;
      printf("Can't find method LPDProgress.DisplayReceivingPrintJobMsgBox().\n");
      return 0;
    }

    __dismissReceivingPrintJobMsgBox_mid =
      (*__env)->GetMethodID (
                  __env,
                  __lpdProgress_class,
                  "DismissReceivingPrintJobMsgBox",
                  "()"                        // void
                  "V" );                      // return value: void

    if (__lpdProgress_mid == 0)
    {
      __jvm = NULL;
      printf("Can't find method LPDProgress.DismissReceivingPrintJobMsgBox().\n");
      return 0;
    }

    __done_mid =
      (*__env)->GetMethodID (
                  __env,
                  __lpdProgress_class,
                  "Done",
                  "()"                        // void
                  "V" );                      // return value: void

    if (__lpdProgress_mid == 0)
    {
      __jvm = NULL;
      printf("Can't find method LPDProgress.Done().\n");
      return 0;
    }

  }

  return 1;
}

void LPDProgress(STRING_UTF8   clientAddress_utf8)
{

  jthrowable exc;  // java exception type

  if (__jvm == NULL)
  {
    return;
  }

  // convert C Strings into Java Strings
  __clientAddress_j = (*__env)->NewStringUTF (__env,clientAddress_utf8);

  // create a lpdProgress object
  __lpdProgress_j = (*__env)->NewObject (
                                __env,
                                __lpdProgress_class,
                                __lpdProgress_mid,
                                __clientAddress_j );
  exc = (*__env)->ExceptionOccurred(__env);

  if (exc)
  {
    __lpdProgress_j = NULL;
  }

  (*__env)->ExceptionDescribe(__env);
  (*__env)->ExceptionClear(__env);

}

void Done ( void )
{
  if (__lpdProgress_j != NULL)
  {
    (*__env)->CallVoidMethod (
                __env,
                __lpdProgress_j,
                __done_mid );
    __lpdProgress_j = NULL;
  }
}

void DisplayReceivingPrintJobMsgBox ( void )
{
  if (__lpdProgress_j != NULL)
  {
    (*__env)->CallVoidMethod (
                __env,
                __lpdProgress_j,
                __displayReceivingPrintJobMsgBox_mid );
  }
}

void DismissReceivingPrintJobMsgBox ( void )
{
  if (__lpdProgress_j != NULL)
  {
    (*__env)->CallVoidMethod (
                __env,
                __lpdProgress_j,
                __dismissReceivingPrintJobMsgBox_mid );
  }
}

void JNI_Done ( void )
{
  if (__jvm != NULL)
  {
    (*__jvm)->DestroyJavaVM(__jvm);
    __jvm = NULL;
  }
}
