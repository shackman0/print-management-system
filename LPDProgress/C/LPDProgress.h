#ifndef _LPDPROGRESS_H_
#define _LPDPROGRESS_H_ 1

#include <stdlib.h>
#include <jni.h>


/*
to use this class:

  1. call JNI_Init() to initialize the JVM environment
  2. call DisplayReceivingPrintJobMsgBox()
  3. call DismissReceivingPrintJobMsgBox()
  4. call JNI_Done() when you are done

You MUST call JNI_Init() before calling anything else to init the JVM.
You MUST call JNI_Done() before you end your program to quiesce the Java Virtual Machine.

*/

typedef char*  STRING_UTF8;



/*
call Init() first to initialize the JVM environment.
pass in the name of the java dll to be used (jdk1.3's dll is called "jvm.dll")
*/
int JNI_Init ( void );

void LPDProgress(STRING_UTF8 clientAddr);

void Done ( void );

void DisplayReceivingPrintJobMsgBox ( void );

void DismissReceivingPrintJobMsgBox ( void );

/*
if you call Init() you must call Done() when you are
ready to shut down your program to quiesce the Java Virtual Machine.
*/
void JNI_Done ( void );

#endif
