#! /bin/bash

#LPDProgress requires the following parms:
#
#   -clientAddr <client address>
#   -action <action number>
#
# where <action number> is
#   1 for display job being received message
#   2 for dismiss job being received message

# This script launches the LPDProgress java script
# You must modify this script before this script will work.

# Set the JDKTOP variable to point to where the Java Virtual Machine
# and supporting files are located. Use an absolute path.

INSTALLDIR=/usr/local/LPDProgress
JDKTOP=/usr/local/java2/
JDKLIB=${JDKTOP}/lib
CLASSPATH=${INSTALLDIR}/LPDProgress.jar:${JDKLIB}:

# echo $* 2>&1 1>/var/log/LPDProgress.log

${JDKTOP}/bin/java -classpath ${CLASSPATH} -Djava.security.policy=${INSTALLDIR}/LPDProgress.policy LPDProgress $*

