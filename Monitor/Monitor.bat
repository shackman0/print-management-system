@echo off

rem This script launches the Installations application.
rem You must modify this script when you install the
rem Installations application before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem Set the SERVER variable to the hostname or the IP address
rem of the Monitor Server. Put the value in between the double
rem quotes.

set SERVER="208.46.68.101"

rem Set the USERID variable to the user id to use to log on to
rem the Monitor database.

set USERID="CMS"

rem Set the PASSWORD variable to the Password for the user id being
rem used to log on to the Monitor database.

set PASSWORD="CMSPASSWD"

rem Set the RESOURCES variable to the directory in which the
rem language-specific string files are stored

set RESOURCES=".\Resources"

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not "%JAVA%". == "". goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %SERVER%. == . goto 2continue

echo ERROR: The SERVER variable is not set.
echo   The SERVER variable must be properly set before running this
echo   script.

goto end

:2continue

if not %USERID%. == . goto 3continue

echo ERROR: The USERID variable is not set.
echo   The USERID variable must be properly set before running this
echo   script.

goto end

:3continue

if not %PASSWORD%. == . goto 4continue

echo ERROR: The PASSWORD variable is not set.
echo   The PASSWORD variable must be properly set before running this
echo   script.

goto end

:4continue

if not %RESOURCES%. == . goto 98continue

echo ERROR: The RESOURCES variable is not set.
echo   The RESOURCES variable must be properly set before running this
echo   script.

goto end

:98continue

rem clean up any upgrade relics
if exist ..\Installations.zip del /f /q ..\Installations.zip >nul

rem enable the new upgrade .jar file if one exists
if not exist .\Installations.jar goto 99continue
if exist .\InstallationsRun.jar del /f /q .\InstallationsRun.jar >nul
ren .\Installations.jar InstallationsRun.jar

:99continue

rem launch javaw in the background so we don't see a dos window
start "Installations" /i /b "%JAVA%\bin\javaw.exe" -classpath .;"%JAVA%\lib";.\InstallationsRun.jar; -Djdbc.drivers=org.postgresql.Driver Installations -dbaddr %SERVER% -dbuser %USERID% -dbpasswd %PASSWORD% -resDir %RESOURCES% %1 %2 %3 %4 %5 %6 %7 %8 %9

:end

set JAVA=
set SERVER=
set USERID=
set PASSWORD=
set RESOURCES=

