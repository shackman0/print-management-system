package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JDialog;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;
import javax.swing.SwingWorker;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.AbstractList4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Row;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL4Row;
import shackelford.floyd.printmanagementsystem.common.ActionMenuItem;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.MyJOptionPane;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.monitordatabase.InstallationsRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorTable;
import shackelford.floyd.printmanagementsystem.monitordatabase.ReceivablesStatistics;


/**
 * <p>Title: MonitorMainPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorMainPanel
  extends AbstractList4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID                        = 3410774798016967107L;

  @SuppressWarnings("unused")
  private JMenuItem           _fileReceivablesMenuItem;

  private JMenuItem           _fileOpenRemoteMenuItem;

  @SuppressWarnings("unused")
  private JMenuItem           _fileCurrencyMenuItem;

  @SuppressWarnings("unused")
  private JMenuItem           _fileChangePassphraseMenuItem;

  private JMenuItem           _editGenerateReceivablesMenuItem;

  private JMenuItem           _fileOpenRemoteFloatingMenuItem;

  @SuppressWarnings("unused")
  private JMenuItem           _fileReceivablesFloatingMenuItem;

  @SuppressWarnings("unused")
  private JMenuItem           _fileCurrencyFloatingMenuItem;

  @SuppressWarnings("unused")
  private JMenuItem           _fileChangePassphraseFloatingMenuItem;

  private JMenuItem           _editGenerateReceivablesFloatingMenuItem;

  private static Resources    __resources;

  private static final String FILE_RECEIVABLES                        = "FILE_RECEIVABLES";

  private static final String FILE_RECEIVABLES_TIP                    = "FILE_RECEIVABLES_TIP";

  private static final String FILE_CURRENCY                           = "FILE_CURRENCY";

  private static final String FILE_CURRENCY_TIP                       = "FILE_CURRENCY_TIP";

  private static final String FILE_OPEN_REMOTE                        = "FILE_OPEN_REMOTE";

  private static final String FILE_OPEN_REMOTE_TIP                    = "FILE_OPEN_REMOTE_TIP";

  private static final String FILE_CHANGE_PASSPHRASE                  = "FILE_CHANGE_PASSPHRASE";

  private static final String FILE_CHANGE_PASSPHRASE_TIP              = "FILE_CHANGE_PASSPHRASE_TIP";

  private static final String EDIT_GENERATE_RECEIVABLES               = "EDIT_GENERATE_RECEIVABLES";

  private static final String EDIT_GENERATE_RECEIVABLES_TIP           = "EDIT_GENERATE_RECEIVABLES_TIP";

  private static final String RECEIVABLES_GENERATED_MSG               = "RECEIVABLES_GENERATED_MSG";

  private static final String RECEIVABLES_GENERATED_TITLE             = "RECEIVABLES_GENERATED_TITLE";

  private static final String REMOTE_INSTALLATION_ROW_NOT_FOUND_MSG   = "REMOTE_INSTALLATION_ROW_NOT_FOUND_MSG";

  private static final String REMOTE_INSTALLATION_ROW_NOT_FOUND_TITLE = "REMOTE_INSTALLATION_ROW_NOT_FOUND_TITLE";

  public MonitorMainPanel()
  {
    super();
    Initialize(MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable(), MonitorLocalEntryPanel.class);
    _table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
  }

  @Override
  protected void AddFileMenuItems(JMenu fileMenu)
  {
    super.AddFileMenuItems(fileMenu);

    _fileOpenRemoteMenuItem = fileMenu.add(new ActionMenuItem(new FileOpenRemoteAction(this)));
    _fileOpenRemoteMenuItem.setEnabled(false);
    fileMenu.addSeparator();

    _fileCurrencyMenuItem = fileMenu.add(new ActionMenuItem(new FileCurrencyAction(this)));
    _fileReceivablesMenuItem = fileMenu.add(new ActionMenuItem(new FileReceivablesAction(this)));
    fileMenu.addSeparator();

    _fileChangePassphraseMenuItem = fileMenu.add(new ActionMenuItem(new FileChangePassphraseAction(this)));
    fileMenu.addSeparator();
  }

  @Override
  protected void AddFloatingFileMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingFileMenuItems(popupMenu);

    _fileCurrencyFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileCurrencyAction(this)));
    _fileReceivablesFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileReceivablesAction(this)));
    popupMenu.addSeparator();

    _fileChangePassphraseFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileChangePassphraseAction(this)));
    popupMenu.addSeparator();
  }

  @Override
  protected void AddEditMenuItems(JMenu editMenu)
  {
    super.AddEditMenuItems(editMenu);

    _editGenerateReceivablesMenuItem = editMenu.add(new ActionMenuItem(new EditGenerateReceivablesAction(this)));
    _editGenerateReceivablesMenuItem.setEnabled(false);
  }

  @Override
  protected void AddFloatingEditMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingEditMenuItems(popupMenu);
    _fileOpenRemoteFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileOpenRemoteAction(this)));
    _fileOpenRemoteFloatingMenuItem.setEnabled(false);
    popupMenu.addSeparator();
    _editGenerateReceivablesFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditGenerateReceivablesAction(this)));
    _editGenerateReceivablesFloatingMenuItem.setEnabled(false);
  }

  @Override
  protected void EnableMenuItems(boolean enabledFlag)
  {
    super.EnableMenuItems(enabledFlag);
    _fileOpenRemoteMenuItem.setEnabled(enabledFlag);
    _fileOpenRemoteFloatingMenuItem.setEnabled(enabledFlag);
    _editGenerateReceivablesMenuItem.setEnabled(enabledFlag);
    _editGenerateReceivablesFloatingMenuItem.setEnabled(enabledFlag);
  }

  @Override
  protected String GetOrderBy()
  {
    return InstallationsRow.Name_installation_id();
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  protected void DisablingRow(AbstractSQL4Row sqlRow)
  {
    super.DisablingRow(sqlRow);

    InstallationsRow installationsRow = (InstallationsRow) sqlRow;
    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
        installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
      InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();
      installationRow.Set_status(AbstractSQL4Row.STATUS_DISABLED_NAME);
      installationRow.Update();
      InstallationDatabase.GetDefaultInstallationDatabase().Done();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
    }
  }

  @Override
  protected void EnablingRow(AbstractSQL4Row sqlRow)
  {
    super.EnablingRow(sqlRow);

    InstallationsRow installationsRow = (InstallationsRow) sqlRow;
    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
        installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
      InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();
      installationRow.Set_status(AbstractSQL4Row.STATUS_ENABLED_NAME);
      installationRow.Update();
      InstallationDatabase.GetDefaultInstallationDatabase().Done();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
    }
  }

  @Override
  protected void DeletingRow(AbstractSQL1Row sqlRow)
  {
    super.DeletingRow(sqlRow);

    InstallationsRow installationsRow = (InstallationsRow) sqlRow;
    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
        installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
      InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();
      installationRow.Remove();
      InstallationDatabase.GetDefaultInstallationDatabase().Done();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
    }
  }

  private class FileOpenRemoteAction
    extends AbstractAction
  {

    /**
     * Eclipse generated value
     */
    private static final long      serialVersionUID = -7868868501868951017L;

    private final MonitorMainPanel _mainPanel;

    public FileOpenRemoteAction(MonitorMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_OPEN_REMOTE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_OPEN_REMOTE_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
      InstallationsRow installationsRow = (InstallationsRow) sqlRows[0];
      try
      {
        InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(installationsRow.Get_installation_server_address(), installationsRow.Get_installation_server_user_id(),
          installationsRow.Get_installation_server_password(), installationsRow.Get_installation_id()));
        InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
      }

      InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();
      // if the installation row has been deleted somehow (like maybe we needed to reinstall
      // the installation), this gives us the ability to reinitialize the remote installation
      // row without having to create a new local installation row.
      if (installationRow == null)
      {
        MyJOptionPane optionPane = new MyJOptionPane(GetResources().getProperty(REMOTE_INSTALLATION_ROW_NOT_FOUND_MSG), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
        JDialog dialog = optionPane.createDialog(_mainPanel, GetResources().getProperty(REMOTE_INSTALLATION_ROW_NOT_FOUND_TITLE));
        dialog.setVisible(true);
        Object selection = optionPane.getValue();
        if ((selection == null) || ((selection instanceof Integer) == false) || (((Integer) selection).intValue() == JOptionPane.NO_OPTION))
        {
          return;
        }
      }

      MonitorRemoteEntryPanel entryPanel = new MonitorRemoteEntryPanel(_mainPanel, InstallationDatabase.GetDefaultInstallationDatabase().GetInstallationTable(), installationRow, installationsRow);
      entryPanel.AddListPanelListener(_mainPanel);
      entryPanel.setVisible(true);

      _mainPanel.invalidate();
    }

  }

  private class FileReceivablesAction
    extends AbstractAction
  {

    /**
     * Eclipse generated value
     */
    private static final long      serialVersionUID = 9148170097377992958L;

    private final MonitorMainPanel _mainPanel;

    public FileReceivablesAction(MonitorMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_RECEIVABLES));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_RECEIVABLES_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      new MonitorReceivablesPanel().setVisible(true);
      _mainPanel.invalidate();
    }

  }

  private class FileCurrencyAction
    extends AbstractAction
  {

    /**
     * Eclipse generated value
     */
    private static final long      serialVersionUID = -4510122264106258831L;

    private final MonitorMainPanel _mainPanel;

    public FileCurrencyAction(MonitorMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_CURRENCY));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_CURRENCY_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      new MonitorCurrenciesPanel().setVisible(true);
      _mainPanel.invalidate();
    }

  }

  private class FileChangePassphraseAction
    extends AbstractAction
  {

    /**
     * Eclipse generated value
     */
    private static final long      serialVersionUID = -9048750328718329662L;

    private final MonitorMainPanel _mainPanel;

    public FileChangePassphraseAction(MonitorMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_CHANGE_PASSPHRASE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_CHANGE_PASSPHRASE_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      MonitorRow monitorRow = MonitorTable.GetCachedMonitorRow();
      MonitorChangePassphraseDialog dialog = new MonitorChangePassphraseDialog(monitorRow);
      Properties props = dialog.WaitOnDialog();
      boolean okSelected = new Boolean(props.getProperty(AbstractDialog.OK)).booleanValue();
      if (okSelected == true)
      {
        String newPassphrase = props.getProperty(MonitorChangePassphraseDialog.NEW_MONITOR_PASSPHRASE);
        monitorRow.Set_passphrase(newPassphrase);
        monitorRow.Update();
      }
      _mainPanel.invalidate();
    }

  }

  private class EditGenerateReceivablesAction
    extends AbstractAction
  {

    /**
     * Eclipse generated value
     */
    private static final long      serialVersionUID = 7321065610085376621L;

    private final MonitorMainPanel _mainPanel;

    public EditGenerateReceivablesAction(MonitorMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_GENERATE_RECEIVABLES));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_GENERATE_RECEIVABLES_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      // display start date and end date dialog
      Properties dialogProperties = null;
      MonitorStartAndEndDatesDialog dialog = new MonitorStartAndEndDatesDialog();
      dialogProperties = dialog.WaitOnDialog();

      if (new Boolean(dialogProperties.getProperty(AbstractDialog.CANCEL)).booleanValue() == true)
      {
        return;
      }

      final _GregorianCalendar startCal = new _GregorianCalendar(dialogProperties.getProperty(MonitorStartAndEndDatesDialog.START_DATE));
      final _GregorianCalendar endCal = new _GregorianCalendar(dialogProperties.getProperty(MonitorStartAndEndDatesDialog.END_DATE));

      final AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();

      new SwingWorker<Object, Object>()
        {
          @Override
          public Object doInBackground()
          {
            for (int indx = 0; indx < sqlRows.length; ++indx)
            {
              InstallationsRow installationsRow = (InstallationsRow) sqlRows[indx];
              ReceivablesStatistics stats = MonitorDatabase.GetDefaultMonitorDatabase().GetReceivablesTable().GenerateReceivablesForInstallation(installationsRow, startCal, endCal);

              MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(RECEIVABLES_GENERATED_MSG));
              Object[] substitutionValues = new Object[] { installationsRow.Get_installation_id().toString(), new Double(stats._totalPerPageCharges), new Double(stats._totalPerPrintJobCharges),
                new Double(stats._totalPercentageSalesCharges), startCal.GetDateString(), endCal.GetDateString() };
              String msgString = msgFormat.format(substitutionValues).trim();

              MsgBox msgBox = new MsgBox(msgString, GetResources().getProperty(RECEIVABLES_GENERATED_TITLE));
              msgBox.setVisible(true);
            }
            return null;
          }
        }.execute();
      _mainPanel.invalidate();
    }

  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorMainPanel.class.getName());
  }

}
