package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.EventQueue;
import java.awt.Frame;
import java.awt.Toolkit;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MultiLineToolTipUI;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.SplashPanel;
import shackelford.floyd.printmanagementsystem.common.WaitCursorEventQueue;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorTable;


/**
 * <p>Title: Monitor</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Monitor
  implements PanelListener
{

  private static int          __fontSizeAdjustment             = 0;

  public static final int     MAJOR_VERSION                    = 1;

  public static final int     MINOR_VERSION                    = 0;

  public static final int     FIX_LEVEL                        = 0;

  private static final int    WAIT_CURSOR_DELAY                = 500;                                      // milliseconds

  private static final String METAL_LOOK_AND_FEEL              = "javax.swing.plaf.metal.MetalLookAndFeel";

  public static final int     EXIT_OK                          = 0;

  public static final int     EXIT_LOGIN_CANCELLED             = 1;

  public static final int     EXIT_ERROR_MISSING_RESDIR        = -2;

  public static final int     EXIT_ERROR_MISSING_DBADDR        = -3;

  public static final int     EXIT_ERROR_MISSING_DBUSERID      = -4;

  public static final int     EXIT_ERROR_MISSING_DBPASSWD      = -5;

  public static final int     EXIT_ERROR_DATABASE_CONNECT      = -6;

  public static final int     EXIT_ERROR_MONITOR_ROW_NOT_FOUND = -7;

  private static final String NEW_LINE                         = "\n";

  private static final String DOUBLE_QUOTE                     = "\"";

  @Override
  public void EndOfPanel(char action)
  {
    // launch this application again ...
    //    SystemUtilities.LaunchApplication(GlobalAttributes.__applicationName);
    // ... because we're getting out of here
    System.exit(EXIT_OK);
  }

  @Override
  public void PanelStateChange(char action)
  {
    // do nothing
  }

  public static void main(String[] args)
    throws Exception
  {
    GlobalAttributes.SetMajorVersion(MAJOR_VERSION);
    GlobalAttributes.SetMinorVersion(MINOR_VERSION);
    GlobalAttributes.SetFixLevel(FIX_LEVEL);

    GlobalAttributes.__applicationName = Monitor.class.getName();
    GlobalAttributes.__mainClass = Monitor.class;

    MultiLineToolTipUI.initialize();
    MultiLineToolTipUI.setDisplayAcceleratorKey(false);

    String splashComments = NEW_LINE + NEW_LINE;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-verbose") == true)
      {
        GlobalAttributes.__verbose = true;
        splashComments += "Running verbose" + NEW_LINE;
      }
      else
        if (args[index].equalsIgnoreCase("-dbaddr") == true)
        {
          GlobalAttributes.__dbAddr = args[++index];
          splashComments += "Database Address = " + DOUBLE_QUOTE + GlobalAttributes.__dbAddr + DOUBLE_QUOTE + NEW_LINE;
        }
        else
          if (args[index].equalsIgnoreCase("-dbuser") == true)
          {
            GlobalAttributes.__dbUserID = args[++index];
            splashComments += "Database User ID = " + DOUBLE_QUOTE + GlobalAttributes.__dbUserID + DOUBLE_QUOTE + NEW_LINE;
          }
          else
            if (args[index].equalsIgnoreCase("-dbpasswd") == true)
            {
              GlobalAttributes.__dbPasswd = args[++index];
              splashComments += "Database Password found" + NEW_LINE;
            }
            else
              if (args[index].equalsIgnoreCase("-resDir") == true)
              {
                GlobalAttributes.__resourcesDirectory = args[++index];
                splashComments += "Language files are located under directory " + DOUBLE_QUOTE + GlobalAttributes.__resourcesDirectory + DOUBLE_QUOTE + NEW_LINE;
              }
              else
                if (args[index].equalsIgnoreCase("-fontSizeAdj") == true)
                {
                  __fontSizeAdjustment = new Integer(args[++index]).intValue();
                  splashComments += "Adjusting font by " + __fontSizeAdjustment + NEW_LINE;
                }
                else
                {
                  splashComments += "Ignoring unknown parameter: " + DOUBLE_QUOTE + args[index] + DOUBLE_QUOTE + NEW_LINE;
                }
    }

    if (GlobalAttributes.__dbAddr == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbAddr was not specified\n" + Usage(), "ERROR: -dbAddr was not specified");
      System.exit(EXIT_ERROR_MISSING_DBADDR);
    }

    if (GlobalAttributes.__dbUserID == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbUser was not specified\n" + Usage(), "ERROR: -dbUser was not specified");
      System.exit(EXIT_ERROR_MISSING_DBUSERID);
    }

    if (GlobalAttributes.__dbPasswd == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbPasswd was not specified\n" + Usage(), "ERROR: -dbPasswd was not specified");
      System.exit(EXIT_ERROR_MISSING_DBPASSWD);
    }

    if (GlobalAttributes.__resourcesDirectory == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -resDir was not specified\n" + Usage(), "ERROR: -resDir was not specified");
      System.exit(EXIT_ERROR_MISSING_RESDIR);
    }

    // set up the wait cursor to be displayed whenever the user has to wait for a response
    EventQueue waitQueue = new WaitCursorEventQueue(WAIT_CURSOR_DELAY); // milliseconds
    Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitQueue);

    // adjust the font size if specified
    if (__fontSizeAdjustment != 0)
    {
      UIManager.setLookAndFeel(METAL_LOOK_AND_FEEL);
      SwingUtilities.updateComponentTreeUI(new JFrame());

      UIDefaults uiDefaults = UIManager.getLookAndFeelDefaults();
      for (Enumeration<Object> enumerator = uiDefaults.keys(); enumerator.hasMoreElements();)
      {
        Object key = enumerator.nextElement();
        if (key.toString().endsWith(".font") == true)
        {
          FontUIResource currentFontResource = (FontUIResource) (uiDefaults.get(key));
          FontUIResource newFontResource = new FontUIResource(currentFontResource.getName(), currentFontResource.getStyle(), currentFontResource.getSize() + __fontSizeAdjustment);
          uiDefaults.put(key, newFontResource);
        }
      }
    }

    SplashPanel splashPanel = new SplashPanel(splashComments);
    splashPanel.setVisible(true);

    try
    {
      MonitorDatabase.SetDefaultMonitorDatabase(new MonitorDatabase(GlobalAttributes.__dbAddr, GlobalAttributes.__dbUserID, GlobalAttributes.__dbPasswd));
      MonitorDatabase.GetDefaultMonitorDatabase().ConnectDatabase();
    }
    catch (SQLException excp)
    {
      excp.printStackTrace();
      MessageDialog.ShowErrorMessageDialog(null, excp, "Cannot Connect to Monitor Database");
      System.exit(EXIT_ERROR_DATABASE_CONNECT);
    }

    if (MonitorTable.GetCachedMonitorRow() == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: monitor row not found", "Monitor Not Found");
      System.exit(EXIT_ERROR_MONITOR_ROW_NOT_FOUND);
    }

    if (DoLogin(splashPanel) == true)
    {
      MonitorMainPanel panel = new MonitorMainPanel();
      panel.AddPanelListener(new Monitor());
      splashPanel.dispose();
      panel.setVisible(true);
      panel.toFront();
    }
    else
    {
      System.exit(EXIT_LOGIN_CANCELLED);
    }
  }

  private static String Usage()
  {
    return "Usage:" + NEW_LINE + "java" + NEW_LINE + "  -Djdbc.drivers=org.postgresql.Driver" + NEW_LINE + "  Monitor" + NEW_LINE + "  -dbaddr <database hostname or ip addr>" + NEW_LINE
      + "  -dbuser <database user id>" + NEW_LINE + "  -dbpasswd <database passwd>" + NEW_LINE + "  -resDir <resources directory>" + NEW_LINE + "  [-fontSizeAdj <[-]font size adj>] [-verbose]";
  }

  private static boolean DoLogin(Frame owner)
  {
    boolean loginOK;
    Properties dialogProperties = null;
    MonitorLoginDialog dialog = new MonitorLoginDialog(owner);
    dialogProperties = dialog.WaitOnDialog();
    loginOK = new Boolean(dialogProperties.getProperty(AbstractDialog.OK)).booleanValue();
    return loginOK;
  }

} // end of Monitor
