package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry3Panel;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.monitordatabase.CurrenciesRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.CurrenciesTable;


/**
 * <p>Title: MonitorCurrenciesEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorCurrenciesEntryPanel
  extends AbstractEntry3Panel
{

  /**
   * Eclipse generated value
   */
  private static final long           serialVersionUID             = -2185838643542631349L;

  private final JLabel                _currenciesIDLabel           = new JLabel();

  private final OverwritableTextField _currencyNameTextField       = new OverwritableTextField();

  private final OverwritableTextField _currencySymbolTextField     = new OverwritableTextField();

  private final NumericTextField      _currencyMultiplierTextField = new NumericTextField();

  private static DecimalFormat        __formatter                  = new DecimalFormat("#,##0.000");

  private static Resources            __resources;

  private static final String         CURRENCY_ID                  = "CURRENCY_ID";

  private static final String         CURRENCY_MULTIPLIER          = "CURRENCY_MULTIPLIER";

  private static final String         CURRENCY_MULTIPLIER_TIP      = "CURRENCY_MULTIPLIER_TIP";

  private static final String         CURRENCY_NAME                = "CURRENCY_NAME";

  private static final String         CURRENCY_NAME_TIP            = "CURRENCY_NAME_TIP";

  private static final String         CURRENCY_SYMBOL              = "CURRENCY_SYMBOL";

  private static final String         CURRENCY_SYMBOL_TIP          = "CURRENCY_SYMBOL_TIP";

  //  private static final String     SYMBOL = "SYMBOL";
  private static final String         INVALID_MULTIPLIER_MSG       = "INVALID_MULTIPLIER_MSG";

  private static final String         INVALID_MULTIPLIER_TITLE     = "INVALID_MULTIPLIER_TITLE";

  private static final String         BLANK_CURRENCY_SYMBOL_MSG    = "BLANK_CURRENCY_SYMBOL_MSG";

  private static final String         BLANK_CURRENCY_SYMBOL_TITLE  = "BLANK_CURRENCY_SYMBOL_TITLE";

  private static final String         BLANK_CURRENCY_NAME_MSG      = "BLANK_CURRENCY_NAME_MSG";

  private static final String         BLANK_CURRENCY_NAME_TITLE    = "BLANK_CURRENCY_NAME_TITLE";

  private static final int            CURRENCY_FIELD_WIDTH         = 10;

  public MonitorCurrenciesEntryPanel()
  {
    super();
  }

  public MonitorCurrenciesEntryPanel(ListPanelCursor listPanel, CurrenciesTable currenciesTable, CurrenciesRow currenciesRow)
  {
    super();
    Initialize(listPanel, currenciesTable, currenciesRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      GetCurrenciesRow().SetIDToNext();
    }
  }

  @Override
  protected Container CreateCenterPanel()
  {

    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // currency id
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currenciesIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_currenciesIDLabel, constraints);

    // currency name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY_NAME), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currencyNameTextField.setColumns(20);
    _currencyNameTextField.setToolTipText(__resources.getProperty(CURRENCY_NAME_TIP));
    centerPanel.add(_currencyNameTextField, constraints);

    // currency symbol
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY_SYMBOL), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currencySymbolTextField.setColumns(4);
    _currencySymbolTextField.setToolTipText(__resources.getProperty(CURRENCY_SYMBOL_TIP));
    centerPanel.add(_currencySymbolTextField, constraints);

    // multiplier
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY_MULTIPLIER), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _currencyMultiplierTextField.SetFormat(__formatter);
    _currencyMultiplierTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _currencyMultiplierTextField.setToolTipText(__resources.getProperty(CURRENCY_MULTIPLIER_TIP));

    centerPanel.add(_currencyMultiplierTextField, constraints);

    return centerPanel;
  }

  @Override
  protected boolean FieldsValid()
  {
    String currencyName = _currencyNameTextField.getText().trim();
    if (currencyName.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_CURRENCY_NAME_MSG), __resources.getProperty(BLANK_CURRENCY_NAME_TITLE));
      _currencyNameTextField.requestFocus();
      return false;
    }

    String currencySymbol = _currencySymbolTextField.getText().trim();
    if (currencySymbol.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_CURRENCY_SYMBOL_MSG), __resources.getProperty(BLANK_CURRENCY_SYMBOL_TITLE));
      _currencyNameTextField.requestFocus();
      return false;
    }

    double multiplier = -1d;
    try
    {
      _currencyMultiplierTextField.GetDoubleValueWithException().doubleValue();
    }
    catch (ParseException excp)
    {
      // do nothing
    }

    if (multiplier <= 0d)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_MULTIPLIER_MSG), __resources.getProperty(INVALID_MULTIPLIER_TITLE));
      _currencyMultiplierTextField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _currenciesIDLabel.setText(GetCurrenciesRow().Get_id().toString());
    _currencyNameTextField.setText(GetCurrenciesRow().Get_name());
    _currencySymbolTextField.setText(GetCurrenciesRow().Get_symbol());
    _currencyMultiplierTextField.SetValue(GetCurrenciesRow().Get_multiplier());
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetCurrenciesRow().Set_name(_currencyNameTextField.getText());
    GetCurrenciesRow().Set_symbol(_currencySymbolTextField.getText());
    GetCurrenciesRow().Set_multiplier(_currencyMultiplierTextField.GetDoubleValue().doubleValue());
  }

  protected CurrenciesRow GetCurrenciesRow()
  {
    return (CurrenciesRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorCurrenciesEntryPanel.class.getName());
  }

}
