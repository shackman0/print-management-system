package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.ActionCheckBox;
import shackelford.floyd.printmanagementsystem.common.DateChooserListener;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.ListPanelListener;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.monitordatabase.CurrenciesRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.InstallationsRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorTable;


/**
 * <p>Title: MonitorLocalEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorLocalEntryPanel
  extends AbstractEntry4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long            serialVersionUID                      = 2295003020593849303L;

  private static final int             NUMBER_OF_CURRENCIES                  = 6;

  private ArrayList<ListPanelListener> _saveListPanelListeners               = null;

  private ArrayList<CurrenciesRow>     _currenciesArrayList                  = null;

  final JLabel[]                       _currencySymbolLabels                 = new JLabel[NUMBER_OF_CURRENCIES];

  private final JLabel                 _installationIDLabel                  = new JLabel();

  private final JLabel                 _installationNameLabel                = new JLabel();

  private final JLabel                 _lastReceivablesGeneratedDTSLabel     = new JLabel();

  private final _JComboBox             _countryComboBox                      = new _JComboBox();

  private final _JComboBox             _currencyComboBox                     = new _JComboBox();

  private final OverwritableTextField  _installationServerAddressTextField   = new OverwritableTextField();

  private final OverwritableTextField  _installationServerPasswordTextField  = new OverwritableTextField();

  private final OverwritableTextField  _installationServerUserIDTextField    = new OverwritableTextField();

  ActionCheckBox                       _upFrontFeeCheckBox                   = null;

  final NumericTextField               _upFrontFeeTextField                  = new NumericTextField();

  ActionCheckBox                       _monthlyFeeCheckBox                   = null;

  final NumericTextField               _monthlyFeeTextField                  = new NumericTextField();

  ActionCheckBox                       _annualFeeCheckBox                    = null;

  final NumericTextField               _annualFeeTextField                   = new NumericTextField();

  ActionCheckBox                       _perPageChargeCheckBox                = null;

  final NumericTextField               _perPageChargeTextField               = new NumericTextField();

  ActionCheckBox                       _perPrintJobChargeCheckBox            = null;

  final NumericTextField               _perPrintJobChargeTextField           = new NumericTextField();

  ActionCheckBox                       _totalSalesPercentageCheckBox;

  final NumericTextField               _totalSalesPercentageTextField        = new NumericTextField();

  private final JSpinner               _expirationDateIncrementSpinField     = new JSpinner();

  private final _GregorianCalendar     _dateCalendar                         = _GregorianCalendar.Now();

  private final JLabel                 _dateLabel                            = new JLabel();

  private final JButton                _dateChooserButton                    = new JButton();

  static Resources                     __resources;

  private static final String          ANNUAL_FEE                            = "ANNUAL_FEE";

  private static final String          ANNUAL_FEE_TIP                        = "ANNUAL_FEE_TIP";

  private static final String          ANNUAL_FEE_TEXT_TIP                   = "ANNUAL_FEE_TEXT_TIP";

  private static final String          COUNTRY                               = "COUNTRY";

  private static final String          CURRENCY                              = "CURRENCY";

  private static final String          EXPIRATION_DATE_INCREMENT             = "EXPIRATION_DATE_INCREMENT";

  private static final String          EXPIRATION_DATE_INCREMENT_TIP         = "EXPIRATION_DATE_INCREMENT_TIP";

  private static final String          INSTALLATION_SERVER_ADDRESS           = "INSTALLATION_SERVER_ADDRESS";

  private static final String          INSTALLATION_SERVER_ADDRESS_TEXT_TIP  = "INSTALLATION_SERVER_ADDRESS_TEXT_TIP";

  private static final String          INSTALLATION_SERVER_PASSWORD          = "INSTALLATION_SERVER_PASSWORD";

  private static final String          INSTALLATION_SERVER_PASSWORD_TEXT_TIP = "INSTALLATION_SERVER_PASSWORD_TEXT_TIP";

  private static final String          INSTALLATION_SERVER_USER_ID           = "INSTALLATION_SERVER_USER_ID";

  private static final String          INSTALLATION_SERVER_USER_ID_TEXT_TIP  = "INSTALLATION_SERVER_USER_ID_TEXT_TIP";

  private static final String          INSTALLATION_ID                       = "INSTALLATION_ID";

  private static final String          INSTALLATION_NAME                     = "INSTALLATION_NAME";

  private static final String          LAST_RECEIVABLES_GENERATED_DTS        = "LAST_RECEIVABLES_GENERATED_DTS";

  private static final String          LICENSE_AGREEMENTS                    = "LICENSE_AGREEMENTS";

  private static final String          LICENSE_EXPIRATION_DATE               = "LICENSE_EXPIRATION_DATE";

  private static final String          LICENSE_EXPIRATION_DATE_TIP           = "LICENSE_EXPIRATION_DATE_TIP";

  private static final String          MONTHLY_FEE                           = "MONTHLY_FEE";

  private static final String          MONTHLY_FEE_TIP                       = "MONTHLY_FEE_TIP";

  private static final String          MONTHLY_FEE_TEXT_TIP                  = "MONTHLY_FEE_TEXT_TIP";

  private static final String          PER_PAGE_CHARGE                       = "PER_PAGE_CHARGE";

  private static final String          PER_PAGE_CHARGE_TIP                   = "PER_PAGE_CHARGE_TIP";

  private static final String          PER_PAGE_CHARGE_TEXT_TIP              = "PER_PAGE_CHARGE_TEXT_TIP";

  private static final String          PER_PRINT_JOB_CHARGE                  = "PER_PRINT_JOB_CHARGE";

  private static final String          PER_PRINT_JOB_CHARGE_TIP              = "PER_PRINT_JOB_CHARGE_TIP";

  private static final String          PER_PRINT_JOB_CHARGE_TEXT_TIP         = "PER_PRINT_JOB_CHARGE_TEXT_TIP";

  private static final String          SHARED_REVENUE_AGREEMENTS             = "SHARED_REVENUE_AGREEMENTS";

  private static final String          TOTAL_SALES_PERCENTAGE                = "TOTAL_SALES_PERCENTAGE";

  private static final String          TOTAL_SALES_PERCENTAGE_TIP            = "TOTAL_SALES_PERCENTAGE_TIP";

  private static final String          TOTAL_SALES_PERCENTAGE_TEXT_TIP       = "TOTAL_SALES_PERCENTAGE_TEXT_TIP";

  private static final String          UP_FRONT_FEE                          = "UP_FRONT_FEE";

  private static final String          UP_FRONT_FEE_TIP                      = "UP_FRONT_FEE_TIP";

  private static final String          UP_FRONT_FEE_TEXT_TIP                 = "UP_FRONT_FEE_TEXT_TIP";

  private static final int             CURRENCY_COLUMNS                      = 10;

  private static final int             PERCENTAGE_COLUMNS                    = 10;

  private static final int             IP_ADDRESS_COLUMNS                    = 15;

  private static final int             USER_ID_COLUMNS                       = 8;

  private static final int             PASSPHRASE_COLUMNS                    = 8;

  private static final DecimalFormat   DECIMAL_FORMAT_PERCENT                = new DecimalFormat("##0.00");

  private static final DecimalFormat   DECIMAL_FORMAT_BIG_MONEY              = new DecimalFormat("###,##0.00");

  private static final DecimalFormat   DECIMAL_FORMAT_SMALL_MONEY            = new DecimalFormat("#,##0.000");

  public MonitorLocalEntryPanel()
  {
    super();
  }

  public MonitorLocalEntryPanel(ListPanelCursor listPanel, MonitorTable installationsTable, MonitorRow installationsRow)
  {
    super();
    Initialize(listPanel, installationsTable, installationsRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      GetInstallationsRow().SetNewInstallationID();
    }
  }

  @SuppressWarnings("unused")
  @Override
  protected Container CreateCenterPanel()
  {
    for (int i = 0; i < _currencySymbolLabels.length; ++i)
    {
      _currencySymbolLabels[i] = new JLabel();
      _currencySymbolLabels[i].setHorizontalAlignment(SwingConstants.RIGHT);
    }

    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // installation id
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_ID), SwingConstants.RIGHT), constraints);

    Box lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _installationIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_installationIDLabel);

    // installation name
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_NAME), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _installationNameLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_installationNameLabel);

    // status
    constraints.gridy += 1;

    ButtonGroup radioButtonGroup = new ButtonGroup();

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(STATUS), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _enabledRadioButton.setText(GetResources().getProperty(ENABLED));
    radioButtonGroup.add(_enabledRadioButton);
    lineBox.add(_enabledRadioButton);

    _disabledRadioButton.setText(GetResources().getProperty(DISABLED));
    radioButtonGroup.add(_disabledRadioButton);
    lineBox.add(_disabledRadioButton);

    // country
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(COUNTRY), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _countryComboBox.setEditable(false);
    lineBox.add(_countryComboBox);

    // currency
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENCY), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _currencyComboBox.addActionListener(new CurrencyActionListener());
    _currencyComboBox.setEditable(false);

    lineBox.add(_currencyComboBox);
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));

    lineBox.add(_currencySymbolLabels[0]);

    // installation server address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_SERVER_ADDRESS), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _installationServerAddressTextField.setToolTipText(__resources.getProperty(INSTALLATION_SERVER_ADDRESS_TEXT_TIP));
    _installationServerAddressTextField.setColumns(IP_ADDRESS_COLUMNS);
    _installationServerAddressTextField.setHorizontalAlignment(SwingConstants.LEFT);

    lineBox.add(_installationServerAddressTextField);

    // installation server user id
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_SERVER_USER_ID), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _installationServerUserIDTextField.setToolTipText(__resources.getProperty(INSTALLATION_SERVER_USER_ID_TEXT_TIP));
    _installationServerUserIDTextField.setColumns(USER_ID_COLUMNS);
    _installationServerUserIDTextField.setHorizontalAlignment(SwingConstants.LEFT);

    lineBox.add(_installationServerUserIDTextField);

    // installation server password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_SERVER_PASSWORD), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _installationServerPasswordTextField.setToolTipText(__resources.getProperty(INSTALLATION_SERVER_PASSWORD_TEXT_TIP));
    _installationServerPasswordTextField.setColumns(PASSPHRASE_COLUMNS);
    _installationServerPasswordTextField.setHorizontalAlignment(SwingConstants.LEFT);

    lineBox.add(_installationServerPasswordTextField);

    // last receivables generated DTS
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LAST_RECEIVABLES_GENERATED_DTS), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _lastReceivablesGeneratedDTSLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_lastReceivablesGeneratedDTSLabel);

    // license expiration date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(LICENSE_EXPIRATION_DATE), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _dateLabel.setToolTipText(__resources.getProperty(LICENSE_EXPIRATION_DATE_TIP));
    _dateChooserButton.setToolTipText(__resources.getProperty(LICENSE_EXPIRATION_DATE_TIP));
    _dateChooserButton.setIcon(SystemUtilities.GetIcon("Calendar_Popup.gif"));

    new DateChooserListener(_dateChooserButton, _dateCalendar, _dateCalendar, _dateLabel);

    lineBox.add(_dateLabel);
    lineBox.add(_dateChooserButton);

    // expiration date increment
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(EXPIRATION_DATE_INCREMENT), SwingConstants.RIGHT), constraints);

    lineBox = Box.createHorizontalBox();

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(lineBox, constraints);

    _expirationDateIncrementSpinField.setToolTipText(__resources.getProperty(EXPIRATION_DATE_INCREMENT_TIP));
    SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
    _expirationDateIncrementSpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(1);
    spinnerNumberModel.setMaximum(360);

    lineBox.add(_expirationDateIncrementSpinField);

    // holding panel for the agreement boxes
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.CENTER;

    JPanel holdingPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    centerPanel.add(holdingPanel, constraints);

    GridBagConstraints constraints2 = new GridBagConstraints();
    constraints2.insets = new Insets(2, 2, 2, 2);
    constraints2.ipadx = 2;
    constraints2.ipady = 2;
    constraints2.weightx = 0;
    constraints2.weighty = 0;

    // license agreement box
    JPanel licenseAgreementsPanel = new JPanel(new GridBagLayout());
    holdingPanel.add(licenseAgreementsPanel);
    licenseAgreementsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), __resources.getProperty(LICENSE_AGREEMENTS)));

    // license agreement box: up front fee
    constraints2.gridy = 0;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    licenseAgreementsPanel.add(lineBox, constraints2);

    _upFrontFeeCheckBox = new ActionCheckBox(new UpFrontFeeAction());
    lineBox.add(_upFrontFeeCheckBox);

    lineBox.add(_currencySymbolLabels[1], constraints2);

    _upFrontFeeTextField.SetFormat(DECIMAL_FORMAT_BIG_MONEY);
    _upFrontFeeTextField.setColumns(CURRENCY_COLUMNS);
    _upFrontFeeTextField.setToolTipText(__resources.getProperty(UP_FRONT_FEE_TEXT_TIP));

    lineBox.add(_upFrontFeeTextField, constraints2);

    // license agreement box: monthly fee
    constraints2.gridy += 1;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    licenseAgreementsPanel.add(lineBox, constraints2);

    _monthlyFeeCheckBox = new ActionCheckBox(new MonthlyFeeAction());
    lineBox.add(_monthlyFeeCheckBox);

    lineBox.add(_currencySymbolLabels[2]);

    _monthlyFeeTextField.SetFormat(DECIMAL_FORMAT_BIG_MONEY);
    _monthlyFeeTextField.setColumns(CURRENCY_COLUMNS);
    _monthlyFeeTextField.setToolTipText(__resources.getProperty(MONTHLY_FEE_TEXT_TIP));
    lineBox.add(_monthlyFeeTextField);

    // license agreement box: annual fee
    constraints2.gridy += 1;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    licenseAgreementsPanel.add(lineBox, constraints2);

    _annualFeeCheckBox = new ActionCheckBox(new AnnualFeeAction());
    lineBox.add(_annualFeeCheckBox);

    lineBox.add(_currencySymbolLabels[3]);

    _annualFeeTextField.SetFormat(DECIMAL_FORMAT_BIG_MONEY);
    _annualFeeTextField.setColumns(CURRENCY_COLUMNS);
    _annualFeeTextField.setToolTipText(__resources.getProperty(ANNUAL_FEE_TEXT_TIP));
    lineBox.add(_annualFeeTextField);

    // shared revenue agreement box
    JPanel sharedRevenueAgreementsPanel = new JPanel(new GridBagLayout());
    holdingPanel.add(sharedRevenueAgreementsPanel);
    sharedRevenueAgreementsPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), __resources.getProperty(SHARED_REVENUE_AGREEMENTS)));

    // shared revenue agreement box: per page charge
    constraints2.gridy = 0;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    sharedRevenueAgreementsPanel.add(lineBox, constraints2);

    _perPageChargeCheckBox = new ActionCheckBox(new PerPageChargeAction());
    lineBox.add(_perPageChargeCheckBox);

    lineBox.add(_currencySymbolLabels[4]);

    _perPageChargeTextField.SetFormat(DECIMAL_FORMAT_SMALL_MONEY);
    _perPageChargeTextField.setColumns(CURRENCY_COLUMNS);
    _perPageChargeTextField.setToolTipText(__resources.getProperty(PER_PAGE_CHARGE_TEXT_TIP));
    lineBox.add(_perPageChargeTextField);

    // shared revenue agreement box: per print job charge
    constraints2.gridy += 1;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    sharedRevenueAgreementsPanel.add(lineBox, constraints2);

    _perPrintJobChargeCheckBox = new ActionCheckBox(new PerPrintJobChargeAction());
    lineBox.add(_perPrintJobChargeCheckBox);

    lineBox.add(_currencySymbolLabels[5]);

    _perPrintJobChargeTextField.SetFormat(DECIMAL_FORMAT_SMALL_MONEY);
    _perPrintJobChargeTextField.setColumns(CURRENCY_COLUMNS);
    _perPrintJobChargeTextField.setToolTipText(__resources.getProperty(PER_PRINT_JOB_CHARGE_TEXT_TIP));
    lineBox.add(_perPrintJobChargeTextField);

    // shared revenue agreement box: total sales percentage
    constraints2.gridy += 1;

    lineBox = Box.createHorizontalBox();

    constraints2.gridx = 0;
    constraints2.gridwidth = 1;
    constraints2.gridheight = 1;
    constraints2.fill = GridBagConstraints.NONE;
    constraints2.anchor = GridBagConstraints.EAST;

    sharedRevenueAgreementsPanel.add(lineBox, constraints2);

    _totalSalesPercentageCheckBox = new ActionCheckBox(new TotalSalesPercentageAction());
    lineBox.add(_totalSalesPercentageCheckBox);

    _totalSalesPercentageTextField.SetFormat(DECIMAL_FORMAT_PERCENT);
    _totalSalesPercentageTextField.setColumns(PERCENTAGE_COLUMNS);
    _totalSalesPercentageTextField.setToolTipText(__resources.getProperty(TOTAL_SALES_PERCENTAGE_TEXT_TIP));
    lineBox.add(_totalSalesPercentageTextField);

    lineBox.add(new JLabel("%", SwingConstants.RIGHT));

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    return super.FieldsValid();
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _installationIDLabel.setText(GetInstallationsRow().Get_installation_id().toString());
    _installationNameLabel.setText(GetInstallationsRow().Get_installation_name());

    String[] countries = Resources.GetCountries();
    String country = GetInstallationsRow().Get_country();
    _countryComboBox.RecreateComboBox(countries, country, false);

    if (_currenciesArrayList == null)
    {
      _currenciesArrayList = (ArrayList<CurrenciesRow>) MonitorDatabase.GetDefaultMonitorDatabase().GetCurrenciesTable().GetRowsAsArrayList(null, // from
        null, // predicate
        CurrenciesRow.Name_id(), // orderBy
        AbstractSQL1Table.NO_LIMIT); // limit
    }
    String[] currencies = new String[_currenciesArrayList.size()];
    for (int i = 0; i < currencies.length; i++)
    {
      currencies[i] = (_currenciesArrayList.get(i)).Get_name();
    }
    String currency = GetInstallationsRow().Get_currency();
    _currencyComboBox.RecreateComboBox(currencies, currency, false);
    // note: the CurrencyActionListener sets the currency symbols during
    // the RecreateComboBox call

    _installationServerAddressTextField.setText(GetInstallationsRow().Get_installation_server_address());
    _installationServerUserIDTextField.setText(GetInstallationsRow().Get_installation_server_user_id());
    _installationServerPasswordTextField.setText(GetInstallationsRow().Get_installation_server_password());
    _lastReceivablesGeneratedDTSLabel.setText(GetInstallationsRow().Get_last_receivables_generated_dts());
    _dateCalendar.SetDate(GetInstallationsRow().Get_license_expiration_date().getTime());
    _dateLabel.setText(_dateCalendar.GetDateString());
    _expirationDateIncrementSpinField.setValue(GetInstallationsRow().Get_expiration_date_increment());

    _upFrontFeeCheckBox.setSelected(GetInstallationsRow().Get_up_front_fee_enabled().booleanValue());
    _upFrontFeeTextField.SetValue(GetInstallationsRow().Get_up_front_fee());
    _upFrontFeeTextField.setEnabled(_upFrontFeeCheckBox.isSelected());

    _monthlyFeeCheckBox.setSelected(GetInstallationsRow().Get_monthly_fee_enabled().booleanValue());
    _monthlyFeeTextField.SetValue(GetInstallationsRow().Get_monthly_fee());
    _monthlyFeeTextField.setEnabled(_monthlyFeeCheckBox.isSelected());

    _annualFeeCheckBox.setSelected(GetInstallationsRow().Get_annual_fee_enabled().booleanValue());
    _annualFeeTextField.SetValue(GetInstallationsRow().Get_annual_fee());
    _annualFeeTextField.setEnabled(_annualFeeCheckBox.isSelected());

    _perPageChargeCheckBox.setSelected(GetInstallationsRow().Get_per_page_charge_enabled().booleanValue());
    _perPageChargeTextField.SetValue(GetInstallationsRow().Get_per_page_charge());
    _perPageChargeTextField.setEnabled(_perPageChargeCheckBox.isSelected());

    _perPrintJobChargeCheckBox.setSelected(GetInstallationsRow().Get_per_print_job_charge_enabled().booleanValue());
    _perPrintJobChargeTextField.SetValue(GetInstallationsRow().Get_per_print_job_charge());
    _perPrintJobChargeTextField.setEnabled(_perPrintJobChargeCheckBox.isSelected());

    _totalSalesPercentageCheckBox.setSelected(GetInstallationsRow().Get_total_sales_percentage_enabled().booleanValue());
    _totalSalesPercentageTextField.SetValue(GetInstallationsRow().Get_total_sales_percentage());
    _totalSalesPercentageTextField.setEnabled(_totalSalesPercentageCheckBox.isSelected());

  }

  @Override
  protected void AssignFieldsToRow()
  {
    super.AssignFieldsToRow();

    GetInstallationsRow().Set_country(_countryComboBox.getSelectedItem().toString());
    GetInstallationsRow().Set_currency(_currencyComboBox.getSelectedItem().toString());
    GetInstallationsRow().Set_installation_server_address(_installationServerAddressTextField.getText());
    GetInstallationsRow().Set_installation_server_user_id(_installationServerUserIDTextField.getText());
    GetInstallationsRow().Set_installation_server_password(_installationServerPasswordTextField.getText());
    GetInstallationsRow().Set_license_expiration_date(_dateCalendar);
    GetInstallationsRow().Set_expiration_date_increment((Integer) (_expirationDateIncrementSpinField.getValue()));
    GetInstallationsRow().Set_up_front_fee_enabled(_upFrontFeeCheckBox.isSelected());
    GetInstallationsRow().Set_up_front_fee(_upFrontFeeTextField.getText());
    GetInstallationsRow().Set_monthly_fee_enabled(_monthlyFeeCheckBox.isSelected());
    GetInstallationsRow().Set_monthly_fee(_monthlyFeeTextField.getText());
    GetInstallationsRow().Set_annual_fee_enabled(_annualFeeCheckBox.isSelected());
    GetInstallationsRow().Set_annual_fee(_annualFeeTextField.getText());
    GetInstallationsRow().Set_per_page_charge_enabled(_perPageChargeCheckBox.isSelected());
    GetInstallationsRow().Set_per_page_charge(_perPageChargeTextField.getText());
    GetInstallationsRow().Set_per_print_job_charge_enabled(_perPrintJobChargeCheckBox.isSelected());
    GetInstallationsRow().Set_per_print_job_charge(_perPrintJobChargeTextField.getText());
    GetInstallationsRow().Set_total_sales_percentage_enabled(_totalSalesPercentageCheckBox.isSelected());
    GetInstallationsRow().Set_total_sales_percentage(_totalSalesPercentageTextField.getText());
  }

  @Override
  protected void SizePanel()
  {
    Dimension dim = getPreferredSize();
    dim.width *= 1.1;
    setSize(dim);
  }

  String GetSelectedCurrencySymbol()
  {
    String currencySymbol = "";
    int selectedIndex = _currencyComboBox.getSelectedIndex();
    if (selectedIndex >= 0)
    {
      currencySymbol = (_currenciesArrayList.get(selectedIndex)).Get_symbol();
    }
    return currencySymbol;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  public InstallationsRow GetInstallationsRow()
  {
    return (InstallationsRow) _sqlRow;
  }

  @Override
  protected void EndingSave(boolean rc)
  {
    super.EndingSave(rc);
    if (rc == true)
    {
      if (ModeIsNew() == true)
      {
        // save _listPanelListeners so we can pass it on to the
        // remote entry panel
        _saveListPanelListeners = _listPanelListeners;
      }
      else
        if (ModeIsUpdate() == true)
        {
          try
          {
            InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(GetInstallationsRow().Get_installation_server_address(), GetInstallationsRow()
              .Get_installation_server_user_id(), GetInstallationsRow().Get_installation_server_password(), GetInstallationsRow().Get_installation_id()));
            InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
            InstallationRow installationRow = InstallationTable.GetCachedInstallationRow();
            installationRow.Set_status(GetInstallationsRow().Get_status());
            installationRow.Set_currency_symbol(GetSelectedCurrencySymbol());
            installationRow.Update();
            InstallationDatabase.GetDefaultInstallationDatabase().Done();
          }
          catch (Exception excp)
          {
            excp.printStackTrace();
            JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
          }
        }
    }
  }

  @Override
  public void setVisible(boolean visibility)
  {
    super.setVisible(visibility);
    // this is as good a place as any to do this
    AddPanelListener(this);
  }

  /**
    i use this to be notified when i'm shutting down so i can
    launch the remote entry panel, if appropriate
  */
  @Override
  public void EndOfPanel(char action)
  {
    if ((action == PanelNotifier.ACTION_SAVE) && (ModeIsNew() == true))
    {
      InstallationDatabase installationDatabase = new InstallationDatabase(GetInstallationsRow().Get_installation_server_address(), GetInstallationsRow().Get_installation_server_user_id(),
        GetInstallationsRow().Get_installation_server_password(), GetInstallationsRow().Get_installation_id());

      try
      {
        installationDatabase.ConnectDatabase();
        MonitorRemoteEntryPanel remoteEntryPanel = new MonitorRemoteEntryPanel(_listPanel, installationDatabase.GetInstallationTable(), null, GetInstallationsRow());
        if (_saveListPanelListeners != null)
        {
          for (int i = 0; i < _saveListPanelListeners.size(); i++)
          {
            ListPanelListener listPanelListener = _saveListPanelListeners.get(i);
            remoteEntryPanel.AddListPanelListener(listPanelListener);
          }
        }
        remoteEntryPanel.setVisible(true);
      }
      catch (SQLException excp)
      {
        excp.printStackTrace();
        JOptionPane.showMessageDialog(null, excp, "ConnectDatabase()", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  @Override
  public void PanelStateChange(char action)
  {
    // do nothing
  }

  private class UpFrontFeeAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -3535815575244551903L;

    public UpFrontFeeAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(UP_FRONT_FEE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(UP_FRONT_FEE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _upFrontFeeTextField.setEnabled(_upFrontFeeCheckBox.isSelected());
    }

  }

  private class MonthlyFeeAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -5588320909255548970L;

    public MonthlyFeeAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(MONTHLY_FEE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(MONTHLY_FEE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _monthlyFeeTextField.setEnabled(_monthlyFeeCheckBox.isSelected());
    }

  }

  private class AnnualFeeAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -4948852483801040145L;

    public AnnualFeeAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(ANNUAL_FEE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(ANNUAL_FEE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _annualFeeTextField.setEnabled(_annualFeeCheckBox.isSelected());
    }

  }

  private class PerPageChargeAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -3781280598209220704L;

    public PerPageChargeAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(PER_PAGE_CHARGE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PER_PAGE_CHARGE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _perPageChargeTextField.setEnabled(_perPageChargeCheckBox.isSelected());
    }

  }

  private class PerPrintJobChargeAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 6378668079977468625L;

    public PerPrintJobChargeAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(PER_PRINT_JOB_CHARGE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(PER_PRINT_JOB_CHARGE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _perPrintJobChargeTextField.setEnabled(_perPrintJobChargeCheckBox.isSelected());
    }

  }

  private class TotalSalesPercentageAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -4832247428086301137L;

    public TotalSalesPercentageAction()
    {
      super();

      putValue(Action.NAME, __resources.getProperty(TOTAL_SALES_PERCENTAGE));
      putValue(Action.SHORT_DESCRIPTION, __resources.getProperty(TOTAL_SALES_PERCENTAGE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _totalSalesPercentageTextField.setEnabled(_totalSalesPercentageCheckBox.isSelected());
    }

  }

  private class CurrencyActionListener
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 3801364819742694686L;

    public CurrencyActionListener()
    {
      super();
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      if (_currencySymbolLabels != null)
      {
        String selectedSymbol = GetSelectedCurrencySymbol();
        for (int i = 0; i < _currencySymbolLabels.length; ++i)
        {
          if (_currencySymbolLabels[i] != null)
          {
            _currencySymbolLabels[i].setText(selectedSymbol);
            _currencySymbolLabels[i].invalidate();
          }
        }
      }
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorLocalEntryPanel.class.getName());
  }

}
