package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry3Panel;
import shackelford.floyd.printmanagementsystem.common.DateChooserListener;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;
import shackelford.floyd.printmanagementsystem.monitordatabase.ReceivablesRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.ReceivablesTable;


/**
 * <p>Title: MonitorReceivablesEntryPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorReceivablesEntryPanel
  extends AbstractEntry3Panel
{

  /**
   * Eclipse generated value
   */
  private static final long        serialVersionUID                      = -1005936333284213917L;

  private final JLabel             _receivablesNumberLabel               = new JLabel();

  private final _JComboBox         _installationIDComboBox               = new _JComboBox();

  private final _GregorianCalendar _billingPeriodStartCalendar           = _GregorianCalendar.Now();

  private final JLabel             _billingPeriodStartLabel              = new JLabel();

  private final JButton            _billingPeriodStartDateChooserButton  = new JButton();

  private final _GregorianCalendar _billingPeriodEndCalendar             = _GregorianCalendar.Now();

  private final JLabel             _billingPeriodEndLabel                = new JLabel();

  private final JButton            _billingPeriodEndDateChooserButton    = new JButton();

  private final NumericTextField   _perPageChargeTextField               = new NumericTextField();

  private final NumericTextField   _perPrintJobChargeTextField           = new NumericTextField();

  private final NumericTextField   _percentageSalesChargeTextField       = new NumericTextField();

  private static DecimalFormat     __formatter                           = new DecimalFormat("#,##0.000");

  private static Resources         __resources;

  private static final String      RECEIVABLES_NUMBER                    = "RECEIVABLES_NUMBER";

  private static final String      INSTALLATION_ID                       = "INSTALLATION_ID";

  private static final String      INSTALLATION_ID_TIP                   = "INSTALLATION_ID_TIP";

  private static final String      BILLING_PERIOD_START                  = "BILLING_PERIOD_START";

  private static final String      BILLING_PERIOD_START_TIP              = "BILLING_PERIOD_START_TIP";

  private static final String      BILLING_PERIOD_END                    = "BILLING_PERIOD_END";

  private static final String      BILLING_PERIOD_END_TIP                = "BILLING_PERIOD_END_TIP";

  private static final String      PER_PAGE_CHARGE                       = "PER_PAGE_CHARGE";

  private static final String      PER_PAGE_CHARGE_TIP                   = "PER_PAGE_CHARGE_TIP";

  private static final String      PER_PRINT_JOB_CHARGE                  = "PER_PRINT_JOB_CHARGE";

  private static final String      PER_PRINT_JOB_CHARGE_TIP              = "PER_PRINT_JOB_CHARGE_TIP";

  private static final String      PERCENTAGE_SALES_CHARGE               = "PERCENTAGE_SALES_CHARGE";

  private static final String      PERCENTAGE_SALES_CHARGE_TIP           = "PERCENTAGE_SALES_CHARGE_TIP";

  private static final String      BLANK_PER_PAGE_CHARGE_MSG             = "BLANK_PER_PAGE_CHARGE_MSG";

  private static final String      BLANK_PER_PAGE_CHARGE_TITLE           = "BLANK_PER_PAGE_CHARGE_TITLE";

  private static final String      BLANK_PER_PRINT_JOB_CHARGE_MSG        = "BLANK_PER_PRINT_JOB_CHARGE_MSG";

  private static final String      BLANK_PER_PRINT_JOB_CHARGE_TITLE      = "BLANK_PER_PRINT_JOB_CHARGE_TITLE";

  private static final String      BLANK_PERCENTAGE_SALES_CHARGE_MSG     = "BLANK_PERCENTAGE_SALES_CHARGE_MSG";

  private static final String      BLANK_PERCENTAGE_SALES_CHARGE_TITLE   = "BLANK_PERCENTAGE_SALES_CHARGE_TITLE";

  private static final String      INVALID_PER_PAGE_CHARGE_MSG           = "INVALID_PER_PAGE_CHARGE_MSG";

  private static final String      INVALID_PER_PAGE_CHARGE_TITLE         = "INVALID_PER_PAGE_CHARGE_TITLE";

  private static final String      INVALID_PER_PRINT_JOB_CHARGE_MSG      = "INVALID_PER_PRINT_JOB_CHARGE_MSG";

  private static final String      INVALID_PER_PRINT_JOB_CHARGE_TITLE    = "INVALID_PER_PRINT_JOB_CHARGE_TITLE";

  private static final String      INVALID_PERCENTAGE_SALES_CHARGE_MSG   = "INVALID_PERCENTAGE_SALES_CHARGE_MSG";

  private static final String      INVALID_PERCENTAGE_SALES_CHARGE_TITLE = "INVALID_PERCENTAGE_SALES_CHARGE_TITLE";

  private static final int         CHARGE_FIELD_WIDTH                    = 6;

  public MonitorReceivablesEntryPanel()
  {
    super();
  }

  public MonitorReceivablesEntryPanel(ListPanelCursor listPanel, ReceivablesTable receivablesTable, ReceivablesRow receivablesRow)
  {
    super();
    Initialize(listPanel, receivablesTable, receivablesRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      GetReceivablesRow().SetToNextReceivablesNumber();
    }
  }

  @SuppressWarnings("unused")
  @Override
  protected Container CreateCenterPanel()
  {

    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // receivables number
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(RECEIVABLES_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _receivablesNumberLabel.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_receivablesNumberLabel, constraints);

    // installation id
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(INSTALLATION_ID), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _installationIDComboBox.setToolTipText(__resources.getProperty(INSTALLATION_ID_TIP));
    centerPanel.add(_installationIDComboBox, constraints);

    // billing period start
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(BILLING_PERIOD_START), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingPeriodStartLabel.setToolTipText(__resources.getProperty(BILLING_PERIOD_START_TIP));
    centerPanel.add(_billingPeriodStartLabel, constraints);

    _billingPeriodStartDateChooserButton.setIcon(SystemUtilities.GetIcon("Calendar_Popup.gif"));
    _billingPeriodStartDateChooserButton.setToolTipText(__resources.getProperty(BILLING_PERIOD_START_TIP));
    centerPanel.add(_billingPeriodStartDateChooserButton, constraints);

    new DateChooserListener(_billingPeriodStartDateChooserButton, _billingPeriodStartCalendar, _billingPeriodStartCalendar, _billingPeriodStartLabel);

    // billing period end
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(BILLING_PERIOD_END), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _billingPeriodEndLabel.setToolTipText(__resources.getProperty(BILLING_PERIOD_END_TIP));
    centerPanel.add(_billingPeriodEndLabel, constraints);

    _billingPeriodEndDateChooserButton.setIcon(SystemUtilities.GetIcon("Calendar_Popup.gif"));
    _billingPeriodEndDateChooserButton.setToolTipText(__resources.getProperty(BILLING_PERIOD_END_TIP));
    centerPanel.add(_billingPeriodEndDateChooserButton, constraints);

    new DateChooserListener(_billingPeriodEndDateChooserButton, _billingPeriodEndCalendar, _billingPeriodEndCalendar, _billingPeriodEndLabel);

    // per page charge
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PER_PAGE_CHARGE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _perPageChargeTextField.SetFormat(__formatter);
    _perPageChargeTextField.setColumns(CHARGE_FIELD_WIDTH);
    _perPageChargeTextField.setToolTipText(__resources.getProperty(PER_PAGE_CHARGE_TIP));

    centerPanel.add(_perPageChargeTextField, constraints);

    // per print job charge
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PER_PRINT_JOB_CHARGE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _perPrintJobChargeTextField.SetFormat(__formatter);
    _perPrintJobChargeTextField.setColumns(CHARGE_FIELD_WIDTH);
    _perPrintJobChargeTextField.setToolTipText(__resources.getProperty(PER_PRINT_JOB_CHARGE_TIP));

    centerPanel.add(_perPrintJobChargeTextField, constraints);

    // percentage sales charge
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(PERCENTAGE_SALES_CHARGE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _percentageSalesChargeTextField.SetFormat(__formatter);
    _percentageSalesChargeTextField.setColumns(CHARGE_FIELD_WIDTH);
    _percentageSalesChargeTextField.setToolTipText(__resources.getProperty(PERCENTAGE_SALES_CHARGE_TIP));

    centerPanel.add(_percentageSalesChargeTextField, constraints);

    return centerPanel;
  }

  @Override
  protected boolean FieldsValid()
  {

    String perPageChargeString = _perPageChargeTextField.getText().trim();
    if (perPageChargeString.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_PER_PAGE_CHARGE_MSG), __resources.getProperty(BLANK_PER_PAGE_CHARGE_TITLE));
      _perPageChargeTextField.requestFocus();
      return false;
    }

    try
    {
      Double.parseDouble(perPageChargeString);
    }
    catch (NumberFormatException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PER_PAGE_CHARGE_MSG), __resources.getProperty(INVALID_PER_PAGE_CHARGE_TITLE));
      _perPageChargeTextField.requestFocus();
      return false;
    }

    String perPrintJobChargeString = _perPrintJobChargeTextField.getText().trim();
    if (perPrintJobChargeString.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_PER_PRINT_JOB_CHARGE_MSG), __resources.getProperty(BLANK_PER_PRINT_JOB_CHARGE_TITLE));
      _perPrintJobChargeTextField.requestFocus();
      return false;
    }

    try
    {
      Double.parseDouble(perPrintJobChargeString);
    }
    catch (NumberFormatException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PER_PRINT_JOB_CHARGE_MSG), __resources.getProperty(INVALID_PER_PRINT_JOB_CHARGE_TITLE));
      _perPrintJobChargeTextField.requestFocus();
      return false;
    }

    String percentageSalesChargeString = _percentageSalesChargeTextField.getText().trim();
    if (percentageSalesChargeString.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_PERCENTAGE_SALES_CHARGE_MSG), __resources.getProperty(BLANK_PERCENTAGE_SALES_CHARGE_TITLE));
      _percentageSalesChargeTextField.requestFocus();
      return false;
    }

    try
    {
      Double.parseDouble(percentageSalesChargeString);
    }
    catch (NumberFormatException excp)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(INVALID_PERCENTAGE_SALES_CHARGE_MSG), __resources.getProperty(INVALID_PERCENTAGE_SALES_CHARGE_TITLE));
      _percentageSalesChargeTextField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _receivablesNumberLabel.setText(GetReceivablesRow().Get_receivables_number().toString());

    String[] installationIDs = MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable().GetInstallationIDsAsStringArray();
    String installationID = String.valueOf(GetReceivablesRow().Get_installation_id());
    _installationIDComboBox.RecreateComboBox(installationIDs, installationID, false);

    _billingPeriodStartCalendar.SetDate(GetReceivablesRow().Get_billing_period_start().getTime());
    _billingPeriodEndCalendar.SetDate(GetReceivablesRow().Get_billing_period_end().getTime());
    _perPageChargeTextField.SetValue(GetReceivablesRow().Get_per_page_charge());
    _perPrintJobChargeTextField.SetValue(GetReceivablesRow().Get_per_print_job_charge());
    _percentageSalesChargeTextField.SetValue(GetReceivablesRow().Get_percentage_of_revenues_charge());
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetReceivablesRow().Set_installation_id(_installationIDComboBox.getSelectedItem().toString());
    GetReceivablesRow().Set_billing_period_start(_billingPeriodStartCalendar);
    GetReceivablesRow().Set_billing_period_end(_billingPeriodEndCalendar);
    GetReceivablesRow().Set_per_page_charge(_perPageChargeTextField.GetDoubleValue());
    GetReceivablesRow().Set_per_print_job_charge(_perPrintJobChargeTextField.GetDoubleValue());
    GetReceivablesRow().Set_percentage_of_revenues_charge(_percentageSalesChargeTextField.GetDoubleValue());
  }

  protected ReceivablesRow GetReceivablesRow()
  {
    return (ReceivablesRow) _sqlRow;
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorReceivablesEntryPanel.class.getName());
  }

}
