package shackelford.floyd.printmanagementsystem.monitor;

import shackelford.floyd.printmanagementsystem.common.AbstractList3Panel;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;


/**
 * <p>Title: MonitorReceivablesPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorReceivablesPanel
  extends AbstractList3Panel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 93161698794310082L;

  private static Resources  __resources;

  public MonitorReceivablesPanel()
  {
    super();
    Initialize(MonitorDatabase.GetDefaultMonitorDatabase().GetReceivablesTable(), MonitorReceivablesEntryPanel.class);
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorReceivablesPanel.class.getName());
  }

}
