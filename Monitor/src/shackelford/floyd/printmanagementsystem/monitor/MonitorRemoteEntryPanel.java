package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractEntry4Panel;
import shackelford.floyd.printmanagementsystem.common.DateChooserListener;
import shackelford.floyd.printmanagementsystem.common.ListPanelCursor;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;
import shackelford.floyd.printmanagementsystem.common._JComboBox;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesTable;
import shackelford.floyd.printmanagementsystem.monitordatabase.InstallationsRow;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorDatabase;


/**
 * <p>Title: MonitorRemoteEntryPanel</p>
 * <p>Description:
  NOTE: bank of america doesn't handle refunds without a corresponding charge order id and authorization code.
  we don't want to tie the refund to a charge order id because the refund may span multiple orders and not
  necessarily be an amount less than any single order. so for the time being, i have disabled the credit
  card refund capability.

  NOTE: pay pal is not "on board" yet. so everything for paypal is disabled for now.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorRemoteEntryPanel
  extends AbstractEntry4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long             serialVersionUID                            = 2979488003039516631L;

  private static final int              NUMBER_OF_CURRENCY_SYMBOL_LABELS            = 10;

  private InstallationsRow              _installationsRow                           = null;

  private RulesRow[]                    _ruleRows                                   = null;

  private final JLabel[]                _ruleNameLabels                             = new JLabel[RulesRow.NUMBER_OF_RULES];

  private final OverwritableTextField[] _ruleValueTextFields                        = new OverwritableTextField[RulesRow.NUMBER_OF_RULES];

  private final JLabel                  _installationIDLabel                        = new JLabel();

  private final _JComboBox              _languageComboBox                           = new _JComboBox();

  private final OverwritableTextField   _installationPassphraseTextField            = new OverwritableTextField();

  private final JLabel[]                _currencySymbolLabels                       = new JLabel[NUMBER_OF_CURRENCY_SYMBOL_LABELS];

  private final JRadioButton            _commaThousandsSeparatorRadioButton         = new JRadioButton();

  private final JRadioButton            _periodThousandsSeparatorRadioButton        = new JRadioButton();

  private final JRadioButton            _commaDecimalSeparatorRadioButton           = new JRadioButton();

  private final JRadioButton            _periodDecimalSeparatorRadioButton          = new JRadioButton();

  private final JRadioButton            _automaticRadioButton                       = new JRadioButton();

  private final JRadioButton            _manualRadioButton                          = new JRadioButton();

  private final JSpinner                _maxClientsSpinField                        = new JSpinner();

  private final JSpinner                _maxGatekeepersSpinField                    = new JSpinner();

  private final OverwritableTextField   _installationNameTextField                  = new OverwritableTextField();

  private final _GregorianCalendar      _dateCalendar                               = _GregorianCalendar.Now();

  private final JLabel                  _dateLabel                                  = new JLabel();

  private final JButton                 _dateChooserButton                          = new JButton();

  private final OverwritableTextField   _egoldChargeURLTextField                    = new OverwritableTextField();

  private final OverwritableTextField   _egoldChargeAccountNumberTextField          = new OverwritableTextField();

  private final OverwritableTextField   _egoldChargePassphraseTextField             = new OverwritableTextField();

  private final NumericTextField        _egoldChargeMinAmountTextField              = new NumericTextField();

  private final NumericTextField        _egoldChargeXactionFeeTextField             = new NumericTextField();

  private final NumericTextField        _egoldChargeXactionFeePercentTextField      = new NumericTextField();

  private final OverwritableTextField   _egoldRefundURLTextField                    = new OverwritableTextField();

  private final OverwritableTextField   _egoldRefundAccountNumberTextField          = new OverwritableTextField();

  private final OverwritableTextField   _egoldRefundPassphraseTextField             = new OverwritableTextField();

  private final NumericTextField        _egoldRefundXactionFeeTextField             = new NumericTextField();

  private final NumericTextField        _egoldRefundXactionFeePercentTextField      = new NumericTextField();

  private final OverwritableTextField   _paypalChargeURLTextField                   = new OverwritableTextField();

  private final OverwritableTextField   _paypalChargeAccountNumberTextField         = new OverwritableTextField();

  private final OverwritableTextField   _paypalChargePassphraseTextField            = new OverwritableTextField();

  private final NumericTextField        _paypalChargeMinAmountTextField             = new NumericTextField();

  private final NumericTextField        _paypalChargeXactionFeeTextField            = new NumericTextField();

  private final NumericTextField        _paypalChargeXactionFeePercentTextField     = new NumericTextField();

  private final OverwritableTextField   _paypalRefundURLTextField                   = new OverwritableTextField();

  private final OverwritableTextField   _paypalRefundAccountNumberTextField         = new OverwritableTextField();

  private final OverwritableTextField   _paypalRefundPassphraseTextField            = new OverwritableTextField();

  private final NumericTextField        _paypalRefundXactionFeeTextField            = new NumericTextField();

  private final NumericTextField        _paypalRefundXactionFeePercentTextField     = new NumericTextField();

  private final OverwritableTextField   _creditcardChargeURLTextField               = new OverwritableTextField();

  private final OverwritableTextField   _creditcardChargeAccountNumberTextField     = new OverwritableTextField();

  private final OverwritableTextField   _creditcardChargePassphraseTextField        = new OverwritableTextField();

  private final NumericTextField        _creditcardChargeMinAmountTextField         = new NumericTextField();

  private final NumericTextField        _creditcardChargeXactionFeeTextField        = new NumericTextField();

  private final NumericTextField        _creditcardChargeXactionFeePercentTextField = new NumericTextField();

  private final OverwritableTextField   _creditcardRefundURLTextField               = new OverwritableTextField();

  private final OverwritableTextField   _creditcardRefundAccountNumberTextField     = new OverwritableTextField();

  private final OverwritableTextField   _creditcardRefundPassphraseTextField        = new OverwritableTextField();

  private final NumericTextField        _creditcardRefundXactionFeeTextField        = new NumericTextField();

  private final NumericTextField        _creditcardRefundXactionFeePercentTextField = new NumericTextField();

  private final JSpinner                _releaseNumberSpinField                     = new JSpinner();

  private final OverwritableTextField   _smtpHostTextField                          = new OverwritableTextField();

  private final OverwritableTextField   _administratorEMailAddressTextField         = new OverwritableTextField();

  private static Resources              __resources;

  private static final String           ECOMMERCE_TAB                               = "ECOMMERCE_TAB";

  private static final String           SITE_DATA_TAB                               = "SITE_DATA_TAB";

  private static final String           RULES_TAB                                   = "RULES_TAB";

  private static final String           AUTOMATIC                                   = "AUTOMATIC";

  private static final String           AUTOMATIC_TIP                               = "AUTOMATIC_TIP";

  //  private static final String     CHARGE_CREDIT_CARD_TRANSACTION_FEE = "CHARGE_CREDIT_CARD_TRANSACTION_FEE";
  //  private static final String     CHARGE_CREDIT_CARD_TRANSACTION_FEE_TIP = "CHARGE_CREDIT_CARD_TRANSACTION_FEE_TIP";
  //  private static final String     CHARGE_CREDIT_CARD_TRANSACTION_FEE_PERCENT_TIP = "CHARGE_CREDIT_CARD_TRANSACTION_FEE_PERCENT_TIP";
  private static final String           COMMA                                       = "COMMA";

  private static final String           COMMA_TIP                                   = "COMMA_TIP";

  private static final String           CURRENCY_SYMBOL                             = "CURRENCY_SYMBOL";

  private static final String           DECIMAL_SEPARATOR                           = "DECIMAL_SEPARATOR";

  private static final String           INSTALLATION_ID                             = "INSTALLATION_ID";

  private static final String           INSTALLATION_NAME                           = "INSTALLATION_NAME";

  private static final String           INSTALLATION_NAME_TIP                       = "INSTALLATION_NAME_TIP";

  private static final String           INSTALLATION_PASSPHRASE                     = "INSTALLATION_PASSPHRASE";

  private static final String           INSTALLATION_PASSPHRASE_TIP                 = "INSTALLATION_PASSPHRASE_TIP";

  private static final String           INTERIM_EXPIRATION_DATE                     = "INTERIM_EXPIRATION_DATE";

  private static final String           INTERIM_EXPIRATION_DATE_TIP                 = "INTERIM_EXPIRATION_DATE_TIP";

  private static final String           LANGUAGE                                    = "LANGUAGE";

  private static final String           MANUAL                                      = "MANUAL";

  private static final String           MANUAL_TIP                                  = "MANUAL_TIP";

  private static final String           CURRENT_RELEASE_NUMBER                      = "CURRENT_RELEASE_NUMBER";

  private static final String           CURRENT_RELEASE_NUMBER_TIP                  = "CURRENT_RELEASE_NUMBER_TIP";

  private static final String           MAX_NUMBER_OF_CLIENTS                       = "MAX_NUMBER_OF_CLIENTS";

  private static final String           MAX_NUMBER_OF_CLIENTS_TIP                   = "MAX_NUMBER_OF_CLIENTS_TIP";

  private static final String           MAX_NUMBER_OF_GATEKEEPERS                   = "MAX_NUMBER_OF_GATEKEEPERS";

  private static final String           MAX_NUMBER_OF_GATEKEEPERS_TIP               = "MAX_NUMBER_OF_GATEKEEPERS_TIP";

  private static final String           PERCENT_SIGN                                = "PERCENT_SIGN";

  private static final String           PERIOD                                      = "PERIOD";

  private static final String           PERIOD_TIP                                  = "PERIOD_TIP";

  private static final String           SMTP_HOST                                   = "SMTP_HOST";

  private static final String           SMTP_HOST_TIP                               = "SMTP_HOST_TIP";

  private static final String           ADMINISTRATOR_E_MAIL_ADDRESS                = "ADMINISTRATOR_E_MAIL_ADDRESS";

  private static final String           ADMINISTRATOR_E_MAIL_ADDRESS_TIP            = "ADMINISTRATOR_E_MAIL_ADDRESS_TIP";

  //  private static final String     PLUS_SIGN = "PLUS_SIGN";
  private static final String           THOUSANDS_SEPARATOR                         = "THOUSANDS_SEPARATOR";

  private static final String           UPGRADE_PREFERENCE                          = "UPGRADE_PREFERENCE";

  private static final String           EGOLD                                       = "EGOLD";

  private static final String           PAYPAL                                      = "PAYPAL";

  private static final String           CREDITCARD                                  = "CREDITCARD";

  private static final String           CHARGE_URL                                  = "CHARGE_URL";

  private static final String           CHARGE_ACCOUNT_NUMBER                       = "CHARGE_ACCOUNT_NUMBER";

  private static final String           CHARGE_PASSPHRASE                           = "CHARGE_PASSPHRASE";

  private static final String           CHARGE_MIN_AMOUNT                           = "CHARGE_MIN_AMOUNT";

  private static final String           CHARGE_XACTION_FEE                          = "CHARGE_XACTION_FEE";

  private static final String           CHARGE_XACTION_FEE_PERCENT                  = "CHARGE_XACTION_FEE_PERCENT";

  private static final String           REFUND_URL                                  = "REFUND_URL";

  private static final String           REFUND_ACCOUNT_NUMBER                       = "REFUND_ACCOUNT_NUMBER";

  private static final String           REFUND_PASSPHRASE                           = "REFUND_PASSPHRASE";

  private static final String           REFUND_XACTION_FEE                          = "REFUND_XACTION_FEE";

  private static final String           REFUND_XACTION_FEE_PERCENT                  = "REFUND_XACTION_FEE_PERCENT";

  private static final String           EGOLD_CHARGE_URL_TIP                        = "EGOLD_CHARGE_URL_TIP";

  private static final String           EGOLD_CHARGE_ACCOUNT_NUMBER_TIP             = "EGOLD_CHARGE_ACCOUNT_NUMBER_TIP";

  private static final String           EGOLD_CHARGE_PASSPHRASE_TIP                 = "EGOLD_CHARGE_PASSPHRASE_TIP";

  private static final String           EGOLD_CHARGE_MIN_AMOUNT_TIP                 = "EGOLD_CHARGE_MIN_AMOUNT_TIP";

  private static final String           EGOLD_CHARGE_XACTION_FEE_TIP                = "EGOLD_CHARGE_XACTION_FEE_TIP";

  private static final String           EGOLD_CHARGE_XACTION_FEE_PERCENT_TIP        = "EGOLD_CHARGE_XACTION_FEE_PERCENT_TIP";

  private static final String           EGOLD_REFUND_URL_TIP                        = "EGOLD_REFUND_URL_TIP";

  private static final String           EGOLD_REFUND_ACCOUNT_NUMBER_TIP             = "EGOLD_REFUND_ACCOUNT_NUMBER_TIP";

  private static final String           EGOLD_REFUND_PASSPHRASE_TIP                 = "EGOLD_REFUND_PASSPHRASE_TIP";

  private static final String           EGOLD_REFUND_XACTION_FEE_TIP                = "EGOLD_REFUND_XACTION_FEE_TIP";

  private static final String           EGOLD_REFUND_XACTION_FEE_PERCENT_TIP        = "EGOLD_REFUND_XACTION_FEE_PERCENT_TIP";

  private static final String           PAYPAL_CHARGE_URL_TIP                       = "PAYPAL_CHARGE_URL_TIP";

  private static final String           PAYPAL_CHARGE_ACCOUNT_NUMBER_TIP            = "PAYPAL_CHARGE_ACCOUNT_NUMBER_TIP";

  private static final String           PAYPAL_CHARGE_PASSPHRASE_TIP                = "PAYPAL_CHARGE_PASSPHRASE_TIP";

  private static final String           PAYPAL_CHARGE_MIN_AMOUNT_TIP                = "PAYPAL_CHARGE_MIN_AMOUNT_TIP";

  private static final String           PAYPAL_CHARGE_XACTION_FEE_TIP               = "PAYPAL_CHARGE_XACTION_FEE_TIP";

  private static final String           PAYPAL_CHARGE_XACTION_FEE_PERCENT_TIP       = "PAYPAL_CHARGE_XACTION_FEE_PERCENT_TIP";

  private static final String           PAYPAL_REFUND_URL_TIP                       = "PAYPAL_REFUND_URL_TIP";

  private static final String           PAYPAL_REFUND_ACCOUNT_NUMBER_TIP            = "PAYPAL_REFUND_ACCOUNT_NUMBER_TIP";

  private static final String           PAYPAL_REFUND_PASSPHRASE_TIP                = "PAYPAL_REFUND_PASSPHRASE_TIP";

  private static final String           PAYPAL_REFUND_XACTION_FEE_TIP               = "PAYPAL_REFUND_XACTION_FEE_TIP";

  private static final String           PAYPAL_REFUND_XACTION_FEE_PERCENT_TIP       = "PAYPAL_REFUND_XACTION_FEE_PERCENT_TIP";

  private static final String           CREDITCARD_CHARGE_URL_TIP                   = "CREDITCARD_CHARGE_URL_TIP";

  private static final String           CREDITCARD_CHARGE_ACCOUNT_NUMBER_TIP        = "CREDITCARD_CHARGE_ACCOUNT_NUMBER_TIP";

  private static final String           CREDITCARD_CHARGE_PASSPHRASE_TIP            = "CREDITCARD_CHARGE_PASSPHRASE_TIP";

  private static final String           CREDITCARD_CHARGE_MIN_AMOUNT_TIP            = "CREDITCARD_CHARGE_MIN_AMOUNT_TIP";

  private static final String           CREDITCARD_CHARGE_XACTION_FEE_TIP           = "CREDITCARD_CHARGE_XACTION_FEE_TIP";

  private static final String           CREDITCARD_CHARGE_XACTION_FEE_PERCENT_TIP   = "CREDITCARD_CHARGE_XACTION_FEE_TIP";

  private static final String           CREDITCARD_REFUND_URL_TIP                   = "CREDITCARD_REFUND_URL_TIP";

  private static final String           CREDITCARD_REFUND_ACCOUNT_NUMBER_TIP        = "CREDITCARD_REFUND_ACCOUNT_NUMBER_TIP";

  private static final String           CREDITCARD_REFUND_PASSPHRASE_TIP            = "CREDITCARD_REFUND_PASSPHRASE_TIP";

  private static final String           CREDITCARD_REFUND_XACTION_FEE_TIP           = "CREDITCARD_REFUND_XACTION_FEE_TIP";

  private static final String           CREDITCARD_REFUND_XACTION_FEE_PERCENT_TIP   = "CREDITCARD_REFUND_XACTION_FEE_PERCENT_TIP";

  private static final String           BLANK_INSTALLATION_NAME_MSG                 = "BLANK_INSTALLATION_NAME_MSG";

  private static final String           BLANK_INSTALLATION_NAME_TITLE               = "BLANK_INSTALLATION_NAME_TITLE";

  private static final int              URL_FIELD_WIDTH                             = 20;

  private static final int              ACCOUNT_NUMBER_FIELD_WIDTH                  = 20;

  private static final int              PASSPHRASE_FIELD_WIDTH                      = 20;

  private static final int              RULE_WIDTH                                  = 10;

  private static final int              INSTALLATION_NAME_WIDTH                     = 20;

  private static final int              HOST_NAME_WIDTH                             = 20;

  private static final int              EMAIL_ADDRESS_WIDTH                         = 20;

  private static final int              CURRENCY_FIELD_WIDTH                        = 6;

  private static final int              PERCENTAGE_FIELD_WIDTH                      = 6;

  private static final Integer          MAX_CLIENTS                                 = new Integer(999999);

  private static final Integer          MAX_GATEKEEPERS                             = new Integer(99999);

  private static final Integer          MAX_RELEASES                                = new Integer(9999);

  private static final DecimalFormat    DECIMAL_FORMAT_MONEY                        = new DecimalFormat("#,##0.000");

  private static final int              NUMBER_OF_RULES_ACROSS                      = 3;

  public MonitorRemoteEntryPanel()
  {
    super();
  }

  public MonitorRemoteEntryPanel(ListPanelCursor listPanel, InstallationTable installationTable, InstallationRow installationRow, InstallationsRow installationsRow)
  {
    super();
    _installationsRow = installationsRow;
    Initialize(listPanel, installationTable, installationRow);
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (ModeIsNew() == true)
    {
      GetInstallationRow().Set_status(GetInstallationsRow().Get_status());
      GetInstallationRow().Set_currency_symbol(MonitorDatabase.GetDefaultMonitorDatabase().GetCurrenciesTable().GetCurrenciesRowUsingName(GetInstallationsRow().Get_currency()).Get_symbol());
      GetInstallationRow().Set_installation_name(GetInstallationsRow().Get_installation_name());
    }
  }

  @Override
  protected Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

    Box lineBox = Box.createHorizontalBox();
    northPanel.add(lineBox);

    // installation id
    lineBox.add(new JLabel(__resources.getProperty(INSTALLATION_ID), SwingConstants.RIGHT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    _installationIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_installationIDLabel);

    // installation name
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_GAP_WIDTH));
    lineBox.add(new JLabel(__resources.getProperty(INSTALLATION_NAME), SwingConstants.RIGHT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    _installationNameTextField.setToolTipText(__resources.getProperty(INSTALLATION_NAME_TIP));
    _installationNameTextField.setColumns(INSTALLATION_NAME_WIDTH);
    _installationNameTextField.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_installationNameTextField);

    // status
    ButtonGroup radioButtonGroup = new ButtonGroup();
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_GAP_WIDTH));
    lineBox.add(new JLabel(GetResources().getProperty(STATUS), SwingConstants.RIGHT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    _enabledRadioButton.setText(GetResources().getProperty(ENABLED).toString());
    radioButtonGroup.add(_enabledRadioButton);
    lineBox.add(_enabledRadioButton);
    _disabledRadioButton.setText(GetResources().getProperty(DISABLED).toString());
    radioButtonGroup.add(_disabledRadioButton);
    lineBox.add(_disabledRadioButton);

    return northPanel;
  }

  @SuppressWarnings("unused")
  @Override
  protected Container CreateCenterPanel()
  {
    for (int i = 0; i < _currencySymbolLabels.length; i++)
    {
      JLabel currencySymbolLabel = new JLabel();
      currencySymbolLabel.setHorizontalAlignment(SwingConstants.LEFT);
      _currencySymbolLabels[i] = currencySymbolLabel;
    }

    JTabbedPane tabbedPane = new JTabbedPane();

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    constraints.gridy = 0;

    // build Site Data Tabbed Pane
    JPanel siteDataPane = new JPanel(new GridBagLayout());
    tabbedPane.addTab(__resources.getProperty(SITE_DATA_TAB), siteDataPane);

    // current release number

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(CURRENT_RELEASE_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
    _releaseNumberSpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(1);
    spinnerNumberModel.setMaximum(MAX_RELEASES);
    _releaseNumberSpinField.setToolTipText(__resources.getProperty(CURRENT_RELEASE_NUMBER_TIP));

    siteDataPane.add(_releaseNumberSpinField, constraints);

    // currency symbol
    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(CURRENCY_SYMBOL), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    siteDataPane.add(_currencySymbolLabels[0], constraints);

    // decimal separator
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(DECIMAL_SEPARATOR), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _periodDecimalSeparatorRadioButton.setText(PERIOD);
    _periodDecimalSeparatorRadioButton.setToolTipText(__resources.getProperty(PERIOD_TIP));
    siteDataPane.add(_periodDecimalSeparatorRadioButton, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _commaDecimalSeparatorRadioButton.setText(COMMA);
    _commaDecimalSeparatorRadioButton.setToolTipText(__resources.getProperty(COMMA_TIP));
    siteDataPane.add(_commaDecimalSeparatorRadioButton, constraints);

    ButtonGroup buttonGroup = new ButtonGroup();
    buttonGroup.add(_periodDecimalSeparatorRadioButton);
    buttonGroup.add(_commaDecimalSeparatorRadioButton);

    // thousands separator
    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(THOUSANDS_SEPARATOR), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _commaThousandsSeparatorRadioButton.setText(COMMA);
    _commaThousandsSeparatorRadioButton.setToolTipText(__resources.getProperty(COMMA_TIP));
    siteDataPane.add(_commaThousandsSeparatorRadioButton, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _periodThousandsSeparatorRadioButton.setText(PERIOD);
    _periodThousandsSeparatorRadioButton.setToolTipText(__resources.getProperty(PERIOD_TIP));
    siteDataPane.add(_periodThousandsSeparatorRadioButton, constraints);

    buttonGroup = new ButtonGroup();
    buttonGroup.add(_periodThousandsSeparatorRadioButton);
    buttonGroup.add(_commaThousandsSeparatorRadioButton);

    // installation passphrase
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(INSTALLATION_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _installationPassphraseTextField.setColumns(20);
    _installationPassphraseTextField.setToolTipText(__resources.getProperty(INSTALLATION_PASSPHRASE_TIP));
    siteDataPane.add(_installationPassphraseTextField, constraints);

    // languages
    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(LANGUAGE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    String[] languages = Resources.GetLanguages();
    String language = GetInstallationRow().Get_language();
    _languageComboBox.RecreateComboBox(languages, language, false);
    _languageComboBox.setEditable(false);
    siteDataPane.add(_languageComboBox, constraints);

    // smtp host
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(SMTP_HOST), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _smtpHostTextField.setToolTipText(__resources.getProperty(SMTP_HOST_TIP));
    _smtpHostTextField.setColumns(HOST_NAME_WIDTH);
    _smtpHostTextField.setHorizontalAlignment(SwingConstants.LEFT);

    siteDataPane.add(_smtpHostTextField, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(ADMINISTRATOR_E_MAIL_ADDRESS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _administratorEMailAddressTextField.setToolTipText(__resources.getProperty(ADMINISTRATOR_E_MAIL_ADDRESS_TIP));
    _administratorEMailAddressTextField.setColumns(EMAIL_ADDRESS_WIDTH);
    _administratorEMailAddressTextField.setHorizontalAlignment(SwingConstants.LEFT);

    siteDataPane.add(_administratorEMailAddressTextField, constraints);

    // max clients
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(MAX_NUMBER_OF_CLIENTS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    spinnerNumberModel = new SpinnerNumberModel();
    _maxClientsSpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(1);
    spinnerNumberModel.setMaximum(MAX_CLIENTS);
    _maxClientsSpinField.setToolTipText(__resources.getProperty(MAX_NUMBER_OF_CLIENTS_TIP));

    siteDataPane.add(_maxClientsSpinField, constraints);

    // max gatekeepers
    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(MAX_NUMBER_OF_GATEKEEPERS), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    spinnerNumberModel = new SpinnerNumberModel();
    _maxGatekeepersSpinField.setModel(spinnerNumberModel);
    spinnerNumberModel.setStepSize(1);
    spinnerNumberModel.setMinimum(1);
    spinnerNumberModel.setMaximum(MAX_GATEKEEPERS);
    _maxGatekeepersSpinField.setToolTipText(__resources.getProperty(MAX_NUMBER_OF_GATEKEEPERS_TIP));

    siteDataPane.add(_maxGatekeepersSpinField, constraints);

    // interim expiration date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(INTERIM_EXPIRATION_DATE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _dateLabel.setToolTipText(__resources.getProperty(INTERIM_EXPIRATION_DATE_TIP));
    _dateChooserButton.setToolTipText(__resources.getProperty(INTERIM_EXPIRATION_DATE_TIP));

    new DateChooserListener(_dateChooserButton, _dateCalendar, _dateCalendar, _dateLabel);

    siteDataPane.add(_dateLabel, constraints);
    siteDataPane.add(_dateChooserButton, constraints);

    // upgrade preference
    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    siteDataPane.add(new JLabel(__resources.getProperty(UPGRADE_PREFERENCE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _automaticRadioButton.setText(__resources.getProperty(AUTOMATIC));
    _automaticRadioButton.setToolTipText(__resources.getProperty(AUTOMATIC_TIP));
    siteDataPane.add(_automaticRadioButton, constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _manualRadioButton.setText(__resources.getProperty(MANUAL));
    _manualRadioButton.setToolTipText(__resources.getProperty(MANUAL_TIP));
    siteDataPane.add(_manualRadioButton, constraints);

    buttonGroup = new ButtonGroup();
    buttonGroup.add(_automaticRadioButton);
    buttonGroup.add(_manualRadioButton);

    // build E-Commerce Tabbed Pane
    JPanel ecommercePane = new JPanel(new GridBagLayout());
    tabbedPane.addTab(__resources.getProperty(ECOMMERCE_TAB), ecommercePane);

    constraints.gridy = 0;

    // top row labels: credit card, e-gold, paypal
    constraints.gridy += 1;

    constraints.gridx = 1;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(CREDITCARD), SwingConstants.LEFT), constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(EGOLD), SwingConstants.LEFT), constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PAYPAL), SwingConstants.LEFT), constraints);

    // Charge URL row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_URL), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargeURLTextField.setColumns(URL_FIELD_WIDTH);
    _creditcardChargeURLTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_URL_TIP));
    ecommercePane.add(_creditcardChargeURLTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargeURLTextField.setColumns(URL_FIELD_WIDTH);
    _egoldChargeURLTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_URL_TIP));
    ecommercePane.add(_egoldChargeURLTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargeURLTextField.setColumns(URL_FIELD_WIDTH);
    _paypalChargeURLTextField.setEnabled(false);
    _paypalChargeURLTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_URL_TIP));
    ecommercePane.add(_paypalChargeURLTextField, constraints);

    // Charge Account Number row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_ACCOUNT_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargeAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _creditcardChargeAccountNumberTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_creditcardChargeAccountNumberTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargeAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _egoldChargeAccountNumberTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_egoldChargeAccountNumberTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargeAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _paypalChargeAccountNumberTextField.setEnabled(false);
    _paypalChargeAccountNumberTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_paypalChargeAccountNumberTextField, constraints);

    // Charge Passphrase row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargePassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _creditcardChargePassphraseTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_PASSPHRASE_TIP));
    ecommercePane.add(_creditcardChargePassphraseTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargePassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _egoldChargePassphraseTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_PASSPHRASE_TIP));
    ecommercePane.add(_egoldChargePassphraseTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargePassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _paypalChargePassphraseTextField.setEnabled(false);
    _paypalChargePassphraseTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_PASSPHRASE_TIP));
    ecommercePane.add(_paypalChargePassphraseTextField, constraints);

    // Charge Min Amount row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_MIN_AMOUNT), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[1], constraints);

    constraints.gridx = 2;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargeMinAmountTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _creditcardChargeMinAmountTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _creditcardChargeMinAmountTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_MIN_AMOUNT_TIP));
    ecommercePane.add(_creditcardChargeMinAmountTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[2], constraints);

    constraints.gridx = 6;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargeMinAmountTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _egoldChargeMinAmountTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _egoldChargeMinAmountTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_MIN_AMOUNT_TIP));
    ecommercePane.add(_egoldChargeMinAmountTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[3], constraints);

    constraints.gridx = 10;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargeMinAmountTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _paypalChargeMinAmountTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _paypalChargeMinAmountTextField.setEnabled(false);
    _paypalChargeMinAmountTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_MIN_AMOUNT_TIP));
    ecommercePane.add(_paypalChargeMinAmountTextField, constraints);

    // Charge Xaction Fee row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_XACTION_FEE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[4], constraints);

    constraints.gridx = 2;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargeXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _creditcardChargeXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _creditcardChargeXactionFeeTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_XACTION_FEE_TIP));
    ecommercePane.add(_creditcardChargeXactionFeeTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[5], constraints);

    constraints.gridx = 6;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargeXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _egoldChargeXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _egoldChargeXactionFeeTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_XACTION_FEE_TIP));
    ecommercePane.add(_egoldChargeXactionFeeTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[6], constraints);

    constraints.gridx = 10;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargeXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _paypalChargeXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _paypalChargeXactionFeeTextField.setEnabled(false);
    _paypalChargeXactionFeeTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_XACTION_FEE_TIP));
    ecommercePane.add(_paypalChargeXactionFeeTextField, constraints);

    // Charge Xaction Fee Percent row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(CHARGE_XACTION_FEE_PERCENT), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardChargeXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _creditcardChargeXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _creditcardChargeXactionFeePercentTextField.setToolTipText(__resources.getProperty(CREDITCARD_CHARGE_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_creditcardChargeXactionFeePercentTextField, constraints);

    constraints.gridx = 3;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldChargeXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _egoldChargeXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _egoldChargeXactionFeePercentTextField.setToolTipText(__resources.getProperty(EGOLD_CHARGE_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_egoldChargeXactionFeePercentTextField, constraints);

    constraints.gridx = 7;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalChargeXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _paypalChargeXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _paypalChargeXactionFeePercentTextField.setEnabled(false);
    _paypalChargeXactionFeePercentTextField.setToolTipText(__resources.getProperty(PAYPAL_CHARGE_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_paypalChargeXactionFeePercentTextField, constraints);

    constraints.gridx = 11;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    // Refund URL row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(REFUND_URL), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardRefundURLTextField.setColumns(URL_FIELD_WIDTH);
    _creditcardRefundURLTextField.setEnabled(false);
    _creditcardRefundURLTextField.setToolTipText(__resources.getProperty(CREDITCARD_REFUND_URL_TIP));
    ecommercePane.add(_creditcardRefundURLTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldRefundURLTextField.setColumns(URL_FIELD_WIDTH);
    _egoldRefundURLTextField.setToolTipText(__resources.getProperty(EGOLD_REFUND_URL_TIP));
    ecommercePane.add(_egoldRefundURLTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalRefundURLTextField.setColumns(URL_FIELD_WIDTH);
    _paypalRefundURLTextField.setEnabled(false);
    _paypalRefundURLTextField.setToolTipText(__resources.getProperty(PAYPAL_REFUND_URL_TIP));
    ecommercePane.add(_paypalRefundURLTextField, constraints);

    // Refund Account Number row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(REFUND_ACCOUNT_NUMBER), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardRefundAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _creditcardRefundAccountNumberTextField.setEnabled(false);
    _creditcardRefundAccountNumberTextField.setToolTipText(__resources.getProperty(CREDITCARD_REFUND_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_creditcardRefundAccountNumberTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldRefundAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _egoldRefundAccountNumberTextField.setToolTipText(__resources.getProperty(EGOLD_REFUND_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_egoldRefundAccountNumberTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalRefundAccountNumberTextField.setColumns(ACCOUNT_NUMBER_FIELD_WIDTH);
    _paypalRefundAccountNumberTextField.setEnabled(false);
    _paypalRefundAccountNumberTextField.setToolTipText(__resources.getProperty(PAYPAL_REFUND_ACCOUNT_NUMBER_TIP));
    ecommercePane.add(_paypalRefundAccountNumberTextField, constraints);

    // Refund Passphrase row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(REFUND_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardRefundPassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _creditcardRefundPassphraseTextField.setEnabled(false);
    _creditcardRefundPassphraseTextField.setToolTipText(__resources.getProperty(CREDITCARD_REFUND_PASSPHRASE_TIP));
    ecommercePane.add(_creditcardRefundPassphraseTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldRefundPassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _egoldRefundPassphraseTextField.setToolTipText(__resources.getProperty(EGOLD_REFUND_PASSPHRASE_TIP));
    ecommercePane.add(_egoldRefundPassphraseTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 4;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalRefundPassphraseTextField.setColumns(PASSPHRASE_FIELD_WIDTH);
    _paypalRefundPassphraseTextField.setEnabled(false);
    _paypalRefundPassphraseTextField.setToolTipText(__resources.getProperty(PAYPAL_REFUND_PASSPHRASE_TIP));
    ecommercePane.add(_paypalRefundPassphraseTextField, constraints);

    // Refund Xaction Fee row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(REFUND_XACTION_FEE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[7], constraints);

    constraints.gridx = 2;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardRefundXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _creditcardRefundXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _creditcardRefundXactionFeeTextField.setEnabled(false);
    _creditcardRefundXactionFeeTextField.setToolTipText(__resources.getProperty(CREDITCARD_REFUND_XACTION_FEE_TIP));
    ecommercePane.add(_creditcardRefundXactionFeeTextField, constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[8], constraints);

    constraints.gridx = 6;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldRefundXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _egoldRefundXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _egoldRefundXactionFeeTextField.setToolTipText(__resources.getProperty(EGOLD_REFUND_XACTION_FEE_TIP));
    ecommercePane.add(_egoldRefundXactionFeeTextField, constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(_currencySymbolLabels[9], constraints);

    constraints.gridx = 10;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalRefundXactionFeeTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _paypalRefundXactionFeeTextField.setColumns(CURRENCY_FIELD_WIDTH);
    _paypalRefundXactionFeeTextField.setEnabled(false);
    _paypalRefundXactionFeeTextField.setToolTipText(__resources.getProperty(PAYPAL_REFUND_XACTION_FEE_TIP));
    ecommercePane.add(_paypalRefundXactionFeeTextField, constraints);

    // Refund Xaction Fee Percent row
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    ecommercePane.add(new JLabel(__resources.getProperty(REFUND_XACTION_FEE_PERCENT), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _creditcardRefundXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _creditcardRefundXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _creditcardRefundXactionFeePercentTextField.setEnabled(false);
    _creditcardRefundXactionFeePercentTextField.setToolTipText(__resources.getProperty(CREDITCARD_REFUND_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_creditcardRefundXactionFeePercentTextField, constraints);

    constraints.gridx = 3;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    constraints.gridx = 5;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _egoldRefundXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _egoldRefundXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _egoldRefundXactionFeePercentTextField.setToolTipText(__resources.getProperty(EGOLD_REFUND_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_egoldRefundXactionFeePercentTextField, constraints);

    constraints.gridx = 7;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    constraints.gridx = 9;
    constraints.gridwidth = 2;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _paypalRefundXactionFeePercentTextField.SetFormat(DECIMAL_FORMAT_MONEY);
    _paypalRefundXactionFeePercentTextField.setColumns(PERCENTAGE_FIELD_WIDTH);
    _paypalRefundXactionFeePercentTextField.setEnabled(false);
    _paypalRefundXactionFeePercentTextField.setToolTipText(__resources.getProperty(PAYPAL_REFUND_XACTION_FEE_PERCENT_TIP));
    ecommercePane.add(_paypalRefundXactionFeePercentTextField, constraints);

    constraints.gridx = 11;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    ecommercePane.add(new JLabel(__resources.getProperty(PERCENT_SIGN), SwingConstants.LEFT), constraints);

    // build Rules Tabbed Pane
    JPanel rulesPane = new JPanel(new GridBagLayout());
    tabbedPane.addTab(__resources.getProperty(RULES_TAB), rulesPane);

    constraints.gridy = 0;
    constraints.gridx = -1;

    for (int i = 0; i < RulesRow.NUMBER_OF_RULES; i++)
    {
      if (constraints.gridx > NUMBER_OF_RULES_ACROSS)
      {
        constraints.gridx = -1;
        constraints.gridy += 1;
      }

      constraints.gridx += 1;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.NONE;
      constraints.anchor = GridBagConstraints.EAST;

      JLabel ruleNameLabel = new JLabel();
      _ruleNameLabels[i] = ruleNameLabel;
      ruleNameLabel.setHorizontalAlignment(SwingConstants.RIGHT);

      rulesPane.add(ruleNameLabel, constraints);

      constraints.gridx += 1;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.NONE;
      constraints.anchor = GridBagConstraints.WEST;

      OverwritableTextField ruleValueTextField = new OverwritableTextField();
      _ruleValueTextFields[i] = ruleValueTextField;
      ruleValueTextField.setColumns(RULE_WIDTH);

      rulesPane.add(ruleValueTextField, constraints);
    }

    return tabbedPane;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    Container container = super.CreateSouthPanel();

    _previousButton.setVisible(false);
    _nextButton.setVisible(false);
    _saveAndNewButton.setVisible(false);

    return container;
  }

  @Override
  protected boolean FieldsValid()
  {
    String installationName = _installationNameTextField.getText().trim();
    if (installationName.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, __resources.getProperty(BLANK_INSTALLATION_NAME_MSG), __resources.getProperty(BLANK_INSTALLATION_NAME_TITLE));
      _installationNameTextField.requestFocus();
      return false;
    }

    return super.FieldsValid();
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    if (ModeIsNew() == true)
    {
      GetInstallationRow().Set_installation_id(GetInstallationsRow().Get_installation_id());
    }

    for (int i = 0; i < _currencySymbolLabels.length; i++)
    {
      _currencySymbolLabels[i].setText(GetInstallationRow().Get_currency_symbol());
    }

    _installationIDLabel.setText(GetInstallationRow().Get_installation_id().toString());
    _installationNameTextField.setText(GetInstallationRow().Get_installation_name());

    _enabledRadioButton.setSelected(GetInstallationRow().StatusIsEnabled());
    _disabledRadioButton.setSelected(GetInstallationRow().StatusIsDisabled());
    _periodDecimalSeparatorRadioButton.setSelected(GetInstallationRow().DecimalSeparatorIsPeriod());
    _commaDecimalSeparatorRadioButton.setSelected(GetInstallationRow().DecimalSeparatorIsComma());
    _commaThousandsSeparatorRadioButton.setSelected(GetInstallationRow().ThousandsSeparatorIsComma());
    _periodThousandsSeparatorRadioButton.setSelected(GetInstallationRow().ThousandsSeparatorIsPeriod());
    _installationPassphraseTextField.setText(GetInstallationRow().Get_installation_passphrase());

    String[] languages = Resources.GetLanguages();
    String language = GetInstallationRow().Get_language();
    _languageComboBox.RecreateComboBox(languages, language, false);

    _smtpHostTextField.setText(GetInstallationRow().Get_smtp_host());
    _administratorEMailAddressTextField.setText(GetInstallationRow().Get_administrator_e_mail_address());

    _maxClientsSpinField.setValue(GetInstallationRow().Get_max_number_of_clients());
    _maxGatekeepersSpinField.setValue(GetInstallationRow().Get_max_number_of_gatekeepers());
    _dateCalendar.SetDate(GetInstallationRow().Get_interim_expiration_date().getTime());
    _dateLabel.setText(_dateCalendar.GetDateString());
    _automaticRadioButton.setSelected(GetInstallationRow().UpgradePreferenceIsAutomatic());
    _manualRadioButton.setSelected(GetInstallationRow().UpgradePreferenceIsManual());
    _releaseNumberSpinField.setValue(GetInstallationRow().Get_current_release_number());
    _creditcardChargeURLTextField.setText(GetInstallationRow().Get_cc_charge_url());
    _egoldChargeURLTextField.setText(GetInstallationRow().Get_egold_charge_url());
    _paypalChargeURLTextField.setText(GetInstallationRow().Get_paypal_charge_url());
    _egoldChargeAccountNumberTextField.setText(GetInstallationRow().Get_egold_charge_account());
    _creditcardChargeAccountNumberTextField.setText(GetInstallationRow().Get_cc_charge_account());
    _paypalChargeAccountNumberTextField.setText(GetInstallationRow().Get_paypal_charge_account());
    _creditcardChargePassphraseTextField.setText(GetInstallationRow().Get_cc_charge_passphrase());
    _egoldChargePassphraseTextField.setText(GetInstallationRow().Get_egold_charge_passphrase());
    _paypalChargePassphraseTextField.setText(GetInstallationRow().Get_paypal_charge_passphrase());
    _creditcardChargeMinAmountTextField.SetValue(GetInstallationRow().Get_cc_charge_minimum_amount());
    _egoldChargeMinAmountTextField.SetValue(GetInstallationRow().Get_egold_charge_minimum_amount());
    _paypalChargeMinAmountTextField.SetValue(GetInstallationRow().Get_paypal_charge_minimum_amount());
    _creditcardChargeXactionFeeTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee());
    _egoldChargeXactionFeeTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee());
    _paypalChargeXactionFeeTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee());
    _creditcardChargeXactionFeePercentTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee_pcent());
    _egoldChargeXactionFeePercentTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee_pcent());
    _paypalChargeXactionFeePercentTextField.SetValue(GetInstallationRow().Get_cc_charge_xaction_fee_pcent());
    _creditcardRefundURLTextField.setText(GetInstallationRow().Get_cc_refund_url());
    _egoldRefundURLTextField.setText(GetInstallationRow().Get_egold_refund_url());
    _paypalRefundURLTextField.setText(GetInstallationRow().Get_paypal_refund_url());
    _creditcardRefundAccountNumberTextField.setText(GetInstallationRow().Get_cc_refund_account());
    _egoldRefundAccountNumberTextField.setText(GetInstallationRow().Get_egold_refund_account());
    _paypalRefundAccountNumberTextField.setText(GetInstallationRow().Get_paypal_refund_account());
    _creditcardRefundPassphraseTextField.setText(GetInstallationRow().Get_cc_refund_passphrase());
    _egoldRefundPassphraseTextField.setText(GetInstallationRow().Get_egold_refund_passphrase());
    _paypalRefundPassphraseTextField.setText(GetInstallationRow().Get_paypal_refund_passphrase());
    _creditcardRefundXactionFeeTextField.SetValue(GetInstallationRow().Get_cc_refund_xaction_fee());
    _egoldRefundXactionFeeTextField.SetValue(GetInstallationRow().Get_egold_refund_xaction_fee());
    _paypalRefundXactionFeeTextField.SetValue(GetInstallationRow().Get_paypal_refund_xaction_fee());
    _creditcardRefundXactionFeePercentTextField.SetValue(GetInstallationRow().Get_cc_refund_xaction_fee_pcent());
    _egoldRefundXactionFeePercentTextField.SetValue(GetInstallationRow().Get_egold_refund_xaction_fee_pcent());
    _paypalRefundXactionFeePercentTextField.SetValue(GetInstallationRow().Get_paypal_refund_xaction_fee_pcent());

    RulesTable rulesTable = GetInstallationTable().GetInstallationDatabase().GetRulesTable();
    _ruleRows = rulesTable.GetRules();
    for (int i = 0; i < RulesRow.NUMBER_OF_RULES; i++)
    {
      RulesRow ruleRow = _ruleRows[i];
      _ruleNameLabels[i].setText(ruleRow.Get_rule_name() + ":");
      _ruleValueTextFields[i].setText(ruleRow.Get_rule_value());
    }

  }

  @Override
  protected void AssignFieldsToRow()
  {
    super.AssignFieldsToRow();

    GetInstallationRow().Set_installation_name(_installationNameTextField.getText());
    GetInstallationRow().Set_installation_passphrase(_installationPassphraseTextField.getText());
    GetInstallationRow().Set_upgrade_preference(_automaticRadioButton.isSelected() ? InstallationRow.UPDATE_AUTOMATIC_NAME : InstallationRow.UPDATE_MANUAL_NAME);
    GetInstallationRow().Set_current_release_number((Integer) _releaseNumberSpinField.getValue());
    GetInstallationRow().Set_smtp_host(_smtpHostTextField.getText());
    GetInstallationRow().Set_administrator_e_mail_address(_administratorEMailAddressTextField.getText());
    GetInstallationRow().Set_interim_expiration_date(_dateCalendar);
    GetInstallationRow().Set_license_expiration_date(GetInstallationsRow().Get_license_expiration_date());
    GetInstallationRow().Set_max_number_of_clients((Integer) _maxClientsSpinField.getValue());
    GetInstallationRow().Set_max_number_of_gatekeepers((Integer) _maxGatekeepersSpinField.getValue());
    GetInstallationRow().Set_language(_languageComboBox.getSelectedItem().toString());
    GetInstallationRow().Set_thousands_separator(_commaThousandsSeparatorRadioButton.isSelected() ? InstallationRow.COMMA : InstallationRow.PERIOD);
    GetInstallationRow().Set_decimal_separator(_periodDecimalSeparatorRadioButton.isSelected() ? InstallationRow.PERIOD : InstallationRow.COMMA);

    GetInstallationRow().Set_egold_charge_url(_egoldChargeURLTextField.getText());
    GetInstallationRow().Set_egold_charge_account(_egoldChargeAccountNumberTextField.getText());
    GetInstallationRow().Set_egold_charge_passphrase(_egoldChargePassphraseTextField.getText());
    GetInstallationRow().Set_egold_charge_minimum_amount(_egoldChargeMinAmountTextField.getText());
    GetInstallationRow().Set_egold_charge_xaction_fee(_egoldChargeXactionFeeTextField.getText());
    GetInstallationRow().Set_egold_charge_xaction_fee_pcent(_egoldChargeXactionFeePercentTextField.getText());
    GetInstallationRow().Set_egold_refund_url(_egoldRefundURLTextField.getText());
    GetInstallationRow().Set_egold_refund_account(_egoldRefundAccountNumberTextField.getText());
    GetInstallationRow().Set_egold_refund_passphrase(_egoldRefundPassphraseTextField.getText());
    GetInstallationRow().Set_egold_refund_xaction_fee(_egoldRefundXactionFeeTextField.getText());
    GetInstallationRow().Set_egold_refund_xaction_fee_pcent(_egoldRefundXactionFeeTextField.getText());
    GetInstallationRow().Set_paypal_charge_url(_paypalChargeURLTextField.getText());
    GetInstallationRow().Set_paypal_charge_account(_paypalChargeAccountNumberTextField.getText());
    GetInstallationRow().Set_paypal_charge_passphrase(_paypalChargePassphraseTextField.getText());
    GetInstallationRow().Set_paypal_charge_minimum_amount(_paypalChargeMinAmountTextField.getText());
    GetInstallationRow().Set_paypal_charge_xaction_fee(_paypalChargeXactionFeeTextField.getText());
    GetInstallationRow().Set_paypal_charge_xaction_fee_pcent(_paypalChargeXactionFeePercentTextField.getText());
    GetInstallationRow().Set_paypal_refund_url(_paypalRefundURLTextField.getText());
    GetInstallationRow().Set_paypal_refund_account(_paypalRefundAccountNumberTextField.getText());
    GetInstallationRow().Set_paypal_refund_passphrase(_paypalRefundPassphraseTextField.getText());
    GetInstallationRow().Set_paypal_refund_xaction_fee(_paypalRefundXactionFeeTextField.getText());
    GetInstallationRow().Set_paypal_refund_xaction_fee_pcent(_paypalRefundXactionFeePercentTextField.getText());
    GetInstallationRow().Set_cc_charge_url(_creditcardChargeURLTextField.getText());
    GetInstallationRow().Set_cc_charge_account(_creditcardChargeAccountNumberTextField.getText());
    GetInstallationRow().Set_cc_charge_passphrase(_creditcardChargePassphraseTextField.getText());
    GetInstallationRow().Set_cc_charge_minimum_amount(_creditcardChargeMinAmountTextField.getText());
    GetInstallationRow().Set_cc_charge_xaction_fee(_creditcardChargeXactionFeeTextField.getText());
    GetInstallationRow().Set_cc_charge_xaction_fee_pcent(_creditcardChargeXactionFeePercentTextField.getText());
    GetInstallationRow().Set_cc_refund_url(_creditcardRefundURLTextField.getText());
    GetInstallationRow().Set_cc_refund_account(_creditcardRefundAccountNumberTextField.getText());
    GetInstallationRow().Set_cc_refund_passphrase(_creditcardRefundPassphraseTextField.getText());
    GetInstallationRow().Set_cc_refund_xaction_fee(_creditcardRefundXactionFeeTextField.getText());
    GetInstallationRow().Set_cc_refund_xaction_fee_pcent(_creditcardRefundXactionFeePercentTextField.getText());

    for (int i = 0; i < _ruleRows.length; i++)
    {
      _ruleRows[i].Set_rule_value(_ruleValueTextFields[i].getText());
    }

  }

  @Override
  protected void EndingSave(boolean rc)
  {
    super.EndingSave(rc);
    if (rc == true)
    {
      GetInstallationsRow().Set_installation_name(GetInstallationRow().Get_installation_name());
      GetInstallationsRow().Set_status(GetInstallationRow().Get_status());
      GetInstallationsRow().Update();

      RulesTable rulesTable = GetInstallationTable().GetInstallationDatabase().GetRulesTable();
      String predicate = RulesRow.Name_installation_id() + " = '" + GetInstallationRow().Get_installation_id() + "'";
      rulesTable.RemoveRows(predicate);
      for (int i = 0; i < _ruleRows.length; i++)
      {
        _ruleRows[i].Insert();
      }
    }

  }

  @Override
  public void dispose()
  {
    GetInstallationTable().GetSQLDatabase().Done();
    super.dispose();
  }

  @Override
  protected void CancelAction()
  {
    if (ModeIsNew() == true)
    {
      GetInstallationsRow().Remove();
    }
    super.CancelAction();
  }

  @Override
  public Resources GetResources()
  {
    return __resources;
  }

  private InstallationsRow GetInstallationsRow()
  {
    if (_installationsRow == null)
    {
      _installationsRow = MonitorDatabase.GetDefaultMonitorDatabase().GetInstallationsTable().GetInstallationsRow(GetInstallationRow().Get_installation_id());
    }
    return _installationsRow;
  }

  private InstallationRow GetInstallationRow()
  {
    return (InstallationRow) _sqlRow;
  }

  private InstallationTable GetInstallationTable()
  {
    return (InstallationTable) _sqlTable;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorRemoteEntryPanel.class.getName());
  }

}
