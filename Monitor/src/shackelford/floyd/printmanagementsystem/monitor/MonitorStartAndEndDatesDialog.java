package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.DateChooserListener;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.common._GregorianCalendar;


/**
 * <p>Title: MonitorStartAndEndDatesDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorStartAndEndDatesDialog
  extends AbstractDialog
{

  /**
   * Eclipse generated value
   */
  private static final long        serialVersionUID                = 5193651908248911387L;

  private final _GregorianCalendar _startCalendar                  = _GregorianCalendar.Now();

  private final JLabel             _startLabel                     = new JLabel();

  private final JButton            _startDateChooserButton         = new JButton();

  private final _GregorianCalendar _endCalendar                    = _GregorianCalendar.Now();

  private final JLabel             _endLabel                       = new JLabel();

  private final JButton            _endDateChooserButton           = new JButton();

  private static Resources         __resources;

  public static final String       START_DATE                      = "START_DATE";

  public static final String       START_DATE_TIP                  = "START_DATE_TIP";

  public static final String       END_DATE                        = "END_DATE";

  public static final String       END_DATE_TIP                    = "END_DATE_TIP";

  public static final String       START_DATE_AFTER_END_DATE_MSG   = "START_DATE_AFTER_END_DATE_MSG";

  public static final String       START_DATE_AFTER_END_DATE_TITLE = "START_DATE_AFTER_END_DATE_TITLE";

  public MonitorStartAndEndDatesDialog()
  {
    super(null, true);
    super.Initialize();
  }

  @SuppressWarnings("unused")
  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // start date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(START_DATE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _startLabel.setToolTipText(GetResources().getProperty(START_DATE_TIP));
    _startDateChooserButton.setToolTipText(GetResources().getProperty(START_DATE_TIP));
    _startDateChooserButton.setIcon(SystemUtilities.GetIcon("Calendar_Popup.gif"));
    centerPanel.add(_startLabel, constraints);
    centerPanel.add(_startDateChooserButton, constraints);

    new DateChooserListener(_startDateChooserButton, _startCalendar, _startCalendar, _startLabel);

    // end date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(END_DATE), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _endLabel.setToolTipText(GetResources().getProperty(END_DATE_TIP));
    _endDateChooserButton.setToolTipText(GetResources().getProperty(END_DATE_TIP));
    _endDateChooserButton.setIcon(SystemUtilities.GetIcon("Calendar_Popup.gif"));
    centerPanel.add(_endLabel, constraints);
    centerPanel.add(_endDateChooserButton, constraints);

    new DateChooserListener(_endDateChooserButton, _endCalendar, _endCalendar, _endLabel);

    return centerPanel;
  }

  @Override
  protected void AssignFields()
  {
    _startCalendar.SetDate(_GregorianCalendar.Now().getTime());
    _startLabel.setText(_startCalendar.GetDateString());

    _endCalendar.SetDate(_GregorianCalendar.Now().getTime());
    _endLabel.setText(_startCalendar.GetDateString());
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(START_DATE, new String(_startCalendar.GetDateString()));
    props.setProperty(END_DATE, new String(_endCalendar.GetDateString()));
    return props;
  }

  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    _GregorianCalendar startDate = new _GregorianCalendar(_startCalendar);
    _GregorianCalendar endDate = new _GregorianCalendar(_endCalendar);

    if (startDate.DaysAfter(endDate) > 0)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(START_DATE_AFTER_END_DATE_MSG), GetResources().getProperty(START_DATE_AFTER_END_DATE_TITLE));
      _startLabel.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorStartAndEndDatesDialog.class.getName());
  }
}
