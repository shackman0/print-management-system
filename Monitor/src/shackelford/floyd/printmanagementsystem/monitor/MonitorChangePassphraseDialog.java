package shackelford.floyd.printmanagementsystem.monitor;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.monitordatabase.MonitorRow;


/**
 * <p>Title: MonitorChangePassphraseDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MonitorChangePassphraseDialog
  extends AbstractDialog
{

  /**
   * Eclipse generated value
   */
  private static final long    serialVersionUID                 = 6783083048875144239L;

  private final MonitorRow     _monitorRow;

  private final JPasswordField _currentPassphrase               = new JPasswordField();

  private final JPasswordField _newMonitorPassphrase            = new JPasswordField();

  private final JPasswordField _newMonitorPassphraseAgain       = new JPasswordField();

  private static Resources     __resources;

  public static final String   CURRENT_MONITOR_PASSPHRASE       = "CURRENT_MONITOR_PASSPHRASE";

  private static final String  CURRENT_MONITOR_PASSPHRASE_TIP   = "CURRENT_MONITOR_PASSPHRASE_TIP";

  private static final String  INVALID_PASSPHRASE_MSG_TEXT      = "INVALID_PASSPHRASE_MSG_TEXT";

  private static final String  INVALID_PASSPHRASE_TITLE         = "INVALID_PASSPHRASE_TITLE";

  public static final String   NEW_MONITOR_PASSPHRASE           = "NEW_MONITOR_PASSPHRASE";

  private static final String  NEW_MONITOR_PASSPHRASE_TIP       = "NEW_MONITOR_PASSPHRASE_TIP";

  private static final String  NEW_MONITOR_PASSPHRASE_AGAIN     = "NEW_MONITOR_PASSPHRASE_AGAIN";

  private static final String  NEW_MONITOR_PASSPHRASE_AGAIN_TIP = "NEW_MONITOR_PASSPHRASE_AGAIN_TIP";

  private static final String  NON_MATCHING_PASSPHRASE_MSG_TEXT = "NON_MATCHING_PASSPHRASE_MSG_TEXT";

  private static final String  NON_MATCHING_PASSPHRASE_TITLE    = "NON_MATCHING_PASSPHRASE_TITLE";

  private static final int     PASSPHRASE_WIDTH                 = 20;

  public MonitorChangePassphraseDialog(MonitorRow monitorRow)
  {
    super(null, true);
    _monitorRow = monitorRow;
    Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    // build center panel

    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // current password
    constraints.gridy = 0;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(CURRENT_MONITOR_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _currentPassphrase.setColumns(PASSPHRASE_WIDTH);
    _currentPassphrase.setToolTipText(__resources.getProperty(CURRENT_MONITOR_PASSPHRASE_TIP));
    centerPanel.add(_currentPassphrase, constraints);

    // new password
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_MONITOR_PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newMonitorPassphrase.setColumns(PASSPHRASE_WIDTH);
    _newMonitorPassphrase.setToolTipText(__resources.getProperty(NEW_MONITOR_PASSPHRASE_TIP));
    centerPanel.add(_newMonitorPassphrase, constraints);

    // new monitor passphrase again
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(__resources.getProperty(NEW_MONITOR_PASSPHRASE_AGAIN), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _newMonitorPassphraseAgain.setColumns(PASSPHRASE_WIDTH);
    _newMonitorPassphraseAgain.setToolTipText(__resources.getProperty(NEW_MONITOR_PASSPHRASE_AGAIN_TIP));
    centerPanel.add(_newMonitorPassphraseAgain, constraints);

    return centerPanel;
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(CURRENT_MONITOR_PASSPHRASE, new String(_currentPassphrase.getPassword()));
    props.setProperty(NEW_MONITOR_PASSPHRASE, new String(_newMonitorPassphrase.getPassword()));
    return props;
  }

  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    // check to see if the current passphrase is valid
    String passphrase = new String(_currentPassphrase.getPassword());
    String newPassphrase = new String(_newMonitorPassphrase.getPassword());
    String newPassphraseAgain = new String(_newMonitorPassphraseAgain.getPassword());

    if (passphrase.equals(_monitorRow.Get_passphrase()) == false)
    {
      JOptionPane.showMessageDialog(this, __resources.getProperty(INVALID_PASSPHRASE_TITLE), __resources.getProperty(INVALID_PASSPHRASE_MSG_TEXT), JOptionPane.ERROR_MESSAGE, null);
      _currentPassphrase.setText("");
      _currentPassphrase.repaint();
      _currentPassphrase.requestFocus();
      return false;
    }

    // check to see if the new passphrases match
    if (newPassphrase.equals(newPassphraseAgain) == false)
    {
      JOptionPane.showMessageDialog(this, __resources.getProperty(NON_MATCHING_PASSPHRASE_TITLE), __resources.getProperty(NON_MATCHING_PASSPHRASE_MSG_TEXT), JOptionPane.ERROR_MESSAGE, null);
      _newMonitorPassphrase.setText("");
      _newMonitorPassphrase.repaint();
      _newMonitorPassphraseAgain.setText("");
      _newMonitorPassphraseAgain.repaint();
      _newMonitorPassphrase.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(MonitorChangePassphraseDialog.class.getName());
  }

}
