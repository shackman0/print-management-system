#! /bin/bash

#
# Be sure you have write permission in the /usr/local directory.
# you may want to "su root" to run this script.
#

# set INSTALLDIR to the installation destination

INSTALLDIR=/usr/local/UpgradeServer

# clean up any DOS residuals
dos2unix ./UpgradeServer.policy            >/dev/null
dos2unix ./UpgradeServer.sh                >/dev/null
dos2unix ./UpgradeServer_rc.local          >/dev/null


# create the installation directory
mkdir -p ${INSTALLDIR}

# copy files to the appropriate locations
cp -p * ${INSTALLDIR}/.

chmod a+rwx  ${INSTALLDIR}
chmod a+rw   ${INSTALLDIR}/*
chmod a-x    ${INSTALLDIR}/*
chmod a+x    ${INSTALLDIR}/*.sh

cat /etc/rc.d/rc.local ./UpgradeServer_rc.local > ./rc.local
mv -f ./rc.local /etc/rc.d/rc.local

