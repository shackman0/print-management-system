@echo off

rem This script launches the UpgradeServer application.
rem You must modify this script when you install the
rem UpgradeServer application before this script will work.

rem Set the JAVA variable to point to where the Java Virtual Machine
rem and supporting files are located. Use an absolute path.

set JAVA=\Program Files\JavaSoft\JRE\1.3.1_02

rem Set the ZIPFILES parm to the path to the zip files
rem You must include a trailing slash at the end of path

set ZIPFILES=c:\cvs\PayToPrint\Install\DigiNet_LX\

rem ********** DO NOT MODIFY ANYTHING AFTER THIS LINE **********

if not "%JAVA%". == "". goto 1continue

echo ERROR: The JAVA variable is not set.
echo   The JAVA variable must be properly set before running this
echo   script.

goto end

:1continue

if not %ZIPFILES%. == . goto 99continue

echo ERROR: The ZIPFILES variable is not set.
echo   The ZIPFILES variable must be properly set before running this
echo   script.

goto end

:99continue

"%JAVA%\bin\java" -classpath .;"%JAVA%\lib";.\UpgradeServer.jar; UpgradeServerImpl -zipFiles %ZIPFILES% -verbose

:end

set JAVA=
set ZIPFILES=
