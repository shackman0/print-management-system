#! /bin/bash

# UpgradeServer requires the following parms:
#
#   -zipFiles <path to zip files>
#

# This script launches the UpgradeServer java script
# You must modify this script before this script will work.

# Set the JDKTOP variable to point to where the Java Virtual Machine
# and supporting files are located. Use an absolute path.
# be sure to preserve the trailing slashes as shown.

INSTALLDIR=/usr/local/UpgradeServer/
ZIPFILES=${INSTALLDIR}
JDKTOP=/usr/local/java2/
JDKLIB=${JDKTOP}/lib/
CLASSPATH=${INSTALLDIR}UpgradeServer.jar:${JDKLIB}:

${JDKTOP}bin/java -classpath ${CLASSPATH} -Djava.security.policy=${INSTALLDIR}UpgradeServer.policy UpgradeServerImpl -zipFiles ${ZIPFILES} 2>&1 1>/var/log/UpgradeServer.log

