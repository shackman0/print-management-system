package shackelford.floyd.printmanagementsystem.upgradeserver;

import java.io.File;
import java.io.FileInputStream;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Calendar;
import java.util.GregorianCalendar;

import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;

/**
 * <p>Title: UpgradeServerImpl</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class UpgradeServerImpl
  extends java.rmi.server.UnicastRemoteObject
  implements UpgradeServer
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID = 1078102050238024153L;

  private static final String APPLICATION_NAME = "UpgradeServer";

  private UpgradeServerImpl()
    throws RemoteException
  {
    // clients: use the NewUpgradeServer interface
  }

  @Override
  protected void finalize()
    throws RemoteException
  {
    if (_didDone == false)
    {
      Done();
    }
  }

  /**
  */
  @Override
  public UpgradeServer NewUpgradeServer()
    throws RemoteException
  {
    UpgradeServerImpl UpgradeServer = new UpgradeServerImpl();
    return UpgradeServer;
  }

  @Override
  public byte[] GetZipFileForApplication(String applicationName)
    throws RemoteException
  {
    try
    {
      File file = new File(__zipFiles + applicationName.trim() + ".zip");
      if (GlobalAttributes.__verbose == true)
      {
        GregorianCalendar calendar = new GregorianCalendar();
        String dateTimeStamp = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH) + 1) + "-" + calendar.get(Calendar.DAY_OF_MONTH) + " at " + calendar.get(Calendar.HOUR_OF_DAY) + ":"
          + calendar.get(Calendar.MINUTE) + ":" + calendar.get(Calendar.SECOND);
        System.out.println("Sending " + file.getAbsolutePath() + " on " + dateTimeStamp);
      }
      int fileLength = (int) (file.length());
      byte[] fileBytes = new byte[fileLength];
      try
      (FileInputStream infile = new FileInputStream(file);)
      {
        infile.read(fileBytes);
      }

      return fileBytes;
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      throw new RemoteException("" + excp);
    }
  }

  @Override
  public void Done()
    throws RemoteException
  {
    if (_didDone == false)
    {
      _didDone = true;
    }
  }

  // I need to know if Done() got called when the finalize method gets called
  private boolean       _didDone   = false;

  private static String __zipFiles = null;

  /**
  */
  public static void main(String[] args)
  {
    GlobalAttributes.__applicationName = APPLICATION_NAME;
    GlobalAttributes.__mainClass = UpgradeServerImpl.class;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-zipFiles") == true)
      {
        __zipFiles = args[++index];
        System.out.println("Zip files are in " + __zipFiles);
      }
      else
        if (args[index].equalsIgnoreCase("-verbose") == true)
        {
          GlobalAttributes.__verbose = true;
          System.out.println("Verbose output");
        }
        else
        {
          System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
        }
    }

    if (__zipFiles == null)
    {
      System.out.println("ERROR: -zipFiles was not specified");
      Usage();
      System.exit(1);
    }

    // bind server to registry
    try
    {
      UpgradeServerImpl UpgradeServer = new UpgradeServerImpl();
      // Bootstrap registry service
      Registry reg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
      reg.rebind("UpgradeServer", UpgradeServer);
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      System.exit(1);
    }

    try
    {
      String[] bindings = Naming.list("localhost");
      System.out.println("Servers currently bound at this host:");
      for (int i = 0; i < bindings.length; i++)
      {
        System.out.println("  " + bindings[i]);
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    System.out.println("UpgradeServer is running ...");
  }

  private static void Usage()
  {
    System.err.println("Usage e.g.: java -Djdbc.drivers=org.postgresql.Driver " + "-zipFiles <path to the zip files> [-verbose]");
  }

}
