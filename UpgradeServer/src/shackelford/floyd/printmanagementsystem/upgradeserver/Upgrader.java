package shackelford.floyd.printmanagementsystem.upgradeserver;

import java.io.File;
import java.io.FileOutputStream;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.util.Properties;

import javax.swing.JOptionPane;

import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationRow;


/**
 * <p>Title: Upgrader</p>
 * <p>Description:
  this class is used to load language specific Upgrade.

  note that all resource files need to end with the Upgrade extension.
  as in "LoginDialog.Upgrade". Resource file names are case sensitive.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Upgrader
{

  private final String        _applicationName;

  private final String        _installationAddress;

  private static Properties   __resources;

  private static final String UPGRADING_NOW_MSG   = "UPGRADING_NOW_MSG";

  private static final String UPGRADING_NOW_TITLE = "UPGRADING_NOW_TITLE";

  /**
   * this is the preferred method to call to check for an upgrade
   */
  public static void CheckForUpgrade(InstallationRow installationRow, String installationServerIPAddress, String applicationName)
  {
    // see if we need to upgrade
    int installationReleaseNumber = installationRow.Get_current_release_number().intValue();
    if (installationReleaseNumber > GlobalAttributes.AUTOMATIC_UPGRADE_NUMBER.intValue())
    {
      if (installationRow.UpgradePreferenceIsAutomatic() == true)
      {
        // create an upgrader object
        Upgrader upgrader = new Upgrader(installationServerIPAddress, applicationName);
        // have it stage all the upgrade files for the next time we run
        upgrader.DoUpgrade();
        // launch this application again ...
        SystemUtilities.LaunchApplication(applicationName);
        // ... because we're getting out of here
        System.exit(0);
      }
    }
  }

  public Upgrader(String installationAddress, String applicationName)
  {
    _applicationName = applicationName.trim();
    _installationAddress = installationAddress.trim();

  }

  public void DoUpgrade()
  {
    try
    {
      // let the user know we're upgrading the application
      MsgBox msgBox = new MsgBox(Upgrader.__resources.getProperty(Upgrader.UPGRADING_NOW_MSG), Upgrader.__resources.getProperty(Upgrader.UPGRADING_NOW_TITLE));

      // get the latest .zip file from the upgrade service on the installation server
      System.setSecurityManager(new RMISecurityManager());
      String upgradeServerURL = "rmi://" + _installationAddress.trim() + "/UpgradeServer";
      UpgradeServer upgradeServer = ((UpgradeServer) Naming.lookup(upgradeServerURL)).NewUpgradeServer();
      byte[] fileBytes = upgradeServer.GetZipFileForApplication(_applicationName);

      String zipFileName = _applicationName + ".zip";
      File file = new File(GlobalAttributes.INSTALL_DIR + zipFileName);
      try (FileOutputStream outfile = new FileOutputStream(file);)
      {
        outfile.write(fileBytes);
      }

      upgradeServer.Done();

      // unzip the upgrade zip file
      file = new File(GlobalAttributes.INSTALL_DIR);
      String commandLine = "\"" + GlobalAttributes.INSTALL_DIR + "Info-Zip/unzip.exe\" -qq -o \"" + GlobalAttributes.INSTALL_DIR + _applicationName + ".zip\" -d \"" + GlobalAttributes.INSTALL_DIR
        + "\"";
      DebugWindow.DisplayText("commandLine=" + commandLine);
      Process process = Runtime.getRuntime().exec(commandLine, null, file);

      // wait for exit code -- if it's 0, command worked
      int exitCode = process.waitFor();
      if (exitCode != 0)
      {
        JOptionPane.showMessageDialog(msgBox, "error " + String.valueOf(exitCode) + " returned from " + commandLine, "UnZip Error", JOptionPane.ERROR_MESSAGE);
      }

      // dismiss our dialog to the user
      msgBox.dispose();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }
  }

  /**
   * unit test code
   */
  public static void main(String[] args)
  {
    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-resDir") == true)
      {
        GlobalAttributes.__resourcesDirectory = args[++index];
        System.out.println("Resource files are located under directory \"" + GlobalAttributes.__resourcesDirectory + "\"");
      }
      else
      {
        System.out.println("Ignoring unknown parameter: \"" + args[index] + "\"");
      }
    }

    if (GlobalAttributes.__resourcesDirectory == null)
    {
      System.out.println("ERROR: -resDir was not specified");
      Upgrader.Usage();
      System.exit(1);
    }

    try
    {
      Upgrader upgrader = new Upgrader("CMSInstallation", "MaintainUserAccount");
      upgrader.DoUpgrade();
      upgrader = null;
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }
  }

  private static void Usage()
  {
    System.err.println("Usage e.g.: java " + "-Djava.security.policy=Upgrader.policy Upgrader " + "-resDir <path to resource files>");
  }

  static
  {
    Upgrader.__resources = Resources.CreateResources("UpgradeServer", "Upgrader");
  }
}
