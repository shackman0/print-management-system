package shackelford.floyd.printmanagementsystem.upgradeserver;


import java.rmi.Remote;
import java.rmi.RemoteException;


/**
 * <p>Title: UpgradeServer</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface UpgradeServer
  extends Remote
{

  /**
  This operation gives you your own instance of the UpgradeServer.
  */
  public UpgradeServer NewUpgradeServer()
    throws RemoteException;

  public byte[] GetZipFileForApplication (
    String applicationName )
    throws RemoteException;

  /**
  This operation notifies the server that you are done with this instance for now.
  */
  public void Done()
    throws RemoteException;

}