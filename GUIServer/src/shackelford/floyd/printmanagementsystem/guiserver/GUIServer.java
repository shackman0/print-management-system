package shackelford.floyd.printmanagementsystem.guiserver;


import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Properties;


/**
 * <p>Title: GUIServer</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface GUIServer
  extends Remote
{
  /**
  These fields are used as keys to the properties objects returned by the dialogs.
  */
  public static final String USER_ACCOUNT_NUMBER = "USER_ACCOUNT_NUMBER";
  public static final String USER_ACCOUNT_PASSPHRASE = "USER_ACCOUNT_PASSPHRASE";
  public static final String OK = "OK";
  public static final String CANCEL = "CANCEL";
  public static final String NAME_ON_CARD = "NAME_ON_CARD";
  public static final String BILLING_ADDRESS = "BILLING_ADDRESS";
  public static final String CREDIT_CARD_NUMBER = "CREDIT_CARD_NUMBER";
  public static final String EXPIRATION_MONTH = "EXPIRATION_MONTH";
  public static final String EXPIRATION_YEAR = "EXPIRATION_YEAR";


  /**
  This operation gives you your own instance of the GUIServer.
  */
  public GUIServer NewGUIServer()
    throws RemoteException;

  /**
  This operation displays the PrintJobInformationEntryDialog. It returns a
  properties object containing the state of the dialog fields at exit. the following
  fields are returned:
    USER_ACCOUNT_NUMBER
    USER_ACCOUNT_PASSPHRASE
    OK
    CANCEL
  Installation specific fields may also be included.
  */

  public Properties DisplayPrintJobInformationEntryDialog (
    String printJobNumber,
    String numberOfPages,
    String printer,
    String currencySymbol,
    String printJobCost )
    throws RemoteException;

  /**
  This operation displays the PrintJobInformationEntryDialog. It returns a
  properties object containing the state of the dialog fields at exit. the following
  fields are returned:
    NAME_ON_CARD
    BILLING_ADDRESS
    CREDIT_CARD_NUMBER
    EXPIRATION_MONTH
    EXPIRATION_YEAR
    OK
    CANCEL
  Installation specific fields may also be included.
  */
  public Properties DisplayGuestPrintJobInformationEntryDialog (
    String printJobNumber,
    String numberOfPages,
    String printer,
    String currencySymbol,
    String printJobCost )
    throws RemoteException;


  /**
  This operation displays the PrintJobReleasedMsgBox.
  It is non-blocking.
  */
  public void DisplayPrintJobReleasedMsgBox (
    String printJobNumber,
    String printer,
    String currencySymbol,
    String newBalance,
    String jobCost )
    throws RemoteException;

  /**
  This operation displays the PrintJobHeldMsgBox.
  It is non-blocking.
  */
  public void DisplayPrintJobHeldMsgBox (
    String printJobNumber,
    String printer )
    throws RemoteException;

  /**
  This operation displays the PrintJobBeingProcessedMsgBox.
  It is non-blocking.
  */
  public void DisplayPrintJobBeingProcessedMsgBox (
    String printJobNumber,
    String printer )
    throws RemoteException;

  /**
  This operation dismisses the PrintJobBeingProcessedMsgBox.
  It is non-blocking.
  */
  public void DismissPrintJobBeingProcessedMsgBox (
    String printJobNumber,
    String printer )
    throws RemoteException;

  /**
  This operation displays the InsufficientPrePaymentBalanceMsgBox.
  It is non-blocking.
  */
  public void DisplayInsufficientPrePaymentBalanceMsgBox (
    String printJobNumber,
    String currencySymbol,
    String printJobCost,
    String prePaymentBalance,
    String printer )
    throws RemoteException;

  /**
  This operation displays the DisplayReceivingPrintJobMsgBox.
  It is non-blocking.
  */
  public void DisplayReceivingPrintJobMsgBox()
    throws RemoteException;

  /**
  This operation displays the DismissReceivingPrintJobMsgBox.
  It is non-blocking.
  */
  public void DismissReceivingPrintJobMsgBox()
    throws RemoteException;

}