package shackelford.floyd.printmanagementsystem.guiserver;

import java.text.MessageFormat;

import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: ReceivingPrintJobMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReceivingPrintJobMsgBox
  extends MsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = -546025127817957573L;

  /**
   * 
   */
  

  private static Resources        __resources;

  public static final String      MSG_1 = "MSG_1";

  public ReceivingPrintJobMsgBox()
  {
    super();

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { };
    String msgString = msgFormat.format(substitutionValues).trim();

    Initialize(msgString,__resources.getProperty(TITLE));
  }

  static
  {
    __resources = Resources.CreateApplicationResources(ReceivingPrintJobMsgBox.class.getName());
  }

}