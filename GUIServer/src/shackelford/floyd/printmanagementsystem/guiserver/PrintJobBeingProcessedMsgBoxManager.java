package shackelford.floyd.printmanagementsystem.guiserver;

import java.util.HashMap;

import shackelford.floyd.printmanagementsystem.common.PanelListener;


/**
 * <p>Title: PrintJobBeingProcessedMsgBoxManager</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobBeingProcessedMsgBoxManager
  implements PanelListener
{

  private final String                                 _hashKey;

  static HashMap<String, PrintJobBeingProcessedMsgBox> __msgBoxes = new HashMap<>();

  public PrintJobBeingProcessedMsgBoxManager(String printJobNumber, String printer)
  {
    PrintJobBeingProcessedMsgBox msgBox = new PrintJobBeingProcessedMsgBox(printJobNumber, printer);
    msgBox.AddPanelListener(this);
    msgBox.setVisible(true);
    _hashKey = GenerateHashKey(printJobNumber, printer);
    __msgBoxes.put(_hashKey, msgBox);
  }

  /**
    treat the notice that the panel is shutting down as a close request.
  */
  @Override
  public void EndOfPanel(char action)
  {
    __msgBoxes.remove(_hashKey);
  }

  @Override
  public void PanelStateChange(char action)
  {
    // do nothing
  }

  public static void DismissPanel(String printJobNumber, String printer)
  {
    String hashKey = GenerateHashKey(printJobNumber, printer);
    PrintJobBeingProcessedMsgBox msgBox = __msgBoxes.get(hashKey);
    if (msgBox != null)
    {
      msgBox.dispose();
      __msgBoxes.remove(hashKey);
    }
  }

  private static String GenerateHashKey(String printJobNumber, String printer)
  {
    return new String(printJobNumber.trim() + "," + printer.trim());
  }

}
