package shackelford.floyd.printmanagementsystem.guiserver;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: PrintJobReleasedMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobReleasedMsgBox
  extends MsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = -7097012387417102874L;
  /**
   * 
   */
  
  @SuppressWarnings("unused")
  private String                       _printJobNumber;
  @SuppressWarnings("unused")
  private String                       _printer;
  private String                       _newBalance;
  private String                       _jobCost;
  private String                       _currencySymbol;

  private static Resources             __resources;

  private static final DecimalFormat   __formatter = new DecimalFormat("#,##0.00");

  private static final String          CURRENT_BALANCE = "CURRENT_BALANCE";
  private static final String          JOB_COST = "JOB_COST";
  private static final String          PREVIOUS_BALANCE = "PREVIOUS_BALANCE";
  private static final String          MSG_1 = "MSG_1";


  public PrintJobReleasedMsgBox (
    String  printJobNumber,
    String  printer,
    String  currencySymbol,
    String  newBalance,
    String  jobCost )
  {
    super();

    _printJobNumber = printJobNumber;
    _printer = printer;
    _currencySymbol = currencySymbol;
    _newBalance = newBalance;
    _jobCost = jobCost;

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MSG_1).toString());
    Object[] substitutionValues = new Object[] { printJobNumber,
                                                 printer };
    String msgString = msgFormat.format(substitutionValues).trim();

    Initialize(msgString,__resources.getProperty(TITLE));
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // previous balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(PREVIOUS_BALANCE).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    Number parsedObj = null;

    double jobCost = 0d;
    double newBalance = 0d;

    try
    {
      parsedObj = (Number) __formatter.parseObject(_jobCost);
      jobCost = parsedObj.doubleValue();

      parsedObj = (Number) __formatter.parseObject(_newBalance);
      newBalance = parsedObj.doubleValue();
    }
    catch (ParseException excp)
    {
      excp.printStackTrace();
    }

    double previousBalance = jobCost + newBalance;
    centerPanel.add (
      new JLabel(_currencySymbol + __formatter.format(previousBalance),SwingConstants.LEFT),
      constraints );

    // job cost
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(JOB_COST).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add (
      new JLabel(_currencySymbol + _jobCost,SwingConstants.LEFT),
      constraints );

    // new balance
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(CURRENT_BALANCE).toString(),SwingConstants.RIGHT),
        constraints );

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add (
      new JLabel(_currencySymbol + _newBalance,SwingConstants.LEFT),
      constraints );

    return centerPanel;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PrintJobReleasedMsgBox.class.getName());
  }

}