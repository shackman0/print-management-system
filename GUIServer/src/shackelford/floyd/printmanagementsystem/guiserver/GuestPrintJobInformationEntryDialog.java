package shackelford.floyd.printmanagementsystem.guiserver;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Properties;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.NumericTextField;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: GuestPrintJobInformationEntryDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GuestPrintJobInformationEntryDialog
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = 3148119778417820343L;
  /**
   * 
   */
  
  private OverwritableTextField        _nameOnCardField = new OverwritableTextField();
  private OverwritableTextField        _billingAddressField = new OverwritableTextField();
  private OverwritableTextField        _creditCardNumberField = new OverwritableTextField();
  private NumericTextField             _expirationDateMonthField = new NumericTextField();
  private NumericTextField             _expirationDateYearField = new NumericTextField();
  private String                       _printJobNumber;
  private String                       _numberOfPages;
  private String                       _printer;
  private String                       _currencySymbol;
  private String                       _printJobCost;

  private static Resources             __resources;

  private static final DecimalFormat   __formatter = new DecimalFormat("#,##0.00");

  private static final String     MSG_1 = "MSG_1";
  private static final String     MSG_2 = "MSG_2";
  public static final String      NAME_ON_CARD = "NAME_ON_CARD";
  private static final String     NAME_ON_CARD_TIP = "NAME_ON_CARD_TIP";
  public static final String      BILLING_ADDRESS = "BILLING_ADDRESS";
  private static final String     BILLING_ADDRESS_TIP = "BILLING_ADDRESS_TIP";
  public static final String      CREDIT_CARD_NUMBER = "CREDIT_CARD_NUMBER";
  private static final String     CREDIT_CARD_NUMBER_TIP = "CREDIT_CARD_NUMBER_TIP";
  private static final String     PAYMENT_INFORMATION = "PAYMENT_INFORMATION";
  public static final String      EXPIRATION_DATE = "EXPIRATION_DATE";
  private static final String     EXPIRATION_DATE_MONTH = "EXPIRATION_DATE_MONTH";
  private static final String     EXPIRATION_DATE_MONTH_TIP = "EXPIRATION_DATE_MONTH_TIP";
  private static final String     EXPIRATION_DATE_YEAR = "EXPIRATION_DATE_YEAR";
  private static final String     EXPIRATION_DATE_YEAR_TIP = "EXPIRATION_DATE_YEAR_TIP";
  private static final String     INVALID_CREDIT_CARD_INFO_MSG = "INVALID_CREDIT_CARD_INFO_MSG";
  private static final String     INVALID_CREDIT_CARD_INFO_TITLE = "INVALID_CREDIT_CARD_INFO_TITLE";
//  private static final String     CREDIT_CARD_CHARGE_DENIED_MSG = "CREDIT_CARD_CHARGE_DENIED_MSG";
//  private static final String     CREDIT_CARD_CHARGE_DENIED_TITLE = "CREDIT_CARD_CHARGE_DENIED_TITLE";


  public GuestPrintJobInformationEntryDialog (
    String printJobNumber,
    String numberOfPages,
    String printer,
    String currencySymbol,
    String printJobCost )
  {
    super(null, true);

    _printJobNumber = printJobNumber;
    _numberOfPages = numberOfPages;
    _printer = printer;
    _currencySymbol = currencySymbol;
    _printJobCost = printJobCost;

    DebugWindow.DisplayText("GuestPrintJobInformationEntryDialog:");
    DebugWindow.DisplayText("\tprintJobNumber = " + _printJobNumber);
    DebugWindow.DisplayText("\tnumberOfPages = " + _numberOfPages);
    DebugWindow.DisplayText("\tprinter = " + _printer);
    DebugWindow.DisplayText("\tcurrencySymbol = " + _currencySymbol);
    DebugWindow.DisplayText("\tprintJobCost = " + _printJobCost);

    Initialize();
  }

  @Override
  protected Container CreateNorthPanel()
  {
    Box northPanel = new Box(BoxLayout.Y_AXIS);

    int cols = 40;

    double printJobCost = 0d;

    try
    {
      Number parsedObj = (Number) __formatter.parseObject(_printJobCost);
      printJobCost = parsedObj.doubleValue();
    }
    catch (ParseException excp)
    {
      excp.printStackTrace();
    }

    MessageFormat msg1Format = new MessageFormat(__resources.getProperty(MSG_1).toString());
    Object[] substitutionValues = new Object[] { _currencySymbol,
                                                 _printJobNumber,
                                                 _numberOfPages,
                                                 _printer,
                                                 new Double(printJobCost) };
    String msg1String = msg1Format.format(substitutionValues).trim();
    int rows = (msg1String.length() / cols) + 1;

    JTextArea textArea = new JTextArea(msg1String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(northPanel.getBackground());
    northPanel.add(textArea);

    // message 2 fields:
    //   none

    String msg2String = __resources.getProperty(MSG_2).toString().trim();
    rows = (msg2String.length() / cols);

    textArea = new JTextArea(msg2String, rows, cols);
    textArea.setLineWrap(true);
    textArea.setWrapStyleWord(true);
    textArea.setEditable(false);
    textArea.setBackground(northPanel.getBackground());
    northPanel.add(textArea);

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());
    centerPanel.setBorder(BorderFactory.createEtchedBorder());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // payment information
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    centerPanel.add(new JLabel(__resources.getProperty(PAYMENT_INFORMATION).toString()), constraints);

    // name on card
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(NAME_ON_CARD).toString(), SwingConstants.RIGHT),
      constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _nameOnCardField.setToolTipText(__resources.getProperty(NAME_ON_CARD_TIP).toString());
    _nameOnCardField.setColumns(FIELD_WIDTH);
    _nameOnCardField.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_nameOnCardField, constraints);

    // billing address
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(BILLING_ADDRESS).toString(), SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _billingAddressField.setToolTipText(__resources.getProperty(BILLING_ADDRESS_TIP).toString());
    _billingAddressField.setColumns(FIELD_WIDTH);
    _billingAddressField.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_billingAddressField, constraints);

    // credit card number
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(CREDIT_CARD_NUMBER).toString(), SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 5;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _creditCardNumberField.setToolTipText(__resources.getProperty(CREDIT_CARD_NUMBER_TIP).toString());
    _creditCardNumberField.setColumns(FIELD_WIDTH);
    _creditCardNumberField.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_creditCardNumberField, constraints);

    // expiration date
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(EXPIRATION_DATE).toString(), SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(EXPIRATION_DATE_MONTH).toString(), SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 2;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _expirationDateMonthField.setToolTipText(__resources.getProperty(EXPIRATION_DATE_MONTH_TIP).toString());
    _expirationDateMonthField.setColumns(4);
    _expirationDateMonthField.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_expirationDateMonthField, constraints);

    constraints.gridx = 3;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(__resources.getProperty(EXPIRATION_DATE_YEAR).toString(), SwingConstants.RIGHT),
      constraints );

    constraints.gridx = 4;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _expirationDateYearField.setToolTipText(__resources.getProperty(EXPIRATION_DATE_YEAR_TIP).toString());
    _expirationDateYearField.setColumns(4);
    _expirationDateYearField.setHorizontalAlignment(SwingConstants.LEFT);
    centerPanel.add(_expirationDateYearField, constraints);

    return centerPanel;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  @Override
  protected Properties GetProperties()
  {
    Properties properties = super.GetProperties();

    properties.setProperty(NAME_ON_CARD,_nameOnCardField.getText().trim());
    properties.setProperty(BILLING_ADDRESS,_billingAddressField.getText().trim());
    properties.setProperty(CREDIT_CARD_NUMBER,_creditCardNumberField.getText().trim());
    properties.setProperty(EXPIRATION_DATE,_expirationDateMonthField.getText().trim() + "-" + _expirationDateYearField.getText().trim());

    return properties;
  }

  @Override
  protected boolean PanelValid()
  {
    String nameOnCard = _nameOnCardField.getText().trim();
    if (nameOnCard.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_MSG).toString(),
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_TITLE).toString() );
      _nameOnCardField.requestFocus();
      return false;
    }

    String billingAddress = _billingAddressField.getText().trim();
    if (billingAddress.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_MSG).toString(),
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_TITLE).toString() );
      _billingAddressField.requestFocus();
      return false;
    }

    String creditCardNumber = _creditCardNumberField.getText().trim();
    if (creditCardNumber.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_MSG).toString(),
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_TITLE).toString() );
      _creditCardNumberField.requestFocus();
      return false;
    }

    String expirationDateMonth = _expirationDateMonthField.getText().trim();
    if (expirationDateMonth.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_MSG).toString(),
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_TITLE).toString() );
      _expirationDateMonthField.requestFocus();
      return false;
    }

    String expirationDateYear = _expirationDateYearField.getText().trim();
    if (expirationDateYear.length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_MSG).toString(),
        __resources.getProperty(INVALID_CREDIT_CARD_INFO_TITLE).toString() );
      _expirationDateYearField.requestFocus();
      return false;
    }

    return true;
  }

  @Override
  protected boolean OKAction()
  {
    boolean rc = PanelValid();
    if (rc == true)
    {
//      String nameOnCard = new String(_nameOnCardField.getText().trim());
//      String billingAddress = new String(_billingAddressField.getText().trim());
//      String creditCardNumber = new String(_creditCardNumberField.getText().trim());
//      String expirationDate = new String (
//                                  _expirationDateMonthField.getText().trim() + "-" +
//                                  _expirationDateYearField.getText().trim() );
      // todo1 do e-commerce charge here
      // todo1 make prepayment history entry here to user account id = "Guest"

      _okButton.setSelected(true);
      NotifyDialogListeners();
      dispose();
    }
    return rc;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(GuestPrintJobInformationEntryDialog.class.getName());
  }

}