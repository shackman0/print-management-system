package shackelford.floyd.printmanagementsystem.guiserver;

import java.text.MessageFormat;

import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: PrintJobBeingProcessedMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobBeingProcessedMsgBox
  extends MsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = -2334665788663161537L;
  /**
   * 
   */
  
  @SuppressWarnings("unused")
  private String                  _printJobNumber;
  @SuppressWarnings("unused")
  private String                  _printer;

  private static Resources        __resources;

  protected static final String   MSG_1 = "MSG_1";


  public PrintJobBeingProcessedMsgBox (
    String printJobNumber,
    String printer )
  {
    super();

    _printJobNumber = printJobNumber;
    _printer = printer;

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { printJobNumber,
                                                 printer };
    String msgString = msgFormat.format(substitutionValues).trim();

    Initialize(msgString,__resources.getProperty(TITLE));
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PrintJobBeingProcessedMsgBox.class.getName());
  }

}