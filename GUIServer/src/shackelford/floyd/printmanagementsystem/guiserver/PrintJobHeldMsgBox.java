package shackelford.floyd.printmanagementsystem.guiserver;

import java.text.MessageFormat;

import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: PrintJobHeldMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobHeldMsgBox
  extends MsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = 7698396375701599158L;

  /**
   * 
   */
  

  private static Resources        __resources;

  private static final String MSG_1 = "MSG_1";

  public PrintJobHeldMsgBox (
    String printJobNumber,
    String printer )
  {
    super();

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { printJobNumber,
                                                 printer };
    String msgString = msgFormat.format(substitutionValues).trim();

    Initialize (
      msgString,
      __resources.getProperty(TITLE) );
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PrintJobHeldMsgBox.class.getName());
  }

}