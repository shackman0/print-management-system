package shackelford.floyd.printmanagementsystem.guiserver;

import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;

import shackelford.floyd.printmanagementsystem.common.MsgBox;
import shackelford.floyd.printmanagementsystem.common.Resources;



/**
 * <p>Title: InsufficientPrePaymentBalanceMsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class InsufficientPrePaymentBalanceMsgBox
  extends MsgBox
{

  /**
   * 
   */
  private static final long serialVersionUID = 3658370670851376107L;

  /**
   * 
   */
  

  private static Resources             __resources;

  private static final DecimalFormat   __formatter = new DecimalFormat("#,##0.00");

  private static final String          MSG_1 = "MSG_1";

  public InsufficientPrePaymentBalanceMsgBox (
    String printJobNumber,
    String currencySymbol,
    String printJobCost,
    String prePaymentBalance,
    String printer )
  {
    super();

    double printJobCostDouble = 0d;
    double prePaymentBalanceDouble = 0d;
    try
    {
      Number parsedObj = (Number) __formatter.parseObject(printJobCost);
      printJobCostDouble = parsedObj.doubleValue();

      parsedObj = (Number) __formatter.parseObject(prePaymentBalance);
      prePaymentBalanceDouble = parsedObj.doubleValue();
    }
    catch (ParseException excp)
    {
      excp.printStackTrace();
    }

    MessageFormat msgFormat = new MessageFormat(__resources.getProperty(MSG_1).toString());
    Object[] substitutionValues = new Object[] { currencySymbol,
                                                 printJobNumber,
                                                 new Double(printJobCostDouble),
                                                 new Double(prePaymentBalanceDouble) };
    String msgString = msgFormat.format(substitutionValues).trim();

    Initialize(msgString,__resources.getProperty(TITLE));
  }

  static
  {
    __resources = Resources.CreateApplicationResources(InsufficientPrePaymentBalanceMsgBox.class.getName());
  }

}