package shackelford.floyd.printmanagementsystem.guiserver;

import java.awt.EventQueue;
import java.awt.Toolkit;
import java.net.InetAddress;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.Enumeration;
import java.util.Properties;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.GlobalAttributes;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MultiLineToolTipUI;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SplashPanel;
import shackelford.floyd.printmanagementsystem.common.WaitCursorEventQueue;
import shackelford.floyd.printmanagementsystem.installationdatabase.ClientRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.upgradeserver.Upgrader;


/**
 * <p>Title: GUIServerImpl</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GUIServerImpl
  extends java.rmi.server.UnicastRemoteObject
  implements GUIServer
{

  /**
   * 
   */
  private static final long              serialVersionUID                      = 8038398791532930374L;

  /**
   * 
   */

  private static ReceivingPrintJobMsgBox __receivingPrintJobMsgBox             = null;

  private static int                     __fontSizeAdjustment                  = 0;

  public static final int                MAJOR_VERSION                         = 1;

  public static final int                MINOR_VERSION                         = 0;

  public static final int                FIX_LEVEL                             = 0;

  private static final String            METAL_LOOK_AND_FEEL                   = "javax.swing.plaf.metal.MetalLookAndFeel";

  private static final int               WAIT_CURSOR_DELAY                     = 500;                                      // milliseconds

  public static final int                EXIT_OK                               = 0;

  public static final int                EXIT_LOGIN_CANCELLED                  = 1;

  public static final int                EXIT_ERROR_MISSING_RESDIR             = -2;

  public static final int                EXIT_ERROR_MISSING_DBADDR             = -3;

  public static final int                EXIT_ERROR_MISSING_DBUSERID           = -4;

  public static final int                EXIT_ERROR_MISSING_DBPASSWD           = -5;

  public static final int                EXIT_ERROR_DATABASE_CONNECT           = -6;

  public static final int                EXIT_ERROR_MISSING_INST_ID            = -7;

  public static final int                EXIT_ERROR_INSTALLATION_DISABLED      = -8;

  public static final int                EXIT_ERROR_RMI_REGISTRATION_FAILURE   = -9;

  public static final int                EXIT_ERROR_CANNOT_GET_LOCAL_HOST_ADDR = -10;

  public static final int                EXIT_ERROR_NOT_VALID_CLIENT           = -11;

  private static final String            NEW_LINE                              = "\n";

  private static final String            DOUBLE_QUOTE                          = "\"";

  private GUIServerImpl()
    throws RemoteException
  {
    // do nothing
  }

  @Override
  protected void finalize()
  {
    InstallationDatabase.GetDefaultInstallationDatabase().CloseDatabase();
  }

  @Override
  public GUIServer NewGUIServer()
    throws RemoteException
  {
    GUIServerImpl guiServer = new GUIServerImpl();

    // try to bind to the database
    try
    {
      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
      throw new RemoteException(excp.toString());
    }

    return guiServer;
  }

  @Override
  public Properties DisplayPrintJobInformationEntryDialog(String printJobNumber, String numberOfPages, String printer, String currencySymbol, String printJobCost)
  {
    PrintJobInformationEntryDialog dialog = new PrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, currencySymbol, printJobCost);
    return dialog.WaitOnDialog();
  }

  @Override
  public Properties DisplayGuestPrintJobInformationEntryDialog(String printJobNumber, String numberOfPages, String printer, String currencySymbol, String printJobCost)
  {
    GuestPrintJobInformationEntryDialog dialog = new GuestPrintJobInformationEntryDialog(printJobNumber, numberOfPages, printer, currencySymbol, printJobCost);
    return dialog.WaitOnDialog();
  }

  @Override
  public void DisplayPrintJobReleasedMsgBox(String printJobNumber, String printer, String currencySymbol, String newBalance, String jobCost)
  {
    PrintJobReleasedMsgBox msgBox = new PrintJobReleasedMsgBox(printJobNumber, printer, currencySymbol, newBalance, jobCost);
    msgBox.setVisible(true);
  }

  @Override
  public void DisplayPrintJobHeldMsgBox(String printJobNumber, String printer)
  {
    PrintJobHeldMsgBox msgBox = new PrintJobHeldMsgBox(printJobNumber, printer);
    msgBox.setVisible(true);
  }

  @SuppressWarnings("unused")
  @Override
  public void DisplayPrintJobBeingProcessedMsgBox(String printJobNumber, String printer)
  {
    DismissReceivingPrintJobMsgBox();
    //    PrintJobBeingProcessedMsgBoxManager msgBox =
    new PrintJobBeingProcessedMsgBoxManager(printJobNumber, printer);
  }

  @Override
  public void DismissPrintJobBeingProcessedMsgBox(String printJobNumber, String printer)
  {
    PrintJobBeingProcessedMsgBoxManager.DismissPanel(printJobNumber, printer);
  }

  @Override
  public void DisplayInsufficientPrePaymentBalanceMsgBox(String printJobNumber, String currencySymbol, String printJobCost, String prePaymentBalance, String printer)
  {
    InsufficientPrePaymentBalanceMsgBox msgBox = new InsufficientPrePaymentBalanceMsgBox(printJobNumber, currencySymbol, printJobCost, prePaymentBalance, printer);
    msgBox.setVisible(true);
  }

  /**
  This operation displays the DisplayReceivingPrintJobMsgBox.
  It is non-blocking.
  */
  @Override
  public void DisplayReceivingPrintJobMsgBox()
  {
    DismissReceivingPrintJobMsgBox();
    __receivingPrintJobMsgBox = new ReceivingPrintJobMsgBox();
    __receivingPrintJobMsgBox.setVisible(true);
  }

  @Override
  public void DismissReceivingPrintJobMsgBox()
  {
    if (__receivingPrintJobMsgBox != null)
    {
      __receivingPrintJobMsgBox.dispose();
      __receivingPrintJobMsgBox = null;
    }
  }

  public static void main(String[] args)
    throws Exception
  {

    GlobalAttributes.SetMajorVersion(MAJOR_VERSION);
    GlobalAttributes.SetMinorVersion(MINOR_VERSION);
    GlobalAttributes.SetFixLevel(FIX_LEVEL);

    GlobalAttributes.__applicationName = GUIServer.class.getName(); // yes; GUIServer is correct.
    GlobalAttributes.__mainClass = GUIServerImpl.class;

    Integer installationID = null;

    MultiLineToolTipUI.initialize();
    MultiLineToolTipUI.setDisplayAcceleratorKey(false);

    String splashComments = NEW_LINE + NEW_LINE;

    // evaluate the command line parameters
    for (int index = 0; index < args.length; index++)
    {
      if (args[index].equalsIgnoreCase("-verbose") == true)
      {
        GlobalAttributes.__verbose = true;
        splashComments += "Running verbose" + NEW_LINE;
      }
      else
        if (args[index].equalsIgnoreCase("-dbAddr") == true)
        {
          GlobalAttributes.__dbAddr = args[++index];
          splashComments += "Database Address = " + DOUBLE_QUOTE + GlobalAttributes.__dbAddr + DOUBLE_QUOTE + NEW_LINE;
        }
        else
          if (args[index].equalsIgnoreCase("-instID") == true)
          {
            installationID = new Integer(args[++index]);
            splashComments += "Installation ID = " + DOUBLE_QUOTE + installationID + DOUBLE_QUOTE + NEW_LINE;
          }
          else
            if (args[index].equalsIgnoreCase("-dbUser") == true)
            {
              GlobalAttributes.__dbUserID = args[++index];
              splashComments += "Database User ID = " + DOUBLE_QUOTE + GlobalAttributes.__dbUserID + DOUBLE_QUOTE + NEW_LINE;
            }
            else
              if (args[index].equalsIgnoreCase("-dbPasswd") == true)
              {
                GlobalAttributes.__dbPasswd = args[++index];
                splashComments += "Database Password found" + NEW_LINE;
              }
              else
                if (args[index].equalsIgnoreCase("-resDir") == true)
                {
                  GlobalAttributes.__resourcesDirectory = args[++index];
                  splashComments += "Resource files are located under directory " + DOUBLE_QUOTE + GlobalAttributes.__resourcesDirectory + DOUBLE_QUOTE + NEW_LINE;
                }
                else
                  if (args[index].equalsIgnoreCase("-fontSizeAdj") == true)
                  {
                    __fontSizeAdjustment = new Integer(args[++index]).intValue();
                    splashComments += "Adjusting font by " + __fontSizeAdjustment + NEW_LINE;
                  }
                  else
                  {
                    splashComments += "Ignoring unknown parameter: " + DOUBLE_QUOTE + args[index] + DOUBLE_QUOTE + NEW_LINE;
                  }
    }

    if (installationID == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -instID was not specified" + NEW_LINE + Usage(), "ERROR: -instID was not specified");
      System.exit(EXIT_ERROR_MISSING_INST_ID);
    }

    if (GlobalAttributes.__dbAddr == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbAddr was not specified" + NEW_LINE + Usage(), "ERROR: -dbAddr was not specified");
      System.exit(EXIT_ERROR_MISSING_DBADDR);
    }

    if (GlobalAttributes.__dbUserID == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbUser was not specified" + NEW_LINE + Usage(), "ERROR: -dbUser was not specified");
      System.exit(EXIT_ERROR_MISSING_DBUSERID);
    }

    if (GlobalAttributes.__dbPasswd == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -dbPasswd was not specified" + NEW_LINE + Usage(), "ERROR: -dbPasswd was not specified");
      System.exit(EXIT_ERROR_MISSING_DBPASSWD);
    }

    if (GlobalAttributes.__resourcesDirectory == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, "ERROR: -resDir was not specified" + NEW_LINE + Usage(), "ERROR: -resDir was not specified");
      System.exit(EXIT_ERROR_MISSING_RESDIR);
    }

    // set up the wait cursor to be displayed whenever the user has to wait for a response
    EventQueue waitQueue = new WaitCursorEventQueue(WAIT_CURSOR_DELAY);
    Toolkit.getDefaultToolkit().getSystemEventQueue().push(waitQueue);

    // adjust the font size if specified
    if (__fontSizeAdjustment != 0)
    {
      UIManager.setLookAndFeel(METAL_LOOK_AND_FEEL);
      SwingUtilities.updateComponentTreeUI(new JFrame());

      UIDefaults uiDefaults = UIManager.getLookAndFeelDefaults();
      for (Enumeration<?> enumerator = uiDefaults.keys(); enumerator.hasMoreElements();)
      {
        Object key = enumerator.nextElement();
        if (key.toString().endsWith(".font") == true)
        {
          FontUIResource currentFontResource = (FontUIResource) (uiDefaults.get(key));
          FontUIResource newFontResource = new FontUIResource(currentFontResource.getName(), currentFontResource.getStyle(), currentFontResource.getSize() + __fontSizeAdjustment);
          uiDefaults.put(key, newFontResource);
        }
      }
    }

    SplashPanel splashPanel = new SplashPanel(splashComments);
    splashPanel.setVisible(true);

    // try to bind to the database
    try
    {
      InstallationDatabase.SetDefaultInstallationDatabase(new InstallationDatabase(GlobalAttributes.__dbAddr, GlobalAttributes.__dbUserID, GlobalAttributes.__dbPasswd, installationID));

      InstallationDatabase.GetDefaultInstallationDatabase().ConnectDatabase();

      // set up to use the installation's preferred language
      Resources.SetLanguageDirectory(InstallationTable.GetCachedInstallationRow().Get_language());
    }
    catch (Exception excp)
    {
      MessageDialog.ShowErrorMessageDialog(null, excp, "Cannot Connect to Installation Database");
      System.exit(EXIT_ERROR_DATABASE_CONNECT);
    }

    if (InstallationTable.StateOK() == false)
    {
      MessageDialog.ShowErrorMessageDialog(null, "Your installation is not enabled.", "Installation Not Enabled");
      System.exit(EXIT_ERROR_INSTALLATION_DISABLED);
    }

    // set up the language to the installation's preference
    Resources.SetLanguageDirectory(InstallationTable.GetCachedInstallationRow().Get_language());

    String localHostIPAddr = null;
    try
    {
      localHostIPAddr = InetAddress.getLocalHost().getHostAddress().trim();
    }
    catch (java.net.UnknownHostException excp)
    {
      MessageDialog.ShowErrorMessageDialog(null, excp, "Cannot Determine Localhost IP Address");
      System.exit(EXIT_ERROR_CANNOT_GET_LOCAL_HOST_ADDR);
    }

    ClientRow clientRow = (ClientRow) (InstallationDatabase.GetDefaultInstallationDatabase().GetClientTable().GetRowForIPAddress(localHostIPAddr));
    if (clientRow == null)
    {
      MessageDialog.ShowErrorMessageDialog(null, localHostIPAddr + " is not a client address registered for this installation.", "Unregistered Client");
      System.exit(EXIT_ERROR_NOT_VALID_CLIENT);
    }

    // see if we need to upgrade - if we upgrade, we don't return from this call
    Upgrader.CheckForUpgrade(InstallationTable.GetCachedInstallationRow(), GlobalAttributes.__dbAddr, GlobalAttributes.__applicationName);

    InstallationDatabase.GetDefaultInstallationDatabase().CloseDatabase();

    // bind server to registry
    try
    {
      GUIServerImpl guiServer = new GUIServerImpl();
      // Bootstrap registry service
      Registry reg = LocateRegistry.createRegistry(Registry.REGISTRY_PORT);
      reg.rebind(GlobalAttributes.__applicationName, guiServer);
    }
    catch (Exception excp)
    {
      MessageDialog.ShowErrorMessageDialog(null, excp, "Cannot Bind Server to Registry");
      System.exit(EXIT_ERROR_RMI_REGISTRATION_FAILURE);
    }

    try
    {
      String[] bindings = Naming.list("localhost");
      if (GlobalAttributes.__verbose == true)
      {
        DebugWindow.DisplayText("Servers currently bound at this host:");
        for (int i = 0; i < bindings.length; i++)
        {
          DebugWindow.DisplayText("  " + bindings[i]);
        }
      }
    }
    catch (Exception excp)
    {
      excp.printStackTrace();
    }

    DebugWindow.DisplayText("GUIServer is running ...");

    splashPanel.dispose();
  }

  private static String Usage()
  {
    return "Usage" + NEW_LINE + "java" + NEW_LINE + "  -Djdbc.drivers=org.postgresql.Driver" + NEW_LINE + "  -dbaddr <database hostname or ip addr>" + NEW_LINE + "  -dbuser <database user id>"
      + NEW_LINE + "  -dbpasswd <database passwd>" + NEW_LINE + "  -instID <installation ID>" + NEW_LINE + "  -resDir <resources directory>" + NEW_LINE
      + "  [-fontSizeAdj <[-]font size adj>] [-verbose]";
  }

}
