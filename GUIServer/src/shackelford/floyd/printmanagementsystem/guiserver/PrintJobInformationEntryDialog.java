package shackelford.floyd.printmanagementsystem.guiserver;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.Properties;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractDialog;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.DebugWindow;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.OverwritableTextField;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.RulesTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;
import shackelford.floyd.printmanagementsystem.useraccount.UserAccountEntryDialog;



/**
 * <p>Title: PrintJobInformationEntryDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrintJobInformationEntryDialog
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = 1039608912870776736L;
  /**
   * 
   */
  
  protected ActionButton               _guestButton = null;
  protected ActionButton               _guestHoldButton = null;
  protected ActionButton               _holdButton = null;
  protected ActionButton               _newUserAccountButton = null;
  protected JPasswordField             _passphraseField = new JPasswordField();
  protected OverwritableTextField      _userAccountIDField = new OverwritableTextField();
  protected OverwritableTextField      _descriptionTextField = new OverwritableTextField();
  protected UserAccountRow             _userAccountRow = null;
  protected String                     _guestHoldAccountID = null;
  protected RulesRow                   _allowGuestHoldRuleRow = null;
  protected RulesRow                   _holdAllJobsRulesRow = null;
  protected RulesRow                   _allowHoldRuleRow = null;
  protected RulesRow                   _allowGuestPrintRuleRow = null;
  protected RulesRow                   _allowUserCreatedAccountRuleRow = null;
  protected RulesRow                   _guestAccountOnlyRuleRow = null;
  protected RulesRow                   _requireUserPasswordRuleRow = null;
  protected String                     _printJobNumber = null;
  protected String                     _numberOfPages = null;
  protected String                     _printer = null;
  protected String                     _currencySymbol = null;
  protected String                     _printJobCost = null;

  private static Resources             __resources = null;

  private static final DecimalFormat   __formatter = new DecimalFormat("#,##0.00");

  public static final String           GUEST = "GUEST";
  protected static final String        GUEST_TIP = "GUEST_TIP";
  public static final String           GUEST_HOLD = "GUEST_HOLD";
  protected static final String        GUEST_HOLD_TIP = "GUEST_HOLD_TIP";
  public static final String           HOLD = "HOLD";
  protected static final String        HOLD_TIP = "HOLD_TIP";
  protected static final String        PASSPHRASE = "PASSPHRASE";
  protected static final String        PASSPHRASE_TIP = "PASSPHRASE_TIP";
  public static final String           USER_ACCOUNT_ID = "USER_ACCOUNT_ID";
  protected static final String        USER_ACCOUNT_ID_TIP = "USER_ACCOUNT_ID_TIP";
  public static final String           DESCRIPTION = "DESCRIPTION";
  protected static final String        DESCRIPTION_TIP = "DESCRIPTION_TIP";
  protected static final String        NEW_USER_ACCOUNT = "NEW_USER_ACCOUNT";
  protected static final String        NEW_USER_ACCOUNT_TIP = "NEW_USER_ACCOUNT_TIP";
  protected static final String        INVALID_USER_ACCOUNT_ID_MSG = "INVALID_USER_ACCOUNT_ID_MSG";
  protected static final String        INVALID_USER_ACCOUNT_ID_TITLE = "INVALID_USER_ACCOUNT_ID_TITLE";
  protected static final String        INVALID_PASSPHRASE_MSG = "INVALID_PASSPHRASE_MSG";
  protected static final String        INVALID_PASSPHRASE_TITLE = "INVALID_PASSPHRASE_TITLE";
  protected static final String        USER_ACCOUNT_NOT_ENABLED_MSG = "USER_ACCOUNT_NOT_ENABLED_MSG";
  protected static final String        USER_ACCOUNT_NOT_ENABLED_TITLE = "USER_ACCOUNT_NOT_ENABLED_TITLE";
  protected static final String        MISSING_DESCRIPTION_MSG = "MISSING_DESCRIPTION_MSG";
  protected static final String        MISSING_DESCRIPTION_TITLE = "MISSING_DESCRIPTION_TITLE";
  protected static final String        MSG_1 = "MSG_1";
  protected static final String        MSG_2 = "MSG_2";
  protected static final String        MSG_3 = "MSG_3";
  protected static final String        MSG_3a = "MSG_3a";
  protected static final String        MSG_3b = "MSG_3b";
  protected static final String        MSG_3c = "MSG_3c";

  protected static final String        CURRENT_BALANCE = "CURRENT_BALANCE";
  protected static final String        JOB_COST = "JOB_COST";
  protected static final String        NEW_BALANCE = "NEW_BALANCE";




  public PrintJobInformationEntryDialog (
    String               printJobNumber,
    String               numberOfPages,
    String               printer,
    String               currencySymbol,
    String               printJobCost )
  {
    super(null, true);

    _printJobNumber = printJobNumber;
    _numberOfPages = numberOfPages;
    _printer = printer;
    _currencySymbol = currencySymbol;
    _printJobCost  = printJobCost;

    RulesTable rulesTable = InstallationDatabase.GetDefaultInstallationDatabase().GetRulesTable();

    _holdAllJobsRulesRow = rulesTable.GetRowForRuleName(RulesRow.HOLD_ALL_JOBS);
    _allowHoldRuleRow = rulesTable.GetRowForRuleName(RulesRow.ALLOW_PRINT_HOLD);
    _allowGuestHoldRuleRow = rulesTable.GetRowForRuleName(RulesRow.ALLOW_GUEST_PRINT_HOLD);
    _allowGuestPrintRuleRow = rulesTable.GetRowForRuleName(RulesRow.ALLOW_GUEST_PRINT);
    _allowUserCreatedAccountRuleRow = rulesTable.GetRowForRuleName(RulesRow.ALLOW_USER_CREATED_ACCOUNT);
    _guestAccountOnlyRuleRow = rulesTable.GetRowForRuleName(RulesRow.GUEST_ACCOUNT_ONLY);
    _requireUserPasswordRuleRow = rulesTable.GetRowForRuleName(RulesRow.REQUIRE_USER_PASSWORD);

    DebugWindow.DisplayText("PrintJobInformationEntryDialog:");
    DebugWindow.DisplayText("\tprintJobNumber = " + _printJobNumber);
    DebugWindow.DisplayText("\tnumberOfPages = " + _numberOfPages);
    DebugWindow.DisplayText("\tprinter = " + _printer);
    DebugWindow.DisplayText("\tcurrencySymbol = " + _currencySymbol);
    DebugWindow.DisplayText("\tprintJobCost = " + _printJobCost);

    Initialize();
  }

  @Override
  protected Container CreateNorthPanel()
  {
    Box northPanel = new Box(BoxLayout.Y_AXIS);

    northPanel.add(Box.createHorizontalStrut(5));

    double printJobCost = 0d;
    try
    {
      Number parsedObj = (Number) __formatter.parseObject(_printJobCost);
      printJobCost = parsedObj.doubleValue();
    }
    catch (ParseException excp)
    {
      excp.printStackTrace();
    }

    MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(MSG_1));
    Object[] substitutionValues = new Object[] { _currencySymbol,
                                                 _printJobNumber,
                                                 _numberOfPages,
                                                 _printer,
                                                 new Double(printJobCost) };
    String msgString = msgFormat.format(substitutionValues).trim();

    northPanel.add(new JLabel(msgString,SwingConstants.LEFT));
    northPanel.add(new JLabel(GetResources().getProperty(MSG_2),SwingConstants.LEFT));

    northPanel.add(Box.createVerticalStrut(10));

    if ( ( (_allowGuestHoldRuleRow != null) &&
           (_allowGuestHoldRuleRow.RuleIsYes() == true) ) ||
         ( (_allowUserCreatedAccountRuleRow != null) &&
           (_allowUserCreatedAccountRuleRow.RuleIsYes() == true) ) ||
         ( (_allowGuestPrintRuleRow != null) &&
           (_allowGuestPrintRuleRow.RuleIsYes() == true) ) )
    {
      northPanel.add(new JLabel(GetResources().getProperty(MSG_3),SwingConstants.LEFT));

      if ( (_allowGuestHoldRuleRow != null) &&
           (_allowGuestHoldRuleRow.RuleIsYes() == true) )
      {
        northPanel.add(new JLabel(GetResources().getProperty(MSG_3a),SwingConstants.LEFT));
      }

      if ( (_allowUserCreatedAccountRuleRow != null) &&
           (_allowUserCreatedAccountRuleRow.RuleIsYes() == true) )
      {
        northPanel.add(new JLabel(GetResources().getProperty(MSG_3b),SwingConstants.LEFT));
      }

      if ( (_allowGuestPrintRuleRow != null) &&
           (_allowGuestPrintRuleRow.RuleIsYes() == true) )
      {
        northPanel.add(new JLabel(GetResources().getProperty(MSG_3c),SwingConstants.LEFT));
      }
    }

    return northPanel;
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 2, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    if ( (_guestAccountOnlyRuleRow != null) &&
         (_guestAccountOnlyRuleRow.RuleIsNo() == true) )
    {
      // user account id
      constraints.gridy += 1;

      constraints.gridx = 0;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.anchor = GridBagConstraints.EAST;

      centerPanel.add (
        new JLabel(GetResources().getProperty(USER_ACCOUNT_ID),SwingConstants.RIGHT),
          constraints );

      constraints.gridx += constraints.gridwidth;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.anchor = GridBagConstraints.WEST;

      _userAccountIDField.setColumns(FIELD_WIDTH);
      _userAccountIDField.setToolTipText(GetResources().getProperty(USER_ACCOUNT_ID_TIP));
      _userAccountIDField.requestFocus();
      centerPanel.add ( _userAccountIDField,	constraints );

      // passphrase
      constraints.gridy += 1;

      constraints.gridx = 0;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.anchor = GridBagConstraints.EAST;

      if (_requireUserPasswordRuleRow.RuleIsYes() == true)
      {
        centerPanel.add (
          new JLabel(GetResources().getProperty(PASSPHRASE),SwingConstants.RIGHT),
            constraints );
      }

      constraints.gridx += constraints.gridwidth;
      constraints.gridwidth = 1;
      constraints.gridheight = 1;
      constraints.fill = GridBagConstraints.HORIZONTAL;
      constraints.anchor = GridBagConstraints.WEST;

      _passphraseField.setText("");
      _passphraseField.setColumns(FIELD_WIDTH);
      _passphraseField.setToolTipText(GetResources().getProperty(PASSPHRASE_TIP));

      if (_requireUserPasswordRuleRow.RuleIsYes() == true)
      {
        centerPanel.add ( _passphraseField,	constraints );
      }
    }

    // description
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add (
      new JLabel(GetResources().getProperty(DESCRIPTION),SwingConstants.RIGHT),
        constraints );

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.HORIZONTAL;
    constraints.anchor = GridBagConstraints.WEST;

    _descriptionTextField.setColumns(FIELD_WIDTH);
    _descriptionTextField.setToolTipText(GetResources().getProperty(DESCRIPTION_TIP));
    centerPanel.add ( _descriptionTextField,	constraints );

    return centerPanel;
  }

  @Override
  protected Container CreateSouthPanel()
  {
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    _okButton = new ActionButton(new OKAction());
    if ( (_holdAllJobsRulesRow != null) &&
         (_holdAllJobsRulesRow.RuleIsNo() == true) )
    {
      southPanel.add(_okButton);
    }

    _holdButton = new ActionButton(new HoldAction());
    if ( (_allowHoldRuleRow != null) &&
         (_allowHoldRuleRow.RuleIsYes() == true) )
    {
      southPanel.add(_holdButton);
    }

    _guestHoldButton = new ActionButton(new GuestHoldAction());
    if ( (_allowGuestHoldRuleRow != null) &&
         (_allowGuestHoldRuleRow.RuleIsYes() == true) )
    {
      southPanel.add(_guestHoldButton);
    }

    _guestButton = new ActionButton(new GuestAction());
    if ( (_allowGuestPrintRuleRow != null) &&
         (_allowGuestPrintRuleRow.RuleIsYes() == true) )
    {
      southPanel.add(_guestButton);
    }

    _newUserAccountButton = new ActionButton(new NewUserAccountAction());
    if ( (_allowUserCreatedAccountRuleRow != null) &&
         (_allowUserCreatedAccountRuleRow.RuleIsYes() == true) )
    {
      southPanel.add(_newUserAccountButton);
    }

    _cancelButton = new ActionButton(new CancelAction());
    southPanel.add(_cancelButton);

    return southPanel;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  @Override
  protected void AssignFields()
  {
    if ( (_guestAccountOnlyRuleRow != null) &&
       (_guestAccountOnlyRuleRow.RuleIsYes() == true) )
    {
      _userAccountIDField.setText(_guestAccountOnlyRuleRow.GetRuleParm(0));
    }
  }

  @Override
  protected Properties GetProperties()
  {
    Properties props = new Properties();

    // massage the user account id to remove any "dangerous" characters
    try
    {
      props.setProperty(USER_ACCOUNT_ID,URLEncoder.encode(_userAccountIDField.getText(),LPDClient.UTF_8));
      props.setProperty(PASSPHRASE,new String(_passphraseField.getPassword()));

      String description = new String(_descriptionTextField.getText());
      if (description.trim().length() == 0)
      {
        description = " ";
      }
      // massage the description to remove any "dangerous" characters
      description = URLEncoder.encode(description,LPDClient.UTF_8);
      props.setProperty(DESCRIPTION,description);

      props.setProperty(GUEST,new Boolean(_guestButton.isSelected()).toString());
      props.setProperty(GUEST_HOLD,new Boolean(_guestHoldButton.isSelected()).toString());
      props.setProperty(NEW_USER_ACCOUNT,new Boolean(_newUserAccountButton.isSelected()).toString());
      props.setProperty(OK,new Boolean(_okButton.isSelected()).toString());
      props.setProperty(HOLD,new Boolean(_holdButton.isSelected()).toString());
      props.setProperty(CANCEL,new Boolean(_cancelButton.isSelected()).toString());
    }
    catch (UnsupportedEncodingException ex)
    {
      throw new RuntimeException(ex);
    }

    return props;
  }

  @Override
  protected boolean PanelValid()
  {
    String userAccountID = _userAccountIDField.getText();
    String passphrase = new String(_passphraseField.getPassword());

    UserAccountRow userAccountRow = UserAccountTable.GetCachedUserAccountRow(userAccountID);

    if (userAccountRow == null)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_USER_ACCOUNT_ID_MSG),
        GetResources().getProperty(INVALID_USER_ACCOUNT_ID_TITLE) );
      _userAccountIDField.requestFocus();
      _passphraseField.setText("");
      return false;
    }

    if (userAccountRow.StatusIsEnabled() == false)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(USER_ACCOUNT_NOT_ENABLED_MSG),
        GetResources().getProperty(USER_ACCOUNT_NOT_ENABLED_TITLE) );
      _userAccountIDField.requestFocus();
      _passphraseField.setText("");
      return false;
    }

    if ( (_requireUserPasswordRuleRow.RuleIsYes() == true) &&
         ( userAccountRow.Get_user_passphrase().equals(passphrase) == false) )
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(INVALID_PASSPHRASE_MSG),
        GetResources().getProperty(INVALID_PASSPHRASE_TITLE) );
      _passphraseField.setText("");
      _passphraseField.requestFocus();
      return false;
    }

    return true;
  }

  protected boolean HoldPanelValid()
  {
    String description = new String(_descriptionTextField.getText());

    if (description.trim().length() == 0)
    {
      MessageDialog.ShowErrorMessageDialog (
        this,
        GetResources().getProperty(MISSING_DESCRIPTION_MSG),
        GetResources().getProperty(MISSING_DESCRIPTION_TITLE) );
      _descriptionTextField.requestFocus();
      return false;
    }

    return true;
  }

  protected boolean GuestHoldPanelValid()
  {
    return HoldPanelValid();
  }

  protected void GuestAction()
  {
    _guestButton.setSelected(true);
    NotifyDialogListeners();
    dispose();
  }

  protected class GuestAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 294810777698545764L;

    /**
     * 
     */
    

    public GuestAction()
    {
      putValue(Action.NAME, GetResources().getProperty(GUEST));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(GUEST_TIP));
    }

    @Override
    public void actionPerformed ( ActionEvent event )
    {
      GuestAction();
    }
  }

  protected class NewUserAccountAction
    extends AbstractAction
    implements PanelListener
  {
    /**
     * 
     */
    private static final long serialVersionUID = 7680834259256472233L;

    /**
     * 
     */
    

    public NewUserAccountAction()
    {
      putValue(Action.NAME, GetResources().getProperty(NEW_USER_ACCOUNT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(NEW_USER_ACCOUNT_TIP));
    }

    @Override
    public void actionPerformed ( ActionEvent event )
    {
      UserAccountEntryDialog entryDialog = new UserAccountEntryDialog();
      Properties properties = entryDialog.WaitOnDialog();
      _userAccountIDField.setText(properties.getProperty(UserAccountEntryDialog.USER_ACCOUNT_ID));
    }

    @Override
    public void EndOfPanel ( char action )
    {
      setVisible(true);
    }

    @Override
    public void PanelStateChange ( char action )
    {
      // do nothing
    }
  }

  protected void HoldAction()
  {
    if ( (PanelValid() == true) &&
          (HoldPanelValid() == true) )
    {
      _holdButton.setSelected(true);
      NotifyDialogListeners();
      dispose();
    }
  }

  protected class HoldAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8686264187594062361L;

    /**
     * 
     */
    

    public HoldAction()
    {
      putValue(Action.NAME, GetResources().getProperty(HOLD));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(HOLD_TIP));
    }

    @Override
    public void actionPerformed ( ActionEvent event )
    {
      HoldAction();
    }
  }

  protected void GuestHoldAction()
  {
    if (GuestHoldPanelValid() == true)
    {
      _guestHoldButton.setSelected(true);
      _userAccountIDField.setText(_allowGuestHoldRuleRow.GetRuleParm(0));
      NotifyDialogListeners();
      dispose();
    }
  }

  protected class GuestHoldAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -627105210567271281L;

    /**
     * 
     */
    

    public GuestHoldAction()
    {
      putValue(Action.NAME, GetResources().getProperty(GUEST_HOLD));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(GUEST_HOLD_TIP));
    }

    @Override
    public void actionPerformed ( ActionEvent event )
    {
      GuestHoldAction();
    }
  }

  static
  {
    __resources = Resources.CreateApplicationResources(PrintJobInformationEntryDialog.class.getName());
  }

}