package shackelford.floyd.printmanagementsystem.releasestation;

import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.LPDClient.JobInfo;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobOnHoldHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: ReleaseStationTableModel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReleaseStationTableModel
  extends AbstractTableModel
{

  /**
   * 
   */
  private static final long         serialVersionUID               = 1L;

  JTable                            _table;

  private Resources                 _resources;

  private LPDClient.JobInfo[]       _jobInfo;

  private PrintJobOnHoldHeaderRow[] _jobHeader;

  ReleaseStationMainPanel           _mainPanel;

  private static Resources          __staticResources;

  private static String[]           __columnNames;

  private static DecimalFormat      __formatter                    = new DecimalFormat("#,##0.00");

  private static final String       NUMBER_OF_PAGES                = "NUMBER_OF_PAGES";

  private static final String       COST                           = "COST";

  private static final String       JOB_NUMBER                     = "JOB_NUMBER";

  //  private static final String PRINT_SERVER = "PRINT_SERVER";
  private static final String       PRINTER_NAME                   = "PRINTER_NAME";

  //  private static final String SUBMITTED_BY_USER_ID = "SUBMITTED_BY_USER_ID";
  //  private static final String SUBMITTED_BY_HOST = "SUBMITTED_BY_HOST";
  //  private static final String STATUS = "STATUS";
  private static final String       DESCRIPTION                    = "DESCRIPTION";

  //  private static final String USER_ACCOUNT_ID = "USER_ACCOUNT_ID";
  //  private static final String SUBMITTED_BY_HOSTNAME = "SUBMITTED_BY_HOSTNAME";
  //  private static final String HOST_JOB_NUMBER = "HOST_JOB_NUMBER";

  private static final String       CANNOT_ACCESS_GATEKEEPER_MSG   = "CANNOT_ACCESS_GATEKEEPER_MSG";

  private static final String       CANNOT_ACCESS_GATEKEEPER_TITLE = "CANNOT_ACCESS_GATEKEEPER_TITLE";

  public ReleaseStationTableModel(MyJTable table, ReleaseStationMainPanel mainPanel)
  {
    super();
    Initialize(table, mainPanel);
  }

  private void Initialize(MyJTable table, ReleaseStationMainPanel mainPanel)
  {
    _table = table;
    _resources = __staticResources;
    _mainPanel = mainPanel;

    //_table.setDefaultRenderer(Object.class, new ReleaseStationCellRenderer(this));
    _table.addMouseListener(new MouseClicked());
  }

  @Override
  public int getRowCount()
  {
    if (_jobInfo == null)
    {
      return 0;
    }
    return _jobInfo.length;
  }

  @Override
  public Class<? extends Object> getColumnClass(int columnIndex)
  {
    return getValueAt(0, columnIndex).getClass();
  }

  @Override
  public int getColumnCount()
  {
    if (__columnNames == null)
    {
      return 0;
    }
    return __columnNames.length;
  }

  @Override
  public Object getValueAt(int rowNum, int colNum)
  {
    switch (colNum)
    {
      case 0:
        return new Integer(_jobInfo[rowNum]._jobNumber.intValue());
      case 1:
        return new String(_jobInfo[rowNum]._description);
      case 2:
        return _jobHeader[rowNum].Get_number_of_pages();
      case 3:
        return __formatter.format(_jobHeader[rowNum].Get_cost());
      case 4:
        return new String(_jobInfo[rowNum]._printerName);
      default:
        return "UNKNOWN column=" + colNum + ", row=" + rowNum;
    }
  }

  public LPDClient.JobInfo[] GetJobInfoRows()
  {
    return _jobInfo;
  }

  public int[] GetSelectedRowNumbers()
  {
    return _table.getSelectedRows();
  }

  public LPDClient.JobInfo[] GetSelectedJobInfoRows()
  {
    ArrayList<JobInfo> selectedRowsArrayList = new ArrayList<>(10);

    int[] selectedRowNumbers = _table.getSelectedRows();
    for (int indx = 0; indx < selectedRowNumbers.length; indx++)
    {
      selectedRowsArrayList.add(_jobInfo[selectedRowNumbers[indx]]);
    }
    selectedRowsArrayList.trimToSize();
    LPDClient.JobInfo[] selectedRows = new LPDClient.JobInfo[selectedRowsArrayList.size()];
    selectedRowsArrayList.toArray(selectedRows);
    return selectedRows;
  }

  public PrintJobOnHoldHeaderRow[] GetSelectedJobHeaderRows()
  {
    ArrayList<PrintJobOnHoldHeaderRow> selectedRowsArrayList = new ArrayList<>(10);

    int[] selectedRowNumbers = _table.getSelectedRows();
    for (int indx = 0; indx < selectedRowNumbers.length; indx++)
    {
      selectedRowsArrayList.add(_jobHeader[selectedRowNumbers[indx]]);
    }
    selectedRowsArrayList.trimToSize();
    PrintJobOnHoldHeaderRow[] selectedRows = new PrintJobOnHoldHeaderRow[selectedRowsArrayList.size()];
    selectedRowsArrayList.toArray(selectedRows);
    return selectedRows;
  }

  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  @Override
  public String getColumnName(int colNum)
  {
    return __columnNames[colNum];
  }

  public void PopulateTable()
  {

    if (getRowCount() > 0)
    {
      fireTableRowsDeleted(0, getRowCount() - 1);
    }

    _jobInfo = null;

    // query each gatekeeper for it's print jobs on hold
    ArrayList<JobInfo> jobsOnHoldInfo = new ArrayList<>(10);
    ArrayList<PrintJobOnHoldHeaderRow> jobsOnHoldHeader = new ArrayList<>(10);

    ArrayList<?> gatekeepersArrayList = InstallationDatabase.GetDefaultInstallationDatabase().GetGatekeeperTable().GetRowsAsArrayList(null, // from
      null, // predicate
      GatekeeperRow.Name_ip_address(), // order by
      AbstractSQL1Table.NO_LIMIT);
    GatekeeperRow[] gatekeepers = new GatekeeperRow[gatekeepersArrayList.size()];
    gatekeepersArrayList.toArray(gatekeepers);
    for (int indx = 0; indx < gatekeepers.length; indx++)
    {
      GatekeeperRow gatekeeper = gatekeepers[indx];
      try
      {
        LPDClient lpdClient = new LPDClient(gatekeeper.Get_ip_address());
        LPDClient.PrinterInfo[] printerInfo = lpdClient.GetPrinterInfo();

        for (int jndx = 0; jndx < printerInfo.length; jndx++)
        {
          LPDClient.JobInfo[] jobInfo = lpdClient.GetJobsWithStatus(printerInfo[jndx]._printerName, LPDClient.JOB_STATUS_HOLD);
          for (int kndx = 0; kndx < jobInfo.length; kndx++)
          {
            LPDClient.JobInfo job = jobInfo[kndx];
            if (job._userAccountID.equals(UserAccountTable.GetCachedUserAccountRow().Get_user_account_id()) == true)
            {
              PrintJobOnHoldHeaderRow printJobHeader = (PrintJobOnHoldHeaderRow) (InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobOnHoldHeaderTable()
                .GetRowForPrintJobNumber(job._jobNumber));
              // there may be other jobs on the print server, but they might not
              // be in the database. we only care about jobs that are in the database.
              if (printJobHeader != null)
              {
                jobsOnHoldInfo.add(job);
                jobsOnHoldHeader.add(printJobHeader);
              }
            }
          }
        }
        lpdClient.Done();
      }
      catch (Exception excp)
      {
        MessageFormat msgFormat = new MessageFormat(_resources.getProperty(CANNOT_ACCESS_GATEKEEPER_MSG));
        Object[] substitutionValues = new Object[] { gatekeeper.Get_ip_address() };
        String msgString = msgFormat.format(substitutionValues).trim();
        // display a message indicating that the gatekeeper is not accessible
        MessageDialog.ShowMessageDialogNoWait(_mainPanel, msgString, _resources.getProperty(CANNOT_ACCESS_GATEKEEPER_TITLE), JOptionPane.ERROR_MESSAGE);
      }
    }
    jobsOnHoldInfo.trimToSize();
    _jobInfo = new LPDClient.JobInfo[jobsOnHoldInfo.size()];
    jobsOnHoldInfo.toArray(_jobInfo);

    jobsOnHoldHeader.trimToSize();
    _jobHeader = new PrintJobOnHoldHeaderRow[jobsOnHoldHeader.size()];
    jobsOnHoldHeader.toArray(_jobHeader);

    //fireTableStructureChanged();
    if (getRowCount() > 0)
    {
      fireTableRowsInserted(0, getRowCount() - 1);
    }
  }

  private class MouseClicked
    extends MouseAdapter
  {
    public MouseClicked()
    {
      super();
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      if ((event.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK)
      {
        _mainPanel.EnableButtons(_table.getSelectedRowCount() > 0);
      }
    }
  }

  static
  {
    __staticResources = Resources.CreateApplicationResources(ReleaseStationTableModel.class.getName());

    __columnNames = new String[] { __staticResources.getProperty(JOB_NUMBER), __staticResources.getProperty(DESCRIPTION), __staticResources.getProperty(NUMBER_OF_PAGES),
    __staticResources.getProperty(COST), __staticResources.getProperty(PRINTER_NAME) };
  }

}
