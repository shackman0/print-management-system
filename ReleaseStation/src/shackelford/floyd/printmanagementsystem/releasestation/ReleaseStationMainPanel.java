package shackelford.floyd.printmanagementsystem.releasestation;

import java.awt.Container;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.Enumeration;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import shackelford.floyd.printmanagementsystem.common.AbstractReportPanel;
import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MyJOptionPane;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SortFilterTableModel;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobOnHoldHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountTable;


/**
 * <p>Title: ReleaseStationMainPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ReleaseStationMainPanel
  extends AbstractReportPanel
{

  /**
   * 
   */
  private static final long serialVersionUID = 5706584196513855403L;

  /**
   * 
   */
  

  protected ReleaseStationTableModel _tableModel;

  protected SortFilterTableModel     _sorter;

  protected ActionButton             _deleteButton;

  protected ActionButton             _releaseButton;

  protected JLabel                   _currentBalanceLabel;

  protected JDialog                  _printJobsPleaseWaitDialog;

  protected javax.swing.Timer        _printJobsPleaseWaitTimer;

  private static Resources           __staticResources;

  protected static final String      RELEASE                   = "RELEASE";

  protected static final String      RELEASE_TIP               = "RELEASE_TIP";

  protected static final String      DELETE                    = "DELETE";

  protected static final String      DELETE_TIP                = "DELETE_TIP";

  protected static final String      INSUFFICIENT_FUNDS_MSG    = "INSUFFICIENT_FUNDS_MSG";

  protected static final String      INSUFFICIENT_FUNDS_TITLE  = "INSUFFICIENT_FUNDS_TITLE";

  protected static final String      PLEASE_WAIT_MSG           = "PLEASE_WAIT_MSG";

  protected static final String      PLEASE_WAIT_TITLE         = "PLEASE_WAIT_TITLE";

  protected static final String      USAGE_MSG                 = "USAGE_MSG";

  protected static final String      CURRENT_BALANCE           = "CURRENT_BALANCE";

  protected static final int         PLEASE_WAIT_MSG_BOX_DELAY = 3000;                      // milliseconds

  protected static final int         INTRA_FIELD_GAP           = 3;

  protected static final int         BORDER_GAP                = 10;

  public ReleaseStationMainPanel()
  {
    super();
    Initialize();
  }

  @Override
  protected void Initialize()
  {
    super.Initialize();

    // build the please wait message boxes.
    // we don't display them immediately. we wait until the timer expires to display them.

    MyJOptionPane pane = new MyJOptionPane(GetResources().getProperty(PLEASE_WAIT_MSG), JOptionPane.INFORMATION_MESSAGE);
    pane.setOptions(new Object[0]); // get rid of the buttons
    _printJobsPleaseWaitDialog = pane.createDialog(this, GetResources().getProperty(PLEASE_WAIT_TITLE));

    PleaseWaitDialogAction printJobsPleaseWaitDialogAction = new PleaseWaitDialogAction(_printJobsPleaseWaitDialog);

    _printJobsPleaseWaitTimer = new javax.swing.Timer(PLEASE_WAIT_MSG_BOX_DELAY, printJobsPleaseWaitDialogAction);
    _printJobsPleaseWaitTimer.setRepeats(false);
  }

  @Override
  protected Container CreateNorthPanel()
  {
    Box northPanel = Box.createVerticalBox();

    Box lineBox = Box.createHorizontalBox();
    northPanel.add(lineBox);

    lineBox.add(Box.createHorizontalStrut(BORDER_GAP));
    lineBox.add(new JLabel(_resources.getProperty(CURRENT_BALANCE), SwingConstants.LEFT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP));
    lineBox.add(new JLabel(InstallationTable.GetCachedInstallationRow().Get_currency_symbol(), SwingConstants.LEFT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP));
    _currentBalanceLabel = new JLabel(UserAccountTable.GetCachedUserAccountRow().GetFormatted_pre_payment_balance().toString(), SwingConstants.LEFT);
    lineBox.add(_currentBalanceLabel);
    lineBox.add(Box.createHorizontalGlue());
    lineBox.add(Box.createHorizontalStrut(BORDER_GAP));

    lineBox = Box.createHorizontalBox();
    northPanel.add(lineBox);
    lineBox.add(Box.createHorizontalStrut(BORDER_GAP));
    lineBox.add(new JLabel(_resources.getProperty(USAGE_MSG), SwingConstants.LEFT));
    lineBox.add(Box.createHorizontalGlue());
    lineBox.add(Box.createHorizontalStrut(BORDER_GAP));

    return northPanel;
  }

  @Override
  protected void AddActionButtonsToSouthPanel(Box lineBox)
  {
    _deleteButton = new ActionButton(new DeleteAction());
    _deleteButton.setEnabled(false);
    lineBox.add(_deleteButton);

    _releaseButton = new ActionButton(new ReleaseAction());
    _releaseButton.setEnabled(false);
    lineBox.add(_releaseButton);

    return;
  }

  @Override
  protected void SizeInitialPanel()
  {
    Dimension panelSize = getSize();
    panelSize.width *= 1.00f;
    panelSize.height *= 1.00f;
    setSize(panelSize);
  }

  @Override
  public void BuildTable()
  {
    _table = new MyJTable();
    _sorter = new SortFilterTableModel(_table);

    ManageButtons manageButtons = new ManageButtons();
    _table.addMouseListener(manageButtons);

    _table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _table.setCellSelectionEnabled(false);
    _table.setColumnSelectionAllowed(false);
    _table.setRowSelectionAllowed(true);
    _table.setShowVerticalLines(true);
    _table.setShowHorizontalLines(true);

    _tableModel = new ReleaseStationTableModel(_table, this);
    _sorter.SetTableModel(_tableModel);
    _table.setModel(_sorter);
    _table.doLayout();
  }

  @Override
  public void RefreshList()
  {
    _deleteButton.setEnabled(false);
    _releaseButton.setEnabled(false);

    _printJobsPleaseWaitTimer.restart();

    // i have to put this in another thread. otherwise, the msg box doesn't
    // have cycles to display its text
    new SwingWorker<Object, Object>()
      {
        @Override
        public Object doInBackground()
        {
          _showButton.setEnabled(false);
          _tableModel.PopulateTable();
          _sorter.TableModelChanged();
          _table.doLayout();
          SetTableColumnWidths();
          _showButton.setEnabled(true);

          return null;
        }

        @Override
        public void done()
        {
          _printJobsPleaseWaitTimer.stop();
          _printJobsPleaseWaitDialog.setVisible(false);
          repaint();
        }
      }.execute();
  }

  protected void SetTableColumnWidths()
  {
    Graphics g = _table.getGraphics();
    if (g != null)
    {
      FontMetrics fm = _table.getGraphics().getFontMetrics();
      TableColumnModel tableColumnModel = _table.getColumnModel();
      for (Enumeration<?> enumerator = tableColumnModel.getColumns(); enumerator.hasMoreElements();)
      {
        TableColumn tableColumn = (TableColumn) enumerator.nextElement();
        int stringWidth = fm.stringWidth(tableColumn.getHeaderValue().toString());
        int additionalWidth = 10;
        if (tableColumn.getModelIndex() == 1)
        {
          additionalWidth = 60;
        }
        else
          if (tableColumn.getModelIndex() == 3)
          {
            additionalWidth = 30;
          }
        tableColumn.setPreferredWidth(stringWidth + additionalWidth);
      }
    }
  }

  @Override
  protected Resources GetResources()
  {
    return __staticResources;
  }

  protected void CallEnableButtons()
  {
    boolean enabledFlag = (_table.getSelectedRowCount() > 0);
    EnableButtons(enabledFlag);
  }

  public void EnableButtons(boolean enable)
  {
    _deleteButton.setEnabled(enable);
    _releaseButton.setEnabled(enable);
  }

  protected class ManageButtons
    extends MouseAdapter
    implements TableModelListener
  {

    @Override
    public void mouseClicked(MouseEvent event)
    {
      CallEnableButtons();
    }

    @Override
    public void tableChanged(TableModelEvent e)
    {
      CallEnableButtons();
    }
  }

  protected class PleaseWaitDialogAction
    implements ActionListener
  {
    private JDialog _dialog = null;

    public PleaseWaitDialogAction(JDialog dialog)
    {
      _dialog = dialog;
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {
      _dialog.setVisible(true);
    }

  }

  protected void DeleteAction()
  {
    // delete the selected job(s)
    LPDClient.JobInfo[] selectedInfoRows = _tableModel.GetSelectedJobInfoRows();
    PrintJobHeaderRow[] selectedHeaderRows = _tableModel.GetSelectedJobHeaderRows();

    for (int indx = 0; indx < selectedInfoRows.length; indx++)
    {
      LPDClient.JobInfo selectedJobInfoRow = selectedInfoRows[indx];
      PrintJobHeaderRow selectedHeaderRow = selectedHeaderRows[indx];

      // delete the job from the on hold table and the properties table
      if ((selectedHeaderRow != null) && (selectedHeaderRow.getClass() == PrintJobOnHoldHeaderRow.class))
      {
        selectedHeaderRow.Remove();
      }

      // delete the job from the print server
      LPDClient lpdClient = new LPDClient(selectedJobInfoRow._printServer);
      try
      {
        lpdClient.RemoveJob(selectedJobInfoRow);
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
      }
      lpdClient.Done();
    }
    RefreshList();
  }

  protected class DeleteAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -142415441171755798L;

    /**
     * 
     */
    

    public DeleteAction()
    {
      putValue(Action.NAME, _resources.getProperty(DELETE).toString());
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(DELETE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      DeleteAction();
    }
  }

  protected void ReleaseAction()
  {
    // get the latest version of the user's account in case he has multiple release stations up and running
    UserAccountTable.GetCachedUserAccountRow(UserAccountTable.GetCachedUserAccountRow().Get_user_account_id());
    // release the selected job(s)
    LPDClient.JobInfo[] selectedJobInfoRows = _tableModel.GetSelectedJobInfoRows();
    PrintJobOnHoldHeaderRow[] selectedJobHeaderRows = _tableModel.GetSelectedJobHeaderRows();
    for (int indx = 0; indx < selectedJobInfoRows.length; indx++)
    {
      LPDClient.JobInfo selectedJobInfoRow = selectedJobInfoRows[indx];
      PrintJobOnHoldHeaderRow selectedJobHeaderRow = selectedJobHeaderRows[indx];
      // check to see if the user has enough money in his account
      Double currentBalance = UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance();
      Double jobCost = selectedJobHeaderRow.Get_cost();
      if (currentBalance.doubleValue() < jobCost.doubleValue())
      {
        MessageFormat msgFormat = new MessageFormat(_resources.getProperty(INSUFFICIENT_FUNDS_MSG));
        Object[] substitutionValues = new Object[] { selectedJobInfoRow._jobNumber };
        String msgString = msgFormat.format(substitutionValues).trim();
        // display an error message and go on to the next job
        MessageDialog.ShowErrorMessageDialog(this, msgString, _resources.getProperty(INSUFFICIENT_FUNDS_TITLE));
      }
      else
      {
        try
        {
          // release the job to the printer
          LPDClient lpdClient = new LPDClient(selectedJobInfoRow._printServer);
          lpdClient.ReleaseJob(selectedJobInfoRow);

          lpdClient.Done();

          // move the print job from the on hold table to the printed table
          InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobOnHoldHeaderTable().MovePrintJobOnHoldHeader(selectedJobHeaderRow);

          // charge the user's account
          UserAccountTable.GetCachedUserAccountRow().DeductFromPrePaymentBalance(jobCost.doubleValue());
          UserAccountTable.GetCachedUserAccountRow().Update();
        }
        catch (Exception excp)
        {
          excp.printStackTrace();
        }
      }
    }
    _currentBalanceLabel.setText(UserAccountTable.GetCachedUserAccountRow().Get_pre_payment_balance().toString());
    _currentBalanceLabel.invalidate();
    RefreshList();
    NotifyListenersPanelStateChange(PanelNotifier.ACTION_REFRESH);
  }

  protected class ReleaseAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -5501753146621976661L;

    /**
     * 
     */
    

    public ReleaseAction()
    {
      putValue(Action.NAME, _resources.getProperty(RELEASE));
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(RELEASE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      ReleaseAction();
    }
  }

  static
  {
    __staticResources = Resources.CreateApplicationResources(ReleaseStationMainPanel.class.getName());
  }

}
