package shackelford.floyd.printmanagementsystem.printercontrolstation;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingWorker;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import shackelford.floyd.printmanagementsystem.common.ActionButton;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MyJOptionPane;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.PanelListener;
import shackelford.floyd.printmanagementsystem.common.PanelNotifier;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.SortFilterTableModel;
import shackelford.floyd.printmanagementsystem.common.SystemUtilities;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobOnHoldHeaderRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.UserAccountRow;


/**
 * <p>Title: PrinterControlMainPanel</p>
 * <p>Description:
 * <ul>
 * <li>Displays a list of the printers and print jobs. 
 * <li>Provides a number of commands for the user to manipulate the print jobs.
 * </ul>
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrinterControlMainPanel
  extends JFrame
  implements PanelNotifier
{

  /**
   * Eclipse generated value
   */
  private static final long                   serialVersionUID                  = 135485206662683694L;

  /*
   * GUI components
   */
  protected JScrollPane                       _printersScrollPane;

  protected JScrollPane                       _printJobsScrollPane;

  protected MyJTable                          _printersTable;

  protected MyJTable                          _printJobsTable;

  protected PrinterControlPrintersTableModel  _printersTableModel;

  protected SortFilterTableModel              _printersSorter;

  protected PrinterControlPrintJobsTableModel _printJobsTableModel;

  protected SortFilterTableModel              _printJobsSorter;

  protected ActionButton                      _releaseButton;

  protected ActionButton                      _deleteButton;

  protected ActionButton                      _refreshPrintJobsButton;

  protected ActionButton                      _refreshPrintersButton;

  /*
   * GUI containers
   */
  protected Container                         _northPanel;

  protected Container                         _southPanel;

  protected Container                         _eastPanel;

  protected Container                         _westPanel;

  protected Container                         _centerPanel;

  /**
   * _selectedPrinter: set to the selection in the printers list
   */
  protected LPDClient.PrinterInfo             _selectedPrinter;

  /**
   * _panelListeners: the listeners for panel events
   */
  protected ArrayList<PanelListener>          _panelListeners;

  /**
   * _printServersPleaseWaitDialog: the dialog that is displayed when we are polling the available printers
   */
  protected JDialog                           _printServersPleaseWaitDialog;

  /**
   * _printJobsPleaseWaitDialog: the dialog that is displayed when we are polling the print jobs for a printer 
   */
  protected JDialog                           _printJobsPleaseWaitDialog;

  /**
   * _printServersPleaseWaitTimer: this timer must expire before we display the _printServersPleaseWaitDialog
   */
  protected javax.swing.Timer                 _printServersPleaseWaitTimer;

  /**
   * _printJobsPleaseWaitTimer: this timer must expire before we display the _printJobsPleaseWaitDialog
   */
  protected javax.swing.Timer                 _printJobsPleaseWaitTimer;

  /**
   * __resources: my resources
   */
  private static Resources                    __resources;

  /*
   * resource keys
   */
  protected static final String               TITLE                             = "TITLE";

  protected static final String               DISMISS                           = "DISMISS";

  protected static final String               DISMISS_TIP                       = "DISMISS_TIP";

  protected static final String               REFRESH_PRINTERS                  = "REFRESH_PRINTERS";

  protected static final String               REFRESH_PRINTERS_TIP              = "REFRESH_PRINTERS_TIP";

  protected static final String               REFRESH_PRINT_JOBS                = "REFRESH_PRINT_JOBS";

  protected static final String               REFRESH_PRINT_JOBS_TIP            = "REFRESH_PRINT_JOBS_TIP";

  protected static final String               RELEASE                           = "RELEASE";

  protected static final String               RELEASE_TIP                       = "RELEASE_TIP";

  protected static final String               DELETE                            = "DELETE";

  protected static final String               DELETE_TIP                        = "DELETE_TIP";

  protected static final String               PLEASE_WAIT_ALL_MSG               = "PLEASE_WAIT_ALL_MSG";

  protected static final String               PLEASE_WAIT_ALL_TITLE             = "PLEASE_WAIT_ALL_TITLE";

  protected static final String               PLEASE_WAIT_THIS_SERVER_MSG       = "PLEASE_WAIT_THIS_SERVER_MSG";

  protected static final String               PLEASE_WAIT_THIS_SERVER_TITLE     = "PLEASE_WAIT_THIS_SERVER_TITLE";

  protected static final String               INSUFFICIENT_FUNDS_MSG            = "INSUFFICIENT_FUNDS_MSG";

  protected static final String               INSUFFICIENT_FUNDS_TITLE          = "INSUFFICIENT_FUNDS_TITLE";

  protected static final int                  TABLE_PREFERRED_WIDTH             = 815;

  protected static final int                  PRINTERS_TABLE_PREFERRED_HEIGHT   = 100;

  protected static final int                  PRINT_JOBS_TABLE_PREFERRED_HEIGHT = 200;

  protected static final int                  PLEASE_WAIT_MSG_BOX_DELAY         = 3000;                           // milliseconds

  protected static final int                  INTER_FIELD_GAP_WIDTH             = 10;

  protected static final int                  INTRA_FIELD_GAP_WIDTH             = 5;

  protected static final int                  HORIZONTAL_STRUT                  = 3;

  /**
   * Constructor
   */
  public PrinterControlMainPanel()
  {
    super();
    Initialize();
  }

  /**
   * "2 phase" creation
   */
  protected void Initialize()
  {
    addWindowListener(new WindowListener());

    setIconImage(SystemUtilities.GetLogoIconImage());

    setTitle(GetResources().getProperty(TITLE));
    setResizable(true);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    // build the please wait message boxes.
    // we don't display them immediately. we wait until the timer expires to display them.

    MyJOptionPane pane = new MyJOptionPane(GetResources().getProperty(PLEASE_WAIT_ALL_MSG), JOptionPane.INFORMATION_MESSAGE);
    pane.setOptions(new Object[0]); // get rid of the buttons
    _printServersPleaseWaitDialog = pane.createDialog(this, GetResources().getProperty(PLEASE_WAIT_ALL_TITLE));

    PleaseWaitDialogAction printServersPleaseWaitDialogAction = new PleaseWaitDialogAction(_printServersPleaseWaitDialog);

    _printServersPleaseWaitTimer = new javax.swing.Timer(PLEASE_WAIT_MSG_BOX_DELAY, printServersPleaseWaitDialogAction);
    _printServersPleaseWaitTimer.setRepeats(false);

    pane = new MyJOptionPane(GetResources().getProperty(PLEASE_WAIT_THIS_SERVER_MSG), JOptionPane.INFORMATION_MESSAGE);
    pane.setOptions(new Object[0]); // get rid of the buttons
    _printJobsPleaseWaitDialog = pane.createDialog(this, GetResources().getProperty(PLEASE_WAIT_THIS_SERVER_TITLE));

    PleaseWaitDialogAction printJobsPleaseWaitDialogAction = new PleaseWaitDialogAction(_printJobsPleaseWaitDialog);

    _printJobsPleaseWaitTimer = new javax.swing.Timer(PLEASE_WAIT_MSG_BOX_DELAY, printJobsPleaseWaitDialogAction);
    _printJobsPleaseWaitTimer.setRepeats(false);

    pack();
    PositionInitialPanel();
    validate();
  }

  /**
   * Create and intialize the North panel
   * @return Container
   */
  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * Create and initialize the Center panel
   * @return Container
   */
  protected Container CreateCenterPanel()
  {
    Box centerPanel = Box.createVerticalBox();

    // printers
    _printersScrollPane = new JScrollPane();
    centerPanel.add(_printersScrollPane);
    _printersScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    _printersScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    BuildPrintersTable();
    _printersScrollPane.setViewportView(_printersTable);
    JViewport viewport = _printersScrollPane.getViewport();
    Dimension dim = viewport.getViewSize();
    dim.width = TABLE_PREFERRED_WIDTH;
    dim.height = PRINTERS_TABLE_PREFERRED_HEIGHT;
    viewport.setPreferredSize(dim);

    // print jobs
    _printJobsScrollPane = new JScrollPane();
    centerPanel.add(_printJobsScrollPane);
    _printJobsScrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    _printJobsScrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    BuildPrintJobsTable();
    _printJobsScrollPane.setViewportView(_printJobsTable);
    viewport = _printJobsScrollPane.getViewport();
    dim = viewport.getViewSize();
    dim.width = TABLE_PREFERRED_WIDTH;
    dim.height = PRINT_JOBS_TABLE_PREFERRED_HEIGHT;
    viewport.setPreferredSize(dim);

    return centerPanel;
  }

  /**
   * Create and initialize the West panel
   * @return Container
   */
  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * Create and initialize the East panel
   * @return Container
   */
  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * Create and initialize the South panel
   * @return Container
   */
  protected Container CreateSouthPanel()
  {
    Box southPanel = Box.createHorizontalBox();

    southPanel.add(Box.createHorizontalGlue());

    _deleteButton = new ActionButton(new DeleteAction());
    _deleteButton.setEnabled(false);
    southPanel.add(_deleteButton);

    _releaseButton = new ActionButton(new ReleaseAction(this));
    _releaseButton.setEnabled(false);
    southPanel.add(_releaseButton);

    _refreshPrintJobsButton = new ActionButton(new RefreshPrintJobsAction());
    _refreshPrintJobsButton.setEnabled(false);
    southPanel.add(_refreshPrintJobsButton);

    _refreshPrintersButton = new ActionButton(new RefreshPrintersAction());
    _refreshPrintersButton.setEnabled(false);
    southPanel.add(_refreshPrintersButton);

    southPanel.add(new ActionButton(new DismissAction()));
    southPanel.add(Box.createHorizontalStrut(HORIZONTAL_STRUT));

    return southPanel;
  }

  /**
   * position the panel in the center of the screen
   */
  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  /**
   * Create and define the printers list
   */
  protected void BuildPrintersTable()
  {
    _printersTable = new MyJTable();
    _printersSorter = new SortFilterTableModel(_printersTable);

    _printersTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _printersTable.setCellSelectionEnabled(false);
    _printersTable.setColumnSelectionAllowed(false);
    _printersTable.setRowSelectionAllowed(true);
    _printersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    _printersTable.setShowVerticalLines(true);
    _printersTable.setShowHorizontalLines(true);

    _printersTableModel = new PrinterControlPrintersTableModel(_printersTable, this);
    _printersSorter.SetTableModel(_printersTableModel);
    _printersTable.setModel(_printersSorter);
    _printersTable.doLayout();
  }

  /**
   * Create and define the print jobs list
   */
  protected void BuildPrintJobsTable()
  {
    _printJobsTable = new MyJTable();
    _printJobsSorter = new SortFilterTableModel(_printJobsTable);

    _printJobsTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _printJobsTable.setCellSelectionEnabled(false);
    _printJobsTable.setColumnSelectionAllowed(false);
    _printJobsTable.setRowSelectionAllowed(true);
    _printJobsTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    _printJobsTable.setShowVerticalLines(true);
    _printJobsTable.setShowHorizontalLines(true);

    _printJobsTableModel = new PrinterControlPrintJobsTableModel(_printJobsTable, this);
    _printJobsSorter.SetTableModel(_printJobsTableModel);
    _printJobsTable.setModel(_printJobsSorter);
    _printJobsTable.doLayout();
  }

  /**
   * Refresh the printers list with the latest available printers
   */
  public void RefreshPrintersList()
  {
    EnablePrintersButtons(false);
    SetSelectedPrinter(null);

    _printServersPleaseWaitTimer.restart();

    // i have to put this in another thread. otherwise, the msg box doesn't
    // have cycles to display its text
    new SwingWorker<Object, Object>()
      {
        @Override
        public Object doInBackground()
        {
          _refreshPrintersButton.setEnabled(false);
          _printersTableModel.PopulateTable();
          _printersSorter.TableModelChanged();
          _printersTable.doLayout();
          SetPrintersTableColumnWidths();

          return null;
        }

        @Override
        public void done()
        {
          _refreshPrintersButton.setEnabled(true);
          _printServersPleaseWaitTimer.stop();
          _printServersPleaseWaitDialog.setVisible(false);
          RefreshPrintJobsList();
        }
      }.execute();
  }

  /**
   * Refresh the print jobs list for the selected printer
   */
  public void RefreshPrintJobsList()
  {
    EnablePrintJobsButtons(false);

    _printJobsPleaseWaitTimer.restart();

    // i have to put this in another thread. otherwise, the msg box doesn't
    // have cycles to display its text
    new SwingWorker<Object, Object>()
      {
        @Override
        public Object doInBackground()
        {
          _refreshPrintJobsButton.setEnabled(false);
          _printJobsTableModel.PopulateTable(_selectedPrinter);
          _printJobsSorter.TableModelChanged();
          _printJobsTable.doLayout();
          SetPrintJobsTableColumnWidths();

          return null;
        }

        @Override
        public void done()
        {
          _refreshPrintJobsButton.setEnabled(_selectedPrinter != null);
          _printJobsPleaseWaitTimer.stop();
          _printJobsPleaseWaitDialog.setVisible(false);
          repaint();
        }
      }.execute();
  }

  /**
   * Make the printers table column widths somewhat fit the expected data
   */
  protected void SetPrintersTableColumnWidths()
  {
    Graphics g = _printersTable.getGraphics();
    if (g != null)
    {
      FontMetrics fm = _printersTable.getGraphics().getFontMetrics();
      TableColumnModel tableColumnModel = _printersTable.getColumnModel();
      for (Enumeration<TableColumn> enumerator = tableColumnModel.getColumns(); enumerator.hasMoreElements();)
      {
        TableColumn tableColumn = enumerator.nextElement();
        int stringWidth = fm.stringWidth(tableColumn.getHeaderValue().toString());
        int additionalWidth = 10;
        //        if (tableColumn.getModelIndex() == 1)
        //        {
        //          additionalWidth = 60;
        //        }
        tableColumn.setPreferredWidth(stringWidth + additionalWidth);
      }
    }
  }

  /**
   * Make the print jobs table column widths somewhat fit the expected data
   */
  protected void SetPrintJobsTableColumnWidths()
  {
    Graphics g = _printJobsTable.getGraphics();
    if (g != null)
    {
      FontMetrics fm = _printJobsTable.getGraphics().getFontMetrics();
      TableColumnModel tableColumnModel = _printJobsTable.getColumnModel();
      for (Enumeration<TableColumn> enumerator = tableColumnModel.getColumns(); enumerator.hasMoreElements();)
      {
        TableColumn tableColumn = enumerator.nextElement();
        int stringWidth = fm.stringWidth(tableColumn.getHeaderValue().toString());
        int additionalWidth = 10;
        if (tableColumn.getModelIndex() == 1)
        {
          additionalWidth = 60;
        }
        tableColumn.setPreferredWidth(stringWidth + additionalWidth);
      }
    }
  }

  /**
   * I10N
   * @return Resources
   */
  protected Resources GetResources()
  {
    return __resources;
  }

  /**
   * When a printer is selected in the printer list, this gets called.
   * @param printerInfo
   */
  public void SetSelectedPrinter(LPDClient.PrinterInfo printerInfo)
  {
    _selectedPrinter = printerInfo;
    EnablePrintersButtons(_selectedPrinter != null);
  }

  /**
   * Toggle the state of these buttons when a print job is selected/deselected.
   * @param enable
   */
  public void EnablePrintJobsButtons(boolean enable)
  {
    _deleteButton.setEnabled(enable);
    _releaseButton.setEnabled(enable);
  }

  /**
   * Toggle the state of these buttons when a printer is selected/deselected.
   * @param enable
   */
  public void EnablePrintersButtons(boolean enable)
  {
    _refreshPrintJobsButton.setEnabled(enable);
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.PanelNotifier#AddPanelListener(shackelford.floyd.printmanagementsystem.common.PanelListener)
   */
  @Override
  public void AddPanelListener(PanelListener listener)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_panelListeners.indexOf(listener) == -1)
      {
        _panelListeners.add(listener);
      }
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.PanelNotifier#AddPanelListeners(java.util.ArrayList)
   */
  @Override
  public void AddPanelListeners(ArrayList<PanelListener> listeners)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddPanelListener((PanelListener) (listeners.get(i)));
      }
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.PanelNotifier#RemovePanelListener(shackelford.floyd.printmanagementsystem.common.PanelListener)
   */
  @Override
  public void RemovePanelListener(PanelListener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _panelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.PanelNotifier#NotifyListenersEndOfPanel(char)
   */
  @Override
  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.PanelNotifier#NotifyListenersPanelStateChange(char)
   */
  @Override
  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.PanelStateChange(action);
      }
    }
  }

  protected class WindowListener
    extends WindowAdapter
  {
    @Override
    public void windowClosing(WindowEvent event)
    {
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
  }

  protected class PleaseWaitDialogAction
    implements ActionListener
  {
    private JDialog _dialog = null;

    public PleaseWaitDialogAction(JDialog dialog)
    {
      _dialog = dialog;
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {
      _dialog.setVisible(true);
    }

  }

  protected class RefreshPrintersAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -4394303447865600677L;

    public RefreshPrintersAction()
    {
      putValue(Action.NAME, GetResources().getProperty(REFRESH_PRINTERS));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(REFRESH_PRINTERS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      RefreshPrintersList();
    }
  }

  protected class RefreshPrintJobsAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 5287422996846569611L;

    public RefreshPrintJobsAction()
    {
      putValue(Action.NAME, GetResources().getProperty(REFRESH_PRINT_JOBS));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(REFRESH_PRINT_JOBS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      RefreshPrintJobsList();
    }
  }

  protected class DismissAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -8372334289389605836L;

    public DismissAction()
    {
      putValue(Action.NAME, GetResources().getProperty(DISMISS));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(DISMISS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
  }

  protected class ReleaseAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long             serialVersionUID = -5602018105290188708L;

    private final PrinterControlMainPanel _mainPanel;

    public ReleaseAction(PrinterControlMainPanel mainPanel)
    {
      putValue(Action.NAME, GetResources().getProperty(RELEASE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(RELEASE_TIP));
      _mainPanel = mainPanel;
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      // release the selected job(s)
      LPDClient.JobInfo[] selectedJobInfoRows = _printJobsTableModel.GetSelectedJobInfoRows();
      PrintJobHeaderRow[] selectedJobHeaderRows = _printJobsTableModel.GetSelectedJobHeaderRows();
      for (int indx = 0; indx < selectedJobInfoRows.length; indx++)
      {
        LPDClient.JobInfo selectedJobInfoRow = selectedJobInfoRows[indx];
        if (selectedJobInfoRow._status.equals(LPDClient.JOB_STATUS_HOLD) == true)
        {
          PrintJobOnHoldHeaderRow selectedJobHeaderRow = (PrintJobOnHoldHeaderRow) (selectedJobHeaderRows[indx]);
          if (selectedJobHeaderRow != null)
          {
            // check to see if the user has enough money in his account
            UserAccountRow userAccountRow = InstallationDatabase.GetDefaultInstallationDatabase().GetUserAccountTable().GetRowForUserAccountID(selectedJobInfoRow._userAccountID);
            Double currentBalance = userAccountRow.Get_pre_payment_balance();
            Double jobCost = selectedJobHeaderRow.Get_cost();
            if (currentBalance.doubleValue() < jobCost.doubleValue())
            {
              MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(INSUFFICIENT_FUNDS_MSG));
              Object[] substitutionValues = new Object[] { selectedJobInfoRow._userAccountID, selectedJobInfoRow._jobNumber };
              String msgString = msgFormat.format(substitutionValues).trim();
              // display an error message and go on to the next job
              MessageDialog.ShowErrorMessageDialog(_mainPanel, msgString, GetResources().getProperty(INSUFFICIENT_FUNDS_TITLE));
            }
            else
            {
              try
              {
                // charge the user's account
                userAccountRow.Set_pre_payment_balance(userAccountRow.Get_pre_payment_balance().doubleValue() - jobCost.doubleValue());
                userAccountRow.Update();

                // move the print job from the on hold table to the printed table
                InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobOnHoldHeaderTable().MovePrintJobOnHoldHeader(selectedJobHeaderRow);

                // release the job to the printer
                LPDClient lpdClient = new LPDClient(selectedJobInfoRow._printServer);
                lpdClient.ReleaseJob(selectedJobInfoRow);

                lpdClient.Done();
              }
              catch (Exception excp)
              {
                excp.printStackTrace();
              }
            }
          }
        }
      }
      RefreshPrintJobsList();
    }
  }

  protected class DeleteAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 1091718205243506949L;

    public DeleteAction()
    {
      putValue(Action.NAME, GetResources().getProperty(DELETE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(DELETE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      // delete the selected job(s)
      LPDClient.JobInfo[] selectedInfoRows = _printJobsTableModel.GetSelectedJobInfoRows();
      PrintJobHeaderRow[] selectedHeaderRows = _printJobsTableModel.GetSelectedJobHeaderRows();

      for (int indx = 0; indx < selectedInfoRows.length; indx++)
      {
        LPDClient.JobInfo selectedJobInfoRow = selectedInfoRows[indx];
        PrintJobHeaderRow selectedHeaderRow = selectedHeaderRows[indx];

        // delete the job from the on hold table and the properties table
        if ((selectedHeaderRow != null) && (selectedHeaderRow.getClass() == PrintJobHeaderRow.class))
        {
          selectedHeaderRow.Remove();
        }

        // delete the job from the print server
        LPDClient lpdClient = new LPDClient(selectedJobInfoRow._printServer);
        try
        {
          lpdClient.RemoveJob(selectedJobInfoRow);
        }
        catch (Exception excp)
        {
          excp.printStackTrace();
        }
        lpdClient.Done();
      }
      RefreshPrintJobsList();
    }
  }

  static
  {
    // I10N
    __resources = Resources.CreateApplicationResources(PrinterControlMainPanel.class.getName());
  }

}
