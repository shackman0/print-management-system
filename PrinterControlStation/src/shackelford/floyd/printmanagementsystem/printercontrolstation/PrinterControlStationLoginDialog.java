package shackelford.floyd.printmanagementsystem.printercontrolstation;

import java.awt.Container;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Properties;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;

import shackelford.floyd.printmanagementsystem.common.AbstractLoginDialog;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationTable;

/**
 * <p>Title: PrinterControlStationLoginDialog</p>
 * <p>Description:
 * The dialog at which the user logs in to be able to access the application.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrinterControlStationLoginDialog
  extends AbstractLoginDialog
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 1342191625040454196L;

  /**
   * _passphraseField: dialog component
   */
  private final JPasswordField _passphraseField         = new JPasswordField();

  /**
   * singleton to the resources file for this class. it will be properly initialized to the site language.
   */
  private static Resources     __resources;

  /**
   * INVALID_PASSPHRASE_MSG: resource key
   */
  public static final String   INVALID_PASSPHRASE_MSG   = "INVALID_PASSPHRASE_MSG";

  /**
   * INVALID_PASSPHRASE_TITLE: resource key
   */
  public static final String   INVALID_PASSPHRASE_TITLE = "INVALID_PASSPHRASE_TITLE";

  /**
   * PASSPHRASE: resource key
   */
  public static final String   PASSPHRASE               = "PASSPHRASE";

  /**
   * PASSPHRASE_TIP: resource key
   */
  public static final String   PASSPHRASE_TIP           = "PASSPHRASE_TIP";

  /**
   * Constructor
   * @param owner
   */
  public PrinterControlStationLoginDialog(Frame owner)
  {
    super(owner, true);
    Initialize();
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.AbstractDialog#CreateCenterPanel()
   */
  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // passphrase
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(PASSPHRASE), SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    _passphraseField.setColumns(FIELD_WIDTH);
    _passphraseField.setToolTipText(GetResources().getProperty(PASSPHRASE_TIP));
    centerPanel.add(_passphraseField, constraints);

    return centerPanel;
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.AbstractDialog#AssignFields()
   */
  @Override
  protected void AssignFields()
  {
    // do nothing
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.AbstractDialog#GetProperties()
   */
  @Override
  public Properties GetProperties()
  {
    Properties props = super.GetProperties();
    props.setProperty(PASSPHRASE, new String(_passphraseField.getPassword()));
    return props;
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.AbstractDialog#PanelValid()
   */
  @Override
  protected boolean PanelValid()
  {
    if (super.PanelValid() == false)
    {
      return false;
    }

    String passphrase = new String(_passphraseField.getPassword());
    if (passphrase.equals(InstallationTable.GetCachedInstallationRow().Get_installation_passphrase()) == false)
    {
      MessageDialog.ShowErrorMessageDialog(this, GetResources().getProperty(INVALID_PASSPHRASE_MSG), GetResources().getProperty(INVALID_PASSPHRASE_TITLE));
      _passphraseField.setText("");
      _passphraseField.requestFocus();
      return false;
    }

    return true;
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.AbstractDialog#GetResources()
   */
  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  /*
   * static initializer.
   */
  static
  {
    // initialize the __resources singleton
    __resources = Resources.CreateApplicationResources(PrinterControlStationLoginDialog.class.getName());
  }
}
