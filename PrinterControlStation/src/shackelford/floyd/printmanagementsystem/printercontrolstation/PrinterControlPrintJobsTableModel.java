package shackelford.floyd.printmanagementsystem.printercontrolstation;

import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.common.LPDClient.JobInfo;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;
import shackelford.floyd.printmanagementsystem.installationdatabase.PrintJobHeaderRow;


/**
 * <p>Title: PrinterControlPrintJobsTableModel</p>
 * <p>Description:
 * The data for populating the print jobs table.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrinterControlPrintJobsTableModel
  extends AbstractTableModel
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID      = 1082717504929834554L;

  /**
   * _table: the table to populate
   */
  JTable                      _table;

  /**
   * _jobInfo: the info for print jobs as received from LPD
   */
  private LPDClient.JobInfo[] _jobInfo;

  /**
   * _jobHeader: the info on the print job as found in the database
   */
  private PrintJobHeaderRow[] _jobHeader;

  /**
   * _callingPanel: the panel that we are partnered with.
   */
  PrinterControlMainPanel     _callingPanel;

  /*
   * I10N resources
   */
  private static Resources    __resources;

  private static String[]     __columnNames;

  private static final String JOB_NUMBER            = "JOB_NUMBER";

  private static final String JOB_COST              = "JOB_COST";

  private static final String NUMBER_OF_PAGES       = "NUMBER_OF_PAGES";

  private static final String HOST_JOB_NUMBER       = "HOST_JOB_NUMBER";

  private static final String PRINT_SERVER          = "PRINT_SERVER";

  private static final String PRINTER_NAME          = "PRINTER_NAME";

  private static final String SUBMITTED_BY_USER_ID  = "SUBMITTED_BY_USER_ID";

  private static final String SUBMITTED_BY_HOST     = "SUBMITTED_BY_HOST";

  private static final String STATUS                = "STATUS";

  private static final String DESCRIPTION           = "DESCRIPTION";

  private static final String USER_ACCOUNT_ID       = "USER_ACCOUNT_ID";

  private static final String SUBMITTED_BY_HOSTNAME = "SUBMITTED_BY_HOSTNAME";

  /**
   * Constructor
   * @param table
   * @param callingPanel
   */
  public PrinterControlPrintJobsTableModel(MyJTable table, PrinterControlMainPanel callingPanel)
  {
    super();
    Initialize(table, callingPanel);
  }

  /**
   * @param table
   * @param callingPanel
   */
  private void Initialize(MyJTable table, PrinterControlMainPanel callingPanel)
  {
    _table = table;
    _callingPanel = callingPanel;

    //_table.setDefaultRenderer(Object.class, new PrinterControlPrintJobsCellRenderer(this));   // todo
    _table.addMouseListener(new MouseClicked());
  }

  /**
   * @see javax.swing.table.TableModel#getRowCount()
   */
  @Override
  public int getRowCount()
  {
    if (_jobInfo == null)
    {
      return 0;
    }

    return _jobInfo.length;
  }

  /**
   * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
   */
  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @see javax.swing.table.TableModel#getColumnCount()
   */
  @Override
  public int getColumnCount()
  {
    if (__columnNames == null)
    {
      return 0;
    }
    return __columnNames.length;
  }

  /**
   * @see javax.swing.table.TableModel#getValueAt(int, int)
   */
  @Override
  public Object getValueAt(int rowNum, int colNum)
  {
    switch (colNum)
    {
      case 0:
        Integer jobNumber = null;
        try
        {
          jobNumber = new Integer(_jobInfo[rowNum]._jobNumber.intValue());
        }
        catch (NumberFormatException excp)
        {
          jobNumber = new Integer(-1);
        }
        return jobNumber;
      case 1:
        String desc = _jobInfo[rowNum]._description;
        if (desc.equals("LGKdesc") == true)
        {
          desc = "";
        }
        return new String(desc);
      case 2:
        return new String(_jobInfo[rowNum]._status);
      case 3:
        String acct = _jobInfo[rowNum]._userAccountID;
        if (acct.equals("LGKacct") == true)
        {
          acct = "";
        }
        return new String(acct);
      case 4:
        return new String(_jobInfo[rowNum]._printerName);
      case 5:
        return new String(_jobInfo[rowNum]._printServer);
      case 6:
        if (_jobHeader[rowNum] == null)
        {
          return "";
        }

        return _jobHeader[rowNum].GetFormatted_cost();
      case 7:
        if (_jobHeader[rowNum] == null)
        {
          return "";
        }

        return _jobHeader[rowNum].Get_number_of_pages().toString();
      case 8:
        return _jobInfo[rowNum]._hostJobNumber.toString();
      case 9:
        return new String(_jobInfo[rowNum]._submittedByUserID);
      case 10:
        return new String(_jobInfo[rowNum]._submittedByHost);
      case 11:
        return new String(_jobInfo[rowNum]._submittedByIP);
      default:
        return "UNKNOWN column=" + colNum + ", row=" + rowNum;
    }
  }

  /**
   * Accessor
   * @return LPDClient.JobInfo[]
   */
  public LPDClient.JobInfo[] GetJobInfoRows()
  {
    return _jobInfo;
  }

  /**
   * Accessor
   * @return LPDClient.JobInfo[]
   */
  public LPDClient.JobInfo[] GetSelectedJobInfoRows()
  {
    ArrayList<JobInfo> selectedRowsArrayList = new ArrayList<>(10);

    int[] selectedRowNumbers = _table.getSelectedRows();
    for (int indx = 0; indx < selectedRowNumbers.length; indx++)
    {
      selectedRowsArrayList.add(_jobInfo[selectedRowNumbers[indx]]);
    }
    selectedRowsArrayList.trimToSize();
    LPDClient.JobInfo[] selectedRows = new LPDClient.JobInfo[selectedRowsArrayList.size()];
    selectedRowsArrayList.toArray(selectedRows);
    return selectedRows;
  }

  /**
   * Accessor
   * @return PrintJobHeaderRow[]
   */
  public PrintJobHeaderRow[] GetSelectedJobHeaderRows()
  {
    ArrayList<PrintJobHeaderRow> selectedRowsArrayList = new ArrayList<>(10);

    int[] selectedRowNumbers = _table.getSelectedRows();
    for (int indx = 0; indx < selectedRowNumbers.length; indx++)
    {
      selectedRowsArrayList.add(_jobHeader[selectedRowNumbers[indx]]);
    }
    selectedRowsArrayList.trimToSize();
    PrintJobHeaderRow[] selectedRows = new PrintJobHeaderRow[selectedRowsArrayList.size()];
    selectedRowsArrayList.toArray(selectedRows);
    return selectedRows;
  }

  /**
   * Accessor
   * @return String[]
   */
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  /**
   * @see javax.swing.table.AbstractTableModel#getColumnName(int)
   */
  @Override
  public String getColumnName(int colNum)
  {
    return __columnNames[colNum];
  }

  /**
   * Populates the table with the print jobs. Most of the work occurs here.
   * @param printerInfo
   */
  public void PopulateTable(LPDClient.PrinterInfo printerInfo)
  {

    if (getRowCount() > 0)
    {
      fireTableRowsDeleted(0, getRowCount() - 1);
    }

    _jobInfo = null;

    if (printerInfo != null)
    {
      try
      {
        // query the specified printer for it's print jobs
        ArrayList<JobInfo> jobsInfoArrayList = new ArrayList<>(10);
        ArrayList<PrintJobHeaderRow> jobsHeaderArrayList = new ArrayList<>(10);

        LPDClient lpdClient = new LPDClient(printerInfo._printServer);
        LPDClient.JobInfo[] jobsInfo = lpdClient.GetPrinterJobsInfo(printerInfo);

        for (int kndx = 0; kndx < jobsInfo.length; kndx++)
        {
          LPDClient.JobInfo jobInfo = jobsInfo[kndx];
          if (jobInfo._status.equals(LPDClient.JOB_STATUS_CANCELLED) == false)
          {
            PrintJobHeaderRow printJobHeader = null;
            if ((jobInfo._status.equals(LPDClient.JOB_STATUS_ACTIVE) == true) || (jobInfo._status.equals(LPDClient.JOB_STATUS_DONE) == true))
            {
              printJobHeader = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobHeaderTable().GetRowForPrintJobNumber(jobInfo._jobNumber);
            }
            else
              if (jobInfo._status.equals(LPDClient.JOB_STATUS_HOLD) == true)
              {
                printJobHeader = InstallationDatabase.GetDefaultInstallationDatabase().GetPrintJobOnHoldHeaderTable().GetRowForPrintJobNumber(jobInfo._jobNumber);
              }
            // there may be other jobs on the print server, but they might not
            // be in the database. we only care about jobs that are in the database.
            if (printJobHeader != null)
            {
              jobsHeaderArrayList.add(printJobHeader);
              jobsInfoArrayList.add(jobInfo);
            }
          }
        }

        lpdClient.Done();

        jobsInfoArrayList.trimToSize();
        _jobInfo = new LPDClient.JobInfo[jobsInfoArrayList.size()];
        jobsInfoArrayList.toArray(_jobInfo);

        jobsHeaderArrayList.trimToSize();
        _jobHeader = new PrintJobHeaderRow[jobsHeaderArrayList.size()];
        jobsHeaderArrayList.toArray(_jobHeader);
      }
      catch (Exception excp)
      {
        excp.printStackTrace();
      }
    }

    //fireTableStructureChanged();
    if (getRowCount() > 0)
    {
      fireTableRowsInserted(0, getRowCount() - 1);
    }
  }

  static
  {
    // I10N ...
    __resources = Resources.CreateApplicationResources(PrinterControlPrintJobsTableModel.class.getName());

    __columnNames = new String[] { __resources.getProperty(JOB_NUMBER), __resources.getProperty(DESCRIPTION), __resources.getProperty(STATUS), __resources.getProperty(USER_ACCOUNT_ID),
    __resources.getProperty(PRINTER_NAME), __resources.getProperty(PRINT_SERVER), __resources.getProperty(JOB_COST), __resources.getProperty(NUMBER_OF_PAGES),
    __resources.getProperty(HOST_JOB_NUMBER), __resources.getProperty(SUBMITTED_BY_USER_ID), __resources.getProperty(SUBMITTED_BY_HOSTNAME), __resources.getProperty(SUBMITTED_BY_HOST) };
  }

  private class MouseClicked
    extends MouseAdapter
  {
    public MouseClicked()
    {
      super();
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      if ((event.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK)
      {
        _callingPanel.EnablePrintJobsButtons(_table.getSelectedRowCount() > 0);
      }
    }
  }

}
