package shackelford.floyd.printmanagementsystem.printercontrolstation;

import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.MessageFormat;
import java.util.ArrayList;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;

import shackelford.floyd.printmanagementsystem.common.AbstractSQL1Table;
import shackelford.floyd.printmanagementsystem.common.LPDClient;
import shackelford.floyd.printmanagementsystem.common.LPDClient.PrinterInfo;
import shackelford.floyd.printmanagementsystem.common.MessageDialog;
import shackelford.floyd.printmanagementsystem.common.MyJTable;
import shackelford.floyd.printmanagementsystem.common.Resources;
import shackelford.floyd.printmanagementsystem.installationdatabase.GatekeeperRow;
import shackelford.floyd.printmanagementsystem.installationdatabase.InstallationDatabase;


/**
 * <p>Title: PrinterControlPrintersTableModel</p>
 * <p>Description:
 * The data for populating the printers table.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class PrinterControlPrintersTableModel
  extends AbstractTableModel
{

  /**
   * Eclipse generated value
   */
  private static final long       serialVersionUID               = -8909206135481529216L;

  /**
   * _table: the table to populate
   */
  private JTable                  _table;

  /**
   * _printerInfo: the printer data we receive from LPD
   */
  private LPDClient.PrinterInfo[] _printerInfo;

  /**
   * _callingPanel: the panel that we are partnered with.
   */
  PrinterControlMainPanel         _callingPanel;

  /*
   * I10N resources
   */
  static Resources                __resources;

  private static String[]         __columnNames;

  private static final String     PRINT_SERVER                   = "PRINT_SERVER";

  private static final String     PRINTER_NAME                   = "PRINTER_NAME";

  private static final String     SPOOLING_STATUS                = "SPOOLING_STATUS";

  private static final String     PRINTING_STATUS                = "PRINTING_STATUS";

  private static final String     NUMBER_OF_ACTIVE_JOBS          = "NUMBER_OF_ACTIVE_JOBS";

  private static final String     NUMBER_OF_HELD_JOBS            = "NUMBER_OF_HELD_JOBS";

  private static final String     DESTINATION_QUEUE              = "DESTINATION_QUEUE";

  private static final String     DESTINATION_HOST               = "DESTINATION_HOST";

  private static final String     CANNOT_ACCESS_GATEKEEPER_MSG   = "CANNOT_ACCESS_GATEKEEPER_MSG";

  private static final String     CANNOT_ACCESS_GATEKEEPER_TITLE = "CANNOT_ACCESS_GATEKEEPER_TITLE";

  /**
   * Constructor
   * @param table
   * @param callingPanel
   */
  public PrinterControlPrintersTableModel(MyJTable table, PrinterControlMainPanel callingPanel)
  {
    super();
    Initialize(table, callingPanel);
  }

  /**
   * "2 phase" construction
   * @param table
   * @param callingPanel
   */
  private void Initialize(MyJTable table, PrinterControlMainPanel callingPanel)
  {
    _table = table;
    _callingPanel = callingPanel;

    //_table.setDefaultRenderer(Object.class, new PrinterControlPrintersCellRenderer(this));
    _table.addMouseListener(new MouseClicked());
  }

  /**
   * @see javax.swing.table.TableModel#getRowCount()
   */
  @Override
  public int getRowCount()
  {
    if (_printerInfo == null)
    {
      return 0;
    }
    return _printerInfo.length;
  }

  /**
   * @see javax.swing.table.AbstractTableModel#getColumnClass(int)
   */
  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    return getValueAt(0, columnIndex).getClass();
  }

  /**
   * @see javax.swing.table.TableModel#getColumnCount()
   */
  @Override
  public int getColumnCount()
  {
    if (__columnNames == null)
    {
      return 0;
    }
    return __columnNames.length;
  }

  /**
   * @see javax.swing.table.TableModel#getValueAt(int, int)
   */
  @Override
  public Object getValueAt(int rowNum, int colNum)
  {
    if (colNum == 0)
    {
      return new String(_printerInfo[rowNum]._printServer);
    }
    else
      if (colNum == 1)
      {
        return new String(_printerInfo[rowNum]._printerName);
      }
      else
        if (colNum == 2)
        {
          return new String(_printerInfo[rowNum]._printingStatus);
        }
        else
          if (colNum == 3)
          {
            return new String(_printerInfo[rowNum]._spoolingStatus);
          }
          else
            if (colNum == 4)
            {
              return String.valueOf(_printerInfo[rowNum]._numberOfActiveJobs);
            }
            else
              if (colNum == 5)
              {
                return String.valueOf(_printerInfo[rowNum]._numberOfHeldJobs);
              }
              else
                if (colNum == 6)
                {
                  return new String(_printerInfo[rowNum]._destinationQueue);
                }
                else
                  if (colNum == 7)
                  {
                    return new String(_printerInfo[rowNum]._destinationHost);
                  }

    return "UNKNOWN column=" + colNum + ", row=" + rowNum;
  }

  /**
   * @return LPDClient.PrinterInfo[]
   */
  public LPDClient.PrinterInfo[] GetPrinterInfoRows()
  {
    return _printerInfo;
  }

  /**
   * @return LPDClient.PrinterInfo[]
   */
  public LPDClient.PrinterInfo[] GetSelectedRows()
  {
    ArrayList<PrinterInfo> selectedRowsArrayList = new ArrayList<>(10);

    int[] selectedRowNumbers = _table.getSelectedRows();
    for (int indx = 0; indx < selectedRowNumbers.length; indx++)
    {
      selectedRowsArrayList.add(_printerInfo[selectedRowNumbers[indx]]);
    }
    selectedRowsArrayList.trimToSize();
    LPDClient.PrinterInfo[] selectedRows = new LPDClient.PrinterInfo[selectedRowsArrayList.size()];
    selectedRowsArrayList.toArray(selectedRows);
    return selectedRows;
  }

  /**
   * @return String[]
   */
  public String[] GetColumnNames()
  {
    return __columnNames;
  }

  /**
   * @see javax.swing.table.AbstractTableModel#getColumnName(int)
   */
  @Override
  public String getColumnName(int colNum)
  {
    return __columnNames[colNum];
  }

  /**
   * Populates the table with the printers. Most of the work occurs here.
   */
  public void PopulateTable()
  {

    if (getRowCount() > 0)
    {
      fireTableRowsDeleted(0, getRowCount() - 1);
    }

    _printerInfo = null;

    // query each gatekeeper
    ArrayList<PrinterInfo> printQueues = new ArrayList<>(10);
    ArrayList<GatekeeperRow> gatekeepersArrayList = (ArrayList<GatekeeperRow>) InstallationDatabase.GetDefaultInstallationDatabase().GetGatekeeperTable().GetRowsAsArrayList(null, // from
      null, // predicate
      GatekeeperRow.Name_ip_address(), // order by
      AbstractSQL1Table.NO_LIMIT); // limit
    for (int indx = 0; indx < gatekeepersArrayList.size(); indx++)
    {
      GatekeeperRow gatekeeper = (gatekeepersArrayList.get(indx));
      try
      {
        LPDClient lpdClient = new LPDClient(gatekeeper.Get_ip_address());
        LPDClient.PrinterInfo[] printerInfo = lpdClient.GetPrinterInfo();

        for (int jndx = 0; jndx < printerInfo.length; jndx++)
        {
          LPDClient.PrinterInfo printQueue = printerInfo[jndx];
          printQueues.add(printQueue);
        }
        lpdClient.Done();
      }
      catch (Exception excp)
      {
        // put the error message dialog into a separate thread so we can continue
        MessageFormat msgFormat = new MessageFormat(__resources.getProperty(CANNOT_ACCESS_GATEKEEPER_MSG));
        Object[] substitutionValues = new Object[] { gatekeeper.Get_ip_address() };
        String msgString = msgFormat.format(substitutionValues).trim();
        // display a message indicating that the gatekeeper is not accessible
        MessageDialog.ShowMessageDialogNoWait(_callingPanel, msgString, __resources.getProperty(CANNOT_ACCESS_GATEKEEPER_TITLE), JOptionPane.ERROR_MESSAGE);
      }
    }
    printQueues.trimToSize();
    _printerInfo = new LPDClient.PrinterInfo[printQueues.size()];
    printQueues.toArray(_printerInfo);

    //fireTableStructureChanged();
    if (getRowCount() > 0)
    {
      fireTableRowsInserted(0, getRowCount() - 1);
    }
  }

  static
  {
    // I10N ...
    __resources = Resources.CreateApplicationResources(PrinterControlPrintersTableModel.class.getName());

    __columnNames = new String[] { __resources.getProperty(PRINT_SERVER), __resources.getProperty(PRINTER_NAME), __resources.getProperty(PRINTING_STATUS), __resources.getProperty(SPOOLING_STATUS),
    __resources.getProperty(NUMBER_OF_ACTIVE_JOBS), __resources.getProperty(NUMBER_OF_HELD_JOBS), __resources.getProperty(DESTINATION_QUEUE), __resources.getProperty(DESTINATION_HOST) };
  }

  private class MouseClicked
    extends MouseAdapter
  {
    public MouseClicked()
    {
      super();
    }

    @SuppressWarnings("synthetic-access")
    @Override
    public void mouseClicked(MouseEvent event)
    {
      if ((event.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK)
      {
        int selectedRowIndx = _table.getSelectedRow();
        if (selectedRowIndx == -1)
        {
          _callingPanel.SetSelectedPrinter(null);
        }
        else
        {
          _callingPanel.SetSelectedPrinter(_printerInfo[selectedRowIndx]);
        }
        _callingPanel.RefreshPrintJobsList();
      }
    }
  }

}
