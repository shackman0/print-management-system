package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 * <p>Title: AbstractEntry1Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractEntry1Panel
  extends JFrame
  implements ListPanelNotifier, PanelNotifier, PanelListener, Notifier, Listener
{

  /**
   * Eclipse generated value
   */
  private static final long              serialVersionUID      = -6479244511706681262L;

  protected ArrayList<Listener>                    _listeners            = null;

  protected ArrayList<ListPanelListener> _listPanelListeners   = null;

  protected ArrayList<PanelListener>     _panelListeners       = null;

  protected AbstractSQL1Row              _sqlRow               = null;

  protected char                         _mode;

  protected AbstractSQL1Table            _sqlTable             = null;

  protected Container                    _northPanel           = null;

  protected Container                    _southPanel           = null;

  protected Container                    _eastPanel            = null;

  protected Container                    _westPanel            = null;

  protected Container                    _centerPanel          = null;

  protected ListPanelCursor              _listPanel            = null;

  protected ActionButton                 _saveButton           = null;

  protected ActionButton                 _cancelButton         = null;

  protected ActionButton                 _saveAndNewButton     = null;

  protected ActionButton                 _previousButton       = null;

  protected ActionButton                 _nextButton           = null;

  protected static final String          PREVIOUS              = "PREVIOUS";

  protected static final String          PREVIOUS_TIP          = "PREVIOUS_TIP";

  protected static final String          NEXT                  = "NEXT";

  protected static final String          NEXT_TIP              = "NEXT_TIP";

  protected static final char            MODE_NEW              = 'n';

  protected static final char            MODE_UPDATE           = 'u';

  protected static final String          TRUE                  = "TRUE";

  protected static final String          FALSE                 = "FALSE";

  protected static final String          TITLE                 = "TITLE";

  protected static final String          SAVE_AND_NEW          = "SAVE_AND_NEW";

  protected static final String          SAVE_AND_NEW_TIP      = "SAVE_AND_NEW_TIP";

  protected static final String          SAVE                  = "SAVE";

  protected static final String          SAVE_TIP              = "SAVE_TIP";

  protected static final String          CANCEL                = "CANCEL";

  protected static final String          CANCEL_TIP            = "CANCEL_TIP";

  protected static final char            SAVE_EVENT            = 's';

  protected static final char            CANCEL_EVENT          = 'x';

  protected static final int             INTER_FIELD_GAP_WIDTH = 10;

  protected static final int             INTRA_FIELD_GAP_WIDTH = 5;

  protected static final int             HORIZONTAL_STRUT1     = 3;

  public AbstractEntry1Panel()
  {
    super();
  }

  public void Initialize(ListPanelCursor listPanel, AbstractSQL1Table sqlTable, AbstractSQL1Row sqlRow) // null => mode_new
  {
    _listPanel = listPanel;
    _sqlTable = sqlTable;
    _sqlRow = sqlRow;

    if (_sqlRow == null)
    {
      _sqlRow = GetSQL1Table().CreateRow();
      _mode = MODE_NEW;
    }
    else
    {
      _mode = MODE_UPDATE;
    }

    AdjustSQLRow();

    addWindowListener(new WindowListener());
    setIconImage(SystemUtilities.GetLogoIconImage());
    setTitle(GetResources().getProperty(TITLE).toString());
    setResizable(false);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    AssignRowToFields();

    pack();
    SizePanel();
    PositionInitialPanel();
    validate();
  }

  @Override
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    if (visible == true)
    {
      toFront();
    }
  }

  protected void SizePanel()
  {
    // do nothing. this is for children
  }

  protected void AdjustSQLRow()
  {
    // do nothing. this is for children
  }

  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateCenterPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    Box verticalBox = Box.createVerticalBox();
    Container optionalInfoRow = GetOptionalInfoRow();
    if (optionalInfoRow != null)
    {
      verticalBox.add(optionalInfoRow);
    }

    Container optionalButtonRow = GetOptionalButtonRow();
    if (optionalButtonRow != null)
    {
      verticalBox.add(optionalButtonRow);
    }

    Box buttonBox = Box.createHorizontalBox();
    buttonBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT1));
    buttonBox.add(new HelpButton(this));

    _previousButton = new ActionButton(new PreviousAction());
    _previousButton.setEnabled((_listPanel != null) && (_mode == MODE_UPDATE));
    buttonBox.add(_previousButton);

    _nextButton = new ActionButton(new NextAction());
    _nextButton.setEnabled((_listPanel != null) && (_mode == MODE_UPDATE));
    buttonBox.add(_nextButton);

    buttonBox.add(Box.createHorizontalGlue());
    AddActionButtonsToSouthPanel(buttonBox);
    _saveAndNewButton = new ActionButton(new SaveAndNewAction());
    buttonBox.add(_saveAndNewButton);
    _saveButton = new ActionButton(new SaveAction());
    buttonBox.add(_saveButton);
    _cancelButton = new ActionButton(new CancelAction());
    buttonBox.add(_cancelButton);
    buttonBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT1));
    verticalBox.add(buttonBox);

    return verticalBox;
  }

  protected Box GetOptionalButtonRow()
  {
    return Box.createHorizontalBox();
  }

  protected Box GetOptionalInfoRow()
  {
    return Box.createHorizontalBox();
  }

  protected void AddActionButtonsToSouthPanel(Container southPanel)
  {
    // place holder
  }

  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  @Override
  public void AddListPanelListener(ListPanelListener listener)
  {
    if (_listPanelListeners == null)
    {
      _listPanelListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_listPanelListeners.indexOf(listener) == -1)
      {
        _listPanelListeners.add(listener);
      }
    }
  }

  @Override
  public void AddListPanelListeners(ArrayList<ListPanelListener> listeners)
  {
    if (_listPanelListeners == null)
    {
      _listPanelListeners = new ArrayList<>();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddListPanelListener((listeners.get(i)));
      }
    }
  }

  @Override
  public void RemoveListPanelListener(ListPanelListener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _listPanelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _listPanelListeners.remove(listenerIndex);
      }
    }
  }

  @Override
  public void NotifyListPanelListeners(boolean resetListenerList)
  {
    if (_listPanelListeners != null)
    {
      for (Iterator<ListPanelListener> iterator = _listPanelListeners.iterator(); iterator.hasNext();)
      {
        ListPanelListener listener = iterator.next();
        listener.RefreshList();
      }
      if (resetListenerList == true)
      {
        _listPanelListeners = null;
      }
    }
  }

  protected boolean ModeIsNew()
  {
    return _mode == MODE_NEW;
  }

  protected boolean ModeIsUpdate()
  {
    return _mode == MODE_UPDATE;
  }

  protected abstract boolean FieldsValid();

  protected abstract void AssignRowToFields();

  protected abstract void AssignFieldsToRow();

  @Override
  public void Notification(char event)
  {
    // do nothing
  }

  @Override
  public void EndOfPanel(char action)
  {
    // do nothing
  }

  @Override
  public void PanelStateChange(char action)
  {
    // do nothing
  }

  @Override
  public void AddListener(Listener listener)
  {
    if (_listeners == null)
    {
      _listeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_listeners.indexOf(listener) == -1)
      {
        _listeners.add(listener);
      }
    }
  }

  @Override
  public void AddListeners(ArrayList<Listener> listeners)
  {
    _listeners.addAll(listeners);
  }

  @Override
  public void RemoveListener(Listener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _listeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _listeners.remove(listenerIndex);
      }
    }
  }

  @Override
  public void NotifyListeners(char event, boolean resetListenerList)
  {
    if (_listeners != null)
    {
      for (Iterator<Listener> iterator = _listeners.iterator(); iterator.hasNext();)
      {
        Listener listener = iterator.next();
        listener.Notification(event);
      }
      if (resetListenerList == true)
      {
        _listeners = null;
      }
    }
  }

  @Override
  public void AddPanelListener(PanelListener listener)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_panelListeners.indexOf(listener) == -1)
      {
        _panelListeners.add(listener);
      }
    }
  }

  @Override
  public void AddPanelListeners(ArrayList<PanelListener> listeners)
  {
    _panelListeners.addAll(listeners);
  }

  @Override
  public void RemovePanelListener(PanelListener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _panelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  @Override
  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  @Override
  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.PanelStateChange(action);
      }
    }
  }

  private AbstractSQL1Row GetSQL1Row()
  {
    return _sqlRow;
  }

  private AbstractSQL1Table GetSQL1Table()
  {
    return _sqlTable;
  }

  public abstract Resources GetResources();

  public void WindowClosing()
  {
    NotifyListeners(CANCEL_EVENT, true);
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_CANCEL);
    dispose();
  }

  protected class WindowListener
    extends WindowAdapter
  {

    @Override
    public void windowClosing(WindowEvent event)
    {
      WindowClosing();
    }
  }

  protected void StartingSave()
  {
    // do nothing
  }

  protected void EndingSave(boolean rc)
  {
    // do nothing
  }

  protected boolean SaveAction()
  {
    _saveButton.setSelected(true);

    if (FieldsValid() == false)
    {
      return false;
    }

    AssignFieldsToRow();

    boolean rc;

    StartingSave();

    if (_mode == MODE_NEW)
    {
      rc = GetSQL1Row().Insert();
    }
    else
    {
      rc = GetSQL1Row().Update();
    }

    EndingSave(rc);

    if (rc == true)
    {
      NotifyListPanelListeners(true);
      NotifyListeners(SAVE_EVENT, true);
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_SAVE);
      _mode = MODE_UPDATE;
      dispose();
    }

    return rc;

  }

  protected class SaveAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 8508145524093966950L;

    public SaveAction()
    {
      putValue(Action.NAME, GetResources().getProperty(SAVE).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(SAVE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      SaveAction();
    }
  }

  protected void CancelAction()
  {
    _cancelButton.setSelected(true);
    NotifyListeners(CANCEL_EVENT, true);
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_CANCEL);
    dispose();
  }

  protected class CancelAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 667913977659642588L;

    public CancelAction()
    {
      putValue(Action.NAME, GetResources().getProperty(CANCEL).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(CANCEL_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      CancelAction();
    }
  }

  protected boolean PreviousAction()
  {
    _previousButton.setSelected(true);
    _sqlRow = _listPanel.GetPreviousRow(_sqlRow);
    AdjustSQLRow();
    AssignRowToFields();
    pack();
    return true;
  }

  protected class PreviousAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -695987065517714689L;

    public PreviousAction()
    {
      putValue(Action.NAME, GetResources().getProperty(PREVIOUS).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(PREVIOUS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PreviousAction();
    }
  }

  protected boolean NextAction()
  {
    _nextButton.setSelected(true);
    _sqlRow = _listPanel.GetNextRow(_sqlRow);
    AdjustSQLRow();
    AssignRowToFields();
    pack();
    return true;
  }

  protected class NextAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -6554785858295026328L;

    public NextAction()
    {
      putValue(Action.NAME, GetResources().getProperty(NEXT).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(NEXT_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      NextAction();
    }
  }

  protected void StartingSaveAndNew()
  {
    // do nothing
  }

  protected void EndingSaveAndNew(boolean rc)
  {
    // do nothing
  }

  protected boolean SaveAndNewAction()
  {
    _saveAndNewButton.setSelected(true);

    if (FieldsValid() == false)
    {
      return false;
    }
    boolean rc = true;

    AssignFieldsToRow();

    StartingSaveAndNew();

    if (_mode == MODE_NEW)
    {
      rc = GetSQL1Row().Insert();
    }
    else
    {
      rc = GetSQL1Row().Update();
    }

    EndingSaveAndNew(rc);

    if (rc == true)
    {
      NotifyListPanelListeners(false);
      NotifyListeners(SAVE_EVENT, false);
      NotifyListenersPanelStateChange(PanelNotifier.ACTION_SAVE_AND_NEW);

      _mode = MODE_NEW;
      _sqlRow = GetSQL1Table().CreateRow();
      AdjustSQLRow();
      AssignRowToFields();
    }

    return rc;

  }

  protected class SaveAndNewAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 2428110154300481825L;

    public SaveAndNewAction()
    {
      putValue(Action.NAME, GetResources().getProperty(SAVE_AND_NEW).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(SAVE_AND_NEW_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      SaveAndNewAction();
    }
  }

}
