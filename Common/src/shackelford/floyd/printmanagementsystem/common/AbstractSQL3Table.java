package shackelford.floyd.printmanagementsystem.common;




/**
 * <p>Title: AbstractSQL3Table</p>
 * <p>Description:
 * this class keeps track of information about the sql table.
 *
 * this class also provides set manipulation methods on sql rows. if you want information from a
 * single row, look for a method in the AbstractSQL2Row class. If you want methods that operate
 * upon 2 or more rows, look here.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL3Table
  extends AbstractSQL2Table
{

  public AbstractSQL3Table (
    AbstractSQLDatabase               sqlDatabase,
    String                            tableName,
    Class<? extends AbstractSQL1Row>  rowClass )
  {
    super(sqlDatabase,tableName,rowClass);
  }

  protected AbstractSQL3Row GetAbstractSQL3Row()
  {
    return (AbstractSQL3Row)_row;
  }

  @Override
  protected boolean InsertWithThisField(String fieldName)
  {
    if ( (super.InsertWithThisField(fieldName) == false) ||
         (fieldName.equals(AbstractSQL3Row.Name_updated_dts()) == true) )
    {
      return false;
    }
    return true;
  }

  @Override
  protected String GetUpdateValue(String fieldName, String value)
  {
    if (fieldName.equals(AbstractSQL3Row.Name_updated_dts()) == true)
    {
      return "now";
    }
    return super.GetUpdateValue(fieldName,value);
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(AbstractSQL3Row.Name_updated_dts()) == true)
    {
      return DTS_PAD;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }
}
