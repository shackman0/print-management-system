package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: ListPanelListener</p>
 * <p>Description:
  use this interface to be notified when a panel is done

  here's a typical implementation:

public class AListPanel
  implements ListPanelListener
{
  public AListPanel ( )
  {
    GUI guiManager = new GUIManager();  // implements ListPanelNotifier
    guiManager.AddListPnelListener(this);
  }

  public synchronized void RefreshList ()
  {
    // the database has changed, so you need to refresh the list
  }

}
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface ListPanelListener
{

  /**
    the GUI class should call RefreshList when it changes the database

  */
  public void RefreshList ();

}
