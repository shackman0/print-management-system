package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.MessageFormat;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;

/**
 * <p>Title: SplashPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SplashPanel
  extends JFrame
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID = 7958771304954841672L;

  private Container           _northPanel      = null;

  private Container           _southPanel      = null;

  private Container           _eastPanel       = null;

  private Container           _westPanel       = null;

  private Container           _centerPanel     = null;

  private String              _title           = null;

  private String              _message         = null;

  private String              _startupMessage  = null;

  private static final String TITLE            = "TITLE";

  private static final String MESSAGE          = "MESSAGE";

  private static final String SPLASH_IMAGE     = "splash.gif";

  private static Resources    __resources      = null;

  public SplashPanel()
  {
    super();
    Initialize(null, null, null);
  }

  public SplashPanel(String startupMessage)
  {
    super();
    Initialize(null, null, startupMessage);
  }

  public SplashPanel(String title, String message)
  {
    super();
    Initialize(title, message, null);
  }

  public void Initialize(String title, String message, String startupMessage)
  {
    _title = title;
    _message = message;
    _startupMessage = startupMessage;

    addWindowListener(new WindowListener());
    setIconImage(SystemUtilities.GetLogoIconImage());
    if (_title == null)
    {
      _title = GetResources().getProperty(TITLE).toString();
    }
    setTitle(_title);

    setResizable(false);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    pack();
    validate();
    PositionInitialPanel();
  }

  Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    northPanel.setBackground(Color.white);
    ImageIcon imageIcon = new ImageIcon(SystemUtilities.GetImage(SPLASH_IMAGE));
    northPanel.add(new JLabel(imageIcon, SwingConstants.CENTER));
    return northPanel;
  }

  Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    centerPanel.setBackground(Color.white);
    JTextArea textArea = new JTextArea();
    textArea.setLineWrap(false);
    textArea.setEditable(false);
    textArea.setForeground(Color.black);
    textArea.setBackground(Color.white);
    centerPanel.add(textArea);
    if (_message == null)
    {
      /*
       * {0} build date
       * {1,number} database version number
       * {2,number} major version
       * {3,number} minor version
       * {4,number} fix level
       */
      MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(MESSAGE).toString());
      Object[] substitutionValues = new Object[] { GlobalAttributes.GetMajorVersion(), GlobalAttributes.GetMinorVersion(), GlobalAttributes.GetFixLevel(), GlobalAttributes.AUTOMATIC_UPGRADE_NUMBER,
        GlobalAttributes.BUILD_DATE };
      _message = msgFormat.format(substitutionValues).trim();
    }
    String message = _message;
    if (_startupMessage != null)
    {
      message += _startupMessage;
    }
    textArea.setText(message);
    textArea.setRows(textArea.getLineCount());
    return centerPanel;
  }

  Container CreateWestPanel()
  {
    JPanel westPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    westPanel.setBackground(Color.white);
    return westPanel;
  }

  Container CreateEastPanel()
  {
    JPanel eastPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    eastPanel.setBackground(Color.white);
    return eastPanel;
  }

  Container CreateSouthPanel()
  {
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    southPanel.setBackground(Color.white);
    return southPanel;
  }

  private void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    int x = (screenSize.width - getWidth()) / 2;
    int y = (screenSize.height - getHeight()) / 2;
    setLocation(x, y);
  }

  private class WindowListener
    extends WindowAdapter
  {

    public WindowListener()
    {
      super();
    }

    @Override
    public void windowClosing(WindowEvent event)
    {
      setVisible(false);
    }
  }

  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateApplicationResources(SplashPanel.class.getName());
  }

}
