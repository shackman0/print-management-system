package shackelford.floyd.printmanagementsystem.common;

import java.util.ArrayList;



/**
 * <p>Title: Notifier</p>
 * <p>Description:
  use this interface to notify listeners.

  here's a typical implementation:


public class AClass
  implements Notifier
{

  private ArrayList                 _listeners;

  public void AddListener ( Listener listener )
  {
    if (_listeners == null)
    {
      _listeners = new ArrayList();
    }
    if (listener != null)
    {
      if (_listeners.indexOf(listener) == -1)
      {
        _listeners.add(listener);
      }
    }
  }

  public void AddListeners(ArrayList listeners)
  {
    if (_listPanelListeners == null)
    {
      _listeners = new ArrayList();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddListener((Listener)(listeners.get(i)));
      }
    }
  }

  public void RemoveListener ( Listener listener )
  {
    if (listener != null)
    {
      int listenerIndex = _listeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _listeners.remove(listenerIndex);
      }
    }
  }

  public void NotifyListeners(char event, boolean resetListenerList)
  {
   if (_listeners != null)
   {
     for (Iterator iterator = _listeners.iterator(); iterator.hasNext();)
      {
        Listener listener = (Listener)iterator.next();
        listener.Notification(event);
      }
      if (resetListenerList == true)
      {
        _listeners = null;
      }
    }
  }

}
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface Notifier
{

  public static final char        EVENT_SAVE         = 's';
  public static final char        EVENT_SAVE_AND_NEW = 'n';
  public static final char        EVENT_CANCEL       = 'c';
  public static final char        EVENT_OK           = 'k';
  public static final char        EVENT_EXIT         = 'x';
  public static final char        EVENT_REFRESH      = 'r';

  /**
    this method lets a listener be added to the list of listeners to be notified

    @param listener the object that is to be registered as a listener
  */
  public void AddListener ( Listener listener );

  /**
    this method to lets a listener remove itself as a listener.

    @param listener the object to be removed as a listener
  */
  public void RemoveListener ( Listener listener );

  /**
    this method is used to let all listeners know when something interesting happens.

    @param event the event that occurred. this is application specific.
  */
  public void NotifyListeners(char event, boolean resetListenerList);

  public void AddListeners(ArrayList<Listener> listeners);

}