package shackelford.floyd.printmanagementsystem.common;

import java.awt.Window;
import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;


/**
 * <p>Title: HelpButton</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class HelpButton
  extends ActionButton
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID = 233296705347857416L;

  final String                _helpFor;

  private static Resources    __resources;

  private static final String HELP             = "HELP";

  private static final String HELP_TIP         = "HELP_TIP";

  private static final String HTML_LEADER      = "file://";

  private static final String HTML_TRAILER     = ".html";

  private static final String HELP_DIRECTORY   = "Help";

  public HelpButton(Window helpFor)
  {
    super();

    _helpFor = helpFor.getClass().getName();

    addActionListener(new HelpAction());
    setVisible(false);
  }

  private class HelpAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 1467191727838670412L;

    public HelpAction()
    {
      putValue(Action.NAME, GetResources().getProperty(HELP));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(HELP_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      String url = HTML_LEADER + GlobalAttributes.__resourcesDirectory + File.separator + Resources.GetLanguageDirectory() + File.separator + HELP_DIRECTORY + File.separator + _helpFor + HTML_TRAILER;
      BrowserControl.displayURL(url);
    }
  }

  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateResources("Utilities", HelpButton.class.getName());
  }

}
