package shackelford.floyd.printmanagementsystem.common;

import java.util.ArrayList;


/**
 * <p>Title: PanelNotifier</p>
 * <p>Description:
 * <p>use this interface to be a notifier of PanelListener objects
 * <p>here's a typical implementation:
 * <code>
public class PanelManager
  implements PanelNotifier
{

  private ArrayList               _panelListeners;

  public void AddPanelListener ( PanelListener listener )
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList();
    }
    if (listener != null)
    {
      if (_panelListeners.indexOf(listener) == -1)
      {
        _panelListeners.add(listener);
      }
    }
  }

  public void AddPanelListeners(ArrayList listeners)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddPanelListener((PanelListener)(listeners.get(i)));
      }
    }
  }

  public void RemovePanelListener ( PanelListener listener )
  {
    if (listener != null)
    {
      int listenerIndex = _panelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = (PanelListener)iterator.next();
        listener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = (PanelListener)iterator.next();
        listener.PanelStateChange(action);
      }
    }
  }

}
 *</code>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface PanelNotifier
{

  /*
   * the list of actions for which the panel will notify listeners.
   */
  public static final char        ACTION_SAVE         = 's';
  public static final char        ACTION_SAVE_AND_NEW = 'n';
  public static final char        ACTION_CANCEL       = 'c';
  public static final char        ACTION_OK           = 'k';
  public static final char        ACTION_EXIT         = 'x';
  public static final char        ACTION_REFRESH      = 'r';

  /**
   * this method lets a panel be added to the list of panels to be notified when this panel exits.
   * @param listener the object that is to be registered as a listener
   */
  public void AddPanelListener ( PanelListener listener );

  /**
   * Convenience method
   * @param listeners
   */
  public void AddPanelListeners(ArrayList<PanelListener> listeners);

  /**
   * this method to lets a panel remove itself as a listener.
   * @param listener the object to be removed as a listener
  */
  public void RemovePanelListener ( PanelListener listener );

  /**
   * this method is used to let all listeners know when this panel exits.
   * @param action the action on the calling panel which caused it to close:
   *   ACTION_SAVE
   *   ACTION_CANCEL
   *   ACTION_OK
   *   ACTION_EXIT
   */
  public void NotifyListenersEndOfPanel(char action);

  /**
   * this method is used to let a listeners know when the panel's state changes.
   * 
   * @param action the action on the calling panel which caused it to change:
   *   ACTION_SAVE_AND_NEW
   *   ACTION_REFRESH
   */
  public void NotifyListenersPanelStateChange(char action);

}
