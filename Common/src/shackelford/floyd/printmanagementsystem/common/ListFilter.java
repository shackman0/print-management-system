package shackelford.floyd.printmanagementsystem.common;



/**
 * <p>Title: ListFilter</p>
 * <p>Description:
 * filter visible rows in a list panel
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ListFilter
{

  private AbstractList1Panel  _listPanel = null;
  private int                 _listLimit = 100;
  private String[]            _filterViewNames = new String[NUMBER_OF_FILTER_FIELDS];
  private String[]            _filterOperators = new String[NUMBER_OF_FILTER_FIELDS];
  private String[]            _filterValues = new String[NUMBER_OF_FILTER_FIELDS];
  private String[]            _sortViewNames = new String[NUMBER_OF_SORT_FIELDS];
  private String[]            _sortValues = new String[NUMBER_OF_SORT_FIELDS];
  private boolean             _showForeignKeys = false;
  private boolean             _showDisabledItems = false;
  private boolean             _showColumnTotals = false;

  public static final char WILD_CARD_MULTI_CHAR   = '*';
  public static final char WILD_CARD_SINGLE_CHAR  = '?';

  public static final String WILD_CARD_MULTI_STRING   = new String(new char[] { WILD_CARD_MULTI_CHAR } );
  public static final String WILD_CARD_SINGLE_STRING  = new String(new char[] { WILD_CARD_SINGLE_CHAR } );

  public static final String SQL_SORT_ASCENDING = "asc";
  public static final String SQL_SORT_DESCENDING = "desc";

  public static final int NUMBER_OF_FILTER_FIELDS = 5;
  public static final int NUMBER_OF_SORT_FIELDS = 3;

  public static final String[] SQL_FILTER_OPERATORS = new String[] {
                                                        "=",
                                                        "<>",
                                                        "<",
                                                        ">",
                                                        ">=",
                                                        "<=",
                                                        "like",
                                                        "not like" };

  public ListFilter(AbstractList1Panel  listPanel)
  {
    _listPanel = listPanel;

    for (int i = 0; i < NUMBER_OF_FILTER_FIELDS; i++)
    {
      _filterViewNames[i] = "";
      _filterOperators[i] = SQL_FILTER_OPERATORS[0];
      _filterValues[i] = "";
    }

    for (int i = 0; i < NUMBER_OF_SORT_FIELDS; i++)
    {
      _sortViewNames[i] = "";
      _sortValues[i] = SQL_SORT_ASCENDING;
    }
  }

  public int GetListLimit()
  {
    return _listLimit;
  }

  public void SetListLimit ( int listLimit )
  {
    _listLimit = listLimit;
  }

  public AbstractList1Panel GetListPanel()
  {
    return _listPanel;
  }

  public String[] GetFilterViewNames()
  {
    return DeepCopyStringArray(_filterViewNames);
  }

  public String GetFilterViewName(int index)
  {
    return new String(_filterViewNames[index]);
  }

  public void SetFilterViewNames(String[] filterViewNames)
  {
    _filterViewNames = DeepCopyStringArray(filterViewNames);
  }

  public void SetFilterViewName(int index, String filterViewName)
  {
    _filterViewNames[index] = new String(filterViewName);
  }

  public String[] GetFilterOperators()
  {
    return DeepCopyStringArray(_filterOperators);
  }

  public String GetFilterOperator(int index)
  {
    return new String(_filterOperators[index]);
  }

  public void SetFilterOperators(String[] filterOperators)
  {
    _filterOperators = DeepCopyStringArray(filterOperators);
  }

  public void SetFilterOperator(int index, String filterOperator)
  {
    _filterOperators[index] = new String(filterOperator);
  }

  public String[] GetFilterValues()
  {
    return DeepCopyStringArray(_filterValues);
  }

  public String GetFilterValue(int index)
  {
    return new String(_filterValues[index]);
  }

  public void SetFilterValues(String[] filterValues)
  {
    _filterValues = DeepCopyStringArray(filterValues);
  }

  public void SetFilterValue(int index, String filterValue)
  {
    _filterValues[index] = new String(filterValue);
  }

  public String[] GetSortViewNames()
  {
    return DeepCopyStringArray(_sortViewNames);
  }

  public String GetSortViewName(int index)
  {
    return new String(_sortViewNames[index]);
  }

  public void SetSortViewNames(String[] sortViewNames)
  {
    _sortViewNames = DeepCopyStringArray(sortViewNames);
  }

  public void SetSortViewName(int index, String sortViewName)
  {
    _sortViewNames[index] = new String(sortViewName);
  }

  public String[] GetSortValues()
  {
    return DeepCopyStringArray(_sortValues);
  }

  public String GetSortValue(int index)
  {
    return new String(_sortValues[index]);
  }

  public void SetSortValues(String[] sortValues)
  {
    _sortValues = DeepCopyStringArray(sortValues);
  }

  public void SetSortValue(int index, String sortValue)
  {
    _sortValues[index] = new String(sortValue);
  }

  public boolean GetShowForeignKeys()
  {
    return _showForeignKeys;
  }

  public void SetShowForeignKeys(boolean showForeignKeys)
  {
    _showForeignKeys = showForeignKeys;
  }

  public boolean GetShowColumnTotals()
  {
    return _showColumnTotals;
  }

  public void SetShowColumnTotals(boolean showColumnTotals)
  {
    _showColumnTotals = showColumnTotals;
  }

  public boolean GetShowDisabledItems()
  {
    return _showDisabledItems;
  }

  public void SetShowDisabledItems(boolean showDisabledItems)
  {
    _showDisabledItems = showDisabledItems;
  }

  private static String[] DeepCopyStringArray(String[] stringArrayToCopy)
  {
    String[] newStringArray = new String[stringArrayToCopy.length];
    for (int i = 0; i < newStringArray.length; i++)
    {
      newStringArray[i] = new String(stringArrayToCopy[i]);
    }
    return newStringArray;
  }

}
