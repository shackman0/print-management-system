package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: _DateChooserAdapter</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class _DateChooserAdapter
  implements _DateChooserListener
{

  protected _DateChooserAdapter()
  {
    super();
  }

  @Override
  public void DateChooserCancelled(_DateChooserEvent event)
  {
    // do nothing
  }

  @Override
  public void DateChooserLostFocus(_DateChooserEvent event)
  {
    // do nothing
  }

  @Override
  public void DateChooserDateSelected(_DateChooserEvent event)
  {
    // do nothing
  }

  @Override
  public void DateChooserTabSelected(_DateChooserEvent event)
  {
    // do nothing
  }

  @Override
  public void DateChooserDismissed(_DateChooserEvent event)
  {
    // fireEditingStopped();
  }

}
