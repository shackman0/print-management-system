package shackelford.floyd.printmanagementsystem.common;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Point2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.RepaintManager;


/**
 * <p>Title: AbstractPrinter</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractPrinter
  extends JPanel
  implements Printable
{

  /**
   * Eclipse generated value
   */
  private static final long    serialVersionUID = -5564104778153758733L;

  protected static final int   RIGHT_JUSTIFIED  = 1;

  protected static final int   LEFT_JUSTIFIED   = 2;

  protected static final int   CENTER_JUSTIFIED = 3;

  protected static final float ONE_INCH         = 72f;                  // 72 points = 1 inch

  protected static final float ONE_HALF_INCH    = ONE_INCH / 2f;

  protected static final float ONE_QUARTER_INCH = ONE_INCH / 4f;

  protected static final int   FONT_BASE_SIZE   = 10;

  protected static final int   TITLE_NDX        = 0;

  protected static final int   HEADING_NDX      = 1;

  protected static final int   LABEL_NDX        = 2;

  protected static final int   VALUE_NDX        = 3;

  protected static final int   HEADER_NDX       = 4;

  protected static final int   FOOTER_NDX       = 5;

  protected static final int   NUMBER_OF_FONTS  = 6;

  public AbstractPrinter()
  {
    super();
    _frame = null;
    SetFonts();
  }

  public AbstractPrinter(JFrame frame)
  {
    super();
    _frame = frame;
    SetFonts();
  }

  protected void SetFonts()
  {
    _fonts[TITLE_NDX] = new Font("SansSerif", Font.BOLD, (int) (FONT_BASE_SIZE * 1.4));
    _fonts[HEADING_NDX] = new Font("SansSerif", Font.PLAIN, (int) (FONT_BASE_SIZE * 1.4));
    _fonts[LABEL_NDX] = new Font("SansSerif", Font.BOLD, (int) (FONT_BASE_SIZE * 1.2));
    _fonts[VALUE_NDX] = new Font("SansSerif", Font.PLAIN, (int) (FONT_BASE_SIZE * 1.2));
    _fonts[HEADER_NDX] = new Font("SansSerif", Font.PLAIN, FONT_BASE_SIZE);
    _fonts[FOOTER_NDX] = new Font("SansSerif", Font.PLAIN, FONT_BASE_SIZE);
  }

  public PageFormat PrintSetup(PageFormat pageFormat)
  {
    if (pageFormat == null)
    {
      pageFormat = GetDefaultPageFormat();
    }
    return PrinterJob.getPrinterJob().pageDialog(pageFormat);
  }

  public void PrintPreview(PageFormat pageFormat)
  {
    if (pageFormat == null)
    {
      pageFormat = GetDefaultPageFormat();
    }
    PrintPreviewDialog previewDialog = new PrintPreviewDialog(this, pageFormat, 1);
    previewDialog.setVisible(true);
  }

  public void Print(PageFormat pageFormat)
  {
    if (pageFormat == null)
    {
      pageFormat = GetDefaultPageFormat();
    }
    PrinterJob printerJob = PrinterJob.getPrinterJob();
    printerJob.setPrintable(this, pageFormat);
    if (printerJob.printDialog() == true)
    {
      try
      {
        Cursor saveCursor = null;
        if (_frame != null)
        {
          RepaintManager.currentManager(_frame).setDoubleBufferingEnabled(false);
          saveCursor = _frame.getCursor();
          _frame.setCursor(new Cursor(Cursor.WAIT_CURSOR));
        }
        printerJob.print();
        if (_frame != null)
        {
          _frame.setCursor(saveCursor);
          RepaintManager.currentManager(_frame).setDoubleBufferingEnabled(true);
        }
      }
      catch (PrinterException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        MessageDialog.ShowErrorMessageDialog(null, excp, "printJob.print()");
        return;
      }
    }
  }

  @Override
  public int print(Graphics g, PageFormat pageFormat, int pageNumber)
  {

    Graphics2D g2D = (Graphics2D) g;

    SetRenderingValues(g2D);

    g2D.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

    _xColumn1 = 0f;
    _xColumn2 = (float) pageFormat.getImageableWidth() / 2f;
    _xTab = 100f;
    _xColumn1Tab = _xColumn1 + _xTab;
    _xColumn2Tab = _xColumn2 + _xTab;

    return Printable.NO_SUCH_PAGE;
  }

  @Override
  public void paintComponent(Graphics g)
  {
    super.paintComponent(g);

    Graphics2D g2D = (Graphics2D) g;

    SetRenderingValues(g2D);

    Dimension dim = getPreferredSize();

    _xColumn1 = 0f;
    _xColumn2 = (float) dim.getWidth() / 2f;
    _xTab = 100f;
    _xColumn1Tab = _xColumn1 + _xTab;
    _xColumn2Tab = _xColumn2 + _xTab;
  }

  protected void SetRenderingValues(Graphics2D g2D)
  {
    _fontMetrics[TITLE_NDX] = g2D.getFontMetrics(_fonts[TITLE_NDX]);
    _fontMetrics[HEADING_NDX] = g2D.getFontMetrics(_fonts[HEADING_NDX]);
    _fontMetrics[LABEL_NDX] = g2D.getFontMetrics(_fonts[LABEL_NDX]);
    _fontMetrics[VALUE_NDX] = g2D.getFontMetrics(_fonts[VALUE_NDX]);
    _fontMetrics[HEADER_NDX] = g2D.getFontMetrics(_fonts[HEADER_NDX]);
    _fontMetrics[FOOTER_NDX] = g2D.getFontMetrics(_fonts[FOOTER_NDX]);

    _offsets[TITLE_NDX] = _fontMetrics[TITLE_NDX].getHeight();
    _offsets[HEADING_NDX] = _fontMetrics[HEADING_NDX].getHeight();
    _offsets[LABEL_NDX] = _fontMetrics[LABEL_NDX].getHeight();
    _offsets[VALUE_NDX] = _fontMetrics[VALUE_NDX].getHeight();
    _offsets[HEADER_NDX] = _fontMetrics[HEADER_NDX].getHeight();
    _offsets[FOOTER_NDX] = _fontMetrics[FOOTER_NDX].getHeight();
  }

  protected double RightJustify(FontMetrics fontMetric, String string2Print, PageFormat pageFormat)
  {
    return RightJustify(fontMetric, string2Print, pageFormat.getImageableWidth());
  }

  protected double RightJustify(int fontIndex, String string2Print, double x)
  {
    return RightJustify(_fontMetrics[fontIndex], string2Print, x);
  }

  protected double RightJustify(FontMetrics fontMetric, String string2Print, double x)
  {
    return x - fontMetric.stringWidth(string2Print);
  }

  protected double CenterJustify(FontMetrics fontMetric, String string2Print, PageFormat pageFormat)
  {
    return CenterJustify(fontMetric, string2Print, pageFormat.getImageableWidth());
  }

  protected double CenterJustify(int fontIndex, String string2Print, double x)
  {
    return CenterJustify(_fontMetrics[fontIndex], string2Print, x);
  }

  protected double CenterJustify(FontMetrics fontMetric, String string2Print, double x)
  {
    return x - (fontMetric.stringWidth(string2Print) / 2d);
  }

  protected float PrintString(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    string2Print.trim();
    g2D.setFont(_fonts[fontIndex]);
    g2D.drawString(string2Print, x, y);
    return _fontMetrics[fontIndex].stringWidth(string2Print);
  }

  protected float PrintStringRightJustified(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    return PrintString(g2D, (float) RightJustify(fontIndex, string2Print, x), y, fontIndex, string2Print);
  }

  protected float PrintStringCenterJustified(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    return PrintString(g2D, (float) CenterJustify(fontIndex, string2Print, x), y, fontIndex, string2Print);
  }

  protected Point2D.Float PrintMultiLineStringRightJustified(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    return PrintMultiLineStringWithJustification(g2D, x, y, fontIndex, string2Print, RIGHT_JUSTIFIED);
  }

  protected Point2D.Float PrintMultiLineStringCenterJustified(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    return PrintMultiLineStringWithJustification(g2D, x, y, fontIndex, string2Print, CENTER_JUSTIFIED);
  }

  protected Point2D.Float PrintMultiLineString(Graphics2D g2D, float x, float y, int fontIndex, String string2Print)
  {
    return PrintMultiLineStringWithJustification(g2D, x, y, fontIndex, string2Print, LEFT_JUSTIFIED);
  }

  protected Point2D.Float PrintMultiLineStringWithJustification(Graphics2D g2D, float x, float y, int fontIndex, String string2Print, int justification)
  {
    Point2D.Float point = new Point2D.Float();
    float maxX = 0f;
    string2Print = string2Print.trim();
    if (string2Print.length() > 0)
    {
      y -= _offsets[fontIndex]; // back up since we add yOffsetIncrement before printing a line;
      string2Print += "\n"; // since we key on "\n", append one in case the last line doesn't have one
      int head = 0;
      int tail = 0;
      while ((tail = string2Print.indexOf('\n', head)) != -1)
      {
        String substring = string2Print.substring(head, tail).trim();
        if (substring.length() > 0) // eliminate blank lines
        {
          y += _offsets[fontIndex];
          float thisX = PrintString(g2D, x, y, fontIndex, substring);
          if (thisX > maxX)
          {
            maxX = thisX;
          }
        }
        head = tail + 1;
      }
      if (head > 0)
      {
        float thisX = PrintString(g2D, x, y, fontIndex, string2Print.substring(head));
        if (thisX > maxX)
        {
          maxX = thisX;
        }
      }
    }
    point.x = maxX;
    point.y = y;

    return point;
  }

  protected JFrame        _frame;

  protected Font[]        _fonts       = new Font[NUMBER_OF_FONTS];

  protected FontMetrics[] _fontMetrics = new FontMetrics[NUMBER_OF_FONTS];

  protected float[]       _offsets     = new float[NUMBER_OF_FONTS];

  protected float         _xColumn1;

  protected float         _xColumn2;

  protected float         _xTab;

  protected float         _xColumn1Tab;

  protected float         _xColumn2Tab;

  public static PageFormat GetDefaultPageFormat()
  {
    PageFormat pageFormat = PrinterJob.getPrinterJob().defaultPage();

    Paper paper = pageFormat.getPaper();
    double paperX = paper.getImageableX();
    double paperY = paper.getImageableY();
    double paperWidth = paper.getImageableWidth();
    double paperHeight = paper.getImageableHeight();
    double newPaperX = ONE_HALF_INCH;
    double newPaperY = ONE_HALF_INCH;
    double newPaperWidth = paperWidth + ((paperX - newPaperX) * 2d);
    double newPaperHeight = paperHeight + ((paperY - newPaperY) * 2d);
    paper.setImageableArea(newPaperX, newPaperY, newPaperWidth, newPaperHeight);
    pageFormat.setPaper(paper);

    return pageFormat;
  }

}
