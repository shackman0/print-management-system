package shackelford.floyd.printmanagementsystem.common;

import java.awt.Graphics;
import java.awt.Rectangle;

import javax.swing.plaf.TextUI;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.JTextComponent;

/**
 * <p>Title: OverwriteCaret</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class OverwriteCaret
  extends DefaultCaret
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -6175567508035865624L;

  @Override
  protected synchronized void damage(Rectangle r)
  {
    if (r != null)
    {
      try
      {
        JTextComponent comp = getComponent();
        TextUI mapper = comp.getUI();
        Rectangle r2 = mapper.modelToView(comp, getDot() + 1);
        int newWidth = r2.x - r.x;
        if (newWidth == 0)
        {
          newWidth = MIN_WIDTH;
        }
        comp.repaint(r.x, r.y, newWidth, r.height);

        // Swing 1.1 beta 2 compat
        this.x = r.x;
        this.y = r.y;
        this.width = newWidth;
        this.height = r.height;
      }
      catch (BadLocationException e)
      {
        // do nothing
      }
    }
  }

  @Override
  public void paint(Graphics g)
  {
    if (isVisible())
    {
      try
      {
        JTextComponent comp = getComponent();
        TextUI mapper = comp.getUI();
        Rectangle r1 = mapper.modelToView(comp, getDot());
        Rectangle r2 = mapper.modelToView(comp, getDot() + 1);
        g = g.create();
        g.setColor(comp.getForeground());
        g.setXORMode(comp.getBackground());
        int newWidth = r2.x - r1.x;
        if (newWidth == 0)
        {
          newWidth = MIN_WIDTH;
        }
        g.fillRect(r1.x, r1.y, newWidth, r1.height);
        g.dispose();
      }
      catch (BadLocationException e)
      {
        // do nothing
      }
    }
  }

  protected static final int MIN_WIDTH = 8;
}
