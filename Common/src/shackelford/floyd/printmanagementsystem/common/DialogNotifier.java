package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: DialogNotifier</p>
 * <p>Description:
 * use this interface to be a notifier of DialogListener objects
 * <p>here's a typical implementation:
 * <pre>
public class MyDialog
  implements DialogNotifier
{

  private ArrayList _dialogListeners;

  public void AddDialogListener ( DialogListener listener )
  {
    if (_dialogListeners == null)
    {
      _dialogListeners = new ArrayList();
    }
    if (listener != null)
    {
      if (_dialogListeners.indexOf(listener) == -1)
      {
        _dialogListeners.add(listener);
      }
    }
  }

  public void RemoveDialogListener ( DialogListener listener )
  {
    if (listener != null)
    {
      int listenerIndex = _dialogListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _dialogListeners.remove(listenerIndex);
      }
    }
  }

  public void NotifyDialogListeners()
  {
    if (_dialogListeners != null)
    {
      Properties props = GetProperties();
      for (Iterator iterator = _dialogListeners.iterator(); iterator.hasNext();)
      {
        DialogListener listener = (DialogListener)iterator.next();
        listener.EndOfDialog(props);
      }
      _dialogListeners = null;
    }
  }

}
 * </pre>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface DialogNotifier
{

  /**
   * let a Dialog be added to the list of Dialogs to be notified when this Dialog exits.
   * @param aListener the object that is to be registered as a listener
   */
  public void AddDialogListener ( DialogListener aListener );

  /**
   * let a Dialog remove itself as a listener.
   * @param aListener the object to be removed as a listener
   */
  public void RemoveDialogListener ( DialogListener aListener );


  /**
   * let all listeners know when this Dialog exits.
   */
  public void NotifyDialogListeners();

}
