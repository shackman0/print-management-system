package shackelford.floyd.printmanagementsystem.common;

import java.awt.Frame;

/**
 * <p>Title: AbstractLoginDialog</p>
 * <p>Description:
 * An abstraction of the login dialog.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractLoginDialog
  extends AbstractDialog
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -1042828509910580713L;

  /**
   * Constructor
   * @param owner
   * @param modal
   */
  public AbstractLoginDialog(Frame owner, boolean modal)
  {
    super(owner, modal);
  }

}
