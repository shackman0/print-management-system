package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;


/**
 * <p>Title: AbstractList4Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractList4Panel
  extends AbstractList3Panel
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID           = 2787161042920885811L;

  protected JMenuItem           _editEnableMenuItem;

  protected JMenuItem           _editDisableMenuItem;

  protected JMenuItem           _editEnableFloatingMenuItem;

  protected JMenuItem           _editDisableFloatingMenuItem;

  protected static final String EDIT                       = "EDIT";

  protected static final String EDIT_DISABLE               = "EDIT_DISABLE";

  protected static final String EDIT_DISABLE_TIP           = "EDIT_DISABLE_TIP";

  protected static final String EDIT_ENABLE                = "EDIT_ENABLE";

  protected static final String EDIT_ENABLE_TIP            = "EDIT_ENABLE_TIP";

  protected static final String DISABLE_CONFIRMATION_MSG   = "DISABLE_CONFIRMATION_MSG";

  protected static final String DISABLE_CONFIRMATION_TITLE = "DISABLE_CONFIRMATION_TITLE";

  public AbstractList4Panel()
  {
    super();
  }

  @Override
  protected void AddMenus(JMenuBar menuBar)
  {
    super.AddMenus(menuBar);
    menuBar.add(CreateEditMenu());
  }

  protected JMenu CreateEditMenu()
  {
    JMenu editMenu = new JMenu(GetResources().getProperty(EDIT));
    AddEditMenuItems(editMenu);

    return editMenu;
  }

  @Override
  protected void AddEditMenuItems(JMenu editMenu)
  {
    _editDisableMenuItem = editMenu.add(new ActionMenuItem(new EditDisableAction()));
    _editDisableMenuItem.setEnabled(false);
    _editEnableMenuItem = editMenu.add(new ActionMenuItem(new EditEnableAction()));
    _editEnableMenuItem.setEnabled(false);
  }

  @Override
  protected void AddFloatingEditMenuItems(JPopupMenu popupMenu)
  {
    super.AddFloatingEditMenuItems(popupMenu);
    _editDisableFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditDisableAction()));
    _editDisableFloatingMenuItem.setEnabled(false);
    _editEnableFloatingMenuItem = popupMenu.add(new ActionMenuItem(new EditEnableAction()));
    _editEnableFloatingMenuItem.setEnabled(false);
    popupMenu.addSeparator();
  }

  @Override
  protected void EnableMenuItems(boolean enabledFlag)
  {
    super.EnableMenuItems(enabledFlag);
    _editEnableMenuItem.setEnabled(enabledFlag);
    _editDisableMenuItem.setEnabled(enabledFlag);
    _editEnableFloatingMenuItem.setEnabled(enabledFlag);
    _editDisableFloatingMenuItem.setEnabled(enabledFlag);
  }

  public AbstractSQL4Table GetAbstractSQL4Table()
  {
    return (AbstractSQL4Table) _sqlTable;
  }

  /*
    public AbstractSQL4Row GetAbstractSQL4Row()
    {
      return (AbstractSQL4Row)_sqlRow;
    }
  */
  /**
   * This method is called by the EditEnableAction.actionPerformed() method in case
   * the child class wants to propagate the enable.
   */
  protected void EnablingRow(AbstractSQL4Row sqlRow)
  {
    // do nothing
  }

  /**
   * This method is called by the EditDisableAction.actionPerformed() method in case
   * the child class wants to propagate the disable.
   */
  protected void DisablingRow(AbstractSQL4Row sqlRow)
  {
    // do nothing
  }

  protected void EditDisableAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      int rc = JOptionPane.showConfirmDialog(this, GetResources().getProperty(DISABLE_CONFIRMATION_MSG), GetResources().getProperty(DISABLE_CONFIRMATION_TITLE), JOptionPane.YES_NO_OPTION);
      if (rc == JOptionPane.YES_OPTION)
      {
        for (int i = 0; i < sqlRows.length; ++i)
        {
          AbstractSQL4Row sqlRow = (AbstractSQL4Row) (sqlRows[i]);
          if (sqlRow.OK2Disable() == true)
          {
            sqlRow.Set_status(AbstractSQL4Row.STATUS_DISABLED_NAME);
            sqlRow.Update();
            DisablingRow(sqlRow);
          }
        }
        RefreshList();
      }
    }
  }

  protected class EditDisableAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -7461136878961532367L;

    public EditDisableAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_DISABLE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_DISABLE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditDisableAction();
    }

  }

  protected void EditEnableAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    for (int i = 0; i < sqlRows.length; ++i)
    {
      AbstractSQL4Row sqlRow = (AbstractSQL4Row) (sqlRows[i]);
      sqlRow.Set_status(AbstractSQL4Row.STATUS_ENABLED_NAME);
      sqlRow.Update();
      EnablingRow(sqlRow);
    }
    RefreshList();
  }

  protected class EditEnableAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -1764754301812385154L;

    public EditEnableAction()
    {
      putValue(Action.NAME, GetResources().getProperty(EDIT_ENABLE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(EDIT_ENABLE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      EditEnableAction();
    }
  }

}
