package shackelford.floyd.printmanagementsystem.common;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * <p>Title: Preferences</p>
 * <p>Description:
   This utility class is for reading properties out of a file
   consisting of name/value pairs separated by an '=' and ending with a new-line.
   The file format supports comments on lines beginning with '#'.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Preferences
  extends Properties
{
  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -6113623111364176388L;

  /**
    This is the name of the preferences file with which
    this Preferences object was constructed.
  */
  String                    _prefsFileName;

  /**
   * this constructor creates an empty list
   */
  public Preferences()
  {
    super();
  }

  /**
    This constructor takes the name of the file where
    the preferences are stored.  If the file doesn't exist,
    then an IOException is thrown.
    @param fileName The name of the preferences file
  */
  public Preferences(String fileName)
    throws IOException
  {
    super();
    Load(fileName);
  }

  /**
   * this method loads the specified preferences file into the properties list
   */
  public void Load(String fileName)
    throws IOException
  {
    _prefsFileName = fileName;
    try (FileInputStream in = new FileInputStream(_prefsFileName);)
    {
      load(in);
    }
  }

  /**
    This method saves the preferences to the same file
    from which the preferences were originally read.
    @param header The header to write at the top of the
    preferences file (as a comment).
  */
  public void Save(String header)
    throws IOException
  {
    try (FileOutputStream out = new FileOutputStream(_prefsFileName);)
    {
      store(out, header);
    }
  }

}
