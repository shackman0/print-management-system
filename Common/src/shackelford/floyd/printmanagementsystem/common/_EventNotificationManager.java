package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Iterator;


/**
 * <p>Title: _EventNotificationManager</p>
 * <p>Description: handles sending event notifications to registered listeners</p>
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 * @param <ListenerType>
 */
public class _EventNotificationManager<ListenerType>
{

  /**
   * HashSet of EventListeners
   * This keeps track of the EventListeners that want to be notified whenever certain actions occur
   */
  protected _ArrayList<ListenerType> _eventListeners      = new _ArrayList<>();

  /**
   * if you want to turn off notifications, then set this to false
   */
  protected boolean                  _notificationEnabled = true;

  public _EventNotificationManager()
  {
    super();
  }

  public void AddListener(ListenerType listener)
  {
    _eventListeners.AddUnique(listener);
  }

  public void RemoveListener(ListenerType listener)
  {
    _ArrayList<ListenerType> tempEventListeners = new _ArrayList<>(_eventListeners);
    for (ListenerType eventListener : tempEventListeners)
    {
      if (eventListener == listener) // this is the listener we want to remove
      {
        _eventListeners.remove(eventListener);
        break;
      }
    }
  }

  public void RemoveAllListeners()
  {
    _eventListeners.clear();
  }

  public _ArrayList<ListenerType> GetListeners()
  {
    return _eventListeners;
  }

  public boolean IsNotificationEnabled()
  {
    return _notificationEnabled;
  }

  public void SetNotificationEnabled(boolean notificationEnabled)
  {
    _notificationEnabled = notificationEnabled;
  }

  /**
   * this method routes the notification to the specified method
   * @param methodName the name of the event starting method to which to route the message
   * @param event the event to pass to the listener's method
   */
  public void NotifyListenersOfEvent(String methodName, _EventNotificationManagerEvent event)
  {
    if (_notificationEnabled == true)
    {
      try
      {
        _ArrayList<ListenerType> tempEventListeners = new _ArrayList<>(_eventListeners);
        for (Iterator<ListenerType> iter = tempEventListeners.iterator(); ((event.GetConsumed() == false) && (iter.hasNext()));)
        {
          ListenerType eventListener = (iter.next());

          Class<? extends _EventNotificationManagerEvent> eventClass = event.getClass();
          Method method = null;
          try
          {
            method = eventListener.getClass().getMethod(methodName, eventClass);
          }
          catch (SecurityException ex)
          {
            throw new RuntimeException(ex);
          }
          catch (NoSuchMethodException ex)
          {
            throw new RuntimeException(ex);
          }

          try
          {
            method.setAccessible(true);
            method.invoke(eventListener, event);
          }
          catch (InvocationTargetException ex)
          {
            throw new RuntimeException(ex);
          }
          catch (IllegalAccessException ex)
          {
            throw new RuntimeException(ex);
          }
        }
      }
      catch (RuntimeException ex)
      {
        ex.printStackTrace();
        throw ex;
      }
    }
  }

}
