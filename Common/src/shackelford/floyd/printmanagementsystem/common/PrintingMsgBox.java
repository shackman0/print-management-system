package shackelford.floyd.printmanagementsystem.common;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;


/**
 * <p>Title: PrintingMsgBox</p>
 * <p>Description:
  this class displays the printing message box.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class PrintingMsgBox
  extends MsgBox
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID = -7581918326472318279L;

  protected PageFormat          _pageFormat      = null;

  protected static final String DISMISS          = "DISMISS";

  protected static final String DISMISS_TIP      = "DISMISS_TIP";

  protected static final String PRINT            = "PRINT";

  protected static final String PRINT_TIP        = "PRINT_TIP";

  protected static final String PAGE_SETUP       = "PAGE_SETUP";

  protected static final String PAGE_SETUP_TIP   = "PAGE_SETUP_TIP";

  public PrintingMsgBox()
  {
    super();
  }

  public PrintingMsgBox(String message, String title)
  {
    super();

    Initialize(message, title);
  }

  @Override
  protected Container CreateSouthPanel()
  {
    JPanel southPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));

    southPanel.add(new ActionButton(new PageSetupAction()));
    southPanel.add(new ActionButton(new PrintAction()));
    southPanel.add(new ActionButton(new DismissAction()));

    return southPanel;
  }

  protected abstract Resources GetResources();

  protected void DismissAction()
  {
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
    dispose();
  }

  protected class DismissAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 242104649491979877L;

    public DismissAction()
    {
      putValue(Action.NAME, GetResources().getProperty(DISMISS));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(DISMISS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      DismissAction();
    }
  }

  protected void PageSetupAction()
  {
    PrinterJob printJob = PrinterJob.getPrinterJob();
    if (_pageFormat == null)
    {
      _pageFormat = printJob.defaultPage();
    }
    _pageFormat = printJob.pageDialog(_pageFormat);
  }

  protected class PageSetupAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 927152795123098461L;

    public PageSetupAction()
    {
      putValue(Action.NAME, GetResources().getProperty(PAGE_SETUP));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(PAGE_SETUP_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PageSetupAction();
    }
  }

  protected void PrintAction(PrintAction printAction, ActionEvent event)
  {
    PrinterJob printJob = PrinterJob.getPrinterJob();
    if (_pageFormat == null)
    {
      _pageFormat = printJob.defaultPage();
    }
    printJob.setPrintable(printAction, _pageFormat);
    if (printJob.printDialog() == true)
    {
      try
      {
        printJob.print();
      }
      catch (PrinterException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        MessageDialog.ShowErrorMessageDialog(this, excp, "printJob.print()");
        return;
      }
    }
  }

  protected int PrintAction_Print(Graphics2D g2D, PageFormat pageFormat, int pageNumber)
  {
    return Printable.NO_SUCH_PAGE;
  }

  protected class PrintAction
    extends AbstractAction
    implements Printable
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -3108953008042742963L;

    public PrintAction()
    {
      putValue(Action.NAME, GetResources().getProperty(PRINT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(PRINT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintAction(this, event);
    }

    @Override
    public int print(Graphics g, PageFormat pageFormat, int pageNumber)
    {
      return PrintAction_Print((Graphics2D) g, pageFormat, pageNumber);
    }
  }

}
