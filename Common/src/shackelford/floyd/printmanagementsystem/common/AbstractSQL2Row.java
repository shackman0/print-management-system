package shackelford.floyd.printmanagementsystem.common;

import java.sql.ResultSet;


/**
 * <p>Title: AbstractSQL2Row</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL2Row
  extends AbstractSQL1Row
{

  protected String                _sql_created_dts;

  public AbstractSQL2Row ()
  {
    super();
  }

  public AbstractSQL2Row( AbstractSQL2Table sqlTable )
  {
    super();
    Initialize(sqlTable);
  }

  public AbstractSQL2Row( AbstractSQL2Table sqlTable, AbstractSQL1Row initRow )
  {
    Initialize(sqlTable, initRow);
  }

  public AbstractSQL2Row ( AbstractSQL2Table sqlTable, ResultSet resultSet )
  {
    Initialize(sqlTable,resultSet);
  }

  public String Get_created_dts() { return new String(_sql_created_dts); }
  public void Set_created_dts(String value) { _sql_created_dts = new String(value); }
  public static String Name_created_dts() { return "created_dts"; }

  public AbstractSQL2Table GetSQL2Table()
  {
    return (AbstractSQL2Table)_sqlTable;
  }

}
