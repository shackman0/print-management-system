package shackelford.floyd.printmanagementsystem.common;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


/**
 * <p>Title: AbstractEntry5Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractEntry5Panel
  extends AbstractEntry4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID   = -568429081180918701L;

  protected JLabel              _rowInfoRowIDLabel = new JLabel();

  protected static final String ROW_ID             = "ROW_ID";

  public AbstractEntry5Panel()
  {
    super();
  }

  @Override
  protected void AdjustSQLRow()
  {
    if (_mode == MODE_NEW)
    {
      GetSQL5Row().AssignNewRowID();
    }
  }

  @Override
  protected Box GetOptionalInfoRow()
  {
    Box lineBox = super.GetOptionalInfoRow();

    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_GAP_WIDTH));
    lineBox.add(new JLabel(GetResources().getProperty(ROW_ID).toString(), SwingConstants.RIGHT));
    _rowInfoRowIDLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    lineBox.add(_rowInfoRowIDLabel);

    return lineBox;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();
    _rowInfoRowIDLabel.setText(GetSQL5Row().Get_row_id().toString());
  }

  public AbstractSQL5Row GetSQL5Row()
  {
    return (AbstractSQL5Row) _sqlRow;
  }

  public AbstractSQL5Table GetSQL5Table()
  {
    return (AbstractSQL5Table) _sqlTable;
  }

}
