package shackelford.floyd.printmanagementsystem.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;


/**
 * <p>Title: Resources</p>
 * <p>Description:
  this class is used to load language specific resources.

  note that all resource files need to end with the RESOURCES extension.
  as in "LoginDialog.resources". Resource file names are case sensitive.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class Resources
  extends Properties
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID    = 6174829640540539768L;

  /*
    put the directory names for each particular language that appears
    under the "language" directory here.
  */
  public final static String  ENGLISH_UK          = "English_UK";        // UK English

  public final static String  ENGLISH_US          = "English_US";        // US English

  public final static String  SPANISH             = "Spanish";           // Spanish

  public final static String  FRENCH              = "French";            // French

  public final static String  PORTUGUESE          = "Portuguese";        // Portuguese

  public final static String  UNITED_STATES       = "United States";

  public final static String  ARGENTINA           = "Argentina";

  public final static String  BELIZE              = "Belize";

  public final static String  BOLIVIA             = "Bolivia";

  public final static String  BRAZIL              = "Brazil";

  public final static String  CANADA              = "Canada";

  public final static String  CHILE               = "Chile";

  public final static String  COLUMBIA            = "Columbia";

  public final static String  COSTA_RICA          = "Costa Rica";

  public final static String  ECUADOR             = "Ecuador";

  public final static String  EL_SALVADOR         = "El Salvador";

  public final static String  GUADELOUPE          = "Guadeloupe";

  public final static String  GUATEMALA           = "Guatemala";

  public final static String  HONDURAS            = "Honduras";

  public final static String  MEXICO              = "Mexico";

  public final static String  NICARAGUA           = "Nicaragua";

  public final static String  PANAMA              = "Panama";

  public final static String  PARAGUAY            = "Paraguay";

  public final static String  PERU                = "Peru";

  public final static String  URUGUAY             = "Uruguay";

  public final static String  VENEZUELA           = "Venezuela";

  /** this is the extension which every resource file needs to have.
  it is case sensitive. */
  private final static String RESOURCES_EXTENSION = ".resources";

  /** this is the language we use to get all the resources. it defaults to ENGLISH_US, but
  can be set anytime. */
  private static String       __languageDirectory = ENGLISH_US;

  /**
    This constructor takes the name of the language directory and the
    resource file and loads it in. it it can't find the specified
    file in the specified directory, an IOException is thrown

    @param languagesDirectory the top level directory under which all the language
      specific directories are located.
    @param subSystemName the name of the subsystem in which the resource is found.
    @param resourcesFileName The name of the file in which the language specific
      resources are located.
  */
  public Resources(String languagesDirectory, String subSystemName, String resourcesFileName)
    throws IOException
  {
    super();
    String fullPath = languagesDirectory + File.separator + __languageDirectory + File.separator + subSystemName + File.separator + resourcesFileName + RESOURCES_EXTENSION;
    try
    (FileInputStream in = new FileInputStream(fullPath);)
    {
      load(in);
    }
  }

  public static Resources CreateApplicationResources(String resourceFileName)
  {
    return CreateResources(GlobalAttributes.__applicationName, resourceFileName);
  }

  public static Resources CreateResources(String applicationName, String resourceFileName)
  {
    Resources resources = null;
    if (GlobalAttributes.__resourcesDirectory != null)
    {
      try
      {
        resources = new Resources(GlobalAttributes.__resourcesDirectory, applicationName, resourceFileName);
      }
      catch (IOException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
      }
    }
    return resources;
  }

  /**
    This method sets the languageDirectory attribute. Be sure to the use the
    constants provided by this class.

   @param language The directory in which the language specific resource files
     are located.
  */
  public static void SetLanguageDirectory(String language)
  {
    language.trim();
    for (int i = 0; i < languages.length; ++i)
    {
      if (language.equals(languages[i]) == true)
      {
        __languageDirectory = language;
        return;
      }
    }
    JOptionPane.showMessageDialog(null, "Error: Resources.SetLanguageDirectory: invalid language \"" + language + "\"", "SetLanguageDirectory()", JOptionPane.ERROR_MESSAGE);
  }

  public static String GetLanguageDirectory()
  {
    return __languageDirectory;
  }

  public static String[] GetCountries()
  {
    return countries;
  }

  public static String[] GetCountryCodes()
  {
    return iso3166CountryCodes;
  }

  public static String[] GetLanguages()
  {
    return languages;
  }

  public static String[] GetDays()
  {
    return __days;
  }

  public static String[] GetFirstLetterOfDays()
  {
    return __firstLetterOfDays;
  }

  public static String[] GetShortDays()
  {
    return __shortDays;
  }

  public static String[] GetMonths()
  {
    return __months;
  }

  public static String[] GetShortMonths()
  {
    return __shortMonths;
  }

  public static String[] GetStates()
  {
    return __states;
  }

  private static final String[] __days              = { "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

  private static final String[] __shortDays         = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" };

  private static final String[] __firstLetterOfDays = { "S", "M", "T", "W", "T", "F", "S" };

  private static final String[] __months            = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };

  private static final String[] __shortMonths       = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

  private static final String[] __states            = { "Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho",
    "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada",
    "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota",
    "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming", "District of Columbia" };

  private static final String[] countries           = { UNITED_STATES, ARGENTINA, BELIZE, BOLIVIA, BRAZIL, CANADA, CHILE, COLUMBIA, COSTA_RICA, ECUADOR, EL_SALVADOR, GUADELOUPE, GUATEMALA, HONDURAS,
    MEXICO, NICARAGUA, PANAMA, PARAGUAY, PERU, URUGUAY, VENEZUELA };

  // this needs to be ordered the same as countries[]
  private static final String[] iso3166CountryCodes = { "US", "AR", "BZ", "BO", "BR", "CA", "CL", "CO", "CR", "EC", "SV", "GP", "GT", "HN", "MX", "NI", "PA", "PY", "PE", "UY", "VE" };

  private final static String[] languages           = { ENGLISH_US,
                                                    //ENGLISH_UK,
    SPANISH,
                                                    //FRENCH,
                                                    //PORTUGUESE
                                                    };

} // end of Resources class
