package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;

/**
 * <p>Title: DateChooserListener</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class DateChooserListener
  extends _DateChooserAdapter
  implements ActionListener
{

  protected JButton            _button;

  protected _GregorianCalendar _initialDate;

  protected _GregorianCalendar _finalDate;

  protected JLabel             _label;

  public DateChooserListener(JButton button, _GregorianCalendar initialDate, _GregorianCalendar finalDate, JLabel label)
  {
    super();

    _button = button;
    _initialDate = initialDate;
    _finalDate = finalDate;
    _label = label;

    _button.addActionListener(this);
  }

  @Override
  public void DateChooserDateSelected(_DateChooserEvent event)
  {
    _finalDate = event.GetSource().GetDate();
    _label.setText(_finalDate.GetDateString());
  }

  @Override
  public void actionPerformed(ActionEvent event)
  {
    _DateChooserPopup.ShowPopup(this, _button, _initialDate);
  }

}
