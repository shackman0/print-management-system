package shackelford.floyd.printmanagementsystem.common;

import java.awt.Component;

import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingWorker;


/**
 * <p>Title: MessageDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MessageDialog
{

  public static void ShowMessageDialog (
    Component parentComponent,
    Object    message,
    String    title,
    int messageType )
  {
    MyJOptionPane pane = new MyJOptionPane (message,messageType);
    JDialog dialog = pane.createDialog(parentComponent, title);
    dialog.setVisible(true);
  }

  /**
   * This method displays the message dialog and immediately
   * returns to the caller, hence the "no wait" appendix to the
   * method name.
   */

  public static void ShowMessageDialogNoWait (
    Component  parentComponent,
    Object     message,
    String     title,
    int        messageType )
  {
    MyJOptionPane pane = new MyJOptionPane (message,messageType);
    final JDialog dialog = pane.createDialog(parentComponent, title);

    new SwingWorker<Object,Object>()
    {
      @Override
      public Object doInBackground()
      {
        dialog.setVisible(true);

        return null;
      }
    }.execute();
  }

  public static void ShowErrorMessageDialog (
    Component parentComponent,
    Object    message,
    String    title)
  {
    ShowMessageDialog (
      parentComponent,
      message,
      title,
      JOptionPane.ERROR_MESSAGE );
  }

  public static void ShowWarningMessageDialog (
    Component parentComponent,
    Object    message,
    String    title)
  {
    ShowMessageDialog (
      parentComponent,
      message,
      title,
      JOptionPane.WARNING_MESSAGE );
  }

  public static void ShowInfoMessageDialog (
    Component parentComponent,
    Object    message,
    String    title)
  {
    ShowMessageDialog (
      parentComponent,
      message,
      title,
      JOptionPane.INFORMATION_MESSAGE );
  }

}