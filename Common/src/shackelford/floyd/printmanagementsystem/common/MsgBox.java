package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


/**
 * <p>Title: MsgBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MsgBox
  extends JFrame
  implements PanelNotifier
{

  /**
   * 
   */
  private static final long     serialVersionUID = -1242460967902173508L;

  private String                _message;

  private final JTextArea       _messageTextArea = new JTextArea();

  private Container             _northPanel;

  private Container             _eastPanel;

  private Container             _westPanel;

  private Container             _southPanel;

  private Container             _centerPanel;

  private ArrayList<PanelListener>             _panelListeners;

  private String                _title;

  protected static final String TITLE            = "TITLE";

  private static final int      MAX_COLUMNS      = 40;

  public MsgBox()
  {
    super();
  }

  public MsgBox(String message, String title)
  {
    super();

    Initialize(message, title);
  }

  public void Initialize(String message, String title)
  {
    _message = message;
    _title = title;

    setIconImage(SystemUtilities.GetLogoIconImage());

    setTitle(_title);

    setResizable(false);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    AssignDataToFields();

    pack();

    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = ((screenSize.width - getWidth()) / 2) + (ran.nextInt(150) - 75);
    int y = ((screenSize.height - getHeight()) / 2) + (ran.nextInt(100) - 50);
    setLocation(x, y);

    validate();
  }

  @Override
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    if (visible == true)
    {
      toFront();
    }
  }

  protected Container CreateNorthPanel()
  {
    JPanel northPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));

    _messageTextArea.setLineWrap(true);
    _messageTextArea.setWrapStyleWord(true);
    _messageTextArea.setEditable(false);
    _messageTextArea.setBackground(northPanel.getBackground());
    _messageTextArea.setColumns(MAX_COLUMNS);
    northPanel.add(_messageTextArea);

    return northPanel;
  }

  protected Container CreateCenterPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected void AssignDataToFields()
  {
    // find out how many newline characters '\n' there are in the
    // message text. we need 1 more line than there are newlines
    // to display the entire message.
    int rows = 1;
    int index = _message.indexOf('\n');
    while (index != -1)
    {
      rows++;
      index = _message.indexOf('\n', index + 1);
    }

    _messageTextArea.setColumns(MAX_COLUMNS);
    _messageTextArea.setRows(rows);
    _messageTextArea.setText(_message);
  }

  public void SetMessageText(String messageText)
  {
    _message = messageText;
    AssignDataToFields();
    _messageTextArea.invalidate();
    validate();
  }

  /**
    use this method to be notified when the panel exits
  */

  @Override
  public void AddPanelListener(PanelListener listener)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_panelListeners.indexOf(listener) == -1)
      {
        _panelListeners.add(listener);
      }
    }
  }

  @Override
  public void AddPanelListeners(ArrayList<PanelListener> listeners)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddPanelListener((listeners.get(i)));
      }
    }
  }

  /**
    use this method to remove yourself as a listener
  */

  @Override
  public void RemovePanelListener(PanelListener listener)
  {
    if ((listener != null) && (_panelListeners != null))
    {
      int listenerIndex = _panelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  /**
    this method is used to let a listeners know when this panel exits.
  */

  @Override
  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  /**
    this method is used to let a listeners know when this panel changes.
  */

  @Override
  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.PanelStateChange(action);
      }
    }
  }

  /**
    this method is used to shut down the window when the user
    selects the system close command.
  */
  protected class WindowListener
    extends WindowAdapter
  {

    @Override
    public void windowClosing(WindowEvent event)
    {
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
  }

}
