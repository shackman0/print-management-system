package shackelford.floyd.printmanagementsystem.common;

import java.text.DecimalFormat;
import java.util.Enumeration;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;



/**
 * <p>Title: MyJTable</p>
 * <p>Description:
  this class provides a means to add additional functionality to JTable
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MyJTable
  extends JTable
{

  /**
   * 
   */
  private static final long serialVersionUID = 3453584820182200270L;
  protected static final DecimalFormat __formatter = new DecimalFormat("#,##0.000");

  public MyJTable()
  {
    super();
  }

  /**
   * this method sets all the column cell renderers to default renderers
   */
  public void SetColumnCellRenderers()
  {
    for (int indx = 0; indx < getColumnCount(); indx++)
    {
      Class<?> columnClass = getColumnClass(indx);
      if (columnClass == Double.class)
      {
        SetColumnCellRenderer(indx, new DoubleColumnCellRenderer());
      }
    }
  }

  public void SetColumnCellRenderer(int modelColumnNumber, TableCellRenderer cellRenderer)
  {
    TableColumnModel tableColumnModel = getColumnModel();
    TableColumn tableColumn = tableColumnModel.getColumn(modelColumnNumber);
    tableColumn.setCellRenderer(cellRenderer);
  }

  /**
    This operation sets the columns to be at least as wide as the column headings.
    this won't do anything if you haven't first pack()'ed the panel on which it appears.

    @param horizontalPad an amount by which to make the columns wider;
      i.e. it sets the column width to column heading size + pad
  */
  public void SetColumnHeaderWidths(int horizontalPad)
  {
    // simply determining the width of the raw string will not return the
    // correct width of the column because the column text may be in html format.
    // we use the width of the string as rendered by JLabel
    // to determine the true default width of the column.
    JLabel testLabel = new JLabel();

    // process each column in the table
    TableColumnModel tableColumnModel = getColumnModel();
    for (Enumeration<?> enumeration = tableColumnModel.getColumns(); enumeration.hasMoreElements();)
    {
      TableColumn tableColumn = (TableColumn) enumeration.nextElement();
      String columnHeaderString = tableColumn.getHeaderValue().toString();
      testLabel.setText(columnHeaderString);
      int stringWidth = testLabel.getPreferredSize().width;
      tableColumn.setPreferredWidth(stringWidth + horizontalPad);
    }
  }

  public void SetColumnHeaderWidths(AbstractSQL1Table sqlTable)
  {
    // simply determining the width of the raw string will not return the
    // correct width of the column because the column text may be in html format.
    // we use the width of the string as rendered by JLabel
    // to determine the true default width of the column.
    JLabel testLabel = new JLabel();

    // process each column in the table
    TableColumnModel tableColumnModel = getColumnModel();
    for (Enumeration<?> enumeration = tableColumnModel.getColumns(); enumeration.hasMoreElements();)
    {
      TableColumn tableColumn = (TableColumn) enumeration.nextElement();
      String columnHeaderString = tableColumn.getHeaderValue().toString();
      testLabel.setText(columnHeaderString);
      TableModel tableModel = getModel();
      if (tableModel instanceof SortFilterTableModel)
      {
        tableModel = ((SortFilterTableModel) tableModel).GetTableModel();
      }
      String sqlTableColumnName;
      if (tableModel instanceof ResultSetTableModel)
      {
        sqlTableColumnName = ((ResultSetTableModel) tableModel).MapViewNameToColumnName(columnHeaderString);
      }
      else
      {
        sqlTableColumnName = sqlTable.GetViewToColumnNamesMap().get(columnHeaderString).toString();
      }
      int stringWidth = testLabel.getPreferredSize().width;
      tableColumn.setPreferredWidth(stringWidth + sqlTable.GetColumnViewWidthAddOn(sqlTableColumnName));
    }
  }

  /**
    converts a view column name into a view column index. returns -1 if the column
    name is not found.

    @param columnName the column name string to be found
  */
  public int ConvertColumnNameToColumnIndex(String columnName)
  {
    int columnIndex = -1;
    for (int i = 0; i < columnModel.getColumnCount(); i++)
    {
      TableColumn tableColumn = columnModel.getColumn(i);
      if (tableColumn.getHeaderValue().toString().equals(columnName) == true)
      {
        columnIndex = i;
        break;
      }
    }
    return columnIndex;
  }

  private class DoubleColumnCellRenderer
    extends DefaultTableCellRenderer
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 5546065199847142985L;

    public DoubleColumnCellRenderer()
    {
      super();
      setHorizontalAlignment(SwingConstants.RIGHT);
    }

    @Override
    protected void setValue(Object value)
    {
      Double valueDouble = (Double) value;
      setText(__formatter.format(valueDouble));
    }
  }

}
