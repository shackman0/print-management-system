package shackelford.floyd.printmanagementsystem.common;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

import javax.swing.JFrame;
import javax.swing.JTable;


/**
 * <p>Title: TablePrinter</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class TablePrinter
  extends AbstractPrinter
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 5036077146261547189L;

  public TablePrinter(JFrame frame, MyJTable table, String title)
  {
    super(frame);

    _table = table;
    _title = title;

    if (_title.endsWith(" Panel") == true)
    {
      _title = _title.substring(0, _title.indexOf(" Panel"));
    }
    if (_title.endsWith(" List") == true)
    {
      _title = _title.substring(0, _title.indexOf(" List")) + "s";
    }
  }

  @Override
  public int print(Graphics g, PageFormat pageFormat, int pageNumber)
  {
    super.print(g, pageFormat, pageNumber);

    Graphics2D g2 = (Graphics2D) g;

    JTable tableView = _table;
    int pageIndex = pageNumber;

    g2.setColor(Color.black);
    int fontHeight = g2.getFontMetrics(tableView.getFont()).getHeight();
    //    int fontDesent=g2.getFontMetrics(tableView.getFont()).getDescent();

    double pageHeight = pageFormat.getImageableHeight() - fontHeight;
    double pageWidth = pageFormat.getImageableWidth();
    double tableWidth = tableView.getColumnModel().getTotalColumnWidth();

    double scale = 1;
    if (tableWidth >= pageWidth)
    {
      scale = pageWidth / tableWidth;
      if (scale < .6f)
      {
        scale = .6f;
      }
    }

    double headerHeightOnPage = tableView.getTableHeader().getHeight() * scale;
    double tableWidthOnPage = tableWidth * scale;

    double oneRowHeight = (tableView.getRowHeight() + tableView.getRowMargin()) * scale;
    int numRowsOnAPage = (int) ((pageHeight - headerHeightOnPage - _offsets[TITLE_NDX]) / oneRowHeight) - 1;
    double pageHeightForTable = oneRowHeight * numRowsOnAPage;
    int totalNumPages = (int) Math.ceil(((double) tableView.getRowCount()) / numRowsOnAPage);
    if (pageIndex >= totalNumPages)
    {
      return NO_SUCH_PAGE;
    }

    g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());

    g2.translate(0f, headerHeightOnPage + _offsets[TITLE_NDX] * 2);
    g2.translate(0f, -pageIndex * pageHeightForTable);
    g2.setClip(0, (int) (pageHeightForTable * pageIndex), (int) Math.ceil(tableWidthOnPage), (int) Math.ceil(pageHeightForTable));

    g2.scale(scale, scale);
    tableView.print(g2);
    g2.scale(1 / scale, 1 / scale);
    g2.translate(0f, pageIndex * pageHeightForTable);
    g2.translate(0f, -headerHeightOnPage - _offsets[TITLE_NDX] * 2);

    g2.translate(0f, _offsets[TITLE_NDX] * 2);
    g2.setClip(0, 0, (int) Math.ceil(tableWidthOnPage), (int) Math.ceil(headerHeightOnPage));
    g2.scale(scale, scale);
    tableView.getTableHeader().print(g2); //paint header at top
    g2.scale(1 / scale, 1 / scale);
    g2.translate(0f, -_offsets[TITLE_NDX] * 2);

    g2.setClip(0, 0, (int) Math.ceil(tableWidthOnPage), (int) (_offsets[TITLE_NDX] + _fontMetrics[TITLE_NDX].getMaxDescent()));
    g2.setFont(_fonts[TITLE_NDX]);
    g2.drawString(_title, 0f, _offsets[TITLE_NDX]);
    String pageOf = "Page " + (pageIndex + 1) + " of " + String.valueOf(totalNumPages).trim();
    g2.drawString(pageOf, (float) RightJustify(_fontMetrics[TITLE_NDX], pageOf, pageFormat), _offsets[TITLE_NDX]);

    return Printable.PAGE_EXISTS;
  }

  protected MyJTable _table;

  protected String   _title;

}
