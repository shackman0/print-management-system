package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;


/**
 * <p>Title: AbstractSQLDatabase</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQLDatabase
{

  /**
   * this is the list of all the tables supported by this database
   * the key is the table name in string form and the value is a reference
   * to an instance of the table.
   */
  protected HashMap<String, AbstractSQL1Table> _tablesMap              = null;

  /**
   * The database program that we are connected to
   */
  protected String                             _databaseProgram        = null;

  /**
   * The host where the database resides and the database program is running
   */
  protected String                             _databaseHost           = null;

  /**
    the password to use when logging in to the database
  */
  protected String                             _databasePassword       = null;

  /**
    this is the port we connect to the database on
  */
  protected String                             _databasePort           = null;

  /**
    the URL at which the database may be found by this service
  */
  protected String                             _databaseURL            = null;

  /**
    the user id to use when logging in to the database
  */
  protected String                             _databaseUserID         = null;

  /**
    this is our connection to the database
  */
  protected Connection                         _databaseConnection     = null;

  /**
    I need to be sure Done() only does its thing once per instance
  */
  protected boolean                            _didDone                = false;

  /**
    this is an array of sql statements
  */
  protected SQLStatementArray                  _sqlStatementArray      = null;

  /**
   * This is the name of the database we are connected to
   */
  protected String                             _databaseName           = null;

  private static final String                  SET_SQL_INHERITANCE_OFF = "SET SQL_Inheritance TO OFF";

  public AbstractSQLDatabase(String databaseProgram, String databaseHost, // ipaddr or hostname
    String databasePort, String databaseName, String databaseUserID, String databasePassword)
  {
    _tablesMap = new HashMap<>(20);
    _didDone = false;
    _databaseProgram = databaseProgram;
    _databaseHost = databaseHost;
    _databasePort = databasePort;
    _databaseName = databaseName;
    _databaseUserID = databaseUserID;
    _databasePassword = databasePassword;
    _databaseURL = MakeDatabaseURL(databaseProgram, databaseHost, databasePort, databaseName);
  }

  /**
    the finalize method is here just in case the creator forgot to call
    the done() method
  */

  @Override
  protected void finalize()
  {
    Done();
  }

  /**
    this method is used to signal that you are done with the database.
  */
  public void Done()
  {
    if (_didDone == false)
    {
      CloseDatabase();
      if (this == GetDefaultDatabase())
      {
        ClearDefaultDatabase();
      }
      _didDone = true;
    }
  }

  public abstract void ClearDefaultDatabase();

  protected abstract AbstractSQLDatabase GetDefaultDatabase();

  public String GetDatabaseName()
  {
    return _databaseName;
  }

  public String GetDatabaseURL()
  {
    return _databaseURL;
  }

  public String GetDatabaseProgram()
  {
    return _databaseProgram;
  }

  public String GetDatabaseHost()
  {
    return _databaseHost;
  }

  public String GetDatabasePassword()
  {
    return _databasePassword;
  }

  public String GetDatabasePort()
  {
    return _databasePort;
  }

  public String GetDatabaseUserID()
  {
    return _databaseUserID;
  }

  /**
   * this operation returns a SQLStatement of a query run against the database.
   * be sure to release the SQLStatement when you're done with it.
   */
  public SQLStatement GetResultSetFromQuery(String sqlString)
  {
    SQLStatement sqlStatement = GetAvailableSQLStatement();
    sqlStatement.ExecuteQuery(sqlString);
    return sqlStatement;
  }

  public SQLStatementArray GetSQLStatementArray()
  {
    return _sqlStatementArray;
  }

  public SQLStatement GetAvailableSQLStatement()
  {
    return _sqlStatementArray.GetAvailableSQLStatement();
  }

  public void RecordEvent(String eventDescription)
  {
    // do nothing
  }

  /**
    this operation connects us to the database if we are not already connected
  */
  public void ConnectDatabase()
    throws SQLException
  {
    if (_databaseConnection == null)
    {
      _databaseConnection = DriverManager.getConnection(_databaseURL, _databaseUserID, _databasePassword);
      _sqlStatementArray = new SQLStatementArray(_databaseConnection);
      try
      (SQLStatement sqlStmt = _sqlStatementArray.GetAvailableSQLStatement();)
      {
        sqlStmt.ExecuteUpdate(SET_SQL_INHERITANCE_OFF);
      }
    }
  }

  /**
    this operation shuts down our database connection if one is open.
    use this to release the database connection but still keep the
    database object alive for a future connection.
  */
  public void CloseDatabase()
  {
    if (_databaseConnection != null)
    {
      try
      {

        _sqlStatementArray.ReleaseAll();
        _sqlStatementArray = null;

        _databaseConnection.close();
        _databaseConnection = null;

      }
      catch (SQLException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        MessageDialog.ShowMessageDialog(null, excp, "databaseConnection.close()", JOptionPane.ERROR_MESSAGE);
      }
    }
  }

  public String[] GetTableNames()
  {
    ArrayList<String> tableNamesArrayList = new ArrayList<>(10);
    try
    {
      DatabaseMetaData databaseMetaData = _databaseConnection.getMetaData();
      try
      (ResultSet schemasResultSet = databaseMetaData.getSchemas();)
      {
        while (schemasResultSet.next())
        {
          String schemaName = schemasResultSet.getString(1);
          try
          (ResultSet tablesResultSet = databaseMetaData.getTables(null, schemaName, "%", null);)
          {
            while (tablesResultSet.next())
            {
              tableNamesArrayList.add(tablesResultSet.getString(3) + " " + tablesResultSet.getString(4));
            }
          }
        }
      }
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
    tableNamesArrayList.trimToSize();
    String[] tableNames = new String[tableNamesArrayList.size()];
    tableNamesArrayList.toArray(tableNames);

    return tableNames;
  }

  public HashMap<String, AbstractSQL1Table> GetTablesMap()
  {
    return _tablesMap;
  }

  public AbstractSQL1Table GetTableByName(String tableName)
  {
    return _tablesMap.get(tableName);
  }

  /**
    this method prints out all the fields
  */
  @Override
  public String toString()
  {
    java.util.Hashtable<String, Object> h = new java.util.Hashtable<>();
    Class<? extends AbstractSQLDatabase> cls = getClass();
    Field[] f = cls.getDeclaredFields();
    try
    {
      AccessibleObject.setAccessible(f, true);
      for (int i = 0; i < f.length; i++)
      {
        h.put(f[i].getName(), f[i].get(this));
      }
    }
    catch (SecurityException ex)
    {
      ex.printStackTrace();
    }
    catch (IllegalAccessException ex)
    {
      ex.printStackTrace();
    }
    if (cls.getSuperclass().getSuperclass() != null)
    {
      h.put("super", super.toString());
    }
    return cls.getName() + h;
  }

  /**
    this is a convenience method to construct a database URL.
  */
  public static String MakeDatabaseURL(String databaseProgram, String databaseHost, String databasePort, String databaseName)
  {
    return "jdbc:" + databaseProgram + "://" + databaseHost + ":" + databasePort + "/" + databaseName;
  }

}
