package shackelford.floyd.printmanagementsystem.common;

import javax.swing.Action;
import javax.swing.JRadioButton;


/**
 * <p>Title: ActionRadioButton</p>
 * <p>Description:
  this class implements an action radio button. pass it an action and it
  creates a jradiobutton registed to look for the specified action.
  You must set the Action.NAME and the Action.SHORT_DESCRIPTION fields
  for this to work. Otherwise, you will get a null pointer exception.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ActionRadioButton
  extends JRadioButton
{
  /**
   * Eclipse generated value 
   */
  private static final long serialVersionUID = -3403155253736527618L;

  public ActionRadioButton(Action act)
  {
    setText(act.getValue(Action.NAME).toString());
    setToolTipText(act.getValue(Action.SHORT_DESCRIPTION).toString());
    addActionListener(act);
  }
}
