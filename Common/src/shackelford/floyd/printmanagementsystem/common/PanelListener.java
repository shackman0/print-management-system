package shackelford.floyd.printmanagementsystem.common;




/**
 * <p>Title: PanelListener</p>
 * <p>Description:
  use this interface to be notified when a panel is done

  here's a typical implementation:

public class PanelManager
  implements PanelListener
{
  public PanelManager ( )
  {
    GUI gui = new GUI();
    gui.AddPanelListener(this);
    gui.setVisible(true);
  }

  public void EndOfPanel (char action)
  {
    // child panel is done
  }

  public void PanelStateChange (char action)
  {
    if (action == 's')
    {
      NotifyListPanelListeners();
    }
  }

}
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface PanelListener
{

  public void EndOfPanel (char action);

  public void PanelStateChange (char action);

}
