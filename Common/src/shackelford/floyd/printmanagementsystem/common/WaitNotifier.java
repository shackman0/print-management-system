package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: WaitNotifier</p>
 * <p>Description:
  use this interface to have other objects wait for an event to occur.

  here's a typical implementation:


  public class AClass
    implements WaitNotifier
  {

    protected synchronized void NotifyListeners(char event)
    {
      notificationEvent = event;
      notifierHasEvent = Boolean.TRUE;
      notifyAll();
      notifierHasEvent = Boolean.FALSE;
    }

    public synchronized char WaitOnNotifier ()  // note the addition of the 'synchronized' modifier!
      throws InterruptedException
    {
      while (notifierHasEvent.equals(Boolean.FALSE))
      {
        wait();
      }
      return notificationEvent;
    }

    private Boolean notifierHasEvent;
    private char notificationEvent;

  }
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface WaitNotifier
{

  public void NotifyListeners(char event);

  public char WaitOnNotifier ()
    throws InterruptedException;

}
