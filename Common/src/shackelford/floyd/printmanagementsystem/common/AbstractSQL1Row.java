package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.util.ArrayList;


/**
 * <p>Title: AbstractSQL1Row</p>
 * <p>Description:
  this class defines a sql row.

  Rules for children:
  1. define a default, no parms constructor (i.e. public MyRowClass() { ... } )
  2a. you must prefix each sql field with the SQL_PREFIX string so that the
     reflection mechanism can find the fields you want to be read from and
     written to the database
  2b. you must prefix each field that makes up the primary key with the
     SQL_PRIMARY_KEY_PREFIX string so that the reflection mechanism can
     determine which columns make up the primary key.
  3. You must define Get, Set, and static Name methods for each SQL_PREFIX'ed field (sans the SQL_PREFIX)
     e.g. _sql_a_field will have Get_a_field(), Set_a_field(x), and Name_a_field() methods. The get/set methods
     must always use copies and not the originals. The Name_() methods return the sql column name (i.e. the
     field name sans the SQL_PREFIX part). Note that the construction of these methods uses the field name
     in a particular manner to construct the method name.
     Note: get methods must convert from external format/value into internal format/value. likewise,
     set methods must convert from internal value to external value.
  4a. define the attribute "private static Field[] __sqlFields;"
  4b. define (and override) the method "public Field[] GetSQLFields()" to return your own static
     __sqlFields
  4c. add "__sqlFields = ConstructSQLFieldsArray(<yourclass>.class);" to your static class initializer
  5a. define the attribute "private static Field[] __sqlKeyFields;"
  5b. define (and override) the method "public Field[] GetSQLKeyFields()" to return your own static
     __sqlKeyFields
  5c. add "__sqlKeyFields = ConstructSQLKeyFieldsArray(__sqlFields);" to your static class initializer
    after the "__sqlFields = ConstructSQLFieldsArray(<yourclass>.class);" statement from 4c above.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL1Row
  implements Cloneable
{

  protected AbstractSQL1Table    _sqlTable;

  public static final int        COLUMN_DOES_NOT_EXIST       = -1;

  public static final Boolean    TRUE                        = new Boolean(true);

  public static final Boolean    FALSE                       = new Boolean(false);

  public static final Integer    ZERO_INTEGER                = new Integer(0);

  public static final Float      ZERO_FLOAT                  = new Float(0f);

  public static final String     EMPTY_STRING                = new String("");

  public static final Double     ZERO_DOUBLE                 = new Double(0);

  public static final BigDecimal ZERO_BIG_DECIMAL            = new BigDecimal(0);

  protected static final String  SQL_PREFIX                  = "_sql_";

  protected static final String  GET_METHOD_PREFIX           = "Get_";

  protected static final String  GET_FORMATTED_METHOD_PREFIX = "GetFormatted_";

  protected static final String  SET_METHOD_PREFIX           = "Set_";

  protected static final String  NAME_METHOD_PREFIX          = "Name_";

  protected static final String  AND                         = " and ";

  public AbstractSQL1Row()
  {
    super();
  }

  public AbstractSQL1Row(AbstractSQL1Table sqlTable)
  {
    Initialize(sqlTable);
  }

  public AbstractSQL1Row(AbstractSQL1Table sqlTable, AbstractSQL1Row initRow)
  {
    Initialize(sqlTable, initRow);
  }

  public AbstractSQL1Row(AbstractSQL1Table sqlTable, ResultSet resultSet)
  {
    Initialize(sqlTable, resultSet);
  }

  public void Initialize(AbstractSQL1Table sqlTable)
  {
    _sqlTable = sqlTable;
    InitFields();
  }

  public void Initialize(AbstractSQL1Table sqlTable, AbstractSQL1Row initRow)
  {
    Initialize(sqlTable);
    SetValues(initRow);
  }

  public void Initialize(AbstractSQL1Table sqlTable, ResultSet resultSet)
  {
    Initialize(sqlTable);
    SetValues(resultSet);
  }

  protected boolean OK2Remove()
  {
    return true;
  }

  public AbstractSQL1Table GetSQLTable()
  {
    return _sqlTable;
  }

  public AbstractSQL1Table GetSQL1Table()
  {
    return _sqlTable;
  }

  public boolean Update()
  {
    return _sqlTable.UpdateRow(this);
  }

  public boolean Remove()
  {
    return _sqlTable.RemoveRow(this);
  }

  public boolean Insert()
  {
    return _sqlTable.InsertRow(this);
  }

  public AbstractSQL1Row DeepCopy()
  {
    AbstractSQL1Row newRow = null;
    try
    {
      newRow = (AbstractSQL1Row) (this.clone());
      newRow.SetValues(this);
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
    return newRow;
  }

  /**
   * this method goes through all "my" fields and asks initRow for its fields with the same name.
   * if initRow has a field with the same name as my field, then i get the data and
   * set my field with it. if initRow doesn't have the field i'm looking for, do nothing because
   * InitFields() already set up my fields for me.
   * @param initRow another AbstractSQLRow
   */
  public void SetValues(AbstractSQL1Row initRow)
  {
    Field[] sqlFields = GetSQLFields();
    for (int i = 0; i < sqlFields.length; i++)
    {
      Field myField = sqlFields[i];
      Field yourField = initRow.GetSQLField(GetFieldName(myField), false);
      if (yourField != null)
      {
        Object yourValue = initRow.GetValue(yourField);
        SetValue(myField, yourValue);
      }
    }
  }

  /**
   * this method goes through all "my" fields and asks initRow for its fields with the same name.
   * if initRow has a field with the same name as my field, then i get the data and
   * set my field with it. if initRow doesn't have the field i'm looking for, do nothing because
   * InitFields() already set up my fields for me.
   *
   * Notice that i don't make a copy of the object i get from the resultSet like i do when i'm
   * initializing from another AbstractSQL1Row. the resultSet.getObject() method gives me a copy
   * of its data already.
   *
   * @param initRow a sql ResultSet
   */
  public void SetValues(ResultSet initRow)
  {
    Field[] sqlFields = GetSQLFields();
    for (int i = 0; i < sqlFields.length; i++)
    {
      Field myField = sqlFields[i];
      try
      {
        Object databaseValue = initRow.getObject(GetFieldName(myField));
        if (databaseValue != null)
        {
          Object convertedDatabaseValue = ConvertSQLObjectToJavaObject(databaseValue);
          // note that i don't use the normal set routine since this is the
          // database version of the data. we want to put it
          // straight into the variable without going through any potential
          // conversion code in the Set_ method. (e.g. Status and Rate_Type)
          SetInternalValue(myField, convertedDatabaseValue);
        }
      }
      catch (Exception excp)
      {
        DebugWindow.DisplayStackTrace(excp);
      }
    }
  }

  /**
   * This operation sets the internal value of the field, bypassing the
   * attribute's Set_ method.
   */
  public void SetInternalValue(Field field, Object value)
  {
    try
    {
      field.set(this, value);
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      DebugWindow.DisplayText("SetInternalValue:this.class=" + getClass().getName());
      DebugWindow.DisplayText("SetInternalValue:Field=" + field);
      DebugWindow.DisplayText("SetInternalValue:value.class=\"" + value.getClass().getName() + "\"");
      DebugWindow.DisplayText("SetInternalValue:value=\"" + value + "\"");
    }
  }

  /**
   * This operation uses the field's Set_ method to set the attribute's value.
   */
  public void SetValue(Field field, Object value)
  {
    String setMethodName = SET_METHOD_PREFIX + GetFieldName(field);
    try
    {
      Method setMethod = getClass().getMethod(setMethodName, new Class[] { value.getClass() });
      setMethod.invoke(this, new Object[] { value });
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      DebugWindow.DisplayText("SetValue:this.class=" + getClass().getName());
      DebugWindow.DisplayText("SetValue:MethodName=" + setMethodName);
      DebugWindow.DisplayText("SetValue:Field=" + field);
      DebugWindow.DisplayText("SetValue:value.class=\"" + value.getClass().getName() + "\"");
      DebugWindow.DisplayText("SetValue:value=\"" + value + "\"");
    }
  }

  /**
   * This method returns the internal value of the attribute, that is, it
   * gets the data from the specified field without going through that field's
   * Get_ method.
   */
  public Object GetInternalValue(Field field)
  {
    Object value = null;
    try
    {
      value = field.get(this);
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
    return value;
  }

  /**
   * This method uses the field's Get_ method to get the value of the attribute.
   */
  public Object GetValue(Field field)
  {
    Object value = null;
    try
    {
      String getMethodName = GET_METHOD_PREFIX + GetFieldName(field);
      Method getMethod = getClass().getMethod(getMethodName, new Class[] {});
      value = getMethod.invoke(this, new Object[] {});
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
    return value;
  }

  public Object GetValue(String fieldName)
  {
    return GetValue(GetSQLField(fieldName, true));
  }

  public Object GetValue(int columnNumber)
  {
    return GetValue(GetSQLFields()[columnNumber]);
  }

  /**
   * This method uses the field's GetFormatted_ method to get the value of the attribute if
   * it has one, otherwise it uses the field's Get_ method.
   */
  public Object GetFormattedValue(Field field)
  {
    Object value = null;
    try
    {
      String getMethodName = GET_FORMATTED_METHOD_PREFIX + GetFieldName(field);
      Method getMethod = getClass().getMethod(getMethodName, new Class[] {});
      value = getMethod.invoke(this, new Object[] {});
    }
    catch (Exception excp)
    {
      value = GetValue(field);
    }
    return value;
  }

  public Object GetFormattedValue(String fieldName)
  {
    return GetFormattedValue(GetSQLField(fieldName, true));
  }

  public Object GetFormattedValue(int columnNumber)
  {
    return GetFormattedValue(GetSQLFields()[columnNumber]);
  }

  public Field GetSQLField(String fieldName, boolean showExcp)
  {
    Field[] sqlFields = GetSQLFields();
    for (int i = 0; i < sqlFields.length; i++)
    {
      Field sqlField = sqlFields[i];
      if (GetFieldName(sqlField).equals(fieldName) == true)
      {
        return sqlField;
      }
    }
    if (showExcp == true)
    {
      new Exception("GetSQLField:fieldName \"" + fieldName + "\" does not exist in class " + getClass().getName()).printStackTrace();
    }
    return null;
  }

  /**
   * This method sets all the sql fields to default values. You only need
   * to override it if you want a field to be initialized with a non-default value.
   */
  protected void InitFields()
  {
    // initialize everything to default values first
    Field[] fields = GetSQLFields();
    for (int i = 0; i < fields.length; i++)
    {
      InitField(fields[i]);
    }
  }

  /**
   * This convenience routine initializes the field to its default value.
   */
  protected void InitField(Field field)
  {
    Class<?> fieldClass = field.getType();
    Object value = null;
    // not all objects have a parm-less constructor, so we handle those
    // first. in fact, we handle most cases explicitely.
    if (fieldClass == Boolean.class)
    {
      value = FALSE;
    }
    else
      if (fieldClass == Integer.class)
      {
        value = ZERO_INTEGER;
      }
      else
        if (fieldClass == Float.class)
        {
          value = ZERO_FLOAT;
        }
        else
          if (fieldClass == String.class)
          {
            value = EMPTY_STRING;
          }
          else
            if (fieldClass == Double.class)
            {
              value = ZERO_DOUBLE;
            }
            else
              if (fieldClass == BigDecimal.class)
              {
                value = ZERO_BIG_DECIMAL;
              }
              //    else
              //    if (fieldClass == _GregorianCalendar.class)
              //    {
              //      value = new _GregorianCalendar();
              //    }
              else
              {
                try
                {
                  Constructor<?> constructor = fieldClass.getDeclaredConstructor(new Class[] {});
                  value = constructor.newInstance(new Object[] {});
                }
                catch (Exception excp)
                {
                  DebugWindow.DisplayStackTrace(excp);
                }
              }
    SetValue(field, value);
  }

  /**
  Prepare the record for insertion into the database.
  */
  public void Prepare()
  {
    Field[] sqlFields = GetSQLFields();
    for (int i = 0; i < sqlFields.length; i++)
    {
      try
      {
        Field sqlField = sqlFields[i];
        Object value = sqlField.get(this); // this can raise an exception
        if (value == null)
        {
          // can't have null fields when we try to update the database,
          // so initialize it to its default value
          InitField(sqlField);
        }
        else
        {
          if (sqlField.getType() == java.lang.String.class)
          {
            // trim up all strings
            sqlField.set(this, ((String) (value)).trim());
          }
        }
      }
      catch (Exception excp)
      {
        DebugWindow.DisplayStackTrace(excp);
      }
    }
  }

  /**
   * this method returns a sql predicate consisting of the primary keys
   * that uniquely identifies this particular row in the table. this
   * predicate can be used for both updating and deleting.
   */
  public String GetPrimaryKeyPredicate()
  {
    Field[] sqlKeyFields = GetSQLKeyFields();
    String predicate = "";
    for (int i = 0; i < sqlKeyFields.length; i++)
    {
      Field sqlKeyField = sqlKeyFields[i];
      predicate += GetFieldName(sqlKeyField) + " = '" + GetInternalValue(sqlKeyField) + "'" + AND;
    }
    if (predicate.length() > 0)
    {
      // remove the trailing AND
      int endOfString = predicate.length() - AND.length();
      predicate = predicate.substring(0, endOfString);
    }
    return predicate;
  }

  /**
   * this operation checks the primary keys between this row and
   * the submitted row. if all the primary keys are equal, then
   * we consider these two rows to be equal, even if they have
   * different object references.
   */
  public boolean Equals(AbstractSQL1Row sqlRow)
  {
    // see if we are comparing to self
    if (this == sqlRow)
    {
      return true;
    }

    // although we are actually doing more than we need, this is a very
    // easy way to see if the keys match
    return GetPrimaryKeyPredicate().equals(sqlRow.GetPrimaryKeyPredicate());
  }

  public String GetJoinColumnPredicateName()
  {
    new Exception("GetJoinColumnPredicateName error in " + this.getClass().getName()).printStackTrace();
    return null;
  }

  public abstract Field[] GetSQLFields();

  public abstract Field[] GetSQLKeyFields();

  public String[] GetSQLFieldNames()
  {
    Field[] fields = GetSQLFields();
    String[] names = new String[fields.length];
    for (int i = 0; i < fields.length; i++)
    {
      names[i] = GetFieldName(fields[i]);
    }
    return names;
  }

  /**
   * use this in a sql select statement as the fields to select to fill in this row type
   * e.g. "select " + GetSelectString() + " from " + table_name ...
   * be sure to pad spaces on each side of GetSelectString() as shown in the example
   */
  public String GetSelectString()
  {
    String selectString = "";
    String tableName = _sqlTable.GetTableName();
    String[] fieldNames = GetSQLFieldNames();
    int i;
    for (i = 0; i < fieldNames.length; i++)
    {
      String fieldName = fieldNames[i];
      selectString += tableName + "." + fieldName + " as " + fieldName + ", ";
    }
    if (i > 0)
    {
      // remove trailing ", "
      selectString = selectString.substring(0, selectString.length() - 2);
    }
    return selectString;
  }

  /**
   * A convenience method for stripping off the SQL_PREFIX from the field name.
   * generally, you should use GetFieldName(field) instead of field.getName().
   */
  public static String GetFieldName(Field sqlField)
  {
    String fieldName = sqlField.getName();
    fieldName = sqlField.getName().substring(SQL_PREFIX.length());
    return fieldName;
  }

  /**
   * The order in which names appear in the returned array is the ordering
   * for the columns in the corresponding list panel. If a field name doesn't
   * appear in the columnNames list, then you'll get an error when the list
   * panel tries to get the name for the column. So be sure that all your
   * SQL_PREFIX'ed fields (including your parents' SQL_PREFIX'ed fields) appear
   * in this list.
  */
  public abstract String[] GetColumnNames();

  /**
   * This is a recursive method that gets all the fields with SQL_PREFIX starting just
   * below Object (Object doesn't have any SQL_PREFIX'ed fields, at least none that i put
   * there), and than back down to the initial aClass class.
   */
  private static void ConstructSQLFieldsArrayList(Class<?> aClass, ArrayList<Field> sqlFieldsArrayList)
  {
    Class<?> superClass = aClass.getSuperclass();
    if (superClass != Object.class)
    {
      ConstructSQLFieldsArrayList(superClass, sqlFieldsArrayList);
    }
    Field[] allFields = aClass.getDeclaredFields();
    for (int i = 0; i < allFields.length; i++)
    {
      if (allFields[i].getName().startsWith(SQL_PREFIX) == true)
      {
        sqlFieldsArrayList.add(allFields[i]);
      }
    }
  }

  public static Field[] ConstructSQLFieldsArray(Class<?> aClass)
  {
    ArrayList<Field> sqlFieldsArrayList = new ArrayList<>(50);
    ConstructSQLFieldsArrayList(aClass, sqlFieldsArrayList);
    sqlFieldsArrayList.trimToSize();
    Field[] justSQLFields = new Field[sqlFieldsArrayList.size()];
    sqlFieldsArrayList.toArray(justSQLFields);
    AccessibleObject.setAccessible(justSQLFields, true);
    return justSQLFields;
  }

  public static String[] ConstructSQLFieldNamesArray(Class<?> aClass)
  {
    Field[] fields = ConstructSQLFieldsArray(aClass);
    String[] fieldNames = new String[fields.length];
    for (int i = 0; i < fields.length; i++)
    {
      fieldNames[i] = GetFieldName(fields[i]);
    }
    return fieldNames;
  }

  public static Field[] ConstructSQLKeyFieldsArray(Field[] sqlFields, String[] keyColumnNames)
  {
    ArrayList<Field> sqlFieldsArrayList = new ArrayList<>(keyColumnNames.length);
    for (int indx = 0; indx < keyColumnNames.length; indx++)
    {
      for (int jndx = 0; jndx < sqlFields.length; jndx++)
      {
        if (GetFieldName(sqlFields[jndx]).equals(keyColumnNames[indx]) == true)
        {
          sqlFieldsArrayList.add(sqlFields[jndx]);
        }
      }
    }
    sqlFieldsArrayList.trimToSize();
    Field[] keyFields = new Field[sqlFieldsArrayList.size()];
    sqlFieldsArrayList.toArray(keyFields);
    return keyFields;
  }

  /**
   * This method converts the sql datatype into the approriate java datatype
   */
  public Object ConvertSQLObjectToJavaObject(Object sqlObject)
  {
    if (sqlObject.getClass() == java.util.Date.class)
    {
      sqlObject = new _GregorianCalendar((java.util.Date) sqlObject);
    }
    else
      if (sqlObject.getClass() == java.util.Calendar.class)
      {
        sqlObject = new _GregorianCalendar((java.util.Calendar) sqlObject);
      }
      else
        if (sqlObject.getClass() == _GregorianCalendar.class)
        {
          sqlObject = new _GregorianCalendar((_GregorianCalendar) sqlObject);
        }
        else
          if (sqlObject.getClass() == java.sql.Time.class)
          {
            sqlObject = sqlObject.toString();
          }
          else
            if (sqlObject.getClass() == java.sql.Timestamp.class)
            {
              sqlObject = sqlObject.toString();
            }
            else
              if (sqlObject.getClass() == java.math.BigDecimal.class)
              {
                sqlObject = new Double(((BigDecimal) sqlObject).doubleValue());
              }
    return sqlObject;
  }

  /**
    this method prints out all the fields
  */
  @Override
  public String toString()
  {
    String toString = "";
    Class<? extends AbstractSQL1Row> cls = getClass();
    Field[] f = cls.getDeclaredFields();
    try
    {
      AccessibleObject.setAccessible(f, true);
      for (int i = 0; i < f.length; i++)
      {
        toString += f[i].getName() + "=\"" + f[i].get(this).toString() + "\" ";
      }
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
    if (cls.getSuperclass() != Object.class)
    {
      toString += super.toString();
    }
    return cls.getName() + ": " + toString;
  }

}
