package shackelford.floyd.printmanagementsystem.common;

import java.sql.ResultSet;


/**
 * <p>Title: AbstractSQL3Row</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL3Row
  extends AbstractSQL2Row
{

  protected String                _sql_updated_dts;

  public AbstractSQL3Row ()
  {
    super();
  }

  public AbstractSQL3Row( AbstractSQL3Table sqlTable )
  {
    super();
    Initialize(sqlTable);
  }

  public AbstractSQL3Row( AbstractSQL3Table sqlTable, AbstractSQL1Row initRow )
  {
    Initialize(sqlTable, initRow);
  }

  public AbstractSQL3Row ( AbstractSQL3Table sqlTable, ResultSet resultSet )
  {
    Initialize(sqlTable,resultSet);
  }

  public String Get_updated_dts() { return new String(_sql_updated_dts); }
  public void Set_updated_dts(String value) { _sql_updated_dts = new String(value); }
  public static String Name_updated_dts() { return "updated_dts"; }

  public AbstractSQL3Table GetSQL3Table()
  {
    return (AbstractSQL3Table)_sqlTable;
  }

}
