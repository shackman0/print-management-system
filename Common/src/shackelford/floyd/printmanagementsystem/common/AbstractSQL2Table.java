package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: AbstractSQL2Table</p>
 * <p>Description:
 * this class keeps track of information about the sql table.
 *
 * this class also provides set manipulation methods on sql rows. if you want information from a
 * single row, look for a method in the AbstractSQL2Row class. If you want methods that operate
 * upon 2 or more rows, look here.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL2Table
  extends AbstractSQL1Table
{

  protected static final int DTS_PAD = 60;

  public AbstractSQL2Table(AbstractSQLDatabase sqlDatabase, String tableName, Class<? extends AbstractSQL1Row> rowClass)
  {
    super(sqlDatabase, tableName, rowClass);
  }

  protected AbstractSQL2Row GetAbstractSQL2Row()
  {
    return (AbstractSQL2Row) _row;
  }

  @Override
  protected boolean InsertWithThisField(String fieldName)
  {
    if ((super.InsertWithThisField(fieldName) == false) || (fieldName.equals(AbstractSQL2Row.Name_created_dts()) == true))
    {
      return false;
    }
    return true;
  }

  @Override
  protected boolean UpdateWithThisField(String fieldName)
  {
    if ((super.UpdateWithThisField(fieldName) == false) || (fieldName.equals(AbstractSQL2Row.Name_created_dts()) == true))
    {
      return false;
    }
    return true;
  }

  @Override
  public int GetColumnViewWidthAddOn(String columnName)
  {
    if (columnName.equals(AbstractSQL2Row.Name_created_dts()) == true)
    {
      return DTS_PAD;
    }
    return super.GetColumnViewWidthAddOn(columnName);
  }
}
