package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.Calendar;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;

/**
 * <p>Title: _DateChooserPopup</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class _DateChooserPopup
  extends javax.swing.JDialog
{

  private static final long                                 serialVersionUID           = 4184657583063849151L;

  protected static final int                                INTER_FIELD_STRUT          = 10;

  protected static final int                                INTRA_FIELD_STRUT          = 5;

  /**
   * Date passed to Popup that will intially be displayed or date last selected on the calendar popup
   */
  protected _GregorianCalendar                              _currentDate;

  protected _EventNotificationManager<_DateChooserListener> _eventNotificationManager  = new _EventNotificationManager<>();

  /**
   * Label in Calendar that shows the current month selected.
   */
  protected JLabel                                          _monthYearLabel            = null;

  /**
   * Buttons are used to display the days on the calendar. <BR>
   * The buttons are layed out on a 7x6 Grid -- NUBER_OF_DAY_BUTTONS should = 42. <BR>
   * The labels on the buttons will adjust according to the current month and year displayed by the calendar. <BR>
   */
  protected JButton[]                                       _days                      = null;

  /**
   * the current day of the month selected
   */
  protected int                                             _currentDayOfMonth         = 1;

  /**
   * Allows user to select month to display in popup
   */
  protected _JComboBox                                      _monthComboBox             = null;

  /**
   * Allows user to cycle the year
   */
  protected JSpinner                                        _yearSpinner               = null;

  /**
   * DateChooserPopup Instance
   */
  public static _DateChooserPopup                           __dateChooserPopup         = null;

  /**
   * Direction Button caption strings.  These are used to cycle through months or years
   */
  public static final String                                YEAR_FORWARD_BUTTON        = ">>";

  public static final String                                YEAR_REVERSE_BUTTON        = "<<";

  public static final String                                MONTH_FORWARD_BUTTON       = ">";

  public static final String                                MONTH_REVERSE_BUTTON       = "<";

  /**
   * Intevals used by direction buttons.
   * When a direction button is pressed the year or month will change by the specified interval
   */
  public static final int                                   YEAR_SPIN_INTERVAL         = 1;

  public static final int                                   MONTH_CHANGE_INTERVAL      = 1;

  // 7x7 grid is used to display day labels(1 row) and day buttons(6 rows).
  // 31 days can potientially be spread across 6 rows(weeks)
  public static final int                                   NUMBER_OF_DAYS_IN_WEEK     = 7;

  public static final int                                   GRID_LAYOUT_ROWS           = 7;

  public static final int                                   NUBER_OF_DAY_BUTTONS       = 42;

  public static final int                                   POPUP_BORDER_THICKNESS     = 1;

  public static final int                                   BUTTON_BORDER_THICKNESS    = 1;

  public static final Dimension                             POPUP_DIMENSION            = new Dimension(200, 180);

  public static final Dimension                             DIRECTION_BUTTON_DIMENSION = new Dimension(20, 10);

  public static final Dimension                             MONTH_COMBO_BOX_DIMENSION  = new Dimension(40, 25);

  public static final Dimension                             YEAR_SPINNER_DIMENSION     = new Dimension(20, 25);

  public static final Dimension                             MONTH_LABEL_DIMENSION      = new Dimension(100, 25);

  /**
   * Factory method -- Singleton to get the shared popup instance
   * @return Shared instance of Popup
   */
  public static _DateChooserPopup GetSharedInstance()
  {
    if (__dateChooserPopup == null)
    {
      __dateChooserPopup = new _DateChooserPopup();
      __dateChooserPopup.Initialize();
    }

    return __dateChooserPopup;
  }

  /**
   * display the popup
   * @param component the component that the date is being displayed for
   * @param initialDate initial date to display. null will display current date.
   */
  public static void ShowPopup(_DateChooserListener listener, Component component, _GregorianCalendar initialDate)
  {
    // Get the DateChooserPopup
    _DateChooserPopup dateChooser = _DateChooserPopup.GetSharedInstance();

    // If the date field is null, popup Calenedar with currentdate.
    if (initialDate == null)
    {
      initialDate = new _GregorianCalendar();
    }
    dateChooser.SetDate(initialDate);

    // Set popup location and show it
    Point point = component.getLocationOnScreen();
    dateChooser.setLocation(point.x, point.y + component.getHeight());
    dateChooser.AddEventListener(listener);
    dateChooser.setVisible(true);
  }

  /**
   * Hide the DateChooserPopup
   */
  public void DismissPopup(_DateChooserListener listener)
  {
    _DateChooserPopup dateChooser = GetSharedInstance();
    dateChooser.setVisible(false);
    dateChooser.RemoveEventListener(listener);
  }

  /**
   * Constructer
   */
  protected _DateChooserPopup()
  {
    super();
  }

  /**
   * Initialize the components
   */
  protected void Initialize()
  {
    _monthYearLabel = new JLabel();
    _days = new JButton[NUBER_OF_DAY_BUTTONS];
    _monthComboBox = new _JComboBox();
    _yearSpinner = new JSpinner();

    addWindowFocusListener(new DialogWindowFocusListener());

    // Initialize the Date
    _currentDate = new _GregorianCalendar();

    // Setup the combo box
    String[] months = Resources.GetMonths();
    for (int indx = 0; indx < months.length; indx++)
    {
      _monthComboBox.addItem(months[indx]);
    }
    _monthComboBox.setFocusable(false);
    _monthComboBox.setPreferredSize(MONTH_COMBO_BOX_DIMENSION);
    _monthComboBox.addActionListener(new MonthComboBoxActionListener());

    // Setup the spinner
    _yearSpinner.setPreferredSize(YEAR_SPINNER_DIMENSION);
    _yearSpinner.addChangeListener(new YearSpinnerChangeListener());
    // Change the spinner formatter so that the editor does not display a comma in the year
    JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor) _yearSpinner.getEditor();
    DefaultFormatterFactory formatterFactory = new DefaultFormatterFactory(new JSpinnerYearFormatter());
    editor.getTextField().setFormatterFactory(formatterFactory);
    // Ensure the spinner does not recieve focus
    _yearSpinner.setFocusable(false);
    editor.getTextField().setEditable(false);
    editor.getTextField().setFocusable(false);

    // Setup the month/year label
    _monthYearLabel.setPreferredSize(MONTH_LABEL_DIMENSION);
    _monthYearLabel.setHorizontalAlignment(SwingConstants.CENTER);

    CreateCalendarView();
  }

  /**
   * Create the initial Calendar Components
   */
  protected void CreateCalendarView()
  {
    JPanel mainPanel = new JPanel();
    JPanel northPanel = new JPanel();
    JPanel centerPanel = new JPanel();
    Box verticalBox = Box.createVerticalBox();
    Box lineBox;

    // Setup the JPanel which will displayed in the Dialog
    mainPanel.setBorder(new LineBorder(Color.BLACK, POPUP_BORDER_THICKNESS));
    mainPanel.setLayout(new BorderLayout());
    mainPanel.add(northPanel, BorderLayout.NORTH);
    mainPanel.add(centerPanel, BorderLayout.CENTER);
    // Allow only the panel to recieve focus so the panel can receive key input to close the dialog
    mainPanel.setFocusable(true);
    // Set Traversal to false in order to capture the Tab key event
    mainPanel.setFocusTraversalKeysEnabled(false);
    mainPanel.addKeyListener(new DialogKeyListener());

    // Setup NORTH Panel
    northPanel.setLayout(new FlowLayout());
    northPanel.add(verticalBox);

    // North - Line1
    lineBox = Box.createHorizontalBox();
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_STRUT));
    // Add Month Combo Box
    lineBox.add(_monthComboBox);
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_STRUT));
    // Add Year Spinner
    lineBox.add(_yearSpinner);
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_STRUT));
    verticalBox.add(lineBox);

    // North - Line2
    verticalBox.add(Box.createVerticalStrut(INTER_FIELD_STRUT));
    lineBox = Box.createHorizontalBox();
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_STRUT));
    // Add Year Reverse Button
    lineBox.add(this.MakeDirectionButton(YEAR_REVERSE_BUTTON));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_STRUT));
    // Add Month Reverse Button
    lineBox.add(this.MakeDirectionButton(MONTH_REVERSE_BUTTON));
    lineBox.add(Box.createHorizontalGlue());
    // Add Month/Year Label
    lineBox.add(_monthYearLabel);
    lineBox.add(Box.createHorizontalGlue());
    // Add Month Forward Button
    lineBox.add(this.MakeDirectionButton(MONTH_FORWARD_BUTTON));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_STRUT));
    // Add Year Forward Button
    lineBox.add(this.MakeDirectionButton(YEAR_FORWARD_BUTTON));
    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_STRUT));
    verticalBox.add(lineBox);

    // CENTER PANEL
    centerPanel.setLayout(new GridLayout(GRID_LAYOUT_ROWS, NUMBER_OF_DAYS_IN_WEEK));
    centerPanel.setBorder(BorderFactory.createEmptyBorder(INTER_FIELD_STRUT, INTRA_FIELD_STRUT, INTER_FIELD_STRUT, INTER_FIELD_STRUT));

    // Add day labels:(S,M,T,W,T,F,S)
    String[] dayName = Resources.GetFirstLetterOfDays();
    for (int indx = 0; indx < dayName.length; indx++)
    {
      JLabel dayLabel = new JLabel(dayName[indx], SwingConstants.CENTER);
      centerPanel.add(dayLabel);
    }

    // Create day buttons
    for (int indx = 0; indx < _days.length; indx++)
    {
      _days[indx] = new JButton();
      _days[indx].addActionListener(new DayButtonActionListener());
      _days[indx].setFocusable(false);
      centerPanel.add(_days[indx]);
    }

    // Setup the Dialog
    this.getContentPane().add(mainPanel);
    this.setSize(POPUP_DIMENSION);
    this.setUndecorated(true);
    this.setFocusable(false);
  }

  /**
   * Set the Day Buttons according to the Month and year shown in the Month Combo Box and Year Spinner.
   * Labels the day buttons
   */
  protected void UpdateCalendarView()
  {
    _GregorianCalendar tempDate = new _GregorianCalendar();

    // create a tempCalendar according to the Month and year shown in the Month Combo Box and Year Spinner
    tempDate.set(Calendar.MONTH, _monthComboBox.getSelectedIndex());
    tempDate.set(Calendar.YEAR, ((Integer) _yearSpinner.getValue()).intValue());
    tempDate.set(Calendar.DAY_OF_MONTH, _currentDayOfMonth);

    // Set text of Month/Year Label
    _monthYearLabel.setText(_monthComboBox.getSelectedItem().toString() + " " + _yearSpinner.getValue());

    // dayNumber is used to label the day buttons on the calendar.
    int dayNumberCounter = 1;
    // Total number of day in the month
    int numberOfDaysInMonth = tempDate.getActualMaximum(Calendar.DAY_OF_MONTH);
    // The position in the week of the first day of the month.
    int firstDayPositionInWeek = tempDate.get(Calendar.DAY_OF_WEEK) - 1;

    // Cycle through all the buttons
    for (int indx = 0; indx < _days.length; indx++)
    {
      // Disable buttons up till the first day's position
      // Disable buttons after the "numberOfDaysInMonth" have been labeled
      if ((indx < firstDayPositionInWeek) || (dayNumberCounter > numberOfDaysInMonth))
      {
        _days[indx].setText(null);
        _days[indx].setEnabled(false);
        _days[indx].setBorder(new EmptyBorder(BUTTON_BORDER_THICKNESS, BUTTON_BORDER_THICKNESS, BUTTON_BORDER_THICKNESS, BUTTON_BORDER_THICKNESS));
      }
      else
      // Label and enable the buttons that are actual days in the month
      {
        _days[indx].setText(String.valueOf(dayNumberCounter));
        _days[indx].setEnabled(true);
        // Highlight the border if the button represents the date in the owner textfield(currentDate)
        if ((dayNumberCounter == _currentDate.get(Calendar.DAY_OF_MONTH)) && (tempDate.get(Calendar.MONTH) == _currentDate.get(Calendar.MONTH))
          && (tempDate.get(Calendar.YEAR) == _currentDate.get(Calendar.YEAR)))
        {
          _days[indx].setBorder(new LineBorder(Color.BLUE, BUTTON_BORDER_THICKNESS + 1));
        }
        else
        // Use the standard border
        {
          _days[indx].setBorder(new LineBorder(Color.BLACK, BUTTON_BORDER_THICKNESS));
        }
        // Increment the dayNumbercounter
        dayNumberCounter++;
      }
    }
  }

  /**
   * Add an event listener
   * @param eventListener
   * no error is generated.
   */
  public void AddEventListener(_DateChooserListener eventListener)
  {
    _eventNotificationManager.AddListener(eventListener);
  }

  /**
   * Remove an event listener.
   * @param eventListener
   * generated.
   */
  public void RemoveEventListener(_DateChooserListener eventListener)
  {
    _eventNotificationManager.RemoveListener(eventListener);
  }

  protected _DateChooserEvent CreateEvent(boolean success)
  {
    _DateChooserEvent event = new _DateChooserEvent(this, success);

    return event;
  }

  /**
   * Override the show method to UpdateCalanderView.
   * If you want a specific date to be shown, be sure to set the date before showing it.
   */
  @Override
  public void setVisible(boolean visible)
  {
    if (visible == true)
    {
      UpdateCalendarView();
    }
    else
    {
      _eventNotificationManager.NotifyListenersOfEvent(_DateChooserListener.DATE_CHOOSER_DISMISSED, CreateEvent(true));
    }

    super.setVisible(visible);
  }

  /**
   * Get the current Calendar
   * @return Clone of popups current date
   */
  public _GregorianCalendar GetDate()
  {
    return new _GregorianCalendar(_currentDate.getTime());
  }

  /**
   * Setter
   * @param newDate NewDate to set as current day
   */
  public void SetDate(_GregorianCalendar newDate)
  {
    _currentDate = new _GregorianCalendar();
    _currentDate.SetDate(newDate.getTime());
    _yearSpinner.setValue(_currentDate.get(Calendar.YEAR));
    _monthComboBox.setSelectedIndex(_currentDate.get(Calendar.MONTH));
    _currentDayOfMonth = _currentDate.get(Calendar.DAY_OF_MONTH);
    UpdateCalendarView();
  }

  /**
   * Create the month and year direction buttons
   * @param caption text placed on the button
   * @return created button
   */
  protected JButton MakeDirectionButton(String caption)
  {
    JButton jButton = new JButton(caption);
    jButton.setBorder(new LineBorder(Color.BLACK, BUTTON_BORDER_THICKNESS));
    jButton.setPreferredSize(DIRECTION_BUTTON_DIMENSION);
    jButton.setActionCommand(caption);
    jButton.addActionListener(new DirectionButtonActionListener());
    jButton.setFocusable(false);
    return jButton;
  }

  /**
   * updates the month or year component according to what direction button is pressed
   * @param direction Caption of button pressed
   */
  protected void DirectionButtonSelected(String direction)
  {
    if (direction.equals(YEAR_FORWARD_BUTTON))
    {
      _yearSpinner.setValue(new Integer(((Integer) _yearSpinner.getValue()).intValue() + YEAR_SPIN_INTERVAL));
    }
    else
      if (direction.equals(YEAR_REVERSE_BUTTON))
      {
        _yearSpinner.setValue(new Integer(((Integer) _yearSpinner.getValue()).intValue() - YEAR_SPIN_INTERVAL));
      }
      else
        if (direction.equals(MONTH_FORWARD_BUTTON))
        {
          _monthComboBox.setSelectedIndex((_monthComboBox.getSelectedIndex() + MONTH_CHANGE_INTERVAL) % _monthComboBox.getItemCount());
        }
        else
          if (direction.equals(MONTH_REVERSE_BUTTON))
          {
            if (_monthComboBox.getSelectedIndex() == Calendar.JANUARY)
            {
              _monthComboBox.setSelectedIndex(Calendar.DECEMBER);
            }
            else
            {
              _monthComboBox.setSelectedIndex((_monthComboBox.getSelectedIndex() - MONTH_CHANGE_INTERVAL) % _monthComboBox.getItemCount());
            }
          }
  }

  /**
   * Process Direction Button events
   */
  protected class DirectionButtonActionListener
    implements ActionListener
  {
    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
      DirectionButtonSelected(actionEvent.getActionCommand());
    }
  }

  /**
   * Sets the currentdate according to the date selected
   * Notifies Listeners
   * @param actionEvent event
   */
  public void DayButtonPressed(ActionEvent actionEvent)
  {
    String day;
    day = ((JButton) actionEvent.getSource()).getText();
    if (day.length() > 0)
    {
      _currentDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(day));
      _currentDate.set(Calendar.MONTH, _monthComboBox.getSelectedIndex());
      _currentDate.set(Calendar.YEAR, ((Integer) _yearSpinner.getValue()).intValue());

      setVisible(false);
      _eventNotificationManager.NotifyListenersOfEvent(_DateChooserListener.DATE_CHOOSER_DATE_SELECTED, CreateEvent(true));
    }
  }

  /**
   * Listen for day buttons pressed
   */
  protected class DayButtonActionListener
    implements ActionListener
  {
    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
      DayButtonPressed(actionEvent);
    }
  }

  /**
   * Call ListerActions to notify listeners
   */
  public void LostFocus()
  {
    _eventNotificationManager.NotifyListenersOfEvent(_DateChooserListener.DATE_CHOOSER_LOST_FOCUS, CreateEvent(true));
    setVisible(false);
  }

  /**
   * Hides the dialog if the dialog loses focus
   */
  protected class DialogWindowFocusListener
    implements WindowFocusListener
  {
    @Override
    public void windowGainedFocus(WindowEvent windowEvent)
    {
      // Do Nothing
    }

    @Override
    public void windowLostFocus(WindowEvent windowEvent)
    {
      LostFocus();
    }
  }

  /**
   * Shift-UP, ESC and TAB  close the Dialog
   */
  protected class DialogKeyListener
    implements KeyListener
  {
    @Override
    public void keyTyped(KeyEvent keyEvent)
    {
      // Do Nothing
    }

    @Override
    public void keyPressed(KeyEvent keyEvent)
    {
      if ((keyEvent.getKeyCode() == KeyEvent.VK_ESCAPE) || ((keyEvent.getKeyCode() == KeyEvent.VK_UP) && (keyEvent.isShiftDown())))
      {
        keyEvent.consume();
        setVisible(false);
        _eventNotificationManager.NotifyListenersOfEvent(_DateChooserListener.DATE_CHOOSER_CANCELLED, CreateEvent(true));
      }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent)
    {
      if (keyEvent.getKeyCode() == KeyEvent.VK_TAB)
      {
        keyEvent.consume();
        setVisible(false);
        _eventNotificationManager.NotifyListenersOfEvent(_DateChooserListener.DATE_CHOOSER_TAB_SELECTED, CreateEvent(true));
      }
    }
  }

  /**
   * Captures Month ComboBox Action events -- updates calendar
   */
  protected class MonthComboBoxActionListener
    implements ActionListener
  {
    @Override
    public void actionPerformed(ActionEvent actionEvent)
    {
      UpdateCalendarView();
    }
  }

  /**
   * Captures Year Spinner changes -- updates calendar
   */
  protected class YearSpinnerChangeListener
    implements ChangeListener
  {
    @Override
    public void stateChanged(ChangeEvent changeEvent)
    {
      UpdateCalendarView();
    }
  }

  /**
   * Modify the JSpinner default formatter.
   * Allows the year to be displayed without a comma.
   */
  protected class JSpinnerYearFormatter
    extends DefaultFormatter
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -9191601799984684309L;

    public JSpinnerYearFormatter()
    {
      super();
    }

    public String stringToValue(Integer integer)
    {
      return integer.toString();
    }
  }

}
