package shackelford.floyd.printmanagementsystem.common;

import javax.swing.Action;
import javax.swing.JMenuItem;


/**
 * <p>Title: ActionMenuItem</p>
 * <p>Description:
  this class implements an action button. pass it an action and it
  creates a jbutton registed to look for the specified action.
  You must set the Action.NAME and the Action.SHORT_DESCRIPTION fields
  for this to work. Otherwise, you will get a null pointer exception.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ActionMenuItem
  extends JMenuItem
{
  /**
   * Eclipse generated value 
   */
  private static final long serialVersionUID = 3042254587480660572L;

  public ActionMenuItem(Action act)
  {
    setText(act.getValue(Action.NAME).toString());
    setToolTipText(act.getValue(Action.SHORT_DESCRIPTION).toString());
    addActionListener(act);
  }
}
