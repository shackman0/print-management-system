package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.ActionEvent;
import java.io.File;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JFrame;
import javax.swing.JMenu;


/**
 * <p>Title: HelpMenu</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class HelpMenu
  extends JMenu
{

  /**
   * 
   */
  private static final long    serialVersionUID   = 3399740000436647139L;

  final String                 _helpFor;

  private final ActionMenuItem _helpForAction;

  private final ActionMenuItem _helpContentsAction;

  private final ActionMenuItem _helpAboutAction;

  private static Resources     __resources;

  private static final String  HELP               = "HELP";

  private static final String  HELP_FOR           = "HELP_FOR";

  private static final String  HELP_FOR_TIP       = "HELP_FOR_TIP";

  private static final String  HELP_CONTENTS      = "HELP_CONTENTS";

  private static final String  HELP_CONTENTS_TIP  = "HELP_CONTENTS_TIP";

  private static final String  HELP_ABOUT         = "HELP_ABOUT";

  private static final String  HELP_ABOUT_TIP     = "HELP_ABOUT_TIP";

  private static final String  HTML_LEADER        = "file:";

  private static final String  HELP_CONTENTS_FILE = "Contents";

  private static final String  HELP_ABOUT_FILE    = "About";

  private static final String  HTML_TRAILER       = ".html";

  static final String          HELP_DIRECTORY     = File.separator + "Help" + File.separator;

  public HelpMenu(JFrame helpFor)
  {
    super();

    _helpFor = helpFor.getClass().getName();

    setText(GetResources().getProperty(HELP).toString());

    _helpForAction = new ActionMenuItem(new HelpForAction());
    add(_helpForAction);
    _helpContentsAction = new ActionMenuItem(new HelpContentsAction());
    add(_helpContentsAction);
    _helpAboutAction = new ActionMenuItem(new HelpAboutAction());
    add(_helpAboutAction);

    setVisible(false);
  }

  private class HelpForAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -7916141795340654878L;

    public HelpForAction()
    {
      putValue(Action.NAME, GetResources().getProperty(HELP_FOR));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(HELP_FOR_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      String url = HTML_LEADER + GlobalAttributes.__resourcesDirectory + File.separator + Resources.GetLanguageDirectory() + File.separator + HELP_DIRECTORY + File.separator + _helpFor + HTML_TRAILER;
      BrowserControl.displayURL(url);
    }
  }

  private class HelpContentsAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -8785698696547804509L;

    public HelpContentsAction()
    {
      putValue(Action.NAME, GetResources().getProperty(HELP_CONTENTS));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(HELP_CONTENTS_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      String url = HTML_LEADER + GlobalAttributes.__resourcesDirectory + File.separator + Resources.GetLanguageDirectory() + File.separator + HELP_DIRECTORY + File.separator + HELP_CONTENTS_FILE
        + HTML_TRAILER;
      BrowserControl.displayURL(url);
    }
  }

  private class HelpAboutAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -9145253560622484161L;

    public HelpAboutAction()
    {
      putValue(Action.NAME, GetResources().getProperty(HELP_ABOUT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(HELP_ABOUT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      String url = HTML_LEADER + GlobalAttributes.__resourcesDirectory + File.separator + Resources.GetLanguageDirectory() + File.separator + HELP_DIRECTORY + File.separator + HELP_ABOUT_FILE
        + HTML_TRAILER;
      BrowserControl.displayURL(url);
    }
  }

  public Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateResources("Utilities", HelpMenu.class.getName());
  }
}
