package shackelford.floyd.printmanagementsystem.common;

import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;
import java.text.ParseException;

import javax.swing.text.AttributeSet;
import javax.swing.text.Document;

/**
 * <p>Title: NumericTextField</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class NumericTextField
  extends OverwritableTextField
  implements NumericPlainDocument.InsertErrorListener
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -3739572521687396554L;

  public NumericTextField()
  {
    this(null, 0, null);
  }

  public NumericTextField(int columns, DecimalFormat format)
  {
    this(null, columns, format);
  }

  public NumericTextField(String text)
  {
    this(text, 0, null);
  }

  public NumericTextField(String text, int columns)
  {
    this(text, columns, null);
  }

  public NumericTextField(String text, int columns, DecimalFormat format)
  {
    super(null, text, columns);

    NumericPlainDocument numericDoc = (NumericPlainDocument) getDocument();
    if (format != null)
    {
      numericDoc.setFormat(format);
    }

    numericDoc.addInsertErrorListener(this);

    addActionListener(new ProcessEnterKey());
    addFocusListener(new ProcessFocusLost());
  }

  public void SetFormat(DecimalFormat format)
  {
    format.setParseIntegerOnly(false);
    ((NumericPlainDocument) getDocument()).setFormat(format);
  }

  public DecimalFormat GetFormat()
  {
    return ((NumericPlainDocument) getDocument()).getFormat();
  }

  public void FormatChanged()
  {
    // Notify change of format attributes.
    SetFormat(GetFormat());
  }

  // Methods to get the field value
  public Long GetLongValue()
  {
    try
    {
      return GetLongValueWithException();
    }
    catch (ParseException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "GetLongValue()");
    }
    return new Long(0);
  }

  public Double GetDoubleValue()
  {
    try
    {
      return GetDoubleValueWithException();
    }
    catch (ParseException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "GetDoubleValue()");
    }
    return new Double(0);
  }

  public Number GetNumberValue()
  {
    try
    {
      return GetNumberValueWithException();
    }
    catch (ParseException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "GetNumberValue()");
    }
    return new Long(0);
  }

  public Long GetLongValueWithException()
    throws ParseException
  {
    return ((NumericPlainDocument) getDocument()).getLongValue();
  }

  public Double GetDoubleValueWithException()
    throws ParseException
  {
    return ((NumericPlainDocument) getDocument()).getDoubleValue();
  }

  public Number GetNumberValueWithException()
    throws ParseException
  {
    return ((NumericPlainDocument) getDocument()).getNumberValue();
  }

  // Methods to set numeric values
  public void SetValue(Number number)
  {
    super.setText(GetFormat().format(number));
  }

  public void SetValue(long l)
  {
    super.setText(GetFormat().format(l));
  }

  public void SetValue(double d)
  {
    super.setText(GetFormat().format(d));
  }

  @Override
  public void setText(String number)
  {
    SetValue(new Double(number).doubleValue());
  }

  public void Normalize()
  {
    // format the value according to the format string
    super.setText(GetFormat().format(GetNumberValue()));
  }

  // Override to handle insertion error

  @Override
  public void insertFailed(NumericPlainDocument doc, int offset, String str, AttributeSet a)
  {
    // By default, just beep
    Toolkit.getDefaultToolkit().beep();
  }

  // Method to create default model

  @Override
  protected Document createDefaultModel()
  {
    return new NumericPlainDocument();
  }

  private class ProcessEnterKey
    implements ActionListener
  {
    public ProcessEnterKey()
    {
      // do nothing
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      Normalize();
    }
  }

  private class ProcessFocusLost
    implements FocusListener
  {
    public ProcessFocusLost()
    {
      // do nothing
    }

    @Override
    public void focusGained(FocusEvent event)
    {
      // do nothing
    }

    @Override
    public void focusLost(FocusEvent event)
    {
      if (event.isTemporary() == false)
      {
        Normalize();
      }
    }
  }

}
