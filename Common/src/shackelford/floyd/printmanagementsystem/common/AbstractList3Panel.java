package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: AbstractList3Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractList3Panel
  extends AbstractList2Panel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 4303708723983818718L;

  public AbstractList3Panel()
  {
    super();
  }

  public AbstractSQL3Table GetAbstractSQL3Table()
  {
    return (AbstractSQL3Table) _sqlTable;
  }
  /*
    public AbstractSQL3Row GetAbstractSQL3Row()
    {
      return (AbstractSQL3Row)_sqlRow;
    }
  */
}
