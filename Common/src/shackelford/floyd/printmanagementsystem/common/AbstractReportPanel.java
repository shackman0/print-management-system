package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;


/**
 * <p>Title: AbstractReportPanel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractReportPanel
  extends JFrame
  implements PanelNotifier
{
  /**
   * 
   */
  private static final long          serialVersionUID      = -5864862241327587541L;

  protected ArrayList<PanelListener> _panelListeners;

  protected Container                _northPanel;

  protected Container                _southPanel;

  protected Container                _eastPanel;

  protected Container                _westPanel;

  protected Container                _centerPanel;

  public Resources                   _resources;

  public MyJTable                    _table;

  protected PageFormat               _pageFormat;

  protected TablePrinter             _tablePrinter;

  protected ActionButton             _printSetupButton;

  protected ActionButton             _printPreviewButton;

  protected ActionButton             _printButton;

  public ActionButton                _showButton;

  protected ActionButton             _dismissButton;

  protected static final String      TITLE                 = "TITLE";

  protected static final String      PRINT_SETUP           = "PRINT_SETUP";

  protected static final String      PRINT_SETUP_TIP       = "PRINT_SETUP_TIP";

  protected static final String      PRINT_PREVIEW         = "PRINT_PREVIEW";

  protected static final String      PRINT_PREVIEW_TIP     = "PRINT_PREVIEW_TIP";

  protected static final String      PRINT                 = "PRINT";

  protected static final String      PRINT_TIP             = "PRINT_TIP";

  protected static final String      SHOW                  = "SHOW";

  protected static final String      SHOW_TIP              = "SHOW_TIP";

  protected static final String      DISMISS               = "DISMISS";

  protected static final String      DISMISS_TIP           = "DISMISS_TIP";

  protected static final String      TRUE                  = "TRUE";

  protected static final String      FALSE                 = "FALSE";

  protected static final int         INTER_FIELD_GAP_WIDTH = 10;

  protected static final int         INTRA_FIELD_GAP_WIDTH = 5;

  protected static final int         HORIZONTAL_STRUT      = 3;

  public AbstractReportPanel()
  {
    super();
    //Initialize();
  }

  protected void Initialize()
  {
    _resources = GetResources();

    addWindowListener(new WindowListener());

    setIconImage(SystemUtilities.GetLogoIconImage());

    setTitle(_resources.getProperty(TITLE).toString());
    setResizable(true);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    pack();
    SizeInitialPanel();
    PositionInitialPanel();
    validate();
  }

  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateCenterPanel()
  {
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    BuildTable();
    scrollPane.setViewportView(_table);
    return scrollPane;
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    Box southPanel = Box.createVerticalBox();

    AddLines2SouthPanel(southPanel);

    Box lineBox = Box.createHorizontalBox();
    southPanel.add(lineBox);

    lineBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT));
    lineBox.add(new HelpButton(this));
    lineBox.add(Box.createHorizontalGlue());

    AddActionButtonsToSouthPanel(lineBox);

    _printSetupButton = new ActionButton(new PrintSetupAction());
    lineBox.add(_printSetupButton);

    _printPreviewButton = new ActionButton(new PrintPreviewAction());
    lineBox.add(_printPreviewButton);

    _printButton = new ActionButton(new PrintAction());
    lineBox.add(_printButton);

    _showButton = new ActionButton(new ShowAction());
    lineBox.add(_showButton);

    _dismissButton = new ActionButton(new DismissAction());
    lineBox.add(_dismissButton);

    lineBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT));

    return southPanel;
  }

  protected void AddLines2SouthPanel(Box verticalBox)
  {
    // do nothing
  }

  protected void AddActionButtonsToSouthPanel(Box lineBox)
  {
    // do nothing
  }

  public abstract void BuildTable();

  public abstract void RefreshList();

  protected void SizeInitialPanel()
  {
    /*
        Dimension panelSize = getSize();
        panelSize.width *= 2.00f;
        panelSize.height *= 1.00f;
        setSize(panelSize);
    */
  }

  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  @Override
  public void AddPanelListener(PanelListener aListener)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (aListener != null)
    {
      if (_panelListeners.indexOf(aListener) == -1)
      {
        _panelListeners.add(aListener);
      }
    }
  }

  @Override
  public void AddPanelListeners(ArrayList<PanelListener> listeners)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddPanelListener((listeners.get(i)));
      }
    }
  }

  @Override
  public void RemovePanelListener(PanelListener aListener)
  {
    if (aListener != null)
    {
      int listenerIndex = _panelListeners.indexOf(aListener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  @Override
  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener aListener = iterator.next();
        aListener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  @Override
  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener aListener = iterator.next();
        aListener.PanelStateChange(action);
      }
    }
  }

  protected abstract Resources GetResources();

  protected class WindowListener
    extends WindowAdapter
  {

    @Override
    public void windowClosing(WindowEvent event)
    {
      NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
      dispose();
    }
  }

  protected void ShowAction()
  {
    RefreshList();
  }

  protected class ShowAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 6070522576228877490L;

    public ShowAction()
    {
      putValue(Action.NAME, _resources.getProperty(SHOW).toString());
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(SHOW_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      ShowAction();
    }
  }

  protected void DismissAction()
  {
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
    dispose();
  }

  protected class DismissAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 2201708759911018302L;

    public DismissAction()
    {
      putValue(Action.NAME, _resources.getProperty(DISMISS).toString());
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(DISMISS_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      DismissAction();
    }
  }

  protected void PrintSetupAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, _resources.getProperty(TITLE));
    }
    _pageFormat = _tablePrinter.PrintSetup(_pageFormat);
  }

  protected class PrintSetupAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -428454475048220190L;

    public PrintSetupAction()
    {
      putValue(Action.NAME, _resources.getProperty(PRINT_SETUP));
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(PRINT_SETUP_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintSetupAction();
    }
  }

  protected void PrintPreviewAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, _resources.getProperty(TITLE));
    }
    _tablePrinter.PrintPreview(_pageFormat);
  }

  protected class PrintPreviewAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -4648361380781945277L;

    public PrintPreviewAction()
    {
      putValue(Action.NAME, _resources.getProperty(PRINT_PREVIEW).toString());
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(PRINT_PREVIEW_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintPreviewAction();
    }
  }

  protected void PrintAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, _resources.getProperty(TITLE));
    }
    _tablePrinter.Print(_pageFormat);
  }

  protected class PrintAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -7328353577508900713L;

    public PrintAction()
    {
      putValue(Action.NAME, _resources.getProperty(PRINT).toString());
      putValue(Action.SHORT_DESCRIPTION, _resources.getProperty(PRINT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      PrintAction();
    }
  }

}
