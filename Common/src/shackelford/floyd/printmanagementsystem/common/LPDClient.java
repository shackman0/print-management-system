package shackelford.floyd.printmanagementsystem.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.BindException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * <p>Title: LPDClient</p>
 * <p>Description:
 * client to communicate with LPD
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class LPDClient
{

  public static final String UTF_8                      = "UTF-8";

  private String             _remoteLPDHost;

  private int                _remoteLPDPort;

  private boolean            _didDone;

  private Socket             _socket;

  private int                _localPortNumber;

  /**
   * LPD commands are sent as follows: The request consists of a single octet
   * indicating the request type, followed by the printer (or print queue) name,
   * followed by a set of options for the request, followed by a LF (line feed)
   * character.
   */

  public static final int    DEFAULT_LPD_SERVER_PORT    = 515;

  public static final int    DEFAULT_LPD_CLIENT_PORT_LO = 512;

  public static final int    DEFAULT_LPD_CLIENT_PORT_HI = 1023;

  public static final int    SOCKET_TIMEOUT             = 10000;      // value is specified in milliseconds

  // LPD commands as documented in RFC1179
  public static final byte   START_PRINT                = (byte) 0x01;

  public static final byte   TRANSFER_JOB               = (byte) 0x02;

  public static final byte   QUEUE_STATUS_SHORT         = (byte) 0x03;

  public static final byte   QUEUE_STATUS_LONG          = (byte) 0x04;

  public static final byte   REMOVE_JOB                 = (byte) 0x05;

  // LPRng extensions to LPD commands
  public static final byte   DO_CONTROL_OPERATION       = (byte) 0x06;

  public static final byte   TRANSFER_BLOCK_FORMAT_JOB  = (byte) 0x07;

  public static final byte   SECURE_COMMAND_XFER        = (byte) 0x08;

  public static final byte   QUEUE_STATUS_VERBOSE       = (byte) 0x09;

  public static final String JOB_STATUS_ACTIVE          = "active";

  public static final String JOB_STATUS_DONE            = "done";

  public static final String JOB_STATUS_HOLD            = "hold";

  public static final String JOB_STATUS_CANCELLED       = "cancelled";

  public static final String JOB_STATUS_ERROR           = "error";

  public static final byte   LF                         = (byte) '\n';

  public static final String PRINT_QUEUE_ALL            = "all";

  public LPDClient(String remoteLPDHost)
  {
    Initialize(remoteLPDHost, DEFAULT_LPD_SERVER_PORT);
  }

  public LPDClient(String remoteLPDHost, int remoteLPDPort)
  {
    Initialize(remoteLPDHost, remoteLPDPort);
  }

  protected void Initialize(String remoteLPDHost, int remoteLPDPort)
  {
    _remoteLPDHost = remoteLPDHost;
    _remoteLPDPort = remoteLPDPort;
    _didDone = true;
    _localPortNumber = DEFAULT_LPD_CLIENT_PORT_LO - 1;
  }

  /**
   * in case we have a "dangling" connection, we need to close it
   */
  @Override
  protected void finalize()
  {
    if (_didDone == false)
    {
      Done();
    }
  }

  /**
   * call done when you are done with this LPD server so we can shut down the
   * socket connection
   */
  public void Done()
  {
    try
    {
      if (_didDone == false)
      {
        _didDone = true;
        _socket.close();
      }
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
  }

  private int GetNextLocalPortNumber()
  {
    _localPortNumber++;
    if (_localPortNumber >= DEFAULT_LPD_CLIENT_PORT_HI)
    {
      _localPortNumber = DEFAULT_LPD_CLIENT_PORT_LO;
    }
    return _localPortNumber;
  }

  /**
   * This method attempts to connect to the remote socket.
   */
  protected Socket Connect()
    throws Exception
  {
    InetAddress localInet = InetAddress.getLocalHost();

    boolean keepTrying = true;
    int numberOfTries = 0;
    BindException saveBindException = null;

    while ((keepTrying == true) & (numberOfTries <= (DEFAULT_LPD_CLIENT_PORT_HI - DEFAULT_LPD_CLIENT_PORT_LO)))
    {
      try
      {
        _socket = new Socket(_remoteLPDHost, _remoteLPDPort, localInet, GetNextLocalPortNumber());
        keepTrying = false;
      }
      catch (BindException ex)
      {
        saveBindException = ex;
        numberOfTries++;
      }
    }
    if (numberOfTries > (DEFAULT_LPD_CLIENT_PORT_HI - DEFAULT_LPD_CLIENT_PORT_LO))
    {
      if (saveBindException == null)
      {
        saveBindException = new BindException("no ports available");
      }
      throw saveBindException;
    }

    _socket.setKeepAlive(true);
    _socket.setSoLinger(true, 1);
    _socket.setSoTimeout(SOCKET_TIMEOUT);
    _socket.setTcpNoDelay(true);

    _didDone = false;

    return _socket;
  }

  protected Socket GetSocket()
  {
    return _socket;
  }

  protected void Write(OutputStream outputStream, String outputLine)
    throws IOException
  {
    outputStream.write(outputLine.getBytes());
  }

  protected void Write(OutputStream outputStream, byte[] outputBytes)
    throws IOException
  {
    outputStream.write(outputBytes);
  }

  protected void Write(OutputStream outputStream, byte outputByte)
    throws IOException
  {
    outputStream.write(outputByte);
  }

  protected String Read(InputStream inputStream)
    throws IOException
  {
    String inputString = "";
    byte[] bytes = new byte[1000];
    int numberOfBytesRead = inputStream.read(bytes);
    while (numberOfBytesRead != -1)
    {
      String inputBytes = new String(bytes, 0, numberOfBytesRead);
      inputString += inputBytes;
      numberOfBytesRead = inputStream.read(bytes);
    }
    return inputString;
  }

  public String GetRawStatus()
    throws Exception
  {
    return GetRawStatus(PRINT_QUEUE_ALL, QUEUE_STATUS_VERBOSE);
  }

  public String GetRawStatus(String printerName)
    throws Exception
  {
    return GetRawStatus(printerName, QUEUE_STATUS_VERBOSE);
  }

  public String GetRawStatus(byte queueCommand)
    throws Exception
  {
    return GetRawStatus(PRINT_QUEUE_ALL, queueCommand);
  }

  @SuppressWarnings("resource")
  public String GetRawStatus(String printerName, byte queueCommand)
    throws Exception
  {
    String inputLine = null;

    if ((queueCommand == QUEUE_STATUS_VERBOSE) || (queueCommand == QUEUE_STATUS_LONG) || (queueCommand == QUEUE_STATUS_SHORT))
    {
      Connect();

      // send a status command
      OutputStream outputStream = _socket.getOutputStream();

      Write(outputStream, queueCommand);
      Write(outputStream, printerName);
      Write(outputStream, LF);

      // read the result
      InputStream inputStream = _socket.getInputStream();

      inputLine = Read(inputStream);

      Done();
    }

    return inputLine;
  }

  public PrinterInfo[] GetPrinterInfo()
    throws Exception
  {
    return GetPrinterInfo(PRINT_QUEUE_ALL);
  }

  public PrinterInfo[] GetPrinterInfo(String printerName)
    throws Exception
  {
    ArrayList<PrinterInfo> statusArrayList = new ArrayList<>(10);

    // get the short status and look for jobs on hold
    String rawStatus = null;
    try
    {
      rawStatus = GetRawStatus(printerName, QUEUE_STATUS_SHORT);
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      return null;
    }

    // the format of the short status is
    // "printer@server [(name value[, name value]) ... [(name value[, name
    // value])]] X jobs " (e.g. "lp@redhat (1 held) (dest lp@kyocera) 0 jobs")
    // where names and values can be:
    // printing disabled
    // spooling disabled
    // dest x@y note that this value has two tokens
    // n held
    // note that this method is HIGHLY DEPENDENT upon this format of the status
    // line being returned
    // by the status request. this is coded to the format returned by LPRng
    // v3.8.9.

    String token;
    StringReader stringReader = new StringReader(rawStatus);
    String inputLine = GetNextInputLine(stringReader);
    while (inputLine != null)
    {
      DebugWindow.DisplayText(inputLine);

      PrinterInfo printerInfo = new PrinterInfo();

      String delimeters = " @(),";
      StringTokenizer tokenizer = new StringTokenizer(inputLine, delimeters);

      token = tokenizer.nextToken();
      printerInfo._printerName = token;

      token = tokenizer.nextToken();
      printerInfo._printServer = token;

      String token1;
      String token2;
      String token3;
      // process the remaining tokens
      while (tokenizer.hasMoreTokens() == true)
      {
        token1 = tokenizer.nextToken();
        token2 = tokenizer.nextToken();
        if (token2.equals("jobs") == true)
        {
          printerInfo._numberOfActiveJobs = new Integer(token1).intValue();
        }
        else
          if (token1.equals("printing") == true)
          {
            printerInfo._printingStatus = token2;
          }
          else
            if (token1.equals("spooling") == true)
            {
              printerInfo._spoolingStatus = token2;
            }
            else
              if (token1.equals("dest") == true)
              {
                printerInfo._destinationQueue = token2;

                token3 = tokenizer.nextToken();
                printerInfo._destinationHost = token3;
              }
              else
                if (token2.equals("held") == true)
                {
                  printerInfo._numberOfHeldJobs = new Integer(token1).intValue();
                }
      }
      statusArrayList.add(printerInfo);

      inputLine = GetNextInputLine(stringReader);
    }
    statusArrayList.trimToSize();

    PrinterInfo[] printerStatus = new PrinterInfo[statusArrayList.size()];
    statusArrayList.toArray(printerStatus);

    return printerStatus;
  }

  public JobInfo[] GetPrinterJobsInfo(PrinterInfo printerInfo)
    throws Exception
  {

    ArrayList<JobInfo> jobInfoArrayList = new ArrayList<>(100);

    String rawStatus = GetRawStatus(printerInfo._printerName, QUEUE_STATUS_VERBOSE);

    DebugWindow.DisplayText("\n-------- begin raw status --------");
    DebugWindow.DisplayText(rawStatus);
    DebugWindow.DisplayText("-------- end raw status --------\n");

    // look for "Job: <userid> CONTROL=" and then get the A record that follows
    // the A record has the format "A<userid>@<printServer>+<jobNumber>

    // note that this method is HIGHLY DEPENDENT upon this format of the status
    // line being returned
    // by the status request. this is coded to the format returned by LPRng
    // v3.8.9.

    // (perl would be all over this right now if i could use it. i wish String
    // supported
    // regular expressions.)

    String token;
    StringReader stringReader = new StringReader(rawStatus);
    String inputLine = GetNextInputLine(stringReader);

    OUTER_WHILE: while (inputLine != null)
    {
      if ((inputLine.startsWith(" Job:") == true) && (inputLine.indexOf("status=") != -1))
      {
        DebugWindow.DisplayText("job status line = \"" + inputLine + "\"");

        JobInfo jobInfo = new JobInfo();

        jobInfo._printerName = printerInfo._printerName;
        jobInfo._printServer = printerInfo._printServer;

        String delimeters = ": @=+";
        StringTokenizer tokenizer = new StringTokenizer(inputLine, delimeters);

        // skip past the "Job:" token
        token = tokenizer.nextToken();

        // skip past the submitter name
        token = tokenizer.nextToken();

        // skip past the host name
        token = tokenizer.nextToken();

        // skip past the host job number
        token = tokenizer.nextToken();

        // skip past the "status=" token
        token = tokenizer.nextToken();

        // get the status token
        token = tokenizer.nextToken();
        if (token.equalsIgnoreCase(JOB_STATUS_ERROR) == true)
        {
          token = JOB_STATUS_CANCELLED;
        }
        jobInfo._status = token;

        // skip down just past the "CONTROL=" line
        do
        {
          inputLine = GetNextInputLine(stringReader);
          if (inputLine == null)
          {
            DebugWindow.DisplayText("Missing CONTROL= line");
            break OUTER_WHILE;
          }
        }
        while (inputLine.indexOf("CONTROL=") == -1);

        // get the A control line and parse it
        do
        {
          inputLine = GetNextInputLine(stringReader);
          if (inputLine == null)
          {
            DebugWindow.DisplayText("Missing \"A\" control line");
            break OUTER_WHILE;
          }
        }
        while (inputLine.startsWith("A") == false);
        DebugWindow.DisplayText("\"A\" control line = \"" + inputLine + "\"");

        delimeters = "@+";
        // tokenize the string with the the leading "A" stripped off
        tokenizer = new StringTokenizer(inputLine.substring(1), delimeters);

        // skip past the user id token
        token = tokenizer.nextToken();

        // skip past the host token
        token = tokenizer.nextToken();

        // get the host job number token
        token = tokenizer.nextToken();
        jobInfo._hostJobNumber = new Integer(token);

        // get the Z control line and parse it
        do
        {
          inputLine = GetNextInputLine(stringReader);
          if (inputLine == null)
          {
            DebugWindow.DisplayText("Missing \"Z\" control line");
            break OUTER_WHILE;
          }
        }
        while (inputLine.startsWith("Z") == false);
        DebugWindow.DisplayText("\"Z\" control line = \"" + inputLine + "\"");

        delimeters = "= ";
        // tokenize the string with the the leading "Z" stripped off
        tokenizer = new StringTokenizer(inputLine.substring(1), delimeters);

        // skip past the USER token
        token = tokenizer.nextToken();

        // get the submitter's user id
        token = tokenizer.nextToken();
        jobInfo._submittedByUserID = token;

        // skip past the HOST ip address token
        token = tokenizer.nextToken();

        // get the submitter's host ip addr
        token = tokenizer.nextToken();
        jobInfo._submittedByIP = token;

        // skip past the HOSTNAME token
        token = tokenizer.nextToken();

        // get the submitter's host name token
        token = tokenizer.nextToken();
        jobInfo._submittedByHost = token;

        // skip past the DESC token
        token = tokenizer.nextToken();

        // get the description
        token = tokenizer.nextToken();
        jobInfo._description = URLDecoder.decode(token, UTF_8);

        // skip past the ACCT token
        token = tokenizer.nextToken();

        // get the user's account id
        token = tokenizer.nextToken();
        jobInfo._userAccountID = URLDecoder.decode(token, UTF_8);

        // skip past the JOBNUM token
        token = tokenizer.nextToken();

        // get the paytoprint job number
        token = tokenizer.nextToken();
        try
        {
          jobInfo._jobNumber = new Integer(token);
        }
        catch (NumberFormatException excp)
        {
          jobInfo._jobNumber = new Integer(-1);
        }

        jobInfoArrayList.add(jobInfo);
        DebugWindow.DisplayText(jobInfo.toString());
      }
      inputLine = GetNextInputLine(stringReader);
    }

    jobInfoArrayList.trimToSize();

    JobInfo[] jobInfoArray = new JobInfo[jobInfoArrayList.size()];
    jobInfoArrayList.toArray(jobInfoArray);

    return jobInfoArray;
  }

  String GetNextInputLine(StringReader stringReader)
    throws IOException
  {
    byte[] byteArray = new byte[1000]; // we have an "index out of bounds
                                       // error" if any line is > 1000
                                       // characters long!!!
    String inputLine = null;
    byte indx = (byte) (stringReader.read());
    if (indx != -1)
    {
      int jndx = 0;
      do
      {
        byteArray[jndx++] = indx;
        indx = (byte) (stringReader.read());
      }
      while ((indx != -1) && (indx != '\n'));
      inputLine = new String(byteArray, 0, jndx);
    }
    return inputLine;
  }

  @SuppressWarnings("resource")
  public String ReleaseJob(JobInfo jobInfo)
    throws Exception
  {
    Connect();

    OutputStream outputStream = _socket.getOutputStream();

    // the control operation has the following format:
    // \006printer user command [options]
    Write(outputStream, DO_CONTROL_OPERATION);
    Write(outputStream, jobInfo._printerName + " ");
    Write(outputStream, jobInfo._submittedByUserID + "@" + jobInfo._submittedByHost + " ");

    // the release command has the following format:
    // release (printer[@host] | all) (name[@host] | job | all)*
    String commandLine = "release " + jobInfo._printerName + " " + String.valueOf(jobInfo._hostJobNumber);
    Write(outputStream, commandLine);

    Write(outputStream, LF);

    // read the result
    InputStream inputStream = _socket.getInputStream();
    String inputLine = Read(inputStream);
    DebugWindow.DisplayText("ReleaseJob result = " + inputLine);

    Done();

    return inputLine;
  }

  @SuppressWarnings("resource")
  public String RemoveJob(JobInfo jobInfo)
    throws Exception
  {
    Connect();

    OutputStream outputStream = _socket.getOutputStream();

    // the control operation has the following format:
    // \005printer user [id]* \n
    Write(outputStream, REMOVE_JOB);
    Write(outputStream, jobInfo._printerName + " ");
    Write(outputStream, jobInfo._submittedByUserID + "@" + jobInfo._submittedByHost + " ");
    Write(outputStream, String.valueOf(jobInfo._hostJobNumber));
    Write(outputStream, LF);

    // read the result
    InputStream inputStream = _socket.getInputStream();
    String inputLine = Read(inputStream);
    DebugWindow.DisplayText("RemoveJob result = " + inputLine);

    Done();

    return inputLine;
  }

  /**
   * This method returns information on all the jobs that have the specified
   * status. it looks for all the print queues and queries each queue for the
   * job specifics.
   */
  public JobInfo[] GetJobsWithStatus(String printerName, String status)
    throws Exception
  {

    PrinterInfo[] printerInfoArray = GetPrinterInfo(printerName);

    ArrayList<JobInfo> jobsArrayList = new ArrayList<>(100);

    for (int indx = 0; indx < printerInfoArray.length; indx++)
    {
      JobInfo[] jobInfoArray = GetPrinterJobsInfo(printerInfoArray[indx]);
      for (int jndx = 0; jndx < jobInfoArray.length; jndx++)
      {
        JobInfo jobInfo = jobInfoArray[jndx];
        /*
         * // the active status has the job number instead of a constant string
         * if (status.equals(JOB_STATUS_ACTIVE) == true) { try { Integer
         * jobNumber = new Integer(jobInfo._status); jobsArrayList.add(jobInfo); }
         * catch (NumberFormatException excp) { // do nothing, the status is not
         * a number, so it's not an active job } } else
         */
        if (jobInfo._status.equalsIgnoreCase(status) == true)
        {
          jobsArrayList.add(jobInfo);
        }
      }
    }

    jobsArrayList.trimToSize();
    JobInfo[] jobsWithStatus = new JobInfo[jobsArrayList.size()];
    jobsArrayList.toArray(jobsWithStatus);

    return jobsWithStatus;
  }

  public class PrinterInfo
  {
    public String _printerName;

    public String _printServer;

    public int    _numberOfActiveJobs = 0;

    public int    _numberOfHeldJobs   = 0;

    public String _destinationQueue   = "unknown";

    public String _destinationHost    = "unknown";

    public String _printingStatus     = "enabled";

    public String _spoolingStatus     = "enabled";

    @Override
    public String toString()
    {
      return "[PrinterInfo: " + "_printServer=" + _printServer + "," + "_printerName=" + _printerName + "," + "_printingStatus=" + _printingStatus + "," + "_spoolingStatus=" + _spoolingStatus + ","
        + "_numberOfActiveJobs=" + _numberOfActiveJobs + "," + "_numberOfHeldJobs=" + _numberOfHeldJobs + "," + "_destinationQueue=" + _destinationQueue + "," + "_destinationHost=" + _destinationHost
        + "]";
    }
  }

  public class JobInfo
  {
    public String  _printServer;

    public String  _printerName;

    public String  _submittedByUserID;

    public String  _submittedByHost;

    public String  _submittedByIP;

    public Integer _jobNumber;

    public Integer _hostJobNumber;

    public String  _status;

    public String  _userAccountID;

    public String  _description;

    @Override
    public String toString()
    {
      return "[JobInfo: " + "_printServer=" + _printServer + "," + "_printerName=" + _printerName + "," + "_submittedByUserID=" + _submittedByUserID + "," + "_submittedByHost=" + _submittedByHost
        + "," + "_submittedByIP=" + _submittedByIP + "," + "_jobNumber=" + _jobNumber + "," + "_hostJobNumber=" + _hostJobNumber + "," + "_userAccountID=" + _userAccountID + "," + "_description="
        + _description + "," + "_status=" + _status + "]";
    }
  }

  /**
   * The main runs a test against a well known gatekeeper host
   */
  public static void main(String[] args)
  {

    GlobalAttributes.__verbose = true;

    try
    {
      PrinterInfo[] printerInfo;
      JobInfo[] jobInfo;

      // LPDClient lpdClient = new LPDClient("192.168.1.51");
      LPDClient lpdClient = new LPDClient("192.168.1.200");

      // System.out.println(lpdClient.GetRawStatus(QUEUE_STATUS_SHORT));
      // System.out.println(lpdClient.GetRawStatus(QUEUE_STATUS_LONG));
      // System.out.println(lpdClient.GetRawStatus(QUEUE_STATUS_VERBOSE));

      System.out.println("\n--- Printers ---");
      printerInfo = lpdClient.GetPrinterInfo();
      System.out.println(+printerInfo.length + " printers:");
      for (int indx = 0; indx < printerInfo.length; indx++)
      {
        System.out.println(printerInfo[indx]);
        jobInfo = lpdClient.GetPrinterJobsInfo(printerInfo[indx]);
        System.out.println("\t" + jobInfo.length + " jobs:");
        for (int jndx = 0; jndx < jobInfo.length; jndx++)
        {
          System.out.println("\t" + jobInfo[jndx]);
        }
      }

      System.out.println("\n--- Active Jobs ---");
      jobInfo = lpdClient.GetJobsWithStatus(PRINT_QUEUE_ALL, JOB_STATUS_ACTIVE);
      System.out.println(+jobInfo.length + " active jobs");
      for (int indx = 0; indx < jobInfo.length; indx++)
      {
        System.out.println("\t" + jobInfo[indx]);
      }

      System.out.println("\n--- Jobs on Hold ---");
      jobInfo = lpdClient.GetJobsWithStatus(PRINT_QUEUE_ALL, JOB_STATUS_HOLD);
      System.out.println(+jobInfo.length + " jobs on hold");
      for (int indx = 0; indx < jobInfo.length; indx++)
      {
        System.out.println("\t" + jobInfo[indx]);
      }

      System.out.println("\n--- Completed Jobs ---");
      jobInfo = lpdClient.GetJobsWithStatus(PRINT_QUEUE_ALL, JOB_STATUS_DONE);
      System.out.println(+jobInfo.length + " completed jobs");
      for (int indx = 0; indx < jobInfo.length; indx++)
      {
        System.out.println("\t" + jobInfo[indx]);
      }

      System.out.println("\n--- Cancelled Jobs ---");
      jobInfo = lpdClient.GetJobsWithStatus(PRINT_QUEUE_ALL, JOB_STATUS_CANCELLED);
      System.out.println(+jobInfo.length + " cancelled jobs");
      for (int indx = 0; indx < jobInfo.length; indx++)
      {
        System.out.println("\t" + jobInfo[indx]);
      }

    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }

  }

}
