package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: _DateChooserEvent</p>
 * <p>Description: this class is used to transport information to recipients of panel events</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class _DateChooserEvent
  extends _EventNotificationManagerEvent
{

  private static final long serialVersionUID = -3755521189299417460L;

  public _DateChooserEvent(_DateChooserPopup eventSource)
  {
    super(eventSource);
  }

  public _DateChooserEvent(_DateChooserPopup eventSource, boolean success)
  {
    super(eventSource, success);
  }

  public _DateChooserPopup GetSource()
  {
    return (_DateChooserPopup) source;
  }

}
