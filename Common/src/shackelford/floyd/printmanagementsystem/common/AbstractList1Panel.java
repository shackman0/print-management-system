package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.print.PageFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;


/**
 * <p>Title: AbstractList1Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractList1Panel
  extends JFrame
  implements ListPanelListener, ListPanelCursor, PanelNotifier
{

  /**
   * Eclipse generated value
   */
  private static final long                      serialVersionUID            = 303860091419974172L;

  protected ArrayList<PanelListener>                            _panelListeners             = null;

  protected ListFilter                           _listFilter                 = null;

  protected JLabel                               _numberOfInstances          = null;

  protected Container                            _northPanel                 = null;

  protected JScrollPane                          _centerPanel                = null;

  protected Container                            _southPanel                 = null;

  protected Container                            _eastPanel                  = null;

  protected Container                            _westPanel                  = null;

  protected MyJTable                             _table                      = null;

  protected SortFilterTableModel                 _sorter                     = null;

  protected ResultSetTableModel                  _resultSetTable             = null;

  protected SQLStatement                         _sqlStatement               = null;

  protected TablePrinter                         _tablePrinter               = null;

  protected PageFormat                           _pageFormat                 = null;

  protected Class<? extends AbstractEntry1Panel> _entryPanelClass            = null;

  protected AbstractSQL1Table                    _sqlTable                   = null;

  protected JMenuItem                            _fileNewMenuItem            = null;

  protected JMenuItem                            _fileOpenMenuItem           = null;

  protected JMenuItem                            _fileDeleteMenuItem         = null;

  protected JMenuItem                            _fileNewFloatingMenuItem    = null;

  protected JMenuItem                            _fileOpenFloatingMenuItem   = null;

  protected JMenuItem                            _fileDeleteFloatingMenuItem = null;

  protected OpenOnDoubleClick                    _openOnDoubleClickAdapter   = null;

  protected ManageMenuItems                      _manageMenuItemsAdapter     = null;

  protected static final String                  TRUE                        = "TRUE";

  protected static final String                  FALSE                       = "FALSE";

  protected static final String                  TITLE                       = "TITLE";

  protected static final String                  FILE                        = "FILE";

  protected static final String                  FILE_NEW                    = "FILE_NEW";

  protected static final String                  FILE_NEW_TIP                = "FILE_NEW_TIP";

  protected static final String                  FILE_OPEN                   = "FILE_OPEN";

  protected static final String                  FILE_OPEN_TIP               = "FILE_OPEN_TIP";

  protected static final String                  FILE_DELETE                 = "FILE_DELETE";

  protected static final String                  FILE_DELETE_TIP             = "FILE_DELETE_TIP";

  protected static final String                  FILE_PRINT_SETUP            = "FILE_PRINT_SETUP";

  protected static final String                  FILE_PRINT_SETUP_TIP        = "FILE_PRINT_SETUP_TIP";

  protected static final String                  FILE_PRINT_LIST_PREVIEW     = "FILE_PRINT_LIST_PREVIEW";

  protected static final String                  FILE_PRINT_LIST_PREVIEW_TIP = "FILE_PRINT_LIST_PREVIEW_TIP";

  protected static final String                  FILE_PRINT_LIST             = "FILE_PRINT_LIST";

  protected static final String                  FILE_PRINT_LIST_TIP         = "FILE_PRINT_LIST_TIP";

  protected static final String                  FILE_LIST_FILTER            = "FILE_LIST_FILTER";

  protected static final String                  FILE_LIST_FILTER_TIP        = "FILE_LIST_FILTER_TIP";

  protected static final String                  FILE_REFRESH                = "FILE_REFRESH";

  protected static final String                  FILE_REFRESH_TIP            = "FILE_REFRESH_TIP";

  protected static final String                  FILE_EXIT                   = "FILE_EXIT";

  protected static final String                  FILE_EXIT_TIP               = "FILE_EXIT_TIP";

  protected static final String                  DELETE_CONFIRMATION_MSG     = "DELETE_CONFIRMATION_MSG";

  protected static final String                  DELETE_CONFIRMATION_TITLE   = "DELETE_CONFIRMATION_TITLE";

  protected static final String                  NUMBER_OF                   = "NUMBER_OF";

  protected static final int                     INTER_FIELD_GAP_WIDTH       = 10;

  protected static final int                     INTRA_FIELD_GAP_WIDTH       = 5;

  protected static final int                     HORIZONTAL_STRUT            = 10;

  public AbstractList1Panel()
  {
    super();
    _openOnDoubleClickAdapter = new OpenOnDoubleClick(this);
    _manageMenuItemsAdapter = new ManageMenuItems();
  }

  @Override
  protected void finalize()
    throws Throwable
  {
    if (_sqlStatement != null)
    {
      _sqlStatement.MakeAvailable();
      _sqlStatement = null;
    }
    super.finalize();
  }

  @Override
  public void dispose()
  {
    if (_sqlStatement != null)
    {
      _sqlStatement.MakeAvailable();
      _sqlStatement = null;
    }
    super.dispose();
  }

  @Override
  public void setVisible(boolean visible)
  {
    super.setVisible(visible);
    if (visible == true)
    {
      toFront();
    }
  }

  public void Initialize(AbstractSQL1Table sqlTable, Class<? extends AbstractEntry1Panel> entryPanelClass)
  {
    _sqlTable = sqlTable;
    _entryPanelClass = entryPanelClass;
    _pageFormat = AbstractPrinter.GetDefaultPageFormat();
    _listFilter = new ListFilter(this);

    SetPermissions();

    addWindowListener(new WindowListener());
    setIconImage(SystemUtilities.GetLogoIconImage());
    setTitle(GetResources().getProperty(TITLE));
    setResizable(true);

    Container contentPane = getContentPane();

    // build menu bar

    JMenu fileMenu = CreateFileMenu();
    JMenuBar menuBar = new JMenuBar();
    if (fileMenu != null)
    {
      menuBar.add(fileMenu);
    }
    AddMenus(menuBar);
    menuBar.add(new HelpMenu(this));
    setJMenuBar(menuBar);

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    _centerPanel.addMouseListener(new FloatingFileMenu(_centerPanel));
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    BuildTable();

    pack();
    _table.SetColumnHeaderWidths(_sqlTable);
    SizeInitialPanel();
    PositionInitialPanel();
    validate();
  }

  protected void BuildTable()
  {
    _table = new MyJTable();
    _sorter = new SortFilterTableModel(_table);

    _table.addMouseListener(_manageMenuItemsAdapter);
    _table.addMouseListener(new FloatingEditMenu(_table));
    _table.addMouseListener(_openOnDoubleClickAdapter);
    _sorter.addTableModelListener(_manageMenuItemsAdapter);
    _table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
    _table.setColumnSelectionAllowed(false);
    _table.setRowSelectionAllowed(true);
    _table.setShowVerticalLines(true);
    _table.setShowHorizontalLines(true);

    RefreshList();

    _table.setModel(_sorter);
    _table.SetColumnCellRenderers();
    _table.doLayout();

    _centerPanel.setViewportView(_table);
  }

  @Override
  public void RefreshList()
  {
    CreateResultSetTable();
    _sorter.SetTableModel(_resultSetTable);
    _table.SetColumnCellRenderers();
    _table.SetColumnHeaderWidths(_sqlTable);
    _numberOfInstances.setText(String.valueOf(GetAbstractSQL1Table().RowCount(GetPredicate(), _listFilter)));
    _numberOfInstances.invalidate();
    repaint();
    NotifyListenersPanelStateChange(PanelNotifier.ACTION_REFRESH);
  }

  protected void CreateResultSetTable()
  {
    if (_resultSetTable == null)
    {
      _resultSetTable = new ResultSetTableModel(_table, GetAbstractSQL1Table(), GetNewSQLStatement(), _listFilter);
    }
    else
    {
      _resultSetTable.SetSQLStatement(GetAbstractSQL1Table(), GetNewSQLStatement());
    }
  }

  protected SQLStatement GetNewSQLStatement()
  {
    if (_sqlStatement != null)
    {
      _sqlStatement.MakeAvailable();
      _sqlStatement = null;
    }
    if (_listFilter.GetShowForeignKeys() == true)
    {
      _sqlStatement = _sqlTable.GetRowsAsResultSet(GetPredicate(), GetOrderBy(), _listFilter);
    }
    else
    {
      _sqlStatement = _sqlTable.GetRowsWithSubstitutionsAsResultSet(GetPredicate(), GetOrderBy(), _listFilter);
    }
    return _sqlStatement;
  }

  protected String GetPredicate()
  {
    return "";
  }

  protected String GetOrderBy()
  {
    return "1 asc";
  }

  protected void SetPermissions()
  {
    // do nothing
  }

  protected Container CreateNorthPanel()
  {
    Box northPanel = Box.createHorizontalBox();

    northPanel.add(Box.createHorizontalStrut(HORIZONTAL_STRUT));
    northPanel.add(new JLabel(GetResources().getProperty(NUMBER_OF), SwingConstants.RIGHT));
    northPanel.add(Box.createHorizontalStrut(HORIZONTAL_STRUT));
    _numberOfInstances = new JLabel("", SwingConstants.LEFT);
    northPanel.add(_numberOfInstances);

    northPanel.add(Box.createHorizontalGlue());
    AddCenterNorthPanel(northPanel);
    northPanel.add(Box.createHorizontalGlue());

    return northPanel;
  }

  protected void AddCenterNorthPanel(Box northPanel)
  {
    // do nothing
  }

  protected JScrollPane CreateCenterPanel()
  {
    JScrollPane scrollPane = new JScrollPane();
    scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
    scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);

    return scrollPane;
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected void SizeInitialPanel()
  {
    Dimension panelSize = getSize();
    panelSize.width *= 1.50f;
    panelSize.height *= 1.00f;
    setSize(panelSize);
  }

  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  protected JMenu CreateFileMenu()
  {
    JMenu fileMenu = new JMenu(GetResources().getProperty(FILE));

    AddFileMenuItems(fileMenu);

    fileMenu.add(new ActionMenuItem(new FilePrintSetupAction()));
    fileMenu.add(new ActionMenuItem(new FilePrintListPreviewAction()));
    fileMenu.add(new ActionMenuItem(new FilePrintListAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileListFilterAction()));
    fileMenu.add(new ActionMenuItem(new FileRefreshAction()));
    fileMenu.addSeparator();

    fileMenu.add(new ActionMenuItem(new FileExitAction()));

    return fileMenu;
  }

  protected void AddFileMenuItems(JMenu fileMenu)
  {
    _fileNewMenuItem = fileMenu.add(new ActionMenuItem(new FileNewAction()));
    _fileNewMenuItem.setEnabled(FileNewOK());

    _fileOpenMenuItem = fileMenu.add(new ActionMenuItem(new FileOpenAction()));
    _fileOpenMenuItem.setEnabled(false);

    _fileDeleteMenuItem = fileMenu.add(new ActionMenuItem(new FileDeleteAction()));
    _fileDeleteMenuItem.setEnabled(false);

    fileMenu.addSeparator();
  }

  protected void AddEditMenuItems(JMenu editMenu)
  {
    // do nothing
  }

  protected void AddMenus(JMenuBar menuBar)
  {
    // do nothing
  }

  protected void CallEnableMenuItems()
  {
    boolean enabledFlag = (_table.getSelectedRowCount() > 0);
    EnableMenuItems(enabledFlag);
  }

  protected void EnableMenuItems(boolean enabledFlag)
  {
    _fileOpenMenuItem.setEnabled(enabledFlag);
    _fileDeleteMenuItem.setEnabled(enabledFlag);
    _fileOpenFloatingMenuItem.setEnabled(enabledFlag);
    _fileDeleteFloatingMenuItem.setEnabled(enabledFlag);
  }

  protected JPopupMenu CreateFloatingFileMenu(Component invoker)
  {
    JPopupMenu popupMenu = new JPopupMenu();
    popupMenu.setInvoker(invoker);

    _fileNewFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileNewAction()));
    _fileNewFloatingMenuItem.setEnabled(FileNewOK());
    popupMenu.addSeparator();

    AddFloatingFileMenuItems(popupMenu);

    popupMenu.add(new ActionMenuItem(new FilePrintSetupAction()));
    popupMenu.add(new ActionMenuItem(new FilePrintListPreviewAction()));
    popupMenu.add(new ActionMenuItem(new FilePrintListAction()));
    popupMenu.addSeparator();

    popupMenu.add(new ActionMenuItem(new FileListFilterAction()));
    popupMenu.add(new ActionMenuItem(new FileRefreshAction()));

    return popupMenu;
  }

  protected void AddFloatingFileMenuItems(JPopupMenu popupMenu)
  {
    // do nothing
  }

  protected JPopupMenu CreateFloatingEditMenu(Component invoker)
  {
    JPopupMenu popupMenu = new JPopupMenu();
    popupMenu.setInvoker(invoker);

    _fileOpenFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileOpenAction()));
    popupMenu.addSeparator();

    AddFloatingEditMenuItems(popupMenu);

    _fileDeleteFloatingMenuItem = popupMenu.add(new ActionMenuItem(new FileDeleteAction()));

    return popupMenu;
  }

  protected void AddFloatingEditMenuItems(JPopupMenu popupMenu)
  {
    // do nothing
  }

  /**
   * you must set the invoker of the popupMenu before calling this method.
   */
  protected void PositionFloatingMenu(JPopupMenu popupMenu, MouseEvent event)
  {
    Component invoker = popupMenu.getInvoker();
    Point tablePoint;
    // if the invoker is not set, we'll get null back from getInvoker()
    if (invoker == null)
    {
      tablePoint = new Point(0, 0);
    }
    else
    {
      tablePoint = invoker.getLocationOnScreen();
    }
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Dimension popupSize = popupMenu.getPreferredSize();

    // display the popup menu to the right of the mouse event point if there's enough room
    // otherwise, display it to the left of the mouse event point
    int x = event.getX() + tablePoint.x;
    if ((x + popupSize.getWidth()) > screenSize.getWidth())
    {
      x -= popupSize.getWidth();
    }

    // display the popup menu below the mouse event point if there's enough room
    // otherwise, display it above the mouse event point
    int y = event.getY() + tablePoint.y;
    if ((y + popupSize.getHeight()) > screenSize.getHeight())
    {
      y -= popupSize.getHeight();
    }
    popupMenu.setLocation(x, y);
  }

  public AbstractSQL1Table GetAbstractSQL1Table()
  {
    return _sqlTable;
  }

  /*
    public AbstractSQL1Row GetAbstractSQL1Row()
    {
      return _sqlRow;
    }
  */
  protected abstract Resources GetResources();

  /**
   * This method is called by the FileNewAction.actionPerformed() method to
   * validate that it's ok to create a new object.
   */
  protected boolean FileNewOK()
  {
    return true;
  }

  protected AbstractEntry1Panel CreateEntryPanel(AbstractSQL1Row sqlRow)
  {
    AbstractEntry1Panel entryPanel;
    try
    {
      entryPanel = (_entryPanelClass.newInstance());
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowMessageDialog(null, excp, "entryPanel = (AbstractEntryPanel) _entryPanelClass.newInstance()", JOptionPane.ERROR_MESSAGE);
      return null;
    }
    entryPanel.Initialize(this, GetAbstractSQL1Table(), sqlRow);
    return entryPanel;
  }

  public AbstractSQL1Row[] GetSelectedSQLRows()
  {
    return _resultSetTable.GetSelectedSQLRows();
  }

  public int FindRow(AbstractSQL1Row sqlRow)
  {
    int rowCount = _table.getRowCount();
    int rowNum;
    for (rowNum = 0; rowNum < rowCount; rowNum++)
    {
      AbstractSQL1Row tableSQLRow = _resultSetTable.GetSQLRowAt(rowNum);
      if (sqlRow.Equals(tableSQLRow) == true)
      {
        break;
      }
    }
    return rowNum;
  }

  @Override
  public AbstractSQL1Row GetPreviousRow(AbstractSQL1Row sqlRow)
  {
    AbstractSQL1Row previousRow = sqlRow;
    int row = FindRow(sqlRow);
    int rowCount = _table.getRowCount();
    if (row < rowCount)
    {
      if (row == 0) // wrap around to the end of the table
      {
        row = rowCount;
      }
      previousRow = _resultSetTable.GetSQLRowAt(row - 1);
    }
    return previousRow;
  }

  @Override
  public AbstractSQL1Row GetNextRow(AbstractSQL1Row sqlRow)
  {
    AbstractSQL1Row nextRow = sqlRow;
    int rowNum = FindRow(sqlRow); // 0 relative
    int rowCount = _table.getRowCount(); // 1 relative
    if (rowNum >= (rowCount - 1)) // wrap around to the beginning of the table
    {
      rowNum = -1;
    }
    nextRow = _resultSetTable.GetSQLRowAt(rowNum + 1);
    return nextRow;
  }

  @Override
  public void AddPanelListener(PanelListener listener)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_panelListeners.indexOf(listener) == -1)
      {
        _panelListeners.add(listener);
      }
    }
  }

  @Override
  public void AddPanelListeners(ArrayList<PanelListener> listeners)
  {
    if (_panelListeners == null)
    {
      _panelListeners = new ArrayList<>();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddPanelListener((listeners.get(i)));
      }
    }
  }

  @Override
  public void RemovePanelListener(PanelListener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _panelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _panelListeners.remove(listenerIndex);
      }
    }
  }

  @Override
  public void NotifyListenersEndOfPanel(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.EndOfPanel(action);
      }
      _panelListeners = null;
    }
  }

  @Override
  public void NotifyListenersPanelStateChange(char action)
  {
    if (_panelListeners != null)
    {
      for (Iterator<PanelListener> iterator = _panelListeners.iterator(); iterator.hasNext();)
      {
        PanelListener listener = iterator.next();
        listener.PanelStateChange(action);
      }
    }
  }

  /**
   * This method is called by the FileDeleteAction.actionPerformed() method in case
   * the child class wants to propagate the delete.
   */
  protected void DeletingRow(AbstractSQL1Row sqlRow)
  {
    // do nothing
  }

  protected class WindowListener
    extends WindowAdapter
  {

    @Override
    public void windowClosing(WindowEvent event)
    {
      FileExitAction();
    }
  }

  protected class FloatingFileMenu
    extends MouseAdapter
  {
    private final JPopupMenu _popupMenu;

    public FloatingFileMenu(Component invoker)
    {
      _popupMenu = CreateFloatingFileMenu(invoker);
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      _popupMenu.setVisible(false);
      repaint();
      // we are looking for "button3", that is, the right-most mouse button
      if ((event.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK)
      {
        PositionFloatingMenu(_popupMenu, event);
        _popupMenu.setVisible(true);
      }
    }

  }

  protected void FileNewAction()
  {
    AbstractEntry1Panel entryPanel = CreateEntryPanel(null);
    entryPanel.AddListPanelListener(this);
    entryPanel.setVisible(true);
    repaint();
  }

  protected class FileNewAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = -5970561674554596374L;

    public FileNewAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_NEW));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_NEW_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileNewAction();
    }

  }

  protected void FileOpenAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    for (int i = 0; i < sqlRows.length; ++i)
    {
      AbstractEntry1Panel entryPanel = CreateEntryPanel(sqlRows[i]);
      entryPanel.AddListPanelListener(this);
      entryPanel.setVisible(true);
    }
    repaint();
  }

  protected class FileOpenAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 6847067861392980601L;

    public FileOpenAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_OPEN));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_OPEN_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileOpenAction();
    }

  }

  protected void FileDeleteAction()
  {
    AbstractSQL1Row[] sqlRows = GetSelectedSQLRows();
    if (sqlRows.length > 0)
    {
      int rc = JOptionPane.showConfirmDialog(this, GetResources().getProperty(DELETE_CONFIRMATION_MSG), GetResources().getProperty(DELETE_CONFIRMATION_TITLE), JOptionPane.YES_NO_OPTION);
      if (rc == JOptionPane.YES_OPTION)
      {
        for (int i = 0; i < sqlRows.length; ++i)
        {
          AbstractSQL1Row sqlRow = sqlRows[i];
          sqlRow.Remove();
          DeletingRow(sqlRow);
        }
        RefreshList();
      }
    }
  }

  protected class FileDeleteAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 2998599133526674901L;

    public FileDeleteAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_DELETE));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_DELETE_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileDeleteAction();
    }

  }

  protected class ManageMenuItems
    extends MouseAdapter
    implements TableModelListener
  {

    @Override
    public void mouseClicked(MouseEvent event)
    {
      CallEnableMenuItems();
    }

    @Override
    public void tableChanged(TableModelEvent e)
    {
      CallEnableMenuItems();
    }
  }

  protected class FloatingEditMenu
    extends MouseAdapter
  {
    private final JPopupMenu _popupMenu;

    public FloatingEditMenu(Component invoker)
    {
      _popupMenu = CreateFloatingEditMenu(invoker);
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      _popupMenu.setVisible(false);
      repaint();
      // we are looking for "button3", that is, the right-most mouse button
      if ((event.getModifiers() & InputEvent.BUTTON3_MASK) == InputEvent.BUTTON3_MASK)
      {
        Point eventPoint = event.getPoint();
        int rowAtPoint = _table.rowAtPoint(eventPoint);
        // we only pop up the edit menu if the mouse is over a row
        if (rowAtPoint >= 0)
        {
          if (_table.isRowSelected(rowAtPoint) == false)
          {
            _table.setRowSelectionInterval(rowAtPoint, rowAtPoint);
          }
          PositionFloatingMenu(_popupMenu, event);
          _popupMenu.setVisible(true);
        }
        CallEnableMenuItems();
      }
    }

  }

  protected static class OpenOnDoubleClick
    extends MouseAdapter
  {
    private final AbstractList1Panel _listPanel;

    public OpenOnDoubleClick(AbstractList1Panel listPanel)
    {
      _listPanel = listPanel;
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      if (((event.getModifiers() & InputEvent.BUTTON1_MASK) == InputEvent.BUTTON1_MASK) && (event.getClickCount() == 2) && (_listPanel._table.getSelectedRowCount() > 0))
      {
        _listPanel.FileOpenAction();
      }
    }
  }

  protected void FileListFilterAction()
  {
    ListFilterDialog dialog = new ListFilterDialog(_listFilter);
    Properties props = dialog.WaitOnDialog();

    if (new Boolean(props.getProperty(AbstractDialog.OK)).booleanValue() == true)
    {
      RefreshList();
    }
  }

  protected class FileListFilterAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -3632027172616662367L;

    public FileListFilterAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_LIST_FILTER));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_LIST_FILTER_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileListFilterAction();
    }
  }

  protected void FileRefreshAction()
  {
    RefreshList();
  }

  protected class FileRefreshAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 7643102662917667808L;

    public FileRefreshAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_REFRESH));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_REFRESH_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileRefreshAction();
    }
  }

  protected void FileExitAction()
  {
    NotifyListenersEndOfPanel(PanelNotifier.ACTION_EXIT);
    dispose();
  }

  protected class FileExitAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 6106221929071821832L;

    public FileExitAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_EXIT));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_EXIT_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FileExitAction();
    }
  }

  protected void FilePrintSetupAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, GetResources().getProperty(TITLE));
    }
    _pageFormat = _tablePrinter.PrintSetup(_pageFormat);
  }

  protected class FilePrintSetupAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -5586176440192159271L;

    public FilePrintSetupAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_PRINT_SETUP));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_PRINT_SETUP_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintSetupAction();
    }
  }

  protected void FilePrintListPreviewAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, GetResources().getProperty(TITLE));
    }
    _tablePrinter.PrintPreview(_pageFormat);
  }

  protected class FilePrintListPreviewAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -5194434594704953575L;

    public FilePrintListPreviewAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_PRINT_LIST_PREVIEW));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_PRINT_LIST_PREVIEW_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintListPreviewAction();
    }
  }

  protected void FilePrintListAction()
  {
    if (_tablePrinter == null)
    {
      _tablePrinter = new TablePrinter(this, _table, GetResources().getProperty(TITLE));
    }
    _tablePrinter.Print(_pageFormat);
  }

  protected class FilePrintListAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -8607845723359965759L;

    public FilePrintListAction()
    {
      putValue(Action.NAME, GetResources().getProperty(FILE_PRINT_LIST));
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(FILE_PRINT_LIST_TIP));
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      FilePrintListAction();
    }
  }

}
