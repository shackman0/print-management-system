package shackelford.floyd.printmanagementsystem.common;

import java.util.Properties;

/**
 * <p>Title: DialogListener</p>
 * <p>Description:
 * use this interface to be notified when a dialog is done</p>
 * <p>here's a typical implementation:
 * <code>
public class ADialog
  implements DialogListener
{
  private Properties              _dialogFinalProperties;
  ...

  public Properties WaitOnDialog ()
  {
    AddDialogListener(this);
    _dialog.show();
    return _dialogFinalProperties;
  }

  public void EndOfDialog ( Properties props )
  {
    _dialogFinalProperties = props;
  }


}
 *</code>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface DialogListener
{

  /**
   * the calling class should call WaitOnDialog after creating the panel manager. WaitOnDialog
   * will return when the panel manager is notified that the panel is done.
  */
  public Properties WaitOnDialog ()
    throws InterruptedException;


  /**
   * this is the callback
   * @param props
   */
  public void EndOfDialog ( Properties props );

}