package shackelford.floyd.printmanagementsystem.common;

import java.awt.AWTEvent;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.MenuComponent;
import java.awt.MenuContainer;

import javax.swing.SwingUtilities;

/**
 * <p>Title: WaitCursorEventQueue</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class WaitCursorEventQueue
  extends EventQueue
{

  final int                     _delay;

  private final WaitCursorTimer _waitTimer;

  public WaitCursorEventQueue(int delay)
  {
    _delay = delay;
    _waitTimer = new WaitCursorTimer();
    _waitTimer.setDaemon(true);
    _waitTimer.start();
  }

  @Override
  protected void dispatchEvent(AWTEvent event)
  {
    _waitTimer.startTimer(event.getSource());
    try
    {
      super.dispatchEvent(event);
    }
    finally
    {
      _waitTimer.stopTimer();
    }
  }

  private class WaitCursorTimer
    extends Thread
  {

    private Object    _source;

    private Component _parent;

    /**
     * 
     */
    public WaitCursorTimer()
    {
      super();
    }

    public synchronized void startTimer(Object source)
    {
      _source = source;
      notify();
    }

    public synchronized void stopTimer()
    {
      if (_parent == null)
      {
        interrupt();
      }
      else
      {
        _parent.setCursor(null);
        _parent = null;
      }
    }

    @Override
    public synchronized void run()
    {
      while (true)
      {
        try
        {
          //wait for notification from startTimer()
          wait();

          //wait for event processing to reach the threshold, or
          //interruption from stopTimer()
          wait(_delay);

          if (_source instanceof Component)
          {
            _parent = SwingUtilities.getRoot((Component) _source);
          }
          else
            if (_source instanceof MenuComponent)
            {
              MenuContainer mParent = ((MenuComponent) _source).getParent();
              if (mParent instanceof Component)
              {
                _parent = SwingUtilities.getRoot((Component) mParent);
              }
            }

          if ((_parent != null) && _parent.isShowing())
          {
            _parent.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
          }
        }
        catch (InterruptedException excp)
        {
          // do nothing
        }
      }
    }

  }

}
