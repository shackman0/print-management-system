package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: AbstractList2Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractList2Panel
  extends AbstractList1Panel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -8018431512641769732L;

  public AbstractList2Panel()
  {
    super();
  }

  public AbstractSQL2Table GetAbstractSQL2Table()
  {
    return (AbstractSQL2Table) _sqlTable;
  }
  /*
    public AbstractSQL2Row GetAbstractSQL2Row()
    {
      return (AbstractSQL2Row)_sqlRow;
    }
  */
}
