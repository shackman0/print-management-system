package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: AbstractList5Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractList5Panel
  extends AbstractList4Panel
{

  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = -1874649958552538967L;

  public AbstractList5Panel()
  {
    super();
  }

  public AbstractSQL5Table GetAbstractSQL5Table()
  {
    return (AbstractSQL5Table) _sqlTable;
  }
  /*
    public AbstractSQL5Row GetAbstractSQL5Row()
    {
      return (AbstractSQL5Row)_sqlRow;
    }
  */
}
