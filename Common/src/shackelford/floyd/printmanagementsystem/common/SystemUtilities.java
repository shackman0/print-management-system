package shackelford.floyd.printmanagementsystem.common;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;
import java.io.IOException;
import java.util.Calendar;

import javax.swing.Icon;
import javax.swing.ImageIcon;


/**
 * <p>Title: SystemUtilities</p>
 * <p>Description:
  this class encapsulates a set of System convenience operations
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SystemUtilities
{

  public static void LaunchApplication (String applicationName)
  {
    try
    {
      File file = new File(GlobalAttributes.INSTALL_DIR + applicationName + "/");
      String commandLine = "\"" + GlobalAttributes.INSTALL_DIR + applicationName + "/" + applicationName + ".bat\"";
      DebugWindow.DisplayText("commandLine="+commandLine);
      Runtime.getRuntime().exec(commandLine,null,file);
    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
  }

  public static String GetTimestamp ()
  {
    Calendar calendar = Calendar.getInstance();
    return
      String.valueOf(calendar.get(Calendar.YEAR)) + "-" +
      String.valueOf(calendar.get(Calendar.MONTH)+1) + "-" +
      String.valueOf(calendar.get(Calendar.DAY_OF_MONTH)) + " " +
      String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)) + ":" +
      String.valueOf(calendar.get(Calendar.MINUTE)) + ":" +
      String.valueOf(calendar.get(Calendar.SECOND)) + "." +
      String.valueOf(calendar.get(Calendar.MILLISECOND)/100);
  }

  public static Image GetLogoIconImage ()
  {
    return GetImage("logo.gif");
  }

  public static Image GetImage (String imageName)
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();

    String imagePath = GlobalAttributes.__resourcesDirectory + File.separator + "Images" + File.separator + imageName;
    Image image = toolkit.getImage(imagePath);

    return image;
  }

  public static Icon GetIcon (String imageName)
  {
    return new ImageIcon(GetImage(imageName));
  }

  public static String Capitalize(String oldString)
  {
    String newString = "";
    if (oldString.length() == 1)
    {
      newString = oldString.toUpperCase();
    }
    else
    if (oldString.length() > 1)
    {
      newString =
        oldString.substring(0,1).toUpperCase() +
        oldString.substring(1).toLowerCase();
    }
    return newString;
  }
}
