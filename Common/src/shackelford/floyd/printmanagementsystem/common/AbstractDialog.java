package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JPanel;

/**
 * <p>Title: AbstractDialog</p>
 * <p>Description:
 * An abstract dialog class to handle the bulk of the "look & feel" of the system's dialogs.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractDialog
  extends JDialog
  implements DialogListener, DialogNotifier
{

  /**
   * Eclipse generated value
   */
  private static final long           serialVersionUID      = -9188403144155939875L;

  /**
   * _dialogFinalProperties: the final values as entered by the user
   */
  protected Properties                _dialogFinalProperties;

  /*
   * GUI components
   */
  protected ActionButton              _okButton;

  protected ActionButton              _cancelButton;

  protected ArrayList<DialogListener> _dialogListeners;

  protected Container                 _northPanel;

  protected Container                 _southPanel;

  protected Container                 _eastPanel;

  protected Container                 _westPanel;

  protected Container                 _centerPanel;

  /**
   * CANCEL: resource key
   */
  public static final String          CANCEL                = "CANCEL";

  /**
   * CANCEL_TIP: hover help resource key
   */
  public static final String          CANCEL_TIP            = "CANCEL_TIP";

  /**
   * OK: resource key
   */
  public static final String          OK                    = "OK";

  /**
   * OK_TIP: hover help resource key
   */
  public static final String          OK_TIP                = "OK_TIP";

  /**
   * TITLE: resource key
   */
  public static final String          TITLE                 = "TITLE";

  /**
   * TRUE: resource key
   */
  public static final String          TRUE                  = "true";

  /**
   * FALSE: resource key
   */
  public static final String          FALSE                 = "false";

  /**
   * FIELD_WIDTH: the width of a field
   */
  public static final int             FIELD_WIDTH           = 16;

  /**
   * INTER_FIELD_GAP_WIDTH: gap between fields
   */
  public static final int             INTER_FIELD_GAP_WIDTH = 10;

  /**
   * INTRA_FIELD_GAP_WIDTH: gap between sub-fields
   */
  public static final int             INTRA_FIELD_GAP_WIDTH = 5;

  /**
   * HORIZONTAL_STRUT1: GUI spacing
   */
  public static final int             HORIZONTAL_STRUT1     = 3;

  /**
   * Constructor
   * @param owner
   * @param modal
   */
  public AbstractDialog(Frame owner, boolean modal)
  {
    super(owner, "", modal);
  }

  /**
   * Initialize the dialog.
   * 
   * I prefer a "2 phase" object construction:
   * 1. create the object (preferably with the default constructor)
   * 2. initialize the object
   */
  public void Initialize()
  {
    addWindowListener(new WindowListener());

    SetTitle();

    setResizable(false);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    AssignFields();

    pack();
    PositionInitialPanel();
    validate();

  }

  /**
   * set the title of the dialog to the TITLE resource 
   */
  protected void SetTitle()
  {
    setTitle(GetResources().getProperty(TITLE));
  }

  /**
   * this method is here to give child classes an opportunity to override the type of panel to be placed
   * in the North section.
   * @return JPanel
   */
  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * this method is here to give child classes an opportunity to override the type of panel to be placed
   * in the Center section.
   * @return JPanel
   */
  protected Container CreateCenterPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * this method is here to give child classes an opportunity to override the type of panel to be placed
   * in the West section.
   * @return JPanel
   */
  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * this method is here to give child classes an opportunity to override the type of panel to be placed
   * in the North section.
   * @return JPanel
   */
  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  /**
   * this method is here to give child classes an opportunity to override the type of panel and contents
   * to be placed in the South section.
   * @return JPanel
   */
  protected Container CreateSouthPanel()
  {
    Box verticalBox = Box.createVerticalBox();

    Box buttonBox = Box.createHorizontalBox();
    buttonBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT1));
    buttonBox.add(new HelpButton(this));
    buttonBox.add(Box.createHorizontalGlue());
    AddSouthPanelButtons(buttonBox);
    _okButton = new ActionButton(new OKAction());
    buttonBox.add(_okButton);
    _cancelButton = new ActionButton(new CancelAction());
    buttonBox.add(_cancelButton);
    buttonBox.add(Box.createHorizontalStrut(HORIZONTAL_STRUT1));
    verticalBox.add(buttonBox);

    return verticalBox;
  }

  /**
   * a "hook" to allow child classes to add additional action buttons in the south panel.
   * @param buttonBox
   */
  protected void AddSouthPanelButtons(Box buttonBox)
  {
    // do nothing
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.DialogNotifier#AddDialogListener(shackelford.floyd.printmanagementsystem.common.DialogListener)
   */
  @Override
  public void AddDialogListener(DialogListener listener)
  {
    if (_dialogListeners == null)
    {
      _dialogListeners = new ArrayList<>();
    }
    if (listener != null)
    {
      if (_dialogListeners.indexOf(listener) == -1)
      {
        _dialogListeners.add(listener);
      }
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.DialogNotifier#RemoveDialogListener(shackelford.floyd.printmanagementsystem.common.DialogListener)
   */
  @Override
  public void RemoveDialogListener(DialogListener listener)
  {
    if (listener != null)
    {
      int listenerIndex = _dialogListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _dialogListeners.remove(listenerIndex);
      }
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.DialogNotifier#NotifyDialogListeners()
   */
  @Override
  public void NotifyDialogListeners()
  {
    if (_dialogListeners != null)
    {
      Properties props = GetProperties();
      for (Iterator<DialogListener> iterator = _dialogListeners.iterator(); iterator.hasNext();)
      {
        DialogListener listener = iterator.next(); 
        listener.EndOfDialog(props); 
      }
      _dialogListeners = null;
    }
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.DialogListener#WaitOnDialog()
   */
  @Override
  public Properties WaitOnDialog()
  {
    AddDialogListener(this);
    setVisible(true);
    return _dialogFinalProperties;
  }

  /**
   * @see shackelford.floyd.printmanagementsystem.common.DialogListener#EndOfDialog(java.util.Properties)
   */
  @Override
  public void EndOfDialog(Properties props)
  {
    _dialogFinalProperties = props;
  }

  /**
   * @return Properties the values of the GUI components of the dialog.
   */
  protected Properties GetProperties()
  {
    Properties props = new Properties();
    props.setProperty(OK, new Boolean(_okButton.isSelected()).toString());
    props.setProperty(CANCEL, new Boolean(_cancelButton.isSelected()).toString());
    return props;
  }

  /**
   * @return true if the panel is valid.
   */
  protected boolean PanelValid()
  {
    return true;
  }

  /**
   * method to position the panel on the screen. this method centers the dialog.
   */
  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  /**
   * @return Resources
   */
  protected abstract Resources GetResources();

  /**
   * assign the GUI fields their values.
   */
  protected abstract void AssignFields();

  protected class WindowListener
    extends WindowAdapter
  {
    @Override
    public void windowClosing(WindowEvent event)
    {
      _cancelButton.setSelected(true);
      NotifyDialogListeners();
      dispose();
    }
  }

  /**
   * the user pressed the OK button.
   * @return true if OK is successful.
   */
  protected boolean OKAction()
  {
    _okButton.setSelected(true);

    boolean rc = PanelValid();
    if (rc == true)
    {
      NotifyDialogListeners();
      dispose();
    }
    return rc;
  }

  protected class OKAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 8459078601991787732L;

    public OKAction()
    {
      putValue(Action.NAME, GetResources().getProperty(OK).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(OK_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      OKAction();
    }
  }

  /**
   * the user pressed the cancel button.
   */
  protected void CancelAction()
  {
    _cancelButton.setSelected(true);
    NotifyDialogListeners();
    dispose();
  }

  protected class CancelAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value 
     */
    private static final long serialVersionUID = 7145382226956227278L;

    public CancelAction()
    {
      putValue(Action.NAME, GetResources().getProperty(CANCEL).toString());
      putValue(Action.SHORT_DESCRIPTION, GetResources().getProperty(CANCEL_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      CancelAction();
    }
  }

}
