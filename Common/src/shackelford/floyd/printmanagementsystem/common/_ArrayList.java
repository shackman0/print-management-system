package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.util.Collection;

/**
 * <p>Title: _ArrayList</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 * @param <ElementType>
 */
public class _ArrayList<ElementType>
  extends java.util.ArrayList<ElementType>
{

  /**
   * serialVersionUID: Eclipse generated value
   */
  private static final long serialVersionUID = 7199485012605969123L;
  
  protected Class<?> _elementType = null;

  public _ArrayList()
  {
    super();
  }

  public _ArrayList(Class<?> elementType)
  {
    super();

    this._elementType = elementType;
  }

  public _ArrayList(int size, Class<?> elementType)
  {
    super(size);

    this._elementType = elementType;
  }

  public _ArrayList(int size)
  {
    super(size);
  }

  public _ArrayList(Collection<? extends ElementType> collection)
  {
    super(collection);
  }

  public _ArrayList(Collection<? extends ElementType> collection, Class<?> elementType)
  {
    super(collection);

    this._elementType = elementType;
  }

  public _ArrayList(ElementType[] array)
  {
    this(array.length, array.getClass().getComponentType());

    for (ElementType element : array)
    {
      add(element);
    }
  }

  public void SetElementType(Class<?> elementType)
  {
    this._elementType = elementType;
  }

  public Class<?> GetElementType()
  {
    return _elementType;
  }

  public void AddAllUnique(Collection<ElementType> collection)
  {
    for (ElementType element : collection)
    {
      AddUnique(element);
    }
  }

  /**
   * clears the array list and initializes it with size fillElements
   */
  public void Fill(ElementType fillElement, int size)
  {
    clear();
    for (int indx = 0; indx < size; indx++)
    {
      add(fillElement);
    }
  }

  /**
   * add the item to the end of the list if it already doesn't exist in the list.
   *
   * @param element ElementType
   * @return boolean true = added; false = already present, not added
   */
  public boolean AddUnique(ElementType element)
  {
    boolean unique = (contains(element) == false);

    if (unique == true)
    {
      add(element);
    }

    return unique;
  }

  public void AddAll(ElementType[] array)
  {
    ensureCapacity(array.length);
    for (int indx = 0; indx < array.length; indx++)
    {
      add(array[indx]);
    }
  }

  /**
   * creates an array
   * @return ElementType[]
   */
  public ElementType[] ToArray(Class<?> elementType)
  {
    ElementType[] array = (ElementType[]) (Array.newInstance(elementType, size()));
    return toArray(array);
  }

  /**
   * creates an array
   * @return Object[]
   */
  public ElementType[] ToArray()
  {
    ElementType[] array = (ElementType[]) (Array.newInstance(_elementType, size()));
    return toArray(array);
  }

  /**
   * this only works if all the elements are Integers. if they aren't you'll get a class cast exception.
   * @return int[]
   */
  public int[] ToIntArray()
  {
    int size = size();
    int[] intArray = new int[size];

    for (int index = 0; index < size; index++)
    {
      Object element = get(index);
      Integer integer = (Integer) element;
      intArray[index] = integer.intValue();
    }

    return intArray;
  }

  /**
   * return the list of elements as a single string constructed as:
   *   element0.toString() + "\n" + element1.toStirng() + "\n" + ... + elementLast.toString()
   */
  public String ToLines()
  {
    StringBuilder stringBuilder = new StringBuilder();

    for (ElementType element : this)
    {
      stringBuilder.append(element.toString());
      stringBuilder.append("\n");
    }

    int length = stringBuilder.length();
    if (length > 0)
    {
      // remove the trailing "\n"
      stringBuilder.setLength(length - 1);
    }

    return stringBuilder.toString();
  }

  public Object[] GetElementData()
  {
    Field elementDataField;
    try
    {
      elementDataField = java.util.ArrayList.class.getDeclaredField("elementData");
    }
    catch (SecurityException ex)
    {
      throw ex;
    }
    catch (NoSuchFieldException ex)
    {
      throw new RuntimeException(ex);
    }

    ElementType[] elementData;
    try
    {
      elementDataField.setAccessible(true);
      elementData = (ElementType[]) (elementDataField.get(this));
    }
    catch (IllegalArgumentException ex)
    {
      throw ex;
    }
    catch (IllegalAccessException ex)
    {
      throw new RuntimeException(ex);
    }

    return elementData;
  }

}
