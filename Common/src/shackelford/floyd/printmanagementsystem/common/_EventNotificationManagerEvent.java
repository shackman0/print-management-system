package shackelford.floyd.printmanagementsystem.common;

import java.util.EventObject;

/**
 * <p>Title: _EventNotificationManagerEvent</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class _EventNotificationManagerEvent
  extends EventObject
{

  /**
   * Eclipse generated value
   */
  private static final long    serialVersionUID = 169204748612084084L;

  /**
   * indicates whether the event was successful (true) or not (false)
   */
  protected boolean            _success         = true;

  /**
   * keeps track of arbitrary message specified object
   */
  protected Object             _other           = null;

  /**
   * if true, then the event was "consumed" by the most recently notified listener and notification of this event should cease immediately.
   */
  protected boolean            _consumed        = false;

  /**
   * the DTS of when the event occurred.
   */
  protected _GregorianCalendar _dts             = new _GregorianCalendar();

  protected _EventNotificationManagerEvent(Object eventSource)
  {
    super(eventSource);
  }

  protected _EventNotificationManagerEvent(Object eventSource, boolean success)
  {
    super(eventSource);

    _success = success;
  }

  protected _EventNotificationManagerEvent(Object eventSource, boolean success, Object other)
  {
    this(eventSource, success);

    _other = other;
  }

  public Object GetOther()
  {
    return _other;
  }

  public _GregorianCalendar GetDTS()
  {
    return _dts;
  }

  /**
   * Setter
   * @param success true if the event was successful; false otherwise. generally, start events always set this to true.
   */
  public void SetSuccess(boolean success)
  {
    _success = success;
  }

  public boolean WasSuccessful()
  {
    return _success;
  }

  public void SetConsumed(boolean consumed)
  {
    _consumed = consumed;
  }

  public boolean GetConsumed()
  {
    return _consumed;
  }

}
