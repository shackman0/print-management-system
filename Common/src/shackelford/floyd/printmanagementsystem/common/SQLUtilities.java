package shackelford.floyd.printmanagementsystem.common;

import java.util.HashMap;


/**
 * <p>Title: SQLUtilities</p>
 * <p>Description:
  this class encapsulates a set of SQL convenience operations
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SQLUtilities
{

  public static final char SQL_WILD_CARD_MULTI   = '%';
  public static final char SQL_WILD_CARD_SINGLE  = '_';

  /**
    This is a convenience method to make a string acceptable to the sql parser.
    It takes a string and:
    1. prepend all \'s with a \ to make \\
    2. prepend all ''s with a \ to make \'

    @param stringIn the string to be fixed-up
    @return the fixed up string
  */
  public static String FixSQLString ( String stringIn )
  {
    String newString = stringIn;
    // escape backslashes
    int i = 0;
    while (i != -1)
    {
      i = newString.indexOf('\\',i);
      if (i != -1)
      {
        String tempString = newString.substring(0,i) + "\\" + newString.substring(i);
        newString = tempString;
        i+=2;
      }
    }
    // escape single quotes
    i = 0;
    while (i != -1)
    {
      i = newString.indexOf('\'',i);
      if (i != -1)
      {
        String tempString = newString.substring(0,i) + "\\" + newString.substring(i);
        newString = tempString;
        i+=2;
      }
    }
    return newString;
  }

  /**
    This is a convenience method to make a string work with the LIKE query
    It takes a string and:
    1. converts all wildCardMulti's to SQL_WILD_CARD_MULTI's
    2. prepend all wildCardSingle's to SQL_WILD_CARD_SINGLE's

    @param stringIn the string to be fixed-up
    @param wildCardMulti the character that represents a multiple (0 or more) character wildcard in stringIn
    @param wildCardSingle the character that represents a single (1 and only 1) character wildcard in stringIn
    @return the fixed up string
  */
  public static String FixSQLLikeQuery (
    String stringIn,
    char wildCardMulti,
    char wildCardSingle )
  {
    String newString = new String(stringIn);
    newString = newString.replace(wildCardMulti,SQL_WILD_CARD_MULTI);
    newString = newString.replace(wildCardSingle,SQL_WILD_CARD_SINGLE);

    return newString;
  }

  public static HashMap<String, String> CreateMap(String[] keys, String[] values)
  {
    HashMap<String, String> hashMap = new HashMap<>(keys.length);
    for (int i = 0; i < keys.length; i++)
    {
      hashMap.put(keys[i],values[i]);
    }
    return hashMap;
  }


  public static String HighValues ( int length )
  {
    // if we need a string that's longer than the one we started with, double it
    while (highValues.length() < length)
    {
      highValues = highValues + highValues;
    }
    return highValues.substring(0,length);
  }

  private static String highValues = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";


  public static void main(String args[])
  {
    System.out.println(FixSQLString("that's all folks!"));
    System.out.println(FixSQLString("'that's all folks!"));
    System.out.println(FixSQLString("that's all folks!'"));
    System.out.println(FixSQLString("''that''s all folks!''"));

    System.out.println(FixSQLString("d:\\temp"));
    System.out.println(FixSQLString("d:\\temp\\"));
    System.out.println(FixSQLString("\\d:\\temp\\"));
    System.out.println(FixSQLString("\\\\d:\\\\temp\\\\"));
  }
}
