package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.Field;
import java.sql.ResultSet;


/**
 * <p>Title: AbstractSQL5Row</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL5Row
  extends AbstractSQL4Row
{

  protected String                _sql_row_id;  // key

  protected static final String   UNASSIGNED_ROW_ID = "-1";
  protected static final String   ROW_ID_SUFFIX = "row_id";

  public AbstractSQL5Row ()
  {
    super();
  }

  public AbstractSQL5Row( AbstractSQL5Table sqlTable )
  {
    super();
    Initialize(sqlTable);
  }

  public AbstractSQL5Row( AbstractSQL5Table sqlTable, AbstractSQL1Row initRow )
  {
    Initialize(sqlTable, initRow);
  }

  public AbstractSQL5Row ( AbstractSQL5Table sqlTable, ResultSet resultSet )
  {
    Initialize(sqlTable,resultSet);
  }

  public String Get_row_id() { return new String(_sql_row_id); }
  public void Set_row_id(String value) { _sql_row_id = new String(value); }
  public static String Name_row_id() { return "row_id"; }

  public void AssignNewRowID()
  {
    Set_row_id(GetSQL5Table().GetNextRowID());
  }

  @Override
  protected void InitField(Field field)
  {
    Class<?> fieldClass = field.getType();
    // not all objects have a parm-less constructor, so we handle those
    // first. in fact, we handle most cases explicitely.
    if ( (fieldClass == String.class) &&
         (GetFieldName(field).endsWith(ROW_ID_SUFFIX) == true) )
    {
      SetValue(field,UNASSIGNED_ROW_ID);
    }
    else
    {
      super.InitField(field);
    }
  }

  private AbstractSQL5Table GetSQL5Table()
  {
    return (AbstractSQL5Table)_sqlTable;
  }

  protected Object ConvertSQLObjectToJavaObject(Field field, Object sqlObject)
  {
    if ( (sqlObject.getClass() == java.lang.Integer.class) &&
         (field.getName().endsWith(ROW_ID_SUFFIX) == true) )
    {
      // we store rowIDs in the database in int4 form, but we store rowIDs
      // in the row object in String form. Why: in our getRowsWithSubstitution...
      // methods, we substitute the foreign key strings for the rowIDs. this means
      // that sometimes, rowIDs come from the database in integer form and sometimes
      // rowIDs come from the database in String form. Alternatively, we could always
      // encode the SQL statement to return a string datatype. However, if we forget
      // to do this just once, we'll have a run time error. this approach makes sure
      // that we always handle it and never have to worry about it again.
      sqlObject = sqlObject.toString();
    }
    else
    {
      sqlObject = super.ConvertSQLObjectToJavaObject(sqlObject);
    }
    return sqlObject;
  }

}
