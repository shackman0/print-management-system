package shackelford.floyd.printmanagementsystem.common;

import javax.swing.JOptionPane;


/**
 * <p>Title: MyJOptionPane</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class MyJOptionPane
  extends JOptionPane
{

  /**
   * 
   */
  private static final long serialVersionUID = -495966915512248195L;


  public MyJOptionPane (
    Object aMessage,
    int    aMessageType )
  {
    super(aMessage,aMessageType);
  }


  public MyJOptionPane (
    Object aMessage,
    int    aMessageType,
    int    anOptionType )
  {
    super(aMessage,aMessageType,anOptionType);
  }


  @Override
  public int getMaxCharactersPerLineCount()
  {
    return 60;
  }

}