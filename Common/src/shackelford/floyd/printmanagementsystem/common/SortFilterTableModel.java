package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;

import javax.swing.JTable;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableModel;

/**
 * <p>Title: SortFilterTableModel</p>
 * <p>Description: </p>
 *   the SortFilterTableModel class provides a sort filter between the display
 *  of a table and the table model. Double clicking on a column header the first
 *  time will sort the rows by that column in ascending order. Double clicking again
 *  on that same column header will sort the rows by that column in descending order.
 *  Double clicking a column reverses the sort order each time.
 *
 *  to do:
 *
 *  1. change the column headers to buttons and let the user just press the button
 *     instead of double clicking (provides better visual feedback)
 *
 *  2. if sorting by a column, change the button for the column to include an up or
 *     down arrow to indicate that the table is sorted by that column and the order
 *     of the sort. when sorting on another table column, revert to the non-arrowed
 *     button.
 *
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SortFilterTableModel
  extends AbstractTableModel
{
  

  /**
   * Eclipse generated value 
   */
  private static final long serialVersionUID = -6112035211767616347L;

  TableModel                _model;

  final JTable              _table;

  int                       _sortColumn;

  private Row[]             _rows;

  int[]                     _columnSortOrder;     // 1 = ascending, -1 = descending

  public SortFilterTableModel(JTable table)
  {
    super();
    _table = table;
    addMouseListener();
  }

  public SortFilterTableModel(JTable table, TableModel m)
  {
    super();
    _table = table;
    addMouseListener();
    SetTableModel(m);
  }

  public void TableModelChanged()
  {
    SetTableModel(_model);
  }

  public TableModel GetTableModel()
  {
    return _model;
  }

  public void SetTableModel(TableModel m)
  {
    _model = m;
    _rows = new Row[_model.getRowCount()];
    for (int i = 0; i < _rows.length; i++)
    {
      _rows[i] = new Row();
      _rows[i].index = i;
    }
    _columnSortOrder = new int[_model.getColumnCount()];
    for (int i = 0; i < _columnSortOrder.length; i++)
    {
      // set it to descending so when it's double clicked the first time,
      // it'll change to ascending
      _columnSortOrder[i] = -1;
    }
    fireTableStructureChanged();
    //fireTableDataChanged();
  }

  public void sort(int modelColumnNumber)
  {
    _sortColumn = modelColumnNumber;
    _columnSortOrder[_sortColumn] *= -1;
    Arrays.sort(_rows);
    fireTableDataChanged();
  }

  public void addMouseListener()
  {
    _table.getTableHeader().addMouseListener(new MouseListener());
  }

  /* compute the moved row for the three methods that access
     model elements
  */

  public int MapViewToSortedRow(int viewRow)
  {
    return _rows[viewRow].index;
  }

  @Override
  public Object getValueAt(int r, int modelColumnNumber)
  {
    return _model.getValueAt(_rows[r].index, modelColumnNumber);
  }

  @Override
  public boolean isCellEditable(int r, int modelColumnNumber)
  {
    return _model.isCellEditable(_rows[r].index, modelColumnNumber);
  }

  @Override
  public void setValueAt(Object aValue, int r, int modelColumnNumber)
  {
    _model.setValueAt(aValue, _rows[r].index, modelColumnNumber);
  }

  /* delegate all remaining methods to the model
  */

  @Override
  public int getRowCount()
  {
    return _model.getRowCount();
  }

  @Override
  public int getColumnCount()
  {
    return _model.getColumnCount();
  }

  @Override
  public String getColumnName(int modelColumnNumber)
  {
    return _model.getColumnName(modelColumnNumber);
  }

  @Override
  public Class<?> getColumnClass(int modelColumnNumber)
  {
    return _model.getColumnClass(modelColumnNumber);
  }

  /* this inner class holds the index of the model row
     Rows are compared by looking at the model row entries
     in the sort column
  */

  private class Row
    implements Comparable<Row>
  {

    public Row()
    {
      super();
    }

    @SuppressWarnings("unchecked")
    @Override
    public int compareTo(Row otherRow)
    {
      Object a = _model.getValueAt(index, _sortColumn);
      Object b = _model.getValueAt(otherRow.index, _sortColumn);

      if (a instanceof Comparable)
      {
        return _columnSortOrder[_sortColumn] * ((Comparable<Object>) a).compareTo(b);
      }

      return _columnSortOrder[_sortColumn] * (index - otherRow.index);
    }

    int index;

  }

  private class MouseListener
    extends MouseAdapter
  {

    public MouseListener()
    {
      super();
    }

    @Override
    public void mouseClicked(MouseEvent event)
    {
      // check for double click
      if (event.getClickCount() < 2)
      {
        return;
      }

      // find column of click and
      int tableColumn = _table.columnAtPoint(event.getPoint());

      // translate to table model index
      int modelColumn = _table.convertColumnIndexToModel(tableColumn);
      sort(modelColumn);
    }
  }

}
