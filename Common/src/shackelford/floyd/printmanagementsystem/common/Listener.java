package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: Listener</p>
 * <p>Description:
  use this interface to be notified by a Notifier.

  here's a typical implementation:

public class AClass
  implements Listener
{

  public AClass ()
  {
    ...
    aNotifier.AddListener(this);
    ...
  }

  protected finalize ()
  {
    ...
    aNotifier.RemoveListener(this);
    ...
  }

  public void Notification (char event)
  {
    // do something with the event
  }

}
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface Listener
{

  public void Notification (char event);

}