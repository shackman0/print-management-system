package shackelford.floyd.printmanagementsystem.common;

import java.awt.Dimension;
import java.util.Vector;

import javax.swing.ComboBoxModel;
import javax.swing.JComboBox;


/**
 * <p>Title: _JComboBox</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class _JComboBox
  extends JComboBox<String>
{

  private static final long serialVersionUID = 8398893600978130894L;

  public _JComboBox()
  {
    super();
  }

  public _JComboBox(ComboBoxModel<String> aModel)
  {
    super(aModel);
  }

  public _JComboBox(String[] items)
  {
    super(items);
  }

  public _JComboBox(Vector<String> items)
  {
    super(items);
  }

  // convenience method
  public static _JComboBox CreateComboBox(String[] listItems, String itemToSelect, boolean insertBlank)
  {
    _JComboBox comboBox = new _JComboBox();
    comboBox.RecreateComboBox(listItems, itemToSelect, insertBlank);
    return comboBox;
  }

  public void RecreateComboBox(String[] listItems, String itemToSelect, boolean insertBlank)
  {
    ReplaceAllItems(listItems);

    if (itemToSelect == null)
    {
      itemToSelect = "";
    }

    if (insertBlank == true)
    {
      int i = FindItemIndex("");
      if (i == -1)
      {
        insertItemAt("", 0);
      }
    }

    if (getItemCount() > 0)
    {
      int i = FindItemIndex(itemToSelect.trim());
      if (i == -1)
      {
        i = 0;
      }
      setSelectedIndex(i);
    }
  }

  public void ReplaceAllItems(String[] newItems)
  {
    removeAllItems();
    for (String newItem : newItems)
    {
      addItem(newItem);
    }
  }

  public void SafeSetSelectedIndex(int index)
  {
    if (index > getItemCount())
    {
      index = 0;
    }
    setSelectedIndex(index);
  }

  /**
   * If the item is found, sets the selected item to it
   */
  public void SafeSetSelectedValue(String value)
  {
    setSelectedIndex(SafeFindValueIndex(value));
  }

  /**
   * If the item is found, it returns the index in the combo box
   * If the item is not found, it returns -1
   */
  public int FindValueIndex(String valueToFind)
  {
    if (valueToFind == null)
    {
      return -1;
    }
    for (int i = 0; i < getItemCount(); i++)
    {
      if (getItemAt(i).toString().equals(valueToFind) == true)
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * If the item is found, it returns the index in the combo box
   * If the item is not found, it returns 0
   */
  public int SafeFindValueIndex(String valueToFind)
  {
    int i = FindValueIndex(valueToFind);
    if (i < 0)
    {
      return 0;
    }
    return i;
  }

  /**
   * If the item is found, sets the selected item to it
   */
  public void SafeSetSelectedItem(Object item)
  {
    setSelectedIndex(SafeFindItemIndex(item));
  }

  /**
   * If an item is selected, returns the item, otherwise returns the first item in the list
   */
  public Object SafeGetSelectedItem()
  {
    Object selectedObj = getSelectedItem();
    if (selectedObj == null)
    {
      selectedObj = "";
    }
    return selectedObj;
  }

  /**
   * If the item is found, it returns the index in the combo box
   * If the item is not found, it returns -1
   */
  public int FindItemIndex(Object itemToFind)
  {
    if (itemToFind == null)
    {
      return -1;
    }
    for (int i = 0; i < getItemCount(); i++)
    {
      if (getItemAt(i).equals(itemToFind) == true)
      {
        return i;
      }
    }
    return -1;
  }

  /**
   * If the item is found, it returns the index in the combo box
   * If the item is not found, it returns 0
   */
  public int SafeFindItemIndex(Object itemToFind)
  {
    int i = FindItemIndex(itemToFind);
    if (i < 0)
    {
      return 0;
    }
    return i;
  }

  /**
   * If the item is found, it returns the object
   * If the item is not found, it returns null
  */
  public Object FindItem(Object itemToFind)
  {
    if (itemToFind == null)
    {
      return null;
    }
    for (int i = 0; i < getItemCount(); i++)
    {
      Object obj = getItemAt(i);
      if (obj.equals(itemToFind) == true)
      {
        return obj;
      }
    }
    return null;
  }

  public void SetWidth(int newWidth)
  {
    Dimension dimension = getPreferredSize();
    dimension.width = newWidth;
    setPreferredSize(dimension);
  }

}
