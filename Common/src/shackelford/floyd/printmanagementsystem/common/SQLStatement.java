package shackelford.floyd.printmanagementsystem.common;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;



/**
 * <p>Title: SQLStatement</p>
 * <p>Description:
  this class encapsulates a SQLStatement and ResultSet pair

  Note: When you are done with a ResultSet, do not call close on the ResultSet,
  instead, call CloseResultSet. this closes the result set for you and
  frees up the corresponding sqlStatement for further use.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SQLStatement
implements AutoCloseable
{

  private Statement               _sqlStatement;
  private ResultSet               _resultSet;
  private boolean                 _available;


  public SQLStatement ( Connection databaseConnection )
  {
    try
    {
      _sqlStatement = databaseConnection.createStatement();
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      JOptionPane.showMessageDialog(null, excp, "databaseConnection.createStatement()", JOptionPane.ERROR_MESSAGE);
      _sqlStatement = null;
    }
    _resultSet = null;
    MakeAvailable();
  }

  @Override
  protected void finalize()
  {
    CloseSQLStatement();
  }

  public Statement GetSQLStatement ()
  {
    return _sqlStatement;
  }

  public ResultSet GetResultSet ()
  {
    return _resultSet;
  }

  public ResultSet ExecuteQuery ( String sqlString )
  {
    if (_resultSet != null)
    {
      CloseResultSet();
    }
    try
    {
      _resultSet = _sqlStatement.executeQuery(sqlString);
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      PrintSQLString(sqlString);
      MessageDialog.ShowErrorMessageDialog (
        null,
        excp,
        "sqlStatement.executeQuery(sqlString)" );
      _resultSet = null;
    }

    return _resultSet;
  }

  public int ExecuteUpdate ( String sqlString )
  {
    int rc = 0;
    if (_resultSet != null)
    {
      CloseResultSet();
    }
    try
    {
      rc = _sqlStatement.executeUpdate(sqlString);
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      PrintSQLString(sqlString);
      JOptionPane.showMessageDialog(null, excp, "sqlStatement.executeUpdate(sqlString)", JOptionPane.ERROR_MESSAGE);
      _resultSet = null;
      rc = -1;
    }
    if (rc < 0)
    {
      DebugWindow.DisplayText("sql rc="+rc);
    }

    return rc;
  }

  public void BeginTransaction()
  {
    ExecuteUpdate("begin transaction");
  }

  public void CommitTransaction()
  {
    ExecuteUpdate("commit transaction");
  }

  public void RollbackTransaction()
  {
    ExecuteUpdate("rollback transaction");
  }

  public void CloseSQLStatement ()
  {
    CloseResultSet();
    if (_sqlStatement != null)
    {
      try
      {
        _sqlStatement.close();
      }
      catch (SQLException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        JOptionPane.showMessageDialog (
          null,
          excp,
          "sqlStatement.close()",
          JOptionPane.ERROR_MESSAGE );
      }
      _sqlStatement = null;
    }
  }

  public void CloseResultSet ()
  {
    if (_resultSet != null)
    {
      try
      {
        _resultSet.close();
      }
      catch (SQLException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        JOptionPane.showMessageDialog (
          null,
          excp,
          "resultSet.close()",
          JOptionPane.ERROR_MESSAGE );
      }
      _resultSet = null;
    }
  }

  public boolean IsAvailable ()
  {
    return _available;
  }

  /**
   * if possible, use try-with-resources rather than calling this method directly.
   */
  public void MakeAvailable()
  {
    CloseResultSet();
    _available = true;
  }

  public void MakeUnavailable()
  {
    _available = false;
  }

  private void PrintSQLString(String sqlString)
  {
/*
    DebugWindow.DisplayText("");
    for (int i = 0; i < sqlString.length(); i+=DEBUG_OUTPUT_WIDTH)
    {
      DebugWindow.DisplayText(sqlString.substring(i,Math.min(i + DEBUG_OUTPUT_WIDTH,sqlString.length())));
    }
*/
  }

  @Override
  public void close()
  {
    MakeAvailable();
  }

}
