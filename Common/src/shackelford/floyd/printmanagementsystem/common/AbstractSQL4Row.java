package shackelford.floyd.printmanagementsystem.common;

import java.sql.ResultSet;


/**
 * <p>Title: AbstractSQL4Row</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL4Row
  extends AbstractSQL3Row
{

  protected String                _sql_status;

  public static final String      STATUS_ENABLED_CODE = "e";
  public static final String      STATUS_DISABLED_CODE = "d";
  public static final String      STATUS_CLOSED_CODE = "c";
  public static final String      STATUS_ENABLED_NAME = "enabled";
  public static final String      STATUS_DISABLED_NAME = "disabled";
  public static final String      STATUS_CLOSED_NAME = "closed";


  public AbstractSQL4Row ()
  {
    super();
  }

  public AbstractSQL4Row( AbstractSQL4Table sqlTable )
  {
    super();
    Initialize(sqlTable);
  }

  public AbstractSQL4Row( AbstractSQL4Table sqlTable, AbstractSQL1Row initRow )
  {
    Initialize(sqlTable, initRow);
  }

  public AbstractSQL4Row ( AbstractSQL4Table sqlTable, ResultSet resultSet )
  {
    Initialize(sqlTable,resultSet);
  }

  @Override
  protected void InitFields()
  {
    super.InitFields();
    _sql_status = STATUS_ENABLED_CODE;
  }

  public boolean OK2Enable()
  {
    return true;
  }

  public boolean OK2Disable()
  {
    return true;
  }

  public boolean OK2Close()
  {
    return true;
  }

  public String Get_status()
  {
    String value = null;
    if (_sql_status.equals(STATUS_ENABLED_CODE) == true)
    {
      value = STATUS_ENABLED_NAME;
    }
    else
    if (_sql_status.equals(STATUS_DISABLED_CODE) == true)
    {
      value = STATUS_DISABLED_NAME;
    }
    else
    if (_sql_status.equals(STATUS_CLOSED_CODE) == true)
    {
      value = STATUS_CLOSED_NAME;
    }
    else
    {
      new Exception("Get_status:Invalid _sql_status=\""+_sql_status+"\"").printStackTrace();
      value = new String(_sql_status);       // fail safe
    }
    return value;
  }
  public void Set_status(String value)
  {
    if (value.equals(STATUS_ENABLED_NAME) == true)
    {
      _sql_status = STATUS_ENABLED_CODE;
    }
    else
    if (value.equals(STATUS_DISABLED_NAME) == true)
    {
      _sql_status = STATUS_DISABLED_CODE;
    }
    else
    if (value.equals(STATUS_CLOSED_NAME) == true)
    {
      _sql_status = STATUS_CLOSED_CODE;
    }
    else
    {
      if (value.length() > 0)
      {
        new Exception("Set_status:Invalid status=\""+value+"\"").printStackTrace();
      }
      _sql_status = new String(value);        // fail safe
    }
  }
  public String GetInternal_status() { return new String(_sql_status); }
  public boolean StatusIsEnabled()
  {
    return _sql_status.equals(STATUS_ENABLED_CODE) == true ? true : false;
  }
  public boolean StatusIsDisabled()
  {
    return _sql_status.equals(STATUS_DISABLED_CODE) == true ? true : false;
  }
  public boolean StatusIsClosed()
  {
    return _sql_status.equals(STATUS_CLOSED_CODE) == true ? true : false;
  }
  public static String Name_status() { return "status"; }


  public AbstractSQL4Table GetSQL4Table()
  {
    return (AbstractSQL4Table)_sqlTable;
  }

}
