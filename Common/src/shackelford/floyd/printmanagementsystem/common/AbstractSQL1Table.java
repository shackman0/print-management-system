package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.JOptionPane;


/**
 * <p>Title: AbstractSQL1Table</p>
 * <p>Description:
 * this class keeps track of information about the sql table.
 *
 * this class also provides set manipulation methods on sql rows. if you want information from a
 * single row, look for a method in the AbstractSQL1Row class. If you want methods that operate
 * upon 2 or more rows, look here.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL1Table
{

  public static final Boolean   TRUE                      = new Boolean(true);

  public static final Boolean   FALSE                     = new Boolean(false);

  public static final int       NO_LIMIT                  = -1;

  public static final int       DEFAULT_VIEW_WIDTH_ADD_ON = 20;

  protected static final String TRAILING_TABLE            = "_table";

  public AbstractSQL1Table(AbstractSQLDatabase sqlDatabase, String tableName, Class<? extends AbstractSQL1Row> rowClass)
  {
    _sqlDatabase = sqlDatabase;
    _tableName = tableName;
    try
    {
      // construct a row using the constructor that takes no parms
      Class<?>[] constructorParmsTypes = new Class[] {};
      Object[] constructorParms = new Object[] {};
      _row = (rowClass.getConstructor(constructorParmsTypes).newInstance(constructorParms));
      _row.Initialize(this);
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
  }

  public int RowCount(String predicate)
  {
    return RowCount(_tableName, predicate);
  }

  public int RowCount(String predicate, ListFilter listFilter)
  {
    predicate = ApplyListFilterToPredicate(predicate, listFilter);
    String from = ApplyListFilterToFrom(_tableName, listFilter);

    return RowCount(from, predicate);
  }

  @SuppressWarnings("resource")
  public int RowCount(String from, String predicate)
  {
    int rowCount = 0;

    SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();
    String sqlString = "select count(*) from " + from + ConstructPredicate(predicate);
    ResultSet resultSet = sqlStatement.ExecuteQuery(sqlString);

    try
    {
      resultSet.next();
      rowCount = resultSet.getInt(1);
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowMessageDialog(null, excp, "resultSet.next() or resultSet.getInt(1)", JOptionPane.ERROR_MESSAGE);
    }
    sqlStatement.MakeAvailable();

    return rowCount;
  }

  // convenience method
  protected String ConstructPredicate(String predicate)
  {
    String sqlString = "";
    if ((predicate != null) && (predicate.equals("") == false))
    {
      sqlString = " where " + predicate;
    }
    return sqlString;
  }

  // convenience method
  protected String ConstructLimit(int limit)
  {
    String sqlString = "";
    if (limit > 0)
    {
      sqlString = " limit " + String.valueOf(limit);
    }
    return sqlString;
  }

  // convenience method
  protected String ConstructOrderBy(String orderBy)
  {
    String sqlString = "";
    if ((orderBy != null) && (orderBy.equals("") == false))
    {
      sqlString = " order by " + orderBy;
    }
    return sqlString;
  }

  // convenience method
  protected String ConstructSelect()
  {
    String sqlString = "select ";
    String[] columnNames = GetSQLFieldNames(); // we don't use GetColumnNames because we want ALL the columns in the row, not just those being displayed on the list panel
    for (int i = 0; i < columnNames.length; i++)
    {
      sqlString += _tableName + "." + columnNames[i] + " as " + columnNames[i] + ", ";
    }
    sqlString = sqlString.substring(0, sqlString.length() - 2) + " ";
    return sqlString;
  }

  protected String ConstructFrom(String from)
  {
    if (from == null)
    {
      from = _tableName;
    }
    from = " from " + from;
    return from;
  }

  // convenience method
  protected String ConstructSQLString(String from, String predicate, String orderBy, int limit)
  {
    String sqlString = ConstructSelect() + ConstructFrom(from) + ConstructPredicate(predicate) + ConstructOrderBy(orderBy) + ConstructLimit(limit);
    return sqlString;
  }

  protected String ApplyListFilterToPredicate(String predicate, ListFilter listFilter)
  {
    if (listFilter == null)
    {
      return predicate;
    }

    String newPredicate = "";

    String[] filterViewNames = listFilter.GetFilterViewNames();
    for (int i = 0; i < filterViewNames.length; i++)
    {
      String filterViewName = filterViewNames[i].trim();
      if (filterViewName.length() > 0)
      {
        String filterOperator = listFilter.GetFilterOperator(i);
        String filterFieldValue = listFilter.GetFilterValue(i).trim();
        filterFieldValue = SQLUtilities.FixSQLString(filterFieldValue);
        filterFieldValue = SQLUtilities.FixSQLLikeQuery(filterFieldValue, ListFilter.WILD_CARD_MULTI_CHAR, ListFilter.WILD_CARD_SINGLE_CHAR);

        if (newPredicate.length() > 0)
        {
          newPredicate += " and ";
        }
        String columnFieldName = GetViewToColumnNamesMap().get(filterViewName).toString(); // convert view name to sql column name
        newPredicate += GetFilterFieldPredicate(columnFieldName, filterOperator, filterFieldValue);
      }
    }
    if (predicate.length() > 0)
    {
      if (newPredicate.length() > 0)
      {
        newPredicate += " and ";
      }
      newPredicate += predicate;
    }

    return newPredicate;
  }

  protected String GetFilterFieldPredicate(String sqlFieldName, String sqlOperator, String fieldValue)
  {
    String predicate = " ( " + _tableName + "." + sqlFieldName + " " + sqlOperator + " '" + fieldValue + "' ) ";
    return predicate;
  }

  protected String ApplyListFilterToOrderBy(String orderBy, ListFilter listFilter)
  {
    if (listFilter == null)
    {
      return orderBy;
    }

    String newOrderBy = "";

    String[] sortViewNames = listFilter.GetSortViewNames();
    for (int i = 0; i < sortViewNames.length; i++)
    {
      String sortViewName = sortViewNames[i].trim();
      if (sortViewName.length() > 0)
      {
        String sortFieldValue = listFilter.GetSortValue(i).trim();
        if (newOrderBy.length() > 0)
        {
          newOrderBy += ", ";
        }
        String columnFieldName = GetViewToColumnNamesMap().get(sortViewName).toString(); // convert view name to sql column name
        newOrderBy += columnFieldName + " " + sortFieldValue + " ";
      }
    }
    if (orderBy.length() > 0)
    {
      if (newOrderBy.length() > 0)
      {
        newOrderBy += ", ";
      }
      newOrderBy += orderBy;
    }

    return newOrderBy;
  }

  protected String GetAdditionalFilterFieldTableName(String sqlFieldName)
  {
    return null;
  }

  protected String ApplyListFilterToFrom(String from, ListFilter listFilter)
  {
    if (listFilter == null)
    {
      return from;
    }

    if (from == null)
    {
      from = _tableName;
    }

    String newFrom = "";

    String[] filterViewNames = listFilter.GetFilterViewNames();
    for (int i = 0; i < filterViewNames.length; i++)
    {
      String filterViewName = filterViewNames[i].trim();
      if (filterViewName.length() > 0)
      {
        String columnFieldName = GetViewToColumnNamesMap().get(filterViewName).toString(); // convert view name to sql column name
        String newTableName = GetAdditionalFilterFieldTableName(columnFieldName);
        if (newTableName != null)
        {
          if ((newFrom.indexOf(newTableName) == -1) && (from.indexOf(newTableName) == -1))
          {
            if (newFrom.length() > 0)
            {
              newFrom += ", ";
            }
            newFrom += newTableName + " ";
          }
        }
      }
    }
    if (from.length() > 0)
    {
      if (newFrom.length() > 0)
      {
        newFrom += ", ";
      }
      newFrom += from;
    }

    return newFrom;
  }

  public SQLStatement GetRowsAsResultSet(String predicate, String orderBy, ListFilter listFilter)
  {
    predicate = ApplyListFilterToPredicate(predicate, listFilter);
    orderBy = ApplyListFilterToOrderBy(orderBy, listFilter);
    String from = ApplyListFilterToFrom(_tableName, listFilter);
    int limit;
    if (listFilter == null)
    {
      limit = -1;
    }
    else
    {
      limit = listFilter.GetListLimit();
    }

    return GetRowsAsResultSet(from, predicate, orderBy, limit);
  }

  public SQLStatement GetRowsAsResultSet(String from, String predicate, String orderBy, int limit)
  {
    SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();
    String sqlString = ConstructSQLString(from, predicate, orderBy, limit);
    sqlStatement.ExecuteQuery(sqlString);
    return sqlStatement;
  }

  /**
   * do not override this method. override the other GetRowsWithSubstitutionsAsResultSet() method.
   */
  public SQLStatement GetRowsWithSubstitutionsAsResultSet(String predicate, String orderBy, ListFilter listFilter)
  {
    predicate = ApplyListFilterToPredicate(predicate, listFilter);
    orderBy = ApplyListFilterToOrderBy(orderBy, listFilter);
    String from = ApplyListFilterToFrom(_tableName, listFilter);
    int limit = listFilter.GetListLimit();

    return GetRowsWithSubstitutionsAsResultSet(from, predicate, orderBy, limit);
  }

  /**
   * it is intended that child classes will override this method and
   * subsititute values for foreign keys where needed
   */
  public SQLStatement GetRowsWithSubstitutionsAsResultSet(String from, String predicate, String orderBy, int limit)
  {
    return GetRowsAsResultSet(from, predicate, orderBy, limit);
  }

  public AbstractSQL1Row CreateRow()
  {
    AbstractSQL1Row row;
    row = GetEmptyRow().DeepCopy();
    row.Initialize(this);
    return row;
  }

  public AbstractSQL1Row CreateRow(AbstractSQL1Row initRow)
  {
    AbstractSQL1Row row;
    row = GetEmptyRow().DeepCopy();
    row.Initialize(this, initRow);
    return row;
  }

  public AbstractSQL1Row CreateRow(ResultSet resultSet)
  {
    AbstractSQL1Row row;
    row = GetEmptyRow().DeepCopy();
    row.Initialize(this, resultSet);
    return row;
  }

  @SuppressWarnings("resource")
  public ArrayList<? extends AbstractSQL1Row> GetRowsAsArrayList(String from, String predicate, String orderBy, int limit)
  {
    ArrayList<AbstractSQL1Row> arrayList = new ArrayList<>(100);
    // this is returned to the caller

    SQLStatement sqlStatement = GetRowsAsResultSet(from, predicate, orderBy, limit);
    ResultSet resultSet = sqlStatement.GetResultSet();

    try
    {
      while (resultSet.next())
      {
        AbstractSQL1Row row = CreateRow(resultSet);
        arrayList.add(row);
      }
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowMessageDialog(null, excp, "resultSet.next()", JOptionPane.ERROR_MESSAGE);
    }

    sqlStatement.MakeAvailable();

    arrayList.trimToSize();
    return arrayList;
  }

  /**
   * return false if you don't want this field used in the insert statement
   * return true if you do want this field used in the insert statement
   */
  protected boolean InsertWithThisField(String fieldName)
  {
    return true;
  }

  /**
   * redefine this if you want to massage certain values
   */
  protected String GetInsertValue(String fieldName, String value)
  {
    return value;
  }

  public boolean InsertRow(AbstractSQL1Row row)
  {
    row.Prepare();

    Field[] fields = row.GetSQLFields();

    int rc = -1;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "insert into " + _tableName + " ( ";

      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (InsertWithThisField(fieldName) == true)
        {
          sqlString += AbstractSQL1Row.GetFieldName(field) + ", ";
        }
      }
      // remove the trailing "," and close out the opening parenthesis
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ) ";

      sqlString += "values ( ";
      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (InsertWithThisField(fieldName) == true)
        {
          String value = row.GetInternalValue(field).toString();
          sqlString += "'" + SQLUtilities.FixSQLString(GetInsertValue(fieldName, value)) + "', ";
        }
      }
      // remove the trailing "," and close out the opening parenthesis
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ) ";

      rc = sqlStatement.ExecuteUpdate(sqlString);
      if (rc < 0)
      {
        DebugWindow.DisplayText(sqlString);
        DebugWindow.DisplayText("sql rc=" + rc);
      }
      _sqlDatabase.RecordEvent("Insert row into " + _tableName);
    }

    return (rc >= 0);
  }

  /**
   * return false if you don't want this field used in the update statement
   * return true if you do want this field used in the update statement
   */
  protected boolean UpdateWithThisField(String fieldName)
  {
    return true;
  }

  /**
   * redefine this if you want to massage certain values during an update
   */
  protected String GetUpdateValue(String fieldName, String value)
  {
    return value;
  }

  public boolean UpdateRow(AbstractSQL1Row row)
  {
    row.Prepare();
    Field[] fields = row.GetSQLFields();
    int rc = -1;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "update " + _tableName + " set ";

      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (UpdateWithThisField(fieldName) == true)
        {
          String value = row.GetInternalValue(field).toString();
          sqlString += fieldName + " = '" + SQLUtilities.FixSQLString(GetUpdateValue(fieldName, value)) + "', ";
        }
      }
      // remove the trailing ","
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ";
      sqlString += ConstructPredicate(row.GetPrimaryKeyPredicate());

      rc = sqlStatement.ExecuteUpdate(sqlString);
      if (rc < 0)
      {
        DebugWindow.DisplayText(sqlString);
        DebugWindow.DisplayText("sql rc=" + rc);
      }
      _sqlDatabase.RecordEvent("Update row in " + _tableName);
    }

    return (rc >= 0);
  }

  public boolean RemoveRow(AbstractSQL1Row row)
  {
    return RemoveRows(row.GetPrimaryKeyPredicate());
  }

  public boolean RemoveRows(String predicate)
  {
    int rc = -1;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "delete from " + _tableName + " where " + predicate;
      rc = sqlStatement.ExecuteUpdate(sqlString);
      if (rc < 0)
      {
        DebugWindow.DisplayText(sqlString);
        DebugWindow.DisplayText("sql rc=" + rc);
      }
      _sqlDatabase.RecordEvent("delete rows from " + _tableName);
    }
    
    return (rc >= 0);
  }

  public AbstractSQL1Row GetFirstRow(String predicate, String orderBy)
  {
    AbstractSQL1Row row = null; 
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = ConstructSQLString(_tableName, predicate, orderBy, 1);
 
      sqlStatement.ExecuteQuery(sqlString);
      
      try
      (ResultSet resultSet = sqlStatement.GetResultSet();)
      {
        if (resultSet.next() == true)
        {
          row = CreateRow(resultSet);
        }
      }
      catch (SQLException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        MessageDialog.ShowMessageDialog(null, excp, "resultSet.next()", JOptionPane.ERROR_MESSAGE);
      }
    }

    return row;
  }

  public AbstractSQL1Row GetLastRow(String predicate, String orderBy)
  {
    orderBy += " desc ";
    return GetFirstRow(predicate, orderBy);
  }

  public Object[] GetDistinctColumnValues(String columnName, String predicate)
  {
    Object valArray = null;

    String sqlString = "select distinct " + columnName + " from " + _tableName + ConstructPredicate(predicate) + " order by 1";
    try
    ( SQLStatement sqlStatement = _sqlDatabase.GetResultSetFromQuery(sqlString); 
      ResultSet resultSet = sqlStatement.GetResultSet();)
    {
      ResultSetMetaData resultSetMetaData = resultSet.getMetaData();
      String componentTypeName = GetJavaClassNameFromSQLTypeName(resultSetMetaData.getColumnTypeName(1));
      Class<?> componentType = Class.forName(componentTypeName);
      resultSet.last();
      int numOfRows = resultSet.getRow();
      valArray = java.lang.reflect.Array.newInstance(componentType, numOfRows);
      for (int i = 0; i < numOfRows; i++)
      {
        resultSet.absolute(i + 1);
        java.lang.reflect.Array.set(valArray, i, resultSet.getObject(columnName));
      }
    }
    catch (Exception excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "resultSet.absolute()");
    }

    return (Object[]) valArray;
  }

  /**
   * this is a convenience method because the jdbc resultSetMetaData.getColumnClassName
   * method throws a notYetImplemented exception! bah! when the jdbc package implements
   * the resultSetMetaData.getColumnClassName, use that instead of this.
   */
  protected String GetJavaClassNameFromSQLTypeName(String columnTypeName)
  {
    String className = columnTypeName;
    if (columnTypeName.equals("int4") == true)
    {
      className = java.lang.Integer.class.getName();
    }
    else
      if (columnTypeName.equals("varchar") == true)
      {
        className = java.lang.String.class.getName();
      }
      else
        if (columnTypeName.equals("decimal") == true)
        {
          className = java.lang.Double.class.getName();
        }
        else
          if (columnTypeName.equals("date") == true)
          {
            className = java.sql.Date.class.getName();
          }
          else
            if (columnTypeName.equals("time") == true)
            {
              className = java.sql.Time.class.getName();
            }
            else
              if (columnTypeName.equals("timestamp") == true)
              {
                className = java.sql.Timestamp.class.getName();
              }
    return className;
  }

  public String GetTableName()
  {
    return _tableName;
  }

  public AbstractSQL1Row GetEmptyRow()
  {
    return _row.DeepCopy();
  }

  public Field[] GetSQLFields()
  {
    return _row.GetSQLFields();
  }

  public String[] GetSQLFieldNames()
  {
    return _row.GetSQLFieldNames();
  }

  public String[] GetColumnNames()
  {
    return _row.GetColumnNames();
  }

  public String[] GetViewNames()
  {
    return CreateViewNames(GetColumnNames(), GetResources());
  }

  public Class<? extends AbstractSQL1Row> GetRowClass()
  {
    return _row.getClass();
  }

  public AbstractSQLDatabase GetSQLDatabase()
  {
    return _sqlDatabase;
  }

  public int GetColumnViewWidthAddOn(String columnName)
  {
    return DEFAULT_VIEW_WIDTH_ADD_ON;
  }

  protected abstract Resources GetResources();

  public abstract HashMap<String, String> GetColumnToViewNamesMap();

  public abstract HashMap<String, String> GetViewToColumnNamesMap();

  protected AbstractSQLDatabase _sqlDatabase;

  protected String              _tableName;

  protected AbstractSQL1Row     _row;

  // convenience method
  public static String GetFieldName(Field sqlField)
  {
    return AbstractSQL1Row.GetFieldName(sqlField);
  }

  // convenience method
  protected static String[] CreateViewNames(String[] columnNames, Resources resources)
  {
    String[] viewNames = (columnNames.clone());
    if (resources != null) // not all applications have resources (e.g. GateKeeper)
    {
      for (int i = 0; i < columnNames.length; i++)
      {
        try
        {
          viewNames[i] = resources.getProperty(columnNames[i]).toString();
        }
        catch (Exception excp)
        {
          new Exception("Property \"" + columnNames[i] + "\" not found in resources").printStackTrace();
          viewNames[i] = columnNames[i];
        }
      }
    }
    return viewNames;
  }

  /**
    this method prints out all the fields
  */
  @Override
  public String toString()
  {
    java.util.Hashtable<String, Object> h = new java.util.Hashtable<>();
    Class<? extends AbstractSQL1Table> cls = getClass();
    Field[] f = cls.getDeclaredFields();
    try
    {
      AccessibleObject.setAccessible(f, true);
      for (int i = 0; i < f.length; i++)
      {
        h.put(f[i].getName(), f[i].get(this));
      }
    }
    catch (SecurityException ex)
    {
      ex.printStackTrace();
    }
    catch (IllegalAccessException ex)
    {
      ex.printStackTrace();
    }
    if (cls.getSuperclass().getSuperclass() != null)
    {
      h.put("super", super.toString());
    }
    return cls.getName() + h;
  }

}
