package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: ListPanelCursor</p>
 * <p>Description:
 * cursor behavior for a list panel
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface ListPanelCursor
{
  public AbstractSQL1Row GetPreviousRow(AbstractSQL1Row sqlRow);
  public AbstractSQL1Row GetNextRow(AbstractSQL1Row sqlRow);
}
