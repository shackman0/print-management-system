package shackelford.floyd.printmanagementsystem.common;

import java.sql.Connection;
import java.util.ArrayList;


/**
 * <p>Title: SQLStatementArray</p>
 * <p>Description:
  this class provides a way to manage multiple SQLStatement's for a single
  database connection
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class SQLStatementArray
{

  private Connection              _databaseConnection;
  private ArrayList<SQLStatement>               _sqlStatementArray;

  public SQLStatementArray ( Connection databaseConnection )
  {
    _databaseConnection = databaseConnection;
    _sqlStatementArray = new ArrayList<>(5);
  }

  @Override
  protected void finalize()
  {
    ReleaseAll();
  }

  public SQLStatement GetAvailableSQLStatement ()
  {
    SQLStatement sqlStatement;
    for (int i = 0; i < _sqlStatementArray.size(); ++i)
    {
      sqlStatement = (_sqlStatementArray.get(i));
      if (sqlStatement.IsAvailable() == true)
      {
        sqlStatement.MakeUnavailable();
        return sqlStatement;
      }
    }
    sqlStatement = new SQLStatement(_databaseConnection);
    _sqlStatementArray.add(sqlStatement);
    return sqlStatement;
  }

  @SuppressWarnings("resource")
  public void ReleaseAll ()
  {
    for (int i = 0; i < _sqlStatementArray.size(); ++i)
    {
      SQLStatement sqlStatement = _sqlStatementArray.get(i);
      sqlStatement.CloseSQLStatement();
      _sqlStatementArray.remove(i);
    }
  }

}
