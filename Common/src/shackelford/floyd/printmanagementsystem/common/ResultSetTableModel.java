package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DecimalFormat;

import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;


/**
 * <p>Title: ResultSetTableModel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
class ResultSetTableModel
  extends AbstractTableModel
{

  /**
   * Eclipse generated value
   */
  private static final long            serialVersionUID   = -1067782935336537269L;

  protected SQLStatement               _sqlStmt;

  protected AbstractSQL1Table          _sqlTable;

  protected MyJTable                   _table;

  protected ListFilter                 _listFilter;

  protected String[]                   _columnTotals;

  protected AbstractSQL1Row            _sqlRowCache       = null;

  protected int                        _sqlRowCacheRowNum = -1;

  protected static Resources           __resources;

  protected static final DecimalFormat __intFormatter     = new DecimalFormat("#,##0");

  protected static final DecimalFormat __doubleFormatter  = new DecimalFormat("#,##0.000");

  protected static final String        SUM                = "SUM";

  protected static final String        COUNT              = "COUNT";

  public static final String           HTML_START         = "<html>";

  public static final String           HTML_BR            = "<br>";

  public ResultSetTableModel(MyJTable table, AbstractSQL1Table sqlTable, SQLStatement sqlStmt, ListFilter listFilter) // null is OK
  {
    super();
    _table = table;
    _listFilter = listFilter;
    SetSQLStatement(sqlTable, sqlStmt);
  }

  @Override
  public Class<?> getColumnClass(int columnIndex)
  {
    String columnName = _sqlTable.GetColumnNames()[columnIndex];
    Field rowColumnField = _sqlRowCache.GetSQLField(columnName, true);
    Class<?> rowColumnClass = rowColumnField.getType();

    return rowColumnClass;
    //return getValueAt(0,columnIndex).getClass();
  }

  @Override
  public String getColumnName(int modelColumnNumber)
  {
    String columnName;
    columnName = _sqlTable.GetColumnNames()[modelColumnNumber];
    Object obj = _sqlTable.GetColumnToViewNamesMap().get(columnName);
    if (obj != null)
    {
      columnName = obj.toString();
      if ((_listFilter != null) && (_listFilter.GetShowColumnTotals() == true))
      {
        // append the column total to the column name.
        // this feature requires that the column names be rendered in <html> format.
        columnName = ResultSetTableModel.HTML_START + columnName + ResultSetTableModel.HTML_BR + GetColumnTotal(modelColumnNumber);
      }
    }
    return columnName;
  }

  @Override
  public int getColumnCount()
  {
    return _sqlTable.GetColumnNames().length;
  }

  @Override
  public int getRowCount()
  {
    try (ResultSet resultSet = GetResultSet();)
    {
      if (resultSet != null)
      {
        resultSet.last();
        int rowCount = resultSet.getRow();
        return rowCount;
      }
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowMessageDialog(null, excp, "getRowCount()", JOptionPane.ERROR_MESSAGE);
    }

    return 0;
  }

  @Override
  public Object getValueAt(int rowNumber, int modelColumnNumber)
  {
    if (_sqlRowCacheRowNum != rowNumber)
    {
      _sqlRowCache = GetSQLRowAt(rowNumber);
      _sqlRowCacheRowNum = rowNumber;
    }
    int viewColumnNumber = _table.convertColumnIndexToView(modelColumnNumber);
    String viewName = _table.getColumnName(viewColumnNumber);
    String columnName = MapViewNameToColumnName(viewName);
    Object value = _sqlRowCache.GetValue(columnName);
    return value;
  }

  public AbstractSQL1Row[] GetSelectedSQLRows()
  {
    int[] selectedRows = _table.getSelectedRows();
    AbstractSQL1Row[] sqlRows = new AbstractSQL1Row[selectedRows.length];
    for (int i = 0; i < sqlRows.length; ++i)
    {
      sqlRows[i] = GetSQLRowAt(selectedRows[i]);
    }
    return sqlRows;
  }

  public AbstractSQL1Row GetSQLRowAt(int tableRowNum) // tables are 0 relative
  {
    AbstractSQL1Row sqlRow = null;
    try (ResultSet resultSet = GetResultSet();)
    {
      if (resultSet.absolute(tableRowNum + 1) == true) // result sets are 1 relative
      {
        sqlRow = _sqlTable.CreateRow(GetResultSet());
      }
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }

    return sqlRow;
  }

  protected ResultSet GetResultSet()
  {
    return _sqlStmt.GetResultSet();
  }

  protected ResultSetMetaData GetResultSetMetaData()
  {
    try (ResultSet resultSet = GetResultSet();)
    {
      return resultSet.getMetaData();
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "SetResultSet()");
    }

    return null;
  }

  protected void SetSQLStatement(AbstractSQL1Table sqlTable, SQLStatement sqlStmt)
  {
    _sqlStmt = sqlStmt;
    _sqlRowCacheRowNum = -1;
    _sqlTable = sqlTable;
    _sqlRowCache = _sqlTable.CreateRow();
    if ((_listFilter != null) && (_listFilter.GetShowColumnTotals() == true))
    {
      SetColumnTotals();
    }
    fireTableDataChanged();
  }

  protected void SetColumnTotals()
  {
    _columnTotals = new String[getColumnCount()];
    for (int columnIndx = 0; columnIndx < getColumnCount(); columnIndx++)
    {
      String totalsString = "";
      Class<?> columnClass = getColumnClass(columnIndx);
      String columnName = _sqlTable.GetColumnNames()[columnIndx];

      // this is kludgy. there's got to be a better way to do this. find one.
      if ((columnName.endsWith("_id") == true) || (columnName.equals("print_job_number") == true) || (columnName.equals("page_charge_number") == true))
      {
        columnClass = String.class;
      }

      if ((columnClass == Integer.class) || (columnClass == Double.class))
      {
        double sum = 0d;
        for (int rowIndx = 0; rowIndx < getRowCount(); rowIndx++)
        {
          sum += ((Number) getValueAt(rowIndx, columnIndx)).doubleValue();
        }
        if (columnClass == Integer.class)
        {
          totalsString = ResultSetTableModel.__intFormatter.format(sum);
        }
        else
        {
          totalsString = ResultSetTableModel.__doubleFormatter.format(sum);
        }
        totalsString = GetResources().getProperty(ResultSetTableModel.SUM) + " " + totalsString;
      }
      /*
            else
            {
              totalsString = GetResources().getProperty(COUNT) + " " + String.valueOf(getRowCount());
            }
      */
      _columnTotals[columnIndx] = totalsString;
    }
  }

  protected String GetColumnTotal(int columnNumber)
  {
    return _columnTotals[columnNumber];
  }

  protected String MapViewNameToColumnName(String viewName)
  {
    // the viewName may include the column totals line.
    // if it does, remove it and the html formatting commands
    if ((_listFilter != null) && (_listFilter.GetShowColumnTotals() == true))
    {
      // this is very dependent upon how the getColumnName() method
      // modifies the column header string. this method basically undoes
      // the changes made by the getColumnName() method.

      // remove the leading HTML_START
      viewName = viewName.substring(ResultSetTableModel.HTML_START.length());

      // remove everything after and including the first HTML_BR string
      viewName = viewName.substring(0, viewName.indexOf(ResultSetTableModel.HTML_BR));
    }
    return _sqlTable.GetViewToColumnNamesMap().get(viewName).toString();
  }

  protected Resources GetResources()
  {
    return ResultSetTableModel.__resources;
  }

  static
  {
    ResultSetTableModel.__resources = Resources.CreateResources("Utilities", ResultSetTableModel.class.getName());
  }

}
