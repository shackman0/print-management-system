package shackelford.floyd.printmanagementsystem.common;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


/**
 * <p>Title: AbstractEntry3Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractEntry3Panel
  extends AbstractEntry2Panel
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID        = 7838795401455321196L;

  protected JLabel              _rowInfoUpdatedDTSLabel = new JLabel();

  protected static final String UPDATED                 = "UPDATED";

  public AbstractEntry3Panel()
  {
    super();
  }

  @Override
  protected Box GetOptionalInfoRow()
  {
    Box lineBox = super.GetOptionalInfoRow();

    lineBox.add(Box.createHorizontalStrut(INTER_FIELD_GAP_WIDTH));
    lineBox.add(new JLabel(GetResources().getProperty(UPDATED), SwingConstants.RIGHT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    _rowInfoUpdatedDTSLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_rowInfoUpdatedDTSLabel);

    return lineBox;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();
    _rowInfoUpdatedDTSLabel.setText(GetSQL3Row().Get_updated_dts());
  }

  public AbstractSQL3Row GetSQL3Row()
  {
    return (AbstractSQL3Row) _sqlRow;
  }

  public AbstractSQL3Table GetSQL3Table()
  {
    return (AbstractSQL3Table) _sqlTable;
  }

}
