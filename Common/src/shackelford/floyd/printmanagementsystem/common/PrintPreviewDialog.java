package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.print.Book;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.util.Random;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;


/**
 * <p>Title: PrintPreviewDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
class PrintPreviewDialog
  extends JDialog
{

  /**
   * Eclipse generated value
   */
  private static final long   serialVersionUID = -3220581207184085167L;

  private static final String SCALE            = "SCALE";

  private static final String SCALE_TIP        = "SCALE_TIP";

  private static final String PREV_PAGE        = "PREV_PAGE";

  private static final String PREV_PAGE_TIP    = "PREV_PAGE_TIP";

  private static final String NEXT_PAGE        = "NEXT_PAGE";

  private static final String NEXT_PAGE_TIP    = "NEXT_PAGE_TIP";

  private static final String CLOSE            = "CLOSE";

  private static final String CLOSE_TIP        = "CLOSE_TIP";

  private static final int    PAPER_WIDTH      = 85;

  private static final int    PAPER_HEIGHT     = 110;

  private static final int    SIZE_MULT        = 5;

  public PrintPreviewDialog(Printable p, PageFormat pf, int pages)
  {
    Book book = new Book();
    book.append(p, pf, pages);
    layoutUI(book);
  }

  public PrintPreviewDialog(Book b)
  {
    layoutUI(b);
  }

  public void layoutUI(Book book)
  {

    addWindowListener(new WindowListener());

    setSize((PAPER_WIDTH * SIZE_MULT), (PAPER_HEIGHT * SIZE_MULT));

    Container contentPane = getContentPane();

    _canvas = new PrintPreviewCanvas(book);
    _scrollPane = new JScrollPane(_canvas);
    contentPane.add(_scrollPane, BorderLayout.CENTER);

    Box buttonPanel = Box.createHorizontalBox();

    _prevPageButton = new ActionButton(new PrevPageAction());
    _prevPageButton.setEnabled(false);
    buttonPanel.add(_prevPageButton);

    _nextPageButton = new ActionButton(new NextPageAction());
    _nextPageButton.setEnabled(_book.getNumberOfPages() > 1);
    buttonPanel.add(_nextPageButton);

    buttonPanel.add(Box.createHorizontalGlue());

    SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
    _scaleSpinField.setModel(spinnerNumberModel);

    buttonPanel.add(new JLabel(staticResources.getProperty(SCALE).toString(), SwingConstants.RIGHT));
    spinnerNumberModel.setValue(new Integer(100));
    spinnerNumberModel.setStepSize(new Integer(50));
    spinnerNumberModel.setMinimum(new Integer(50));
    spinnerNumberModel.setMaximum(new Integer(1000));
    _scaleSpinField.addChangeListener(new ScaleChangedAction());
    _scaleSpinField.setToolTipText(staticResources.getProperty(SCALE_TIP).toString());
    buttonPanel.add(_scaleSpinField);

    buttonPanel.add(Box.createHorizontalGlue());

    buttonPanel.add(new ActionButton(new CloseAction()));

    contentPane.add(buttonPanel, BorderLayout.SOUTH);

    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  PrintPreviewCanvas  _canvas                  = null;

  Dimension           _canvasOriginalDimension = null;

  Book                _book                    = null;

  int                 _currentPage             = 0;

  ActionButton        _prevPageButton          = null;

  ActionButton        _nextPageButton          = null;

  final JSpinner      _scaleSpinField          = new JSpinner();

  private JScrollPane _scrollPane              = null;

  static Resources    staticResources;

  private class PrintPreviewCanvas
    extends JPanel
  {

    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = -5761200847248824773L;

    public PrintPreviewCanvas(Book book)
    {
      super();
      _book = book;
      _currentPage = 0;
      setPreferredSize(new Dimension(540, 720));
    }

    @Override
    public void paintComponent(Graphics g)
    {
      super.paintComponent(g);
      Graphics2D g2 = (Graphics2D) g;
      PageFormat pageFormat = _book.getPageFormat(_currentPage);

      //double xoff = 0; // x offset of page start in window
      //double yoff = 0; // y offset of page start in window
      double scale = ((Integer) (_scaleSpinField.getValue())) / 100; // scale factor to fit page in window

      double px = pageFormat.getWidth();
      double py = pageFormat.getHeight();
      //double sx = getWidth() - 1;
      //double sy = getHeight() - 1;

      /*
            if (px / py < sx / sy) // center horizontally
            {
              scale = sy / py;
              xoff = 0.5 * (sx - scale * px);
              yoff = 0;
            }
            else // center vertically
            {
              scale = sx / px;
              xoff = 0;
              yoff = 0.5 * (sy - scale * py);
            }

            g2.translate((float)xoff, (float)yoff);
      */

      g2.scale((float) scale, (float) scale);

      // draw page outline (ignoring margins)
      Rectangle2D page = new Rectangle2D.Double(0, 0, px, py);
      g2.setPaint(Color.white);
      g2.fill(page);
      g2.setPaint(Color.black);
      g2.draw(page);
      g2.clip(new Rectangle2D.Double(0, 0, pageFormat.getImageableWidth(), pageFormat.getImageableHeight()));

      Printable printable = _book.getPrintable(_currentPage);
      try
      {
        printable.print(g2, pageFormat, _currentPage);
      }
      catch (PrinterException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        g2.draw(new Line2D.Double(0, 0, px, py));
        g2.draw(new Line2D.Double(0, px, 0, py));
      }
    }

  }

  protected class WindowListener
    extends WindowAdapter
  {

    @Override
    public void windowClosing(WindowEvent event)
    {
      dispose();
    }
  }

  protected class ScaleChangedAction
    implements ChangeListener
  {

    @Override
    public void stateChanged(ChangeEvent arg0)
    {
      if (_canvasOriginalDimension == null)
      {
        _canvasOriginalDimension = _canvas.getSize();
      }
      Dimension newDimension = new Dimension();
      newDimension.height = ((_canvasOriginalDimension.height * (Integer) (_scaleSpinField.getValue())) / 100);
      newDimension.width = ((_canvasOriginalDimension.height * (Integer) (_scaleSpinField.getValue())) / 100);
      _canvas.setSize(newDimension);
      invalidate();
    }

  }

  protected class PrevPageAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 3819248060589777574L;

    public PrevPageAction()
    {
      putValue(Action.NAME, staticResources.getProperty(PREV_PAGE).toString());
      putValue(Action.SHORT_DESCRIPTION, staticResources.getProperty(PREV_PAGE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _currentPage--;
      _nextPageButton.setEnabled(true);
      if (_currentPage == 0)
      {
        _prevPageButton.setEnabled(false);
      }
      repaint();
    }
  }

  protected class NextPageAction
    extends AbstractAction
  {
    /**
     * 
     */
    private static final long serialVersionUID = 337704058470808349L;

    public NextPageAction()
    {
      putValue(Action.NAME, staticResources.getProperty(NEXT_PAGE).toString());
      putValue(Action.SHORT_DESCRIPTION, staticResources.getProperty(NEXT_PAGE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      _currentPage++;
      _prevPageButton.setEnabled(true);
      if (_currentPage == (_book.getNumberOfPages() - 1))
      {
        _nextPageButton.setEnabled(false);
      }
      repaint();
    }
  }

  protected class CloseAction
    extends AbstractAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 3959022151232451224L;

    public CloseAction()
    {
      putValue(Action.NAME, staticResources.getProperty(CLOSE).toString());
      putValue(Action.SHORT_DESCRIPTION, staticResources.getProperty(CLOSE_TIP).toString());
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
      dispose();
    }
  }

  static
  {
    staticResources = Resources.CreateResources("Utilities", "PrintPreviewDialog");
  }

}
