package shackelford.floyd.printmanagementsystem.common;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

/**
 * <p>Title: CompressedObject</p>
 * <p>Description:
  This utility class is used to compress a Serializable object.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class CompressedObject
  implements Serializable
{
  /**
   * Eclipse generated value
   */
  private static final long serialVersionUID = 5510601765928670977L;

  /**
    This array contains the bytes of the compressed
    Serializable object.
  */
  private final byte[]      objectData;

  /**
    The constructor takes a Serializable and compresses
    it into the objectData instance variable.  A number
    of exceptions can occur during the compression
    process, which should invalidate the object.

    How it works:
    The object is serialized onto an ObjectOutputStream
    created atop a GZIP output stream created atop a
    ByteArray output stream.  The result is a byte[]
    containing the compressed serialized object.
  */
  public CompressedObject(Serializable o)
    throws Exception
  {

    try (ByteArrayOutputStream bs = new ByteArrayOutputStream(); GZIPOutputStream gzs = new GZIPOutputStream(bs); ObjectOutputStream os = new ObjectOutputStream(gzs);)
    {
      os.writeObject(o);
      objectData = bs.toByteArray();
    }
  }

  /**
    The decompress() method returns the object that was given
    to the constructor originally.  The decompression is performed
    as the inverse of the compression.  A ByteArray input stream
    is created using the objectData instance variable.  A GZIP
    input stream is created from that, and an ObjectInputStream
    created from that.  Then we just read the object from the
    ObjectInputStream as normal.  If an error occurs during
    decompression, this method will return null.
  */
  public Object decompress()
  {
    Object rc = null;

    try (ByteArrayInputStream bs = new ByteArrayInputStream(objectData); GZIPInputStream gzs = new GZIPInputStream(bs); ObjectInputStream is = new ObjectInputStream(gzs);)
    {
      rc = is.readObject();
    }
    catch (Exception e)
    {
      System.err.println("Couldn't decompress object: " + e);
    }

    return rc;
  }

  /**
   * unit test
   */
  public static void main(String args[])
  {
    byte[] bytes = new byte[100];
    for (int i = 0; i < bytes.length; ++i)
    {
      bytes[i] = (byte) 10;
    }

    CompressedObject compressedObject = null;

    try
    {
      compressedObject = new CompressedObject(bytes);
    }
    catch (Exception ex)
    {
      System.err.println("Caught exception: " + ex);
      System.exit(0);
    }

    @SuppressWarnings("null")
    Object out = compressedObject.decompress();

    if (out instanceof byte[])
    {
      byte[] outbytes = (byte[]) out;
      System.out.println("Got back a byte[]");
      System.out.println("Size of array is " + outbytes.length);
      for (int i = 0; i < outbytes.length; ++i)
      {
        if (outbytes[i] != bytes[i])
        {
          System.out.println("Arrays are different at index " + i);
          System.exit(1);
        }
      }
      System.out.println("Arrays are the same.");
    }
    else
    {
      System.out.println("Don't know what we have.");
    }
  }
}
