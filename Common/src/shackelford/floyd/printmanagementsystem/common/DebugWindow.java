package shackelford.floyd.printmanagementsystem.common;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


/**
 * <p>Title: DebugWindow</p>
 * <p>Description:
 * a dialog window for displaying debugging data
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class DebugWindow
  extends JFrame
{
  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID = -5148485040559154969L;

  protected Container           _northPanel;

  protected Container           _southPanel;

  protected Container           _eastPanel;

  protected Container           _westPanel;

  protected Container           _centerPanel;

  protected JScrollPane         _scrollPane;

  protected JTextArea           _textArea;

  protected static DebugWindow  __debugWindow    = null;

  protected static final String NEW_LINE         = "\n";

  protected DebugWindow()
  {
    super();
    Initialize();
  }

  protected void Initialize()
  {
    addWindowListener(new WindowListener());

    setIconImage(SystemUtilities.GetLogoIconImage());

    setTitle("Output Window");
    setResizable(true);

    Container contentPane = getContentPane();

    // build north panel

    _northPanel = CreateNorthPanel();
    contentPane.add(_northPanel, BorderLayout.NORTH);

    // build center panel

    _centerPanel = CreateCenterPanel();
    contentPane.add(_centerPanel, BorderLayout.CENTER);

    // build south panel

    _southPanel = CreateSouthPanel();
    contentPane.add(_southPanel, BorderLayout.SOUTH);

    // build east panel

    _eastPanel = CreateEastPanel();
    contentPane.add(_eastPanel, BorderLayout.EAST);

    // build west panel

    _westPanel = CreateWestPanel();
    contentPane.add(_westPanel, BorderLayout.WEST);

    pack();
    SizeInitialPanel();
    PositionInitialPanel();
    validate();
  }

  protected Container CreateNorthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateCenterPanel()
  {
    _scrollPane = new JScrollPane();

    _textArea = new JTextArea();
    _textArea.setColumns(80);
    //_textArea.setRows(1000);
    _textArea.setEditable(false);
    _textArea.setLineWrap(true);
    _textArea.setWrapStyleWord(true);

    _scrollPane.setViewportView(_textArea);

    return _scrollPane;
  }

  protected Container CreateWestPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateEastPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected Container CreateSouthPanel()
  {
    return new JPanel(new FlowLayout(FlowLayout.CENTER));
  }

  protected void SizeInitialPanel()
  {
    Dimension panelSize = getSize();
    panelSize.width = 600;
    panelSize.height = 300;
    setSize(panelSize);
  }

  protected void PositionInitialPanel()
  {
    Toolkit toolkit = Toolkit.getDefaultToolkit();
    Dimension screenSize = toolkit.getScreenSize();
    Random ran = new Random();
    int x = (screenSize.width - getWidth()) / 2;
    if (x > 0)
    {
      x = x + (ran.nextInt(x) - (x / 2));
    }
    else
    {
      x = 0;
    }
    int y = (screenSize.height - getHeight()) / 2;
    if (y > 0)
    {
      y = y + (ran.nextInt(y) - (y / 2));
    }
    else
    {
      y = 0;
    }
    setLocation(x, y);
  }

  protected class WindowListener
    extends WindowAdapter
  {
    @Override
    public void windowClosing(WindowEvent event)
    {
      setVisible(false);
    }
  }

  protected static void WriteText(String displayString)
  {
    if (GlobalAttributes.__verbose == true)
    {
      if (__debugWindow == null)
      {
        __debugWindow = new DebugWindow();
      }
      __debugWindow._textArea.append(displayString + NEW_LINE);
      __debugWindow._scrollPane.getVerticalScrollBar().setValue(__debugWindow._scrollPane.getVerticalScrollBar().getMaximum());
      __debugWindow.setVisible(true);
    }
  }

  public static void DisplayText(String displayString)
  {
    if (GlobalAttributes.__verbose == true)
    {
      WriteText(displayString);
    }
  }

  public static void DisplayStackTrace(Exception excp)
  {
    if (GlobalAttributes.__verbose == true)
    {
      StringWriter stringWriter = new StringWriter();
      PrintWriter printWriter = new PrintWriter(stringWriter);
      excp.printStackTrace(printWriter);
      WriteText(stringWriter.toString());
    }
  }

  public static void main(String[] args)
  {
    GlobalAttributes.__verbose = true;
    DebugWindow.DisplayText("this is a test");
    for (int i = 0; i < 500; ++i)
    {
      DebugWindow.DisplayText("line " + i);
    }
  }

}
