package shackelford.floyd.printmanagementsystem.common;



/**
 * <p>Title: AbstractSQL4Table</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL4Table
  extends AbstractSQL3Table
{

  protected String                _excludeDisabledItemsPredicate;

  public AbstractSQL4Table (
    AbstractSQLDatabase                sqlDatabase,
    String                             tableName,
    Class<? extends AbstractSQL1Row>   rowClass )
  {
    super(sqlDatabase,tableName,rowClass);
    SetExcludeDisabledItemsPredicate();
  }

  protected void SetExcludeDisabledItemsPredicate()
  {
    _excludeDisabledItemsPredicate =
      "( " + _tableName + "." + AbstractSQL4Row.Name_status() + " <> '" + AbstractSQL4Row.STATUS_DISABLED_CODE + "' )";
  }

  @Override
  protected String ApplyListFilterToPredicate (
    String       predicate,
    ListFilter   listFilter )
  {
    predicate = super.ApplyListFilterToPredicate(predicate,listFilter);

    if (listFilter == null)
    {
      return predicate;
    }

    if (listFilter.GetShowDisabledItems() == true)
    {
      if (predicate.trim().length() > 0)
      {
        predicate += " and ";
      }
      predicate += _excludeDisabledItemsPredicate;
    }
    return predicate;
  }

  protected AbstractSQL4Row GetAbstractSQL4Row()
  {
    return (AbstractSQL4Row)_row;
  }

}
