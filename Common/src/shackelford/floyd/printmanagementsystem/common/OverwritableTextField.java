package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;

import javax.swing.Action;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;
import javax.swing.text.JTextComponent;
import javax.swing.text.Keymap;
import javax.swing.text.TextAction;

/**
 * <p>Title: OverwritableTextField</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class OverwritableTextField
  extends JTextField
{

  /**
   * Eclipse generated value
   */
  private static final long       serialVersionUID      = 678745495169916337L;

  protected Caret                 _overwriteCaret;

  protected Caret                 _insertCaret;

  protected static boolean        overwriting           = false;

  public static final String      toggleOverwriteAction = "toggle-overwrite";

  protected static Keymap         sharedKeymap;

  protected static final String   keymapName            = "OverwriteMap";

  protected static final Action[] defaultActions        = { new ToggleOverwriteAction() };

  public OverwritableTextField()
  {
    this(null, null, 0);
  }

  public OverwritableTextField(String text)
  {
    this(null, text, 0);
  }

  public OverwritableTextField(int columns)
  {
    this(null, null, columns);
  }

  public OverwritableTextField(String text, int columns)
  {
    this(null, text, columns);
  }

  public OverwritableTextField(Document doc, String text, int columns)
  {
    super(doc, text, columns);
    _overwriteCaret = new OverwriteCaret();
    super.setCaret(overwriting ? _overwriteCaret : _insertCaret);
  }

  @Override
  public void setKeymap(Keymap map)
  {
    if (map == null)
    {
      super.setKeymap(null);
      sharedKeymap = null;
      return;
    }

    if (getKeymap() == null)
    {
      if (sharedKeymap == null)
      {
        // Switch keymaps. Add extra bindings.
        removeKeymap(keymapName);
        sharedKeymap = addKeymap(keymapName, map);
        loadKeymap(sharedKeymap, bindings, defaultActions);
      }
      map = sharedKeymap;
    }
    super.setKeymap(map);
  }

  @Override
  public void replaceSelection(String content)
  {
    Document doc = getDocument();
    if (doc != null)
    {
      // If we are not overwriting, just do the
      // usual insert. Also, if there is a selection,
      // just overwrite that (and that only).
      if ((overwriting == true) && (getSelectionStart() == getSelectionEnd()))
      {

        // Overwrite and no selection. Remove
        // the stretch that we will overwrite,
        // then use the usual code to insert the
        // new text.
        int insertPosition = getCaretPosition();
        int overwriteLength = doc.getLength() - insertPosition;
        int length = content.length();

        if (overwriteLength > length)
        {
          overwriteLength = length;
        }

        // Remove the range being overwritten
        try
        {
          doc.remove(insertPosition, overwriteLength);
        }
        catch (BadLocationException e)
        {
          // Won't happen
        }
      }
    }

    super.replaceSelection(content);
  }

  // Change the global overwriting mode
  public static void SetOverwriting(boolean isOverwriting)
  {
    OverwritableTextField.overwriting = isOverwriting;
  }

  public static boolean IsOverwriting()
  {
    return overwriting;
  }

  // Configuration of the insert caret

  @Override
  public void setCaret(Caret caret)
  {
    _insertCaret = caret;
  }

  // Allow configuration of a new
  // overwrite caret.
  public void SetOverwriteCaret(Caret caret)
  {
    _overwriteCaret = caret;
  }

  public Caret GetOverwriteCaret()
  {
    return _overwriteCaret;
  }

  // Caret switching

  @Override
  public void processFocusEvent(FocusEvent evt)
  {
    if (evt.getID() == FocusEvent.FOCUS_GAINED)
    {
      selectCaret();
    }
    super.processFocusEvent(evt);
  }

  protected void selectCaret()
  {
    // Select the appropriate caret for the
    // current overwrite mode.
    Caret newCaret = overwriting ? _overwriteCaret : _insertCaret;

    if (newCaret != getCaret())
    {
      Caret caret = getCaret();
      int mark = caret.getMark();
      int dot = caret.getDot();
      caret.setVisible(false);

      super.setCaret(newCaret);

      newCaret.setDot(mark);
      newCaret.moveDot(dot);
      newCaret.setVisible(true);
    }
  }

  protected static JTextComponent.KeyBinding[] bindings = { new JTextComponent.KeyBinding(KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, 0), toggleOverwriteAction) };

  // Insert/overwrite toggling action
  public static class ToggleOverwriteAction
    extends TextAction
  {
    /**
     * Eclipse generated value
     */
    private static final long serialVersionUID = 177667745980695614L;

    ToggleOverwriteAction()
    {
      super(toggleOverwriteAction);
    }

    @Override
    public void actionPerformed(ActionEvent evt)
    {
      OverwritableTextField.SetOverwriting(!OverwritableTextField.IsOverwriting());
      JTextComponent target = getFocusedComponent();
      if (target instanceof OverwritableTextField)
      {
        OverwritableTextField field = (OverwritableTextField) target;
        field.selectCaret();
      }
    }
  }

}
