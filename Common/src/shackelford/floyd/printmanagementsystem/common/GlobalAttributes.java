package shackelford.floyd.printmanagementsystem.common;




/**
 * <p>Title: GlobalAttributes</p>
 * <p>Description:
  this class holds global session variables for the system
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class GlobalAttributes
{

  public static final String           INSTALL_DIR = "/Program Files/DigiNet_LX/";
  public static final Integer          AUTOMATIC_UPGRADE_NUMBER = new Integer(3);
  public static final String           BUILD_DATE = "24May2002@1700cst";

  public static boolean                __verbose = false;
  public static String                 __resourcesDirectory = null;
  public static String                 __applicationName = null;
  public static String                 __dbAddr = null;
  public static String                 __dbUserID = null;
  public static String                 __dbPassword = null;
  public static String                 __dbPasswd = null;
  public static Class<?>                  __mainClass = null;

  private static int                   __majorVersion = -1;
  private static int                   __minorVersion = -1;
  private static int                   __fixLevel = -1;

  public static void SetMajorVersion(int majorVersion)
  {
    __majorVersion = majorVersion;
  }

  public static Integer GetMajorVersion()
  {
    return new Integer(__majorVersion);
  }

  public static void SetMinorVersion(int minorVersion)
  {
    __minorVersion = minorVersion;
  }

  public static Integer GetMinorVersion()
  {
    return new Integer(__minorVersion);
  }

  public static void SetFixLevel(int fixLevel)
  {
    __fixLevel = fixLevel;
  }

  public static Integer GetFixLevel()
  {
    return new Integer(__fixLevel);
  }

}