package shackelford.floyd.printmanagementsystem.common;

import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.SwingConstants;


/**
 * <p>Title: AbstractEntry2Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractEntry2Panel
  extends AbstractEntry1Panel
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID        = 5335363358890624904L;

  protected JLabel              _rowInfoCreatedDTSLabel = new JLabel();

  protected static final String CREATED                 = "CREATED";

  public AbstractEntry2Panel()
  {
    super();
  }

  @Override
  protected Box GetOptionalInfoRow()
  {
    Box lineBox = super.GetOptionalInfoRow();

    lineBox.add(new JLabel(GetResources().getProperty(CREATED), SwingConstants.RIGHT));
    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));
    _rowInfoCreatedDTSLabel.setHorizontalAlignment(SwingConstants.LEFT);
    lineBox.add(_rowInfoCreatedDTSLabel);

    return lineBox;
  }

  @Override
  protected void AssignRowToFields()
  {
    _rowInfoCreatedDTSLabel.setText(GetSQL2Row().Get_created_dts());
  }

  public AbstractSQL2Row GetSQL2Row()
  {
    return (AbstractSQL2Row) _sqlRow;
  }

  public AbstractSQL2Table GetSQL2Table()
  {
    return (AbstractSQL2Table) _sqlTable;
  }

}
