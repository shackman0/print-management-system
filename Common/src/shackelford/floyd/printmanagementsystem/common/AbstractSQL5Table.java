package shackelford.floyd.printmanagementsystem.common;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;


/**
 * <p>Title: AbstractSQL5Table</p>
 * <p>Description:
 * this class keeps track of information about the sql table.
 *
 * this class also provides set manipulation methods on sql rows. if you want information from a
 * single row, look for a method in the AbstractSQL5Row class. If you want methods that operate
 * upon 2 or more rows, look here.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSQL5Table
  extends AbstractSQL4Table
{

  protected static final String ROW_ID          = "row_id";

  protected static final String TRAILING_ROW_ID = "_" + ROW_ID;

  public AbstractSQL5Table(AbstractSQLDatabase sqlDatabase, String tableName, Class<? extends AbstractSQL1Row> rowClass)
  {
    super(sqlDatabase, tableName, rowClass);
  }

  public String GetNextRowID()
  {
    String nextRowID = AbstractSQL5Row.UNASSIGNED_ROW_ID;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      sqlStatement.BeginTransaction();
      String sqlString = "select (last_row_id + 1) from row_id_table for update";
      try
      (ResultSet resultSet = sqlStatement.ExecuteQuery(sqlString);)
      {
        resultSet.next(); // position us to the first row returned from the query
        nextRowID = resultSet.getString(1);
      }
      catch (SQLException excp)
      {
        DebugWindow.DisplayStackTrace(excp);
        MessageDialog.ShowMessageDialog(null, excp, "resultSet.next() or resultSet.getString(1)", JOptionPane.ERROR_MESSAGE);
      }
      sqlStatement.ExecuteUpdate("delete from row_id_table");
      sqlStatement.ExecuteUpdate("insert into row_id_table (last_row_id) values (" + nextRowID + ")");
      sqlStatement.CommitTransaction();
    }
    
    return nextRowID;

  }

  protected boolean OK2Remove(String rowID)
  {
    if (ValidRowID(rowID, false) == false)
    {
      return false;
    }
    AbstractSQL5Row sqlRow = GetRow(rowID);
    return sqlRow.OK2Remove();
  }

  protected boolean OK2Disable(String rowID)
  {
    if (ValidRowID(rowID, false) == false)
    {
      return false;
    }
    AbstractSQL5Row sqlRow = GetRow(rowID);
    return sqlRow.OK2Disable();
  }

  public boolean RemoveRow(String rowID)
  {
    if (ValidRowID(rowID, true) == false)
    {
      return false;
    }

    if (OK2Remove(rowID) == true)
    {
      try
      (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
      {
        String sqlString = "delete from " + _tableName + " where row_id = '" + rowID + "'";
        sqlStatement.ExecuteUpdate(sqlString);
      }
      _sqlDatabase.RecordEvent("Remove row " + rowID + " from " + _tableName);

      return true;
    }
    
    return false;
  }

  @Override
  protected String GetFilterFieldPredicate(String sqlFieldName, String sqlOperator, String fieldValue)
  {
    String predicate;
    String joinTableName = GetAdditionalFilterFieldTableName(sqlFieldName);
    if (joinTableName != null)
    {
      predicate = " ( " + joinTableName + "." + ROW_ID + " = " + _tableName + "." + sqlFieldName + " and " + joinTableName + "."
        + _sqlDatabase.GetTableByName(joinTableName).GetEmptyRow().GetJoinColumnPredicateName() + " " + sqlOperator + " '" + fieldValue + "' ) ";
    }
    else
    {
      predicate = super.GetFilterFieldPredicate(sqlFieldName, sqlOperator, fieldValue);
    }
    return predicate;
  }

  public AbstractSQL5Row CreateRow(boolean assignNewRowID)
  {
    AbstractSQL5Row row;
    row = (AbstractSQL5Row) (GetEmptyRow().DeepCopy());
    row.Initialize(this);
    if (assignNewRowID == true)
    {
      row.AssignNewRowID();
    }
    return row;
  }

  public AbstractSQL5Row CreateRow(AbstractSQL1Row initRow, boolean assignNewRowID)
  {
    AbstractSQL5Row row;
    row = (AbstractSQL5Row) (GetEmptyRow().DeepCopy());
    row.Initialize(this, initRow);
    if (assignNewRowID == true)
    {
      row.AssignNewRowID();
    }
    return row;
  }

  @Override
  public boolean InsertRow(AbstractSQL1Row rowIn)
  {
    AbstractSQL5Row row = (AbstractSQL5Row) rowIn;
    row.Prepare();

    Field[] fields = row.GetSQLFields();

    int rc = -1;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "insert into " + _tableName + " ( ";
  
      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (fieldName.equals(AbstractSQL2Row.Name_created_dts()) == true)
        {
          // do nothing
        }
        else
          if (fieldName.equals(AbstractSQL3Row.Name_updated_dts()) == true)
          {
            // do nothing
          }
          else
          {
            sqlString += AbstractSQL1Row.GetFieldName(field) + ", ";
          }
      }
      // remove the trailing "," and close out the opening parenthesis
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ) ";
  
      sqlString += "values ( ";
      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (fieldName.equals(AbstractSQL2Row.Name_created_dts()) == true)
        {
          // do nothing
        }
        else
          if (fieldName.equals(AbstractSQL3Row.Name_updated_dts()) == true)
          {
            // do nothing
          }
          else
          {
            String value = row.GetInternalValue(field).toString();
            sqlString += "'" + SQLUtilities.FixSQLString(value) + "', ";
          }
      }
      // remove the trailing "," and close out the opening parenthesis
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ) ";
  
      rc = sqlStatement.ExecuteUpdate(sqlString);
      _sqlDatabase.RecordEvent("Insert row " + row.Get_row_id() + " into " + _tableName);
    }

    return (rc >= 0);
  }

  @Override
  public boolean UpdateRow(AbstractSQL1Row rowIn)
  {
    AbstractSQL5Row row = (AbstractSQL5Row) rowIn;
    row.Prepare();
    Field[] fields = row.GetSQLFields();

    int rc = -1;
    try
    (SQLStatement sqlStatement = _sqlDatabase.GetSQLStatementArray().GetAvailableSQLStatement();)
    {
      String sqlString = "update " + _tableName + " set ";
  
      for (int i = 0; i < fields.length; i++)
      {
        Field field = fields[i];
        String fieldName = AbstractSQL1Row.GetFieldName(field);
        if (fieldName.equals(AbstractSQL5Row.Name_row_id()) == true)
        {
          // do nothing
        }
        else
          if (fieldName.equals(AbstractSQL2Row.Name_created_dts()) == true)
          {
            // do nothing
          }
          else
            if (fieldName.equals(AbstractSQL3Row.Name_updated_dts()) == true)
            {
              sqlString += fieldName + " = 'now', ";
            }
            else
            {
              String value = row.GetInternalValue(field).toString();
              sqlString += fieldName + " = '" + SQLUtilities.FixSQLString(value) + "', ";
            }
      }
      // remove the trailing ","
      sqlString = sqlString.substring(0, sqlString.length() - 2) + " ";
  
      sqlString += ConstructPredicate(row.GetPrimaryKeyPredicate());
  
      rc = sqlStatement.ExecuteUpdate(sqlString);
      _sqlDatabase.RecordEvent("Update row " + row.Get_row_id() + " in " + _tableName);
    }
    
    return (rc >= 0);
  }

  @Override
  protected String GetAdditionalFilterFieldTableName(String sqlFieldName)
  {
    String joinTableName = null;
    if (sqlFieldName.endsWith(TRAILING_ROW_ID) == true)
    {
      joinTableName = sqlFieldName.substring(0, sqlFieldName.length() - TRAILING_ROW_ID.length()) + TRAILING_TABLE;
    }
    return joinTableName;
  }

  public String[] GetRowIDs()
  {
    return (String[]) GetDistinctColumnValues(AbstractSQL5Row.Name_row_id(), null);
  }

  public AbstractSQL5Row GetRow(String rowID)
  {
    if (ValidRowID(rowID, true) == false)
    {
      return null;
    }

    String predicate = AbstractSQL5Row.Name_row_id() + " = " + rowID;
    String sqlString = ConstructSQLString(_tableName, predicate, null, NO_LIMIT);

    AbstractSQL5Row row = null;
    try
    ( SQLStatement sqlStatement = _sqlDatabase.GetResultSetFromQuery(sqlString);
      ResultSet resultSet = sqlStatement.GetResultSet();
    )
    {
      if (resultSet.next() == true)
      {
        row = (AbstractSQL5Row) (CreateRow(resultSet));
      }
    }
    catch (SQLException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowMessageDialog(null, excp, "resultSet.next()", JOptionPane.ERROR_MESSAGE);
    }

    return row;
  }

  protected AbstractSQL5Row GetAbstractSQL5Row()
  {
    return (AbstractSQL5Row) _row;
  }

  // convenience method
  public static boolean ValidRowID(String rowID, boolean printStackTrace)
  {
    if ((rowID == null) || (rowID.trim().length() == 0) || (rowID.equals(AbstractSQL5Row.UNASSIGNED_ROW_ID) == true))
    {
      if (printStackTrace == true)
      {
        new Exception("bad rowID = \"" + rowID + "\"").printStackTrace();
      }
      return false;
    }
    return true;
  }

}
