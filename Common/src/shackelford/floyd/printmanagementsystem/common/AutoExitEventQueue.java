package shackelford.floyd.printmanagementsystem.common;

import java.awt.AWTEvent;
import java.awt.EventQueue;


/**
 * <p>Title: AutoExitEventQueue</p>
 * <p>Description:
 * this class is used to get control when an event takes too long to process
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class AutoExitEventQueue
  extends EventQueue
{

  int                         _delay;    // miliseconds

  private final AutoExitTimer _exitTimer;

  public AutoExitEventQueue(int delay) // miliseconds
  {
    super();

    _delay = delay;

    _exitTimer = new AutoExitTimer();
    _exitTimer.setDaemon(true);
    _exitTimer.start();
  }

  @Override
  protected void dispatchEvent(AWTEvent event)
  {
    // we have an event before the timer expired
    _exitTimer.interrupt();

    // dispatch the event
    super.dispatchEvent(event);

    // restart the timer to see if we timeout before another event
    _exitTimer.interrupt();
  }

  protected class AutoExitTimer
    extends Thread
  {

    @Override
    public synchronized void run()
    {
      while (true)
      {
        try
        {
          //wait for event processing to reach the threshold, or
          //interruption from start/stopTimer()
          wait(_delay);

          // we are here because we didn't get an interrupt exception
          // iow: we timed out
          ProcessTimeOut();
        }
        catch (InterruptedException excp)
        {
          // do nothing
        }
      }
    }

    protected synchronized void ProcessTimeOut()
    {
      // launch this application again ...
      SystemUtilities.LaunchApplication(GlobalAttributes.__applicationName);
      // ... because we're getting out of here
      System.exit(0);
    }

  }

}
