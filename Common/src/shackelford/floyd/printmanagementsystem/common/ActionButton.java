package shackelford.floyd.printmanagementsystem.common;

import java.awt.event.ActionListener;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;



/**
 * <p>Title: ActionButton</p>
 * <p>Description:
  this class implements an action button. pass it an action and it
  creates a jbutton registed to look for the specified action.
  You must set the Action.NAME and the Action.SHORT_DESCRIPTION fields
  for this to work. Otherwise, you will get a null pointer exception.
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ActionButton
  extends JButton
{
  /**
   * 
   */
  private static final long serialVersionUID = -6773564197074905823L;

  public ActionButton()
  {
    super();
  }

  public ActionButton ( Action act )
  {
    super();
    this.addActionListener(act);
  }

  public ActionButton ( Action act, Icon icon )
  {
    super(icon);
    this.addActionListener(act);
  }

  public ActionListener GetActionListener()
  {
    return actionListener;
  }

  public void addActionListener(Action act)
  {
    setText(act.getValue(Action.NAME).toString());
    setToolTipText(act.getValue(Action.SHORT_DESCRIPTION).toString());
    super.addActionListener(act);
  }

}