package shackelford.floyd.printmanagementsystem.common;


/**
 * <p>Title: _DateChooserListener</p>
 * <p>Description: Interface to Listen for _DateChooserPoup actions</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface _DateChooserListener
{
  // method names
  public static final String DATE_CHOOSER_CANCELLED     = "DateChooserCancelled";
  public static final String DATE_CHOOSER_DATE_SELECTED = "DateChooserDateSelected";
  public static final String DATE_CHOOSER_TAB_SELECTED  = "DateChooserTabSelected";
  public static final String DATE_CHOOSER_LOST_FOCUS    = "DateChooserLostFocus";
  public static final String DATE_CHOOSER_DISMISSED     = "DateChooserDismissed";


  public void DateChooserCancelled(_DateChooserEvent event);

  public void DateChooserDateSelected(_DateChooserEvent event);

  public void DateChooserTabSelected(_DateChooserEvent event);

  public void DateChooserLostFocus(_DateChooserEvent event);

  public void DateChooserDismissed(_DateChooserEvent event);

}

