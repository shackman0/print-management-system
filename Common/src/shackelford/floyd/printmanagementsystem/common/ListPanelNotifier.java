package shackelford.floyd.printmanagementsystem.common;

import java.util.ArrayList;



/**
 * <p>Title: ListPanelNotifier</p>
 * <p>Description:
  use this interface to be a notifier of PanelListener objects. typically, a list
  panel will be a ListPanelListener, which calls a ListPanelNotifier to open an
  entry panel. When the entry panel is done, the List Panel needs to refresh its
  list.

  here's a typical implementation:


public class AnEntryPanelManager
  implements ListPanelNotifier
{

  private ArrayList                 _listPanelListeners;

  public void AddListPanelListener ( ListPanelListener listener )
  {
    if (_listPanelListeners == null)
    {
      _listPanelListeners = new ArrayList();
    }
    if (listener != null)
    {
      if (_listPanelListeners.indexOf(listener) == -1)
      {
        _listPanelListeners.add(listener);
      }
    }
  }

  public void AddListPanelListeners(ArrayList listeners)
  {
    if (_listPanelListeners == null)
    {
      _listPanelListeners = new ArrayList();
    }
    if (listeners != null)
    {
      for (int i = 0; i < listeners.size(); i++)
      {
        AddListPanelListener((ListPanelListener)(listeners.get(i)));
      }
    }
  }

  public void RemoveListPanelListener ( ListPanelListener listener )
  {
    if (listener != null)
    {
      int listenerIndex = _listPanelListeners.indexOf(listener);
      if (listenerIndex != -1)
      {
        _listPanelListeners.remove(listenerIndex);
      }
    }
  }

  public void NotifyListPanelListeners(boolean resetListenerList)
  {
    if (_listPanelListeners != null)
    {
      for (Iterator iterator = _listPanelListeners.iterator(); iterator.hasNext();)
      {
        ListPanelListener listener = (ListPanelListener)iterator.next();
        listener.RefreshList();
      }
      if (resetListenerList == true)
      {
        _listPanelListeners = null;
      }
    }
  }

}
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public interface ListPanelNotifier
{

  public static final char        ACTION_SAVE         = 's';
  public static final char        ACTION_SAVE_AND_NEW = 'n';
  public static final char        ACTION_CANCEL       = 'c';
  public static final char        ACTION_OK           = 'k';
  public static final char        ACTION_EXIT         = 'x';
  public static final char        ACTION_REFRESH      = 'r';

  /**
    this method lets a list panel be added to the list of panels to be notified
    when this panel exits.

    @param listener the object that is to be registered as a listener
  */
  public void AddListPanelListener ( ListPanelListener listener );

  /**
    this method to lets a list panel remove itself as a listener.

    @param listener the object to be removed as a listener
  */
  public void RemoveListPanelListener ( ListPanelListener listener );


  /**
    this method is used to let all listeners know the database has changed
  */
  public void NotifyListPanelListeners(boolean resetListenerList);

  public void AddListPanelListeners(ArrayList<ListPanelListener> listeners);

}
