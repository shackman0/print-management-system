package shackelford.floyd.printmanagementsystem.common;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Enumeration;

import javax.comm.CommPortIdentifier;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.SerialPort;
import javax.comm.UnsupportedCommOperationException;


/**
 * <p>Title: AbstractSerialPort</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractSerialPort
{

  protected String              _portName           = "COM1";

  protected int                 _baudRate           = 9600;

  protected int                 _dataBits           = SerialPort.DATABITS_8;

  protected int                 _parity             = SerialPort.PARITY_NONE;

  protected int                 _stopBits           = SerialPort.STOPBITS_1;

  protected int                 _flowControlIn      = SerialPort.FLOWCONTROL_RTSCTS_IN;

  protected int                 _flowControlOut     = SerialPort.FLOWCONTROL_RTSCTS_OUT;

  protected int                 _inputBufferSize    = 4096;

  protected int                 _outputBufferSize   = 4096;

  protected CommPortIdentifier  _portID             = null;

  protected SerialPort          _serialPort         = null;

  public OutputStream           _outputStream       = null;

  public InputStream            _inputStream        = null;

  protected static Resources    __resources         = null;

  protected static final String DATABITS_5          = "5";

  protected static final String DATABITS_6          = "6";

  protected static final String DATABITS_7          = "7";

  protected static final String DATABITS_8          = "8";

  protected static final String STOPBITS_1          = "1";

  protected static final String STOPBITS_2          = "2";

  protected static final String STOPBITS_1_5        = "1.5";

  protected static final String PARITY_NONE         = "none";

  protected static final String PARITY_ODD          = "odd";

  protected static final String PARITY_EVEN         = "even";

  protected static final String PARITY_MARK         = "mark";

  protected static final String PARITY_SPACE        = "space";

  protected static final String FLOWCONTROL_NONE    = "none";                           // no flow control

  protected static final String FLOWCONTROL_RTSCTS  = "rtscts";                         // RTS/CTS (hardware) flow control

  protected static final String FLOWCONTROL_XONXOFF = "xonxoff";                        //  XON/XOFF (software) flow control

  protected static final String UNDEFINED_STRING    = "undefined";

  protected static final int    UNDEFINED_INT       = -1;

  protected static final int    END_OF_FILE         = -1;

  protected static final int    PORT_OPEN_TIMEOUT   = 1000;                             // milliseconds

  protected static final int    WAIT_FOR_DATA_SLEEP = 100;                              // milliseconds

  public AbstractSerialPort()
  {
    super();
  }

  protected boolean InitializeDevice()
  {
    /*
    NOTE: be sure to install the javax.comm.properties file into the <jre>/lib directory.
          otherwise, no ports will be found.
    */
    if (GlobalAttributes.__verbose == true)
    {
      DebugWindow.DisplayText("available ports");
      Enumeration<?> portList = CommPortIdentifier.getPortIdentifiers();
      while (portList.hasMoreElements())
      {
        CommPortIdentifier portID = (CommPortIdentifier) portList.nextElement();
        DebugWindow.DisplayText("portID.getName()=" + portID.getName());
        DebugWindow.DisplayText("\tportID.getPortType()=" + portID.getPortType());
      }
    }

    try
    {
      _portID = CommPortIdentifier.getPortIdentifier(_portName);
    }
    catch (NoSuchPortException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp + " \"" + _portName + "\"", "Comm Port ID Error");
      return false;
    }

    try
    {
      _serialPort = (SerialPort) (_portID.open(this.getClass().getName(), PORT_OPEN_TIMEOUT));
    }
    catch (PortInUseException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Serial Port Open Error");
      return false;
    }

    _serialPort.setInputBufferSize(_inputBufferSize);
    _serialPort.setOutputBufferSize(_outputBufferSize);

    try
    {
      _outputStream = _serialPort.getOutputStream();
    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Serial Port Output Stream Error");
      return false;
    }

    try
    {
      _inputStream = _serialPort.getInputStream();
    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Serial Port Input Stream Error");
      return false;
    }

    try
    {
      _serialPort.setFlowControlMode(_flowControlIn | _flowControlOut);
    }
    catch (UnsupportedCommOperationException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      DebugWindow.DisplayText("_flowControlIn=" + _flowControlIn + ",_flowControlOut=" + _flowControlOut);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Serial Port Flow Control Mode Error");
      return false;
    }

    try
    {
      _serialPort.setSerialPortParams(_baudRate, _dataBits, _stopBits, _parity);
    }
    catch (UnsupportedCommOperationException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      DebugWindow.DisplayText("_baudRate=" + _baudRate + ",_dataBits=" + _dataBits + ",_stopBits=" + _stopBits + ",_parity=" + _parity);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Serial Port Parameters Error");
      return false;
    }

    return true;
  }

  protected boolean Write(String outputString)
  {
    return Write(outputString.getBytes());
  }

  protected boolean Write(byte outputByte)
  {
    byte[] bytes = new byte[] { outputByte };
    return Write(bytes);
  }

  protected boolean Write(byte[] outputBytes)
  {
    DebugWindow.DisplayText("writing \"" + outputBytes + "\"");
    try
    {
      _outputStream.write(outputBytes);
      _outputStream.flush();
    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Error Writing \"" + outputBytes + "\"");
      return false;
    }
    return true;
  }

  public static void Delay(int delay)
  {
    try
    {
      Thread.sleep(delay);
    }
    catch (InterruptedException excp)
    {
      // do nothing
    }
  }

  public void WaitForTheData()
  {
    // wait until the sender quits sending data or until the buffer gets full
    int previousAvailable = 0;
    try
    {
      do
      {
        previousAvailable = _inputStream.available();
        Delay(WAIT_FOR_DATA_SLEEP);
      }
      while ((_inputStream.available() < _inputBufferSize) && (previousAvailable < _inputStream.available()));

    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
    }
  }

  /**
   * returns null if end of file reached or an IOException occurs
   */
  protected String Read()
  {
    WaitForTheData();

    byte[] bytes = new byte[_inputBufferSize];
    int numberOfBytesRead = END_OF_FILE;
    try
    {
      numberOfBytesRead = _inputStream.read(bytes);
    }
    catch (IOException excp)
    {
      DebugWindow.DisplayStackTrace(excp);
      MessageDialog.ShowErrorMessageDialog(null, excp, "Read Error");
      return null;
    }
    String inputString = null;
    if (numberOfBytesRead != END_OF_FILE)
    {
      inputString = new String(bytes, 0, numberOfBytesRead);
    }
    DebugWindow.DisplayText("read()=\"" + inputString + "\"");

    return inputString;
  }

  public String Get_portName()
  {
    return new String(_portName);
  }

  public void Set_portName(String value)
  {
    _portName = new String(value);
  }

  public String Get_baudRate()
  {
    return String.valueOf(_baudRate);
  }

  public void Set_baudRate(String value)
    throws IllegalArgumentException
  {
    try
    {
      _baudRate = new Integer(value).intValue();
    }
    catch (NumberFormatException excp)
    {
      throw new IllegalArgumentException("" + excp);
    }
  }

  public String Get_dataBits()
  {
    switch (_dataBits)
    {
      case SerialPort.DATABITS_5:
        return DATABITS_5;
      case SerialPort.DATABITS_6:
        return DATABITS_6;
      case SerialPort.DATABITS_7:
        return DATABITS_7;
      case SerialPort.DATABITS_8:
        return DATABITS_8;
      default:
        return UNDEFINED_STRING;
    }
  }

  public void Set_dataBits(String value)
    throws IllegalArgumentException
  {
    if (value.equalsIgnoreCase(DATABITS_5) == true)
    {
      _dataBits = SerialPort.DATABITS_5;
    }
    else
      if (value.equalsIgnoreCase(DATABITS_6) == true)
      {
        _dataBits = SerialPort.DATABITS_6;
      }
      else
        if (value.equalsIgnoreCase(DATABITS_7) == true)
        {
          _dataBits = SerialPort.DATABITS_7;
        }
        else
          if (value.equalsIgnoreCase(DATABITS_8) == true)
          {
            _dataBits = SerialPort.DATABITS_8;
          }
          else
          {
            throw new IllegalArgumentException("Invalid dataBits value \"" + value + "\"");
          }
  }

  public String Get_parity()
  {
    switch (_parity)
    {
      case SerialPort.PARITY_NONE:
        return PARITY_NONE;
      case SerialPort.PARITY_ODD:
        return PARITY_ODD;
      case SerialPort.PARITY_EVEN:
        return PARITY_EVEN;
      case SerialPort.PARITY_MARK:
        return PARITY_MARK;
      case SerialPort.PARITY_SPACE:
        return PARITY_SPACE;
      default:
        return UNDEFINED_STRING;
    }
  }

  public void Set_parity(String value)
  {
    if (value.equalsIgnoreCase(PARITY_NONE) == true)
    {
      _parity = SerialPort.PARITY_NONE;
    }
    else
      if (value.equalsIgnoreCase(PARITY_ODD) == true)
      {
        _parity = SerialPort.PARITY_ODD;
      }
      else
        if (value.equalsIgnoreCase(PARITY_EVEN) == true)
        {
          _parity = SerialPort.PARITY_EVEN;
        }
        else
          if (value.equalsIgnoreCase(PARITY_MARK) == true)
          {
            _parity = SerialPort.PARITY_MARK;
          }
          else
            if (value.equalsIgnoreCase(PARITY_SPACE) == true)
            {
              _parity = SerialPort.PARITY_SPACE;
            }
            else
            {
              throw new IllegalArgumentException("Invalid parity value \"" + value + "\"");
            }
  }

  public String Get_stopBits()
  {
    switch (_stopBits)
    {
      case SerialPort.STOPBITS_1:
        return STOPBITS_1;
      case SerialPort.STOPBITS_1_5:
        return STOPBITS_1_5;
      case SerialPort.STOPBITS_2:
        return STOPBITS_2;
      default:
        return UNDEFINED_STRING;
    }
  }

  public void Set_stopBits(String value)
  {
    if (value.equalsIgnoreCase(STOPBITS_1) == true)
    {
      _stopBits = SerialPort.STOPBITS_1;
    }
    else
      if (value.equalsIgnoreCase(STOPBITS_1_5) == true)
      {
        _stopBits = SerialPort.STOPBITS_1_5;
      }
      else
        if (value.equalsIgnoreCase(STOPBITS_2) == true)
        {
          _stopBits = SerialPort.STOPBITS_2;
        }
        else
        {
          throw new IllegalArgumentException("Invalid stopBits value \"" + value + "\"");
        }
  }

  public String Get_flowControlIn()
  {
    switch (_flowControlIn)
    {
      case SerialPort.FLOWCONTROL_NONE:
        return FLOWCONTROL_NONE;
      case SerialPort.FLOWCONTROL_RTSCTS_IN:
        return FLOWCONTROL_RTSCTS;
      case SerialPort.FLOWCONTROL_XONXOFF_IN:
        return FLOWCONTROL_XONXOFF;
      default:
        return UNDEFINED_STRING;
    }
  }

  public void Set_flowControlIn(String value)
  {
    if (value.equalsIgnoreCase(FLOWCONTROL_NONE) == true)
    {
      _flowControlIn = SerialPort.FLOWCONTROL_NONE;
    }
    else
      if (value.equalsIgnoreCase(FLOWCONTROL_RTSCTS) == true)
      {
        _flowControlIn = SerialPort.FLOWCONTROL_RTSCTS_IN;
      }
      else
        if (value.equalsIgnoreCase(FLOWCONTROL_XONXOFF) == true)
        {
          _flowControlIn = SerialPort.FLOWCONTROL_XONXOFF_IN;
        }
        else
        {
          throw new IllegalArgumentException("Invalid flowControlIn value \"" + value + "\"");
        }
  }

  public String Get_flowControlOut()
  {
    switch (_flowControlOut)
    {
      case SerialPort.FLOWCONTROL_NONE:
        return FLOWCONTROL_NONE;
      case SerialPort.FLOWCONTROL_RTSCTS_OUT:
        return FLOWCONTROL_RTSCTS;
      case SerialPort.FLOWCONTROL_XONXOFF_OUT:
        return FLOWCONTROL_XONXOFF;
      default:
        return UNDEFINED_STRING;
    }
  }

  public void Set_flowControlOut(String value)
  {
    if (value.equalsIgnoreCase(FLOWCONTROL_NONE) == true)
    {
      _flowControlOut = SerialPort.FLOWCONTROL_NONE;
    }
    else
      if (value.equalsIgnoreCase(FLOWCONTROL_RTSCTS) == true)
      {
        _flowControlOut = SerialPort.FLOWCONTROL_RTSCTS_OUT;
      }
      else
        if (value.equalsIgnoreCase(FLOWCONTROL_XONXOFF) == true)
        {
          _flowControlOut = SerialPort.FLOWCONTROL_XONXOFF_OUT;
        }
        else
        {
          throw new IllegalArgumentException("Invalid flowControlOut value \"" + value + "\"");
        }
  }

  public String Get_inputBufferSize()
  {
    return String.valueOf(_inputBufferSize);
  }

  public void Set_inputBufferSize(String value)
    throws IllegalArgumentException
  {
    try
    {
      _inputBufferSize = new Integer(value).intValue();
    }
    catch (NumberFormatException excp)
    {
      throw new IllegalArgumentException("" + excp);
    }
  }

  public String Get_outputBufferSize()
  {
    return String.valueOf(_outputBufferSize);
  }

  public void Set_outputBufferSize(String value)
    throws IllegalArgumentException
  {
    try
    {
      _outputBufferSize = new Integer(value).intValue();
    }
    catch (NumberFormatException excp)
    {
      throw new IllegalArgumentException("" + excp);
    }
  }

}
