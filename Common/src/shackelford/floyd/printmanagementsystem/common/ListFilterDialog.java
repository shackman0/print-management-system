package shackelford.floyd.printmanagementsystem.common;

import java.awt.Container;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Properties;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;


/**
 * <p>Title: ListFilterDialog</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public class ListFilterDialog
  extends AbstractDialog
{

  /**
   * 
   */
  private static final long serialVersionUID = 7632004440759057704L;

  private final ListFilter              _listFilter;

  private final _JComboBox[]            _filterFieldNamesComboBoxes = new _JComboBox[ListFilter.NUMBER_OF_FILTER_FIELDS];

  private final _JComboBox[]            _filterOperatorsComboBoxes  = new _JComboBox[ListFilter.NUMBER_OF_FILTER_FIELDS];

  private final OverwritableTextField[] _filterFieldValueTextFields = new OverwritableTextField[ListFilter.NUMBER_OF_FILTER_FIELDS];

  private final _JComboBox[]            _sortFieldNamesComboBoxes   = new _JComboBox[ListFilter.NUMBER_OF_SORT_FIELDS];

  private final JRadioButton[]          _sortAscendingRadioButtons  = new JRadioButton[ListFilter.NUMBER_OF_SORT_FIELDS];

  private final JRadioButton[]          _sortDescendingRadioButtons = new JRadioButton[ListFilter.NUMBER_OF_SORT_FIELDS];

  private final JSpinner                _listLimitSpinField         = new JSpinner();

  private final JCheckBox               _showDisabledItemsCheckBox  = new JCheckBox();

  private final JCheckBox               _showForeignKeysCheckBox    = new JCheckBox();

  private final JCheckBox               _showColumnTotalsCheckBox   = new JCheckBox();

  private static Resources              __resources;

  private static final String           FILTER_                     = "FILTER_";

  private static final String           FIELD_NAMES_TIP             = "FIELD_NAMES_TIP";

  private static final String           FILTER_OPERATORS_TIP        = "FILTER_OPERATORS_TIP";

  private static final String           SORT_                       = "SORT_";

  private static final String           SORT_ASCENDING              = "SORT_ASCENDING";

  private static final String           SORT_ASCENDING_TIP          = "SORT_ASCENDING_TIP";

  private static final String           SORT_DESCENDING             = "SORT_DESCENDING";

  private static final String           SORT_DESCENDING_TIP         = "SORT_DESCENDING_TIP";

  private static final String           LIST_LIMIT                  = "LIST_LIMIT";

  private static final String           LIST_LIMIT_TIP              = "LIST_LIMIT_TIP";

  private static final String           LIST_LIMIT_INCREMENT        = "LIST_LIMIT_INCREMENT";

  private static final String           LIST_LIMIT_MAXIMUM          = "LIST_LIMIT_MAXIMUM";

  private static final String           SHOW_DISABLED_ITEMS         = "SHOW_DISABLED_ITEMS";

  private static final String           SHOW_DISABLED_ITEMS_TIP     = "SHOW_DISABLED_ITEMS_TIP";

  private static final String           SHOW_FOREIGN_KEYS           = "SHOW_FOREIGN_KEYS";

  private static final String           SHOW_FOREIGN_KEYS_TIP       = "SHOW_FOREIGN_KEYS_TIP";

  private static final String           SHOW_COLUMN_TOTALS          = "SHOW_COLUMN_TOTALS";

  private static final String           SHOW_COLUMN_TOTALS_TIP      = "SHOW_COLUMN_TOTALS_TIP";

  private static final int              FIELD_VALUES_COLUMNS        = 10;

  public ListFilterDialog(ListFilter listFilter)
  {
    super(listFilter.GetListPanel(), true);
    _listFilter = listFilter;
    super.Initialize();
  }

  @Override
  protected Container CreateCenterPanel()
  {
    JPanel centerPanel = new JPanel(new GridBagLayout());

    GridBagConstraints constraints = new GridBagConstraints();
    constraints.insets = new Insets(2, 4, 2, 2);
    constraints.ipadx = 2;
    constraints.ipady = 2;
    constraints.gridy = 0;
    constraints.weightx = 0;
    constraints.weighty = 0;

    // filters
    for (int i = 0; i < ListFilter.NUMBER_OF_FILTER_FIELDS; i++)
    {
      AddFilterFields(centerPanel, constraints, i);
    }

    // sort order
    for (int i = 0; i < ListFilter.NUMBER_OF_SORT_FIELDS; i++)
    {
      AddSortFields(centerPanel, constraints, i);
    }

    // list limit
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(LIST_LIMIT).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    Box lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    SpinnerNumberModel spinnerNumberModel = new SpinnerNumberModel();
    _listLimitSpinField.setModel(spinnerNumberModel);

    spinnerNumberModel.setStepSize(new Integer(GetResources().getProperty(LIST_LIMIT_INCREMENT).toString()));
    spinnerNumberModel.setMinimum(new Integer(1));
    spinnerNumberModel.setMaximum(new Integer(GetResources().getProperty(LIST_LIMIT_MAXIMUM).toString()));
    _listLimitSpinField.setToolTipText(GetResources().getProperty(LIST_LIMIT_TIP).toString());
    lineBox.add(_listLimitSpinField);

    // column totals
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(SHOW_COLUMN_TOTALS).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    _showColumnTotalsCheckBox.setToolTipText(GetResources().getProperty(SHOW_COLUMN_TOTALS_TIP).toString());
    lineBox.add(_showColumnTotalsCheckBox);

    // disabled items
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(SHOW_DISABLED_ITEMS).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    _showDisabledItemsCheckBox.setToolTipText(GetResources().getProperty(SHOW_DISABLED_ITEMS_TIP).toString());
    lineBox.add(_showDisabledItemsCheckBox);

    // foreign keys
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    centerPanel.add(new JLabel(GetResources().getProperty(SHOW_FOREIGN_KEYS).toString(), SwingConstants.RIGHT), constraints);

    constraints.gridx += constraints.gridwidth;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    _showForeignKeysCheckBox.setToolTipText(GetResources().getProperty(SHOW_FOREIGN_KEYS_TIP).toString());
    lineBox.add(_showForeignKeysCheckBox);

    return centerPanel;
  }

  protected void AddFilterFields(JPanel centerPanel, GridBagConstraints constraints, int filterNumber)
  {
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(FILTER_).toString());
    Object[] substitutionValues = new Object[] { new Integer(filterNumber + 1) };
    String msgString = msgFormat.format(substitutionValues).trim();

    centerPanel.add(new JLabel(msgString, SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    Box lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    _filterFieldNamesComboBoxes[filterNumber] = new _JComboBox();
    _filterFieldNamesComboBoxes[filterNumber].setEditable(false);
    //_filterFieldNamesComboBoxes[filterNumber].SetWidth(FIELD_NAMES_WIDTH);
    _filterFieldNamesComboBoxes[filterNumber].setToolTipText(GetResources().getProperty(FIELD_NAMES_TIP));
    lineBox.add(_filterFieldNamesComboBoxes[filterNumber]);

    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));

    _filterOperatorsComboBoxes[filterNumber] = new _JComboBox();
    _filterOperatorsComboBoxes[filterNumber].setEditable(false);
    _filterOperatorsComboBoxes[filterNumber].setToolTipText(GetResources().getProperty(FILTER_OPERATORS_TIP));
    lineBox.add(_filterOperatorsComboBoxes[filterNumber]);

    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));

    _filterFieldValueTextFields[filterNumber] = new OverwritableTextField();
    _filterFieldValueTextFields[filterNumber].setColumns(FIELD_VALUES_COLUMNS);
    _filterFieldValueTextFields[filterNumber].setToolTipText(GetResources().getProperty(FIELD_NAMES_TIP));
    lineBox.add(_filterFieldValueTextFields[filterNumber]);

  }

  protected void AddSortFields(JPanel centerPanel, GridBagConstraints constraints, int sortNumber)
  {
    constraints.gridy += 1;

    constraints.gridx = 0;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.EAST;

    MessageFormat msgFormat = new MessageFormat(GetResources().getProperty(SORT_).toString());
    Object[] substitutionValues = new Object[] { new Integer(sortNumber + 1) };
    String msgString = msgFormat.format(substitutionValues).trim();

    centerPanel.add(new JLabel(msgString, SwingConstants.RIGHT), constraints);

    constraints.gridx = 1;
    constraints.gridwidth = 1;
    constraints.gridheight = 1;
    constraints.fill = GridBagConstraints.NONE;
    constraints.anchor = GridBagConstraints.WEST;

    Box lineBox = Box.createHorizontalBox();
    centerPanel.add(lineBox, constraints);

    _sortFieldNamesComboBoxes[sortNumber] = new _JComboBox();
    _sortFieldNamesComboBoxes[sortNumber].setEditable(false);
    //_sortFieldNamesComboBoxes[sortNumber].SetWidth(FIELD_NAMES_WIDTH);
    _sortFieldNamesComboBoxes[sortNumber].setToolTipText(GetResources().getProperty(FIELD_NAMES_TIP).toString());
    lineBox.add(_sortFieldNamesComboBoxes[sortNumber]);

    lineBox.add(Box.createHorizontalStrut(INTRA_FIELD_GAP_WIDTH));

    ButtonGroup radioButtonGroup = new ButtonGroup();

    _sortAscendingRadioButtons[sortNumber] = new JRadioButton();
    _sortAscendingRadioButtons[sortNumber].setText(GetResources().getProperty(SORT_ASCENDING).toString());
    _sortAscendingRadioButtons[sortNumber].setToolTipText(GetResources().getProperty(SORT_ASCENDING_TIP).toString());
    lineBox.add(_sortAscendingRadioButtons[sortNumber]);

    _sortDescendingRadioButtons[sortNumber] = new JRadioButton();
    _sortDescendingRadioButtons[sortNumber].setText(GetResources().getProperty(SORT_DESCENDING).toString());
    _sortDescendingRadioButtons[sortNumber].setToolTipText(GetResources().getProperty(SORT_DESCENDING_TIP));
    lineBox.add(_sortDescendingRadioButtons[sortNumber]);

    radioButtonGroup.add(_sortAscendingRadioButtons[sortNumber]);
    radioButtonGroup.add(_sortDescendingRadioButtons[sortNumber]);
  }

  @Override
  protected void AssignFields()
  {
    String[] viewNames = _listFilter.GetListPanel().GetAbstractSQL1Table().GetViewNames();
    Arrays.sort(viewNames);

    // filters
    String[] filterViewNames = _listFilter.GetFilterViewNames();
    String[] filterOperators = _listFilter.GetFilterOperators();
    String[] filterValues = _listFilter.GetFilterValues();
    for (int i = 0; i < ListFilter.NUMBER_OF_FILTER_FIELDS; i++)
    {
      _filterFieldNamesComboBoxes[i].RecreateComboBox(viewNames, filterViewNames[i], true);
      _filterOperatorsComboBoxes[i].RecreateComboBox(ListFilter.SQL_FILTER_OPERATORS, filterOperators[i], false);
      _filterFieldValueTextFields[i].setText(filterValues[i]);
    }

    // sort order
    String[] sortViewNames = _listFilter.GetSortViewNames();
    String[] sortValues = _listFilter.GetSortValues();
    for (int i = 0; i < ListFilter.NUMBER_OF_SORT_FIELDS; i++)
    {
      _sortFieldNamesComboBoxes[i].RecreateComboBox(viewNames, sortViewNames[i], true);

      String sortFieldValue = sortValues[i];
      if (sortFieldValue.equals(ListFilter.SQL_SORT_DESCENDING) == true)
      {
        _sortDescendingRadioButtons[i].setSelected(true);
      }
      else
      {
        _sortAscendingRadioButtons[i].setSelected(true);
      }
    }

    // list limit
    _listLimitSpinField.setValue(new Integer(_listFilter.GetListLimit()));

    // column totals
    _showColumnTotalsCheckBox.setSelected(_listFilter.GetShowColumnTotals());

    // disabled items
    _showDisabledItemsCheckBox.setSelected(_listFilter.GetShowDisabledItems());

    // foreign keys
    _showForeignKeysCheckBox.setSelected(_listFilter.GetShowForeignKeys());
  }

  @Override
  protected Properties GetProperties()
  {
    // filters
    for (int i = 0; i < ListFilter.NUMBER_OF_FILTER_FIELDS; i++)
    {
      _listFilter.SetFilterViewName(i, _filterFieldNamesComboBoxes[i].getSelectedItem().toString().trim());
      _listFilter.SetFilterOperator(i, _filterOperatorsComboBoxes[i].getSelectedItem().toString().trim());
      _listFilter.SetFilterValue(i, _filterFieldValueTextFields[i].getText().trim());
    }

    // sort order
    for (int i = 0; i < ListFilter.NUMBER_OF_SORT_FIELDS; i++)
    {
      String sortFieldName = _sortFieldNamesComboBoxes[i].getSelectedItem().toString().trim();
      _listFilter.SetSortViewName(i, sortFieldName);
      String sortFieldValue = ListFilter.SQL_SORT_ASCENDING;
      if (sortFieldName.length() > 0)
      {
        if (_sortAscendingRadioButtons[i].isSelected() == true)
        {
          sortFieldValue = ListFilter.SQL_SORT_ASCENDING;
        }
        else
        {
          sortFieldValue = ListFilter.SQL_SORT_DESCENDING;
        }
      }
      _listFilter.SetSortValue(i, sortFieldValue);
    }

    // list limit
    _listFilter.SetListLimit((Integer) (_listLimitSpinField.getValue()));

    // column totals
    _listFilter.SetShowColumnTotals(_showColumnTotalsCheckBox.isSelected());

    // disabled items
    _listFilter.SetShowDisabledItems(_showDisabledItemsCheckBox.isSelected());

    // foreign keys
    _listFilter.SetShowForeignKeys(_showForeignKeysCheckBox.isSelected());

    Properties props = super.GetProperties();
    return props;
  }

  @Override
  protected Resources GetResources()
  {
    return __resources;
  }

  static
  {
    __resources = Resources.CreateResources("Utilities", ListFilterDialog.class.getName());
  }
}
