package shackelford.floyd.printmanagementsystem.common;

import javax.swing.JRadioButton;


/**
 * <p>Title: AbstractEntry4Panel</p>
 * <p>Description:
 *</p>
 * <p>Copyright (c) 2002, 2006, 2013 Floyd Shackelford</p>
 * <p>Company: n/a</p> 
 *
 * @author Floyd Shackelford
 * @version $Id$
 */
public abstract class AbstractEntry4Panel
  extends AbstractEntry3Panel
{

  /**
   * Eclipse generated value
   */
  private static final long     serialVersionUID     = -5706197707009325890L;

  protected JRadioButton        _enabledRadioButton  = new JRadioButton();

  protected JRadioButton        _disabledRadioButton = new JRadioButton();

  protected static final String STATUS               = "STATUS";

  protected static final String ENABLED              = "ENABLED";

  protected static final String ENABLED_TIP          = "ENABLED_TIP";

  protected static final String DISABLED             = "DISABLED";

  protected static final String DISABLED_TIP         = "DISABLED_TIP";

  public AbstractEntry4Panel()
  {
    super();
  }

  @Override
  protected boolean FieldsValid()
  {
    if ((_disabledRadioButton.isSelected() == true) && (GetSQL4Row().OK2Disable() == false))
    {
      return false;
    }
    return true;
  }

  @Override
  protected void AssignRowToFields()
  {
    super.AssignRowToFields();

    _enabledRadioButton.setSelected(GetSQL4Row().Get_status().equals(AbstractSQL4Row.STATUS_ENABLED_NAME));
    _disabledRadioButton.setSelected(GetSQL4Row().Get_status().equals(AbstractSQL4Row.STATUS_DISABLED_NAME));
  }

  @Override
  protected void AssignFieldsToRow()
  {
    GetSQL4Row().Set_status(_enabledRadioButton.isSelected() == true ? AbstractSQL4Row.STATUS_ENABLED_NAME : AbstractSQL4Row.STATUS_DISABLED_NAME);
  }

  public AbstractSQL4Row GetSQL4Row()
  {
    return (AbstractSQL4Row) _sqlRow;
  }

  public AbstractSQL4Table GetSQL4Table()
  {
    return (AbstractSQL4Table) _sqlTable;
  }

}
